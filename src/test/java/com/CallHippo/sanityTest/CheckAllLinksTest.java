package com.CallHippo.sanityTest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.ChangePasswordPage;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.web.numberAndUserSubscription.AddNumberPage;
import com.CallHippo.web.numberAndUserSubscription.InviteUserPage;

public class CheckAllLinksTest {
	
	WebDriver driverWebApp1;
	WebDriver driverPhoneAppMainUser;
	
	WebToggleConfiguration webAppMainUserLogin;
	
	DialerIndex phoneAppMainUser;
	
	PropertiesFile url;
	Common signUpExcel;
	WebDriver driverMail;
	SignupPage dashboard;
	SignupPage registrationPage;
	LoginPage loginpage;
	InviteUserPage inviteUserPageObj;
	AddNumberPage addNumberPage;
	LiveCallPage Web2liveCallPage;
	CheckAllLinksPage checkAllLinksPageWeb;
	CheckAllLinksPage checkAllLinksPageDialer;
	ChangePasswordPage changePasswordPage;
	XLSReader planPrice;
	
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");
	
	String gmailUser = data.getValue("gmailUser");
	String signUpUserEmailId="jigneshshah+15_09_2021_05_55_51@callhippo.com";
	
	public CheckAllLinksTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		signUpExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			// driver.get(url);S
			try {
				dashboard.deleteAllMail();
			} catch (Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}

	@BeforeMethod
	public void initialization() throws IOException {
		driverWebApp1 = TestBase.init2();
		webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
		registrationPage = PageFactory.initElements(driverWebApp1, SignupPage.class);
		loginpage = PageFactory.initElements(driverWebApp1, LoginPage.class);
		inviteUserPageObj = PageFactory.initElements(driverWebApp1, InviteUserPage.class);
		addNumberPage = PageFactory.initElements(driverWebApp1, AddNumberPage.class);
		checkAllLinksPageWeb = PageFactory.initElements(driverWebApp1, CheckAllLinksPage.class);
		Web2liveCallPage = PageFactory.initElements(driverWebApp1, LiveCallPage.class);
		changePasswordPage = PageFactory.initElements(driverWebApp1, ChangePasswordPage.class);
		
		driverPhoneAppMainUser = TestBase.init_dialer();
		checkAllLinksPageDialer = PageFactory.initElements(driverPhoneAppMainUser, CheckAllLinksPage.class);
		phoneAppMainUser = PageFactory.initElements(driverPhoneAppMainUser, DialerIndex.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, "Web Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneAppMainUser, testname, "PhoneApp Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, "Web Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneAppMainUser, testname, "PhoneApp Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		 driverWebApp1.quit();
		 driverPhoneAppMainUser.quit();
	}

	private String signUpUser() throws Exception {
		driverWebApp1.get(url.livesignUp());
		Common.pause(1);
		
		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signUpExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signUpExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signUpExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signUpExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signUpExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");

		return email;
	}
	
	private void LoginGmailAndConfirmYourMail() throws Exception {

		registrationPage.ValidateVerifyButtonOnGmail();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();
	}
	
	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		Common.pause(2);
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}
	
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void validate_links_for_web_dialer() throws Exception {

		String actualResult;
		String expectedResult;
	
		signUpUserEmailId=signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(2);
		
		try {
			driverPhoneAppMainUser.get(url.dialerSignIn());
			Common.pause(4);
			System.out.println("Opened phoneAap1 signin page");
			loginDialer(phoneAppMainUser, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
			System.out.println("loggedin phoneAap1");
		} catch (Exception e) {
			driverPhoneAppMainUser.get(url.dialerSignIn());
			Common.pause(4);
			System.out.println("Opened phoneAap1 signin page");
			loginDialer(phoneAppMainUser, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
			System.out.println("loggedin phoneAap1");
		}
		
//		driverWebApp1.navigate().to(url.signIn()); 
//		Common.pause(2);
//		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
		Common.pause(3);
		/*** verify web links before number purchase ***/
		Common.pause(2);
		checkAllLinksPageWeb.verify_DashboardPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_LiveCallPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_PowerDialerLiveCallPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenuNumbers_link();
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenuUsers_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Teams_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Integrations_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_dummy_ActivityFeed_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_CallPlanner_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_PowerDialer_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_CallScripts_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Reports_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_PlanBilling_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Settings_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_NeedHelp_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_UpgradeNow_link();
		registrationPage.closeAddnumberPopup();
		verify_ChangePassword_Page_Fields();
		
		/*** verify dialer links before number purchase ***/
		checkAllLinksPageDialer.waitForDialerPage();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_sideMenu_DialPad_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_LiveCall_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_AllCalls_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Contacts_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_CallPlanner_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Settings_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Credits_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_LeaderBoard_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Feedback_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_sideMenu_DialPad_link();
		
		purchase_1st_number();
		
		registrationPage.userLogout();
		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		registrationPage.closeAddnumberPopup();
		
		Common.pause(2);
		checkAllLinksPageWeb.verify_DashboardPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_LiveCallPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_PowerDialerLiveCallPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenuNumbers_link();
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenuUsers_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Teams_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Integrations_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_ActivityFeed_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_CallPlanner_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_PowerDialer_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_CallScripts_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Reports_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_PlanBilling_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Settings_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_NeedHelp_link();
		Common.pause(2);
		verify_ChangePassword_Page_Fields();
		
		/*** verify dialer links after number purchase ***/
		checkAllLinksPageDialer.waitForDialerPage();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_sideMenu_DialPad_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_LiveCall_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_AllCalls_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Contacts_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.clickOn_dialer_SideMenu_CallPlanner();
		checkAllLinksPageDialer.validate_dialer_Contacts_Page();
		assertEquals("Call reminder feature is not available in your plan.", checkAllLinksPageDialer.validationMessage());
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Settings_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Credits_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.clickOn_dialer_SideMenu_LeaderBoard();
		checkAllLinksPageDialer.validate_dialer_Credits_Page();
		assertEquals("Leader board feature is not available in your plan.", checkAllLinksPageDialer.validationMessage());
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Feedback_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_sideMenu_DialPad_link();
		
		invite_1st_user();
		
		registrationPage.userLogout();
		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
		Common.pause(2);
		checkAllLinksPageWeb.verify_DashboardPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_LiveCallPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_PowerDialerLiveCallPage();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenuNumbers_link();
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenuUsers_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Teams_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Integrations_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_ActivityFeed_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_CallPlanner_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_PowerDialer_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_CallScripts_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Reports_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_PlanBilling_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_Settings_link();
		Common.pause(2);
		checkAllLinksPageWeb.verify_leftMenu_NeedHelp_link();
		Common.pause(2);
		verify_ChangePassword_Page_Fields();
		
		/*** verify dialer links after invite user ***/
		checkAllLinksPageDialer.waitForDialerPage();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_sideMenu_DialPad_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_LiveCall_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_AllCalls_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Contacts_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.clickOn_dialer_SideMenu_CallPlanner();
		checkAllLinksPageDialer.validate_dialer_Contacts_Page();
		assertEquals("Call reminder feature is not available in your plan.", checkAllLinksPageDialer.validationMessage());
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Settings_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Credits_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.clickOn_dialer_SideMenu_LeaderBoard();
		checkAllLinksPageDialer.validate_dialer_Credits_Page();
		assertEquals("Leader board feature is not available in your plan.", checkAllLinksPageDialer.validationMessage());
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_dialer_sideMenu_Feedback_link();
		checkAllLinksPageDialer.clickOnDialerSideMenu();
		checkAllLinksPageDialer.verify_sideMenu_DialPad_link();
		
	}
	
	public void verify_ChangePassword_Page_Fields() {
		webAppMainUserLogin.clickOnChangePassword();
		driverWebApp1.navigate().refresh();
		Common.pause(3);
		assertEquals(changePasswordPage.validateCurrentPasswordTextBoxIsDisplayed(), true);
		assertEquals(changePasswordPage.validateNewPasswordTextBoxIsDisplayed(), true);
		//assertEquals(changePasswordPage.validateConfirmTextBoxIsDisplayed(), true);
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}
	
	public void purchase_1st_number() throws Exception {
		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
//		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
//		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
//				planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"), "1"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		driverWebApp1.navigate().to(url.numbersPage());
		Common.pause(2);
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));
	}
	
	public void invite_1st_user() throws Exception {
		Common.pause(2);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickYesBtnToConfirmInviteUser();
		
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.openInvitationMail();
		registrationPage.gClickOnRecentAcceptInvitationlink();
		registrationPage.switchToWindowBasedOnTitle("Login | Callhippo.com");
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
	}
	
	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}
	
}
