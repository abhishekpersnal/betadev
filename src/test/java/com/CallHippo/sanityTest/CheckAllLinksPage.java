package com.CallHippo.sanityTest;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class CheckAllLinksPage {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait1;
	WebDriverWait wait2;
	Actions action;
	JavascriptExecutor js;

	public CheckAllLinksPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait1 = new WebDriverWait(this.driver, 20);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		js = (JavascriptExecutor) driver;
	}
	
	
	@FindBy(xpath = "//div[contains(text(),'Dashboard')]")
	WebElement dashboardTab;
	
	@FindBy(xpath = "//h5[contains(.,'Total Users :')]")
	WebElement dashboardTotalUsersTxt;
	
	@FindBy(xpath = "//div[contains(text(),'Live Calls')]")
	WebElement livecallTab;
	
	@FindBy(xpath = "//p[contains(.,'Total Inbound Calls')]")
	WebElement inboundCallsText;
	
	@FindBy(xpath = "//div[contains(text(),'Power Dialer')]")
	WebElement powerDialerLiveCallTab;
	
	@FindBy(xpath = "//h5[contains(.,'Total Calls of Campaign : ')]")
	WebElement totalCallsOfCampaignText;
	
	@FindBy(xpath = "//a[contains(.,'Numbers')]")
	WebElement leftMenu_numbers_link;
	
	@FindBy(xpath = "//button[contains(.,'Add Number')]")
	WebElement add_number_button;
	
	@FindBy(xpath = "//a[contains(.,'Users')]")
	WebElement leftMenu_users_link;
	
	@FindBy(xpath = "//button[contains(.,'Invite Users')]")
	WebElement invite_user_button;
	
	@FindBy(xpath = "//a[contains(.,'Teams')]")
	WebElement leftMenu_teams_link;
	
	@FindBy(xpath = "//button[contains(.,'Create Team')]")
	WebElement create_team_button;
	
	@FindBy(xpath = "//a[contains(.,'Integrations')]")
	WebElement leftMenu_integrations_link;
	
	@FindBy(xpath = "//h2[contains(.,'Integrations')]")
	WebElement integrations_text;
	
	@FindBy(xpath = "//a[contains(.,'Activity Feed')]")
	WebElement leftMenu_activityFeed_link;
	
	
	@FindBy(xpath = "//h3[contains(.,'Activity Feeds')]")
	WebElement dummy_activityFeed_text;
	
	@FindBy(xpath = "//p[contains(.,'One-Stop place to view all the Calls / SMS sent or received in your account.')]")
	WebElement activityFeed_text;
	
	@FindBy(xpath = "//a[contains(.,'Call Planner')]")
	WebElement leftMenu_callPlanner_link;
	
	@FindBy(xpath = "//div[contains(@class,'abandonratepopover')][contains(.,'Scheduled')]")
	WebElement leftMenu_callPlanner_text;
	
	@FindBy(xpath = "//a[contains(.,'Power Dialer')]")
	WebElement leftMenu_powerDialer_link;
	
	@FindBy(xpath = "//h2[contains(.,'Power Dialer')]")
	WebElement leftMenu_powerDialer_text;
	
	@FindBy(xpath = "//a[contains(.,'Call Scripts')]")
	WebElement leftMenu_callScripts_link;
	
	@FindBy(xpath = "//h2[contains(.,'Call Scripts')]")
	WebElement leftMenu_callScripts_text;
	
	@FindBy(xpath = "//a[contains(.,'Reports')]")
	WebElement leftMenu_reports_link;
	
	@FindBy(xpath = "//div[contains(@class,'abandonratepopover')][contains(.,'Call Status Report')]")
	WebElement leftMenu_reports_text;
	
	@FindBy(xpath = "//a[contains(.,'Plan and Billing')]")
	WebElement leftMenu_planBilling_link;
	
	@FindBy(xpath = "//div[contains(@class,'plantitle')][contains(.,'Plan')]")
	WebElement leftMenu_planBilling_text;
	
	@FindBy(xpath = "//a[contains(.,'Settings')]")
	WebElement leftMenu_settings_link;
	
	
	@FindBy(xpath = "//h3[contains(.,'Holidays')]")
	WebElement leftMenu_settings_text;
	
	@FindBy(xpath = "//span[contains(.,'Need Help')]")
	WebElement leftMenu_needHelp_link;
	
	@FindBy(xpath = "//span[contains(.,'Help Documentation')]")
	WebElement leftMenu_needHelp_text;
	
	@FindBy(xpath = "//span[contains(.,'Upgrade Now!')]")
	WebElement leftMenu_upgradeNow_link;
	
	@FindBy(xpath = "//h3[contains(.,'Select your plan and you are done!')]")
	WebElement leftMenu_upgradeNow_text;
	

	/*** Dialer objects ***/
	
	@FindBy(xpath = "//*[@id=\"backSpace\"]")
	WebElement backBtn;
	
	@FindBy(id = "btnDraw")
	WebElement menuSlider;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Dialpad')]")
	WebElement sideMenu_DialPad;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Live Calls')]")
	WebElement sideMenu_LiveCalls;
	
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Live Calls')]")
	WebElement sideMenu_LiveCalls_text;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'All calls/Inbox')]")
	WebElement sideMenu_AllCalls;
	
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Call Logs')]")
	WebElement sideMenu_Call_Logs_text;
	
	@FindBy(xpath = "//li[contains(.,'Contacts')]")
	WebElement sideMenu_Contacts;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'Contacts')]")
	WebElement sideMenu_Contacts_title;
	
	@FindBy(xpath = "//li[contains(.,'SMS')]")
	WebElement sideMenu_SMS;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'SMS')]")
	WebElement sideMenu_SMS_title;
	
	@FindBy(xpath = "//li[contains(.,'Call Planner')]")
	WebElement sideMenu_CallPlanner;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'Call Reminder')]")
	WebElement sideMenu_CallPlanner_title;
	
	@FindBy(xpath = "//li[contains(.,'Settings')]")
	WebElement sideMenu_Settings;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'Settings')]")
	WebElement sideMenu_Settings_title;
	
	@FindBy(xpath = "//li[contains(.,'Credits')]")
	WebElement sideMenu_Credits;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'Credits')]")
	WebElement sideMenu_Credits_title;
	
	@FindBy(xpath = "//li[contains(.,'Leader Board')]")
	WebElement sideMenu_Leaderboard;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'Leader Board')]")
	WebElement sideMenu_Leaderboard_title;
	
	@FindBy(xpath = "//li[contains(.,'Feedback')]")
	WebElement sideMenu_Feedback;
	
	@FindBy(xpath = "//div[@class='headTitleSubTitle']/span[contains(.,'Feedback')]")
	WebElement sideMenu_Feedback_title;
	
	@FindBy(xpath = "//div[@class='toast-body']/span")
	WebElement validationMessage;
	
	
	
	
	public void verify_DashboardPage() {
		clickOnDashboardTab();
		Common.pause(3);
		assertEquals(validateTotalUsersText(), true);
	}
	
	public void clickOnDashboardTab() {
		wait.until(ExpectedConditions.visibilityOf(dashboardTab)).click();
	}
	
	public boolean validateTotalUsersText() {
		wait2.until(ExpectedConditions.visibilityOf(dashboardTotalUsersTxt));
		return true;
	}
	
	public void verify_LiveCallPage() {
		clickOnLiveCallTab();
		Common.pause(3);
		if (validateSubscriptionPopup()==false) {
			assertEquals(validateInboundCallsText(), true);
		}
	}
	
	public void clickOnLiveCallTab() {
		wait.until(ExpectedConditions.visibilityOf(livecallTab)).click();
	}
	
	public boolean validateSubscriptionPopup() {
		boolean subscriptionPopupDisplay = false;
		int size = driver.findElements(By.xpath("//h2[contains(.,'Subscription Details')]")).size();
		if (size > 0) {
			Common.pause(1);
			action.sendKeys(Keys.ESCAPE).build().perform();
			Common.pause(1);
			subscriptionPopupDisplay = true;
		}
		return subscriptionPopupDisplay;
	}
	
	public boolean validateInboundCallsText() {
		wait2.until(ExpectedConditions.visibilityOf(inboundCallsText));
		return true;
	}
	
	public void verify_PowerDialerLiveCallPage() {
		clickOnPowerDialerLiveCallTab();
		Common.pause(3);
		assertEquals(validateTotalCallsOfCampaignText(), true);
	}
	
	public void clickOnPowerDialerLiveCallTab() {
		wait.until(ExpectedConditions.visibilityOf(powerDialerLiveCallTab)).click();
	}
	
	public boolean validateTotalCallsOfCampaignText() {
		wait2.until(ExpectedConditions.visibilityOf(totalCallsOfCampaignText));
		return true;
	}
	
	public void verify_leftMenuNumbers_link() {
		clickLeftMenuNumbers();
		Common.pause(3);
		assertEquals(validateNumbersPage(), true);
	}
	
	public void clickLeftMenuNumbers() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_numbers_link));
		leftMenu_numbers_link.click();
		Common.pause(1);
	}
	
	public boolean validateNumbersPage() {
		wait2.until(ExpectedConditions.visibilityOf(add_number_button));
		return true;
	}
	
	public void verify_leftMenuUsers_link() {
		clickLeftMenuUsers();
		Common.pause(3);
		assertEquals(validateUsersPage(), true);
	}
	
	public void clickLeftMenuUsers() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_users_link));
		leftMenu_users_link.click();
		Common.pause(1);
	}
	
	public boolean validateUsersPage() {
		wait2.until(ExpectedConditions.visibilityOf(invite_user_button));
		return true;
	}
	
	public void verify_leftMenu_Teams_link() {
		clickLeftMenuTeams();
		Common.pause(3);
		assertEquals(validateTeamPage(), true);
	}
	
	public void clickLeftMenuTeams() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_teams_link));
		leftMenu_teams_link.click();
		Common.pause(1);
	}
	
	public boolean validateTeamPage() {
		wait2.until(ExpectedConditions.visibilityOf(create_team_button));
		return true;
	}
	
	public void verify_leftMenu_Integrations_link() {
		clickLeftMenuIntegrations();
		Common.pause(3);
		assertEquals(validateIntegrationsPage(), true);
	}
	
	public void clickLeftMenuIntegrations() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_integrations_link));
		leftMenu_integrations_link.click();
		Common.pause(1);
	}
	
	public boolean validateIntegrationsPage() {
		wait2.until(ExpectedConditions.visibilityOf(integrations_text));
		return true;
	}
	
	public void verify_leftMenu_dummy_ActivityFeed_link() {
		clickLeftMenuActivityFeed();
		Common.pause(3);
		assertEquals(validateDummyActivityFeedPage(), true);
	}
	
	public void verify_leftMenu_ActivityFeed_link() {
		clickLeftMenuActivityFeed();
		Common.pause(3);
		assertEquals(validateActivityFeedPage(), true);
	}
	
	public void clickLeftMenuActivityFeed() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_activityFeed_link));
		leftMenu_activityFeed_link.click();
		Common.pause(1);
	}
	
	public boolean validateDummyActivityFeedPage() {
		wait2.until(ExpectedConditions.visibilityOf(dummy_activityFeed_text));
		return true;
	}
	
	public boolean validateActivityFeedPage() {
		wait2.until(ExpectedConditions.visibilityOf(activityFeed_text));
		return true;
	}
	
	public void verify_leftMenu_CallPlanner_link() {
		clickLeftMenuCallPlanner();
		Common.pause(3);
		if (validateSubscriptionPopup()==false) {
			assertEquals(validateCallPlannerPage(), true);
		}
		
	}
	
	public void clickLeftMenuCallPlanner() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_callPlanner_link));
		leftMenu_callPlanner_link.click();
		Common.pause(1);
	}
	
	public boolean validateCallPlannerPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_callPlanner_text));
		return true;
	}
	
	public void verify_leftMenu_PowerDialer_link() {
		clickLeftMenuPowerDialer();
		Common.pause(3);
		assertEquals(validatePowerDialerPage(), true);
	}
	
	public void clickLeftMenuPowerDialer() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_powerDialer_link));
		leftMenu_powerDialer_link.click();
		Common.pause(1);
	}
	
	public boolean validatePowerDialerPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_powerDialer_text));
		return true;
	}
	
	public void verify_leftMenu_CallScripts_link() {
		clickLeftMenuCallScripts();
		Common.pause(3);
		assertEquals(validateCallScriptsPage(), true);
	}
	
	public void clickLeftMenuCallScripts() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_callScripts_link));
		leftMenu_callScripts_link.click();
		Common.pause(1);
	}
	
	public boolean validateCallScriptsPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_callScripts_text));
		return true;
	}
	
	public void verify_leftMenu_Reports_link() {
		clickLeftMenuReports();
		Common.pause(3);
		assertEquals(validateReportsPage(), true);
	}
	
	public void clickLeftMenuReports() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_reports_link));
		leftMenu_reports_link.click();
		Common.pause(1);
	}
	
	public boolean validateReportsPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_reports_text));
		return true;
	}
	
	public void verify_leftMenu_PlanBilling_link() {
		clickLeftMenuPlanBilling();
		Common.pause(3);
		assertEquals(validatePlanBillingPage(), true);
	}
	
	public void clickLeftMenuPlanBilling() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_planBilling_link));
		leftMenu_planBilling_link.click();
		Common.pause(1);
	}
	
	public boolean validatePlanBillingPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_planBilling_text));
		return true;
	}
	
	public void verify_leftMenu_Settings_link() {
		clickLeftMenuSettings();
		Common.pause(3);
		assertEquals(validateSettingsPage(), true);
	}
	
	public void clickLeftMenuSettings() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_settings_link));
		leftMenu_settings_link.click();
		Common.pause(1);
	}
	
	public boolean validateSettingsPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_settings_text));
		return true;
	}
	
	public void verify_leftMenu_NeedHelp_link() {
		clickLeftMenuNeedHelp();
		Common.pause(3);
		assertEquals(validateNeedHelpPage(), true);
	}
	
	public void clickLeftMenuNeedHelp() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_needHelp_link));
		leftMenu_needHelp_link.click();
		Common.pause(1);
	}
	
	public boolean validateNeedHelpPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_needHelp_text));
		return true;
	}
	
	public void verify_leftMenu_UpgradeNow_link() {
		clickLeftMenuUpgradeNow();
		Common.pause(3);
		assertEquals(validateUpgradeNowPage(), true);
	}
	
	public void clickLeftMenuUpgradeNow() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_upgradeNow_link));
		leftMenu_upgradeNow_link.click();
		Common.pause(1);
	}
	
	public boolean validateUpgradeNowPage() {
		wait2.until(ExpectedConditions.visibilityOf(leftMenu_upgradeNow_text));
		return true;
	}
	
	
	public void waitForDialerPage() {
		wait.until(ExpectedConditions.visibilityOf(backBtn));
	}
	
	public void clickOnDialerSideMenu() {
		wait.until(ExpectedConditions.visibilityOf(menuSlider));
		menuSlider.click();
	}
	
	public void verify_sideMenu_DialPad_link() {
		clickOnSideMenuDialPad();
		Common.pause(3);
		assertEquals(validateDialPadPage(), true);
	}
	
	public void clickOnSideMenuDialPad() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_DialPad));
		sideMenu_DialPad.click();
	}
	
	public boolean validateDialPadPage() {
		wait2.until(ExpectedConditions.visibilityOf(backBtn));
		return true;
	}
	
	public void verify_dialer_sideMenu_LiveCall_link() {
		clickOn_dialer_SideMenu_LiveCalls();
		Common.pause(3);
		assertEquals(validate_dialer_LiveCalls_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_LiveCalls() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_LiveCalls));
		sideMenu_LiveCalls.click();
	}
	
	public boolean validate_dialer_LiveCalls_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_LiveCalls_text));
		return true;
	}
	
	public void verify_dialer_sideMenu_AllCalls_link() {
		clickOn_dialer_SideMenu_AllCalls();
		Common.pause(3);
		assertEquals(validate_dialer_AllCalls_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_AllCalls() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_AllCalls));
		sideMenu_AllCalls.click();
	}
	
	public boolean validate_dialer_AllCalls_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_Call_Logs_text));
		return true;
	}
	
	public void verify_dialer_sideMenu_Contacts_link() {
		clickOn_dialer_SideMenu_Contacts();
		Common.pause(3);
		assertEquals(validate_dialer_Contacts_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_Contacts() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_Contacts));
		sideMenu_Contacts.click();
	}
	
	public boolean validate_dialer_Contacts_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_Contacts_title));
		return true;
	}
	
	public void verify_dialer_sideMenu_SMS_link() {
		clickOn_dialer_SideMenu_SMS();
		Common.pause(3);
		assertEquals(validate_dialer_SMS_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_SMS() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_SMS));
		sideMenu_SMS.click();
	}
	
	public boolean validate_dialer_SMS_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_SMS_title));
		return true;
	}
	
	public void verify_dialer_sideMenu_CallPlanner_link() {
		clickOn_dialer_SideMenu_CallPlanner();
		Common.pause(3);
		assertEquals(validate_dialer_CallPlanner_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_CallPlanner() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_CallPlanner));
		sideMenu_CallPlanner.click();
	}
	
	public boolean validate_dialer_CallPlanner_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_CallPlanner_title));
		return true;
	}
	
	public void verify_dialer_sideMenu_Settings_link() {
		clickOn_dialer_SideMenu_Settings();
		Common.pause(3);
		assertEquals(validate_dialer_Settings_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_Settings() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_Settings));
		sideMenu_Settings.click();
	}
	
	public boolean validate_dialer_Settings_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_Settings_title));
		return true;
	}
	
	public void verify_dialer_sideMenu_Credits_link() {
		clickOn_dialer_SideMenu_Credits();
		Common.pause(3);
		assertEquals(validate_dialer_Credits_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_Credits() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_Credits));
		sideMenu_Credits.click();
	}
	
	public boolean validate_dialer_Credits_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_Credits_title));
		return true;
	}
	
	public void verify_dialer_sideMenu_LeaderBoard_link() {
		clickOn_dialer_SideMenu_LeaderBoard();
		Common.pause(3);
		assertEquals(validate_dialer_LeaderBoard_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_LeaderBoard() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_Leaderboard));
		sideMenu_Leaderboard.click();
	}
	
	public boolean validate_dialer_LeaderBoard_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_Leaderboard_title));
		return true;
	}
	
	public void verify_dialer_sideMenu_Feedback_link() {
		clickOn_dialer_SideMenu_Feedback();
		Common.pause(3);
		assertEquals(validate_dialer_Feedback_Page(), true);
	}
	
	public void clickOn_dialer_SideMenu_Feedback() {
		wait.until(ExpectedConditions.visibilityOf(sideMenu_Feedback));
		sideMenu_Feedback.click();
	}
	
	public boolean validate_dialer_Feedback_Page() {
		wait2.until(ExpectedConditions.visibilityOf(sideMenu_Feedback_title));
		return true;
	}
	
	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		System.out.println(validationMessage.getText());
		return validationMessage.getText();
	}
}
