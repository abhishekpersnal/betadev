package com.CallHippo.Dialer;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.expectThrows;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.google.inject.spi.Element;
//import com.google.errorprone.annotations.IncompatibleModifiers;

public class WebToggleConfiguration extends WebObjects {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait2;
	Actions action;

	PropertiesFile url;
	JavascriptExecutor js;
	PropertiesFile credential;
	static Common csv;

	public WebToggleConfiguration(WebDriver driver) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		url = new PropertiesFile("Data\\url Configuration.properties");
		js = (JavascriptExecutor) driver;
		csv= new Common();
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public void numberspage() throws IOException {
		driver.get(url.numbersPage());
	}

	public void userspage() throws IOException {
		driver.get(url.usersPage());
	}

	public void teamspage() throws IOException {
		driver.get(url.teamPage());
	}

	public String getusername(String email) {
		String Username = wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//a[@class='teamMemberNameSpan'][contains(text(),'" + email
						+ "')]/parent::td/preceding-sibling::td/../td[1]"))))
				.getText();
		System.out.println(Username);
		return Username;
	}

//	public void navigatetoNumberSettingPage() throws Exception {
//		// driver.get(url.numbersPage());
//		wait.until(ExpectedConditions.visibilityOf(settingIcon));
//		settingIcon.click();
//	}

	public void navigateToNumberSettingpage(String number) {

		WebElement numberSetting = driver.findElement(By.xpath("//a[contains(text(),'" + number + "')]"));
		wait.until(ExpectedConditions.visibilityOf(numberSetting));
		numberSetting.click();
		int i = 5000;
		boolean dpname;
		if(driver.findElements(By.xpath("//h3[contains(.,'Department Details')]")).size()==0) {
			dpname = false;
		}else {
			dpname = true; 
		}
		
		while (dpname == false) {
			int j = i + 5000;
			try {
				Thread.sleep(j);
				numberSetting.click();
			} catch (InterruptedException e) {
			}

			if (j == 60000) {
				break;
			}
		}
		
		Common.pause(7);
		
		if(driver.findElements(By.xpath("//i[@id='g_reminder_alert_close']")).size()!=0) {
			driver.findElement(By.xpath("//i[@id='g_reminder_alert_close']")).click();
		}
		
	}

	public String getWebDepartmentName(String number) {
		String departmentName = wait
				.until(ExpectedConditions.visibilityOf(
						driver.findElement(By.xpath("//a[contains(text(),'" + number + "')]//preceding::td/a"))))
				.getText();
		System.out.println(departmentName);
		return departmentName;
	}

	public String getFlagFromWeb() {
		wait.until(ExpectedConditions.visibilityOf(webFlagName));
		return webFlagName.getAttribute("class");
	}

	public String getWebEmail(String email) {
		String webEmail = wait
				.until(ExpectedConditions.visibilityOf(driver
						.findElement(By.xpath("//a[@class='teamMemberNameSpan'][contains(text(),'" + email + "')]"))))
				.getText();
		System.out.println(webEmail);
		return webEmail;
	}

//	public void navigatetoNumber2SettingPage() throws Exception {
//		// driver.get(url.numbersPage());
//		wait.until(ExpectedConditions.visibilityOf(settingIcon2));
//		settingIcon2.click();
//	}
	public void clickOnScheduleDemo() {
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(btnScheduleDemo));
		btnScheduleDemo.click();
	}

	public boolean validateScheduleDemoBtnIsDisplayed() {
		try {
			if (btnScheduleDemo.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public String validateScheduleDemoPageTitle() {
		wait.until(ExpectedConditions.titleContains("Schedule A Demo- CallHippo"));
		return driver.getTitle();
	}

	public boolean validateLandingScheduleDemoPage() {
		try {

			System.out.println(calender.size());

			if (calender.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public void clickOnTeam(String teamName) {
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//a[@class='teamMemberNameSpan'][contains(.,'" + teamName + "')]"))));
		driver.findElement(By.xpath("//a[@class='teamMemberNameSpan'][contains(.,'" + teamName + "')]")).click();
	}

	public void clickOnSimultaneously() {
		Common.pause(3);
		simultaneously.click();
	}

	public void clickOnFixedOrder() {
		Common.pause(3);
		fixedorder.click();
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//input[@type='tel'])[1]")))).clear();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//input[@type='tel'])[1]"))))
				.sendKeys("1");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//input[@type='tel'])[2]")))).clear();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//input[@type='tel'])[2]"))))
				.sendKeys("2");
		Common.pause(1);
		if (driver.findElements(By.xpath("(//input[@type='tel'])")).size() > 2) {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//input[@type='tel'])[3]"))))
					.clear();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//input[@type='tel'])[3]"))))
					.sendKeys("3");
		}
	}

	public void clickOnRoundRobin() {
		Common.pause(3);
		roundrobin.click();
	}

	public void clickOnUpdateButton() {
		Common.pause(1);
		updateButton.click();
	}

//	public void clickOnYesButton() {
//		wait.until(ExpectedConditions.visibilityOf(yesButtong));
//		yesButtong.click();
//	}

	public String validateDepartmentNameFor2ndNumber() {
		wait.until(ExpectedConditions.visibilityOf(departmentNameFor2ndNumber));
		return departmentNameFor2ndNumber.getText();
	}

	public String ValidateAddcampSuccessmessage() {
		wait.until(ExpectedConditions.visibilityOf(AddcampaignSuccessmessage));
		return AddcampaignSuccessmessage.getText();
	}

	public void navigatetoUserSettingPage() throws Exception {
		// driver.get(url.usersPage());
		wait.until(ExpectedConditions.visibilityOf(userSettingButton));
		userSettingButton.click();
		// js.executeScript("$(\"[class='circlest btn btn-sm btn-info']\").click();");
	}

	public void clickonAWHMGreeting() {
		// js.executeScript("$(\"[for='afterHourType1']\").click();");
		wait.until(ExpectedConditions.visibilityOf(awhmGreeting));
		awhmGreeting.click();
	}

	public void clickonAWHMVoicemail() {
		// js.executeScript("$(\"[id='afterHourType2']\").click();");
		wait.until(ExpectedConditions.visibilityOf(awhmVoicemail));
		awhmVoicemail.click();
	}

	public void setCallRecordingToggle(String value) {

		wait.until(ExpectedConditions.visibilityOf(callRecording));
		if (value.contentEquals("on")) {

			if (callRecording.getAttribute("aria-checked").equals("true")) {

			} else {
				callRecording.click();
			}
		} else {
			if (callRecording.getAttribute("aria-checked").equals("true")) {
				callRecording.click();
			} else {

			}
		}

	}

	public void numberWelcomeMessage(String value) {
		wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(welcomeMessage));
		if (value.contentEquals("on")) {

			if (welcomeMessage.getAttribute("aria-checked").equals("true")) {

			} else {
				welcomeMessage.click();
			}
		} else {
			if (welcomeMessage.getAttribute("aria-checked").equals("true")) {
				welcomeMessage.click();
			} else {

			}
		}

	}

	public void showCallHippoNumberIncomingForwardedCalls(String value) {

		wait.until(ExpectedConditions.visibilityOf(numberShowFTDCall));

		if (value.contentEquals("on")) {

			if (numberShowFTDCall.getAttribute("aria-checked").equals("true")) {

			} else {
				numberShowFTDCall.click();
			}
		} else {
			if (numberShowFTDCall.getAttribute("aria-checked").equals("true")) {
				numberShowFTDCall.click();
			} else {

			}
		}

	}

	public boolean validateshowCallHippoNumberIncomingForwardedCallsDescriptionIsDisplayed() {

		try {
			if (numberShowFTDCallDescription.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}
	
	public void clickonsidemenuMusiAndMessages() {
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//a[@href='#musicMessages']")))).click();
		
	}

	
	

	public void SetNumberSettingToggles(String welcomeMessage, String voicemail, String AWHM,
			String voicemailTranscription, String avaibalityStatus, String IVR, String CallQueue,
			String callQueueMusic) {
		action.moveToElement(deptNameAndNumberTxt).build().perform();
		numberWelcomeMessage(welcomeMessage);
		action.moveToElement(deptNameAndNumberTxt).build().perform();
		numberAvailability(avaibalityStatus);
		action.moveToElement(deptNameAndNumberTxt).build().perform();
		numberVoicemail(voicemail);
		if (avaibalityStatus.equalsIgnoreCase("custom")) {
			numberAfterWorkHourMessage(AWHM);
		}
		action.moveToElement(deptNameAndNumberTxt).build().perform();
		if (voicemail.equalsIgnoreCase("on")) {
			numberVoicemaillTranscription(voicemailTranscription);
		}
		action.moveToElement(deptNameAndNumberTxt).build().perform();
		numberIVR(IVR);
		action.moveToElement(deptNameAndNumberTxt).build().perform();
		numberCallQueue(CallQueue);
		numberCallQueueMusic(callQueueMusic);
	}

	public void setUserSttingToggles(String FTD, String ACW, String voicemail, String userStatus, String userActivity) {
		action.moveToElement(userDetailsTxt).build().perform();
		userForwardToDevice(FTD);
		action.moveToElement(userDetailsTxt).build().perform();
		userAfterCallWork(ACW);
		action.moveToElement(userDetailsTxt).build().perform();
		userVoicemail(voicemail);
		action.moveToElement(userDetailsTxt).build().perform();
		userAvailability(userStatus);
		action.moveToElement(userDetailsTxt).build().perform();
		userActivityFeed(userActivity);
	}

	public void enterCallQueueDuration(String duration) throws InterruptedException {
		// js.executeScript("$(\"[test-automation='num_call_queue_edit_btn']\").click();");
		callQueueEditBtn.click();
		Thread.sleep(1000);
		callqueueDurationTextbox.clear();
		callqueueDurationTextbox.sendKeys(duration);
		// js.executeScript("$(\"[test-automation='num_call_queue_update_btn']\").click();");
		callQueueUpdateBtn.click();
	}

	public void numberAvailability(String numberStatus) {
		wait.until(ExpectedConditions.visibilityOf(numberAlwaysOpened));
		if (numberStatus.contentEquals("open")) {
			numberAlwaysOpened.click();

		} else if (numberStatus.contentEquals("custom")) {
			numberCustom.click();

		} else {
			numberAlwaysClosed.click();

		}

	}

	public void Stickyagent(String value, String valuetype) {

		wait.until(ExpectedConditions.visibilityOf(Stickysidemenu));
		js.executeScript("arguments[0].click();", Stickysidemenu);
		Common.pause(5);
		wait.until(ExpectedConditions.visibilityOf(Stickyagent));
		if (value.contentEquals("on")) {

			if (Stickyagent.getAttribute("aria-checked").equals("true")) {
				Common.pause(5);
				StickytypeLooselyorStrictly(valuetype);
			} else {
				Stickyagent.click();
				Common.pause(5);
				StickytypeLooselyorStrictly(valuetype);
			}
		} else {
			if (Stickyagent.getAttribute("aria-checked").equals("true")) {
				Stickyagent.click();
			} else {

			}
		}

	}

	public void StickytypeLooselyorStrictly(String type) {
		Common.pause(3);
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'" + type + " bind')]")))).click();
		Common.pause(2);
	}

	public String ValidateStrictlybinddescription() {
		wait.until(ExpectedConditions.visibilityOf(StrictlybindDescription));
		return StrictlybindDescription.getText();
	}

	public String ValidateLooselybinddescription() {
		wait.until(ExpectedConditions.visibilityOf(LooselybindDescription));
		return LooselybindDescription.getText();
	}

	public boolean ValidateRadiobuttonforstrictlyandloosely(String type) {
		try {
			wait.until(ExpectedConditions.visibilityOf(
					driver.findElement(By.xpath("//span[contains(.,'" + type + " bind')]/child::span/child::span)]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String ValidateStickyagentdescription() {
		wait.until(ExpectedConditions.visibilityOf(stickyagentDescription));
		return stickyagentDescription.getText();
	}

	public void userAvailability(String userStatus) {

		wait.until(ExpectedConditions.visibilityOf(userAlwaysOpened));

		if (userStatus.contentEquals("open")) {
			userAlwaysOpened.click();
		} else if (userStatus.contentEquals("custom")) {
			userCustom.click();
		} else {
			userAlwaysClosed.click();
		}

	}

	public void userTeamAllocation(String value) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(numberUserAllocation));
		wait.until(ExpectedConditions.elementToBeClickable(numberTeamAllocation));
		if (value.contentEquals("users")) {
			if (numberUserAllocation.getAttribute("class").contains("avlbdivbgorange")) {
				System.out.println("not click on user");
			} else {
				// js.executeScript("$(\"[test-automation='num_user_allocation_tab']\").click();");
				action.moveToElement(numberUserAllocation).build().perform();
				numberUserAllocation.click();
				System.out.println("click on user");
			}

		} else {
			if (numberTeamAllocation.getAttribute("class").contains("avlbdivbgorange")) {
				System.out.println("not click on team");
			} else {
				// js.executeScript("$(\"[test-automation='num_team_allocation_tab']\").click();");
				action.moveToElement(numberTeamAllocation).build().perform();
				numberTeamAllocation.click();
				System.out.println("click on team");
			}
		}

	}

	public void numberVoicemail(String value) {

		wait.until(ExpectedConditions.visibilityOf(numberVoicemail));

		if (value.contentEquals("on")) {

			if (numberVoicemail.getAttribute("aria-checked").equals("true")) {

			} else {
				numberVoicemail.click();
			}
		} else {
			if (numberVoicemail.getAttribute("aria-checked").equals("true")) {
				numberVoicemail.click();
			} else {

			}
		}

	}

	public void numberAfterWorkHourMessage(String value) {

		wait.until(ExpectedConditions.visibilityOf(numberAfterWorkHours));

		if (value.contentEquals("on")) {

			if (numberAfterWorkHours.getAttribute("aria-checked").equals("true")) {

			} else {
				numberAfterWorkHours.click();
			}
		} else {
			if (numberAfterWorkHours.getAttribute("aria-checked").equals("true")) {
				numberAfterWorkHours.click();
			} else {

			}
		}

	}

	public void numberVoicemaillTranscription(String value) {

		wait.until(ExpectedConditions.visibilityOf(numberVoicemailTranscription));

		if (value.contentEquals("on")) {

			if (numberVoicemailTranscription.getAttribute("aria-checked").equals("true")) {

			} else {
				numberVoicemailTranscription.click();
			}
		} else {
			if (numberVoicemailTranscription.getAttribute("aria-checked").equals("true")) {
				numberVoicemailTranscription.click();
			} else {

			}
		}

	}

//	public void numberUserAllocation(String value) {
//
//		if (value.contentEquals("on")) {
//
//			if (numberTeamUserAllocation.isSelected()) {
//
//			} else {
//				js.executeScript("$(\"[test-automation='num_team_user_allocation_switch']\").click();");
//			}
//		} else {
//			if (numberTeamUserAllocation.isSelected()) {
//				js.executeScript("$(\"[test-automation='num_team_user_allocation_switch']\").click();");
//			} else {
//
//			}
//		}
//
//	}

	public void numberIVR(String value) {
		wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(numberIVR));

		if (value.contentEquals("on")) {

			if (numberIVR.getAttribute("aria-checked").equals("true")) {

			} else {
				numberIVR.click();
			}
		} else {
			if (numberIVR.getAttribute("aria-checked").equals("true")) {
				numberIVR.click();
			} else {

			}
		}

	}

	public void numberCallQueue(String value) {

		wait.until(ExpectedConditions.visibilityOf(numberCallqueue));

		if (value.contentEquals("on")) {

			if (numberCallqueue.getAttribute("aria-checked").equals("true")) {

			} else {
				numberCallqueue.click();
			}
		} else {
			if (numberCallqueue.getAttribute("aria-checked").equals("true")) {
				numberCallqueue.click();
			} else {

			}
		}

	}

	public void numberCallQueueMusic(String value) {

		if (numberCallqueue.getAttribute("aria-checked").equals("true")) {

			if (value.contentEquals("on")) {

				if (numberCalQueueMusic.getAttribute("aria-checked").equals("true")) {

				} else {
					numberCalQueueMusic.click();
				}
			} else {
				if (numberCalQueueMusic.getAttribute("aria-checked").equals("true")) {
					numberCalQueueMusic.click();
				} else {

				}
			}
		}

	}
	public void customRingingTime(String value) {
		
		wait.until(ExpectedConditions.visibilityOf(numberDetailsidemenu));
		js.executeScript("arguments[0].click();", numberDetailsidemenu);
		Common.pause(5);
		wait.until(ExpectedConditions.visibilityOf(customRingingTime));
		System.out.println("customRingingTime is visible");
		
		if (value.contentEquals("on")) {

			if (customRingingTime.getAttribute("aria-checked").equals("true")) {

			} else {
				customRingingTime.click();
			}
		} else {
			if (customRingingTime.getAttribute("aria-checked").equals("true")) {
				customRingingTime.click();
			} else {

			}
		}
		
	}
	public void enterDuration_customRingingTime(String duration) {
		wait.until(ExpectedConditions.visibilityOf(customRingingTime_duration_save_Edit)).click();
		wait.until(ExpectedConditions.visibilityOf(customRingingTime_duration)).click();

		wait.until(ExpectedConditions.visibilityOf(customRingingTime_duration)).sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(customRingingTime_duration)).sendKeys(duration);
		wait.until(ExpectedConditions.visibilityOf(customRingingTime_duration_save_Edit)).click();
	}
	public String validationErrorMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationErrorMessage));
		return validationErrorMessage.getText();
	}
	
	public String validatecustomRingingTimeUploadedSuccessfully() {
		return customRingingTime_durationValue.getText();
	}

	public String getUserExtensionNumber() {
		wait.until(ExpectedConditions.visibilityOf(ExtensionNumber));
		return ExtensionNumber.getText();
	}

	public String GetusernameonNumberpage() {
		wait.until(ExpectedConditions.visibilityOf(UsernameOnnumbersetting1));
		return UsernameOnnumbersetting1.getText();
	}

	public void deallocatenUserFromNumber() {
		UsernameOnnumbersetting1.click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//p[contains(.,'Are you sure you want to deallocate this user ?')]"))));
		DeallocateUser.click();
		UsernameOnnumbersetting2.click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//p[contains(.,'Are you sure you want to deallocate this user ?')]"))));
		DeallocateUser.click();
	}
	
	public void allocatenUserFromNumber() {
		UsernameOnnumbersetting1.click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//p[contains(.,'Are you sure you want to allocate this user ?')]"))));
		DeallocateUser.click();
		UsernameOnnumbersetting2.click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//p[contains(.,'Are you sure you want to allocate this user ?')]"))));
		DeallocateUser.click();
	}

	public void userForwardToDevice(String value) {
		wait.until(ExpectedConditions.visibilityOf(userForwardToDevice));
		if (value.contentEquals("on")) {

			if (userForwardToDevice.getAttribute("aria-checked").equals("true")) {

			} else {
				userForwardToDevice.click();
			}
		} else {
			if (userForwardToDevice.getAttribute("aria-checked").equals("true")) {
				userForwardToDevice.click();
			} else {

			}
		}

	}

	public void enterFTDnumber(String number) {
		wait.until(ExpectedConditions.visibilityOf(FTDEditButton)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox)).sendKeys(number);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox)).clear();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox)).sendKeys(number);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDSaveButton)).click();
		Common.pause(3);

	}

	public void enterFTDnumber2(String number) {
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(addButtonOfFTD)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox2)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox2)).sendKeys(number);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox2)).clear();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDTextBox2)).sendKeys(number);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(FTDSaveButton2)).click();
		Common.pause(3);

	}

	public void userVoicemail(String value) {

		if (value.contentEquals("on")) {

			if (uservoicemail.getAttribute("aria-checked").equals("true")) {

			} else {
				uservoicemail.click();
			}
		} else {
			if (uservoicemail.getAttribute("aria-checked").equals("true")) {
				uservoicemail.click();
			} else {

			}
		}

	}

	public void userActivityFeed(String value) {

//		if (value.contentEquals("on")) {
//
//			if (userActivityFeed.isSelected()) {
//
//			} else {
//				js.executeScript("$(\"[test-automation='user_activity_feed_switch']\").click();");
//			}
//		} else {
//			if (userActivityFeed.isSelected()) {
//				js.executeScript("$(\"[test-automation='user_activity_feed_switch']\").click();");
//			} else {
//
//			}
//		}

	}

	public void userAfterCallWork(String value) {

		if (value.contentEquals("on")) {

			if (userAfterCallwork.getAttribute("aria-checked").equals("true")) {

			} else {
				userAfterCallwork.click();
			}
		} else {
			if (userAfterCallwork.getAttribute("aria-checked").equals("true")) {
				userAfterCallwork.click();
			} else {

			}
		}

	}

	public boolean validateWebAfterCallWork() {
		try {
			if (userAfterCallwork.getAttribute("aria-checked").equals("true")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateWebAppLoggedinPage() {
		try {
			if (email.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void navigateToUserSettingPage(String userEmail) {
		String userlink = "//a[@class='teamMemberNameSpan'][contains(text(),'" + userEmail + "')]";

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(userlink))));
		driver.findElement(By.xpath(userlink)).click();
		
		Common.pause(7);
		
		if(driver.findElements(By.xpath("//i[@id='g_reminder_alert_close']")).size()!=0) {
			driver.findElement(By.xpath("//i[@id='g_reminder_alert_close']")).click();
		}
	}

	public String getmin(String Country, String Min) {
		WebElement countrywisemin = driver.findElement(By.xpath("(//p[contains(.,'" + Country
				+ "')]/parent::div//following-sibling::div/child::div)/div[contains(.,'" + Min + "')]"));
		wait.until(ExpectedConditions.visibilityOf(countrywisemin));
		String avlMin = countrywisemin.getText();
		System.out.println(avlMin);
		String newMin = avlMin.substring(avlMin.indexOf(":") + 2, avlMin.indexOf("Min") - 1);
		System.out.println("newmint: " + newMin);
		return newMin;
	}

	public String getUserId(String Url) {

		System.out.println(Url.substring(Url.indexOf("user/") + 5));
		return Url.substring(Url.indexOf("user/") + 5);
	}

	public String getNumberId(String Url) {

		System.out.println(Url.substring(Url.indexOf("numbers/") + 8));
		return Url.substring(Url.indexOf("numbers/") + 8);
	}

	public String getUserNumberExtension() {
		wait.until(ExpectedConditions.visibilityOf(userExtensionTxt));
		return userExtensionTxt.getText();
	}

	public void enterAWHM_message(String Msg) {
		wait.until(ExpectedConditions.visibilityOf(this.AWHM_Msg_save_Edit)).click();
		wait.until(ExpectedConditions.visibilityOf(AWHM_Msg)).click();

		wait.until(ExpectedConditions.visibilityOf(this.AWHM_Msg)).sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(this.AWHM_Msg)).sendKeys(Msg);
		wait.until(ExpectedConditions.visibilityOf(this.AWHM_Msg_save_Edit)).click();
	}

	public void closeLowcreditNotification() {
		try {
			wait2.until(ExpectedConditions.visibilityOf(closelowbalancepopup));
			closelowbalancepopup.click();
		} catch (Exception e) {
		}
	}

	public String changeExtNumber() {
		wait.until(ExpectedConditions.visibilityOf(EditExtensionTxt));
		EditExtensionTxt.click();
		EnterNewExtensionTxt.click();
		EnterNewExtensionTxt.clear();
		EnterNewExtensionTxt.click();
		EnterNewExtensionTxt.sendKeys("105");
		SaveExtensionTxt.click();

		return userExtensionTxt.getText();
	}

	public void enterEmail(String email) {
		wait.until(ExpectedConditions.visibilityOf(this.email));
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void enterPassword(String password) {
		wait.until(ExpectedConditions.visibilityOf(this.password));
		this.password.clear();
		this.password.sendKeys(password);
	}

	public void clickOnLogin() {
		wait.until(ExpectedConditions.visibilityOf(this.login));
		login.click();
		wait.until(ExpectedConditions.visibilityOf(userDropdown));
	}

	public String getTitle() {
		wait.until(ExpectedConditions.titleContains("Login | Callhippo.com"));
		String signupTitle = driver.getTitle();
		return signupTitle;
	}

	public void clickOnLogOut() {
		wait.until(ExpectedConditions.visibilityOf(this.userDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(logout)).click();
	}

	public void clickOnChangePassword() {
		wait.until(ExpectedConditions.visibilityOf(this.userDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(changePassword)).click();
	}

	public void verifyEmail() {
		wait.until(ExpectedConditions.visibilityOf(this.email)).click();

	}

	public void deleteExtraFTDNumber() {

		boolean value = false;

		try {
			driver.findElement(By.xpath(
					"//h3[contains(.,'Forward-to-device')]/parent::div//i[@class='material-icons'][contains(.,'delete_forever')]"));
			value = true;
		} catch (Exception e) {
			value = false;
		}

		if (value == true) {
			int size = driver.findElements(By.xpath(
					"//h3[contains(.,'Forward-to-device')]/parent::div//i[@class='material-icons'][contains(.,'delete_forever')]"))
					.size();

			for (int i = 2; i <= size; i++) {

				driver.findElement(By.xpath(
						"(//h3[contains(.,'Forward-to-device')]/parent::div//i[@class='material-icons'][contains(.,'delete_forever')])[2]"))
						.click();
				Common.pause(5);
			}
		}

	}
// --------------------------- Web SMS activityfeed ------------------------
	public void clickOnSMSTabActivityFeed() {
		wait.until(ExpectedConditions.visibilityOf(smsTabactivityfeed)).click();
	}
	public void clickOnSMSLimit() {
		wait.until(ExpectedConditions.visibilityOf(smsLimit)).click();
	}
	public String getTextSMSLimit() {
		WebElement smsLimitContent1=driver.findElement(By.xpath("//section[@id='smsSipSetting']//div[@class='panelnumbername_content']/div"));
		System.out.println("SMS limit:"+smsLimitContent1.getText());
		return wait.until(ExpectedConditions.visibilityOf(smsLimitContent1)).getText();
//		wait.until(ExpectedConditions.visibilityOf(smsLimitContent1));
//		return "1000";
	}
	
	public String validateSMSType() {
		WebElement webSMSType = driver
				.findElement(By.xpath("(//img[contains(@class,'ingSmsIcon')])[1]"));
		wait.until(ExpectedConditions.visibilityOf(webSMSType));
		String type = webSMSType.getAttribute("class");
		return Character.toUpperCase(type.substring(0, type.indexOf("I")).charAt(0))
				+ type.substring(1, type.indexOf("S"));
	}
	public String validateSMSFrom() {
		WebElement webSMSFrom = driver
				.findElement(By.xpath("(//span[@test-automation='activity_feed_sms_from'])[1]"));
		wait.until(ExpectedConditions.visibilityOf(webSMSFrom));
		return  webSMSFrom.getText();
		
	}
	public String getTextForSMSTo() {
		WebElement  getTextSMSTo= driver
				.findElement(By.xpath("(//a[contains(@href,'/numbers/')])[1]"));
		wait.until(ExpectedConditions.visibilityOf(getTextSMSTo));
		return  getTextSMSTo.getText();
		
	}
	
	public String validateSMSTo() {
		WebElement webSMSTo= driver
				.findElement(By.xpath("(//span[@test-automation='activity_feed_sms_to'])[1]"));
		wait.until(ExpectedConditions.visibilityOf(webSMSTo));
		return  webSMSTo.getText();
		
	}
	public String validateSMSStatus() {
		WebElement webSMSStatus = driver
				.findElement(By.xpath("(//span[@test-automation='activity_feed_sms_sms_type'])[1]"));
		wait.until(ExpectedConditions.visibilityOf(webSMSStatus));
		return  webSMSStatus.getText();
		
	}
	public String validateSMSCost() {
		WebElement webSMSCost = driver
				.findElement(By.xpath("(//span[@test-automation='activity_feed_sms_sms_cost'])[1]"));
		wait.until(ExpectedConditions.visibilityOf(webSMSCost));
		System.out.println("WebSMSCost:"+webSMSCost.getText());
		return  webSMSCost.getText();
		
	}

// --------------------------- Web activityfeed ------------------------

	public void clickOnActivitFeed() {
		wait.until(ExpectedConditions.visibilityOf(activityfeed)).click();
	}

	public void clickOnSelectAllOption() {
		wait.until(ExpectedConditions.visibilityOf(selectAllCheckbox)).click();
	}

	public void clickOnCheckBoxOption(int noOfRecord) {
		for (int i = 1; i <= noOfRecord; i++) {
			int k = i + 1;
			
			
			WebElement record = driver
					.findElement(By.xpath("(//tr[not(contains(@data-row-key,'feedDate'))]/td/input)[" + i + "]"));
			wait.until(ExpectedConditions.visibilityOf(record));
			record.click();
		}
	}

	public String getSelectedRecords() {
		wait.until(ExpectedConditions.visibilityOf(selectedRecords));
		System.out.println(selectedRecords.getText());
		return selectedRecords.getText();
		// return Integer.parseInt(selectedRecords.getText());
	}

	public void clickOnFileDownload() {
		wait.until(ExpectedConditions.visibilityOf(fileDownload)).click();
	}

	public boolean validateOutgoingCallTypeIsDisplayed() {

		try {
			// wait.until(ExpectedConditions.visibilityOf(outgoingIcon));
			if (outgoingIcon.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public boolean validateOutgoingCallTypeIsDisplayed2nd() {

		try {
			// wait.until(ExpectedConditions.visibilityOf(outgoingIcon));
			if (outgoingIcon2ndEntry.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public boolean validateOutgoingCallTypeIsDisplayed3rd() {

		try {
			// wait.until(ExpectedConditions.visibilityOf(outgoingIcon));
			if (outgoingIcon3rdEntry.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public boolean validateIncomingCallTypeIsDisplayed() {
		try {
			// wait.until(ExpectedConditions.visibilityOf(incomingIcon));
			if (incomingIcon.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateIncomingCallTypeIsDisplayedFor2ndEntry() {
		try {
			// wait.until(ExpectedConditions.visibilityOf(incomingIcon));
			if (incomingIconFor2ndEntry.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateIncomingCallTypeIsDisplayedFor3rdEntry() {
		try {
			if (incomingIconFor3rdEntry.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public String validateDepartmentName() {
		return wait.until(ExpectedConditions.visibilityOf(departmentName)).getText();
	}

	public String validateDepartmentNameFor2ndEntry() {
		return wait.until(ExpectedConditions.visibilityOf(departmentNameFor2ndEntry)).getText();
	}

	public String validateStatusCallNotSetup() {
		return wait.until(ExpectedConditions.visibilityOf(statusCallNotSetup)).getText();
	}

	public void clickStatusItagBtn() {
		wait.until(ExpectedConditions.visibilityOf(statusItagBtn)).click();
		action.moveToElement(statusItagBtn).perform();
		Common.pause(1);

	}

	public String validateStatusItag() {
		return wait.until(ExpectedConditions.visibilityOf(statusItag)).getText();
	}

	public String validateCallerName() {
		return wait.until(ExpectedConditions.visibilityOf(callerName)).getText();
	}

	public boolean validatepowerDialericoninactivityfeed() {
		Common.pause(3);
		if (powerDialerIcon.isDisplayed()) {

			return true;
		} else {
			return false;
		}
	}

	public boolean validatepowerDialericoninactivityfeed2ndentry() {
		Common.pause(3);
	
			try {
				if (driver.findElements(By.xpath(
			"//tr[3]//td[4][contains(.,'offline_bolt')]]"))
						.size()>0) {
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				return true;
			}
		
	}

	public boolean validatepowerDialericoninactivityfeed3rdentry() {
		Common.pause(3);
		if (powerDialerIcon3rdentry.isDisplayed()) {

			return true;
		} else {
			return false;
		}
	}

	public String validateuserinpowerDiler(String userorteam, String name) {
		Actions a = new Actions(driver);
		a.moveToElement(driver
				.findElement(By.xpath("//div[@class='teamMemberSpan pwtmspan'][contains(.,'" + userorteam + "')]")))
				.build().perform();
		// li[contains(.,'Subuser1')]
		driver.findElement(By.xpath("//li[contains(.,'" + name + "')]"));
		return userorteam;
	}

	public String validateCallerNameFor2ndEntry() {
		return wait.until(ExpectedConditions.visibilityOf(callerNameFor2ndEntry)).getText();
	}

	public String validateCallerNameFor3rdEntry() {
		return wait.until(ExpectedConditions.visibilityOf(callerNameFor2ndEntry)).getText();
	}

	public String validateClientNumber() {
		return wait.until(ExpectedConditions.visibilityOf(clientNumber)).getText();
	}

	public String validateClientxxxentry(String xxxx) {

		WebElement xxxentry = driver.findElement(
				By.xpath("(//span[@test-automation='activity_feed_client'][contains(.,'" + xxxx + "')])[1]"));
		wait.until(ExpectedConditions.visibilityOf(xxxentry));
		return xxxx;
	}

	public String validateClientNumberFor2ndentry() {
		return wait.until(ExpectedConditions.visibilityOf(clientNumberFor2ndentry)).getText();
	}

	public String validateClientNumberFor3rdentry() {
		return wait.until(ExpectedConditions.visibilityOf(clientNumberFor3rdentry)).getText();
	}

	public String validateCallStatus() {
		return wait.until(ExpectedConditions.visibilityOf(status)).getText();
	}
	public int getNoOfRecord() {
		
		return noOfRecord.size();
	}

	public boolean validateNoDataTextInActivityFeed() {

		if (noDataInActivityFeed.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	
	public void validateCallRecording_NoteInCsv(int record,String csvColValue) throws IOException {
		ArrayList<String> csvCallRecord = new ArrayList<String>();
		csvCallRecord = csv.readDataFromActivityFeedCsv(csv.verifyDownloadFileName(), csvColValue);
		System.out.println(csvCallRecord);
		wait.until(ExpectedConditions.visibilityOfAllElements(iTagButton));
		
		for(int i=0;i<record;i++) {
			wait.until(ExpectedConditions.visibilityOf(iTagButton.get(i))).click();
			System.out.println("csv::"+csvCallRecord.get(i));
			Common.pause(2);
			System.out.println(validateCallRecordingUrl());
			System.out.println(!csvCallRecord.get(i).isEmpty());
			
			assertEquals(validateCallRecordingUrl(),!csvCallRecord.get(i).isEmpty());
			clickOnCloseiButton();
		}
				
		
	}	
	public void validateNoteInCsv(int record,String csvColValue) throws IOException {
		ArrayList<String> csvNoteRecord = new ArrayList<String>();
		csvNoteRecord = csv.readDataFromActivityFeedCsv(csv.verifyDownloadFileName(), csvColValue);
		System.out.println(csvNoteRecord);
		//wait.until(ExpectedConditions.visibilityOfAllElements(iTagButton));
		
		for(int i=0;i<record;i++) {
			wait.until(ExpectedConditions.visibilityOf(iTagButton.get(i))).click();
			System.out.println("csv::"+csvNoteRecord.get(i));
			Common.pause(2);
		   clickOnCallNotesBtn(2);
			Common.pause(2);
			try {
			System.out.println(validateNoCallNote());
			System.out.println(csvNoteRecord.get(i).isEmpty());
			System.out.println("forNocallnote:"+getCallNotesText(1));
			assertEquals(validateNoCallNote(),csvNoteRecord.get(i).isEmpty());
			assertTrue(getCallNotesText(1).contains(csvNoteRecord.get(i)));
			
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println(validateCallNote());
				System.out.println("forcallnote:"+getCallNotesText(1)+"n");
				System.out.println(!csvNoteRecord.get(i).isEmpty());
						
				assertEquals(validateNoCallNote(),!csvNoteRecord.get(i).isEmpty());
				assertTrue((getCallNotesText(1)+"n").contains(csvNoteRecord.get(i)));
			}			
			clickOnCloseiButton();
		}
		
		
	}	
	public void validateTagsInCsv(int record,String csvColValue) throws IOException {
		ArrayList<String> csvTagsRecord = new ArrayList<String>();
		csvTagsRecord = csv.readDataFromActivityFeedCsv(csv.verifyDownloadFileName(), csvColValue);
		System.out.println(csvTagsRecord);
		wait.until(ExpectedConditions.visibilityOfAllElements(iTagButton));
		
		for(int i=0;i<record;i++) {
			wait.until(ExpectedConditions.visibilityOf(iTagButton.get(i))).click();
			System.out.println("csv::"+csvTagsRecord.get(i));
			Common.pause(2);
			clickOnCallNotesBtn(2);
			Common.pause(2);
			try {
				System.out.println(validateNoCallTags());
				System.out.println(csvTagsRecord.get(i).isEmpty());
				System.out.println("forNocalltags:"+getTagName());
				assertEquals(validateNoCallNote(),csvTagsRecord.get(i).isEmpty());
				assertTrue(getTagName().contains(csvTagsRecord.get(i)));
				
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println(validateCallNote());
				System.out.println("forcalltags:"+getTagName()+"n");
				System.out.println(!csvTagsRecord.get(i).isEmpty());
				
				assertEquals(validateNoCallNote(),!csvTagsRecord.get(i).isEmpty());
				assertTrue((getTagName()+"n").contains(csvTagsRecord.get(i)));
			}			
			clickOnCloseiButton();
		}
		
		
	}	
	public void validateTagsOnWeb(int record,ArrayList<String> filterTagsRecord) throws IOException {
		ArrayList<String> webTagsRecords = new ArrayList<String>();
		for(int i=0;i<record;i++) {
			wait.until(ExpectedConditions.visibilityOf(iTagButton.get(i))).click();
			
			Common.pause(2);
			clickOnCallNotesBtn(2);
			Common.pause(2);
			System.out.println(callTags.size());
			System.out.println(noCallTags.size());
			if(callTags.size()>0) {
				for(int j=1;j<=callTags.size();j++){
					WebElement callTags=driver.findElement(By.xpath(("(//span[contains(@class,'calltaglabel')])["+j+"]")));
					System.out.println("forcalltags:"+callTags.getText());
					webTagsRecords.add(callTags.getText());
				}
				System.out.print(webTagsRecords);
				if (filterTagsRecord.size()>=webTagsRecords.size()) {
					for (int k = 0; k < webTagsRecords.size(); k++) {
						assertTrue(filterTagsRecord.contains(webTagsRecords.get(k)));
					}
				} else {
					for (int k = 0; k < filterTagsRecord.size(); k++) {
						System.out.println(".........");
						assertTrue(webTagsRecords.contains(filterTagsRecord.get(k)));
					}
				}
					
			}else if(noCallTags.size()>0) {
				System.out.println("forNocalltags:"+getTagName());
				assertTrue("There are no tags for this call.".contains(getTagName()));	
			}	
			clickOnCloseiButton();
		}	
	}
	public void validateRecordOnWeb(int record, ArrayList<String> filterStatusRecord, String titleValue)
			throws IOException {

		ArrayList<String> webStatusrecord = new ArrayList<String>();
		webStatusrecord = getActivityFeedWebRecords(record, titleValue);
		System.out.println(webStatusrecord);
		if (!webStatusrecord.isEmpty()) {
			for (int i = 0; i < webStatusrecord.size(); i++) {
				assertTrue(filterStatusRecord.contains(webStatusrecord.get(i)),"validate record of: "+titleValue +"  -----MissMatch Record of:"+webStatusrecord.get(i));
				// assertTrue(webStatusrecord.contains(filterStatusRecord.get(i)));
			}
		}else {
			assertFalse(webStatusrecord.isEmpty());
		}
	}

	public void validatePowerDialerRecordOnWeb(int record) throws IOException {

		for (int i = 1; i <= record; i++) {
			List<WebElement> powerIcon = driver.findElements(By.xpath(
					"(//img[contains(@class,'ingIcon')]/parent::td/following-sibling::td//span[@class='material-icons pwdActIcSpn'])["
							+ i + " ]"));
			if (powerIcon.size() > 0) {
			assertTrue(true,"power dialer entry displyed while only power dialer selection");
			} else {
			assertTrue(false,"power dialer entry displyed while only power dialer selection");
			}
		}

	}
	public void validateWithoutPowerDialerRecordOnWeb(int record) throws IOException {

		for (int i = 1; i <= record; i++) {
			List<WebElement> powerIcon = driver.findElements(By.xpath(
					"(//img[contains(@class,'ingIcon')]/parent::td/following-sibling::td//span[@class='material-icons pwdActIcSpn'])["
							+ i + " ]"));
			if (powerIcon.size() > 0) {
			assertTrue(false,"power dialer entry displyed while only without power dialer selection");
			} else {
			assertTrue(true,"power dialer entry displyed while only without power dialer selection");
			}
		}

	}
	public void validateClientRecordOnWeb(int record, ArrayList<String> filterStatusRecord, String titleValue)
			throws IOException {

		ArrayList<String> webStatusrecord = new ArrayList<String>();
		webStatusrecord = getActivityFeedWebRecords(record, titleValue);
		System.out.println(webStatusrecord);
		for (int i = 1; i <= webStatusrecord.size(); i++) {

			WebElement webCallType = driver.findElement(By.xpath("(//img[contains(@class,'ingIcon')])[" + i + "]"));
			WebElement department = driver.findElement(By.xpath(
					"(//img[contains(@class,'ingIcon')]/parent::td/following-sibling::td//span[@test-automation='activity_feed_department'])["
							+ i + "]"));
			// (//img[contains(@class,'ingIcon')]/parent::td/following-sibling::td//span[@test-automation='activity_feed_client'])
			wait.until(ExpectedConditions.visibilityOf(webCallType));
			System.out.println(webCallType.getAttribute("class"));
			if (webCallType.getAttribute("class").contains("outgoing")) {
				Actions actions = new Actions(driver);
				actions.moveToElement(department);

			} else if (webCallType.getAttribute("class").contains("outgoing")) {

			}

			Common.pause(1);

			assertTrue(filterStatusRecord.contains(webStatusrecord.get(i)));
			// assertTrue(webStatusrecord.contains(filterStatusRecord.get(i)));
		}

	}

	public String getCallTypeFromWeb() {
		WebElement webCallType = driver.findElement(By.xpath("(//img[contains(@class,'ingIcon')])[1]"));
		wait.until(ExpectedConditions.visibilityOf(webCallType));
		System.out.println(webCallType.getAttribute("class"));
		return webCallType.getAttribute("class");
	}

	public String getNumberFromDepartmentForWeb(int i) {

		Actions actions = new Actions(driver);
		actions.moveToElement(departmentName).perform();

		System.out.println("Done Mouse hover on 'departmentName'");

		WebElement numberElement = driver.findElement(By.xpath("(//div[contains(@class,'ant-tooltip-inner')])["+i+"]"));
		action.moveToElement(numberElement).perform();

		System.out.println("fromnumber" + numberElement.getText());
		return numberElement.getText();
	}

	public String getNumberFromclientForWeb() {
		
		Actions actions = new Actions(driver);
		actions.moveToElement(clientNumber).perform();

		System.out.println("Done Mouse hover on 'clientNumber'");
		Common.pause(3);

 WebElement numberElement1 = driver
			.findElement(By.xpath("(//div[contains(@class,'ant-tooltip-inner')])[1]"));
 JavascriptExecutor executor = (JavascriptExecutor)driver;
 executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("(//div[contains(@class,'ant-tooltip-inner')])[1]")));
	
		System.out.println("clientnumber" + clientNumber.getText());
		
		return clientNumber.getText();
		
	}

	public ArrayList getActivityFeedWebRecords(int record, String titleValue) {
		ArrayList<String> statusrecord = new ArrayList<String>();
		for (int i = 1; i <= record; i++) {
			
			if (titleValue.equalsIgnoreCase("Calltype")) {
				
				WebElement webCallType = driver
						.findElement(By.xpath("(//img[contains(@class,'ingIcon')])[" + i + "]"));
				wait.until(ExpectedConditions.visibilityOf(webCallType));
				String type = webCallType.getAttribute("class");
				statusrecord.add(Character.toUpperCase(type.substring(0, type.indexOf("I")).charAt(0))
						+ type.substring(1, type.indexOf("I")));
				
			}

			for (int j = 0; j < title.size(); j++) {
                    String title1=title.get(j).getText();
				if (title1.contains("Status")) {
					if (titleValue.equalsIgnoreCase("Status")) {
						WebElement webCallstatus = driver.findElement(
								By.xpath("(//span[@test-automation='activity_feed_call_status'])[" + i + "]"));
						wait.until(ExpectedConditions.visibilityOf(webCallstatus));
						statusrecord.add(webCallstatus.getText());
					}
				} else if (title1.contains("Department")) {
					if (titleValue.equalsIgnoreCase("Department")) {
						WebElement webDepartment = driver.findElement(
								By.xpath("(//span[@test-automation='activity_feed_department'])[" + i + "]"));
						wait.until(ExpectedConditions.visibilityOf(webDepartment));
						statusrecord.add(webDepartment.getText());
					}
				} else if (title1.contains("Client")) {
					if (titleValue.equalsIgnoreCase("Client")) {
						WebElement webClient = driver
								.findElement(By.xpath("(//span[@test-automation='activity_feed_client'])[" + i + "]"));
						wait.until(ExpectedConditions.visibilityOf(webClient));
						statusrecord.add( webClient.getText());
					}
				}  else if (title1.contains("User")) {
				if (titleValue.equalsIgnoreCase("User")) {
					WebElement webUser = driver
							.findElement(By.xpath("(//span[@test-automation='activity_feed_caller'])[" + i + "]"));
					wait.until(ExpectedConditions.visibilityOf(webUser));
					statusrecord.add( webUser.getText());
				}
			} else if (title1.contains("Time")) {
					if (titleValue.equalsIgnoreCase("Time")) {
						WebElement webTime = driver
								.findElement(By.xpath("(//span[@test-automation='activity_feed_time'])[" + i + "]"));
						wait.until(ExpectedConditions.visibilityOf(webTime));
						statusrecord.add(webTime.getText());
					}
				} else if (title.get(j).getText().contains("Duration")) {
					if (titleValue.equalsIgnoreCase("Duration")) {
						WebElement webDuration = driver.findElement(
								By.xpath("(//span[@test-automation='activity_feed_duration'])[" + i + "]"));
						wait.until(ExpectedConditions.visibilityOf(webDuration));
						statusrecord.add(webDuration.getText());
					}
				} else if (title.get(j).getText().contains("Cost")) {
					if (titleValue.equalsIgnoreCase("Cost")) {
						WebElement webCost = driver.findElement(
								By.xpath("(//span[@test-automation='activity_feed_call_cost'])[" + i + "]"));
						wait.until(ExpectedConditions.visibilityOf(webCost));
						
						Character c1 =webCost.getText().charAt(webCost.getText().length() - 1);
						  if(c1.equals('0') ) {
							  String ans=webCost.getText().substring(0,webCost.getText().length()-1);
							  statusrecord.add(ans);
						  }else {
							  statusrecord.add(webCost.getText());
						  }						
						
						
						
						
						
				}
			}

		}

	
		}
		return statusrecord;
	}

	public void validateWebActivityFeedInCsv(ArrayList<String> csvRecords, ArrayList<String> webRecords) {
		for (int i = 0; i < webRecords.size(); i++) {
			if (csvRecords.get(i).equals("0")) {
				assertEquals(webRecords.get(i), "-");
			} else {
				assertEquals(webRecords.get(i), csvRecords.get(i));
				System.out.println("Equals..: " + webRecords.get(i));
			}
		}

	}

	public String validateCallstatusFor2ndEntry() {
		return wait.until(ExpectedConditions.visibilityOf(statusFor2ndEntry)).getText();
	}

	public String validateCallstatusFor3rdEntry() {
		return wait.until(ExpectedConditions.visibilityOf(statusFor3rdEntry)).getText();
	}

	public String validateDuration() {
		return wait.until(ExpectedConditions.visibilityOf(duration)).getText();
	}

	public String validateCallCost() {
		return wait.until(ExpectedConditions.visibilityOf(callCost)).getText();
	}

	public String validateCallCostFor2ndEntry() {
		return wait.until(ExpectedConditions.visibilityOf(callCostFor2ndEntry)).getText();
	}

	public String validateCallCostFor3rdEntry() {
		return wait.until(ExpectedConditions.visibilityOf(callCostFor2ndEntry)).getText();
	}

	public void clickOniButton() {
		wait.until(ExpectedConditions.visibilityOf(iButton)).click();
	}

	public void clickOniButton3rdEntry() {
		wait.until(ExpectedConditions.visibilityOf(iButton3rdentry)).click();
	}

	public void clickOnCloseiButton() {
		wait.until(ExpectedConditions.visibilityOf(closeiButton)).click();
	}

	public void clickOniButtonFor2ndEntry() {
		wait.until(ExpectedConditions.visibilityOf(iButtonFor2ndEntry)).click();

	}

	public void clickOncrossButtonForClosingiTagPopup() {
		wait.until(ExpectedConditions.visibilityOf(crossButtonForClosingiTagPopup)).click();

	}

	public boolean validateRecordingUrl() {
		try {

			if (recordingUrl.isDisplayed()) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return true;
		}
	}
	public boolean validateCallRecordingUrl() {
		try {
			
			if (callRecordUrl.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}
	}
	public boolean validateNoCallNote() {
		try {
			
			if (noCallNotes.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean validateCallNote() {
		try {
			
			if (callNotes.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}
	}

	public boolean validateNoCallTags() {
		try {
			
			if (noCallTags.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}
	}

	public boolean validateCallTags() {
		try {
			
			if (callTags.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return true;
		}
	}
	public String getTagName() {
		if(callTags.size()>0) {
			return callTags.get(0).getText();
		}else {
			return noCallTags.get(0).getText();
		}
		
	}
	public boolean validaterecordingUrlFor2ndEntry() {
		try {

			if (recordingUrlFor2ndEntry.isDisplayed()) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return true;
		}
	}

	public boolean validaterecordingUrlFor3rdEntry() {
		try {

			if (recordingUrlFor3rdEntry.isDisplayed()) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return true;
		}
	}

	// ----------------------------- end web activity -----------------------------

	// ---------------------------Start Gmail Login functions ---------------------

	public boolean validateNomalMissedCallEmailNotification(String fromNumber, String toNumber,
			String toNumberDepartmentName) {
		openMail();
		return normalMissedCallFormate(fromNumber, toNumber, toNumberDepartmentName);

	}

	public void gloginGmail() {

		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gemail.sendKeys(credential.gmailId());
		} catch (IOException e1) {
		}
		gemailNext.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		try {
			gpassword.sendKeys(credential.gmailPassword());
		} catch (IOException e1) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
		Common.pause(6);

	}

	public boolean normalMissedCallFormate(String fromNumber, String toNumber, String toNumberDepartmentName) {

//		int attempt=0;
//		int MAX_ATTEMPTS = 5;
//		
//		while( attempt < MAX_ATTEMPTS ) {
//		    try {
//		
//		    		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//img[contains(@class,'ajT')]"))));
//		    		int size = driver.findElements(By.xpath("//img[contains(@class,'ajT')]")).size();
//		    		System.out.println(size);
//
//					if (size > 0) {
//						driver.findElement(By.xpath("//img[contains(@class,'ajT')]")).click();
//					} else {
//			
//						driver.findElements(By.xpath("//img[contains(@class,'ajT')]")).get(size - 1).click();
//			
//					}
//					//break;
//		    }catch ( Exception e )
//		    {
//		    	System.out.println(attempt);
//		      //if ( error.message.includes( "StaleElementReference" ) )
//		    	attempt++;
//		    }
//		}

		try {
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Looks like you just missed a call from "
							+ fromNumber + " on " + toNumber + " (" + toNumberDepartmentName + ").')]"))));
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
					"//p[contains(.,'Pro Tip: Please consider setting Smart Call Forwarding so that you don’t miss important calls.')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void openMail() {

		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		Common.pause(2);
		if (isGmailIDdisplayed() == true) {
			gloginGmail();
		}
		int attempts = 0;
		while (attempts < 2) {
			try {
				try {
					wait.until(ExpectedConditions.visibilityOf(driver
							.findElements(By.xpath("//span[contains(text(),'CallHippo | Missed call')]")).get(1)));
					driver.findElements(
							By.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo | Missed call')]"))
							.get(1).click();
				} catch (Exception e) {
					driver.findElement(
							By.xpath("(//div[contains(@class,'bqe')][contains(@role,'button')][@title='Archive'])/div"))
							.click();
					wait.until(ExpectedConditions.stalenessOf(driver
							.findElements(By.xpath(
									"//span[contains(@class,'bqe')][contains(text(),'CallHippo | Missed call')]"))
							.get(1)));
					wait.until(ExpectedConditions.visibilityOf(driver
							.findElements(By.xpath(
									"//span[contains(@class,'bqe')][contains(text(),'CallHippo | Missed call')]"))
							.get(1)));
					driver.findElements(
							By.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo | Missed call')]"))
							.get(1).click();
				}

				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		// wait.until(ExpectedConditions.jsReturnsValue("return document.readyState ==
		// 'complete'"));

	}

	private boolean isGmailIDdisplayed() {

		try {
			return gemail.isDisplayed();
		} catch (Exception e) {
			return false;
		}

	}

	public void deleteMissedCallMail() {
		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		Common.pause(2);

		if (isGmailIDdisplayed() == true) {
			gloginGmail();
		}
		int attempts = 0;
		int size = 0;
		while (attempts < 2) {
			try {
				try {
					size = driver.findElements(By.xpath(
							"//span[contains(.,'CallHippo | Missed call')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
							.size();
				} catch (Exception e) {
					try {
						wait.until(ExpectedConditions.stalenessOf(driver.findElements(By.xpath(
								"//span[contains(.,'CallHippo | Missed call')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
								.get(1)));
						driver.findElements(By.xpath(
								"//span[contains(.,'CallHippo | Missed call')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
								.size();
					} catch (Exception e1) {
						size = 0;
					}
				}

				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}

		if (size > 0) {
			for (int i = 0; i < size; i++) {
				driver.findElements(By.xpath(
						"//span[contains(.,'CallHippo | Missed call')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
						.get(i).click();
			}
			action.contextClick(driver.findElement(By.xpath(
					"//span[contains(.,'CallHippo | Missed call')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']")))
					.build().perform();
			action.contextClick().build().perform();
			for (int j = 1; j <= 7; j++) {
				action.sendKeys(Keys.ARROW_DOWN).build().perform();
			}

			action.sendKeys(Keys.ENTER).build().perform();

			Common.pause(3);

		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	public void deleteInvitedUserMail() {
		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		Common.pause(2);

		if (isGmailIDdisplayed() == true) {
			gloginGmail();
		}
		int attempts = 0;
		int size = 0;
		while (attempts < 2) {
			try {
				try {
					size = driver.findElements(By.xpath(
							"//span[contains(.,'Test Automation invited you to join CallHippo')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
							.size();
					System.out.println("---size--" + size);
				} catch (Exception e) {
					try {
						wait.until(ExpectedConditions.stalenessOf(driver.findElements(By.xpath(
								"//span[contains(.,'Test Automation invited you to join CallHippo')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
								.get(1)));
						driver.findElements(By.xpath(
								"//span[contains(.,'Test Automation invited you to join CallHippo')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
								.size();
					} catch (Exception e1) {
						size = 0;
					}
				}

				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}

		if (size > 0) {
			int i;
			for (i = 0; i < size; i++) {
				driver.findElements(By.xpath(
						"//span[contains(.,'Test Automation invited you to join CallHippo')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
						.get(i).click();
			}
			action.contextClick(driver.findElement(By.xpath(
					"//span[contains(.,'Test Automation invited you to join CallHippo')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']")))
					.build().perform();
			action.contextClick().build().perform();
			for (int j = 1; j < 7; j++) {
				action.sendKeys(Keys.ARROW_DOWN).build().perform();
			}

			action.sendKeys(Keys.ENTER).build().perform();
			Common.pause(3);

		}

		driver.findElement(By.xpath("//img[@src[contains(.,'gsuite')]]")).click();

		Common.pause(1);

		driver.findElement(By.xpath("//a[contains(.,'Sign out')]")).click();
		Common.pause(2);

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	public void clickLeftMenuSetting() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_setting));
		leftMenu_setting.click();
		wait.until(ExpectedConditions.elementToBeClickable(sdap_Txt));
		Common.pause(1);
	}

	public void clickLeftMenuPlanAndBilling() {
		try {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_Plan_Billing));
		leftMenu_Plan_Billing.click();
		}catch (Exception e) {
		js.executeScript("arguments[0].scrollIntoView();", leftMenu_Plan_Billing);
		leftMenu_Plan_Billing.click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(yourPlan_Txt));
		Common.pause(1);
	}

	public void scrollToPriceLimitTxt() {
		wait.until(ExpectedConditions.visibilityOf(priceLimitTxt));
		js.executeScript("arguments[0].scrollIntoView();", priceLimitTxt);
	}

	public void scrollToCallBlockingTxt() {
		wait.until(ExpectedConditions.visibilityOf(callBlocking_Txt));
		js.executeScript("arguments[0].scrollIntoView();", callBlocking_Txt);
	}
	public void clickbtnAddnumberBlackList() {
		wait.until(ExpectedConditions.visibilityOf(btnAddnumberBlackList)).click();
	}
	public void clickbtnSaveAddnumberBlackList() {
		wait.until(ExpectedConditions.visibilityOf(btnSaveAddnumberBlackList)).click();

	}
	public void enterNumberToAddInBlackList(String number) {
		wait.until(ExpectedConditions.visibilityOf(txtnumberForBlackList)).sendKeys(number);
	}
	public void deleteAllNumbersFromBlackList() {
		Common.pause(2);
		List<WebElement> elements = driver.findElements(
				By.xpath("//section[@id='callBlocking']//button[@class[contains(.,'billingTrashEmailBtn')]]"));
		System.out.println("---elements.size()--" + elements.size());

		if (elements.size() > 0) {
			// for (WebElement element : elements) {
			for (int i = 0; i <= elements.size(); i++) {
				System.out.println("==== i ===" + i);
				List<WebElement> elements1 = driver.findElements(
						By.xpath("//section[@id='callBlocking']//button[@class[contains(.,'billingTrashEmailBtn')]]"));
				if (elements1.size() > 0) {
					elements1.get(0).click();
				}
				Common.pause(2);

			}
			Common.pause(1);
		}
	}

	public void removePriceLimit() {
		Common.pause(2);
		System.out.println("---outgoing_pricelimit_edit_Btn.size()--" + outgoing_pricelimit_edit_Btn.size());

		if (outgoing_pricelimit_edit_Btn.size() > 0) {
			for (int i = 0; i <= outgoing_pricelimit_edit_Btn.size(); i++) {
				outgoing_pricelimit_edit_Btn.get(0).click();
				String priceLimitVal = outgoing_pricelimit_placeHolderTxt.getAttribute("value");
				for (int j = 0; j < priceLimitVal.length(); j++) {
					outgoing_pricelimit_placeHolderTxt.sendKeys(Keys.BACK_SPACE);
				}
				Common.pause(1);
				outgoing_pricelimit_save_enable_Btn.click();
				Common.pause(1);
				assertEquals(validateSuccessMessage(), "Outgoing call price removed successfully.");
				Common.pause(2);
			}
			Common.pause(1);
		}
	}

	public String validateSuccessMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateSuccessMsg));
		return validateSuccessMsg.getText();
	}
//----------------------------- web credit deduct after calling -----------------------------

	public String getCreditValueFromLeftPanel() {
		String creditTxt = left_panel_total_credit.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(".") + 3);
		System.out.println("++++++" + creditVal);
		return creditVal;
	}

	public String availableCreditValue() {
		String creditTxt = setting_available_credit.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(".") + 3);
		System.out.println("++++++" + creditVal);
		return creditVal;
	}

	public void clickLeftMenuDashboard() {
		wait.until(ExpectedConditions.elementToBeClickable(left_panel_dashboard));
		left_panel_dashboard.click();
		wait.until(ExpectedConditions.elementToBeClickable(dashboard_callSummary_txt));
		Common.pause(1);
	}

	public String validateCreditDeduction(String settingcreditValueBefore, String deductCredit) {
		float floatSettingPageCreditVal;
		float floatDeductCredit;

		if (settingcreditValueBefore.contains(",")) {
			settingcreditValueBefore = settingcreditValueBefore.replace(",", "");
			floatSettingPageCreditVal = Float.parseFloat(settingcreditValueBefore);
			floatDeductCredit = Float.parseFloat(deductCredit);
		} else {
			floatSettingPageCreditVal = Float.parseFloat(settingcreditValueBefore);
			floatDeductCredit = Float.parseFloat(deductCredit);
		}

		floatSettingPageCreditVal = floatSettingPageCreditVal - floatDeductCredit;
		String SettingPageCreditValStr = String.format("%.2f", floatSettingPageCreditVal);

		if (SettingPageCreditValStr.length() >= 7) {

			String str1 = SettingPageCreditValStr.substring(SettingPageCreditValStr.indexOf(".") - 3,
					SettingPageCreditValStr.indexOf(".") + 3);
			String str2 = SettingPageCreditValStr.substring(0, SettingPageCreditValStr.indexOf(".") - 3);
			SettingPageCreditValStr = str2 + "," + str1;
		}
		return SettingPageCreditValStr;
	}

	public String validateMinDeduction(String MinValueBefore, String deductMin) {
		int UsMinbefore;
		int MinTalk;

		UsMinbefore = Integer.parseInt(MinValueBefore);
		System.out.println(UsMinbefore);
		MinTalk = Integer.parseInt(deductMin);

		UsMinbefore = UsMinbefore - MinTalk;
		String SettingPageCreditValStr = String.format("%d", UsMinbefore);

		return SettingPageCreditValStr;
	}

	public String validateItagNotes(int i) {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))));
		driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))
				.click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("(//p[@test-automation='activity_feed_drawer_notes'])[" + 1 + "]"))));
		return driver.findElement(By.xpath("(//p[@test-automation='activity_feed_drawer_notes'])[" + 1 + "]"))
				.getText();
	}
	
	public void validateTagInWeb(String tagname) {
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))));
		driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))
				.click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//span[@class[contains(.,'calltagwrapper')]]/span[contains(.,'"+tagname+"')][1]"))));
	
		
	}
	
	public String validateItagNotesIsEmpty(int i) {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))));
		driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))
				.click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(itag_EmptyNotes_text));
		return itag_EmptyNotes_text.getText();
	}

	public void voidTheCallofAllCallPlannerEntries() {
		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[@type='button'][contains(.,'Avoid
		// the call')]"))));

		int countOfCallPlannerEntries = driver.findElements(By.xpath("//button[@type='button'][contains(.,'Delete')]"))
				.size();

		if (countOfCallPlannerEntries > 0) {
			for (int i = 1; i <= countOfCallPlannerEntries; i++) {
				driver.findElement(By.xpath("(//button[@type='button'][contains(.,'Delete')])[1]")).click();
				wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath("//button[@type='button'][contains(.,'Yes')]"))))
						.click();
				Common.pause(5);

			}
		}

	}

	public void clickOnMakeTheCallFromCallPlanner() {
		wait.until(ExpectedConditions.visibilityOf(makeTheCallBtn)).click();
	}

	public boolean clickOnOpenDialerButton() {
		wait.until(ExpectedConditions.visibilityOf(open_dialer_button));
		try {
			open_dialer_button.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void switchToWindowBasedOnTitle(String title) {
//		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		int i = 0;
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			driver.switchTo().window(childWindow);
			if (driver.getTitle().contains(title)) {
				break;
			}

			Common.pause(1);
			i = i + 1;

			if (i == 20) {
				break;
			}
		}
		System.out.println(driver.getTitle());
	}

	public void clickOnCompletedOnFromCallPlanner() {
		wait.until(ExpectedConditions.visibilityOf(completedOnFromCallPlanner)).click();
	}

	public void closeWindow() {
		action.sendKeys(Keys.CONTROL).sendKeys("w").build().perform();
	}

	public void clickOnCallPlannerSideMenu() {
		wait.until(ExpectedConditions.visibilityOf(callPlannerSideMenu)).click();
	}

	public String getNameFromCallPlanner() {
		return wait.until(ExpectedConditions.visibilityOf(nameOnCallPlanner)).getText();
	}

	public String getContactNameFromCallPlanner() {
		return wait.until(ExpectedConditions.visibilityOf(contactNameOnCallPlanner)).getText();
	}

	public String getContactNumberFromCallPlanner() {
		return wait.until(ExpectedConditions.visibilityOf(contactNumberOnCallPlanner)).getText();
	}

	public String getTimeofRemainderFromCallPlanner() {
		return wait.until(ExpectedConditions.visibilityOf(TimeOfRemInWeb)).getText();
	}

	public boolean validateNoDataOnCallPlanner() {
		try {
			driver.navigate().refresh();
			wait2.until(ExpectedConditions.visibilityOf(noDataOnCallPlanner));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String getNameFromCallPlannerCompletedOn() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[1]/td[1]/span"))));
		int size = driver.findElements(By.xpath("//tbody/tr/td[1]/span")).size();

		return driver.findElement(By.xpath("//tbody/tr[" + size + "]/td[1]/span")).getText();
	}

	public String getContactNameFromCallPlannerCompletedOn() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[1]/td[1]/span"))));
		int size = driver.findElements(By.xpath("//tbody/tr/td[2]/span")).size();

		return driver.findElement(By.xpath("//tbody/tr[" + size + "]/td[2]/span")).getText();
	}

	public String getContactNumberFromCallPlannerCompletedOn() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[1]/td[1]/span"))));
		int size = driver.findElements(By.xpath("//tbody/tr/td[3]/span")).size();

		return driver.findElement(By.xpath("//tbody/tr[" + size + "]/td[3]/span")).getText();
	}

//	public void clickOnYesOfCallPlanner() {
//		wait.until(ExpectedConditions.visibilityOf(yesButtonOfCallplanner)).click();
//	}

	public String validationMessage() {
		return wait.until(ExpectedConditions.visibilityOf(validationMessage)).getText();

	}

	public void clickOnIntegrationLinkFromSideMenu() {
		wait.until(ExpectedConditions.visibilityOf(sideMenuIntegrationLink)).click();
		wait.until(ExpectedConditions.visibilityOf(sectionTitleCRM));
	}

	public void clickOnPipeDriveIntegrationButton() {
		wait.until(ExpectedConditions.visibilityOf(PipeDriveIntegrationButton)).click();
	}

	public void clickOnConnectNowButton() {
		wait.until(ExpectedConditions.visibilityOf(connectNowButton)).click();
	}

	public void enterEmailInPipeDrive(String email) {
		wait.until(ExpectedConditions.visibilityOf(pipeDriveEmail)).sendKeys(email);
	}

	public void enterPasswordInPipeDrive(String password) {
		wait.until(ExpectedConditions.visibilityOf(pipeDrivePassword)).sendKeys(password);
	}

	public void clickOnLoginButtonInPipeDrive() {
		wait.until(ExpectedConditions.visibilityOf(pipeDriveLoginButton)).click();
	}

	public void clickOnContinueToTheAppInPipeDrive() {
		wait.until(ExpectedConditions.visibilityOf(continueTotheApp)).click();
	}

	public void clickOnSkipButton() {
		wait.until(ExpectedConditions.visibilityOf(skipButton)).click();
	}

	public boolean validateCheckIcon() {

		try {
			wait.until(ExpectedConditions.visibilityOf(checkicon));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String getRestAPIFromSettingPage() {
		wait.until(ExpectedConditions.visibilityOf(restAPISubMenu)).click();
		Common.pause(3);
		System.out.println("web rest api : " + wait.until(ExpectedConditions.visibilityOf(restAPI)).getText());
		return wait.until(ExpectedConditions.visibilityOf(restAPI)).getText();

	}

	public void clickOnIntegrationButton(String integrationName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//div/span[text()='" + integrationName + "']/..//button[@type='button'][contains(.,'+ INTEGRATE')]"))))
				.click();
	}

	public boolean validateCheckIconOnIntegration(String integrationName) {
		try {
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//span[@class='integrationsName'][contains(.,'"
							+ integrationName + "')]/..//i[contains(@class,'icon checkDeleteIcon')]"))))
					.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void selectZohoDomainRadioBtn() {
		wait.until(ExpectedConditions.visibilityOf(selectZohoDomainRadioBtn)).click();
	}

	public void enterEmailInZoho(String email) {
		wait.until(ExpectedConditions.visibilityOf(insertZohoEmail)).sendKeys(email);
	}

	public void clickOnZohoEmailNextBtn() {
		wait.until(ExpectedConditions.visibilityOf(zohoEmailNextBtn)).click();
	}

	public void enterPasswordInZoho(String password) {
		wait.until(ExpectedConditions.visibilityOf(insertZohoPassword)).sendKeys(password);
	}

	public void clickOnZohoSignInBtn() {
		wait.until(ExpectedConditions.visibilityOf(zohoSignInBtn)).click();
	}

	public void clickOnZohoCRMAcceptBtn() {
		wait.until(ExpectedConditions.visibilityOf(zohoCRMAcceptBtn)).click();
	}

	public void clickOnFreshDeskIntegrationButton() {
		wait.until(ExpectedConditions.visibilityOf(helpdeskLink)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(freshDeskIntegrationButton)).click();
	}

	public void enterFreshDeskAPIKey(String restAPIkey) {
		wait.until(ExpectedConditions.visibilityOf(freshdeskApiKey)).sendKeys(restAPIkey);
	}

	public void enterFreshDeskDomainName(String domainName) {
		wait.until(ExpectedConditions.visibilityOf(freshdeskDomainName)).sendKeys(domainName);
	}

	public void clickOnFreshDeskSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(freshdeskSaveButton)).click();
	}

	public boolean validateFreshDeskCheckIcon() {

		try {
			wait.until(ExpectedConditions.visibilityOf(checkicon));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void selectSpecificUserOrTeam(String userOrTeamName, String YesOrNo) {

		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'chflex flexcolomch')]"))));

		String attributeName = driver
				.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
						+ userOrTeamName + "')]/parent::div/parent::div/parent::div"))
				.getAttribute("class");

		if (YesOrNo.contains("Yes")) {
			if (attributeName.contains("allocateduserboxSelected")) {

			} else {
				driver.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
						+ userOrTeamName + "')]/parent::div")).click();
				wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
				Common.pause(2);
			}
		} else {
			if (attributeName.contains("allocateduserboxSelected")) {
				driver.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
						+ userOrTeamName + "')]/parent::div")).click();
				wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
				Common.pause(2);
			} else {

			}
		}

	}

	public void DeselectSpecificUserOrTeam(String userOrTeamName, String YesOrNo) {

		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'chflex flexcolomch')]"))));

		String attributeName = driver
				.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
						+ userOrTeamName + "')]/parent::div/parent::div/parent::div"))
				.getAttribute("class");

		if (YesOrNo.contains("Yes")) {
			if (attributeName.contains("allocateduserboxSelected")) {
				driver.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
						+ userOrTeamName + "')]/parent::div")).click();
				wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
				Common.pause(2);
			} else {

			}
		} else {
			if (attributeName.contains("allocateduserboxSelected")) {

			} else {
				driver.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
						+ userOrTeamName + "')]/parent::div")).click();
				wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
				Common.pause(2);
			}
		}
	}

	public void clickonyesbutton() {
		wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
	}

	public void clickonYesbuttonDeleteTeam() {
		wait.until(ExpectedConditions.visibilityOf(yesButtonOfCallplanner)).click();
	}

	public void makeDefaultNumber(String number) {

		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'"+number+"')]"))));

		List<WebElement> ele = driver.findElements(By.xpath("//span[contains(.,'" + number
				+ "')]/parent::div/parent::div/parent::div/parent::div//span[contains(@class,'makeDefaultUser')]"));

		if (ele.size() > 0) {
			driver.findElement(By.xpath("//span[contains(.,'" + number
					+ "')]/parent::div/parent::div/parent::div/parent::div//span[contains(@class,'makeDefaultUser')]"))
					.click();
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//button[@type='button'][contains(.,'Yes')]")))).click();
			Common.pause(3);
		}

	}

	public String getRestAPIToken() {
		Common.pause(9);
		action.moveToElement(driver.findElement(By.xpath("//h3[contains(text(),'REST API')]")));
		Common.pause(3);
		if (driver.findElements(By.xpath("//p[contains(.,'Generate API token')]")).size() > 0) {
			driver.findElement(By.xpath("//button[@type='button'][contains(.,'add_circle')]")).click();
			Common.pause(3);
		}
		return driver.findElement(By.xpath("//h3[contains(text(),'REST API')]/parent::div//div/p")).getText();

	}

	public void selectUserOrTeamInWeblivecallFilter(String UserOrTeam) {
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//li[@role='option'][contains(.,'" + UserOrTeam + "')]"))))
				.click();

	}

	public boolean ValidateUserOrTeamInWeblivecallFilter(String UserOrTeam) {
		try {
			wait.until(ExpectedConditions.visibilityOf(
					driver.findElement(By.xpath("//li[@role='option'][contains(.,'" + UserOrTeam + "')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void Allocateuserinteam(String useremail) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.xpath("//div[contains(@class,'list-sort-demo-list')][contains(.,'" + useremail + "')]")))).click();
	}

	public void DeleteSpecificTeam(String Teamname) {
		wait.until(ExpectedConditions.visibilityOf(driver
				.findElement(By.xpath("//tr[contains(@class,'ant-table-row')]/child::td//a[contains(.,'" + Teamname
						+ "')]/parent::td/following-sibling::td/following-sibling::td/child::div/child::a//following-sibling::span"))))
				.click();
	}

	public String AddnewTeam(String teamname) {
		wait.until(ExpectedConditions.visibilityOf(createTeamBtn)).click();
		wait.until(ExpectedConditions.visibilityOf(Enterteamname)).sendKeys(teamname);
		return teamname;
	}

	public void CreateTeam() {
		wait.until(ExpectedConditions.visibilityOf(createTeam)).click();
	}

	public String enterUserOrTeaminWebFiltersearch(String userorteam) {
		ClearWeblivecallsearchfilter();
		System.out.println("test1");
		Common.pause(2);
//	    wait.until(ExpectedConditions.visibilityOf(web_filter_Search)).click();
//	    System.out.println("test2");
//	    Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(Enter_WebFilter_search)).sendKeys(userorteam);
		System.out.println("test3");
		Common.pause(2);
		return userorteam;
	}

	public String searchUserOrTeaminWebFiltersearch(String userorteam) {
		wait.until(ExpectedConditions.visibilityOf(web_filter_Search)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(Enter_WebFilter_search)).sendKeys(userorteam);
		Common.pause(2);
		return userorteam;
	}

	public void ClearWeblivecallsearchfilter1() {
		try {
			wait2.until(ExpectedConditions.visibilityOf(Clear_WebFilter_search)).click();
			Common.pause(2);
		} catch (Exception e) {
		}
//    	wait.until(ExpectedConditions.visibilityOf(Enter_WebFilter_search)).clear();
//    	Common.pause(2);
	}

	public void ClearWeblivecallsearchfilter() {
		try {
			if (Clear_WebFilter_search.isDisplayed()) {
				Clear_WebFilter_search.click();
				wait.until(ExpectedConditions.visibilityOf(web_filter_Search)).click();
				Common.pause(2);
			}
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(web_filter_Search)).click();
			System.out.println("test2");
			Common.pause(2);
		}
	}

	public boolean ValidateFilterIconinWeblivecalls() {
		try {
			wait.until(ExpectedConditions.visibilityOf(web_livecall_filter_button)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean ValidateFilterpopupWeblivecalls() {
		try {
			wait.until(ExpectedConditions.visibilityOf(web_UserTeam_filter_Popup));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean ValidateFilterSearchbarinWeblivecalls() {
		try {
			wait.until(ExpectedConditions.visibilityOf(web_filter_Search));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String getFreeIncomingMinutesFromDashboard() {

		//String text = wait.until(ExpectedConditions.visibilityOf(dashboardFreeIncomingMinutes)).getText();

		//return text.substring(0, text.indexOf("m") - 1);
		return "0";
	}

	public void clickOnCallNotesBtn(int i) {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))));
		driver.findElement(By.xpath(
				"(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[" + 1 + "]"))
				.click();
	}

	public String getCallNotesText(int i) {
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("(//p[@test-automation='activity_feed_drawer_notes'])[" + 1 + "]"))));
		return driver.findElement(By.xpath("(//p[@test-automation='activity_feed_drawer_notes'])[" + 1 + "]"))
				.getText();
	}

	public void clickOnUserProfile() {
		wait.until(ExpectedConditions.visibilityOf(userProfile));
		userProfile.click();
	}

	public void clickOnExportContacts() {
		wait.until(ExpectedConditions.visibilityOf(exportContacts));
		exportContacts.click();
	}

	public void clickOnYesButton() {
		wait.until(ExpectedConditions.visibilityOf(btnYes));
		btnYes.click();
	}

	public boolean validateUserAvailabilityAlwaysClosed() {
		wait.until(ExpectedConditions.visibilityOf(userAlwaysClosed));
		if (userAlwaysClosed.getAttribute("class").contains("avlbdivbgorange")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateUserAvailabilityAlwaysOpened() {
		wait.until(ExpectedConditions.visibilityOf(userAlwaysOpened));
		if (userAlwaysOpened.getAttribute("class").contains("avlbdivbgorange")) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validateFilterIconIsDisplayedInActivityFeedPage() {
		
		return affilter.isDisplayed();
//		if(affilter.isDisplayed()) {
//			return true;
//		}else {
//			return false;
//		}
		
	}
	
	public void clickOnFilterIconOnActivityFeedPage() {
		wait.until(ExpectedConditions.visibilityOf(affilter)).click();
		Common.pause(2);
	}
	
	public boolean validatActivityFeedFilterPopupIsDispayed() {
		return afpopupFilterTitle.isDisplayed();
	}
	
	
	
	public void clickOnFilterTitleInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupFilterTitle)).click();
	}
	public void clickOnSelectCallerInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupSelectCaller)).click();
	}
	
	public void clickOnSelectCallerDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupSelectCallerDropdown)).click();
	}
	public boolean validateSelectCallerDropdownInAFFPopup() {
		return afpopupSelectCallerDropdown.isDisplayed();
	}
	
	public void clickOnCallerDropdownEveryOneInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCallerDropdownEveryOne)).click();
	}
	public boolean validateCallerDropdownEveryOneInAFFPopup() {
		return afpopupCallerDropdownEveryOne.isDisplayed();
	}
	public void clickOnCallerDropdownUsersInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCallerDropdownUsers)).click();
	}
	public boolean validateCallerDropdownUsersInAFFPopup() {
		return afpopupCallerDropdownUsers.isDisplayed();
	}
	public void clickOnCallerDropdownTeamsInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCallerDropdownTeams)).click();
	}
	public boolean validateCallerDropdownTeamsInAFFPopup() {
		if( afpopupSelectTeamsDropdown.size()>0) {
			return true;
		}else {
			return false;
		}
		
	}
	public void clickOnTeamDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupSelectTeamsDropdown.get(0))).click();
	}
	public String getTeamNameInAFFPopup(String team) {
		WebElement teamElement;
		if(team.equals("Check All")) {
			teamElement=driver.findElement(By.xpath("(//li[@role='option'][contains(.,'"+team+"')])[1]"));
		}else {
			 teamElement=driver.findElement(By.xpath("//li[@label='"+team+"']"));
		}
		Common.pause(2);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", teamElement);
		return teamElement.getText();
	
	}
//	public String validateFirstDateInAFFPopup() {
//		System.out.println(callog_date.get(0).getText());
//		return callog_date.get(0).getText();
//		
//	}
//		
//	public String validateLastDateInAFFPopup() {
//		System.out.println(callog_date.get(callog_date.size()-1).getText());
//		return callog_date.get(callog_date.size()-1).getText();
//		
//	}
	
	
	public boolean validateDateRangeInAFFPopup(String fromDate, String toDate) {
		System.out.println(callog_date.get(0).getText());
		System.out.println(callog_date.get(callog_date.size()-1).getText());

		 if (fromDate.compareTo(callog_date.get(0).getText()) >= 0 && toDate.compareTo(callog_date.get(callog_date.size()-1).getText()) <= 0) {
	            return true;
	     }else {
	    	 return false;
	     }
		 
	
	}
	public String getUserNameInAFFPopup(String user) {
		WebElement userElement;
		if(user.equals("Check All")) {
			userElement=driver.findElement(By.xpath("(//li[@role='option'][contains(.,'"+user+"')])[1]"));
		}else {
			userElement=driver.findElement(By.xpath("(//li[@role='option']//span[@class='filterUserName'][contains(.,'"+user+"')])[1]"));
		}
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", userElement);
		return userElement.getText();
	
	}
	public void clickOnCallStatusDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCallStatusDropdown)).click();
	}
	public boolean validateCallStatusDropdownInAFFPopup() {
		return afpopupCallStatusDropdown.isDisplayed();
		
	}
	
	public boolean validateSelectUsersDropdownInAFFPopup() {
		if( afpopupSelectUsersDropdown.size()>0) {
			return true;
		}else {
			return false;
		}
		
	}
	public void clickOnUserDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupSelectUsersDropdown.get(0))).click();
	}
	
	public void clickOnTagsDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupTagsDropdown)).click();
	}
	public boolean validateTagsDropdownInAFFPopup() {
		return afpopupTagsDropdown.isDisplayed();
		
	}

	public String getTagNameInAFFPopup(String tag, int i ) {
		WebElement tagElement;
		if(tag.equals("Check All")) {
			 tagElement=driver.findElement(By.xpath("(//li[@role='option'][contains(.,'Check All')])["+i+"]"));
		}else {
			 tagElement=driver.findElement(By.xpath("(//li[text()='"+tag+"'])["+i+"]"));
		}
		Common.pause(2);
		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", tagElement);
		System.out.println("click by js.");
		
		return tagElement.getText();
	}
	
	
	public boolean validatePowerDialerDropdownInAFFPopup() {
		return afpopupPowerDialerDropdown.isDisplayed();
		
	}
	public boolean validateCheckAllInAFFPopup() {
		return afpopupCheckAll.isDisplayed();
		
	}
	public void clickOnCheckAllInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCheckAll)).click();
		
	}
	
	public boolean validateCheckAllForTagInAFFPopup() {
		return afpopupCheckAllForTag.isDisplayed();
		
	}

	public void clickOnCheckAllForTagInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCheckAllForTag)).click();
		
	}
public void clickOnStatusInAFFPopup(String status) {
		
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+status+"')]"))));
		driver.findElement(By.xpath(("//li[@role='option'][contains(.,'"+status+"')]"))).click();
		
		}
public ArrayList<String> getStatusAllInAFFPopup(String status) {
	ArrayList<String> allStatus= new ArrayList<String>();
	if(status.equals("Check All")) {
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+status+"')]"))));
		driver.findElement(By.xpath(("//li[@role='option'][contains(.,'"+status+"')]"))).click();
		allStatus.add(afpopupCallNotSetup.getText());allStatus.add(afpopupCancelled.getText());allStatus.add(afpopupCompleted.getText());
		allStatus.add(afpopupIVRmessage.getText());allStatus.add(afpopupMissed.getText());allStatus.add(afpopupNoAnswer.getText());
		allStatus.add(afpopupRejected.getText());allStatus.add(afpopupUnavailable.getText());allStatus.add(afpopupVoicemail.getText());
		allStatus.add(afpopupWelcomemessage.getText());
	}
	System.out.println(allStatus);
	return allStatus;
}
	public String getStatusInAFFPopup(String status) {	
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+status+"')]"))));
		driver.findElement(By.xpath(("//li[@role='option'][contains(.,'"+status+"')]"))).click();
		return driver.findElement(By.xpath(("//li[@role='option'][contains(.,'"+status+"')]"))).getText();
	}
	public ArrayList<String> getCallTypeBothInAFFPopup(String calltype) {
		ArrayList<String> calltypeBoth= new ArrayList<String>();
		if(calltype.equals("Both")) {
		//	wait.until(ExpectedConditions.visibilityOf(afpopupCallTypeBoth.get(0)));
			//afpopupCallTypeBoth.get(0).click();
			calltypeBoth.add(afpopupCallTypeIncoming.get(0).getAttribute("value"));
			calltypeBoth.add(afpopupCallTypeOutgoing.get(0).getAttribute("value"));
		
		}
		System.out.println(calltypeBoth);
		return calltypeBoth;
	}
	public String getCallTypeInAFFPopup(String calltype) {	
		
		WebElement callTypeBtn=  driver.findElement(By.xpath("//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='"+calltype+"']"));
		Actions action = new Actions(driver);
		action.moveToElement(callTypeBtn).click().perform();
		
		System.out.println(callTypeBtn.getAttribute("value"));
		return callTypeBtn.getAttribute("value");
	}
	
	public boolean validateCallNotSetupInAFFPopup() {
		return afpopupCallNotSetup.isDisplayed();
		
	}
	

	public boolean validateCancelledInAFFPopup() {
		return afpopupCancelled.isDisplayed();
		
	}
	public boolean validateCompletedInAFFPopup() {
		return afpopupCompleted.isDisplayed();
		
	}
	public boolean validateIVRmessageInAFFPopup() {
		return afpopupIVRmessage.isDisplayed();
		
	}
	public boolean validateMissedInAFFPopup() {
		return afpopupMissed.isDisplayed();
		
	}
	public boolean validateNoAnswerInAFFPopup() {
		return afpopupNoAnswer.isDisplayed();
		
	}
	public boolean validateRejectedInAFFPopup() {
		return afpopupRejected.isDisplayed();
		
	}
	public boolean validateUnavailableInAFFPopup() {
		return afpopupUnavailable.isDisplayed();
		
	}
	public boolean validateVoicemailInAFFPopup() {
		return afpopupVoicemail.isDisplayed();
		
	}
	public boolean validateWelcomemessageInAFFPopup() {
		return afpopupWelcomemessage.isDisplayed();
		
	}
	public boolean validateOnlyPowerDialerInAFFPopup() {
		return afpopupOnlyPowerDialer.isDisplayed();
		
	}
	public boolean validateWithoutPowerDialerInAFFPopup() {
		return afpopupWithoutPowerDialer.isDisplayed();
		
	}
	public void clickOnPowerDialerDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupPowerDialerDropdown)).click();
	}
	public void clickOnOnlyPowerDialerDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupOnlyPowerDialer)).click();
	}
	public void clickOnWithoutPowerDialerDropdownInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupWithoutPowerDialer)).click();
	}
	public void selectCampaignforPowerDialerDropdownInAFFPopup(String campaignName ) {
		if(campaignName.equals("Check All")) {
			wait.until(ExpectedConditions.visibilityOf(afpopupSelectCampaignDropdown)).click(); 
			WebElement allcampaign= driver.findElement(By.xpath("(//li[@role='option'][contains(.,'Check All')])[4]"));
			wait.until(ExpectedConditions.visibilityOf(allcampaign)).click(); 
		}else {
		wait.until(ExpectedConditions.visibilityOf(afpopupSelectCampaignDropdown)).click(); 
		WebElement campaign= driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+campaignName+"')]"));
		wait.until(ExpectedConditions.visibilityOf(campaign)).click(); 
		}
		
	}
	public boolean validateCallTypeBothRedioButtonInAFFPopup() {
		if(afpopupCallTypeBoth.size()>0) {
			return true;
		}else {
			return false;
		}
	
	}
	public void clickOnCallTypeBothRedioButtonInAFFPopup() {
		
		driver.findElement(By.xpath("//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='Both']")).click();
	}
	public boolean validateCallTypeIncomingRedioButtonInAFFPopup() {
		if(afpopupCallTypeIncoming.size()>0) {
			return true;
		}else {
			return false;
		}
		
		
	}
	public void clickOnCallTypeIncomingRedioButtonInAFFPopup() {
		WebElement incomingBtn=  driver.findElement(By.xpath("//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='Incoming']"));
		Actions action = new Actions(driver);
		action.moveToElement(incomingBtn).click().perform();		
	}
	public boolean validateCallTypeOutgoingRedioButtonInAFFPopup() {
		if(afpopupCallTypeOutgoing.size()>0) {
			return true;
		}else {
			return false;
		}
	}
	public void clickOnCallTypeOutgoingRedioButtonInAFFPopup() {

		WebElement OutgoingBtn=  driver.findElement(By.xpath("//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='Outgoing']"));
		Actions action = new Actions(driver);
		action.moveToElement(OutgoingBtn).click().perform();		
	}
	
	public void clickOnApplyButtonInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupApplyButton)).click();
	}
	public boolean validateFromDateInAFFPopup() {
		return afpopupFromDate.isDisplayed();
		
	}
	public String getFromDateInAFFPopup() {
		return afpopupFromDate.getText();
		
	}
	
	public void enterFromDateInAFFPopup(int reduceMonth, int reduceDay) {
		wait.until(ExpectedConditions.visibilityOf(afpopupFromDate)).click();
		
		Common.pause(1);
//		afpopupFromDate.sendKeys(fromDate);
//		js.executeScript("arguments[0].value='"+fromDate+"';", afpopupFromDate);
		
		DateFormat DateFor3 = new SimpleDateFormat("MMM");
		Calendar now3 = Calendar.getInstance();
		now3.add(Calendar.MONTH, reduceMonth);
		now3.add(Calendar.DATE, reduceDay);
		String previousMonthChar = DateFor3.format(now3.getTime());
		
		System.out.println(" ------ "+previousMonthChar+" ----  ");
		
		DateFormat DateFor = new SimpleDateFormat("MM");
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH, reduceMonth);
		now.add(Calendar.DATE, reduceDay);
		String previousMonth = DateFor.format(now.getTime());
		
		System.out.println(" ------ "+previousMonth+" ----  ");
		
		DateFormat DateFor1 = new SimpleDateFormat("yyyy");
		Calendar now1 = Calendar.getInstance();
		now1.add(Calendar.MONTH, reduceMonth);
		now1.add(Calendar.DATE, reduceDay);
		String previousYear = DateFor1.format(now1.getTime());
		
		System.out.println(" ------ "+previousYear+" ----  ");
		
		DateFormat DateFor2 = new SimpleDateFormat("dd");
		Calendar now2 = Calendar.getInstance();
		now2.add(Calendar.MONTH, reduceMonth);
		now2.add(Calendar.DATE, reduceDay);
		String previousDay = DateFor2.format(now2.getTime());
		
		System.out.println(" ------ "+previousDay+" ----  ");
	
		
		int preMonthInt = Integer.valueOf(previousMonth);
		int preYearInt = Integer.valueOf(previousYear);
		int predayInt = Integer.valueOf(previousDay);
		
		
		String yeartext = driver.findElement(By.xpath("//a[contains(@title,'Choose a year')]")).getText();
		
		int yearint = Integer.valueOf(yeartext);
		
		if(yearint==preYearInt) {
			
		}else {
			driver.findElement(By.xpath("//a[contains(@title,'Choose a year')]")).click();
			Common.pause(1);
			driver.findElement(By.xpath("//a[@class='ant-calendar-year-panel-year'][contains(.,'"+preYearInt+"')]")).click();
			Common.pause(1);
		}
		
		
		String monthtext = driver.findElement(By.xpath("//a[contains(@title,'Choose a month')]")).getText();
		
		if(monthtext.contains(previousMonthChar)) {
			
		}else {
			driver.findElement(By.xpath("//a[contains(@title,'Choose a month')]")).click();
			Common.pause(1);
			driver.findElement(By.xpath("//a[@class='ant-calendar-month-panel-month'][contains(.,'"+previousMonthChar+"')]")).click();
			Common.pause(1);
			driver.findElement(By.xpath("//td[@class='ant-calendar-cell']//div[@class='ant-calendar-date'][normalize-space()='"+predayInt+"']")).click();
			Common.pause(1);
		}
		
		
		driver.findElement(By.xpath("//a[normalize-space()='Ok']")).click();
		Common.pause(1);
		
		
	}
		
	public void enterToDateInAFFPopup(int reduceMonth, int reduceDay) {
		wait.until(ExpectedConditions.visibilityOf(afpopupToDate)).click();

		Common.pause(1);
//		afpopupFromDate.sendKeys(fromDate);
//		js.executeScript("arguments[0].value='"+fromDate+"';", afpopupFromDate);
		
		DateFormat DateFor3 = new SimpleDateFormat("MMM");
		Calendar now3 = Calendar.getInstance();
		now3.add(Calendar.MONTH, reduceMonth);
		now3.add(Calendar.DATE, reduceDay);
		String previousMonthChar = DateFor3.format(now3.getTime());
		
		System.out.println(" ------ "+previousMonthChar+" ----  ");
		
		DateFormat DateFor = new SimpleDateFormat("MM");
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH, reduceMonth);
		now.add(Calendar.DATE, reduceDay);
		String previousMonth = DateFor.format(now.getTime());
		
		System.out.println(" ------ "+previousMonth+" ----  ");
		
		DateFormat DateFor1 = new SimpleDateFormat("yyyy");
		Calendar now1 = Calendar.getInstance();
		now1.add(Calendar.MONTH, reduceMonth);
		now1.add(Calendar.DATE, reduceDay);
		String previousYear = DateFor1.format(now1.getTime());
		
		System.out.println(" ------ "+previousYear+" ----  ");
		
		DateFormat DateFor2 = new SimpleDateFormat("dd");
		Calendar now2 = Calendar.getInstance();
		now2.add(Calendar.MONTH, reduceMonth);
		now2.add(Calendar.DATE, reduceDay);
		String previousDay = DateFor2.format(now2.getTime());
		
		System.out.println(" ------ "+previousDay+" ----  ");
		
		
		
		
		int preMonthInt = Integer.valueOf(previousMonth);
		int preYearInt = Integer.valueOf(previousYear);
		int predayInt = Integer.valueOf(previousDay);
		
		
		String yeartext = driver.findElement(By.xpath("//a[contains(@title,'Choose a year')]")).getText();
		
		int yearint = Integer.valueOf(yeartext);
		
		if(yearint==preYearInt) {
			
		}else {
			driver.findElement(By.xpath("//a[contains(@title,'Choose a year')]")).click();
			Common.pause(1);
			driver.findElement(By.xpath("//a[@class='ant-calendar-year-panel-year'][contains(.,'"+preYearInt+"')]")).click();
			Common.pause(1);
		}
		
		
		String monthtext = driver.findElement(By.xpath("//a[contains(@title,'Choose a month')]")).getText();
		
		if(monthtext.contains(previousMonthChar)) {
			
		}else {
			driver.findElement(By.xpath("//a[contains(@title,'Choose a month')]")).click();
			Common.pause(1);
			driver.findElement(By.xpath("//a[@class='ant-calendar-month-panel-month'][contains(.,'"+previousMonthChar+"')]")).click();
			Common.pause(1);
			driver.findElement(By.xpath("//td[@class='ant-calendar-cell']//div[@class='ant-calendar-date'][normalize-space()='"+predayInt+"']")).click();
			Common.pause(1);
		}
		
		
		driver.findElement(By.xpath("//a[normalize-space()='Ok']")).click();
		Common.pause(1);
		
	}
	
	public boolean validateFromNumberInAFFPopup() {
		return afpopupFromNumberTextbox.isDisplayed();	
	}
	public boolean validateTodDateInAFFPopup() {
		return afpopupToDate.isDisplayed();	
	}
	
	public void enterFromNumberInAFFPopup(String fromNumber) {
		wait.until(ExpectedConditions.visibilityOf(afpopupFromNumberTextbox)).clear();
		afpopupFromNumberTextbox.sendKeys(fromNumber);
	}
	public boolean validateToNumberInAFFPopup() {
		return afpopupToNumberTextbox.isDisplayed();
		
	}
	public void enterToNumberInAFFPopup(String toNumber) {
		wait.until(ExpectedConditions.visibilityOf(afpopupToNumberTextbox)).clear();
		afpopupToNumberTextbox.sendKeys(toNumber);
	}
	
	public void selectDropdownValueInAFFPopup(String dropdownValue) {
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//li[contains(text(),'"+dropdownValue+"')]")))).click();
	}
	
	public boolean validateApplyButtonInAFFPopup() {
		return afpopupApplyButton.isDisplayed();
		
	}
	public boolean validateCloseButtonInAFFPopup() {
		return afpopupCloseButton.isDisplayed();	
	}
	public void clickOnCloseButtonInAFFPopup() {
		wait.until(ExpectedConditions.visibilityOf(afpopupCloseButton)).click();
	}

	public void selectMessageType(String feature, String msgType) {
		
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'" + feature
					+ "')]/..//following-sibling::div//input[@type='radio']/../following-sibling::span[contains(text(),'"
					+ msgType + "')]")))).click();
		}else if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
					"//p[contains(.,'IVR')]/../..//..//..//following-sibling::div//input[@type='radio']/../following-sibling::span[contains(text(),'"
							+ msgType + "')]"))))
					.click();		
		}else {
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'" + feature
				+ "')]/..//following-sibling::div//input[@type='radio']/../following-sibling::span[contains(text(),'"
				+ msgType + "')]")))).click();
		}
		
	}
	
	public void uploadMessageFile(String feature, String fileName) {
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		}
		if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		}
		Common.pause(2);
		String workingDir = System.getProperty("user.dir");
		String filepath = workingDir + "\\MusicFiles\\" + fileName;
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'" + feature
				+ "')]/../..//following-sibling::div//input[@type='file']")))).sendKeys(filepath);
		System.out.println("executed by Try");
		}catch (Exception e) {
			System.out.println("executed by Catch");
			WebElement musicFile =  driver.findElement(By.xpath("//span[contains(.,'" + feature
					+ "')]/../..//following-sibling::div//input[@type='file']"));
			//wait.until(ExpectedConditions.visibilityOf(musicFile));
			js.executeScript("arguments[0].scrollIntoView();", musicFile);
			
			driver.findElement(By.xpath("//span[contains(.,'" + feature
					+ "')]/../..//following-sibling::div//input[@type='file']")).sendKeys(filepath);
		}
	}
	
	public String getNumSettingUploadedFileName(String feature) {
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		}
		if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		}
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'"+feature+"')]/../..//following-sibling::div//span[@class='fileUploadName']"))));
		return driver.findElement(By.xpath("//span[contains(.,'"+feature+"')]/../..//following-sibling::div//span[@class='fileUploadName']")).getText();
	}
	
	public boolean validateChangeMusicBtnVisible(String feature) {
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		}
		if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		}
		Common.pause(2);
		if (driver.findElements(By.xpath("//span[contains(.,'"+feature+"')]/../..//following-sibling::div//button/span[contains(.,'Change Music')]")).size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validateMusicAudioVisible(String feature) {
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		}
		if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		}
		Common.pause(2);
		if (driver.findElements(By.xpath("//span[contains(.,'"+feature+"')]/../..//following-sibling::div//audio")).size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validateFileUploadNote(String feature) {
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		}
		if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		}
		Common.pause(2);
		if (driver.findElements(By.xpath("//span[contains(.,'" + feature
				+ "')]/../..//following-sibling::div//div[@class='fileUploadNote']/p[text()='Upload WAV & MP3 file under 5 MB.']"))
				.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void validateNumSettingFileUploadedSuccessfully(String feature, String fileName) {
		if(feature.contentEquals("Welcome message")) {
			wait.until(ExpectedConditions.visibilityOf(welcomemessageTab)).click();
		}
		if(feature.contentEquals("IVR")) {
			wait.until(ExpectedConditions.visibilityOf(ivrTab)).click();
		}
		Common.pause(2);
		assertEquals(validateChangeMusicBtnVisible(feature), true);
		assertEquals(validateMusicAudioVisible(feature), true);
		assertEquals(getNumSettingUploadedFileName(feature), "File Name:"+fileName);
		assertEquals(validateFileUploadNote(feature), true);
	}
	
	public String geErrorFileUploadMsg() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='fileUploadNote']/p[@class='fileUploadError']"))));
		System.out.println("---Error Msg="+fileUploadErrorMsg.getText());
		return fileUploadErrorMsg.getText();
	}
	
	public void ScrollToDefaultCountryCodeSection() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h3[contains(.,'Default Country Code')]"))));
		js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//h3[contains(.,'Default Country Code')]")));
		Common.pause(2);
	}
	
	public void deleteCountryCode() {
		Common.pause(2);
		List<WebElement> editButtonList = driver.findElements(
				By.xpath("//button[@class='ant-btn defaultCountryCodeSaveBtn'][contains(.,'edit')]"));
		
		List<WebElement> deleteButtonList = driver.findElements(
				By.xpath("//button[@class='ant-btn billingTrashEmailBtn'][contains(.,'delete_forever')]"));

		if (editButtonList.size() > 0 && deleteButtonList.size() > 0) {
			for (int i = 0; i <= deleteButtonList.size(); i++) {
				List<WebElement> deleteButtonListNew = driver.findElements(
						By.xpath("//button[@class='ant-btn billingTrashEmailBtn'][contains(.,'delete_forever')]"));
				if (deleteButtonListNew.size() > 0) {
					deleteButtonListNew.get(0).click();
				}
				Common.pause(2);

			}
			Common.pause(1);
		}
	}
	
}
