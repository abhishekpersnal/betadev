package com.CallHippo.Dialer.authentication;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class DialerLoginTest {
	SignupPage signupPage;
	DialerLoginPage dialerloginpage;
	LoginPage loginpage;
	WebDriver driver;
	static Common signUpexcel;
	static Common loginexcel;
	static Common magiclinkexcel;
	
	PropertiesFile url;
	PropertiesFile data;
	SignupPage registrationPage;
	String gmailUser;
	
	
	public DialerLoginTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signUpexcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		loginexcel = new Common(data.getValue("environment")+"\\login.xlsx");
		magiclinkexcel = new Common(data.getValue("environment")+"\\MagicLink.xlsx");
		gmailUser = url.getValue("gmailUser");
		
	}
	
	//public String account2Number1 = data.getValue("account2Number1");

	@BeforeMethod
	public void initialization() throws Exception {
		try {
			driver = TestBase.init();
			signupPage = PageFactory.initElements(driver, SignupPage.class);
			loginpage = PageFactory.initElements(driver, LoginPage.class);
			dialerloginpage = PageFactory.initElements(driver, DialerLoginPage.class);
			registrationPage = PageFactory.initElements(driver, SignupPage.class);
			try {
				driver.get(url.dialerSignIn());
			} catch (IOException e) {
				driver.get(url.dialerSignIn());
			}
		} catch (Exception e) {
			Common.Screenshot(driver," Dialer Login Test - Fail ","DialerLoginTest - BeforeMethod - initialization");
			System.out.println("----Need to check issue - DialerLoginTest - BeforeMethod - initialization----");
		} 
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	   driver.quit();
	}
	
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_title() {
		String actualResult = dialerloginpage.getTitle();
		String expectedResult = "CallHippo Dialer";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_Fields_On_Dialer_Login_Page() throws IOException {
		String actualResult = dialerloginpage.getTitle();
		String expectedResult = "CallHippo Dialer";
		assertEquals(actualResult, expectedResult);
		assertTrue(dialerloginpage.verifyFieldsDisplayOnLoginPage());
		assertFalse(dialerloginpage.verifyFieldsNotDisplayOnLoginPage());
	}
	
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_User_Redirect_On_CallHippo_Website() {  
		dialerloginpage.getTitle();
		dialerloginpage.clickOnCallHippoLogo();
		assertEquals(dialerloginpage.verifyUserRedirectOnCallHippoWebsite(), true);
	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void login_toWebDialer_using_open_dialer() throws IOException {
		driver.get(url.signIn());
		Common.pause(3);
		loginpage.enterEmail(signUpexcel.getdata(0, 4, 2));
		loginpage.enterPassword(signUpexcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		String actualResult = loginpage.loginSuccessfully();
		String expectedResult = "Dashboard | Callhippo.com";
		assertEquals(actualResult, expectedResult);
		//loginpage.closePopup();
		//loginpage.closePopup();
		assertEquals(dialerloginpage.clickOnOpenDialerButton(), true);
		dialerloginpage.switchToNewTab();
		driver.get(url.dialerSignIn());
		assertEquals(dialerloginpage.verifySlideMenuIconsAvailable(driver), true);
	}
	
	@DataProvider(name = "InvalidEmailValidation")
	public static Object[][] invalidEmails() {

		return new Object[][] { { signUpexcel.getdata(0, 9, 2) }, { signUpexcel.getdata(0, 9, 3) }, { signUpexcel.getdata(0, 9, 4) },
				{ signUpexcel.getdata(0, 9, 5) }, { signUpexcel.getdata(0, 9, 6) } };

	}

	@Test(priority = 5, dataProvider = "InvalidEmailValidation", retryAnalyzer = Retry.class)
	public void verify_Invalid_Email_Validation(String email) {
		dialerloginpage.enterEmail(email);
		String actualResult = dialerloginpage.invalidEmailValidationTxt();
		String expectedResult = "Enter a valid email.";
		assertEquals(actualResult, expectedResult);
	}
	
	@DataProvider(name = "VerifySignInBtnIsNotClickable")
	public static Object[][] invalidCredentials() {

		return new Object[][] { { signUpexcel.getdata(0, 9, 2), " " }, { "", signUpexcel.getdata(0, 5, 2)}, { signUpexcel.getdata(0, 9, 2), signUpexcel.getdata(0, 10, 2) },
				{ signUpexcel.getdata(0, 9, 2), signUpexcel.getdata(0, 5, 2) },{signUpexcel.getdata(0, 9, 5),signUpexcel.getdata(0, 10, 2)} };
	}

	@Test(priority = 6, dataProvider = "VerifySignInBtnIsNotClickable", retryAnalyzer = Retry.class)
	public void verify_signInButton_IsDisable(String emailId, String pass) {
		dialerloginpage.enterEmail(emailId);
		dialerloginpage.enterPassword(pass);

		boolean btn = dialerloginpage.signupIsNotClickable();
		assertEquals(btn, true);
	}
	
	@DataProvider(name = "EmailDoesNotExist")
	public static Object[][] emailDoesNotExist() {

		return new Object[][] { { signUpexcel.getdata(0, 9, 8), " " }, { signUpexcel.getdata(0, 9, 9), signUpexcel.getdata(0, 5, 2) },
				{ signUpexcel.getdata(0, 9, 8), signUpexcel.getdata(0, 10, 2) }};
	}
	
	@Test(priority = 7, dataProvider = "EmailDoesNotExist", retryAnalyzer = Retry.class)
	public void verify_validation_for_InvalidEmail_InvalidPass(String emailId, String pass) {
		dialerloginpage.enterEmail(emailId);
		dialerloginpage.enterPassword(pass);
		dialerloginpage.clickOnLogin();
		Common.pause(1);
		String actualResult = dialerloginpage.validationMessageForInvalidLogin();
		String expectedResult = "This email doesn't exist";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_validation_for_validEmail_InvalidPass() {
		dialerloginpage.enterEmail(signUpexcel.getdata(0, 4, 2));
		dialerloginpage.enterPassword(signUpexcel.getdata(0, 10, 2));
		dialerloginpage.clickOnLogin();
		String actualResult = dialerloginpage.validationMessageForInvalidLogin();
		String expectedResult = "Incorrect email or password.";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void check_require_field_validation() {
		dialerloginpage.clearEmailPassword();
		assertEquals(dialerloginpage.verifyRequireFieldValidationOnLoginPage(), true);
	}
	
	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void readMore_functionality() {
		Common.pause(3);
		dialerloginpage.getTitle();		
		assertEquals(dialerloginpage.verifyReadMoreFunctionality(), true);
		//assertEquals(dialerloginpage.verifyCallHippoImageFunctionality(), true);
		assertEquals(dialerloginpage.verifyPostUpdateDateFunctionality(), true);
	}
	
	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void verify_googleplay_functionality() {
		Common.pause(3);
		dialerloginpage.getTitle();		
		assertEquals(dialerloginpage.verifyGooglePlayFunctionality(), true);
	}
	
	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void verify_AppStore_functionality() {
		Common.pause(3);
		dialerloginpage.getTitle();		
		assertEquals(dialerloginpage.verifyAppStoreFunctionality(), true);
	}
	
	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void login_Dialer_With_Valid_Credentials() {
		Common.pause(3);
		dialerloginpage.getTitle();
		
		dialerloginpage.loginDialer(loginexcel.getdata(0, 5, 5),loginexcel.getdata(0, 6, 5));
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
		Common.pause(5);
		assertEquals(dialerloginpage.verifySlideMenuIconsAvailable(driver), true);
	}
	
	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void verify_inactive_user_validation() throws Exception {
		driver.get(url.signUp());
		String date = signUpexcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.enterFullname(signUpexcel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(signUpexcel.getdata(0, 2, 2));
		registrationPage.enterMobile(signUpexcel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(signUpexcel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		driver.get(url.dialerSignIn());
		dialerloginpage.loginDialer(email,signUpexcel.getdata(0, 5, 2));
		String actualValidationMsg = dialerloginpage.validationMessageForInvalidLogin();
		String expectedValidationMsg = "Please activate your account.";
		assertEquals(actualValidationMsg, expectedValidationMsg);
	}
	
	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void number_not_assigned_user_check_validation() throws Exception {
		dialerloginpage.enterEmail(signUpexcel.getdata(0, 4, 2));
		dialerloginpage.enterPassword(signUpexcel.getdata(0, 5, 2));
		dialerloginpage.clickOnLogin();
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
		assertEquals(dialerloginpage.verifyDepartmentArrow(), true);
		dialerloginpage.enterNumberinDialer(data.getValue("account2Number1"));
		Common.pause(2);
		dialerloginpage.clickOnGreenDialButton();
		String validationOnCallDial = dialerloginpage.validationMessageForInvalidLogin();
		assertEquals(validationOnCallDial, "You haven't been assigned any number.");
		
		dialerloginpage.clickOnSmsMessage();
		dialerloginpage.typeSmsMessage();
		String sendSmsMsgValidation = dialerloginpage.validationMessageForInvalidLogin();
		assertEquals(sendSmsMsgValidation, "You haven't been assigned any number.");
	}
	
	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void verify_Add_Credit_Button() {
		Common.pause(3);
		dialerloginpage.getTitle();
		
		dialerloginpage.loginDialer(loginexcel.getdata(0, 5, 5),loginexcel.getdata(0, 6, 5));
		Common.pause(5);
		assertEquals(dialerloginpage.verifySlideMenuIconsAvailable(driver), true);
		assertEquals(dialerloginpage.verifyAddCreditButtonIsClickable(), true);
	}
	
	
	
}
