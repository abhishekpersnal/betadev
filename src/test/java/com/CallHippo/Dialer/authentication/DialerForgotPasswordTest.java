package com.CallHippo.Dialer.authentication;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.authentication.SignupPage;

public class DialerForgotPasswordTest {

	DialerForgotPasswordPage forgotPasswordPage;
	WebDriver driver;
	static Common excel;
	PropertiesFile url;
	DialerLoginPage loginpage;
	WebDriver driverMail;
	SignupPage dashboard;
	String prePass1,prePass2,prePass3;

	public DialerForgotPasswordTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(url.getValue("environment")+"\\Forgot Password.xlsx");

	}
	
	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			//driver.get(url);
			try {
				dashboard.deleteAllMail();
			}catch(Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail," Gmail issue ","BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}
	

	@BeforeMethod
	public void initialization() throws IOException {
		try {
			driver = TestBase.init();
			forgotPasswordPage = PageFactory.initElements(driver, DialerForgotPasswordPage.class);
			loginpage = PageFactory.initElements(driver, DialerLoginPage.class);
			
			try {
				driver.get(url.dialerSignIn());
			} catch (IOException e) {
				driver.get(url.dialerSignIn());
			}
		} catch (Exception e) {
			Common.Screenshot(driver," Dialer Forgot Password - Fail ","DialerForgotPasswordTest - BeforeMethod - initialization");
			System.out.println("----Need to check issue - DialerForgotPasswordTest - BeforeMethod - initialization----");
		} 
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		try {
			if (ITestResult.FAILURE == result.getStatus()) {
				String testname = "Fail";
				Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
				System.out.println(testname + " - " + result.getMethod().getMethodName());
				Common.errorLogs(result.getThrowable());
			} else {
				String testname = "Pass";
				Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
				System.out.println(testname + " - " + result.getMethod().getMethodName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			driver.quit();
		}
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void VerifyTitle() {

		String actualResult = loginpage.getTitle();
		String expectedResult = "CallHippo Dialer";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Verify_Forgot_Password_Link_in_login_Page() {

		assertTrue(forgotPasswordPage.validateForgotPasswordLink());
		
	}
	
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_Back_Signin_link() {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		forgotPasswordPage.clickOnBackToSignInLink();
		assertTrue(forgotPasswordPage.validateForgotPasswordLink());
	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void Verify_Forgot_Password_Fields() {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		assertTrue(forgotPasswordPage.validateBackToSignInLink());
		assertTrue(forgotPasswordPage.validateForgotPasswrdEmailTextBoxIsDisplayed());
		assertTrue(forgotPasswordPage.validateForgotPasswrdResetPasswordButtonIsDisplayed());
	}
	
	
	@DataProvider(name = "EmailValidation")
	public static Object[][] invalidEmails() {

		return new Object[][] { { excel.getdata(0, 1, 2) }, { excel.getdata(0, 1, 3) }, { excel.getdata(0, 1, 4) },
				{ excel.getdata(0, 1, 5) }, { excel.getdata(0, 1, 6) } };

	}

	@Test(priority = 5, dataProvider = "EmailValidation", retryAnalyzer = Retry.class)
	public void verify_email_validation_with_Invalid_email(String email) {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		forgotPasswordPage.enterEmail(email);
		boolean actualResult = forgotPasswordPage.emailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Verify_Email_is_required_field() throws InterruptedException {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		forgotPasswordPage.clearEmail();
		String actualResult = forgotPasswordPage.ValidateEmailRequiredField();
		String expectedResult = "Email is required.";
		assertEquals(actualResult, expectedResult);

	}
	
	
	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Verify_with_non_registered_email() {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		forgotPasswordPage.enterEmail(excel.getdata(0, 2, 2));
		forgotPasswordPage.clickOnForgotPasswordResetPasswordButton();
		String actualResult = forgotPasswordPage.ValidateEmailDoesnotExist();
		String expectedResult = "This email doesn't exist.";
		assertEquals(actualResult, expectedResult);

	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Verify_with_registered_email() {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		forgotPasswordPage.enterEmail(excel.getdata(0, 3, 2));
		//forgotPasswordPage.enterEmail("automation+auto7acc1@callhippo.com");
		forgotPasswordPage.clickOnForgotPasswordResetPasswordButton();
		Common.pause(1);
		String actualResult = forgotPasswordPage.validateWithRegisteredEmail();
		String expectedResult = "Please check your mail to reset your password.";
		assertEquals(actualResult, expectedResult);

	}
	

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_reset_password_link_From_gmail() {
		Verify_with_registered_email();
		forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
	}
	
	
	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void reset_password_without_entering_any_value_in_reset_password_page() {
		verify_reset_password_link_From_gmail();
		boolean actualResutl = forgotPasswordPage.submitIsClickable();
		boolean expectedResult = true;
		assertEquals(actualResutl, expectedResult);
	}
	
	
	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Verify_with_registered_email1() {
		Common.pause(1);
		forgotPasswordPage.clickOnForgotPasswordLink();
		forgotPasswordPage.enterEmail(excel.getdata(0, 3, 2));
		forgotPasswordPage.clickOnForgotPasswordResetPasswordButton();
		String actualResult = forgotPasswordPage.validateWithRegisteredEmail();
		String expectedResult = "Please check your mail to reset your password.";
		assertEquals(actualResult, expectedResult);

	}
	
	@DataProvider(name = "mismatchPasswordValidation")
	public static Object[][] mismatchpasswords() {

		return new Object[][] { { excel.getdata(0, 6, 2), excel.getdata(0, 7, 2) },
				{ excel.getdata(0, 6, 3), excel.getdata(0, 7, 3) }, { excel.getdata(0, 6, 4), excel.getdata(0, 7, 4) },
				{ excel.getdata(0, 6, 5), excel.getdata(0, 7, 5) } };

	}

	@Test(enabled=false,priority = 12, dependsOnMethods = "Verify_with_registered_email1", dataProvider = "mismatchPasswordValidation", retryAnalyzer = Retry.class)
	public void mismatch_Reset_Password_Validation(String password) { 
		
		forgotPasswordPage.ValidateResetPasswordButtonOnGmail();
		// forgotPasswordPage.enterFullName("Test Auto");
		forgotPasswordPage.enterPassword("Test@"+password);
		//forgotPasswordPage.enterConfirmPassword(confirmPassword);
		assertEquals(true, forgotPasswordPage.submitIsClickable());
		String actualResult = forgotPasswordPage.mismatchResetPasswordValidation();
		String expectedResult = "Password and confirm password does not matched.";
		assertEquals(actualResult, expectedResult);

	}
	
//	@DataProvider(name = "passwordValidation")
//	public static Object[][] passwordValidation() {
//
//		return new Object[][] { { excel.getdata(0, 20, 2), excel.getdata(0, 7, 2) },
//				{ excel.getdata(0, 6, 3), excel.getdata(0, 7, 3) }, { excel.getdata(0, 6, 4), excel.getdata(0, 7, 4) },
//				{ excel.getdata(0, 6, 5), excel.getdata(0, 7, 5) } };
//
//	}
	//@Test(priority = 13,  dataProvider = "passwordValidation", retryAnalyzer = Retry.class)	
	@Test(priority = 13,  retryAnalyzer = Retry.class)	
	public void short_Password_Validation() { 
		Verify_with_registered_email();
		forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
		
		
		forgotPasswordPage.enterPassword(excel.getdata(0, 20, 2));
		//forgotPasswordPage.enterConfirmPassword(excel.getdata(0, 11, col));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateMinimun8CharacterPassword(), true);
		

		forgotPasswordPage.enterPassword(excel.getdata(0, 20, 3));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateMinimunOnenumberPassword(), true);
		
		forgotPasswordPage.enterPassword(excel.getdata(0, 20, 4));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateUpperCaseValidationInPassword(), true);
		
		forgotPasswordPage.enterPassword(excel.getdata(0, 20, 5));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateLowerCaseValidationInPassword(), true);
		
		forgotPasswordPage.enterPassword(excel.getdata(0, 20, 6));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateSpecialCharacterValidationInPassword(), true);

//		for (int col = 2; col <= 5; col++) {
//
//			forgotPasswordPage.enterPassword(excel.getdata(0, 10, col));
//		//	forgotPasswordPage.enterConfirmPassword(excel.getdata(0, 11, col));
//			Common.pause(1);
//			String actualResult = forgotPasswordPage.validateShortPassword();
//			String expectedResult = "Password is too short.";
//			assertEquals(actualResult, expectedResult);
//
//			Common.pause(1);
//			String actualResult1 = forgotPasswordPage.validateShortConfirmPassword();
//			String expectedResult1 = "Confirm password is too short.";
//			assertEquals(actualResult1, expectedResult1);
//
//		}

	}
	
	
	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void setPreviousFirstPassword() throws Exception {  
		
			prePass1="Test@"+excel.randomPassword();
			System.out.println(prePass1); 
			Verify_with_registered_email();
			forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
			forgotPasswordPage.enterPassword(prePass1);
			//forgotPasswordPage.enterConfirmPassword(prePass1);
			forgotPasswordPage.clickOnResetPasswordButton();
			String actualResult1 = forgotPasswordPage.ValidateResetPasswordSuccessfully();
			String expectedResult1 = "Your password has been reset successfully";
			assertEquals(actualResult1, expectedResult1);
			assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
	}
			
	@Test(priority = 15, dependsOnMethods = "setPreviousFirstPassword",retryAnalyzer = Retry.class )
	public void setPreviousSecondPassword() throws Exception {
			
			prePass2="Test@"+excel.randomPassword();
			System.out.println(prePass2); 
			Verify_with_registered_email();
			forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
			forgotPasswordPage.enterPassword(prePass2);
			//forgotPasswordPage.enterConfirmPassword(prePass2);
			forgotPasswordPage.clickOnResetPasswordButton();
			String actualResult2 = forgotPasswordPage.ValidateResetPasswordSuccessfully();
			String expectedResult2 = "Your password has been reset successfully";
			assertEquals(actualResult2, expectedResult2);
			assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
	}
	@Test(priority = 16,dependsOnMethods = "setPreviousSecondPassword", retryAnalyzer = Retry.class)
	public void setPreviousThirdPassword() throws Exception {	
			prePass3="Test@"+excel.randomPassword();
			System.out.println(prePass3); 
			Verify_with_registered_email();
			forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
			forgotPasswordPage.enterPassword(prePass3);
			//forgotPasswordPage.enterConfirmPassword(prePass3);
			forgotPasswordPage.clickOnResetPasswordButton();
			String actualResult3 = forgotPasswordPage.ValidateResetPasswordSuccessfully();
			String expectedResult3 = "Your password has been reset successfully";
			assertEquals(actualResult3, expectedResult3);
			assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
		
	}
	@Test(priority = 17,dependsOnMethods = "setPreviousThirdPassword", retryAnalyzer = Retry.class)
	public void verify_set_previous_password() throws Exception {	
			Verify_with_registered_email();
			forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
			forgotPasswordPage.enterPassword(prePass1);
			//forgotPasswordPage.enterConfirmPassword(prePass1);
			forgotPasswordPage.clickOnResetPasswordButton();
			String actualResult1 = forgotPasswordPage.ValidateAlreadysetPasswordValidation();
			System.out.println(actualResult1);
			String expectedResult1 = "You used this password recently. Please choose a different one.";
			assertEquals(actualResult1, expectedResult1);
			assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
			
			forgotPasswordPage.enterPassword(prePass2);
			//forgotPasswordPage.enterConfirmPassword(prePass2);
			forgotPasswordPage.clickOnResetPasswordButton();
			String actualResult2 = forgotPasswordPage.ValidateAlreadysetPasswordValidation();
			System.out.println(actualResult2);
			String expectedResult2 = "You used this password recently. Please choose a different one.";
			assertEquals(actualResult2, expectedResult2);
			assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
			
			forgotPasswordPage.enterPassword(prePass3);
			//forgotPasswordPage.enterConfirmPassword(prePass3);
			forgotPasswordPage.clickOnResetPasswordButton();
			String actualResult3 = forgotPasswordPage.ValidateAlreadysetPasswordValidation();
			System.out.println(actualResult3);
			String expectedResult3 = "You used this password recently. Please choose a different one.";
			assertEquals(actualResult3, expectedResult3);
			assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
		
	}
	@Test(priority = 18,retryAnalyzer = Retry.class)
	public void reset_Password_and_login_successfully() throws Exception {
	
		Verify_with_registered_email();
		forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
		// forgotPasswordPage.enterFullName("Test Auto");
		String pass="Test@"+excel.randomPassword();

//		forgotPasswordPage.enterPassword(excel.getdata(0, 14, 2));
//		forgotPasswordPage.enterConfirmPassword(excel.getdata(0, 15, 2));
		forgotPasswordPage.enterPassword(pass);
		//forgotPasswordPage.enterConfirmPassword(pass);
		forgotPasswordPage.clickOnResetPasswordButton();
		String actualResult = forgotPasswordPage.ValidateResetPasswordSuccessfully();
		String expectedResult = "Your password has been reset successfully";
		assertEquals(actualResult, expectedResult);
		assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
		}
	
	

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void reset_Password_successfully() throws Exception {  // need to remove confirm password
		Verify_with_registered_email();
		forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
		String pass="Test@"+excel.randomPassword();

		// forgotPasswordPage.enterFullName("Test Auto");
//		 forgotPasswordPage.enterPassword(excel.getdata(0, 14, 2));
//		 forgotPasswordPage.enterConfirmPassword(excel.getdata(0, 15, 2));
		 
		forgotPasswordPage.enterPassword(pass);
		//forgotPasswordPage.enterConfirmPassword("Test@"+pass);
		forgotPasswordPage.clickOnResetPasswordButton();
		String actualResult = forgotPasswordPage.ValidateResetPasswordSuccessfully();
		String expectedResult = "Your password has been reset successfully";
		assertEquals(actualResult, expectedResult);
		//assertEquals("Login | Callhippo.com", loginpage.getTitle());
		assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());

	}

	@Test(priority = 20, dependsOnMethods = "reset_Password_successfully", retryAnalyzer = Retry.class) 
	public void reset_Password_twice() throws IOException {
		forgotPasswordPage.ValidateResetPasswordButtonTwiceOnGmail();
		String actualResult = forgotPasswordPage.validateexpiredResetPasswordLinkErrorMessage();
		String expectedResult = "Sorry this password reset request has expired. We have send you a new link to reset your password.";
		assertEquals(actualResult, expectedResult);

	}
	
	

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void reset_Password_and_login_with_newPassword() throws Exception {
		Verify_with_registered_email();
		forgotPasswordPage.ValidateResetPasswordButtonOnGmail1();
		String pass="Test@"+excel.randomPassword();
		// forgotPasswordPage.enterFullName("Test Auto");
//		forgotPasswordPage.enterPassword(excel.getdata(0, 18, 2));
//		forgotPasswordPage.enterConfirmPassword(excel.getdata(0, 19, 2));
		forgotPasswordPage.enterPassword(pass);
		//forgotPasswordPage.enterConfirmPassword(pass);
		forgotPasswordPage.clickOnResetPasswordButton();
		String actualResult = forgotPasswordPage.ValidateResetPasswordSuccessfully();
		String expectedResult = "Your password has been reset successfully";
		assertEquals(actualResult, expectedResult);
		//assertEquals("Login | Callhippo.com", loginpage.getTitle());
		assertEquals("Login | Callhippo.com", loginpage.getLoginTitle());
		
		driver.get(url.dialerSignIn());
		
		loginpage.enterEmail(excel.getdata(0, 3, 2));
		//loginpage.enterPassword(excel.getdata(0, 18, 2));
		loginpage.enterPassword(pass);
		loginpage.clickOnLogin();
		
		assertTrue(loginpage.verifyUserLoggedInDialerSuccessfully());

	}
	


}
