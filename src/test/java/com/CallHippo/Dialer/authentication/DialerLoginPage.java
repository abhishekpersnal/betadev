package com.CallHippo.Dialer.authentication;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class DialerLoginPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	public DialerLoginPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 30);
		action = new Actions(this.driver);
	}

	@FindBy(xpath = "//div[@class='g-signin2']//span[contains(.,'Sign in')][1]")
	WebElement google_signIn;

	@FindBy(xpath = "//label[contains(.,'Use Magic Link info')]")
	WebElement magiclink;

	@FindBy(xpath = "//a[contains(.,'Create Account')]")
	WebElement createAccount; 

	@FindBy(xpath = "//img[@class='chLoginLogo']")
	WebElement dialer_CallHippoLogo;

	@FindBy(xpath = "(//a[contains(.,'Sign Up Now!')])[1]")
	WebElement callHippo_signUpNow_Btn;

	@FindBy(xpath = "//span[@class='chPostReadMore'][contains(.,'Read More')]")
	WebElement readMore_link;

	@FindBy(xpath = "//img[@class='chLoginPostImage'][contains(@src,'Web-Updates-CallHippo.png')]")
	WebElement callHippo_blog_Img;

	@FindBy(xpath = "//h4[@class='chPostUpdateDate'][contains(.,'Web Updates @CallHippo')]")
	WebElement postUpdate_date;

	@FindBy(xpath = "//img[@class='chPostAndroidImg'][contains(@src,'googleplay')]")
	WebElement googleplay_img;

	@FindBy(xpath = "//img[@class='chPostIosImg'][contains(@src,'iosappstore')]")
	WebElement appStore_img;

	@FindBy(xpath = "//span[contains(.,'Sign in to CallHippo')]")
	WebElement signIn_callhippo_txt;

	@FindBy(xpath = "//input[@name='email']")
	WebElement email;

	@FindBy(xpath = "//input[@name='password']")
	WebElement password;

	@FindBy(xpath = "//button[@type='button'][contains(.,'Sign In')]")
	WebElement login;

	@FindBy(xpath = "//span[@class='chForgotPass'][contains(.,'Forgot password?')]")
	WebElement forgot_password_link;

	@FindBy(xpath = "//span[contains(.,'Dialpad')]")
	WebElement dialPad_txt;

	@FindBy(xpath = "//div[@title='Dial']")
	WebElement dial_button;

	@FindBy(id = "btnDraw")
	WebElement menuSlider_icon;

	@FindBy(xpath = "//div[@title='Dial']")
	WebElement green_dialButton;

	@FindBy(xpath = "//span[contains(.,'Enter a valid email.')]")
	WebElement emailValidation;

	@FindBy(xpath = "//button[@class[contains(.,'chInputSignInBtn')]][contains(.,'Sign In')]")
	WebElement disabled_SignIn;

	@FindBy(xpath = "//button[@class='ant-btn chInputSignInBtnEnable ant-btn-primary'][contains(.,'Sign In')]")
	WebElement enabled_SignIn;

	@FindBy(xpath = "//div[@class='toast-body']/span")
	WebElement validationMessage;

	@FindBy(xpath = "//p[contains(.,'Open Dialer')]")
	WebElement open_dialer_button;

	@FindBy(xpath = "//i[contains(.,'arrow_drop_up')]")
	WebElement dept_arrow_on_dialer_home;

	@FindBy(xpath = "//*[@id=\"backSpace\"]")
	WebElement backBtn;

	@FindBy(id = "nameInput")
	WebElement numberTextbox;

	@FindBy(xpath = "//i[@class[contains(.,'smsicon')]]//following-sibling::span[contains(.,'Send Message')]")
	WebElement smsMessageBtn;

	@FindBy(xpath = "//textarea[contains(@placeholder,'Type your message')]")
	WebElement typeSmsMessagePlace;

	@FindBy(xpath = "//i[@title='Send']")
	WebElement sendSmsMessage;

	@FindBy(xpath = "(//div[@class='usernicon_ch']//*[local-name()='svg'])[1]")
	WebElement profile_photo;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Dialpad')]")
	WebElement slideMenu_DialPad_icon;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'All calls')]")
	WebElement slideMenu_AllCalls_icon;

	@FindBy(xpath = "//li[@class='nav_chmenu_list drp cursor'][contains(.,'Contacts')]")
	WebElement slideMenu_contacts_icon;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'SMS')]")
	WebElement slideMenu_sms_icon;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Call Planner')]")
	WebElement slideMenu_callPlanner_icon;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Settings')]")
	WebElement slideMenu_Settings_icon;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Credits')]")
	WebElement slideMenu_credit_icon;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Feedback')]")
	WebElement slideMenu_feedback_icon;

	@FindBy(xpath = "//span[@class='fnnvwightqy'][contains(.,'Logout')]")
	WebElement slideMenu_logout_icon;

	@FindBy(xpath = "//a[contains(.,'Add Credits')]")
	WebElement addCredit_button;

	@FindBy(xpath = "//select[@class='browser-default crditselectop']")
	private WebElement select_addCredit_dropdown;
	
	@FindBy(xpath = "//a[@href='/credits']") private WebElement sidemenuCreditLink;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'YES')]") private WebElement yesButton;

	public boolean verifyFieldsDisplayOnLoginPage() {
		boolean welcome = false;
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}

		getTitle();

		try {
			if (signIn_callhippo_txt.isDisplayed() && email.isDisplayed() && password.isDisplayed()
					&& disabled_SignIn.isDisplayed() && forgot_password_link.isDisplayed()) {
				welcome = true;
			} else {
				welcome = false;
			}

		} catch (Exception e) {

		}
		return welcome;
	}

	public boolean verifyFieldsNotDisplayOnLoginPage() {
		boolean welcome = false;
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}

		getTitle();

		try {
			if (google_signIn.isDisplayed() && magiclink.isDisplayed() && createAccount.isDisplayed()) {
				welcome = true;
			} else {
				welcome = false;
			}

		} catch (Exception e) {

		}
		return welcome;
	}

	
	
	public String getTitle() {
		wait.until(ExpectedConditions.titleContains("CallHippo Dialer"));
		return driver.getTitle();
	}
	public String getLoginTitle() {
		wait.until(ExpectedConditions.titleContains("Login | Callhippo.com"));
		return driver.getTitle();
	}

	public void clickOnCallHippoLogo() {
		wait.until(ExpectedConditions.elementToBeClickable(dialer_CallHippoLogo));
		dialer_CallHippoLogo.click();
	}

	public boolean verifyUserRedirectOnCallHippoWebsite() {
		boolean welcome = false;
		System.out.println("----driver.getTitle()-----" + driver.getTitle());
		System.out.println("-------driver.getCurrentUrl()------" + driver.getCurrentUrl());
	//	wait.until(ExpectedConditions.titleContains("Best Virtual Phone System For Business"));
		wait.until(ExpectedConditions.titleContains("Virtual Phone System For All Businesses | CallHippo"));
		//wait.until(ExpectedConditions.visibilityOf(callHippo_signUpNow_Btn));

		// String actual_welcome_msg_str = driver.getCurrentUrl();
		// String expected_welcome_msg_str = "Welcome to CallHippo!!";//"Hey Unblocked
		// User Welcome Aboard!";

		if (driver.getCurrentUrl().equalsIgnoreCase("https://callhippo.com/")) {
			welcome = true;
		} else {
			welcome = false;
		}
		System.out.println("------currentURL-----" + driver.getCurrentUrl());
		return welcome;
	}

	public void enterEmail(String email) {
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void enterPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}

	public void clickOnLogin() {
		wait.until(ExpectedConditions.visibilityOf(login));
		login.click();
	}

	public void clearEmailPassword() {
		wait.until(ExpectedConditions.visibilityOf(email));
		email.sendKeys("a");
		email.sendKeys(Keys.BACK_SPACE);
		email.clear();

		password.sendKeys("b");
		password.sendKeys(Keys.BACK_SPACE);
		password.clear();
	}

	public void loginDialer(String email, String password) {
		enterEmail(email);
		enterPassword(password);
		clickOnLogin();
	}

	public boolean verifyUserLoggedInDialerSuccessfully() {
		boolean welcome = false;
		wait.until(ExpectedConditions.titleContains("CallHippo Dialer"));
		wait.until(ExpectedConditions.visibilityOf(dial_button));

		if (dialPad_txt.isDisplayed() && dial_button.isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}
		return welcome;
	}

	public boolean verifySlideMenuIconsAvailable(WebDriver driver) {
		boolean welcome = false;
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(menuSlider_icon));
		menuSlider_icon.click();
		wait.until(ExpectedConditions.visibilityOf(slideMenu_logout_icon));
		if (profile_photo.isDisplayed() 
				&& slideMenu_DialPad_icon.isDisplayed() 
				&& slideMenu_AllCalls_icon.isDisplayed()
				&& slideMenu_contacts_icon.isDisplayed() && slideMenu_sms_icon.isDisplayed()
				&& slideMenu_callPlanner_icon.isDisplayed() && slideMenu_Settings_icon.isDisplayed()
				&& slideMenu_credit_icon.isDisplayed() && slideMenu_feedback_icon.isDisplayed()
				&& slideMenu_logout_icon.isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}
		return welcome;
	}

	public String invalidEmailValidationTxt() {
		return emailValidation.getText();
	}

	public boolean signupIsNotClickable() {

		try {
			wait.until(ExpectedConditions.visibilityOf(enabled_SignIn));
			enabled_SignIn.click();
			return false;
		} catch (Exception e) {

			return true;
		}
	}

	public String validationMessageForInvalidLogin() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}

	public boolean verifyRequireFieldValidationOnLoginPage() {
		boolean welcome = false;

		if (driver
				.findElement(
						By.xpath("//input[@name='email']//following-sibling::span[contains(.,'Email is required.')]"))
				.isDisplayed()
				&& driver.findElement(By.xpath(
						"//input[@name='password']//following-sibling::span[contains(.,'Password is required.')]"))
						.isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}
		return welcome;
	}

	public boolean clickOnOpenDialerButton() {
		wait.until(ExpectedConditions.visibilityOf(open_dialer_button));
		try {
			open_dialer_button.click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean verifyDepartmentArrow() {

		try {
			// wait.until(ExpectedConditions.visibilityOf(dept_arrow_on_dialer_home));
			dept_arrow_on_dialer_home.click();
			return false;
		} catch (Exception e) {

			return true;
		}
	}

	public void enterNumberinDialer(String number) {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(backBtn));
			while (backBtn.isDisplayed() == true) {
				backBtn.click();
			}
		} catch (Exception e) {
		}

		wait.until(ExpectedConditions.visibilityOf(numberTextbox));

		numberTextbox.sendKeys(number);
	}

	public void clickOnSmsMessage() {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(smsMessageBtn));
			if (smsMessageBtn.isDisplayed() == true) {
				smsMessageBtn.click();
			}
		} catch (Exception e) {
		}
	}

	public void typeSmsMessage() {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(typeSmsMessagePlace));
			if (typeSmsMessagePlace.isDisplayed() == true) {
				typeSmsMessagePlace.sendKeys("Hello");
				wait.until(ExpectedConditions.elementToBeClickable(sendSmsMessage));
				sendSmsMessage.click();
			}
		} catch (Exception e) {
		}
	}

	public void clickOnGreenDialButton() {
		wait.until(ExpectedConditions.elementToBeClickable(green_dialButton));
		green_dialButton.click();
	}

	public void clickOnSideMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(menuSlider_icon));
		menuSlider_icon.click();
	}

	public boolean verifyAddCreditButtonIsClickable() {
		boolean welcome = false;

		slideMenu_credit_icon.click();
		wait.until(ExpectedConditions.visibilityOf(addCredit_button));
		wait.until(ExpectedConditions.elementToBeClickable(addCredit_button));

		if (addCredit_button.isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}
		return welcome;
	}

	public void switchToNewTab() {
		String currentWindowHandle = driver.getWindowHandle();

		// Get the list of all window handles
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());

		for (String window : windowHandles) {

			// if it contains the current window we want to eliminate that from switchTo();
			if (window != currentWindowHandle) {
				// Now switchTo new Tab.
				driver.switchTo().window(window);
				// Close the newly opened tab
				// driver.close();
				System.out.println("Switched successfully");
			}
		}
	}

	public boolean verifyReadMoreFunctionality() {
		boolean welcome = false;

		wait.until(ExpectedConditions.visibilityOf(readMore_link));
		readMore_link.click();
		welcome = verifyWebUpdates();
		return welcome;
	}

	public boolean verifyCallHippoImageFunctionality() {
		boolean welcome = false;

		wait.until(ExpectedConditions.visibilityOf(callHippo_blog_Img));
		callHippo_blog_Img.click();
		welcome = verifyWebUpdates();
		return welcome;
	}

	public boolean verifyPostUpdateDateFunctionality() {
		boolean welcome = false;

		wait.until(ExpectedConditions.visibilityOf(postUpdate_date));
		postUpdate_date.click();
		welcome = verifyWebUpdates();
		return welcome;
	}

	public boolean verifyWebUpdates() {
		boolean welcome = false;
		String currentWindowHandle = driver.getWindowHandle();
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windowHandles.get(1));

		Common.pause(5);
		wait.until(ExpectedConditions.titleContains("Release Note"));

		if (driver
				.findElement(By.xpath("//h1[contains(.,'Release Note')]"))
				.isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}
		driver.close();
		driver.switchTo().window(currentWindowHandle);
		return welcome;
	}

	public boolean verifyGooglePlayFunctionality() {
		boolean welcome = false;

		wait.until(ExpectedConditions.visibilityOf(googleplay_img));
		googleplay_img.click();

		Common.pause(3);
		switchToNewTab();
		//wait.until(ExpectedConditions.titleContains("CallHippo Cloud-based Business Telephony Solution - Apps on Google Play"));

		if (driver.findElement(By.xpath("//span[contains(.,'CallHippo Cloud-based Business Telephony Solution')]")).isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}

		return welcome;
	}

	public boolean verifyAppStoreFunctionality() {
		boolean welcome = false;

		wait.until(ExpectedConditions.visibilityOf(appStore_img));
		appStore_img.click();

		Common.pause(3);
		switchToNewTab();
		wait.until(ExpectedConditions.titleContains(driver.getTitle()));

		if (driver.findElement(By.xpath("//h1[contains(.,'CallHippo')]")).isDisplayed()) {
			welcome = true;
		} else {
			welcome = false;
		}

		return welcome;
	}

	// <------------------------- credit page ---------------------->

	public void clickOnCreditLink() {
		wait.until(ExpectedConditions.elementToBeClickable(sidemenuCreditLink)).click();

	}

	public void selectAddCreditDropdownValue(String creditValue) {
		Select credit = new Select(select_addCredit_dropdown);
		credit.selectByValue(creditValue);
	}
	
	public void clickOnAddCreditButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addCredit_button)).click();
	}
	
	public void clickOnYesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(yesButton)).click();;
		
	}

	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}
	
	public void invisibleValidationMessage() {
//		wait.until(ExpectedConditions.invisibilityOf(validationMessage));
		boolean toggle = true;
		while(toggle) {
			try {
				validationMessage.click();
			} catch (Exception e1) {
				break;
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}
		}
		
	}
	
}
