package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.AbstractPage;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.CallHippo.credit.CreditWebSettingPage;

public class CustomPlanCallingTest {
	DialerIndex phoneApp2;
	WebDriver driver2;
	DialerIndex phoneApp2Subuser;
	WebDriver driver2Subuser;
	DialerIndex phoneApp3;
	WebDriver driver3;
	WebDriver driverWebApp2;

	WebToggleConfiguration webApp2;
	HolidayPage webApp2HolidayPage;
	DialerIndex phoneApp4;
	WebDriver driver4;
	CreditWebSettingPage WebCreditpage;
	CallingCallCostTest callingCallCostobj;
	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number2 = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number1 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	String account4Email = data.getValue("account4MainEmail");
	String account5Email = data.getValue("account5MainEmail");
	String number4 = data.getValue("account4Number1");
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String userID;
	String numberID;
	Common excelTestResult;
	String outgoingCallPrice;
	
	public CustomPlanCallingTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj = new CallingCallCostTest();
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driverWebApp2 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driverWebApp2, HolidayPage.class);
		WebCreditpage = PageFactory.initElements(driverWebApp2, CreditWebSettingPage.class);

		driver2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp2 = PageFactory.initElements(driver2, DialerIndex.class);
		

		driver2Subuser = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2Subuser = PageFactory.initElements(driver2Subuser, DialerIndex.class);

		driver3 = TestBase.init_dialer();
		System.out.println("Opened phoneAap3 session");
		phoneApp3 = PageFactory.initElements(driver3, DialerIndex.class);

		driver4 = TestBase.init_dialer();
		System.out.println("Opened webaap session");
		phoneApp4 =PageFactory.initElements(driver4, DialerIndex.class);


		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPrice=callingCallCostobj.callCost(number2,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPrice);
		try {
			try {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap2 signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap2 signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened PhoneApp2 signin Page");
				loginDialer(phoneApp2, account2Email, account1Password);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened PhoneApp2 signin Page");
				loginDialer(phoneApp2, account2Email, account1Password);
			}

			try {
				driver2Subuser.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2Sub signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account1Password);
				System.out.println("loggedin phoneAap2Sub");
			} catch (Exception e) {
				driver2Subuser.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2Sub signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account1Password);
				System.out.println("loggedin phoneAap2Sub");
			}

			try {
				driver3.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account2Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driver3.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account2Password);
				System.out.println("loggedin phoneAap3");
			}
			try {
				driver4.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap4 signin page");
				loginDialer(phoneApp4, account4Email, account3Password);
				System.out.println("loggedin phoneAap4");
			} catch (Exception e) {
				driver4.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap4 signin page");
				loginDialer(phoneApp4, account4Email, account3Password);
				System.out.println("loggedin phoneAap4");
			}

			try {
				
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver2.get(url.dialerSignIn());

				phoneApp2Subuser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Subuser.getCallerName();
				driver2Subuser.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				String Url = driverWebApp2.getCurrentUrl();
				creditAPI.UpdateCustomCredit("500", "0", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.clickLeftMenuSetting();
				driverWebApp2.get(url.signIn());
				Thread.sleep(9000);
				webApp2.numberspage();
				
				webApp2.navigateToNumberSettingpage(number2);
				String Url1 = driverWebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver2.get(url.dialerSignIn());

				phoneApp3.afterCallWorkToggle("off");
				driver3.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();
				
				webApp2.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				WebCreditpage.autoRechargeToggle("off");
				Common.pause(2);

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver2.get(url.dialerSignIn());

				phoneApp2Subuser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Subuser.getCallerName();
				driver2Subuser.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				String Url = driverWebApp2.getCurrentUrl();
				creditAPI.UpdateCustomCredit("500", "0", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.clickLeftMenuSetting();
				driverWebApp2.get(url.signIn());
				Thread.sleep(9000);
				webApp2.numberspage();
				
				webApp2.navigateToNumberSettingpage(number2);
				String Url1 = driverWebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver2.get(url.dialerSignIn());

				phoneApp3.afterCallWorkToggle("off");
				driver3.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();
				
				webApp2.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				WebCreditpage.autoRechargeToggle("off");
				Common.pause(2);
			}
		} catch (Exception e1) {
			String testname = "Normal calling Before Method ";
			Common.Screenshot(driverWebApp2, testname, "WebApp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2Subuser, testname, "PhoneAppp2subuser Fail login");
			Common.Screenshot(driver3, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "PhoneAppp4 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driverWebApp2.get(url.signIn());
			driver2.navigate().to(url.dialerSignIn());
			driver2Subuser.navigate().to(url.dialerSignIn());
			driver3.navigate().to(url.dialerSignIn());
			driver4.navigate().to(url.dialerSignIn());
			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email, account3Password);
				Common.pause(3);
			}
			
			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Common.pause(9);
			String Url = driverWebApp2.getCurrentUrl();
			userID = webApp2.getUserId(Url);
			creditAPI.UpdateCustomCredit("500", "0", "0", "0", webApp2.getUserId(Url));
			webApp2.clickLeftMenuSetting();
			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			webApp2.userspage();
		} catch (Exception e) {

			try {
				Common.pause(2);
				driverWebApp2.get(url.signIn());
				driver2.navigate().to(url.dialerSignIn());
				driver2Subuser.navigate().to(url.dialerSignIn());
				driver3.navigate().to(url.dialerSignIn());
				driver4.navigate().to(url.dialerSignIn());
				Common.pause(3);
				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}
				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account4Email, account3Password);
					Common.pause(3);
				}
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				String Url = driverWebApp2.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				creditAPI.UpdateCustomCredit("500", "0", "0", "0", webApp2.getUserId(Url));
				webApp2.clickLeftMenuSetting();
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
			} catch (Exception e1) {
				String testname = "Normal calling Before Method ";
				Common.Screenshot(driverWebApp2, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2Subuser, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver3, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp1 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp2, testname, "webApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2Subuser, testname, "PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp2, testname, "webApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2Subuser, testname, "PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driverWebApp2.quit();
		driver2.quit();
		driver2Subuser.quit();
		driver3.quit();
		driver4.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Only_OverAll_Minutes_available_Incoming_Call_Completed() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "2", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		Reporter.log("last dial toggle is ON" + "</br>");

		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		Reporter.log("Dial number from phoneApp1" + "</br>");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		Reporter.log("validateCallingScreenOutgoingVia" + "</br>");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		Reporter.log("validateCallingScreenOutgoingNumber" + "</br>");

		phoneApp2.waitForIncomingCallingScreen();
		Reporter.log("waitForIncomingCallingScreen" + "</br>");

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		Reporter.log("validateCallingScreenIncomingNumber" + "</br>");

		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		
		Reporter.log("validateCallStatusInCallDetail" + "</br>");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Only_OverAll_Minutes_available_Outgoing_Call_Completed() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "2", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		Reporter.log("last dial toggle is ON" + "</br>");

		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		Reporter.log("Dial number from phoneApp1" + "</br>");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		Reporter.log("validateCallingScreenOutgoingVia" + "</br>");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		Reporter.log("validateCallingScreenOutgoingNumber" + "</br>");

		phoneApp3.waitForIncomingCallingScreen();
		Reporter.log("waitForIncomingCallingScreen" + "</br>");

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		Reporter.log("validateCallingScreenIncomingNumber" + "</br>");

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		
		Reporter.log("validateCallStatusInCallDetail" + "</br>");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp3.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Only_OverAll_Minutes_available_Incoming_Call_Completed_Answer_By_FTD() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number4);
	
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "2", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		Reporter.log("last dial toggle is ON" + "</br>");

		
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		Reporter.log("Dial number from phoneApp1" + "</br>");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		Reporter.log("validateCallingScreenOutgoingVia" + "</br>");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		Reporter.log("validateCallingScreenOutgoingNumber" + "</br>");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp4.waitForDialerPage();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		
		Reporter.log("validateCallStatusInCallDetail" + "</br>");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "2");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Only_1_OverAll_Minute_1_Min_Credit_available_Incoming_Call_Completed_Again_Incoming_xxx_Entry() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0.02", "0", "0", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(70000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
        Thread.sleep(5000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        // validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		
		
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		

		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
//		driver2.get(url.dialerSignIn());
//		driver3.get(url.dialerSignIn());
//		phoneApp2.enterNumberinDialer(number3);
//		phoneApp2.clickOnDialButton();
//		// Validate outgoingVia on outgoing calling screen
//		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
//		// validate outgoing Number on outgoing calling screen
//		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
//		phoneApp2.waitForDialerPage();
//		phoneApp3.waitForDialerPage();
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		
		phoneApp3.waitForDialerPage();
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		webApp2.validateClientxxxentry("XXXX");
		assertEquals("Call not setup", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		
	}
	
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Only_1_OverAll_Minute_1_min_Credit_available_Outgoing_Call_Completed_Again_Ougoing_CallHnagup() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0.02", "0", "0", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(70000);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPrice);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPrice);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		
		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		
		phoneApp2.waitForDialerPage();
		driverWebApp2.get(url.signIn());
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Credit_And_Incoming_Minute_Available__1_min_Outgoing_Call_Completed_Again_Ougoing_CallHnagup() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0.02", "1", "0", "0", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(50000);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPrice);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPrice);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		
		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Credit_And_Incoming_Min_Avl_1_Min_Incoming_Call_Completed_Again_Incoming_xxx_Entry() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0.04", "1", "0", "0", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(50000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
        Thread.sleep(5000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        // validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		
		
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		

		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.closeLowcreditNotification();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number4);
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(5000);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);driverWebApp2.get(url.signIn());webApp2.clickOnActivitFeed();
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		
		String settingcreditValueAfterFTD = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost2);
		String leftPanelcreditValueAfterFTD = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost2);
		
		driverWebApp2.get(url.signIn());
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedFTD = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedFTD = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedFTD, leftPanelcreditValueAfterFTD);
		assertEquals(settingcreditValueUpdatedFTD, settingcreditValueAfterFTD);
		//--------------------------------------------------
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		
        phoneApp3.enterNumberinDialer(number2);
		
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		
		phoneApp3.waitForDialerPage();
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		webApp2.validateClientxxxentry("XXXX");
		assertEquals("Call not setup", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		
	}


	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_and_Credit_Minute_Available_Incoming_Call_Completed_Again_Incoming_Call_Credit_Zero_CallHnagup() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0.02", "0", "10", "0", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(50000);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "0");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		
		phoneApp3.waitForDialerPage();
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		webApp2.validateClientxxxentry("XXXX");
		assertEquals("Call not setup", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_and_Credit_Minute_Available_Outgoing_Call_Completed_Again_Outgoing_Call_Credit_Zero_CallHnagup() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "0", "1", "0", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(50000);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		creditAPI.UpdateCustomCredit("0", "0", "0", GeneralOutgoingAfter, webApp2.getUserId(Url));
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		
		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		
		phoneApp2.waitForDialerPage();
		
	}
	

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_and_Credit_Available_for_1_Min_Outgoing_Call_Completed_Again_Outgoing_Call_Credit_Zero_CallHnagup() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0.02", "0", "1", "0", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		

		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(70000);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
        assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPrice);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPrice);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		
		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		
		phoneApp2.waitForDialerPage();
		
	}
	
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void All_GenralMinutes_Avl_IN_OUT_And_OverAll_Make_Incoming_Outgoing_And_FTD_Call() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "10", "10", "10", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		
		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		
		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
        phoneApp3.waitForIncomingCallingScreen();
        phoneApp3.clickOnIncomimgAcceptCallButton();
        Thread.sleep(5000);
        phoneApp2.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutCall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutCall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutCall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutCall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutCall, leftPanelcreditValueAfteroutCall);
		assertEquals(settingcreditValueUpdatedafteroutCall, settingcreditValueAfteroutCall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateMinDeductionafteroutcall);
		assertEquals(validateMinDeductionafteroutcall, GeneralOutgoingAfteroutcall);
		//--------------------------------------------------------------------------------------------------
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.closeLowcreditNotification();
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number4);
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
        phoneApp2.waitForIncomingCallingScreen();
        phoneApp4.waitForIncomingCallingScreen();
        phoneApp4.clickOnIncomimgAcceptCallButton();
        Thread.sleep(5000);
        phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfterFTD = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfterFTD = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafterFTD = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafterFTD = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafterFTD, leftPanelcreditValueAfterFTD);
		assertEquals(settingcreditValueUpdatedafterFTD, settingcreditValueAfterFTD);
		Common.pause(2);
		String GeneralMinutesAftreFTD = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfterFTD = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfterFTD = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateINMinDeductionafterFTD = webApp2.validateMinDeduction(validateMinDeduction, "1");
		System.out.println(validateINMinDeductionafterFTD);
		String validateOutMinDeductionafterFTD = webApp2.validateMinDeduction(validateMinDeductionafteroutcall, "1");
		System.out.println(validateOutMinDeductionafterFTD);
		assertEquals(validateINMinDeductionafterFTD, GeneralIncomingAfterFTD);
		assertEquals(validateOutMinDeductionafterFTD, GeneralOutgoingAfterFTD);
		
	}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void OverAll_MinAval_IN_OUT_Avl_For_1_Min_And_Make_Incoming_Call_And_FTD_Call_Call_Goes_Morethan_1_min() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "1", "1", "20", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(70000);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		
		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateInMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateInMinDeduction);
		String validateGenearalMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGenearalMinDeduction);
		assertEquals(validateInMinDeduction, GeneralIncomingAfter);
		assertEquals(validateGenearalMinDeduction, GeneralMinutesAftre);
		
		//------------------------------------------------------------------
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.closeLowcreditNotification();
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number4);
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
        phoneApp2.waitForIncomingCallingScreen();
        phoneApp4.waitForIncomingCallingScreen();
        phoneApp4.clickOnIncomimgAcceptCallButton();
        Thread.sleep(5000);
        phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfterFTD = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfterFTD = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafterFTD = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafterFTD = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafterFTD, leftPanelcreditValueAfterFTD);
		assertEquals(settingcreditValueUpdatedafterFTD, settingcreditValueAfterFTD);
		Common.pause(2);
		String GeneralMinutesAftreFTD = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfterFTD = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfterFTD = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateInMinDeductionafterFTD = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateInMinDeductionafterFTD);
		assertEquals(validateInMinDeductionafterFTD, GeneralOutgoingAfterFTD);
	
}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void OverAll_MinAval_IN_OUT_Avl_For_1_Min_And_Make_Outgoing_Call_And_Again_outgoing_Call_Minute_Deduct_From_OverAll() throws Exception {

		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "1", "1", "20", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(70000);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp3.validatecallType());
		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateoutMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateoutMinDeduction);
		String validategeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validategeneralMinDeduction);
		assertEquals(validateoutMinDeduction, GeneralOutgoingAfter);
		assertEquals(validategeneralMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		driver3.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter2ndoutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter2ndoutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated2ndoutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated2ndoutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated2ndoutcall, leftPanelcreditValueAfter2ndoutcall);
		assertEquals(settingcreditValueUpdated2ndoutcall, settingcreditValueUpdated2ndoutcall);
		Common.pause(2);
		String GeneralMinutesAftre2ndoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter2ndoutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter2ndoutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction2ndoutcall = webApp2.validateMinDeduction(validategeneralMinDeduction, "1");
		System.out.println(validateMinDeduction2ndoutcall);
		assertEquals(validateMinDeduction2ndoutcall, validateMinDeduction2ndoutcall);
		
		
	}
	
	//----- Countrywise calling
	
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out_and_Countrywise_In_Out_1_Minutes_available_Incoming_Call_Call_to_more_than_4_Min() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "1", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(250);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "1");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out_and_Countrywise_In_Out_1_Minutes_available_Ougoing_Call_Call_to_more_than_4_Min() throws Exception {

		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "1", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(250);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPrice);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPrice);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateOutgoingMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateOutgoingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateOutgoingMinDeduction, GeneralOutgoingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "1");
		System.out.println(validateUSMinDeduction);
		String validateUSOutgoingMinDeduction = webApp2.validateMinDeduction(USOutgoingBefore, "1");
		System.out.println(validateUSOutgoingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSOutgoingMinDeduction, USOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}


	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out_and_Countrywise_In_Out_1_Minutes_available_Incoming_FTD_Call_Call_to_more_than_2_Min() throws Exception {

		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number4);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "1", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Common.pause(130);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateOutMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateOutMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		assertEquals(validateOutMinDeduction, GeneralOutgoingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "1");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		String validateUSOutMinDeduction = webApp2.validateMinDeduction(USOutgoingBefore, "1");
		System.out.println(validateUSOutMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);
		assertEquals(validateUSOutMinDeduction, USOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out_and_Countrywise_In_Out__Minutes_available_Incoming_Call_() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "1", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out_and_Countrywise_In_Out_Minutes_available_Ougoing_Call_Countrywise_minute_Deduct() throws Exception {

		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "1", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateOutgoingMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "0");
		System.out.println(validateOutgoingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateOutgoingMinDeduction, GeneralOutgoingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSOutgoingMinDeduction = webApp2.validateMinDeduction(USOutgoingBefore, "1");
		System.out.println(validateUSOutgoingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSOutgoingMinDeduction, USOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out__Avl_Countrywise_In_Out_Minutes_Used_OverAll_Min_Avl_Incoming_Call_OverAll_countrywise_Minute_Deduct() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "10", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "1");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "0");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out__Avl_Countrywise_In_Out_Minutes_Used_OverAll_Min_Avl_Ougoing_Call_OverAll_Countrywise_minute_Deduct() throws Exception {

		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "10", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateOutgoingMinDeduction = webApp2.validateMinDeduction(GeneralOutgoingBefore, "0");
		System.out.println(validateOutgoingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateOutgoingMinDeduction, GeneralOutgoingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "1");
		System.out.println(validateUSMinDeduction);
		String validateUSOutgoingMinDeduction = webApp2.validateMinDeduction(USOutgoingBefore, "0");
		System.out.println(validateUSOutgoingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSOutgoingMinDeduction, USOutgoingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	
	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out__Avl_Countrywise_All_Mins_Used_Incoming_Outgoing_Call_OverAll_General_IN_Out_Minute_Deduct() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "5", "5", "5", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "0");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "0");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingBefore, "1");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralIncomingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "0");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
		
	}
	
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_Only_Total_and_Out_Min_Avl_Countrywise_All_Mins_Used_Incoming_Outgoing_Call_OverAll_and_Out_Min_General_Minute_Deduct() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "0", "10", "10", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "0");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "0");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingAfter, "1");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralOutgoingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "0");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
		
	}

	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_Only_Total_and_In_Min_Avl_Countrywise_All_Mins_Used_Incoming_Outgoing_Call_OverAll_and_In_Min_General_Minute_Deduct() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "10", "0", "10", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "0");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "1");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingAfter, "0");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralOutgoingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "0");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
		
	}
	
	
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_Only_Total_Min_Avl_Countrywise_All_Mins_Used_Incoming_Outgoing_Total_General_Minute_Deduct() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "10", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "0");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "1");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingAfter, "0");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralIncomingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "0");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
		
	}
	
	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Countrywise_IN_OUT_Mins_Avl_All_General_Minutes_Used_Incoming_Outgoing_Call_Morethan_1_min() throws Exception {

		
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("10", "0", "0", "0", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(70);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(70);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueAfter, callCost1);
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValueAfter, callCost1);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "0");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingAfter, "0");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralIncomingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "1");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
		
	}
	
	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_Only_Total_Min_Avl_Countrywise_In_Out_Mins_Avl_Incoming_Outgoing_Call__Minute_deduct_from_country_In_Out() throws Exception {

		
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "10", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("10", "10", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "0");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "0");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingAfter, "0");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralOutgoingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "1");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
		
	}
	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_Only_Total_Min_Avl_Countrywise_In_Out_Mins_Avl_Incoming_Outgoing_Call_Morethan_1_Min_Minute_deduct_from_country_And_General() throws Exception {

		
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "10", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(70);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(5000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "0");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "0");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		driver2.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number3);

		phoneApp2.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(70);
		phoneApp2.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfteroutcall = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfteroutcall = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdatedafteroutcall = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdatedafteroutcall = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdatedafteroutcall, leftPanelcreditValueAfteroutcall);
		assertEquals(settingcreditValueUpdatedafteroutcall, settingcreditValueAfteroutcall);
		Common.pause(2);
		String GeneralMinutesAftreoutcall = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfteroutcall = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfteroutcall = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeductionafteroutcall = webApp2.validateMinDeduction(validateGeneralMinDeduction, "1");
		System.out.println(validateGeneralMinDeductionafteroutcall);
		String validateOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(GeneralOutgoingAfter, "0");
		System.out.println(validateOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateGeneralMinDeductionafteroutcall, GeneralMinutesAftreoutcall);
		assertEquals(validateOutgoingMinDeductionafteroutcall, GeneralOutgoingAfteroutcall);
		String USMinutesAfteroutcall = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfteroutcall = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfteroutcall = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeductionafteroutcall = webApp2.validateMinDeduction(validateUSMinDeduction, "0");
		System.out.println(validateUSMinDeductionafteroutcall);
		String validateUSOutgoingMinDeductionafteroutcall = webApp2.validateMinDeduction(USOutgoingAfter, "1");
		System.out.println(validateUSOutgoingMinDeductionafteroutcall);
		
		assertEquals(validateUSMinDeductionafteroutcall, USMinutesAfteroutcall);
		assertEquals(validateUSOutgoingMinDeductionafteroutcall, USOutgoingAfteroutcall);
	}
	@Test(priority = 28, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Only_OverAll_Minutes_available_Incoming_Call_Completed_SubUser() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("0", "0", "0", "2", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("0", "0", "0", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		Reporter.log("last dial toggle is ON" + "</br>");

		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		Reporter.log("Dial number from phoneApp1" + "</br>");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		Reporter.log("validateCallingScreenOutgoingVia" + "</br>");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		Reporter.log("validateCallingScreenOutgoingNumber" + "</br>");

		phoneApp2Subuser.waitForIncomingCallingScreen();
		Reporter.log("waitForIncomingCallingScreen" + "</br>");

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2Subuser.validateCallingScreenIncomingVia());
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2Subuser.validateCallingScreenIncomingNumber());
		Reporter.log("validateCallingScreenIncomingNumber" + "</br>");

		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		
		Reporter.log("validateCallStatusInCallDetail" + "</br>");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver3.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp3.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp3.validateNumberInCallDetail(), "validate  number2 in call log details");
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		String status = phoneApp3.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateMinDeduction);
		assertEquals(validateMinDeduction, GeneralMinutesAftre);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
	@Test(priority = 29, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void General_IN_Out_and_Countrywise_In_Out_1_Minutes_available_Incoming_Subuser_Call_Call_to_more_than_4_Min() throws Exception {

		driver2.get(url.dialerSignIn());
//		phoneApp2.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driverWebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		creditAPI.UpdateCustomCredit("1", "1", "1", "1", webApp2.getUserId(Url));
		creditAPI.UpdateCreditCountyWise("1", "1", "1", "US", webApp2.getUserId(Url));
		
		webApp2.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		String GeneralMinutesBefore = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingBefore = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingBefore = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String USMinutesBefore = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingBefore = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingBefore = webApp2.getmin("US", "Free outgoing minutes");

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver2.get(url.dialerSignIn());
		phoneApp3.enterNumberinDialer(number2);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber());
		phoneApp2Subuser.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2Subuser.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2Subuser.validateCallingScreenIncomingNumber());
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		Common.pause(250);
		phoneApp3.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver3.get(url.dialerSignIn());
		Common.pause(3);
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		Common.pause(2);
		String GeneralMinutesAftre = webApp2.getmin("General minutes", "Free total minutes");
		Thread.sleep(1000);
		String GeneralIncomingAfter = webApp2.getmin("General minutes", "Free incoming minutes");
		Thread.sleep(1000);
		String GeneralOutgoingAfter = webApp2.getmin("General minutes", "Free outgoing minutes");
		Thread.sleep(1000);
		String validateGeneralMinDeduction = webApp2.validateMinDeduction(GeneralMinutesBefore, "1");
		System.out.println(validateGeneralMinDeduction);
		String validateIncomingMinDeduction = webApp2.validateMinDeduction(GeneralIncomingBefore, "1");
		System.out.println(validateIncomingMinDeduction);
		
		assertEquals(validateGeneralMinDeduction, GeneralMinutesAftre);
		assertEquals(validateIncomingMinDeduction, GeneralIncomingAfter);
		String USMinutesAfter = webApp2.getmin("US", "Free total minutes");
		Thread.sleep(1000);
		String USIncomingAfter = webApp2.getmin("US", "Free incoming minutes");
		Thread.sleep(1000);
		String USOutgoingAfter = webApp2.getmin("US", "Free outgoing minutes");
		String validateUSMinDeduction = webApp2.validateMinDeduction(USMinutesBefore, "1");
		System.out.println(validateUSMinDeduction);
		String validateUSIncomingMinDeduction = webApp2.validateMinDeduction(USIncomingBefore, "1");
		System.out.println(validateUSIncomingMinDeduction);
		
		assertEquals(validateUSMinDeduction, USMinutesAfter);
		assertEquals(validateUSIncomingMinDeduction, USIncomingAfter);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
	}
}