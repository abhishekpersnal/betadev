package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;

public class CallQueueCallingTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	DialerIndex phoneApp4;
	WebDriver driverPhoneApp4;
	DialerIndex phoneApp2SubUser;
	WebDriver driver6;
	WebDriver driverphoneApp5;
	DialerIndex phoneApp5;
	
	DialerLoginPage phoneApp5New;

	WebToggleConfiguration webApp2;
	LiveCallPage webApp2LivePage;
	WebDriver driver4;
	
	WebDriver driver5;
	LiveCallPage webApp2SubUserLivePage;
	WebToggleConfiguration webApp2SubUser;
	
	HolidayPage webApp2HolidayPage;
	CallingCallCostTest callingCallCostobj;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String number1 = data.getValue("account2Number2"); // account 2's number 2
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");

	String account4Email = data.getValue("account4MainEmail");
	String account5Email = data.getValue("account5MainEmail");
	String account4Password = data.getValue("masterPassword");

	String number3 = data.getValue("account3Number1"); // account 3's number
	String number4 = data.getValue("account4Number1"); // account 4's number
	String number5 = data.getValue("account5Number1"); // account 4's number


	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	String userID;
	String numberID;
	String subuserID;
	String account2Sub1CallerName;

	Common excelTestResult;
	String outgoingCallPrice,outgoingCallPrice2Min;
	
	public CallQueueCallingTest() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj = new CallingCallCostTest();
	}
	
	@BeforeTest
	public void initialization() throws Exception {
		
		driver4 = TestBase.init();
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2LivePage = PageFactory.initElements(driver4, LiveCallPage.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);

		
		driver5 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2SubUser = PageFactory.initElements(driver5, WebToggleConfiguration.class);
		webApp2SubUserLivePage = PageFactory.initElements(driver5, LiveCallPage.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		driverPhoneApp4 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driverPhoneApp4, DialerIndex.class);

		driver6 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver6, DialerIndex.class);
		
		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPrice=callingCallCostobj.callCost(number,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPrice);
		outgoingCallPrice2Min=Double.toString(Double.parseDouble(outgoingCallPrice)*2);
		System.out.println("outgoingCallPrice 2 Minute:"+outgoingCallPrice2Min);
		
		try {
			try {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			
			try {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2SubUser, account2EmailSubUser, account1Password);
			} catch (Exception e) {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2SubUser, account2EmailSubUser, account1Password);
			}

			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			try {
				driver6.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver6.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			try {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());
				
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
					Common.pause(3);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);
				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				
				webApp2.userspage();
				Common.pause(2);
				account2Sub1CallerName = webApp2.getusername(account2EmailSubUser);
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number);
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();
				
				
				
			} catch (InterruptedException e) {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());
				
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
					Common.pause(3);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);
				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				
				webApp2.userspage();
				Common.pause(2);
				account2Sub1CallerName = webApp2.getusername(account2EmailSubUser);
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number);
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}
		} catch (Exception e1) {
			String testname = "Call Queue Before Test";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driver6, testname, "PhoneAppp6 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}

	@BeforeMethod
	public void login() throws Exception {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver.get(url.dialerSignIn());
			driver1.get(url.dialerSignIn());
			driver2.get(url.dialerSignIn());
			driverPhoneApp4.get(url.dialerSignIn());
			driver6.get(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2SubUser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}

			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}
			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			driver4.get(url.usersPage());
			webApp2.navigateToUserSettingPage(account2Email);
			Common.pause(5);
			webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
			webApp2.selectMessageType("Voicemail","Text");
			Common.pause(2);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(2);

			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Thread.sleep(2000);
			webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
			Thread.sleep(2000);
			webApp2.selectMessageType("Welcome message","Text");
			webApp2.selectMessageType("Voicemail","Text");
			webApp2.clickonAWHMVoicemail();
			webApp2.selectMessageType("After Work Hours Message","Text");
			webApp2.clickonAWHMGreeting();
			webApp2.selectMessageType("After Work Hours Message","Text");
			webApp2.selectMessageType("Wait Music","Text");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
			webApp2.selectMessageType("IVR","Text");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
			Thread.sleep(3000);
			webApp2.customRingingTime("off");
			System.out.println("customRingingTime OFF");
			Thread.sleep(3000);
			
			webApp2.setCallRecordingToggle("on");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number1);
			Common.pause(9);
			webApp2.setCallRecordingToggle("on");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

		} catch (Exception e) {
			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver.get(url.dialerSignIn());
				driver1.get(url.dialerSignIn());
				driver2.get(url.dialerSignIn());
				driverPhoneApp4.get(url.dialerSignIn());
				driver6.get(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2SubUser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}

				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}
				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account4Email, account4Password);
					Common.pause(3);
				}
				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}

				driver4.get(url.usersPage());
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(5);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Thread.sleep(3000);
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			} catch (Exception e1) {
				String testname = "Call Queue Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver5, testname, "WebAppp2SubUser Fail login");
				Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driver6, testname, "PhoneAppp6 Fail login");

				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp6 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp6 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driver5.quit();
		driverPhoneApp4.quit();
		driver6.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password){

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.getLivCall(), "0");
		assertEquals(webApp2SubUserLivePage.getLivCall(), "0");
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		
		
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");
		assertEquals(webApp2LivePage.getLivCall(), "2");
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "2");
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		Common.pause(3);
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.getLivCall(), "0");
		
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ringing_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_Ringing_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		// phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber(),
				"--Incoming number in calling screen of dialer2--");
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_and_Missed()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status in call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ringing_Incoming_queued_call_served_to_user_and_Missed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_and_Miised()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_Ringing_Incoming_queued_call_served_to_user_and_Missed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		// phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber(),
				"--Incoming number in calling screen of dialer2--");
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"phoneApp1.validateDurationInCallDetail()");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_ACW_running_call_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		
		phoneApp3.clickOnIncomimgAcceptCallButton();
		
		phoneApp2.validateDurationInCallingScreen(1);
		
		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp2.clickOnOutgoingHangupButton();
		
		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "After Call Work");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		webApp2LivePage.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");
		
		Thread.sleep(10000);

		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();
		
		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "After Call Work");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_and_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_remaing_in_Queue_and_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(35000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_remaing_in_Queue_and_Missed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.waitForDialerPage();

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_remaing_in_Queue_and_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
        phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(35000);

		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_remaing_in_Queue_and_Missed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.waitForDialerPage();

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_and_Rejected()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status in call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_WelcomeMessage_status()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		// phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status in call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Welcome message", webApp2.validateCallStatus(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_after_Welcomemessage_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		
		phoneApp3.clickOnIncomimgAcceptCallButton();
		
		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		assertEquals(webApp2LivePage.validateCallStatus("2"), "Welcome Msg");
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		webApp2LivePage.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_1queued_call_served_to_user_and_Accepted_2queued_call_served_to_user_and_Accepted_()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		Thread.sleep(50000);
		phoneApp1.validateDurationInCallingScreen(4);
		phoneApp4.validateDurationInCallingScreen(4);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_1queued_call_served_to_user_and_Rejected_2queued_call_served_to_user_and_Accepted_()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomingRejectButton();

		// Thread.sleep(10000);

		// phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		// phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_1queued_call_served_to_user_and_Missed_2queued_call_served_to_user_and_Accepted_()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();

		// Thread.sleep(10000);

		// phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		// phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void call_queue_Multi_user_All_User_Logout_1stincoming_call_callNotSetup_2ndincoming_call_callNotSetup()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		// phoneApp2.waitForIncomingCallingScreen();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber(),
		// "--Incoming number in calling screen of dialer2--");
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Call not setup", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Call not setup", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void call_queue_Multi_user_busy_with_Incoming_ongoing_call_On1stNumber_subUser_AlwaysClosed_Incoming_call_On2ndNumber_queuedCall_served_to_mainUser2ndNumber_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);

		driver4.get(url.numbersPage());

		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		String department3 = webApp2.validateDepartmentNameFor2ndNumber();
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number1, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number3, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(department3, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + department3, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(department3, webApp2.validateDepartmentName(), "--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void call_queue_Team_user_busy_with_Incoming_ongoing_call_On1stNumber_2ndUser_bussy_with_Incoming_call_On2ndNumber_queuedCall_served_to_mainUser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(4000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();

		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number1, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		// String durationOnOutgoingCallScreen =
		// phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2SubUser.clickOnSideMenu();
		String callerNameSub = phoneApp2SubUser.getCallerName();

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName3, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		// softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail(), "--Duration of outgoing call in
		// call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName3, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName3, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerNameSub, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 24, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_1queued_callOn1stNumber_served_to_user_and_Accept_2queued_callOn2ndNumber_served_to_user_and_Accepted_()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		Common.pause(3);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();

		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp4.enterNumberinDialer(number1);
		phoneApp4.clickOnDialButton();

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		// phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName3, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName3, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName3, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 25, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_2ndUser_Login_queued_call_served_to_2nduser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_2ndUser_Login_queued_call_served_to_2nduser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 27, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_IVR_user_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("1");
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 28, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_IVR_user_served_to_Team_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 29, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_IVR_user_served_to_number_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();

		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 31, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_and_Rejected()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 32, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_welcomemessage_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
        phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 33, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_welcomemessagestatus()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(1000);
		phoneApp3.clickOnOutgoingHangupButton();
		// phoneApp3.clickOnOutgoingHangupButton();
		// phoneApp2.waitForDialerPage();
		// phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(1000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Welcome message", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 34, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_secondqueued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 35, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_rejectfirstqueuedcall_acceptsecondqueued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		//Thread.sleep(10000);
		Thread.sleep(5000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomingRejectButton();
		//Thread.sleep(10000);
		Thread.sleep(7000);
		// phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		// phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();
		//Thread.sleep(10000);
		phoneApp1.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 36, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_missedfirstqueuedcall_acceptsecondqueued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		// phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(10000);
		// phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		// phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 38, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_subUserAlwaysClosed_Incoming_queued_call_subUserAlwaysopened_served_to_suBser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());

		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(3000);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(7);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(5000);
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp3.clickOnOutgoingHangupButton();
//		//phoneApp2.waitForDialerPage();
//		//phoneApp2.clickOnIncomimgAcceptCallButton();
//
//
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number3, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		// assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2
		// Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 39, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_subUserAlwaysClosed_Incoming_queued_call_subUserAlwaysopened_served_to_suBser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(3000);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(7);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed2nd(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(outgoingCallPrice2Min, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 40, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_firstQueued_call_served_to_user_and_Miised_secondQueued_onSecondnumber_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		driver4.get(url.numbersPage());

		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();

		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp4.enterNumberinDialer(number1);
		phoneApp4.clickOnDialButton();
		//Thread.sleep(10000);
		Thread.sleep(5000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	
		phoneApp2.clickOnIncomimgAcceptCallButton();
		//Thread.sleep(10000);
		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		//phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton(); 
		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp4.waitForDialerPage(); 
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");
		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();
        Common.pause(2);
       
		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName3, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 1st Entry(2nd queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName3, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName3, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 41, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_IVR_user_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		Thread.sleep(5000);
		phoneApp3.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp3.pressIVRKey("1");
		Thread.sleep(10000);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("1");
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		Common.pause(10);
		webApp2.clickOnActivitFeed();
		
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 42, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_call_IVR_Number_busy_with_Incoming_ongoing_call_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp3.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		phoneApp3.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp3.pressIVRKey("3");

		Thread.sleep(5000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 3 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 3--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer3--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp3.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 43, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_call_IVR_Team_Simultenious_busy_with_Incoming_ongoing_call_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp3.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		phoneApp3.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp3.pressIVRKey("2");

		Thread.sleep(5000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp3.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 44, retryAnalyzer = Retry.class)
	public void team_call_queue_mainuser_and_subUser_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_mainuser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(4000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.enterNumberinDialer(number4);
		phoneApp2SubUser.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		// --------------------------------------------------
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp2SubUser.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 45, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_pressStar_callGoesToVoicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("10");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(1000);
		phoneApp1.pressIVRKey("*");
		Thread.sleep(25000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 46, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_voicemailDisabled_Incoming_queued_pressStar_callGetsHangedup()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("100");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(4000);
		phoneApp1.pressIVRKey("*");
		Thread.sleep(25000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		// phoneApp1.clickOnOutgoingHangupButton();
		// phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"missed status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 47, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_call_Incoming_queued_pressStar_callGoesToVoicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("100");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(4000);
		phoneApp1.pressIVRKey("*");
		Thread.sleep(25000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 48, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_call_VoicemailDisabled_Incoming_queued_pressStar_callGoesHangedup()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("100");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(4000);
		phoneApp1.pressIVRKey("*");
		Thread.sleep(25000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Missed status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 49, retryAnalyzer = Retry.class)
	public void team_call_queue_mainuser_and_subUser_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_subUser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.enterNumberinDialer(number4);
		phoneApp2SubUser.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 50, retryAnalyzer = Retry.class)
	public void Users_call_queue_mainuser_and_subUser_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_subUser_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		// webApp2.userTeamAllocation("Team");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.enterNumberinDialer(number4);
		phoneApp2SubUser.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 51, retryAnalyzer = Retry.class)
	public void IVR_Team_Fixed_Both_User_busy_with_Outgoing_Ongoing_Call_Incoming_queued_call_AnswerBy_Mainuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(4000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.enterNumberinDialer(number4);
		phoneApp2SubUser.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp2SubUser.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 52, retryAnalyzer = Retry.class)
	public void IVR_Team_Round_Robin_Both_User_busy_with_Outgoing_Ongoing_Call_Incoming_queued_call_AnswerBy_Mainuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(4000);
		String departmentName3 = webApp2.validateDepartmentNameFor2ndNumber();
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.enterNumberinDialer(number4);
		phoneApp2SubUser.clickOnDialButton();

		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		// phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp2SubUser.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	@Test(priority = 53, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_Number_Custom_Available()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, true);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "custom", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 54, retryAnalyzer = Retry.class)
	public void Outgoing_ongoing_call_Incoming_queued_call_served_to_Sub_User_Main_User_avaialble_SubUser_CustomAvl()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 55, retryAnalyzer = Retry.class)
	public void Incoming_ongoing_call_Incoming_queued_call_served_to_Sub_User_Both_CustomAvl()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 56, retryAnalyzer = Retry.class)
	public void Incoming_ongoing_call_Incoming_queued_call_served_to_Main_User_SubUser_custom_Unavailable()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", subuserID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		
		phoneApp2.waitForDialerPage();

		

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
	
		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 57, retryAnalyzer = Retry.class)
	public void Incoming_ongoing_call_Incoming_queued_call_served_to_sub_User_SubUser_custom_Unavailable_to_Available()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", subuserID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		api.addCustomTimeSlot("user", subuserID, true);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		
		phoneApp2SubUser.waitForDialerPage();

		

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	
	@Test(priority = 58, retryAnalyzer = Retry.class)
	public void Incoming_ongoing_call_Incoming_queued_call_served_to_sub_User_MainUser_custom_Available_to_unavailable()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForDialerPage();
		
		api.addCustomTimeSlot("user", userID, false);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		
		phoneApp2SubUser.clickOnIncomingHangupButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();
		Thread.sleep(5000);
		phoneApp2SubUser.waitForDialerPage();

		

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 59, retryAnalyzer = Retry.class)
	public void User_Outgoing_Call_ACW_running_call_Incoming_queued_call_remains_in_queue()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		driver1.get(url.dialerSignIn());
        phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2.validateDurationInCallingScreen(1);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(1000);
		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 60, retryAnalyzer = Retry.class)
	public void User_busy_with_ACW_running_call_Incoming_queued_call_Remains_in_Queue_and_Voicemal()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2.validateDurationInCallingScreen(1);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.clickOnOutgoingHangupButton();
		
		Thread.sleep(40000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 61, retryAnalyzer = Retry.class)
	public void User_Incoming_Call_ACW_running_call_Incoming_queued_call_remains_in_queue()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
        Thread.sleep(1000);
		phoneApp2.validateDurationInCallingScreen(1);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(1000);
		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 62, retryAnalyzer = Retry.class)
	public void User_busy_with_Incoming_Call_ACW_running_call_Incoming_queued_call_Remains_in_Queue_and_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp3.validateDurationInCallingScreen(1);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		
		phoneApp3.clickOnOutgoingHangupButton();
		Thread.sleep(40000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 63, retryAnalyzer = Retry.class)
	public void Outgoing_ongoing_call_Incoming_queued_call_served_to_SubUser_and_SubUser_loggedin_after_call_in_queue()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2 = phoneApp2SubUser.getCallerName();
		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName2, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 64, retryAnalyzer = Retry.class)
	public void call_queue_Multiuser_busy_with_Incoming_ongoing_call_Call_On1stNumber_Goes_in_Queue_Again_Incoming_call_On1stdNumber_queuedCall_All_call_served_User()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);

		
		phoneApp3.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
        phoneApp2SubUser.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number);
		
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(1000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		Thread.sleep(3000);
		phoneApp1.validateDurationInCallingScreen(4);
        Thread.sleep(5000);
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		phoneApp5New.enterNumberinDialer(number);
		phoneApp5.clickOnDialButton();
		Thread.sleep(3000);
		phoneApp5.validateDurationInCallingScreen(3);

		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2SubUser.clickOnIncomingHangupButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number5, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number5, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number5, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	
	@Test(priority = 65, retryAnalyzer = Retry.class)
	public void call_queue_Multiuser_busy_with_Outgoing_ongoing_call_Call_On1stNumber_Goes_in_Queue_Again_Incoming_call_On1stdNumber_queuedCall_All_call_served_User()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(3);

		
		phoneApp2.enterNumberinDialer(number3);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
        
		phoneApp2SubUser.enterNumberinDialer(number2);
		
		phoneApp2SubUser.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName2, phoneApp2SubUser.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Thread.sleep(1000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();
		Thread.sleep(3000);
		phoneApp1.validateDurationInCallingScreen(4);
       
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		//phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		phoneApp5.enterNumberinDialer(number);
		phoneApp5.clickOnDialButton();
		Thread.sleep(3000);
		phoneApp5.validateDurationInCallingScreen(3);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(number4, phoneApp2.validateNumberInAllcallsFor2ndEntry(), "--Number on dialer 2 all call page--");
		// assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on
		// dialer 2 all call page--");

		// validate via number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		// assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type
		// in dialer 2--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in
		// call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(number5, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page For 1st Entry(2nd queued call)
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page For 1st Entry(2nd queued call)
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page For 1st Entry(2nd queued call)
		assertEquals(number5, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerNameFor2ndEntry(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumberFor2ndentry(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number5, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	
	
	@Test(priority = 66, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_reject_Voicemail_call_hangup_while_Voicemail_script_is_playing()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
        phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(3000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Rejected call status call log detail of dialer2--");

	//	assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
		//		"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "--Rejected call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 67, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_reject_Voicemail_call_hangup_while_Voicemail_script_is_playing()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(3000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Rejected call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	
	@Test(priority = 68, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_missed_Voicemail_call_hangup_while_Voicemail_script_is_playing()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.waitForDialerPage();

		Thread.sleep(3000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

	  // validate call status on call details page
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer1--");

		
		//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
		//		"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 69, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_Incoming_queued_call_served_to_user_missed_Voicemail_call_hangup_while_Voicemail_script_is_playing()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		Thread.sleep(3000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals(true, phoneApp2.ValidateDurationonCalldetailspage());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}
	
	@Test(priority = 70, retryAnalyzer = Retry.class)
	public void user_busy_with_Incoming_ongoing_call_Incoming_queued_pressStar_callGoesToVoicemail_Call_hangup_while_voicemail_script_is_playing()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("60");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(15000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(1000);
		phoneApp1.pressIVRKey("*");
		Common.pause(3);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}
	@Test(priority = 71, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_call_Incoming_queued_pressStar_callGoesToVoicemailCall_hangup_while_voicemail_script_is_playing()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("100");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(14000);
		phoneApp1.clickOnDialPad();
		phoneApp1.pressIVRKey("*");
		phoneApp2.clickOnOutgoingHangupButton();
        Common.pause(2);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}


	@Test(priority = 72, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_call_IVR_Team_Simultenious_busy_with_Incoming_ongoing_call_queued_call_served_to_FTD_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		String durationOnOutgoingCallScreen = phoneApp3.validateDurationInCallingScreen(4);

		Thread.sleep(10000);
		phoneApp3.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp3.pressIVRKey("2");

		Thread.sleep(5000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		
		assertEquals(number3, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp5.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp3.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	@Test(priority = 73, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_1queued_call_served_to_user_Dialer_and_Accepted_2queued_call_served_to_FTD_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia(),
				"--Department name of dialer2--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer2--");


		phoneApp1.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp1.validateCallingScreenIncomingNumber());
		phoneApp1.clickOnIncomimgAcceptCallButton();

		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		String durationOnOutgoingCallScreen = phoneApp3.validateDurationInCallingScreen(4);

		phoneApp4.enterNumberinDialer(number);
		Common.pause(2);
		phoneApp4.clickOnDialButton();

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp4.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer4--");

		phoneApp4.validateDurationInCallingScreen(4);

		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		assertEquals(number4, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp5.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp3.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number4, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number4, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number4, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	
	@Test(priority = 74, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ongoing_call_Incoming_queued_call_served_to_2nd_FTD_and_Accept()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number4);
		webApp2.enterFTDnumber2(number5);
		
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp1.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp1.validateCallingScreenIncomingNumber());
		phoneApp1.clickOnIncomimgAcceptCallButton();
					
		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		Common.pause(2);
		phoneApp3.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
        phoneApp2.clickOnIncomingHangupButton();
        phoneApp2.waitForIncomingCallingScreen();
        phoneApp4.waitForIncomingCallingScreen();
        phoneApp5.waitForIncomingCallingScreen();
	    assertEquals(number3, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();
	
		Thread.sleep(10000);
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp5.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 4 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 3 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 3--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		phoneApp3.validateDurationInCallDetail();

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		phoneApp2.validateDurationInCallDetail();

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	
	@Test(priority = 75, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_subUserAlwaysClosed_Incoming_queued_call_subUserAlwaysopened_served_to_suBser_FTD_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp1.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp1.validateCallingScreenIncomingNumber());
		phoneApp1.clickOnIncomimgAcceptCallButton();

		
		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		Common.pause(2);
		phoneApp3.validateDurationInCallingScreen(4);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(5000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		
		
		assertEquals(number3, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();
		phoneApp2.clickOnOutgoingHangupButton();
		Thread.sleep(10000);
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp5.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 3 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 3 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 3--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer3--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer3--");

		// validate duration in call Details page
		phoneApp3.validateDurationInCallDetail();

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2SubUser.clickOnSideMenu();
		String callerNameSubuser = phoneApp2SubUser.getCallerName();
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		phoneApp2.validateDurationInCallDetail();

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerNameSubuser, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	
	@Test(priority = 76, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_FTD_Ongoing_call_Incoming_queued_call_served_to_FTD_and_Accept()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer2--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer2--");


		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();

		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		String durationOnOutgoingCallScreen = phoneApp3.validateDurationInCallingScreen(4);
		Thread.sleep(50000);
		phoneApp3.validateDurationInCallingScreen(4);
		phoneApp5.clickOnIncomingHangupButton();
		
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		assertEquals(number3, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp5.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp3.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("0.080", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	@Test(priority = 77, retryAnalyzer = Retry.class)
	public void call_queue_Dialer_Logout_user_busy_with_Incoming_FTD_Ongoing_call_Incoming_queued_call_served_to_FTD_and_Accept()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		
		driverphoneApp5 = TestBase.init2();
		driverphoneApp5.get(url.dialerSignIn());
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);
		phoneApp5New = PageFactory.initElements(driverphoneApp5, DialerLoginPage.class);
		try {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp5, account5Email, account1Password);
			phoneApp5.waitForDialerPage();
		}
		
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer2--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer2--");


		phoneApp5.waitForIncomingCallingScreen();

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();

		phoneApp3.enterNumberinDialer(number);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		String durationOnOutgoingCallScreen = phoneApp3.validateDurationInCallingScreen(4);
		Thread.sleep(50000);
		phoneApp2.validateDurationInCallingScreen(2);
		phoneApp5.clickOnIncomingHangupButton();
		
		phoneApp5.waitForIncomingCallingScreen();
		assertEquals(number3, phoneApp5.validateCallingScreenIncomingNumber());
		phoneApp5.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp5.waitForDialerPage();
	    loginDialer(phoneApp2, account2Email, account1Password);
		Thread.sleep(10000);
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp3.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp3.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp3.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp3.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp3.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number3, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("0.080", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

		driverphoneApp5.quit();
	}
	
	
	@Test(priority = 78, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_missed_Voicemail_call()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		
		webApp2LivePage.waitForChangeCallStatusTo("2", "Welcome Msg");
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		assertEquals(webApp2LivePage.validateCallStatus("2"), "Welcome Msg");
		assertEquals(webApp2LivePage.validateHangupButton("2"), true, "validate hangup button is display.");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
//		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
//				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		webApp2LivePage.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");
		
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.waitForDialerPage();
		
		Thread.sleep(3000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		Common.pause(6);
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

	  // validate call status on call details page
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");

		
		//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
		//		"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 79, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_Rejected_Voicemail_call()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		
		webApp2LivePage.waitForChangeCallStatusTo("2", "Welcome Msg");
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		assertEquals(webApp2LivePage.validateCallStatus("2"), "Welcome Msg");
		assertEquals(webApp2LivePage.validateHangupButton("2"), true, "validate hangup button is display.");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
//		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
//				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		webApp2LivePage.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");
		
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(3000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		Common.pause(6);
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

	  // validate call status on call details page
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");

		
		//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
		//		"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 80, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_IVR_Press_queued_call_served_to_user_missed_Voicemail_call()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		
		webApp2LivePage.waitForChangeCallStatusTo("2", "IVR");
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		assertEquals(webApp2LivePage.validateCallStatus("2"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("2"), true, "validate hangup button is display.");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
//		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
//				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Common.pause(2);

		phoneApp1.clickOnDialPad();
		Common.pause(2);
		phoneApp1.pressIVRKey1();

		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		webApp2LivePage.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");
		
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.waitForDialerPage();
		
		Thread.sleep(3000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		Common.pause(6);
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

	  // validate call status on call details page
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");

		
		//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
		//		"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 81, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
	
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
	
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(enabled = false, priority = 82, retryAnalyzer = Retry.class)
	public void mainuser_busy_with_Incoming_ongoing_call_new_Incoming_queued_call_served_to_mainuser_and_Accepted()
			throws Exception {
		
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String acc2SubUser1CallerName = phoneApp2SubUser.getCallerName();
		Common.pause(2);
		phoneApp2SubUser.clickOnSideMenuDialPad();
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		webApp2.DeselectSpecificUserOrTeam(acc2SubUser1CallerName, "Yes");
		Common.pause(1);
		webApp2.enterCallQueueDuration("300");
		webApp2.userspage();
		Common.pause(2);
		account2Sub1CallerName = webApp2.getusername(account2EmailSubUser);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
//		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}
	
	@Test(enabled = false, priority = 83, retryAnalyzer = Retry.class)
	public void mainuser_busy_with_outgoing_ongoing_call_new_Incoming_queued_call_served_to_mainuser_and_Accepted()
			throws Exception {
		
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String acc2SubUser1CallerName = phoneApp2SubUser.getCallerName();
		Common.pause(2);
		phoneApp2SubUser.clickOnSideMenuDialPad();
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		webApp2.DeselectSpecificUserOrTeam(acc2SubUser1CallerName, "Yes");
		Common.pause(1);
		webApp2.enterCallQueueDuration("300");
		webApp2.userspage();
		Common.pause(2);
		account2Sub1CallerName = webApp2.getusername(account2EmailSubUser);
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.getLivCall(), "0");
		assertEquals(webApp2SubUserLivePage.getLivCall(), "0");
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		
		
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(6);
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
//		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		
		phoneApp2SubUser.clickOnSideMenu();
//		phoneApp2SubUser.clickOnLogout();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		assertEquals(webApp2LivePage.validateCallType("2", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("2"), "");
		assertEquals(webApp2LivePage.validateFrom("2"), number2);
		assertEquals(webApp2LivePage.validateTo("2"), number);
		assertEquals(webApp2LivePage.validateCallStatus("2"), "In Queue");
		assertEquals(webApp2LivePage.getLivCall(), "2");
		
		assertEquals(webApp2LivePage.validateCallType("1", "Outgoing"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number);
		assertEquals(webApp2LivePage.validateTo("1"), number3);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "2");
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		Common.pause(3);
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2LivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2LivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.getLivCall(), "0");
		
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 84, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_ongoing_call_voicemailDisabled_Incoming_queued_pressStar_callGetsHangedup_with_music_file()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(2000);
		webApp2.selectMessageType("Wait Music","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Wait Music", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Wait Music", "butler_voicemail.mp3");
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(5000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(4000);
		phoneApp1.pressIVRKey("*");
		Thread.sleep(25000);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		// phoneApp1.clickOnOutgoingHangupButton();
		// phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"missed status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}
	//custom incoming Ringing time
		@Test(priority = 85, retryAnalyzer = Retry.class)
		public void verify_by_set_custom_incoming_Ringing_time_when_queue_call_served() throws Exception {
			System.out.print("Start......");
			driver4.get(url.numbersPage());
		
			webApp2.navigateToNumberSettingpage(number);
			
			Thread.sleep(9000);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			
			Common.pause(3);
			webApp2.customRingingTime("on");
			System.out.println("customRingingTime ON");
			Common.pause(3);
			webApp2.enterDuration_customRingingTime("15");
			System.out.println(webApp2.validateSuccessMessage());
			assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
			assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "15");
			//------------------------------------------------------------------------
			driver4.get(url.numbersPage());
			webApp2.navigateToNumberSettingpage(number);
			Thread.sleep(9000);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
			webApp2.enterCallQueueDuration("300");

			String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
			phoneApp3.enterNumberinDialer(number);
			phoneApp3.clickOnDialButton();

			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			phoneApp2SubUser.clickOnLogout();

			phoneApp1.enterNumberinDialer(number);
			String departmentName = phoneApp1.validateDepartmentNameinDialpade();
			phoneApp1.clickOnDialButton();

			// Validate outgoingVia on outgoing calling screen
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
					"--Department name of dialer1--");

			// validate outgoing Number on outgoing calling screen
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
					"--Number on calling screen of dialer1--");

			String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

			Thread.sleep(10000);

			phoneApp3.clickOnOutgoingHangupButton();
			phoneApp2.waitForDialerPage();
		
			Common.pause(15); // 15 sec ringing
	        assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");                                          
	        
	        assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

			Thread.sleep(10000);
			phoneApp1.clickOnSideMenu();
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			// validate Number in All Calls page
			assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");

			// validate via number in All Calls page
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
					"--Department name of dialer 1 all calls page--");

			// validate call type in all calls page
			assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

			phoneApp1.clickOnRightArrow();

			// validate Number on call details page
			assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");

			// validate call status in Call detail page
			assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
					"--Completed call status call log detail of dialer1--");

			// validate duration in call Details page
			softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());

			// validate department name in Call detail page
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
					"--Department name of dialer1 call log detail page--");

			phoneApp2.clickOnSideMenu();

			String callerName = phoneApp2.getCallerName();

			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			// validate Number in All Calls page
			assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");

			// validate via number in All Calls page
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
					"--Department name of dialer 2 all calls page--");

			// validate call type in all calls page
			assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

			phoneApp2.clickOnRightArrow();

			// validate Number on call details page
			assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");

			// validate duration in call Details page
			softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
					"--Duration of outgoing call in call detail page of dialer 2--");

			assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
					"--Missed call status call log detail of dialer2--");
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
					"--Department name of dialer2 call log detail page--");

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName(),
					"--Department name in web2 Activityfeed page--");
			assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
			assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
			assertEquals("Missed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
					"Incoming call type in web2 Activityfeed page--");
			assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	       
			System.out.println("All Assert Pass for WEB2");


		}
}
