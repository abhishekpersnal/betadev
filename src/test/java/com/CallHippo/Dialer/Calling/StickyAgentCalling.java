package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

//import org.omg.CORBA.ORB;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.CallHippo.Init.*;

public class StickyAgentCalling {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp2Sub2;
	WebDriver driverSub2;
	DialerIndex phoneApp2SubUser;
	WebDriver driver2;
	DialerIndex phoneApp3;
	WebDriver driver3;
	WebToggleConfiguration webApp2;
	WebDriver driverwebApp2;
	WebToggleConfiguration webApp;
	WebDriver driver6;
	DialerIndex phoneApp4;
	WebDriver driverPhoneApp4;
	DialerIndex phoneApp5;
	WebDriver driverPhoneApp5;
	HolidayPage WebApp2page;
	RestAPI api;

	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");

	String account2EmailMainUser = data.getValue("account2MainEmail");
	String account2PasswordMainUser = data.getValue("masterPassword");

	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2Email2SubUser = data.getValue("account2Sub2Email");
	String account2PasswordSubUser = data.getValue("masterPassword");

	String account4Email = data.getValue("account4MainEmail");
	String account4Password = data.getValue("masterPassword");

	String account5Email = data.getValue("account5MainEmail");
	String account5Password = data.getValue("masterPassword");

	String number2 = data.getValue("account2Number1"); // account 2's number
	String number1 = data.getValue("account1Number1"); // account 1's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String callerName2 = null;
	String callerName2Subuser = null;
	String callerName2Subuser2 = null;

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number

	String account4Number1 = data.getValue("account4Number1");
	String account5Number1 = data.getValue("account5Number1");
	String userID;
	String subuserID;
	String numberID;

	Common excelTestResult;

	public StickyAgentCalling() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();

		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}

	@BeforeTest
	public void initialization() throws Exception {

		driverwebApp2 = TestBase.init();
		webApp2 = PageFactory.initElements(driverwebApp2, WebToggleConfiguration.class);
		WebApp2page = PageFactory.initElements(driverwebApp2, HolidayPage.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver2, DialerIndex.class);

		driverSub2 = TestBase.init_dialer();
		phoneApp2Sub2 = PageFactory.initElements(driverSub2, DialerIndex.class);

		driver3 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driver3, DialerIndex.class);

		driverPhoneApp4 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driverPhoneApp4, DialerIndex.class);

		driverPhoneApp5 = TestBase.init_dialer();
		phoneApp5 = PageFactory.initElements(driverPhoneApp5, DialerIndex.class);

		try {
			try {
				driverwebApp2.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driverwebApp2.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			try {
				driverSub2.get(url.dialerSignIn());
				loginDialer(phoneApp2Sub2, account2Email2SubUser, account4Password);
				Common.pause(3);
			} catch (Exception e) {
				driverSub2.get(url.dialerSignIn());
				loginDialer(phoneApp2Sub2, account2Email2SubUser, account4Password);
				Common.pause(3);
			}

			try {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			try {
				driverPhoneApp5.get(url.dialerSignIn());
				loginDialer(phoneApp5, account5Email, account5Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp5.get(url.dialerSignIn());
				loginDialer(phoneApp5, account5Email, account5Password);
				Common.pause(3);
			}

			try {
				driver3.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driver3.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			}

			try {

				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

				phoneApp2Sub2.clickOnSideMenu();
				callerName2Subuser2 = phoneApp2Sub2.getCallerName();
				driverSub2.get(url.dialerSignIn());

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number2);
				Common.pause(9);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				String Url1 = driverwebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= " + numberID);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("on", "Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				webApp2.DeselectSpecificUserOrTeam(callerName2Subuser2, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number2);

				String Url = driverwebApp2.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number2);

				String subuserUrl = driverwebApp2.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			} catch (Exception e) {

				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

				phoneApp2Sub2.clickOnSideMenu();
				callerName2Subuser2 = phoneApp2Sub2.getCallerName();
				driverSub2.get(url.dialerSignIn());

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number2);
				Common.pause(9);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				String Url1 = driverwebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= " + numberID);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("on", "Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				webApp2.DeselectSpecificUserOrTeam(callerName2Subuser2, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number2);

				String Url = driverwebApp2.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number2);

				String subuserUrl = driverwebApp2.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}
		} catch (Exception e1) {
			String testname = "StickyAgernt Calling Test - Before Test";
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverSub2, testname, "PhoneAppp2sub2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail login");
			Common.Screenshot(driver3, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Fail login");
			Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(3);
			driverwebApp2.get(url.signIn());
			driverSub2.navigate().to(url.dialerSignIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			driver3.navigate().to(url.dialerSignIn());
			driverPhoneApp4.navigate().to(url.dialerSignIn());
			driverPhoneApp5.navigate().to(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			if (phoneApp2Sub2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Sub2, account2Email2SubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}

			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			if (phoneApp5.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account5Email, account5Password);
				Common.pause(3);
			}

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number2);
			Common.pause(3);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			webApp2.userTeamAllocation("Teams");
			Common.pause(3);
			webApp2.selectSpecificUserOrTeam("Team", "Yes");
			Common.pause(2);

			webApp2.userTeamAllocation("users");
			Common.pause(3);
			webApp2.Stickyagent("off", "Strictly");
			Common.pause(3);
			webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
			webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
			Common.pause(3);

			driverwebApp2.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			WebApp2page.clickOnHolidaySubMenu();
			WebApp2page.deleteAllHolidayEntries();

			driverwebApp2.get(url.usersPage());

			Common.pause(3);
			webApp2.navigateToUserSettingPage(account2EmailMainUser);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Thread.sleep(2000);
			webApp2.makeDefaultNumber(number2);

			driverwebApp2.get(url.usersPage());
			Common.pause(2);
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2.makeDefaultNumber(number2);

		} catch (Exception e) {
			try {
				Common.pause(3);
				driverwebApp2.get(url.signIn());
				driverSub2.navigate().to(url.dialerSignIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				driver3.navigate().to(url.dialerSignIn());
				driverPhoneApp4.navigate().to(url.dialerSignIn());
				driverPhoneApp5.navigate().to(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}

				if (phoneApp2Sub2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Sub2, account2Email2SubUser, account2PasswordSubUser);
					Common.pause(3);
				}

				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account4Email, account4Password);
					Common.pause(3);
				}

				if (phoneApp5.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp5, account5Email, account5Password);
					Common.pause(3);
				}

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number2);
				Common.pause(3);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);

				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.Stickyagent("off", "Strictly");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);

				driverwebApp2.get(url.signIn());
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				WebApp2page.clickOnHolidaySubMenu();
				WebApp2page.deleteAllHolidayEntries();

				driverwebApp2.get(url.usersPage());

				Common.pause(3);
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Thread.sleep(2000);
				webApp2.makeDefaultNumber(number2);

				driverwebApp2.get(url.usersPage());
				Common.pause(2);
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.makeDefaultNumber(number2);

			} catch (Exception e1) {
				String testname = "StickyAgernt Calling Test - Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail login");
				Common.Screenshot(driverSub2, testname, "PhoneAppp2Sub2 Fail login");
				Common.Screenshot(driver3, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Fail login");
				Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}

	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverSub2, testname, "PhoneAppp2Sub2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverSub2, testname, "PhoneAppp2Sub2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebApp2, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() throws IOException, InterruptedException {

		driver.quit();
		driver1.quit();
		driver2.quit();
		driverSub2.quit();
		driverwebApp2.quit();
		driver3.quit();
		driverPhoneApp4.quit();
		driverPhoneApp5.quit();
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void View_sticky_Agent_blog() throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Strictly");
		assertEquals(webApp2.ValidateStickyagentdescription(),
				"This features allows you to ensure your callers are always connected to the agent he/she last spoke to.To know more, Click here");
		assertEquals(webApp2.ValidateStrictlybinddescription(),
				"Connect the call to the same agent, the user spoke to last time and if that agent is busy, the call will not be redirected to any other agents.");
		assertEquals(webApp2.ValidateLooselybinddescription(),
				"Connect the call to the same agent, the user spoke to last time and if that agent is busy, the call will be redirected to other agents.");
		webApp2.Stickyagent("off", "Strictly");

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_call_Missed_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_call_Missed_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)

	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_call_Missed_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_call_Missed_FTD_ON_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_call_Missed_FTD_ON_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_call_Reject_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_call_Reject_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Unavailable_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Unavailable_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_User_Last_Attended_User_Busy_On_Call_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		// phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_User_Last_Attended_User_Busy_On_Call_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		// phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(35000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_On_Holiday_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_On_Holiday_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Not_Login_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Last_call_user_is_Not_Login_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Call_Missed_Voicemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_call_Missed_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_call_Missed_FTD_ON_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(4000);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Common.pause(3);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(account4Number1);
		Common.pause(3);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_call_Missed_FTD_ON_Vociemail_On()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(3);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(4000);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Common.pause(3);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(account4Number1);
		Common.pause(3);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Order_call_Reject_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Order_call_Reject_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_team_Simultaneous_order_Last_call_attained_user_is_Unavailable_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 24, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_team_Simultaneous_order_Last_call_attained_user_is_Unavailable_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 25, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Order_Last_Attended_User_Busy_On_Call_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		webApp2.Stickyagent("on", "Strictly");

		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Order_Last_Attended_User_Busy_On_Call_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number2);
		String departmentName1 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName1, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(20000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 27, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_Team_Simultaneous_Order_Last_call_attained_user_is_on_Holiday_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");

		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 28, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_Team_Simultaneous_Order_Last_call_attained_user_is_on_Holiday_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 29, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Order_Last_call_attained_user_is_Not_Login_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 30, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simultaneous_Order_Last_call_user_is_Not_Login_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 31, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_call_Missed_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 32, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_call_Missed_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 33, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_call_Missed_FTD_ON_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		webApp2.Stickyagent("off", "Strictly");

		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);

		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Common.pause(3);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(account4Number1);
		Common.pause(3);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 34, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_call_Missed_FTD_ON_Vociemail_On()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(3);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Common.pause(3);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(account4Number1);
		Common.pause(3);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 35, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_call_Reject_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();

		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 36, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_call_Reject_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(1000);
		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(35000);
		phoneApp1.clickOnIncomingHangupButton();
		// Thread.sleep(3000);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 37, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_Attended_User_Unavailable_Logout_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 38, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_Attended_User_Unavailable_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(3000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 39, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_Attended_User_Busy_On_Call_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		// phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 40, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_Attended_User_Busy_On_Call_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number2);
		String departmentName1 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName1, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		// phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		Thread.sleep(40000);
		phoneApp1.clickOnIncomingHangupButton();
		Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 41, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_Attended_User_Always_Closed_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		driverwebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		// Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 42, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_Attended_User_Always_Closed_Vociemail_ON_extra()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		driverwebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(5000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 43, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Main_Case()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(10000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		// --------------------------------------------------

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		Thread.sleep(10000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);

		// -----------------------------------------------------
//			  driverwebApp2.get(url.numbersPage());
//				webApp2.navigateToNumberSettingpage(number2);
//				Thread.sleep(9000);
//				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//				webApp2.Stickyagent("off", "Strictly");
//				phoneApp4.enterNumberinDialer(number2);
//				String departmentName5 = phoneApp4.validateDepartmentNameinDialpade();
//				String departmentName6 = phoneApp4.validateDepartmentNameinDialpade();
//				phoneApp4.clickOnDialButton();
//				// Validate outgoingVia on outgoing calling screen
//				assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//						"--Department name of dialer4--");
//
//				// validate outgoing Number on outgoing calling screen
//				assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//						"--Number on calling screen of dialer4--");
//
//				phoneApp2Sub2.waitForIncomingCallingScreen();
//				phoneApp2Sub2.clickOnIncomimgAcceptCallButton();
//				Common.pause(5);
//				phoneApp2Sub2.clickOnIncomingHangupButton();
//				
//				webApp2.Stickyagent("on", "Strictly");
//				
//				phoneApp4.enterNumberinDialer(number2);
//				phoneApp4.clickOnDialButton();
//				// Validate outgoingVia on outgoing calling screen
//				assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//						"--Department name of dialer4--");
//
//				// validate outgoing Number on outgoing calling screen
//				assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//						"--Number on calling screen of dialer4--");
//
//				Thread.sleep(10000);
//				phoneApp2Sub2.waitForIncomingCallingScreen();
//		        assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
//		        assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//				  Thread.sleep(1000);
		// -------------------------------------------------------------

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(10000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);

//---------------------------------------------------------------
		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		Thread.sleep(10000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);

		// ---------------------------------------------------------------
//						  phoneApp4.enterNumberinDialer(number2);
//							phoneApp4.clickOnDialButton();
//							// Validate outgoingVia on outgoing calling screen
//							assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//									"--Department name of dialer4--");
//
//							// validate outgoing Number on outgoing calling screen
//							assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//									"--Number on calling screen of dialer4--");
//
//							Thread.sleep(10000);
//							phoneApp2Sub2.waitForIncomingCallingScreen();
//					        assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
//					        assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//							  Thread.sleep(1000);						  

	}

	@Test(priority = 44, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_Team_Fixed_Order_Last_call_attained_user_is_on_Holiday_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");

		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);

		Thread.sleep(3000);

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 45, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_Team_Fixed_Order_Last_call_attained_user_is_on_Holiday_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);

		Thread.sleep(3000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 51, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Round_Robin_call_Missed_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}

		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 52, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Round_Robin_call_Missed__Vociemail_on()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}

		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 53, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Round_Robin_FTD_Enabled_call_Missed_Vociemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Thread.sleep(4000);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Common.pause(3);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(account4Number1);
		Common.pause(3);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 54, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Round_Robin_FTD_Enabled_call_Missed_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Thread.sleep(4000);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Common.pause(3);
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(account4Number1);
		Common.pause(3);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(45000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 55, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Round_Robin_call_Reject_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}

		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName3, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 56, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Simulatenous_Main_Case()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Thread.sleep(2000);
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(10000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		// --------------------------------------------------

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		Thread.sleep(10000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);

		// -----------------------------------------------------
//			  driverwebApp2.get(url.numbersPage());
//				webApp2.navigateToNumberSettingpage(number2);
//				Thread.sleep(9000);
//				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//				webApp2.Stickyagent("off", "Strictly");
//				phoneApp4.enterNumberinDialer(number2);
//				String departmentName5 = phoneApp4.validateDepartmentNameinDialpade();
//				String departmentName6 = phoneApp4.validateDepartmentNameinDialpade();
//				phoneApp4.clickOnDialButton();
//				// Validate outgoingVia on outgoing calling screen
//				assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//						"--Department name of dialer4--");
		//
//				// validate outgoing Number on outgoing calling screen
//				assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//						"--Number on calling screen of dialer4--");
		//
//				phoneApp2Sub2.waitForIncomingCallingScreen();
//				phoneApp2Sub2.clickOnIncomimgAcceptCallButton();
//				Common.pause(5);
//				phoneApp2Sub2.clickOnIncomingHangupButton();
//				
//				webApp2.Stickyagent("on", "Strictly");
//				
//				phoneApp4.enterNumberinDialer(number2);
//				phoneApp4.clickOnDialButton();
//				// Validate outgoingVia on outgoing calling screen
//				assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//						"--Department name of dialer4--");
		//
//				// validate outgoing Number on outgoing calling screen
//				assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//						"--Number on calling screen of dialer4--");
		//
//				Thread.sleep(10000);
//				phoneApp2Sub2.waitForIncomingCallingScreen();
//		        assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
//		        assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//				  Thread.sleep(1000);
		// -------------------------------------------------------------

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(10000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);

		// ---------------------------------------------------------------
		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		Thread.sleep(10000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);

		// ---------------------------------------------------------------
//						  phoneApp4.enterNumberinDialer(number2);
//							phoneApp4.clickOnDialButton();
//							// Validate outgoingVia on outgoing calling screen
//							assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//									"--Department name of dialer4--");
		//
//							// validate outgoing Number on outgoing calling screen
//							assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//									"--Number on calling screen of dialer4--");
		//
//							Thread.sleep(10000);
//							phoneApp2Sub2.waitForIncomingCallingScreen();
//					        assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
//					        assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//							  Thread.sleep(1000);						  
	}

	@Test(priority = 57, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Fixedorder_Main_Case()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		Thread.sleep(2000);
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(10000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		// --------------------------------------------------

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Strictly");

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		Thread.sleep(10000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);

		// -----------------------------------------------------
//			  driverwebApp2.get(url.numbersPage());
//				webApp2.navigateToNumberSettingpage(number2);
//				Thread.sleep(9000);
//				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//				webApp2.Stickyagent("off", "Strictly");
//				phoneApp4.enterNumberinDialer(number2);
//				String departmentName5 = phoneApp4.validateDepartmentNameinDialpade();
//				String departmentName6 = phoneApp4.validateDepartmentNameinDialpade();
//				phoneApp4.clickOnDialButton();
//				// Validate outgoingVia on outgoing calling screen
//				assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//						"--Department name of dialer4--");
		//
//				// validate outgoing Number on outgoing calling screen
//				assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//						"--Number on calling screen of dialer4--");
		//
//				phoneApp2Sub2.waitForIncomingCallingScreen();
//				phoneApp2Sub2.clickOnIncomimgAcceptCallButton();
//				Common.pause(5);
//				phoneApp2Sub2.clickOnIncomingHangupButton();
//				
//				webApp2.Stickyagent("on", "Strictly");
//				
//				phoneApp4.enterNumberinDialer(number2);
//				phoneApp4.clickOnDialButton();
//				// Validate outgoingVia on outgoing calling screen
//				assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//						"--Department name of dialer4--");
		//
//				// validate outgoing Number on outgoing calling screen
//				assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//						"--Number on calling screen of dialer4--");
		//
//				Thread.sleep(10000);
//				phoneApp2Sub2.waitForIncomingCallingScreen();
//		        assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
//		        assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//				  Thread.sleep(1000);
		// -------------------------------------------------------------

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(10000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);

		// ---------------------------------------------------------------
		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");

		Thread.sleep(10000);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(10000);

		// ---------------------------------------------------------------
//						  phoneApp4.enterNumberinDialer(number2);
//							phoneApp4.clickOnDialButton();
//							// Validate outgoingVia on outgoing calling screen
//							assertEquals("Outgoing Via " + departmentName5, phoneApp4.validateCallingScreenOutgoingVia(),
//									"--Department name of dialer4--");
		//
//							// validate outgoing Number on outgoing calling screen
//							assertEquals(number2, phoneApp4.validateCallingScreenOutgoingNumber(),
//									"--Number on calling screen of dialer4--");
		//
//							Thread.sleep(10000);
//							phoneApp2Sub2.waitForIncomingCallingScreen();
//					        assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
//					        assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//							  Thread.sleep(1000);						  
	}

	@Test(priority = 120, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_Round_Robin_call_Reject_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}

		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(1000);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		Thread.sleep(35000);
		phoneApp1.clickOnIncomingHangupButton();
		// Thread.sleep(3000);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 121, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_unavailable_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}

		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		driverwebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		// Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 122, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_unavailable_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		Common.pause(2);
		driverwebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(5000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 123, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_on_holiday_Vociemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 124, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_on_holiday_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(3000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 125, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_Not_Login_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(3000);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 126, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_Not_Login_Vociemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}

		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("on", "Strictly");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		Common.pause(2);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Call not setup", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Call not setup", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Call not setup", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 127, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_busy_Vociemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.userTeamAllocation("Team");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.clickOnIncomingHangupButton();
		Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 128, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_busy_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.Stickyagent("on", "Strictly");
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "on", "off");
		Thread.sleep(9000);
		webApp2.enterCallQueueDuration("10");
		webApp2.userTeamAllocation("Team");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(1000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		Thread.sleep(2000);
		phoneApp2.clickOnIncomingHangupButton();
		Thread.sleep(40000);
		phoneApp1.clickOnIncomingHangupButton();
		Thread.sleep(2000);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 58, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_call_Missed_Vociemail_ON()
			throws Exception {

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(20000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 59, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_call_Missed_FTD_ON_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 60, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_call_Missed_FTD_ON_Vociemail_ON()
			throws Exception {

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(20000);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 61, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_call_Rejecetd_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 62, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_call_Rejected_Vociemail_ON()
			throws Exception {

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(20000);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 63, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Unavailable_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Subuser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 64, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Unavailable_Vociemail_ON()
			throws Exception {

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Common.pause(2);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 65, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Busy_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 66, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_Busy_Vociemail_ON()
			throws Exception {

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.validateDurationInCallingScreen(1);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 67, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_on_Holiday_Vociemail_OFF()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(5000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 68, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_on_Holiday_2nd_User_Miss_call_Vociemail_ON()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 69, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_is_on_Holiday_2nd_User_rejected_Vociemail_ON()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number2);

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 70, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_Logout_Vociemail_off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Loosely");
		phoneApp1.enterNumberinDialer(number2);

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account1Password);

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Common.pause(5);

		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 71, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Last_call_attained_user_Logout_Vociemail_ON()
			throws Exception {

		phoneApp1.enterNumberinDialer(number2);

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnLogout();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(5);
		loginDialer(phoneApp2, account2EmailMainUser, account1Password);

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 72, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Call_Missed_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 73, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_Missed_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 74, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_Missed_FTD_ON_Vociemail_Off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 75, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_Missed_FTD_ON_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(2);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 76, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_Rejecet_FVociemail_Off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2Sub2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals("-", webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 77, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_Rejected_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2Sub2.clickOnIncomingRejectButton();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 78, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_user_is_Unavailable_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		Common.pause(5);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 79, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_user_is_Unavailable_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 80, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_user_is_Busy_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.validateDurationInCallingScreen(1);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);

		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 81, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_users_Team_Simultaneously_call_attained_user_is_Busy_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.validateDurationInCallingScreen(2);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 82, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_user_is_on_Holiday_Vociemail_OFF()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.DeselectUser(callerName2sub2);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(5000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 83, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_is_on_Holiday_2nd_User_Miss_call_Vociemail_ON()
			throws Exception {

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.DeselectUser(callerName2sub2);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 84, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_user_Logout_Vociemail_off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account1Password);

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Common.pause(5);

		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 85, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Simultaneously_Last_call_attained_user_Logout_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);

		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnLogout();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(2);
		loginDialer(phoneApp2, account2EmailMainUser, account1Password);

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 86, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Call_Missed_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(3);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();

		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 87, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_Missed_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);

		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 88, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Call_Missed_FTD_ON_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(3000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp2Sub2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 89, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_Missed_FTD_ON_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(3000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);

		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 90, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_Rejecet_Vociemail_Off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals("-", webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 91, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_call_Missed_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//		
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			
//			}
//			System.out.println("while loop conter = " +i);
//		}

		Common.pause(10);

		System.out.println("out of while loop ");
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execurt ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execurt ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execurt ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}

		System.out.println("loop end");
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 92, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_call_Missed_FTD_ON_Vociemail_Off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//		
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			
//			}
//			System.out.println("while loop conter = " +i);
//		}

		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 93, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_call_Missed_FTD_ON_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

////	int i = 1;
////	while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
////			&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
////			   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
////	
////		i = i + 1;
////		Thread.sleep(1000);
////
////		if (i == 60) {
////			break;
////		
////		}
////		System.out.println("while loop conter = " +i);
////	}
//	
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		Thread.sleep(2000);
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 94, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_call_Reject_Vociemail_Off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

//		
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.clickOnIncomingRejectButton();
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", (phoneApp2.validateCallStatusInCallDetail()));
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", (phoneApp2.validateCallStatusInCallDetail()));
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 95, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_Rejecet_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 96, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_attained_user_is_Unavailable_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 97, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_attained_user_is_Unavailable_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 98, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Fixed_Order_Last_call_attained_user_is_Busy_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		driverwebApp2.get(url.numbersPage());
		Thread.sleep(9000);
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 99, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Fixed_Order_Last_call_attained_user_is_Busy_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp3.enterNumberinDialer(number2);
		phoneApp3.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		driverwebApp2.get(url.numbersPage());
		Thread.sleep(9000);
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		phoneApp2Sub2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	// Deallocate User case at last

	@Test(priority = 100, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Last_call_attained_user_is_on_Holiday_Vociemail_OFF()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.DeselectUser(callerName2sub2);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(5000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 101, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Last_call_attained_user_is_on_Holiday_Vociemail_ON()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(5000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		WebApp2page.DeselectUser(callerName2sub2);
		WebApp2page.clickOnHolidaySaveButton();

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(5000);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Thread.sleep(2000);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForDialerPage();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 102, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_lossely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_attained_user_is_Not_Login_Vociemail_OFF()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Loosely");
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForDialerPage();

		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 103, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_lossely_bind_sticky_agent_Number_Allocation_type_Team_Fixed_Order_Last_call_attained_user_is_Not_Login_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.Stickyagent("on", "Loosely");
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(2000);
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), true);
		phoneApp2Sub2.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 104, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_call_Missed_Vociemail_OFF()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//		
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			
//			}
//			System.out.println("while loop conter = " +i);
//		}

		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execute ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execute ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execute ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 105, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_User_Unavailable_Vociemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

////		int i = 1;
////		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
////				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
////				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
		////
////			i = i + 1;
////			Thread.sleep(1000);
		////
////			if (i == 60) {
////				break;
////			
////			}
////			System.out.println("while loop conter = " +i);
////		}
		//
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execurt ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execurt ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execurt ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 106, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_call_Reject_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.clickOnIncomingRejectButton();
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 111, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_User_Unavailable_Vociemail_ON()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//		
//			i = i + 1;
//			Thread.sleep(1000);
//
//			if (i == 60) {
//				break;
//			
//			}
//			System.out.println("while loop conter = " +i);
//		}

		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		driverwebApp2.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "close", "off");
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 112, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Lossely_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_busy_Vociemail_On()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "on", "off");
		webApp2.Stickyagent("on", "Loosely");
		webApp2.enterCallQueueDuration("100");
		webApp2.userTeamAllocation("Team");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 113, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Lossely_bind_sticky_agent_Number_Allocation_type_Team_RoundRObin_Last_Attended_User_busy_Vociemail_Off()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
//		int i = 1;
//		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
//				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
//			    && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
//			i = i + 1;
//			Thread.sleep(1000);
//			if (i == 60) {
//				break;
//			}
//		}
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp2.clickOnIncomingHangupButton();
		}
		phoneApp3.enterNumberinDialer(number2);
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer3--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer3--");
		Common.pause(10);
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			// phoneApp2.clickOnIncomingHangupButton();
		}

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.Stickyagent("on", "Loosely");
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();

		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

	}

	@Test(priority = 114, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_User_Holiday_Vociemail_Off()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

////		int i = 1;
////		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
////				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
////				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
////		
////			i = i + 1;
////			Thread.sleep(1000);
////
////			if (i == 60) {
////				break;
////			
////			}
////			System.out.println("while loop conter = " +i);
////		}
//		
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execurt ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execurt ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execurt ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		// phoneApp1.clickOnOutgoingHangupButton();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 115, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_User_Holiday_Vociemail_On()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

////		int i = 1;
////		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
////				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
////				   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
////		
////			i = i + 1;
////			Thread.sleep(1000);
////
////			if (i == 60) {
////				break;
////			
////			}
////			System.out.println("while loop conter = " +i);
////		}
//		
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execurt ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execurt ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execurt ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverSub2.navigate().to(url.dialerSignIn());

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		WebApp2page.clickOnHolidaySubMenu();
		WebApp2page.clickOnHolidayButton();
		WebApp2page.enterHolidayName("holiday for user");
		WebApp2page.clickOnAllocationDropDown();
		WebApp2page.selectAllocation("Users");
		WebApp2page.DeselectUser(callerName2Sub);
		Common.pause(2);
		WebApp2page.DeselectUser(callerName2Sub2);
		WebApp2page.clickOnHolidaySaveButton();
		Common.pause(2);
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 116, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_User_not_Login_Vociemail_Off()
			throws Exception {
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub = phoneApp2Sub2.getCallerName();
		driver2.navigate().to(url.dialerSignIn());

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

////	int i = 1;
////	while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
////			&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
////			   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
////	
////		i = i + 1;
////		Thread.sleep(1000);
////
////		if (i == 60) {
////			break;
////		
////		}
////		System.out.println("while loop conter = " +i);
////	}
//	
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execurt ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execurt ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execurt ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2Sub2.clickOnIncomingHangupButton();
		phoneApp2Sub2.waitForDialerPage();
		// phoneApp1.clickOnOutgoingHangupButton();
		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 117, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Loosely_bind_sticky_agent_Number_Allocation_type_Team_Round_Last_User_not_Login_Vociemail_On()
			throws Exception {

		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverwebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

////	int i = 1;
////	while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
////			&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false
////			   && phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == false) {
////	
////		i = i + 1;
////		Thread.sleep(1000);
////
////		if (i == 60) {
////			break;
////		
////		}
////		System.out.println("while loop conter = " +i);
////	}
//	
		Common.pause(10);

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2 execurt ");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub1 execurt ");
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		} else if (phoneApp2Sub2.validateIncomingCallingScreenIsdisplayed() == true) {
			System.out.println("phoneApp2sub2 execurt ");
			phoneApp2Sub2.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
		}
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(5);
		webApp2.Stickyagent("on", "Loosely");
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2Sub2.waitForDialerPage();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(12);
		phoneApp1.clickOnOutgoingHangupButton();
		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 118, retryAnalyzer = Retry.class)
	public void Verify_call_Flow_of_Strictly_bind_sticky_agent_Number_Allocation_type_users_Deallocated_From_Number_Vociemail_ON()
			throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.Stickyagent("off", "Strictly");
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(5);
		phoneApp1.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		webApp2.Stickyagent("on", "Strictly");
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		Common.pause(5);
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(5);

		String department3 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Common.pause(2);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 118, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_All_user_and_Team_dealocated() throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(3);

		webApp2.userTeamAllocation("Teams");
		Common.pause(3);
		webApp2.DeselectSpecificUserOrTeam("Team", "Yes");
		Common.pause(2);

		webApp2.Stickyagent("off", "Strictly");
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver2.get(url.dialerSignIn());

		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2Subuser, "Yes");
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Common.pause(2);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

	@Test(priority = 118, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_callNotSetup_All_user_And_Team_dealocated() throws Exception {
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(3);

		webApp2.userTeamAllocation("Teams");
		Common.pause(3);
		webApp2.DeselectSpecificUserOrTeam("Team", "Yes");
		Common.pause(2);

		webApp2.Stickyagent("off", "Strictly");
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver2.get(url.dialerSignIn());

		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2Subuser, "Yes");
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Common.pause(2);
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false);
//	String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
//      Thread.sleep(10000);
//      phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("No User is allocated", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number1, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number1, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("No User is allocated", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("No User is allocated", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	}

}