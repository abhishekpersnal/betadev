package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.AbstractPage;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class NormalCallingTest {
	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	WebDriver driver3;

	WebToggleConfiguration webApp2;
	LiveCallPage webApp2LivePage;
	LiveCallPage webApp1LivePage;
	WebToggleConfiguration webApp1;
	WebDriver driver4;
	
	WebDriver driver5;
	LiveCallPage webApp2SubUserLivePage;
	WebToggleConfiguration webApp2SubUser;
	HolidayPage webApp2HolidayPage;
	CallingCallCostTest callingCallCostobj;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String userID;
	String numberID;
	String outgoingCallPrice;
	Common excelTestResult;
	
	public NormalCallingTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj = new CallingCallCostTest();
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2LivePage = PageFactory.initElements(driver4, LiveCallPage.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);

		driver3 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp1 = PageFactory.initElements(driver3, WebToggleConfiguration.class);
		webApp1LivePage = PageFactory.initElements(driver3, LiveCallPage.class);
		
		driver5 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2SubUser = PageFactory.initElements(driver5, WebToggleConfiguration.class);
		webApp2SubUserLivePage = PageFactory.initElements(driver5, LiveCallPage.class);

		driver = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap3 session");
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPrice=callingCallCostobj.callCost(number,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPrice);
		
		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driver3.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp1, account1Email, account1Password);
			} catch (Exception e) {
				driver3.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp1, account1Email, account1Password);
			}
			
			try {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2SubUser, account2EmailSubUser, account1Password);
			} catch (Exception e) {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2SubUser, account2EmailSubUser, account1Password);
			}

			try {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}
			try {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);
				
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				Thread.sleep(9000);
				webApp2.setCallRecordingToggle("on");
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
				webApp2.enterAWHM_message(""
						+ "You have called out of our business hours. Please call later You have called out of our business hours. Please call later You have called out of our business hours. ");
				Thread.sleep(3000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(acc2Number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				driver3.get(url.numbersPage());
				webApp1.numberspage();

				webApp1.navigateToNumberSettingpage(number2);
				Thread.sleep(9000);
				webApp1.setCallRecordingToggle("on");
			} catch (Exception e) {
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);

				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getUserId(Url1);
				
				Thread.sleep(9000);
				webApp2.setCallRecordingToggle("on");
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
				webApp2.enterAWHM_message(""
						+ "You have called out of our business hours. Please call later You have called out of our business hours. Please call later You have called out of our business hours. ");
				Thread.sleep(3000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Thread.sleep(2000);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(acc2Number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				driver3.get(url.numbersPage());
				webApp1.numberspage();

				webApp1.navigateToNumberSettingpage(number2);
				Thread.sleep(9000);
				webApp1.setCallRecordingToggle("on");
			}
		} catch (Exception e1) {
			String testname = "Normal calling Before Method ";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driver3.get(url.signIn());
			driver4.get(url.signIn());
			driver5.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			
			
			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2SubUser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			if (webApp1.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp1, account1Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			
			driver4.get(url.numbersPage());
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Thread.sleep(9000);
			
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Thread.sleep(2000);
			webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
			Thread.sleep(2000);
			webApp2.selectMessageType("Welcome message","Text");
			webApp2.selectMessageType("Voicemail","Text");
			webApp2.clickonAWHMVoicemail();
			webApp2.selectMessageType("After Work Hours Message","Text");
			webApp2.clickonAWHMGreeting();
			webApp2.selectMessageType("After Work Hours Message","Text");
			webApp2.selectMessageType("Wait Music","Text");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
			webApp2.selectMessageType("IVR","Text");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
			Thread.sleep(3000);
			webApp2.customRingingTime("off"); //add new
			System.out.println("customRingingTime OFF");
			Thread.sleep(3000);
			webApp2.setCallRecordingToggle("on");

			driver3.get(url.numbersPage());
			webApp1.numberspage();

			webApp1.navigateToNumberSettingpage(number2);
			Thread.sleep(9000);
			webApp1.setCallRecordingToggle("on");
			Common.pause(2);
			driver3.get(url.signIn());
			driver4.get(url.signIn());
		} catch (Exception e) {

			try {
				Common.pause(3);
				driver3.get(url.signIn());
				driver4.get(url.signIn());
				driver5.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2SubUser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}
				if (webApp1.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp1, account1Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}
				
				driver4.get(url.numbersPage());
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Thread.sleep(9000);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Thread.sleep(3000);
				webApp2.setCallRecordingToggle("on");
				

				driver3.get(url.numbersPage());
				webApp1.numberspage();
				webApp1.navigateToNumberSettingpage(number2);
				Thread.sleep(9000);
				webApp1.setCallRecordingToggle("on");
				Common.pause(2);
				driver3.get(url.signIn());
				driver4.get(url.signIn());
				
			} catch (Exception e1) {
				String testname = "Normal calling Before Method ";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
				Common.Screenshot(driver5, testname, "WebAppp2SubUser Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "WebAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "WebAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driver3.quit();
		driver5.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_NO_Anawer_Incoming_Missed() throws Exception {

		driver.get(url.dialerSignIn());
		phoneApp1.verifySdap();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();

//+++++++++++++++++++++++++++++++++++++++++
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		Reporter.log("last dial toggle is ON" + "</br>");

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.getLivCall(), "0");
		assertEquals(webApp1LivePage.getLivCall(), "0");
		driver1.get(url.dialerSignIn());
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		Reporter.log("Dial number from phoneApp1" + "</br>");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		Reporter.log("validateCallingScreenOutgoingVia" + "</br>");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		Reporter.log("validateCallingScreenOutgoingNumber" + "</br>");

		phoneApp2.waitForIncomingCallingScreen();
		Reporter.log("waitForIncomingCallingScreen" + "</br>");

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		Reporter.log("validateCallingScreenIncomingNumber" + "</br>");
		
		// -------------------- start validate Live call --------------
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true);
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		
		assertEquals(webApp2SubUserLivePage.validateCallType("1", "Incoming"), true);
		assertEquals(webApp2SubUserLivePage.validateAgentName("1"), "");
		assertEquals(webApp2SubUserLivePage.validateFrom("1"), number2);
		assertEquals(webApp2SubUserLivePage.validateTo("1"), number);
		assertEquals(webApp2SubUserLivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2SubUserLivePage.getLivCall(), "1");
		
		assertEquals(webApp1LivePage.validateCallType("1", "Outgoing"), true);
		assertEquals(webApp1LivePage.validateAgentName("1"), callerNameOfAccount1);
		assertEquals(webApp1LivePage.validateFrom("1"), number2);
		assertEquals(webApp1LivePage.validateTo("1"), number);
		assertEquals(webApp1LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp1LivePage.getLivCall(), "1");
		// -------------------- End validate Live call --------------

		phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp1LivePage.getLivCall(), "0");
		assertEquals(webApp2LivePage.getLivCall(), "0");
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		} catch (AssertionError  e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		}
		Reporter.log("validateCallStatusInCallDetail" + "</br>");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		Reporter.log("validatecallType" + "</br>");
		
		String callTime = phoneApp2.getTimeFromAllCalls();
		
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "validate  number2 in call log details");
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");
		
		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		try {
		assertEquals("No Answer", webApp1.validateCallStatus());
	    } catch (AssertionError e) {
		assertEquals("Rejected", webApp1.validateCallStatus());
	    }
		System.out.println("WebApp1 Call Status" + webApp1.validateCallStatus()); // ------check 1
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		System.out.println("DONE----priority = 1-----");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		Reporter.log("validateLastCallDetailStatus" + "</br>");

		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		Reporter.log("validateLastCallDetailIncomingIcon" + "</br>");
		
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );

	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Cancelled_Incoming_Missed() throws Exception {

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		phoneApp1.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver.get(url.dialerSignIn());
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		String callTime = phoneApp1.getTimeFromAllCalls();

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Cancelled", phoneApp1.validateCallStatusInCallDetail());
		String status = phoneApp1.validateCallStatusInCallDetail();

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "validate  number2 in call log details");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Cancelled", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		// phoneApp2.waitForIncomingCallingScreen();
		phoneApp1.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
		assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
		assertEquals(phoneApp1.validateLastCallDetailDateAndTime(), callTime );
		phoneApp1.clickOnOutgoingHangupButton();
	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_Rejected() throws Exception {

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();


		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "validate  number in call log details");

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );

	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed() throws Exception {
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
//+++++++++++++++++++++++++++++++++++++++++
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		// validate buttons
//		assertEquals(true, phoneApp1.verifyDisabledMuteHoldForward());
//		assertEquals(true, phoneApp1.validateDisabledNoteButton());
//		assertEquals(true, phoneApp1.validateDisabledDialpadButton());

		phoneApp2.waitForIncomingCallingScreen();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		
		assertEquals(webApp2SubUserLivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2SubUserLivePage.validateAgentName("1"), "");
		assertEquals(webApp2SubUserLivePage.validateFrom("1"), number2);
		assertEquals(webApp2SubUserLivePage.validateTo("1"), number);
		assertEquals(webApp2SubUserLivePage.validateCallStatus("1"), "Ringing");
		
		assertEquals(webApp1LivePage.validateCallType("1", "Outgoing"), true, "validate in outgoing call type icon is display.");
		assertEquals(webApp1LivePage.validateAgentName("1"), callerNameOfAccount1);
		assertEquals(webApp1LivePage.validateFrom("1"), number2);
		assertEquals(webApp1LivePage.validateTo("1"), number);
		assertEquals(webApp1LivePage.validateCallStatus("1"), "Ringing");

		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2SubUserLivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2SubUserLivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2LivePage.getLivCall(), "1");
		
		assertEquals(webApp2SubUserLivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2SubUserLivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2SubUserLivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2SubUserLivePage.validateFrom("1"), number2);
		assertEquals(webApp2SubUserLivePage.validateTo("1"), number);
		webApp2SubUserLivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2SubUserLivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2SubUserLivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(webApp2SubUserLivePage.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(webApp2SubUserLivePage.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(webApp2SubUserLivePage.validateCallWhisper("1"), true, "validate call whishper button is display.");
		

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);
		assertEquals(phoneApp2.isMuteButtonDisplay(), true);
		assertEquals(phoneApp2.isRecordingButtonDisplay(), true);
		assertEquals(phoneApp2.isDialPadButtonDisplay(), true);
		assertEquals(phoneApp2.isForwardButtonDisplay(), true);
		assertEquals(phoneApp2.isHoldButtonDisplay(), true);
		assertEquals(phoneApp2.isNoteButtonDisplay(), true);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();
		
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		

	}

	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_WelcomeMessage() throws Exception {

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();


		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Welcome message", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();

	}

	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Missed_after_WelcomeMessage() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		Thread.sleep(15000);
//		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(3);
//		String durationOnOutgoingCallScreen1 = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// try {
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// } catch (AssertionError e) {
		// assertEquals(durationOnOutgoingCallScreen1 + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// }

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();
		
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed_after_WelcomeMessage() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp1.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		
//		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		webApp2LivePage.waitForChangeCallStatusTo("1", "Welcome Msg");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Welcome Msg");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2.clickOnIncomimgAcceptCallButton();

		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2SubUserLivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2SubUserLivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Thread.sleep(5000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(3);
		String durationOnOutgoingCallScreen1 = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		String callTime = phoneApp1.getTimeFromAllCalls();

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		// assertEquals(number, phoneApp1.validateNumberInCallDetail());
		//
		// try {
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// } catch (AssertionError e) {
		// assertEquals(durationOnOutgoingCallScreen1 + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// }

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		String status = phoneApp1.validateCallStatusInCallDetail();
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		// phoneApp2.waitForIncomingCallingScreen();
		phoneApp1.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
		assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
		assertEquals(phoneApp1.validateLastCallDetailDateAndTime(), callTime );
		phoneApp1.clickOnOutgoingHangupButton();
	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Rejected_after_WelcomeMessage() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		Thread.sleep(10000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();
		
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVRMessage() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("IVR message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("IVR message", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();
	}

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_Reject() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2.clickOnIncomingRejectButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(10000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		// assertEquals("-", webApp2.validateCallStatus(),"----verify call statuts in
		// webapp2---");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		// assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		System.out.println("----status---" + status.replace(" ", "") + "----" + "---phoneApp2---"
				+ phoneApp2.validateLastCallDetailStatus().replace(" ", ""));
		assertTrue(status.replace(" ", "").equalsIgnoreCase(phoneApp2.validateLastCallDetailStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp1.clickOnOutgoingHangupButton();

	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_Missed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		// String durationOnOutgoingCallScreen =
		// phoneApp1.validateDurationInCallingScreen(4);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// softassertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// softassertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertTrue(status.replace(" ", "").equalsIgnoreCase(phoneApp2.validateLastCallDetailStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();

	}

	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_loggedoutUser() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(15000);
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_NumberClosed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "closed", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(15000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_UserClosed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(15000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_AWHM_Voicemail_by_Number_CustomClosed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		
		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_user_busy_with_outgoing_Ringing() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_user_busy_with_Incoming_call_Ringing() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		// phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_user_busy_with_Incoming_call_Ongoing() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_user_busy_with_Outgoing_call_Ongoing() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_user_busy_with_ACW_Screen_is_Running() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(9000);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.clickOnEndACWButton();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_Number_Closed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "closed", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Number is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Number is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();

	}

	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Closed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		phoneApp1.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		String callTime = phoneApp1.getTimeFromAllCalls();

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		String status = phoneApp1.validateCallStatusInCallDetail();
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();

		phoneApp1.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
		assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
		assertEquals(phoneApp1.validateLastCallDetailDateAndTime(), callTime );
		// phoneApp1.clickOnOutgoingHangupButton();

	}

	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_Number_Custom_Closed_AWHM_Greeting_ON() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.clickonAWHMGreeting();
		Thread.sleep(5000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Caller tried to call out of business hours", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Caller tried to call out of business hours", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	}

	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_Outgoing_Ringing() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp1.clickOnOutgoingHangupButton();
	}

	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_incoming_Ringing() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_incoming_call_Ongoing_Screen()
			throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_outgoing_call_Ongoing_Screen()
			throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 28, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_ACW_Screen_is_Running() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 29, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_User_Missed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(5000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals(callerName1, webApp1.validateCallerName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();

	}

	@Test(priority = 30, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_User_Rejected() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(10);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 27-----");
	}

	@Test(priority = 31, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_User_Completed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		
		phoneApp1.validateDurationInCallingScreen(1);

		
		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		
		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();
		
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		webApp2SubUserLivePage.waitForChangeAgentNameTo("1", callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateAgentName("1"), callerNameOfAccount2);
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		webApp2SubUserLivePage.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(webApp2LivePage.validateCallStatus("1"), "On Call");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		Common.pause(5);
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 29-----");
	}

	@Test(priority = 32, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_User_Unavailable() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Unavailable", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 30-----");
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();

	}

	@Test(enabled = false) // (priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_CallNotSetup_by_loggedoutUser() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Call not setup", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
	}

	@Test(priority = 33, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_Number_User_Missed() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 31-----");

	}

	@Test(priority = 34, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_Number_User_Rejected() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 32-----");
	}

	@Test(priority = 35, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_Number_User_Completed() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 33-----");

	}

	@Test(priority = 36, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_Number_closed_CallNotSetup() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "closed", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Number is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();


		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals("Number is not available", webApp2.validateStatusCallNotSetup());
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 34-----");

	}

	@Test(priority = 37, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void lastCallDetail_Incoming_WelcomeMessage_status() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals(callerName1, webApp1.validateCallerName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Welcome message", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp1.clickOnOutgoingHangupButton();

	}

	@Test(priority = 38, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void lastCallDetail_Incoming_IVRMessage_status() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("IVR message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals(callerName1, webApp1.validateCallerName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("IVR message", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp1.clickOnOutgoingHangupButton();
	}

	@Test(priority = 39, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void lastCallDetail_Outgoing_Cancelled_status() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Cancelled", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals(callerName1, webApp1.validateCallerName());
		assertEquals("Cancelled", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();

	}

	@Test(priority = 40, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_User_Custom_Unavailable() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// softassertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not  availble--------");

	}

	@Test(priority = 41, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_VoiceMail_User_Custom_Unavailable() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(15000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// softassertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not  availble--------");
	}

	@Test(priority = 42, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Reject_Incoming_CallNotSetup_by_loggedoutUser() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 42, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Missed() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(5000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		// phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----Verify deparment name in webApp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "----Verify Caller name in webApp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----Verify Call status in webApp1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "----Verify Call type in webApp1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "----Verify Call cost in webApp1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "----Verify call recording _ recording available webApp1 ");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----Verify deparment name in webApp1");
		assertEquals("-", webApp2.validateCallerName(), "----Verify Caller name in webApp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----Verify Call status in webApp1");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----Verify Call type in webApp1");
		assertEquals(callCost1, webApp2.validateCallCost(), "----Verify Call cost in webApp2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----Verify call recording _ recording available webApp2 ");
		System.out.println("All Assert Pass for WEB2");

	}

	@Test(priority = 43, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Rejected() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(5000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(10000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		// phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----Verify deparment name in webApp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "----Verify Caller name in webApp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "-----Verify Call status in webApp1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "-----verify Calltype in webApp1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "-------------Verify Callcost in webApp1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----Validate call recording, Call recording availble");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----Verify deparment name in webApp2");
		assertEquals("-", webApp2.validateCallerName(), "----Verify Caller name in webApp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "-----Verify Call status in webApp2");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "-----verify Calltype in webApp2");
		assertEquals(callCost1, webApp2.validateCallCost(), "-------------Verify Callcost in webApp2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----Validate call recording, Call recording availble");
		System.out.println("All Assert Pass for WEB2");

	}

	@Test(priority = 44, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Missed_after_WelcomeMessage_VoiceMail_Added() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		Thread.sleep(15000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(3);
//		String durationOnOutgoingCallScreen1 = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// try {
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// } catch (AssertionError e) {
		// assertEquals(durationOnOutgoingCallScreen1 + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// }

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 45, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Reject_after_WelcomeMessage_VoiceMail_Added() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(3);
//		String durationOnOutgoingCallScreen1 = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp2.clickOnIncomingRejectButton();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// try {
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// } catch (AssertionError e) {
		// assertEquals(durationOnOutgoingCallScreen1 + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// }

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}

	@Test(priority = 46, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Unavailable() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		Thread.sleep(15000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "---- verify department name in web app1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "_____ verify call status in web app1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "------- verify call type in web app1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "--- verify call cost in web app1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "verify call recording, recording available");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "---- verify department name in web app2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "verify callstatus in web app2");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "verify call type in webapp2");
		assertEquals(callCost1, webApp2.validateCallCost(), "---- verify call cost in web app2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		System.out.println("All Assert Pass for WEB2");

	}

	@Test(priority = 47, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_after_WelcomeMessage_User_Unavailable() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(20000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		loginDialer(phoneApp2, account2Email, account2Password);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// try {
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// } catch (AssertionError e) {
		// assertEquals(durationOnOutgoingCallScreen1 + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// }

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

	}

	@Test(priority = 48, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Missed_after_WelcomeMessage_User_Unavailable() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		loginDialer(phoneApp2, account2Email, account2Password);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// try {
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// } catch (AssertionError e) {
		// assertEquals(durationOnOutgoingCallScreen1 + " Mins",
		// phoneApp1.validateDurationInCallDetail());
		// }

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not  availble--------");

	}

	@Test(priority = 49, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_Number_Number_Custom_Unavailable() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "custom", "off", "off", "off");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3");
		Thread.sleep(25000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "-----Verify deaprtment name in webApp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "-----Verify CAll status in webApp1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "---- Verify Call type in webApp1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------ Verify Call cost in webApp1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "----- verify call recording. recording available");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "-----Verify deaprtment name in webApp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "-----Verify CAll status in webApp2");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---- Verify Call type in webApp2");
		assertEquals(callCost1, webApp2.validateCallCost(), "------ Verify Call cost in webApp2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----- verify call recording. recording available");
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 49-----");

	}
	@Test(priority = 50, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_AWHM_ON_Number_Custom_available() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
		api.addCustomTimeSlot("number", numberID, userID, true);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(CallerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}
	@Test(priority = 51, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed_User_Custom_available() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
        phoneApp2.waitForIncomingCallingScreen();
        phoneApp2.clickOnIncomimgAcceptCallButton();
        Thread.sleep(8000);
        phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// softassertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(CallerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording  availble--------");

	}

	@Test(priority = 52, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_User_Custom_available() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		Common.pause(5);
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 29-----");


	}
	
	@Test(priority = 53, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void LastCallDetails_Last_Call_Outgoing_Completed_make_Outgoing_and_Incoming() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
	
	phoneApp2.lastCallDetailToggle("on");
	Thread.sleep(2000);
	driver1.get(url.dialerSignIn());

	driver4.get(url.numbersPage());
	webApp2.navigateToNumberSettingpage(number);
	Thread.sleep(9000);
	webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
	Thread.sleep(2000);
	driver4.get(url.usersPage());
	webApp2.navigateToUserSettingPage(account2Email);
	Thread.sleep(9000);
	webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
	phoneApp1.enterNumberinDialer(number);
	String departmentName = phoneApp1.validateDepartmentNameinDialpade();
	String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
	phoneApp1.clickOnDialButton();
	// Validate outgoingVia on outgoing calling screen
	assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
	// validate outgoing Number on outgoing calling screen
	assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
	phoneApp2.waitForIncomingCallingScreen();
	phoneApp2.clickOnIncomimgAcceptCallButton();
	String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
	Thread.sleep(9000);
	phoneApp1.clickOnOutgoingHangupButton();
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	String callerName2 = phoneApp2.getCallerName();
	phoneApp2.clickOnAllCalls();
	Thread.sleep(4000);

	// validate Number in All Calls page
	assertEquals(number2, phoneApp2.validateNumberInAllcalls());

	// validate via number in All Calls page
	assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

	// validate call type in all calls page
	assertEquals("Incoming", phoneApp2.validatecallType());

	phoneApp2.clickOnRightArrow();

	// validate Number on call details page
	assertEquals(number2, phoneApp2.validateNumberInCallDetail());

	assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
	String status = phoneApp2.validateCallStatusInCallDetail();
	
	driver1.get(url.dialerSignIn());

	phoneApp1.enterNumberinDialer(number);
	phoneApp1.clickOnDialButton();
	phoneApp2.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	phoneApp2.clickOnIncomimgAcceptCallButton();
	Thread.sleep(2000);
	phoneApp1.clickOnOutgoingHangupButton();
	driver.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	
	Thread.sleep(2000);
	phoneApp2.enterNumberinDialer(number2);
	phoneApp2.clickOnDialButton();
	phoneApp1.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());

}
	@Test(priority = 54, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void LastCallDetails_Last_Call_Outgoing_Cancelled_make_Outgoing_and_Incoming() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
	phoneApp2.lastCallDetailToggle("on");
	Thread.sleep(2000);
	driver1.get(url.dialerSignIn());
	driver4.get(url.numbersPage());
	webApp2.navigateToNumberSettingpage(number);
	Thread.sleep(9000);
	webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
	Thread.sleep(2000);
	driver4.get(url.usersPage());
	webApp2.navigateToUserSettingPage(account2Email);
	Thread.sleep(9000);
	webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
	phoneApp1.enterNumberinDialer(number);
	String departmentName = phoneApp1.validateDepartmentNameinDialpade();
	String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
	phoneApp1.clickOnDialButton();
	// Validate outgoingVia on outgoing calling screen
	assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
	// validate outgoing Number on outgoing calling screen
	assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
	phoneApp2.waitForIncomingCallingScreen();
	Thread.sleep(1000);
	phoneApp1.clickOnOutgoingHangupButton();
	phoneApp1.waitForDialerPage();
	phoneApp1.clickOnSideMenu();
	String callerName = phoneApp1.getCallerName();
	phoneApp1.clickOnAllCalls();
	Thread.sleep(4000);

	// validate Number in All Calls page
	assertEquals(number, phoneApp1.validateNumberInAllcalls());

	// validate via number in All Calls page
	assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

	// validate call type in all calls page
	assertEquals("Outgoing", phoneApp1.validatecallType());

	phoneApp1.clickOnRightArrow();

	// validate Number on call details page
	assertEquals(number, phoneApp1.validateNumberInCallDetail());

	assertEquals("Cancelled", phoneApp1.validateCallStatusInCallDetail());
	String status = phoneApp1.validateCallStatusInCallDetail();
	Thread.sleep(2000);
	
	driver.get(url.dialerSignIn());
    phoneApp1.waitForDialerPage();
	phoneApp1.enterNumberinDialer(number);
	phoneApp1.clickOnDialButton();
	phoneApp2.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	Thread.sleep(2000);
	phoneApp1.clickOnOutgoingHangupButton();
	
	
	driver1.get(url.dialerSignIn());
	
	phoneApp2.waitForDialerPage();
	Thread.sleep(2000);
	phoneApp2.enterNumberinDialer(number2);
	phoneApp2.clickOnDialButton();
	phoneApp1.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	
	
}
	@Test(priority = 55, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void LastCallDetails_Last_Call_Outgoing_Rejected_make_Outgoing_and_Incoming() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
	phoneApp2.lastCallDetailToggle("on");
	Thread.sleep(2000);
	driver1.get(url.dialerSignIn());
	driver4.get(url.numbersPage());
	webApp2.navigateToNumberSettingpage(number);
	Thread.sleep(9000);
	webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
	Thread.sleep(2000);
	driver4.get(url.usersPage());
	webApp2.navigateToUserSettingPage(account2Email);
	Thread.sleep(9000);
	webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
	phoneApp1.enterNumberinDialer(number);
	String departmentName = phoneApp1.validateDepartmentNameinDialpade();
	String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
	phoneApp1.clickOnDialButton();
	// Validate outgoingVia on outgoing calling screen
	assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
	// validate outgoing Number on outgoing calling screen
	assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
	phoneApp2.waitForIncomingCallingScreen();
	phoneApp2.clickOnIncomingRejectButton();
	phoneApp1.waitForDialerPage();
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	String callerName2 = phoneApp2.getCallerName();
	phoneApp2.clickOnAllCalls();
	Thread.sleep(4000);

	// validate Number in All Calls page
	assertEquals(number2, phoneApp2.validateNumberInAllcalls());

	// validate via number in All Calls page
	assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

	// validate call type in all calls page
	assertEquals("Incoming", phoneApp2.validatecallType());

	phoneApp2.clickOnRightArrow();

	// validate Number on call details page
	assertEquals(number2, phoneApp2.validateNumberInCallDetail());

	assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
	String status = phoneApp2.validateCallStatusInCallDetail();
	
	driver1.get(url.dialerSignIn());
	phoneApp1.waitForDialerPage();
	phoneApp1.enterNumberinDialer(number);
	phoneApp1.clickOnDialButton();
	phoneApp2.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	phoneApp2.clickOnIncomingRejectButton();
	Thread.sleep(2000);
	phoneApp1.waitForDialerPage();
	driver.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	
	Thread.sleep(2000);
	phoneApp2.enterNumberinDialer(number2);
	phoneApp2.clickOnDialButton();
	phoneApp1.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	}
	
	@Test(priority = 56, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void LastCallDetails_Last_Call_Outgoing_NoAnswer_make_Outgoing_and_Incoming() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
	phoneApp2.lastCallDetailToggle("on");
	Thread.sleep(2000);
	driver1.get(url.dialerSignIn());
	driver4.get(url.numbersPage());
	webApp2.navigateToNumberSettingpage(number);
	Thread.sleep(9000);
	webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
	Thread.sleep(2000);
	driver4.get(url.usersPage());
	webApp2.navigateToUserSettingPage(account2Email);
	Thread.sleep(9000);
	webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
	phoneApp1.enterNumberinDialer(number);
	String departmentName = phoneApp1.validateDepartmentNameinDialpade();
	String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
	phoneApp1.clickOnDialButton();
	// Validate outgoingVia on outgoing calling screen
	assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
	// validate outgoing Number on outgoing calling screen
	assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
	phoneApp2.waitForIncomingCallingScreen();
	
	phoneApp1.waitForDialerPage();
	phoneApp2.waitForDialerPage();
	phoneApp1.clickOnSideMenu();
	String callerName2 = phoneApp1.getCallerName();
	phoneApp1.clickOnAllCalls();
	Thread.sleep(4000);

	// validate Number in All Calls page
	assertEquals(number, phoneApp1.validateNumberInAllcalls());

	// validate via number in All Calls page
	assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

	// validate call type in all calls page
	assertEquals("Outgoing", phoneApp1.validatecallType());

	phoneApp1.clickOnRightArrow();

	// validate Number on call details page
	assertEquals(number, phoneApp1.validateNumberInCallDetail());
	try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		} catch (AssertionError  e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		}
	String status = phoneApp1.validateCallStatusInCallDetail();
	driver.get(url.dialerSignIn());
	phoneApp1.waitForDialerPage();
	phoneApp1.enterNumberinDialer(number);
	phoneApp1.clickOnDialButton();
	phoneApp2.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	phoneApp2.clickOnIncomingRejectButton();
	Thread.sleep(2000);
	phoneApp1.waitForDialerPage();
	driver1.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	
	Thread.sleep(2000);
	phoneApp2.enterNumberinDialer(number2);
	phoneApp2.clickOnDialButton();
	phoneApp1.waitForIncomingCallingScreen();
	phoneApp1.waitForLastCallDetailPopuppDisplayed();
	assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
	assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
	driver1.get(url.dialerCreditPage());
	phoneApp2.clickOnSideMenu();
	
	}

	
	@Test(priority = 57, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_Rejected_Voicemail_CallHangupWhileScriptIsPlaying() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		//---------------------------------------------------------------------------------------//
				
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

				phoneApp2.clickOnIncomingRejectButton();
		
				String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
				System.out.println(durationOnOutgoingCallScreen);
				
				Thread.sleep(3000);
				phoneApp1.clickOnOutgoingHangupButton();
				phoneApp1.waitForDialerPage();

				Thread.sleep(10000);

				phoneApp1.clickOnSideMenu();
				String callerName1 = phoneApp1.getCallerName();
				phoneApp1.clickOnAllCalls();
				Thread.sleep(4000);
				// validate Number in All Calls page
				assertEquals(number, phoneApp1.validateNumberInAllcalls());

				// validate via number in All Calls page
				assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

				// validate call type in all calls page
				assertEquals("Outgoing", phoneApp1.validatecallType());

				phoneApp1.clickOnRightArrow();

				// validate Number on call details page
				assertEquals(number, phoneApp1.validateNumberInCallDetail());

				// validate call status in Call detail page
				assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

				// validate duration in call Details page
				// assertEquals(durationOnOutgoingCallScreen + " Mins",
				// phoneApp1.validateDurationInCallDetail());

				// validate department name in Call detail page
				assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				phoneApp2.clickOnAllCalls();
				Thread.sleep(4000);
				// validate Number in All Calls page
				assertEquals(number2, phoneApp2.validateNumberInAllcalls());

				// validate via number in All Calls page
				assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

				// validate call type in all calls page
				assertEquals("Incoming", phoneApp2.validatecallType());
				String callTime = phoneApp2.getTimeFromAllCalls();

				phoneApp2.clickOnRightArrow();

				// validate Number on call details page
				assertEquals(number2, phoneApp2.validateNumberInCallDetail());

				// validate duration in call Details page
				// assertEquals(durationOnOutgoingCallScreen + " Mins",
				// phoneApp2.validateDurationInCallDetail());
				assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
				//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
						//"Voicemail status not matched");
				String status = phoneApp2.validateCallStatusInCallDetail();

				assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

				driver4.get(url.signIn());
				assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
				webApp2.clickOnActivitFeed();
				Common.pause(5);
				assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
				// assertEquals("-", webApp2.validateCallStatus(),"----verify call statuts in
				// webapp2---");
				assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
				assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
				assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
				webApp2.clickOniButton();
				Common.pause(5);
				//assertEquals(true, webApp2.validateRecordingUrl());

				driver3.get(url.signIn());
				assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
				webApp1.clickOnActivitFeed();
				Common.pause(5);
				assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
				assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
				assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
				assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
				assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
				webApp1.clickOniButton();
				Common.pause(5);
				assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				driver4.get(url.signIn());
				String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
				String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
				Common.pause(2);
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuPlanAndBilling();
				String settingcreditValueUpdated = webApp2.availableCreditValue();
				String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
				assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
				assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

				driver3.get(url.signIn());
				String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
						outgoingCallPrice);
				String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
						outgoingCallPrice);
				Common.pause(2);
				webApp1.clickLeftMenuDashboard();
				Common.pause(2);
				webApp1.clickLeftMenuPlanAndBilling();
				String settingcreditValueUpdated1 = webApp1.availableCreditValue();
				String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
				assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
				assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

				driver.get(url.dialerSignIn());

				phoneApp1.enterNumberinDialer(number);
				phoneApp1.clickOnDialButton();
				phoneApp2.waitForIncomingCallingScreen();
				phoneApp2.waitForLastCallDetailPopuppDisplayed();
				// assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
				assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
				assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
				assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );

				phoneApp1.clickOnOutgoingHangupButton();
	
				

	}
		
	
	@Test(priority = 58, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_Missed_Voicemail_CallHangupWhileScriptIsPlaying() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		
	
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		//---------------------------------------------------------------------------------------//
				
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

				//phoneApp2.clickOnIncomingRejectButton();
				
				phoneApp2.waitForDialerPage();
				Common.pause(5);
				String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
				System.out.println(durationOnOutgoingCallScreen);
				phoneApp1.clickOnOutgoingHangupButton();
				phoneApp1.waitForDialerPage();

				Thread.sleep(10000);

				phoneApp1.clickOnSideMenu();
				String callerName1 = phoneApp1.getCallerName();
				phoneApp1.clickOnAllCalls();
				Thread.sleep(4000);
				// validate Number in All Calls page
				assertEquals(number, phoneApp1.validateNumberInAllcalls());

				// validate via number in All Calls page
				assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

				// validate call type in all calls page
				assertEquals("Outgoing", phoneApp1.validatecallType());

				phoneApp1.clickOnRightArrow();

				// validate Number on call details page
				assertEquals(number, phoneApp1.validateNumberInCallDetail());

				// validate call status in Call detail page
				assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

				// validate duration in call Details page
				// assertEquals(durationOnOutgoingCallScreen + " Mins",
				// phoneApp1.validateDurationInCallDetail());

				// validate department name in Call detail page
				assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				phoneApp2.clickOnAllCalls();
				Thread.sleep(4000);
				// validate Number in All Calls page
				assertEquals(number2, phoneApp2.validateNumberInAllcalls());

				// validate via number in All Calls page
				assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

				// validate call type in all calls page
				assertEquals("Incoming", phoneApp2.validatecallType());
				String callTime = phoneApp2.getTimeFromAllCalls();

				phoneApp2.clickOnRightArrow();

				// validate Number on call details page
				assertEquals(number2, phoneApp2.validateNumberInCallDetail());

				// validate duration in call Details page
				// assertEquals(durationOnOutgoingCallScreen + " Mins",
				// phoneApp2.validateDurationInCallDetail());
				assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
				//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
						//"Voicemail status not matched");
				String status = phoneApp2.validateCallStatusInCallDetail();

				assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

				driver4.get(url.signIn());
				assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
				webApp2.clickOnActivitFeed();
				Common.pause(5);
				assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
				// assertEquals("-", webApp2.validateCallStatus(),"----verify call statuts in
				// webapp2---");
				assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
				assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
				assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
				webApp2.clickOniButton();
				Common.pause(5);
				//assertEquals(true, webApp2.validateRecordingUrl());

				driver3.get(url.signIn());
				assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
				webApp1.clickOnActivitFeed();
				Common.pause(5);
				assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
				assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
				assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
				assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
				assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
				webApp1.clickOniButton();
				Common.pause(5);
				assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				driver4.get(url.signIn());
				String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
				String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
				Common.pause(2);
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuPlanAndBilling();
				String settingcreditValueUpdated = webApp2.availableCreditValue();
				String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
				assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
				assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

				driver3.get(url.signIn());
				String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
						outgoingCallPrice);
				String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
						outgoingCallPrice);
				Common.pause(2);
				webApp1.clickLeftMenuDashboard();
				Common.pause(2);
				webApp1.clickLeftMenuPlanAndBilling();
				String settingcreditValueUpdated1 = webApp1.availableCreditValue();
				String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
				assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
				assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

				driver.get(url.dialerSignIn());

				phoneApp1.enterNumberinDialer(number);
				phoneApp1.clickOnDialButton();
				phoneApp2.waitForIncomingCallingScreen();
				phoneApp2.waitForLastCallDetailPopuppDisplayed();
				// assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
				assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
				assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
				assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
				phoneApp1.clickOnOutgoingHangupButton();

			
	}
	
	
	@Test(priority=59, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_DialerNotLogin_VoiceMail_CallHangupWhileScriptIsPlaying() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		//---------------------------------------------------------------------------------------//
				
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
	//	String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		
		Common.pause(8);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
		System.out.println(durationOnOutgoingCallScreen);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(6);
		//assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		// assertEquals("-", webApp2.validateCallStatus(),"----verify call statuts in
		// webapp2---");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		//phoneApp2.waitForIncomingCallingScreen();
		//phoneApp2.waitForLastCallDetailPopuppDisplayed();
		// assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		//assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		//assertEquals(true, phoneApp1.validateLastCallDetailIncomingIcon());
		phoneApp1.clickOnOutgoingHangupButton();
		
		
	}
	
	@Test(priority=60, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_Number_Always_close_VoiceMail_CallHangupWhileScriptIsPlaying() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "closed", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(8);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());
	assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
		//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
		//	"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Number is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	
	@Test(priority=61, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_NumberCustomClosed_Voicemail_CallHangupWhileScriptIsPlaying() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		//---------------------------------------------------------------------------------------//
				
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "custom", "off", "off", "off");
		Thread.sleep(2000);
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		//phoneApp2.waitForIncomingCallingScreen();
		
		// Validate IncomingVia on Incoming calling screen
				//assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				//assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

				//phoneApp2.clickOnIncomingRejectButton();
				
				//phoneApp2.waitForDialerPage();
				Common.pause(8);
				String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);
				System.out.println(durationOnOutgoingCallScreen);
				phoneApp1.clickOnOutgoingHangupButton();
				phoneApp1.waitForDialerPage();

				Thread.sleep(10000);

				phoneApp1.clickOnSideMenu();
				String callerName1 = phoneApp1.getCallerName();
				phoneApp1.clickOnAllCalls();
				Thread.sleep(4000);
				// validate Number in All Calls page
				assertEquals(number, phoneApp1.validateNumberInAllcalls());

				// validate via number in All Calls page
				assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

				// validate call type in all calls page
				assertEquals("Outgoing", phoneApp1.validatecallType());

				phoneApp1.clickOnRightArrow();

				// validate Number on call details page
				assertEquals(number, phoneApp1.validateNumberInCallDetail());

				// validate call status in Call detail page
				assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

				// validate duration in call Details page
				// assertEquals(durationOnOutgoingCallScreen + " Mins",
				// phoneApp1.validateDurationInCallDetail());

				// validate department name in Call detail page
				assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				phoneApp2.clickOnAllCalls();
				Thread.sleep(4000);
				// validate Number in All Calls page
				assertEquals(number2, phoneApp2.validateNumberInAllcalls());

				// validate via number in All Calls page
				assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

				// validate call type in all calls page
				assertEquals("Incoming", phoneApp2.validatecallType());

				phoneApp2.clickOnRightArrow();

				// validate Number on call details page
				assertEquals(number2, phoneApp2.validateNumberInCallDetail());

				// validate duration in call Details page
				// assertEquals(durationOnOutgoingCallScreen + " Mins",
				// phoneApp2.validateDurationInCallDetail());
				assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
				//assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
						//"Voicemail status not matched");
				String status = phoneApp2.validateCallStatusInCallDetail();

				assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

				driver4.get(url.signIn());
				assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
				webApp2.clickOnActivitFeed();
				Common.pause(5);
				assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
				// assertEquals("-", webApp2.validateCallStatus(),"----verify call statuts in
				// webapp2---");
				assertEquals("Number is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
				assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
				assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
				webApp2.clickOniButton();
				Common.pause(5);
				//assertEquals(true, webApp2.validateRecordingUrl());

				driver3.get(url.signIn());
				assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
				webApp1.clickOnActivitFeed();
				Common.pause(5);
				assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
				assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
				assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
				assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
				assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
				webApp1.clickOniButton();
				Common.pause(5);
				assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				driver4.get(url.signIn());
				String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
				String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
				Common.pause(2);
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuPlanAndBilling();
				String settingcreditValueUpdated = webApp2.availableCreditValue();
				String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
				assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
				assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

				driver3.get(url.signIn());
				String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
						outgoingCallPrice);
				String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
						outgoingCallPrice);
				Common.pause(2);
				webApp1.clickLeftMenuDashboard();
				Common.pause(2);
				webApp1.clickLeftMenuPlanAndBilling();
				String settingcreditValueUpdated1 = webApp1.availableCreditValue();
				String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
				assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
				assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

				driver.get(url.dialerSignIn());

				phoneApp1.enterNumberinDialer(number);
				phoneApp1.clickOnDialButton();
				//phoneApp2.waitForIncomingCallingScreen();
				//phoneApp2.waitForLastCallDetailPopuppDisplayed();
				// assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
				//assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
				//assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
				phoneApp1.clickOnOutgoingHangupButton();
		
	}

	
	@Test(priority=62, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_NumberCustomClosed_AWHMGreeting_CallHangupWhileAWHMGreetingScriptIsPlaying() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.clickonAWHMGreeting();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		
		webApp2LivePage.waitForChangeCallStatusTo("1", "AWH Greeting");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "AWH Greeting");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
//		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		
		
//		Common.pause(8);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("Caller tried to call out of business hours", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Call not setup", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	@Test(priority=63, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_NumberCustomClosed_AWHMVoicemail_CallHangupWhileAWHMVoicemailScriptIsPlaying() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("Caller tried to call out of business hours", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Caller tried to call out of business hours", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5); 
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0.00");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,"0.00");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	@Test(priority=64, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_User_Always_Closed_Voicemail_Call_Hangup_While_VoicemailScript_Is_Playing() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
//		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
//		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(8);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0.00");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,"0.00");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	@Test(priority=65, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_User_Custom_Closed_Voicemail_Call_Hangup_While_VoicemailScript_Is_Playing() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
//		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
//		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(8);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0.00");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,"0.00");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	
	@Test(priority=66, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_IVR_Press_1_USer_Unavailable_Number_Voicemail_Call_hangup_while_voicemail_script_is_playing() throws Exception{

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
//		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
//		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(8);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();
		
		Common.pause(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Unavailable", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	@Test(priority=67, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_IVR_Press_1_USer_Unavailable_User_Voicemail_Call_hangup_while_voicemail_script_is_playing() throws Exception{
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
//		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
//		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(8);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();
		
		Common.pause(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Unavailable", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		//assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	@Test(priority = 68, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_Reject_recording_TOggle_OFF() throws Exception {

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++

		driver3.get(url.numbersPage());
		webApp1.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp1.setCallRecordingToggle("off");
		driver4.get(url.numbersPage());
		
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
        Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	@Test(priority = 69, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void call_Recording_OFF_Outgoing_Completed_Incoming_Completed() throws Exception {
		// ++++++++++++++++++++++++++++++++++++++
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		driver4.get(url.signIn());
		
		driver3.get(url.numbersPage());
		webApp1.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp1.setCallRecordingToggle("off");
		driver3.get(url.signIn());
		
		
		
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
//+++++++++++++++++++++++++++++++++++++++++
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		// validate buttons
//		assertEquals(true, phoneApp1.verifyDisabledMuteHoldForward());
//		assertEquals(true, phoneApp1.validateDisabledNoteButton());
//		assertEquals(true, phoneApp1.validateDisabledDialpadButton());
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
	
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(9000);
//		assertEquals(false, phoneApp1.verifyDisabledMuteHoldForward());
//		assertEquals(false, phoneApp1.validateDisabledNoteButton());
//		assertEquals(false, phoneApp1.validateDisabledDialpadButton());
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
	
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		driver3.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl());
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	}
	
	@Test(priority = 80, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Missed_Number_Voicemail_ON_User_Voicemail_OFF() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(5000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		// phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----Verify deparment name in webApp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "----Verify Caller name in webApp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----Verify Call status in webApp1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "----Verify Call type in webApp1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "----Verify Call cost in webApp1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "----Verify call recording _ recording available webApp1 ");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----Verify deparment name in webApp1");
		assertEquals("-", webApp2.validateCallerName(), "----Verify Caller name in webApp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----Verify Call status in webApp1");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----Verify Call type in webApp1");
		assertEquals(callCost1, webApp2.validateCallCost(), "----Verify Call cost in webApp2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----Verify call recording _ recording available webApp2 ");
		System.out.println("All Assert Pass for WEB2");

	}

	@Test(priority = 81, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Rejected_Number_Voicemail_ON_User_Voicemail_OFF() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(5000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Ringing");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Ringing");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(10000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		// phoneApp1.waitForDialerPage();

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----Verify deparment name in webApp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "----Verify Caller name in webApp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "-----Verify Call status in webApp1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "-----verify Calltype in webApp1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "-------------Verify Callcost in webApp1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----Validate call recording, Call recording availble");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----Verify deparment name in webApp2");
		assertEquals("-", webApp2.validateCallerName(), "----Verify Caller name in webApp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "-----Verify Call status in webApp2");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "-----verify Calltype in webApp2");
		assertEquals(callCost1, webApp2.validateCallCost(), "-------------Verify Callcost in webApp2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----Validate call recording, Call recording availble");
		System.out.println("All Assert Pass for WEB2");

	}
	
	@Test(priority = 82, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Logout_Number_Voicemail_ON_User_Voicemail_OFF() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		Thread.sleep(15000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "---- verify department name in web app1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "_____ verify call status in web app1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "------- verify call type in web app1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "--- verify call cost in web app1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "verify call recording, recording available");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "---- verify department name in web app2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "verify callstatus in web app2");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "verify call type in webapp2");
		assertEquals(callCost1, webApp2.validateCallCost(), "---- verify call cost in web app2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		System.out.println("All Assert Pass for WEB2");

	}
	
	@Test(priority = 83, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_IVR_User_Unavailable_Number_Voicemail_ON_User_Voicemail_OFF() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		driver.navigate().to(url.dialerSignIn());
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);
		
//		phoneApp2.clickOnSideMenu();
//		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "IVR");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "IVR");
		assertEquals(webApp2LivePage.validateHangupButton("1"), true, "validate hangup button is display.");

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		Thread.sleep(15000);
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Voicemail");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Voicemail");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2SubUserLivePage.validateThereIsNoLiveCall(), true);

		Thread.sleep(10000);

//		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "---- verify department name in web app1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "_____ verify call status in web app1");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed(), "------- verify call type in web app1");
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "--- verify call cost in web app1");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "verify call recording, recording available");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "---- verify department name in web app2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "verify callstatus in web app2");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "verify call type in webapp2");
		assertEquals(callCost1, webApp2.validateCallCost(), "---- verify call cost in web app2");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		System.out.println("All Assert Pass for WEB2");

	}
	
	@Test(priority = 84, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_valid_music_file_uploaded() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.clickonsidemenuMusiAndMessages();
		webApp2.selectMessageType("Welcome message","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Welcome message", "MP3-245KB.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Welcome message", "MP3-245KB.mp3");
		
		webApp2.uploadMessageFile("Welcome message", "MP3-4_80MB.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Welcome message", "MP3-4_80MB.mp3");
		
		webApp2.uploadMessageFile("Welcome message", "WAV_2MG.wav");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Welcome message", "WAV_2MG.wav");
		
		webApp2.uploadMessageFile("Welcome message", "WAV-1_32MB.wav");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Welcome message", "WAV-1_32MB.wav");
		
		webApp2.uploadMessageFile("Welcome message", "WAV_5MG.wav");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Welcome message", "WAV_5MG.wav");
		System.out.println("----Done----");
	}
	
	@Test(priority = 85, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void check_validation_for_invalid_music_file_uploaded() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.clickonsidemenuMusiAndMessages();
		webApp2.selectMessageType("Welcome message","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Welcome message", "MP3-6_6MB.mp3");
		assertEquals(webApp2.geErrorFileUploadMsg(), "File is too large, max size is 5MB.");
		
		webApp2.uploadMessageFile("Welcome message", "templetd.xlsx");
		assertEquals(webApp2.geErrorFileUploadMsg(), "File is invalid, supported files are .mp3, .mpeg and .wav");
		
		webApp2.uploadMessageFile("Welcome message", "MP4-1_5MG.mp4");
		assertEquals(webApp2.geErrorFileUploadMsg(), "File is invalid, supported files are .mp3, .mpeg and .wav");
		
		System.out.println("----Done----");
	}
	
	@Test(priority = 86, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_WelcomeMessage_with_music() throws Exception {
		System.out.println("---------method started--------");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.clickonsidemenuMusiAndMessages();
		Thread.sleep(2000);
		webApp2.selectMessageType("Welcome message","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Welcome message", "MP3-245KB.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Welcome message", "MP3-245KB.mp3");
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();


		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Welcome message", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();
	}
	
	@Test(priority = 87, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Missed_after_WelcomeMessage_VoiceMail_Added_with_musicfile() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.selectMessageType("Voicemail","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Voicemail", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Voicemail", "butler_voicemail.mp3");
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		Thread.sleep(2000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(3);
//		String durationOnOutgoingCallScreen1 = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}
	
	@Test(priority = 88, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVRMessage_Added_with_musicfile() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		webApp2.selectMessageType("IVR","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("IVR", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("IVR", "butler_voicemail.mp3");
		
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("IVR message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("IVR message", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail);
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		// phoneApp1.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		assertEquals(phoneApp2.validateLastCallDetailDateAndTime(), callTime );
		phoneApp2.clickOnOutgoingHangupButton();
	}
	
	@Test(priority = 89, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_AWHM_Voicemail_by_Number_CustomClosed_with_AWHM_Voicemail_music_file() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		webApp2.clickonAWHMVoicemail();
		Thread.sleep(2000);
		webApp2.selectMessageType("After Work Hours Message","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("After Work Hours Message", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("After Work Hours Message", "butler_voicemail.mp3");
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp1.validateDurationInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		
		// validate duration in call Details page
		// assertEquals(durationOnOutgoingCallScreen + " Mins",
		// phoneApp2.validateDurationInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
	}
	
	@Test(priority = 90, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_CallNotSetup_by_Number_CustomClosed_with_AWHM_Greetings_music_file() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");

		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

//+++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.clickonAWHMGreeting();
		Thread.sleep(5000);
		webApp2.selectMessageType("After Work Hours Message","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("After Work Hours Message", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("After Work Hours Message", "butler_voicemail.mp3");
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Caller tried to call out of business hours", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Caller tried to call out of business hours", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
	}
	
	@Test(priority = 91, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_after_WelcomeMessage_User_Unavailable_with_music_file() throws Exception {

		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.selectMessageType("Voicemail","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Voicemail", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Voicemail", "butler_voicemail.mp3");
		
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
		webApp2.selectMessageType("Voicemail","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("Voicemail", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("Voicemail", "butler_voicemail.mp3");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(20000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);
		loginDialer(phoneApp2, account2Email, account2Password);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "-----verify call recording- recording availble--------");

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");

	}
	
	@Test(priority = 92, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Number_Custom_Unavailable_Voicemail_ON_Transcription_ON_AWHM_ON_Voicemail_With_Text_of_AWHM() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
	//----------------------------------------------------------------
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "on", "on", "custom", "off", "off", "off");
		
		webApp2.clickonAWHMVoicemail();
		Thread.sleep(5000);
		webApp2.selectMessageType("After Work Hours Message","Text");
		Common.pause(3);
	
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		//phoneApp2.waitForIncomingCallingScreen();
		Thread.sleep(20000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		
		assertEquals("Voice Mail", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		//-----------------------------------------------------------------------
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
	}
	
	@Test(priority = 93, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Number_Custom_Unavailable_Voicemail_ON_Transcription_ON_AWHM_ON_Voicemail_With_music_of_AWHM() throws Exception {
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
	//----------------------------------------------------------------
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "on", "on", "custom", "off", "off", "off");
		
		webApp2.clickonAWHMVoicemail();
		Thread.sleep(5000);
		webApp2.selectMessageType("After Work Hours Message","Music");
		Common.pause(3);
		webApp2.uploadMessageFile("After Work Hours Message", "butler_voicemail.mp3");
		assertEquals(webApp2.validateSuccessMessage(), "Your file has been uploaded successfully");
		webApp2.validateNumSettingFileUploadedSuccessfully("After Work Hours Message", "butler_voicemail.mp3");
	
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		//phoneApp2.waitForIncomingCallingScreen();
		Thread.sleep(20000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		
		assertEquals("Voice Mail", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		
		//-----------------------------------------------------------------------
		
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				outgoingCallPrice);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				outgoingCallPrice);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
	}
	//************************************************************************************
	//custom Ringing time.
	@Test(priority = 94, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void check_validation_for_invalid_custom_Ringing_duration_And_verify_by_set_custom_incoming_Ringing_time_between_10_to_60() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(6);
		webApp2.enterDuration_customRingingTime("9");
		assertEquals(webApp2.validationErrorMessage(), "Value should not be less than 10.");
		
		Common.pause(5);
		webApp2.enterDuration_customRingingTime("65");
		assertEquals(webApp2.validationErrorMessage(), "Value should not be greater than 60.");
	
		System.out.println("----verify validation DONE for invalid custom Ringing duration----");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("20");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "20");
	
		// ----------------------------------------------------
		driver.get(url.dialerSignIn());
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);
		
	
//+++++++++++++++++++++++++++++++++++++++++
		
		phoneApp1.clickOnSideMenu();
		String callerNameOfAccount1 = phoneApp1.getCallerName();
		
		driver.get(url.dialerSignIn());
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		Reporter.log("Dial number from phoneApp1" + "</br>");

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		Reporter.log("validateCallingScreenOutgoingVia" + "</br>");

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		Reporter.log("validateCallingScreenOutgoingNumber" + "</br>");

		phoneApp2.waitForIncomingCallingScreen();
		Reporter.log("waitForIncomingCallingScreen" + "</br>");

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
	
		Reporter.log("validateCallingScreenIncomingNumber" + "</br>");
		
        Common.pause(22); // 20 sec ringing
    	assertEquals(phoneApp2.validateDialerScreen(),true,"--validate dialer screen after completed custom incoming Ringing time--");
    	assertEquals(phoneApp1.validateDialerScreen(),true,"--validate dialer screen after completed custom incoming Ringing time--");
	

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		Reporter.log("validatecallType" + "</br>");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		} catch (AssertionError  e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		}
		Reporter.log("validateCallStatusInCallDetail" + "</br>");

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");

		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		Reporter.log("validateNumberInAllcalls" + "</br>");

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		Reporter.log("validateViaNumberInAllCalls" + "</br>");

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		Reporter.log("validatecallType" + "</br>");
		
		String callTime = phoneApp2.getTimeFromAllCalls();
		
		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "validate  number2 in call log details");
		Reporter.log("validateCallingScreenIncomingVia" + "</br>");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		Reporter.log("validateNumberInCallDetail" + "</br>");

		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Reporter.log("validateDepartmentNameinCallDetail" + "</br>");
		
		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		try {
		assertEquals("No Answer", webApp1.validateCallStatus());
	    } catch (AssertionError e) {
		assertEquals("Rejected", webApp1.validateCallStatus());
	    }
		System.out.println("WebApp1 Call Status" + webApp1.validateCallStatus()); // ------check 1
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		
		System.out.println("DONE----priority = 94-----");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driver3.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Common.pause(2);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		
	}
	
	@Test(priority = 95, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_by_set_custom_incoming_Ringing_time_for_user_IVR() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("25");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "25");
		
		//--------------------------------------------------------------------
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(5000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1(); // user

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		// phoneApp1.waitForDialerPage();
		Common.pause(27); // 27 sec ringing

		assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		String callTime = phoneApp2.getTimeFromAllCalls();

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals(callerName1, webApp1.validateCallerName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");

	}
	@Test(priority = 96, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_by_set_custom_incoming_Ringing_time_for_Number_IVR() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("30");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "30");
		//------------------------------------------------------------------------
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("3"); // number

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		// phoneApp1.waitForDialerPage();
		Common.pause(32); // 30 sec ringing

		assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		String callDurationOnCallDetail = phoneApp2.validateDurationInCallDetail();

		driver3.get(url.signIn());
		assertEquals(webApp1.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driver4.get(url.signIn());
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0","Validate Free Incoming Minutes on Dashboard.");
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		assertEquals(webApp2.validateDuration(), callDurationOnCallDetail, "validate call duration");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		
	}
}



