package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
//import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.AbstractPage;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.numberAndUserSubscription.InviteUserPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class InternalTeamCallingTest {
	DialerIndex phoneApp2;
	WebDriver driverPhoneApp2;
	DialerIndex phoneApp2Subuser;
	WebDriver driverPhoneApp2Subuser;
	WebToggleConfiguration webApp2;
	WebDriver driverWebApp2;
	WebToggleConfiguration webApp2Subuser;
	WebDriver driverWebApp2Subuser;
	DialerIndex phoneApp3;
	WebDriver driverPhoneApp3;
	WebDriver drivergmail;
	InviteUserPage inviteUserPageObj;
	Common signUpExcel;
	RestAPI api;
	HolidayPage webApp2HolidayPage;
	CallingCallCostTest callingCallCostobj;

	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2SubUserEmail = data.getValue("account2SubEmail");
	String account2Password = data.getValue("masterPassword");
	String account2Number1 = data.getValue("account2Number1"); // account 2's number
	String account2Number2 = data.getValue("account2Number2");
	String account1Number1 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String account3Number1 = data.getValue("account3Number1"); // account 3's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	RestAPI creditAPI;

	String callerName2 = null;
	String callerName2Sub = null;
	// String userID;
	String gmailUser = data.getValue("gmailUser");
	String userID;
	String subuserID;
	String numberID;
	
	Common excelTestResult;
	String outgoingCallPrice;
	public InternalTeamCallingTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj = new CallingCallCostTest();
	}
	

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driverWebApp2Subuser = TestBase.init();
		System.out.println("Opened webaap2 session");
		webApp2Subuser = PageFactory.initElements(driverWebApp2Subuser, WebToggleConfiguration.class);

		driverWebApp2 = TestBase.init();
		System.out.println("Opened webaap1 session");
		webApp2 = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driverWebApp2, HolidayPage.class);

		driverPhoneApp2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);

		driverPhoneApp2Subuser = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2Subuser = PageFactory.initElements(driverPhoneApp2Subuser, DialerIndex.class);

		driverPhoneApp3 = TestBase.init_dialer();
		System.out.println("Opened PhoneApp3 session");
		phoneApp3 = PageFactory.initElements(driverPhoneApp3, DialerIndex.class);

		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPrice=callingCallCostobj.callCost(account2Number1,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPrice);
		
		// drivergmail = TestBase.init();
		try {
			try {
				driverWebApp2Subuser.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2Subuser, account2SubUserEmail, account2Password);
			} catch (Exception e) {
				driverWebApp2Subuser.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2Subuser, account2SubUserEmail, account2Password);
			}

			try {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account1Password);
			} catch (Exception e) {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account1Password);
			}

			try {
				driverPhoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp2, account2Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driverPhoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp2, account2Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				driverPhoneApp2Subuser.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2Subuser, account2SubUserEmail, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driverPhoneApp2Subuser.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2Subuser, account2SubUserEmail, account2Password);
				System.out.println("loggedin phoneAap2");
			}

			try {
				driverPhoneApp3.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account1Email, account1Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driverPhoneApp3.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account1Email, account1Password);
				System.out.println("loggedin phoneAap3");
			}
			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverPhoneApp2.get(url.dialerSignIn());
				phoneApp2Subuser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Subuser.getCallerName();
				driverPhoneApp2Subuser.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(account2Number1);
				String Url1 = driverWebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(3000);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(account2Number1);

				String Url = driverWebApp2.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				System.out.println("\n\njayadip user id :: "+userID+"\n\n");
				creditAPI.UpdateCredit("500", "0", "0", userID);
				System.out.println("\n\njayadip user id :: after perform api "+"\n\n");
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2SubUserEmail);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(account2Number1);
				String subuserUrl = driverWebApp2.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverPhoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverPhoneApp2.get(url.dialerSignIn());

				phoneApp3.afterCallWorkToggle("off");
				driverPhoneApp3.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverPhoneApp2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			} catch (Exception e) {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverPhoneApp2.get(url.dialerSignIn());

				phoneApp2Subuser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Subuser.getCallerName();
				driverPhoneApp2Subuser.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(account2Number1);
				String Url1 = driverWebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(account2Number1);

				String Url = driverWebApp2.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				creditAPI.UpdateCredit("500", "0", "0", userID);
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2SubUserEmail);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(account2Number1);
				String subuserUrl = driverWebApp2.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverPhoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverPhoneApp2.get(url.dialerSignIn());

				phoneApp3.afterCallWorkToggle("off");
				driverPhoneApp3.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverPhoneApp2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}
		} catch (Exception e) {
			String testname = "Internal Team calling Before test ";
			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driverPhoneApp2Subuser, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driverWebApp2, testname, "webAap1 Fail login");
			Common.Screenshot(driverWebApp2Subuser, testname, "webAap2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			creditAPI.UpdateCredit("500", "0", "0", userID);
		
			Common.pause(2);

			driverPhoneApp2.navigate().to(url.dialerSignIn());
			driverPhoneApp2Subuser.navigate().to(url.dialerSignIn());
			driverPhoneApp3.navigate().to(url.dialerSignIn());
			driverWebApp2.get(url.signIn());
			driverWebApp2Subuser.get(url.signIn());
			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2Subuser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2Subuser, account2SubUserEmail, account2Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2SubUserEmail, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account1Email, account1Password);
				Common.pause(3);
			}
			driverWebApp2.get(url.usersPage());
			Common.pause(3);
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			// driverWebApp2.getCurrentUrl();
			String Url = driverWebApp2.getCurrentUrl();
			System.out.println(webApp2.getUserId(Url));
			String userID = webApp2.getUserId(Url);
			creditAPI.UpdateCredit("500", "0", "0", userID);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Thread.sleep(2000);
			driverWebApp2.get(url.usersPage());
			Common.pause(2);
			webApp2.navigateToUserSettingPage(account2SubUserEmail);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(account2Number1);
			Common.pause(9);
			webApp2.setCallRecordingToggle("on");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Common.pause(3);
			webApp2.customRingingTime("off"); 
			System.out.println("customRingingTime OFF");
			Common.pause(3);

		} catch (Exception e) {

			try {
				Common.pause(3);
				driverPhoneApp2.navigate().to(url.dialerSignIn());
				driverPhoneApp2Subuser.navigate().to(url.dialerSignIn());
				driverPhoneApp3.navigate().to(url.dialerSignIn());
				driverWebApp2.get(url.signIn());
				driverWebApp2Subuser.get(url.signIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (webApp2Subuser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2Subuser, account2SubUserEmail, account2Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Subuser, account2SubUserEmail, account2Password);
					Common.pause(3);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account1Email, account1Password);
					Common.pause(3);
				}
				driverWebApp2.get(url.usersPage());
				Common.pause(3);
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Thread.sleep(2000);
				driverWebApp2.get(url.usersPage());
				Common.pause(2);
				webApp2.navigateToUserSettingPage(account2SubUserEmail);
				Common.pause(9);
				// driverWebApp2.getCurrentUrl();
				String Url = driverWebApp2.getCurrentUrl();
				System.out.println(webApp2.getUserId(Url));
				String userID = webApp2.getUserId(Url);
				creditAPI.UpdateCredit("500", "0", "0", userID);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(account2Number1);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);

			} catch (Exception e1) {
				String testname = "Internal team calling Before Method ";
				Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driverPhoneApp2Subuser, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driverWebApp2, testname, "webAap1 Fail login");
				Common.Screenshot(driverWebApp2Subuser, testname, "webAap2 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2Subuser, testname,
					"PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "webApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2Subuser, testname,
					"webApp2Subuser Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
			

		} else {
			String testname = "Pass";
			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2Subuser, testname,
					"PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "webApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2Subuser, testname, "webAap2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() throws IOException {
		try {

			driverPhoneApp2.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
			driverPhoneApp2.get(url.dialerSignIn());
			creditAPI.UpdateCredit("500", "0", "0", userID);
		} catch (Exception e) {
			driverPhoneApp2.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
			driverPhoneApp2.get(url.dialerSignIn());
			creditAPI.UpdateCredit("500", "0", "0", userID);
		}finally {
		driverPhoneApp2.quit();
		driverPhoneApp2Subuser.quit();
		driverPhoneApp3.quit();
		driverWebApp2.quit();
		driverWebApp2Subuser.quit();
		}
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin(); 
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_Completed_Incoming_SubUser_Completed() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
		Common.pause(2);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		String acc2MainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
		phoneApp2Subuser.clickOnSideMenu();
		String acc2SubUser1CallerName = phoneApp2Subuser.getCallerName();
		Common.pause(2);
		phoneApp2Subuser.clickOnSideMenuDialPad();
		
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");
		assertEquals(phoneApp2.validateExtCallOutgoingNameCallingScreen(), acc2SubUser1CallerName);
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);
		
		
		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);
		Common.pause(2);
		assertEquals(phoneApp2Subuser.validateExtCallIncomingNameCallingScreen(), acc2MainUserCallerName);
		assertEquals(phoneApp2Subuser.validateSubUserIncomingNumberCallingScreen(), account2MainUserExtension);
		assertEquals(phoneApp2Subuser.validateCallDurationonDisplayOnCallingScreen(), true);
		assertEquals(phoneApp2Subuser.ExtOutgoingScreenRecordingButtonIsDisabled(), true);
		assertEquals(phoneApp2Subuser.ExtOutgoingScreenForwardButtonIsDisabled(), true);
		
		Thread.sleep(8000);

		assertEquals(phoneApp2.validateCallDurationonDisplayOnCallingScreen(), true);
		assertEquals(phoneApp2.ExtOutgoingScreenRecordingButtonIsDisabled(), true);
		assertEquals(phoneApp2.ExtOutgoingScreenForwardButtonIsDisabled(), true);
		
		phoneApp2.insertCallNotes("Mainuser call Notes..");
		Common.pause(2);
		phoneApp2Subuser.insertCallNotes("Subuser call Notes..");
		Common.pause(2);
		
		phoneApp2Subuser.clickOnIncomingHangupButton();
		Common.pause(3);
		assertEquals(false, phoneApp2Subuser.validateACWNoteVisible());
		phoneApp2Subuser.waitForDialerPage();
		
		assertEquals(false, phoneApp2.validateACWNoteVisible());
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		assertEquals(phoneApp2.callDetailSMSIconNotVisible(), true);
		
		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		assertEquals(phoneApp2.callDetailSMSIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		assertEquals(phoneApp2Subuser.callDetailSMSIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");
		webApp2.clickOnCallNotesBtn(2);
		Common.pause(2);
		assertEquals("Subuser call Notes..", webApp2.getCallNotesText(1));

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		webApp2.clickOnCallNotesBtn(1);
		Common.pause(2);
		assertEquals("Mainuser call Notes.", webApp2.getCallNotesText(1));

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		webApp2Subuser.clickOnCallNotesBtn(1);
		Common.pause(2);
		assertEquals("Subuser call Notes..", webApp2Subuser.getCallNotesText(1));

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);
		
	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_SubUser_Completed_Incoming_MainUser_Completed() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
		System.out.println(account2SubUserExtension);

		phoneApp2.clickOnSideMenu();
		String acc2MainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
		phoneApp2Subuser.clickOnSideMenu();
		String acc2SubUser1CallerName = phoneApp2Subuser.getCallerName();
		Common.pause(2);
		phoneApp2Subuser.clickOnSideMenuDialPad();
		
		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2Subuser.clickOnDialButton();

		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");
		assertEquals(phoneApp2Subuser.validateExtCallOutgoingNameCallingScreen(), acc2MainUserCallerName);
		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);

		Common.pause(1);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(2);
		assertEquals(phoneApp2.validateExtCallIncomingNameCallingScreen(), acc2SubUser1CallerName);
		assertEquals(phoneApp2.validateSubUserIncomingNumberCallingScreen(), account2SubUserExtension);
		assertEquals(phoneApp2.validateCallDurationonDisplayOnCallingScreen(), true);
		assertEquals(phoneApp2.ExtOutgoingScreenRecordingButtonIsDisabled(), true);
		assertEquals(phoneApp2.ExtOutgoingScreenForwardButtonIsDisabled(), true);
		
		Thread.sleep(8000);

		assertEquals(phoneApp2Subuser.validateCallDurationonDisplayOnCallingScreen(), true);
		assertEquals(phoneApp2Subuser.ExtOutgoingScreenRecordingButtonIsDisabled(), true);
		assertEquals(phoneApp2Subuser.ExtOutgoingScreenForwardButtonIsDisabled(), true);
		
		phoneApp2.insertCallNotes("Mainuser call Notes..");
		Common.pause(2);
		phoneApp2Subuser.insertCallNotes("Subuser call Notes..");
		Common.pause(2);
		
		phoneApp2Subuser.clickOnOutgoingHangupButton();
		Common.pause(3);
		assertEquals(false, phoneApp2Subuser.validateACWNoteVisible());
		phoneApp2Subuser.waitForDialerPage();
		
		assertEquals(false, phoneApp2.validateACWNoteVisible());
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		
		phoneApp2.clickOnRightArrowFor2ndEntry();
		
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		assertEquals(phoneApp2.callDetailSMSIconNotVisible(), true);
		
		phoneApp2.clickOnBackOnCallDetailPage();
		
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		assertEquals(phoneApp2.callDetailSMSIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail());
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		assertEquals(phoneApp2Subuser.callDetailSMSIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");
		webApp2.clickOnCallNotesBtn(2);
		Common.pause(2);
		assertEquals("Mainuser call Notes.", webApp2.getCallNotesText(1));
		
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		webApp2.clickOnCallNotesBtn(1);
		Common.pause(2);
		assertEquals("Subuser call Notes..", webApp2.getCallNotesText(1));
		
		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		webApp2Subuser.clickOnCallNotesBtn(1);
		Common.pause(2);
		assertEquals("Subuser call Notes..", webApp2Subuser.getCallNotesText(1));
		
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);
	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)

	public void InternalTeamCalling_Outgoing_MainUser_Completed_Incoming_SubUser_Completed_QueueCalled_ServedToUSer()
			throws Exception {
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "on");
		webApp2.enterCallQueueDuration("300");

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);
		phoneApp3.enterNumberinDialer(account2Number1);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2Subuser.waitForDialerPage();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2Subuser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2Subuser.waitForDialerPage();
			phoneApp2Subuser.clickOnSideMenu();
			callerName2Sub = phoneApp2Subuser.getCallerName();

		} else {
			phoneApp2Subuser.waitForIncomingCallingScreen();
			assertEquals(account1Number1, phoneApp2Subuser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName, phoneApp2Subuser.validateCallingScreenIncomingVia());
			phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
			phoneApp2Subuser.clickOnIncomingHangupButton();
			phoneApp2Subuser.waitForDialerPage();
			
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

			phoneApp2Subuser.waitForDialerPage();
			phoneApp2Subuser.clickOnSideMenu();
			callerName2 = phoneApp2Subuser.getCallerName();

		}

		Thread.sleep(10000);
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Outgoing", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2Subuser.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2Subuser.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2Subuser.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed2nd(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2Subuser.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2Subuser.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2Subuser.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validaterecordingUrlFor2ndEntry(),
				"--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Mainuser_To_Subuser_Subuser_Always_close() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Mainuser_To_Subuser_Subuser_Unavailable() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		api.addCustomTimeSlot("user", subuserID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording  availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording  availble--------");

	}

	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Mainuser_To_Subuser_Subuser_Logout() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2SubUserExtension);
		Thread.sleep(2000);
		phoneApp2Subuser.clickOnSideMenu();
		phoneApp2Subuser.clickOnLogout();
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		phoneApp2.waitForDialerPage();
		loginDialer(phoneApp2Subuser, account2SubUserEmail, account2Password);
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());
		assertEquals("Cancelled", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web1 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web1 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web1 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web1 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web1 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web1 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web1 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web1 Activityfeed page--");

		assertEquals("Cancelled", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Subuser_To_Mainuser_mainUser_Always_close() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2SubUserExtension);

		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2Subuser.clickOnDialButton();

		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);

		Common.pause(1);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2Subuser.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2Subuser.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail());
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Subuser_To_Mainuser_Subuser_Unavailable() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		api.addCustomTimeSlot("user", userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		api.addCustomTimeSlot("user", subuserID, false);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		System.out.println(account2SubUserExtension);

		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2Subuser.clickOnDialButton();

		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);

		Common.pause(1);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2Subuser.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2Subuser.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail());
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Subuser_To_Mainuser_Subuser_Logout() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		System.out.println(account2SubUserExtension);
		Thread.sleep(2000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2Subuser.clickOnDialButton();

		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(8000);
		loginDialer(phoneApp2, account2Email, account2Password);
		Thread.sleep(2000);
		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Cancelled", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Cancelled", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Cancelled", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Cancelled", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_No_Answer_Incoming_SubUser_Missed() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());
		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_Cancelled_Incoming_SubUser_Missed() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Cancelled", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Cancelled", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_Rejected_Incoming_SubUser_Rejected() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Rejected", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_Rejected_Incoming_SubUser_Rejected_Number_Voicemail_ON()
			throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallstatusFor2ndEntry(), "-- call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Rejected", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_NoAnswer_Incoming_SubUser_Missed_Number_Voicemail_ON()
			throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_NoAnswer_Incoming_SubUser_Missed_Number_IVR_ON()
			throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_NoAnswer_Incoming_SubUser_Missed_Number_Welcome_Message_ON()
			throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_From_MainUser_And_InternalTeamCalling__From_SubUser_To_Main_User() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp2.validateCallingScreenOutgoingVia());
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.clickOnDialButton();
		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");
		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);
		phoneApp2Subuser.waitForDialerPage();
		phoneApp2.clickOnOutgoingHangupButton();
		// validate outgoing Number on outgoing calling screen

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account1Number1, phoneApp2Subuser.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2Subuser.validatecallType());
		phoneApp2Subuser.clickOnRightArrow();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2Subuser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2Subuser.validateDepartmentName(),
				"----verify departmrnt name in webApp2Subuser");
		assertEquals(mainUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPrice, webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Incoming_Call_To_MainUser_And_InternalTeamCalling__From_SubUser_To_Main_User() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp3.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		assertEquals("Incoming Via " + departmentName, phoneApp2.validateCallingScreenIncomingVia());
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.waitForDialerPage();
		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		phoneApp2Subuser.clickOnDialButton();
		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");
		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);
		phoneApp2Subuser.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(2);
		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account1Number1, phoneApp2Subuser.validateNumberInAllcalls());
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());
		phoneApp2Subuser.clickOnRightArrow();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2Subuser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2Subuser.validateDepartmentName(),
				"----verify departmrnt name in webApp2Subuser");
		assertEquals(mainUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_To_SubUser_User_Assign_To_Team_Simultanious() throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverWebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_To_SubUser_User_Assign_To_Team_Fixedorder() throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverWebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_To_SubUser_User_Assign_To_Team_RoundRobin() throws Exception {

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driverWebApp2.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)

	public void InternalTeamCalling_Outgoing_MainUser_Completed_Incoming_SubUser_Completed_Incoming_Call_Voicemail_ON()
			throws Exception {
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);
		phoneApp3.enterNumberinDialer(account2Number1);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(1);
		Thread.sleep(12000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(2);

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Outgoing", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());
		
		// validate call status in Call detail page
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");

		// validate duration in call Details page
//		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
//				"--Duration of outgoing call in call detail page of dialer 2--");

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2Subuser.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2Subuser.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2Subuser.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed2nd(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2Subuser.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2Subuser.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2Subuser.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validaterecordingUrlFor2ndEntry(),
				"--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)

	public void InternalTeamCalling_Outgoing_MainUser_Completed_Incoming_SubUser_Completed_Incoming_Call_CallQueueOFF()
			throws Exception {
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "Off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);
		phoneApp3.enterNumberinDialer(account2Number1);
		phoneApp3.clickOnDialButton();
		phoneApp3.waitForDialerPage();
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(2);

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Outgoing", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2Subuser.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2Subuser.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2Subuser.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed2nd(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(account1Number1, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available", webApp2.validateStatusCallNotSetup());
//	webApp2.clickStatusItagBtn();
//	assertEquals("User is not available", webApp2.validateStatusItag());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentNameFor2ndEntry(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2Subuser.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2Subuser.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2Subuser.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validaterecordingUrlFor2ndEntry(),
				"--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)

	public void InternalTeamCalling_Call_from_Call_log_details_page() throws Exception {
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());
		assertEquals("Cancelled", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2.clickDialButtonOnCalldtlPage();
		Thread.sleep(1000);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();

	}

	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dial_own_Extension_via_Dialpad() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2MainUserExtension);
		phoneApp2Subuser.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		assertEquals(phoneApp2.validationMessage(), "You can't dial own extension number");
		phoneApp2Subuser.clickOnDialButton();
		assertEquals(phoneApp2Subuser.validationMessage(), "You can't dial own extension number");
	}

	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dial_Invalid_Extension_via_Dialpad() throws Exception {

		phoneApp2.enterNumberinDialer("000");
		phoneApp2Subuser.enterNumberinDialer("000");
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		assertEquals(phoneApp2.validationMessage(), "please enter valid extension number");
		phoneApp2Subuser.clickOnDialButton();
		assertEquals(phoneApp2Subuser.validationMessage(), "please enter valid extension number");
	}

	@Test(enabled = false) // priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Outgoing_MainUser_Completed_IncomingNumber_Not_Assign_To_User() throws Exception {

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		driverPhoneApp2Subuser.get(url.dialerSignIn());

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		webApp2.userTeamAllocation("users");
		webApp2.deallocatenUserFromNumber();
		Thread.sleep(1000);
		driverWebApp2.get(url.numbersPage());
		webApp2Subuser.navigateToNumberSettingpage(account2Number2);
		webApp2Subuser.userTeamAllocation("users");
		webApp2Subuser.deallocatenUserFromNumber();

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName2 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 28, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Credit_Zero_InternalTeamCalling_Outgoing_MainUser_Completed_Incoming_SubUser_Completed()
			throws Exception {

		creditAPI.UpdateCredit("0", "0", "0", userID);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		creditAPI.UpdateCredit("500", "0", "0", userID);
	}

	@Test(enabled = false) // priority = 29, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Internal_team_call_to_user_whose_extension_contain_4_digit() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.changeExtNumber();
		System.out.println(account2SubUserExtension);

		driverWebApp2.get(url.usersPage());
		((InviteUserPage) driverWebApp2).clickOnInviteUserButton();

		String actualTitle = ((InviteUserPage) driverWebApp2).validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		inviteUserPageObj.enterInviteUserEmail("");
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

//			drivergmail.openInvitationMail();
//			registrationPage.gClickOnRecentAcceptInvitationlink();
//			registrationPage.switchToWindowBasedOnTitle("Login | Callhippo.com");
//			inviteUserPageObj.enterSubUserFullName("Invited User1");
//			inviteUserPageObj.enterSubUserNewPassword("test@123");
//			inviteUserPageObj.enterSubUserConfirmPassword("test@123");
//			inviteUserPageObj.clickOnSubUserSubmitButton();
//			registrationPage.verifyWelcomeToCallHippoPopup();
		phoneApp2.enterNumberinDialer("000");
		phoneApp2Subuser.enterNumberinDialer("000");
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		assertEquals(phoneApp2.validationMessage(), "please enter valid extension number");
		phoneApp2Subuser.clickOnDialButton();
		assertEquals(phoneApp2Subuser.validationMessage(), "please enter valid extension number");
	}

	@Test(priority = 30, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Extension_Number_Change_InternalTeamCalling_Outgoing_MainUser_Completed_Incoming() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		webApp2.changeExtNumber();
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		Thread.sleep(2000);
		loginDialer(phoneApp2, account2Email, account2Password);
		
		phoneApp2Subuser.clickOnSideMenu();
		phoneApp2Subuser.clickOnLogout();
		Thread.sleep(2000);
		loginDialer(phoneApp2Subuser, account2SubUserEmail, account2Password);
	
		
		
        phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate duration in call Details page
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	// Extension Call after Changing Extension Number

	@Test(priority = 50, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Save_contact_with_Extension_And_Make_Call_MainUser_To_SubUser() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);
		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		phoneApp2Subuser.AddContact("2AccountMainuser");
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		phoneApp2.AddContact("2AccountSubuser");
		phoneApp2.clickOnconDetailCallBtn1();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		// phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		// String durationOnOutgoingCallScreen =
		// phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

	}

	@Test(priority = 51, dependsOnMethods = "Save_contact_with_Extension_And_Make_Call_MainUser_To_SubUser", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Save_contact_with_Extension_And_Make_Completed_Call_MainUser_To_SubUser() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

//		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
//		phoneApp2Subuser.AddExtensionOfSubuserAsContact("2AccountMainuser");
//		phoneApp2Subuser.clickOnTickGreenBtnToAddContactName1();
//		phoneApp2.enterNumberinDialer(account2SubUserExtension);
//		phoneApp2.AddExtensionOfSubuserAsContact("2AccountSubuser");
//		phoneApp2.clickOnTickGreenBtnToAddContactName1();
//		phoneApp2.clickOnconDetailCallBtn1();

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		phoneApp2.clickOnDialButton();
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		// assertEquals(phoneApp2.validateExtCallOutgoingNameCallingScreen(),"2AccountSubuser");

		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();

		assertEquals(phoneApp2Subuser.validateCallingScreenIncomingNumber(), "2AccountMainuser");
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		assertEquals("2AccountMainuser", phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();

		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail1(),
				"--Number in call log detail of dialer2--");
		assertEquals("2AccountMainuser", phoneApp2.validateNameInCallDetail(),
				"--Name in call log detail of dialer2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();

		assertEquals("2AccountSubuser", phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals("2AccountSubuser", phoneApp2.validateNameInCallDetail());
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail1());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("2AccountMainuser", phoneApp2Subuser.validateNumberInAllcalls());
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());
		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals("2AccountMainuser", phoneApp2Subuser.validateNameInCallDetail());
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail1());
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals("2AccountMainuser", webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals("2AccountSubuser", webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");
		assertEquals("2AccountMainuser", webApp2Subuser.validateClientNumber(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}

	@Test(priority = 52, dependsOnMethods = "Save_contact_with_Extension_And_Make_Call_MainUser_To_SubUser", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Save_contact_with_Extension_And_Make_Completed_Call_SubUser_To_MainUser() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

//		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
//		phoneApp2Subuser.AddExtensionOfSubuserAsContact("2AccountMainuser");
//		phoneApp2Subuser.clickOnTickGreenBtnToAddContactName1();
//		phoneApp2.enterNumberinDialer(account2SubUserExtension);
//		phoneApp2.AddExtensionOfSubuserAsContact("2AccountSubuser");
//		phoneApp2.clickOnTickGreenBtnToAddContactName1();
//		phoneApp2.clickOnconDetailCallBtn1();

		phoneApp2Subuser.enterNumberinDialer(account2MainUserExtension);
		phoneApp2Subuser.clickOnDialButton();
		assertEquals(phoneApp2Subuser.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		// assertEquals(phoneApp2.validateExtCallOutgoingNameCallingScreen(),"2AccountSubuser");

		assertEquals(phoneApp2Subuser.validateSubUserOutgoingNumberCallingScreen(), account2MainUserExtension);

		Common.pause(1);
		phoneApp2.waitForIncomingCallingScreen();

		assertEquals(phoneApp2.validateCallingScreenIncomingNumber(), "2AccountSubuser");
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2Subuser.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2Subuser.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		assertEquals("2AccountSubuser", phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();

		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail1(),
				"--Number in call log detail of dialer2--");
		assertEquals("2AccountSubuser", phoneApp2.validateNameInCallDetail(), "--Name in call log detail of dialer2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();

		assertEquals("2AccountMainuser", phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals("2AccountMainuser", phoneApp2.validateNameInCallDetail());
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail1());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("2AccountMainuser", phoneApp2Subuser.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2Subuser.validatecallType());
		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals("2AccountMainuser", phoneApp2Subuser.validateNameInCallDetail());
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail1());
		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals("2AccountSubuser", webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals("2AccountMainuser", webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");
		assertEquals("2AccountMainuser", webApp2Subuser.validateClientNumber(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}

	@Test(priority = 53, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dial_own_Extension_via_contact_details_page() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);
		phoneApp2Subuser.enterNumberinDialer(account2SubUserExtension);
		phoneApp2Subuser.AddContact("2AccountCallMainuser");
		phoneApp2.enterNumberinDialer(account2MainUserExtension);
		phoneApp2.AddContact("2AccountCallSubuser");
		phoneApp2.clickOnconDetailCallBtn1();
		assertEquals(phoneApp2.ExtvalidationMessage(), "You can't dial own extension number");
		phoneApp2Subuser.clickOnconDetailCallBtn1();
		assertEquals(phoneApp2Subuser.ExtvalidationMessage(), "You can't dial own extension number");

	}

	@Test(priority = 36, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void InternalTeamCalling_Mainuser_To_Subuser_Subuser_Custom_available() throws Exception {

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Subuser.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());
		assertEquals("Outgoing", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());

		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Completed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

	}
	//custom incoming Ringing time
	@Test(priority = 37, retryAnalyzer = Retry.class)
	public void verify_by_set_custom_incoming_Ringing_time_in_internal_team_call() throws Exception {
		System.out.print("Start......");
		driverWebApp2.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(account2Number1);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("35");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "35");
		//-------------------------------------------------------------
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2MainUserExtension);

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2SubUserEmail);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		System.out.println(account2SubUserExtension);

		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2Subuser.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);

		Common.pause(1);
		phoneApp2Subuser.waitForIncomingCallingScreen();
		//phoneApp2Subuser.waitForDialerPage();
		Common.pause(35); // 35 sec ringing
        assertEquals(phoneApp2Subuser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");                                          
        assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");                                          
        
		Thread.sleep(5000);

		phoneApp2.clickOnSideMenu();
		String mainUserCallerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		// validate Number in All Calls page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
				"--Number on dialer 2 all call page--");

		// validate call type in all calls page For 2nd Entry(1st queued call)
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page For 2nd Entry(1st queued call)
		assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
				"--Number in call log detail of dialer2--");

		// validate Status For 2nd Entry(1st queued call)
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");

		phoneApp2.clickOnBackOnCallDetailPage();

		// validate Number in All Calls page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);

		// validate Number on call details page
		assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());

		phoneApp2Subuser.clickOnSideMenu();
		String subUserCallerName = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInAllcalls());
		assertEquals("Incoming", phoneApp2Subuser.validatecallType());
		phoneApp2Subuser.clickOnRightArrow();
		assertEquals(phoneApp2Subuser.callDetailBlackListIconNotVisible(), true);
		Thread.sleep(4000);
		assertEquals(account2MainUserExtension, phoneApp2Subuser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2Subuser.validateCallStatusInCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
		assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
				"--Caller name in web2 Activityfeed page--");
		assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
				"--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallstatusFor2ndEntry(),
				"--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(false, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
		assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
		assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");

		driverWebApp2Subuser.get(url.signIn());
		webApp2Subuser.clickOnActivitFeed();
		Common.pause(5);
		assertEquals("-", webApp2Subuser.validateDepartmentName(), "----verify departmrnt name in webApp2Subuser");
		assertEquals(subUserCallerName, webApp2Subuser.validateCallerName(),
				"-----verify caller name in webApp2Subuser--");

		assertEquals("Missed", webApp2Subuser.validateCallStatus(), "----verify call statuts in webApp2Subuser---");
		assertEquals(true, webApp2Subuser.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2Subuser.validateCallCost(), "------verify call cost in webApp2Subuser-----");
		webApp2Subuser.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2Subuser.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
	}
}
