package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

//import org.omg.CORBA.ORB;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.*;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class TeamCallingTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp2SubUser;
	WebDriver driver2;
	DialerIndex phoneApp3;
	WebDriver driver5;
	WebToggleConfiguration webApp2;
	WebDriver driver4;
	WebToggleConfiguration webApp;
	WebDriver driver6;
	WebDriver driverphoneApp5;
	DialerIndex phoneApp5;
	HolidayPage webApp2HolidayPage;
	CallingCallCostTest callingCallCostobj;
	
	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2EmailMainUser = data.getValue("account2MainEmail");
	String account2PasswordMainUser = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2EmailSub2 = data.getValue("account2Sub2Email");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String number2 = data.getValue("account1Number1"); // account 1's number
	String account5Email = data.getValue("account5MainEmail");
	String number5 = data.getValue("account5Number1");
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String callerName2 = null;
	String callerName2Sub = null;
	String CallerName2Sub2 = null;
	String numberID;
	String userID;
	String subuserID;

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	
	Common excelTestResult;
	String outgoingCallPrice,outgoingCallPrice2Min;
	public TeamCallingTest() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj= new CallingCallCostTest();
	}

	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);


		driver6 = TestBase.init();
		webApp = PageFactory.initElements(driver6, WebToggleConfiguration.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver2, DialerIndex.class);
		
		driverphoneApp5 = TestBase.init_dialer();
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);

		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPrice=callingCallCostobj.callCost(number,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPrice);
		outgoingCallPrice2Min=Double.toString(Double.parseDouble(outgoingCallPrice)*2);
		System.out.println("outgoingCallPrice 2 Minute:"+outgoingCallPrice2Min);
		try {
			try {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}
			try {
				driver6.get(url.signIn());
				loginWeb(webApp, account1Email, account1Password);
				Common.pause(3);  
			} catch (Exception e) {
				driver6.get(url.signIn());
				loginWeb(webApp, account1Email, account1Password);
				Common.pause(3);
			}
			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			
			try {
			     driverphoneApp5.get(url.dialerSignIn());
			     loginDialer(phoneApp5, account5Email, account1Password);
			     phoneApp5.waitForDialerPage();
			} catch (Exception e) {
			     driverphoneApp5.get(url.dialerSignIn());
			     loginDialer(phoneApp5, account5Email, account1Password);
			     phoneApp5.waitForDialerPage();
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				callerName2Sub = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Sub, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				CallerName2Sub2 = webApp2.getusername(account2EmailSub2);
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);
				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number);
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();
				
				
				driver6.get(url.numbersPage());
				webApp.numberspage();
				webApp.navigateToNumberSettingpage(number2);
				Common.pause(3);
				webApp.setCallRecordingToggle("on");
			} catch (Exception e) {
				
				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				callerName2Sub = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Sub, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				CallerName2Sub2 = webApp2.getusername(account2EmailSub2);
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);
				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number);
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();
				
				
				driver6.get(url.numbersPage());
				webApp.numberspage();
				webApp.navigateToNumberSettingpage(number2);
				Common.pause(3);
				webApp.setCallRecordingToggle("on");
			}
		} catch (Exception e1) {
			String testname = "Team Calling Test - Before Method";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driver6, testname, "WebAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver6.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			driverphoneApp5.get(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (webApp.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp, account1Email, account1Password);
				Common.pause(3);

			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			
			if (phoneApp5.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account5Email, account2PasswordMainUser);
				Common.pause(3);
			}

			
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.setCallRecordingToggle("on");
			Common.pause(3);
			webApp2.customRingingTime("off"); //add new
			System.out.println("customRingingTime OFF");
			Common.pause(3);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2.userTeamAllocation("users");
			Thread.sleep(4000);
			webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
			webApp2.selectSpecificUserOrTeam(callerName2Sub, "Yes");
		
			driver6.get(url.numbersPage());
			webApp.numberspage();
			webApp.navigateToNumberSettingpage(number2);
			Common.pause(9);
			webApp.setCallRecordingToggle("on");
			Common.pause(2);
			
			driver4.get(url.usersPage());
			Common.pause(2);
			webApp2.navigateToUserSettingPage(account2EmailMainUser);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2.makeDefaultNumber(number);
			driver4.get(url.usersPage());
			Common.pause(2);
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2.makeDefaultNumber(number);
		} catch (Exception e) {
			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver6.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				driverphoneApp5.get(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (webApp.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}
				
				if (phoneApp5.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp5, account5Email, account2PasswordMainUser);
					Common.pause(3);
				}

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Thread.sleep(4000);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Sub, "Yes");
			
				driver6.get(url.numbersPage());
				webApp.numberspage();
				webApp.navigateToNumberSettingpage(number2);
				Common.pause(9);
				webApp.setCallRecordingToggle("on");
				Common.pause(2);
				
				driver4.get(url.usersPage());
				Common.pause(2);
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.makeDefaultNumber(number);
				driver4.get(url.usersPage());
				Common.pause(2);
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.makeDefaultNumber(number);
			} catch (Exception e1) {
				String testname = "Team Calling Test - Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver6, testname, "WebAppp1 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}

	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "WebAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp5, testname, "phoneApp5 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "WebApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp5, testname, "phoneApp5 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driver6.quit();
		driverphoneApp5.quit();
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		 } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		 }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		// ----------------------------------------

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	} catch (AssertionError e) {
		assertEquals("Rejected", webApp.validateCallStatus());
	}
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 2, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_Reject_Incoming_Reject_By_Both_Users() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(3000);
		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName3, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 3, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_No_Answer_Incoming_Reject_By_main_user_Missed_By_sub_user() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
	    assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());  
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());

		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());

		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	} catch (AssertionError e) {
		assertEquals("Rejected", webApp.validateCallStatus());
	}
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");

	}

	@Test(priority = 4, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_No_Answer_Incoming_Reject_By_Sub_user_Missed_By_main_user() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
		  } catch (AssertionError e) {
		assertEquals("Rejected", webApp.validateCallStatus());	  
		  }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 5, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Answered_By_Main_user_Missed_By_sub_user() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();

		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");

	}

	@Test(priority = 6, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Answered_By_Sub_user_Missed_By_Main_user() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForDialerPage();

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 7, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Answered_By_Main_user_and_Sub_User_AlwaysClosed() throws Exception {
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(true, phoneApp2SubUser.validateDialerScreenDisplayed());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 8, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Answered_By_Sub_user_and_Main_User_AlwaysClosed() throws Exception {
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(enabled = false) // (priority = 8,groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Answered_By_Main_user_and_Sub_User_is_busy_with_ongoingCall()
			throws Exception {

		phoneApp2SubUser.enterNumberinDialer(number3);
		phoneApp2SubUser.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(false, phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
	}

	// Team simulteneously

	@Test(priority = 9, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 10, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Reject_Incoming_Reject_By_Both_Users() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(3000);
		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 11, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_No_Answer_Incoming_Reject_By_main_user_Missed_By_sub_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
			  assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail()); 
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	  } catch (AssertionError e) {
		  assertEquals("Rejected", webApp.validateCallStatus()); 
	  }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 12, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_No_Answer_Incoming_Reject_By_Sub_user_Missed_By_main_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	} catch (AssertionError e) {
		assertEquals("Rejected", webApp.validateCallStatus());
	}
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 13, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_No_Answer_Incoming_Answered_By_Main_user_Missed_By_sub_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();

		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");

	}

	@Test(priority = 14, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Sub_user_Missed_By_Main_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForDialerPage();
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(10);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}

	@Test(priority = 15, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Main_user_and_Sub_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(true, phoneApp2SubUser.validateDialerScreenDisplayed());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(15);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");

	}

	@Test(priority = 16, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Sub_user_and_Main_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");

		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	@Test(priority = 77, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users_User_Custom_Available() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(2000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}
	
	@Test(priority = 78, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users_Number_Custom_Available() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, true);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}


	// Team Fixed Order

	@Test(priority = 17, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 18, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Rejected_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 19, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_main_user_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 20, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_main_user_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");

	}

	@Test(priority = 21, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_main_user_And_Missed_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 22, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_main_user_And_Reject_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 23, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Answer_By_main_user() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 24, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_main_user_is_AlwaysClosed_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incomimg----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");

	}
	
	@Test(priority = 79, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_Fixed_Order_Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users_User_Custom_Available() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(2000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 80, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_Fixed_Order_Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users_Number_Custom_Available() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, true);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}
	
	// Team Round Robin

	@Test(priority = 25, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 26, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 27, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_1stUser_Missed_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page

		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();

		Common.pause(5);

		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals( callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);

		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 28, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1stUser_Reject_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();

		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller Number in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller Number in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");
	}

	@Test(priority = 29, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1stUser_Answer_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller Number in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller Number in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp2 Recording Should Display----");

	}

	@Test(priority = 30, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Rejected_By_1stUser_Answer_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			// phoneApp2.clickOnIncomingRejectButton();
			phoneApp2SubUser.clickOnIncomingRejectButton();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller Number in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller Number in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 31, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Verify_calling_sequance() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();
			phoneApp2.waitForDialerPage();

			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();

			phoneApp1.clickOnOutgoingHangupButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			phoneApp1.clickOnOutgoingHangupButton();

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);

			// validate Call type in webApp for incoming call
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
					"---webApp2 Call Type Should Incoming----");

			// validate Department name in webApp for incoming call
			assertEquals(departmentName2, webApp2.validateDepartmentName());

			// validate Client Number in webApp for incoming call
			assertEquals(number2, webApp2.validateClientNumber());

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);

			// validate Call type in webApp for outgoing call
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

			// validate Department name in webApp for outgoing call
			assertEquals(departmentName, webApp.validateDepartmentName());

			// validate Client Number in webApp for outgoing call
			assertEquals(number, webApp.validateClientNumber());

		}

	}
	
	@Test(priority = 81, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_Both_Users_User_Custom_available() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(2000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		Common.pause(10);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 82, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_Both_Users_Number_Custom_available() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, true);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	// IVR Team Simulteneously

	@Test(priority = 32, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Missed_By_Both_Users() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller Number in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 33, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(4000);
		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		// assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 34, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Reject_By_main_user_Missed_By_sub_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 35, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Reject_By_Sub_user_Missed_By_main_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming---");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 36, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Main_user_Missed_By_sub_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 37, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Sub_user_Missed_By_Main_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForDialerPage();

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}

	@Test(priority = 38, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Main_user_and_Sub_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(true, phoneApp2SubUser.validateDialerScreenDisplayed());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 39, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Sub_user_and_Main_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");

		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	// IVR Team Fixed Order

	@Test(priority = 40, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 41, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 42, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_main_user_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "---webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "---webApp Recording Should Display----");

	}

	@Test(priority = 43, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_main_user_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "---webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "---webApp Recording Should Display----");

	}

	@Test(priority = 44, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_main_user_And_Missed_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen.
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		// assertEquals(callerName2Sub , webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}

	@Test(priority = 45, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_main_user_And_Reject_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp2SubUser.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();

		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}

	@Test(priority = 46, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Answer_By_main_user() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}

	@Test(priority = 47, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_main_user_is_AlwaysClosed_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName3 = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName3, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}

	// IVR Team Round Robin

	@Test(priority = 48, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1000;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			Thread.sleep(3000);
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			Thread.sleep(3000);

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 49, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_Both_Users() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 50, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_1stUser_Missed_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		// assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
//		assertEquals(callerName2Sub , webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 51, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1stUser_Reject_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Rejected", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 52, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1stUser_Answer_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(15000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(15000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 53, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Rejected_By_1stUser_Answer_by_2ndUser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.clickOnIncomingRejectButton();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}

	@Test(priority = 54, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Verify_calling_sequance() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();
			phoneApp2.waitForDialerPage();

			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();

			phoneApp1.validateDurationInCallingScreen(1);
			Thread.sleep(2000);
			phoneApp1.clickOnDialPad();
			Thread.sleep(2000);
			phoneApp1.pressIVRKey("2");

			phoneApp2.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			phoneApp1.clickOnOutgoingHangupButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();

			phoneApp1.validateDurationInCallingScreen(1);
			Thread.sleep(2000);
			phoneApp1.clickOnDialPad();
			Thread.sleep(2000);
			phoneApp1.pressIVRKey("2");

			phoneApp2SubUser.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();

			phoneApp1.clickOnOutgoingHangupButton();

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);

			// validate Call type in webApp for incoming call
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());

			// validate Department name in webApp for incoming call
			assertEquals(departmentName2, webApp2.validateDepartmentName());

			// validate Client Number in webApp for incoming call
			assertEquals(number2, webApp2.validateClientNumber());

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);

			// validate Call type in webApp for outgoing call
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed());

			// validate Department name in webApp for outgoing call
			assertEquals(departmentName, webApp.validateDepartmentName());

			// validate Client Number in webApp for outgoing call
			assertEquals(number, webApp.validateClientNumber());

		}

	}

	@Test(enabled = false, priority = 55, groups = { "internal_blind_Transfer_calling" }, retryAnalyzer = Retry.class)
	public void incoming_blind_Transfer_Completed_Hangup_From_UserA() throws Exception {

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp2.validateDurationInCallingScreen(1);

		phoneApp2.clickOnForwardButton();
		phoneApp2.enterUserNameInFDserchbox("subuser1");
		Thread.sleep(3000);
		phoneApp2.clickOnFDuser();
		phoneApp2.clickOnTransferNowButton();

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);

		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

	}

	@Test(enabled = false, priority = 56, groups = { "internal_blind_Transfer_calling" }, retryAnalyzer = Retry.class)
	public void incoming_blind_Transfer_Completed_Hangup_From_UserC() throws Exception {

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp2.validateDurationInCallingScreen(1);

		phoneApp2.clickOnForwardButton();
		phoneApp2.enterUserNameInFDserchbox("subuser1");
		Thread.sleep(3000);
		phoneApp2.clickOnFDuser();
		phoneApp2.clickOnTransferNowButton();

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);

		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());

		phoneApp2SubUser.clickOnIncomingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

	}

	@Test(priority = 57, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Missed_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();

		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 58, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Reject_By_Both_Users_Vociemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		Thread.sleep(3000);
		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(3000);
		phoneApp2SubUser.clickOnIncomingRejectButton();

		Thread.sleep(20000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		// String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 59, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		// String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 60, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomingRejectButton();

		Thread.sleep(40000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 61, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			Thread.sleep(40000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			Thread.sleep(40000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// ++++++++++++++++++++++++++++
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 62, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			Thread.sleep(50000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			Thread.sleep(50000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		// String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 63, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Reject_By_One_User_Missed_By_Another_User_Vociemail()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		Thread.sleep(3000);

		phoneApp2.clickOnIncomingRejectButton();
		Thread.sleep(30000);
		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		// String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 64, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Voicemail_Both_User_Always_Close() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(25000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		// String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 65, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Voicemail_Both_User_Always_Close() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(25000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		// String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 66, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Voicemail_Both_User_Always_Close() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "Closed", "off");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(25000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		// String callerName3 = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 67, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_One_User_Missed_By_Another_User_Vociemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.clickOnIncomingRejectButton(); // reject by main user
		phoneApp2.waitForDialerPage();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

//			phoneApp2.clickOnIncomingRejectButton(); // reject by main user
//			phoneApp2.waitForDialerPage();
//			
//			phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		// String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 68, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void IVR_Team_Incoming_Missed_By_Both_Users_UserVoicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"outgoingVia on outgoing calling screen::PhonApp 1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"Number on outgoing calling screen::PhonApp 1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);

		phoneApp1.pressIVRKey("2");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia(),
				"IncomingVia on Incoming calling screen::PhonApp 2--");
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia(),
				"IncomingVia on Incoming calling screen::phoneApp2SubUser--");
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber(),
				"Incoming Number on Incoming calling screen::PhonApp2--");
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber(),
				"Incoming Number on Incoming calling screen::phoneApp2SubUser--");

		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 69, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_Fixed_Incoming_Missed_By_Both_Users_UserVoicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);

		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 70, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Incoming_Missed_By_Both_Users_UserVoicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);

		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.waitForDialerPage();
			Thread.sleep(50000);
			phoneApp1.clickOnOutgoingHangupButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.waitForDialerPage();
			Thread.sleep(50000);
			phoneApp1.clickOnOutgoingHangupButton();
		}
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 71, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_1st_User_Missed_By_Another_User_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();
			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomingRejectButton();
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			// phoneApp2SubUser.clickOnIncomingRejectButton();
			Thread.sleep(65000);
			phoneApp1.clickOnOutgoingHangupButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			// assertEquals(number2,
			// phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			phoneApp2SubUser.clickOnIncomingRejectButton();
			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();
			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			// phoneApp2.clickOnIncomingRejectButton();

			Thread.sleep(65000);
			phoneApp1.clickOnOutgoingHangupButton();

		}
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 72, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_One_User_Rejected_By_Another_User_Vociemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomingRejectButton();
		Thread.sleep(50000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		// String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 73, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1st_User_Rejected_By_Another_User_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();
			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();
			Thread.sleep(60000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();
			Thread.sleep(10000);

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();
			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomingRejectButton();

			Thread.sleep(60000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();
			Thread.sleep(10000);

		}
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 74, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void IVR_Team_Incoming_Missed_By_Both_Users_NumberVoicemailDisabled() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"outgoingVia on outgoing calling screen::PhonApp 1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"Number on outgoing calling screen::PhonApp 1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);

		phoneApp1.pressIVRKey("2");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia(),
				"IncomingVia on Incoming calling screen::PhonApp 2--");
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia(),
				"IncomingVia on Incoming calling screen::phoneApp2SubUser--");
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber(),
				"Incoming Number on Incoming calling screen::PhonApp2--");
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber(),
				"Incoming Number on Incoming calling screen::phoneApp2SubUser--");

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 75, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void IVR_Team_Fixed_Incoming_Missed_By_Both_Users_NumberVoicemailDisabled() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);

		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 76, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Incoming_Missed_By_Both_Users_NumberVoicemailDisabled() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);

		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.waitForDialerPage();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.waitForDialerPage();

		}
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Missed".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 83, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Main_user_FTD_and_Sub_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	
	@Test(priority = 84, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Sub_user_FTD_and_Main_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}

	@Test(priority = 85, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Answered_By_Main_user_FTD()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	@Test(priority = 86, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrde_Outgoing_Completed_Incoming_Answered_By_Sub_user_FTD_Missed_By_Main_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		Common.pause(10);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	
	@Test(priority = 87, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Answered_By_Main_user_FTD_and_Sub_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	@Test(priority = 88, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrde_Outgoing_Completed_Incoming_Answered_By_Sub_user_FTD_and_Main_User_AlwaysClosed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(5000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number5);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	@Test(priority = 89, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Call_Recordoing_oFFTeam_simulteneously_Outgoing_Completed_Incoming_Missed_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();

		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}
	@Test(priority = 90, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Call_Recording_OFF_Team_FixedOrder_Outgoing_Completed_Incoming_Reject_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		
		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomingRejectButton();

		Thread.sleep(40000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}
	@Test(priority = 91, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Call_recording_OFF_Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_Both_Users_Voicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		
		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			Thread.sleep(40000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomingRejectButton();

			Thread.sleep(40000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// ++++++++++++++++++++++++++++
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Sh ould  Display----");
	}
	@Test(priority = 92, groups = { "users_calling" }, retryAnalyzer = Retry.class)
	public void Call_Recording_Off_IVR_Team_Simultaneously_Incoming_Missed_By_Both_Users_UserVoicemail() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"outgoingVia on outgoing calling screen::PhonApp 1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"Number on outgoing calling screen::PhonApp 1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);

		phoneApp1.pressIVRKey("2");
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia(),
				"IncomingVia on Incoming calling screen::PhonApp 2--");
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia(),
				"IncomingVia on Incoming calling screen::phoneApp2SubUser--");
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber(),
				"Incoming Number on Incoming calling screen::PhonApp2--");
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber(),
				"Incoming Number on Incoming calling screen::phoneApp2SubUser--");

		Thread.sleep(60000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}
	@Test(priority = 93, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Call_Recording_Off_IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_One_User_Rejected_By_Another_User_Vociemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"outgoingVia on outgoing calling screen::PhonApp 1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"Number on outgoing calling screen::PhonApp 1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);

		phoneApp1.pressIVRKey("2");
		phoneApp2.waitForIncomingCallingScreen();
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomingRejectButton();
		Thread.sleep(50000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		// String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 94, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Call_Recording_Off_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1st_User_Rejected_By_Another_User_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"outgoingVia on outgoing calling screen::PhonApp 1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"Number on outgoing calling screen::PhonApp 1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);

		phoneApp1.pressIVRKey("2");
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();
			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomingRejectButton();
			Thread.sleep(60000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();
			Thread.sleep(10000);

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			phoneApp2SubUser.waitForDialerPage();
			phoneApp2.waitForIncomingCallingScreen();
			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomingRejectButton();

			Thread.sleep(60000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp1.waitForDialerPage();
			Thread.sleep(10000);

		}
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice2Min, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 95, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Sub_user_Missed_By_Main_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForDialerPage();
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}

	@Test(priority = 96, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_main_user_And_Answer_By_Subuser()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		
		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");

	}

	@Test(priority = 97, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1stUser_Answer_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		
		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			// phoneApp2.clickOnIncomingRejectButton();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(5000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller Number in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller Number in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 98, groups = { "ivr_team_simulteneously" }, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answered_By_Main_user_Missed_By_sub_user()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForDialerPage();

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 99, groups = { "ivr_team_roundrobin" }, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_1stUser_Answer_by_2ndUser() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		
		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");


		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Thread.sleep(15000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();
			Thread.sleep(15000);
			phoneApp1.clickOnOutgoingHangupButton();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		// phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		// phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2Sub, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 100, groups = { "ivr_team_fixedorder" }, retryAnalyzer = Retry.class)
	public void call_Recording_OFF_IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Answer_By_main_user() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("off");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("off");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals(callerName2, webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Completed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}
	
	@Test(priority = 101, retryAnalyzer = Retry.class)
	public void Incoming_call_Simultaneous_2_users_1_users_is_busy_with_call_nextincoming_call_should_serve_to_2nd_user_Queue_ON_Voicemail_OFF() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(3000);
		webApp2.setCallRecordingToggle("on");
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(2000);
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");
		
		phoneApp2.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp1.validateCallingScreenIncomingNumber());

		phoneApp1.clickOnIncomimgAcceptCallButton();

		phoneApp5.enterNumberinDialer(number);
		phoneApp5.clickOnDialButton();
		
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp5.waitForDialerPage();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number5, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals("-", webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateIncomingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(callCost1, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}
	
	
	@Test(priority = 102, retryAnalyzer = Retry.class)
	public void Incoming_call_Simultaneous_2_users_1_users_is_busy_with_call_nextincoming_call_should_serve_to_2nd_user_Queue_ON_Voicemail_On() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(3000);
		webApp2.setCallRecordingToggle("on");
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(2000);
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");
		
		phoneApp2.enterNumberinDialer(number2);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp1.validateCallingScreenIncomingNumber());

		phoneApp1.clickOnIncomimgAcceptCallButton();

		phoneApp5.enterNumberinDialer(number);
		phoneApp5.clickOnDialButton();
		
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp5.validateDurationInCallingScreen(2);
		Common.pause(12);
		phoneApp5.clickOnOutgoingHangupButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number5, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Voicemail", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateIncomingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(callCost1, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}
	
	@Test(priority = 103, retryAnalyzer = Retry.class)
	public void Incoming_call_Fixedorder_2_users_1_users_is_busy_with_call_next_incoming_call_should_serve_to_2nd_user_Queue_ON_Voicemail_On() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(3000);
		webApp2.setCallRecordingToggle("on");
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(2000);
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForDialerPage();

		phoneApp5.enterNumberinDialer(number);
		phoneApp5.clickOnDialButton();
		
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		phoneApp5.validateDurationInCallingScreen(2);
		Common.pause(12);
		phoneApp5.clickOnOutgoingHangupButton();
		phoneApp2SubUser.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number5, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Voicemail", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}
	
	@Test(priority = 104, retryAnalyzer = Retry.class)
	public void Incoming_call_Simultanious_2_users_1_users_is_busy_with_call_next_incoming_call_should_serve_to_2nd_user_Queue_ON_Voicemail_On() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(3000);
		webApp2.setCallRecordingToggle("on");
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Teams");
		Thread.sleep(2000);
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.clickOnIncomimgAcceptCallButton();

			phoneApp5.enterNumberinDialer(number);
			phoneApp5.clickOnDialButton();
			
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForDialerPage();
			phoneApp5.validateDurationInCallingScreen(2);
			Common.pause(12);
			phoneApp5.clickOnOutgoingHangupButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp1.waitForDialerPage();

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

			phoneApp5.enterNumberinDialer(number);
			phoneApp5.clickOnDialButton();
			
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.waitForDialerPage();
			phoneApp5.validateDurationInCallingScreen(2);
			Common.pause(20);
			phoneApp5.clickOnOutgoingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp1.waitForDialerPage();

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}
		

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number5, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Voicemail", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display---- ");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display---- ");

	}
	
	@Test(priority = 105, retryAnalyzer = Retry.class)
	public void voicemail_OFF_IVR_ON_Team_simulteneously_Outgoing_Completed_Incoming_Unavailable__all_users_are_logout()
			throws Exception {
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		assertTrue(phoneApp2.validateDialerAppLoggedinPage());
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		assertTrue(phoneApp2SubUser.validateDialerAppLoggedinPage());
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("on");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
//		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");
		
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail()); 

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Unavailable", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);
		phoneApp2SubUser.waitForDialerPage();
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Unavailable", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		System.out.print(webApp2.validateCallStatus()) ;
		assertEquals("Unavailable", webApp2.validateCallStatus()); // current : Missed, correct:Unavailable

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		System.out.print(webApp.validateCallStatus());
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 106, retryAnalyzer = Retry.class)
	public void voicemail_OFF_IVR_OFF_Team_simulteneously_Outgoing_Rejected_Incoming_Call_not_setup_all_users_are_logout()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		assertTrue(phoneApp2.validateDialerAppLoggedinPage());
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		assertTrue(phoneApp2SubUser.validateDialerAppLoggedinPage());
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("on");
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
//		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);
		phoneApp2SubUser.waitForDialerPage();
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("User is not available", webApp2.validateCallStatus()); 

		// validate Call cost in webApp for incoming call
		assertEquals("-", webApp2.validateCallCost());
		
		// validate Status in webApp for incoming call in i tag	
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag()); 

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Rejected", webApp.validateCallStatus()); 

		// validate Call cost in webApp for outgoing call
		assertEquals("-", webApp.validateCallCost());
	
		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 107, retryAnalyzer = Retry.class)
	public void voicemail_ON_IVR_ON_Team_simulteneously_Outgoing_Completed_Incoming_Voicemail__all_users_are_logout()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		assertTrue(phoneApp2.validateDialerAppLoggedinPage());
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		assertTrue(phoneApp2SubUser.validateDialerAppLoggedinPage());
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("on");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");


		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail()); 

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);
		phoneApp2SubUser.waitForDialerPage();
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		System.out.print(webApp2.validateCallStatus()) ;
		assertEquals("Voicemail", webApp2.validateCallStatus()); // current : Missed, correct:Voicemail

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----"); //make true when status voice mail  

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		System.out.print(webApp.validateCallStatus());
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 108, retryAnalyzer = Retry.class)
	public void voicemail_ON_IVR_OFF_Team_simulteneously_Outgoing_Completed_Incoming_Voicemail__all_users_are_logout()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		assertTrue(phoneApp2.validateDialerAppLoggedinPage());
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		assertTrue(phoneApp2SubUser.validateDialerAppLoggedinPage());
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.setCallRecordingToggle("on");
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		driver6.get(url.numbersPage());
		webApp.navigateToNumberSettingpage(number2);
		Thread.sleep(9000);
		webApp.setCallRecordingToggle("on");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
//		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(10000);
		
		phoneApp1.clickOnOutgoingHangupButton();
        phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail()); 

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		phoneApp2.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
	    Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);
		phoneApp2SubUser.waitForDialerPage();
		
		phoneApp2SubUser.clickOnSideMenu();
	    phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		System.out.print(webApp2.validateCallStatus()) ;
		//assertEquals("Unavailable", webApp2.validateCallStatus()); 
		assertEquals("Voicemail", webApp2.validateCallStatus()); 

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		System.out.print(webApp.validateCallStatus());
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 109, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Incoming_call_allocation_team_simultaneous_number_deallocated_from_all_user_incoming_call_served_to_all_users_in_team()
			throws Exception {
		
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(9);
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		webApp2.DeselectSpecificUserOrTeam(CallerName2Sub2, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2Sub, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		
		
		Thread.sleep(4000);
		
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		try {
			assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
			 } catch (AssertionError e) {
			assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
			 }

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
			assertEquals("No Answer", webApp.validateCallStatus());
		} catch (AssertionError e) {
			assertEquals("Rejected", webApp.validateCallStatus());
		}
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	
	@Test(priority = 110, groups = { "team_Fixedorder" }, retryAnalyzer = Retry.class)
	public void Incoming_call_allocation_team_Fixedorder_number_deallocated_from_all_user_incoming_call_served_to_all_users_in_team()
			throws Exception {
		
		
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver1.get(url.dialerSignIn());
		driver2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		webApp2.DeselectSpecificUserOrTeam(CallerName2Sub2, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2Sub, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		Thread.sleep(4000);
		
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		
		

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		
		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
	

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
        assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
			 

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}

	@Test(priority = 111, groups = { "team_RoundRobin" }, retryAnalyzer = Retry.class)
	public void Incoming_call_allocation_team_Roundrobin_number_deallocated_from_all_user_incoming_call_served_to_all_users_in_team()
			throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver1.get(url.dialerSignIn());
		driver2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		webApp2.DeselectSpecificUserOrTeam(CallerName2Sub2, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2Sub, "Yes");
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		Thread.sleep(4000);
		
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		
		
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp2.waitForDialerPage();

			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			Thread.sleep(3000);
			phoneApp2SubUser.waitForDialerPage();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.waitForDialerPage();

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			Thread.sleep(3000);

			phoneApp2.waitForDialerPage();
		}
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
        assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
			 

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should   Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should   Display----");
	}
	
	@Test(priority = 112, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void Team_simulteneously_call_queue_ON__Outgoing_No_Answer_Incoming_Missed_main_Users_busy_subUser_no_answer_call_hangup_not_goto_queue() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();
		driver1.get(url.dialerSignIn());
		driver2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number5);
		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number5, phoneApp2.validateCallingScreenOutgoingNumber());
		
		phoneApp5.waitForIncomingCallingScreen();
		// Validate outgoingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName5, phoneApp5.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		
		phoneApp5.validateDurationInCallingScreen(1);
		
		phoneApp2.validateDurationInCallingScreen(1);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		
		//validate  phoneApp2 busy
		assertEquals(false,phoneApp2.validateDialerScreenDisplayed());
		
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
         //wait for call cut 
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcallsFor2ndEntry());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallTypeFor2ndentry());

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		
		phoneApp2.clickOnBackOnCallDetailPage();
		
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		//subuser
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		//String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e1) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
		
	}
	
	@Test(priority = 113, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_call_queue_ON__Outgoing_No_Answer_Incoming_Missed_main_Users_busy_subUser_no_answer_call_hangup_not_goto_queue() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(number5);
		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number5, phoneApp2.validateCallingScreenOutgoingNumber());
		
		phoneApp5.waitForIncomingCallingScreen();
		// Validate outgoingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName5, phoneApp5.validateCallingScreenIncomingVia());
		// validate Incoming Number on Incoming calling screen
		assertEquals(number, phoneApp5.validateCallingScreenIncomingNumber());

		phoneApp5.clickOnIncomimgAcceptCallButton();
		
		phoneApp5.validateDurationInCallingScreen(1);
		
		phoneApp2.validateDurationInCallingScreen(1);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		//validate  phoneApp2 busy
		assertEquals(false,phoneApp2.validateDialerScreenDisplayed());
		
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
         //wait for call cut 
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcallsFor2ndEntry());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallTypeFor2ndentry());

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		
		phoneApp2.clickOnBackOnCallDetailPage();
	
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		//subuser
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		//String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e1) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
		
	}
		
	@Test(priority = 114, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_call_queue_ON__Outgoing_No_Answer_Incoming_Missed_main_Users_busy_subUser_no_answer_call_hangup_not_goto_queue() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		phoneApp5.enterNumberinDialer(number);
		phoneApp5.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName5, phoneApp5.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp5.validateCallingScreenOutgoingNumber());
	
				
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number5, phoneApp2.validateCallingScreenIncomingNumber());
			
			phoneApp2.clickOnIncomimgAcceptCallButton();
			
			phoneApp2.validateDurationInCallingScreen(1);
			
			phoneApp5.validateDurationInCallingScreen(1);
			
			phoneApp1.enterNumberinDialer(number);
			
			phoneApp1.clickOnDialButton();

			// Validate outgoingVia on outgoing calling screen
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

			// validate outgoing Number on outgoing calling screen
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
			
			assertEquals(false,phoneApp2.validateDialerScreenDisplayed());
			
			phoneApp2SubUser.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			//wait for call cut 
			phoneApp2SubUser.waitForDialerPage();
			phoneApp1.waitForDialerPage();
			
			phoneApp5.clickOnOutgoingHangupButton();
			phoneApp2.waitForDialerPage();
			Thread.sleep(10000);
			
			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();
					

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number5, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			
			phoneApp2SubUser.validateDurationInCallingScreen(1);
			
			phoneApp5.validateDurationInCallingScreen(1); 
			
			phoneApp1.enterNumberinDialer(number);
			
			phoneApp1.clickOnDialButton();

			// Validate outgoingVia on outgoing calling screen
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

			// validate outgoing Number on outgoing calling screen
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
			
			assertEquals(false,phoneApp2SubUser.validateDialerScreenDisplayed());
			
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
	         //wait for call cut 
			phoneApp2.waitForDialerPage();
			phoneApp1.waitForDialerPage();
			
			phoneApp5.clickOnOutgoingHangupButton();
			phoneApp2SubUser.waitForDialerPage();
			Thread.sleep(10000);
			
			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
	
		}
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number5, phoneApp2.validateNumberInAllcallsFor2ndEntry());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCallsFor2ndEntry());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry());

		phoneApp2.clickOnRightArrowFor2ndEntry();

		// validate Number on call details page
		assertEquals(number5, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		
		phoneApp2.clickOnBackOnCallDetailPage();
		
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		
		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		 } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		 }

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e1) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	
// Custom incoming ringing time
	@Test(priority = 115, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void verify_custom_incoming_ringing_time_in_Simultaneous_order_in_team() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("45");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "45");
		
		//*********************************************
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");

		webApp2.clickOnSimultaneously();
		Common.pause(2);
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
	
		Common.pause(47); // 45 sec ringing
		
		assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		  } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		  }
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	 } catch (AssertionError e) {
		 assertEquals("Rejected", webApp.validateCallStatus());
	 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
		
	}
	
	@Test(priority = 116, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void verify_custom_incoming_ringing_time_in_Fixed_order_in_team() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("17");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "17");
		
		//*********************************************************************
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		Common.pause(2);
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		//phoneApp2.waitForDialerPage();
		Common.pause(18); // 17 sec ringing
		assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		
		phoneApp2SubUser.waitForIncomingCallingScreen();

		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		//phoneApp1.waitForDialerPage();
		
		Common.pause(18); // 17 sec ringing
		assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
		
		
	}
	@Test(priority = 117, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void verify_custom_incoming_ringing_time_in_Round_Robbin_order_in_team() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("13");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "13");
		
        //************************************************************************************************
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			//phoneApp2.waitForDialerPage();
			Common.pause(15); // 13 sec ringing
			assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			Common.pause(15); // 13 sec ringing
			assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

			//phoneApp2SubUser.waitForDialerPage();
			Common.pause(15); // 13 sec ringing
			assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			Common.pause(15); // 13 sec ringing
			assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		}

		//phoneApp1.waitForDialerPage();
		assertEquals(phoneApp1.validateDialerScreen(), true);
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	
	//custom_IVR
	@Test(priority = 118, groups = { "team_simulteneously" }, retryAnalyzer = Retry.class)
	public void verify_custom_incoming_ringing_time_in_Simultaneous_order_in_team_IVR_ON() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("22");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "22");
		
		//*********************************************
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2"); 

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

        Common.pause(24); // 22 sec ringing
		
		assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller Number in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");
	}
	
	@Test(priority = 119, groups = { "team_fixedorder" }, retryAnalyzer = Retry.class)
	public void verify_custom_incoming_ringing_time_in_Fixed_order_in_team_IVR_ON() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("15");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "15");
		
		//*********************************************************************
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

	//	phoneApp2.waitForDialerPage();
		Common.pause(17); // 15 sec ringing
		assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		
		phoneApp2SubUser.waitForIncomingCallingScreen();
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
		//phoneApp1.waitForDialerPage();
		Common.pause(17); // 15 sec ringing
		assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");	
		
	}
	@Test(priority = 120, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void verify_custom_incoming_ringing_time_in_Round_Robbin_order_in_team_IVR_ON() throws Exception {
		System.out.print("Start......");
		driver4.get(url.numbersPage());
	
		webApp2.navigateToNumberSettingpage(number);
		
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		Common.pause(3);
		webApp2.customRingingTime("on");
		System.out.println("customRingingTime ON");
		Common.pause(3);
		webApp2.enterDuration_customRingingTime("20");
		System.out.println(webApp2.validateSuccessMessage());
		assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
		assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "20");
		
        //************************************************************************************************
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(7000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			//phoneApp2.waitForDialerPage();
			Common.pause(22); // 20 sec ringing
			assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());
			Common.pause(22); // 20 sec ringing
			assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();

			assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());
			assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		//	phoneApp2SubUser.waitForDialerPage();
			Common.pause(22); // 20 sec ringing
			assertEquals(phoneApp2SubUser.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

			phoneApp2.waitForIncomingCallingScreen();

			// Validate IncomingVia on Incoming calling screen
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

			// validate Incoming Number on Incoming calling screen
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			Common.pause(22); // 20 sec ringing
			assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		}

		//phoneApp1.waitForDialerPage();
		assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for incoming call
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");

		// validate Department name in webApp for incoming call
		assertEquals(departmentName2, webApp2.validateDepartmentName());

		// validate Caller name in webApp for incoming call
		assertEquals("-", webApp2.validateCallerName());

		// validate Client Number in webApp for incoming call
		assertEquals(number2, webApp2.validateClientNumber());

		// validate Status in webApp for incoming call
		assertEquals("Missed", webApp2.validateCallStatus());

		// validate Call cost in webApp for incoming call
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for incoming call
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		// validate Call type in webApp for outgoing call
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");

		// validate Department name in webApp for outgoing call
		assertEquals(departmentName, webApp.validateDepartmentName());

		// validate Caller name in webApp for outgoing call
		assertEquals(callerName, webApp.validateCallerName());

		// validate Client Number in webApp for outgoing call
		assertEquals(number, webApp.validateClientNumber());

		// validate Status in webApp for outgoing call
		assertEquals("Completed", webApp.validateCallStatus());

		// validate Call cost in webApp for outgoing call
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);

		// validate Recording URL present in webApp for Outgoing call
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");


	}

	
	
}
