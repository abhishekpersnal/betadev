package com.CallHippo.Dialer.Calling;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

//import org.omg.CORBA.ORB;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.*;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

import okhttp3.Call;

public class ForwordToDeviceTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp2SubUser;
	WebDriver driver2;
	DialerIndex phoneApp3;
	WebDriver driver5;
	WebToggleConfiguration webApp2;
	WebDriver driver4;
	WebToggleConfiguration webApp;
	WebDriver driver6;
	DialerIndex phoneApp4;
	WebDriver driverPhoneApp4;
	DialerIndex phoneApp5;
	WebDriver driverPhoneApp5;
	RestAPI api;
	HolidayPage webApp2HolidayPage;
	SignupPage dashboard;
	CallingCallCostTest callingCallCostobj;

	RestAPI creditAPI;
	static Common excel,csv;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");

	String account2EmailMainUser = data.getValue("account2MainEmail");
	String account2PasswordMainUser = data.getValue("masterPassword");

	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");

	String account4Email = data.getValue("account4MainEmail");
	String account4Password = data.getValue("masterPassword");

	String account5Email = data.getValue("account5MainEmail");
	String account5Password = data.getValue("masterPassword");

	String number = data.getValue("account2Number1"); // account 2's number
	String number2 = data.getValue("account1Number1"); // account 1's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String callerName2 = null;
	String callerName2Sub = null;

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number

	String account4Number1 = data.getValue("account4Number1");	
	String account5Number1 = data.getValue("account5Number1");
	String userID;
	String subuserID;
	String numberID;
	
	Common excelTestResult;
	String outgoingCallPrice,outgoingCallPrice2Min,outFTDCallPrice;
	public ForwordToDeviceTest() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		csv= new Common();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj= new CallingCallCostTest();
	}
	
	int record=1;
	ArrayList<String> csvRecords = new ArrayList<String>();
	ArrayList<String> webRecords= new ArrayList<String>();
	
	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);
		dashboard = PageFactory.initElements(driver4, SignupPage.class);

		driver6 = TestBase.init();
		webApp = PageFactory.initElements(driver6, WebToggleConfiguration.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver2, DialerIndex.class);

		driver5 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driver5, DialerIndex.class);

		driverPhoneApp4 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driverPhoneApp4, DialerIndex.class);

		driverPhoneApp5 = TestBase.init_dialer();
		phoneApp5 = PageFactory.initElements(driverPhoneApp5, DialerIndex.class);

		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPrice=callingCallCostobj.callCost(number,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPrice);
		outgoingCallPrice2Min=Double.toString(Double.parseDouble(outgoingCallPrice)*2);
		System.out.println("outgoingCallPrice 2 Minute:"+outgoingCallPrice2Min);
		
		outFTDCallPrice=Double.toString(Double.parseDouble(callCost1)+Double.parseDouble(outgoingCallPrice));
		System.out.println("outgoing FTDCallPrice:"+outFTDCallPrice);
		try {
			try {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}
			try {
				driver6.get(url.signIn());
				loginWeb(webApp, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver6.get(url.signIn());
				loginWeb(webApp, account1Email, account1Password);
				Common.pause(3);
			}
			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			try {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			try {
				driverPhoneApp5.get(url.dialerSignIn());
				loginDialer(phoneApp5, account5Email, account5Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp5.get(url.dialerSignIn());
				loginDialer(phoneApp5, account5Email, account5Password);
				Common.pause(3);
			}

			try {
				driver5.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driver5.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				Common.pause(9);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Common.pause(9);

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				String Url = driver4.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				webApp.numberspage();
				webApp.navigateToNumberSettingpage(number2);
				Common.pause(9);
				webApp.setCallRecordingToggle("on");
			} catch (Exception e) {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				Common.pause(9);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); //add new
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Common.pause(9);

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				String Url = driver4.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				webApp.numberspage();
				webApp.navigateToNumberSettingpage(number2);
				Common.pause(9);
				webApp.setCallRecordingToggle("on");
			}
		} catch (Exception e1) {
			String testname = "FTD Calling Test - Before Test";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail login");
			Common.Screenshot(driver5, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driver6, testname, "WebAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver6.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			driver5.navigate().to(url.dialerSignIn());
			driverPhoneApp4.navigate().to(url.dialerSignIn());
			driverPhoneApp5.navigate().to(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (webApp.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp, account1Email, account1Password);
				Common.pause(3);

			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}

			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			if (phoneApp5.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account5Email, account5Password);
				Common.pause(3);
			}

			driver4.get(url.usersPage());

			Common.pause(3);
			webApp2.navigateToUserSettingPage(account2EmailMainUser);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
			webApp2.deleteExtraFTDNumber();
			webApp2.enterFTDnumber(number3);
			Thread.sleep(2000);
			driver4.get(url.usersPage());
			Common.pause(2);
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
			webApp2.deleteExtraFTDNumber();
			Common.pause(3);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.showCallHippoNumberIncomingForwardedCalls("off");
			webApp2.setCallRecordingToggle("on");
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2.userTeamAllocation("users");
			Common.pause(3);
			webApp2.customRingingTime("off"); //add new
			System.out.println("customRingingTime OFF");
			Common.pause(3);
			webApp.numberspage();
			webApp.navigateToNumberSettingpage(number2);
			Common.pause(9);
			webApp.setCallRecordingToggle("on");
			
		} catch (Exception e) {
			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver6.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				driver5.navigate().to(url.dialerSignIn());
				driverPhoneApp4.navigate().to(url.dialerSignIn());
				driverPhoneApp5.navigate().to(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (webApp.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}

				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account4Email, account4Password);
					Common.pause(3);
				}

				if (phoneApp5.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp5, account5Email, account5Password);
					Common.pause(3);
				}

				driver4.get(url.usersPage());
				Common.pause(3);
				webApp2.navigateToUserSettingPage(account2EmailMainUser);
				Thread.sleep(9000);
				webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
				webApp2.deleteExtraFTDNumber();
				webApp2.enterFTDnumber(number3);
				Thread.sleep(2000);
				driver4.get(url.usersPage());
				Common.pause(2);
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
				webApp2.deleteExtraFTDNumber();
				Common.pause(3);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.showCallHippoNumberIncomingForwardedCalls("off");
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp.numberspage();
				webApp.navigateToNumberSettingpage(number2);
				Common.pause(9);
				webApp.setCallRecordingToggle("on");
				
			} catch (Exception e1) {
				String testname = "FTD Calling Test - Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail login");
				Common.Screenshot(driver5, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver6, testname, "WebAppp1 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}

	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "WebAppp1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp5, testname, "PhoneAppp5 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "WebApp1 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driver5.quit();
		driver6.quit();
		driverPhoneApp4.quit();
		driverPhoneApp5.quit();
	}
private void validateRecord(String csvColValue,String titleValue) throws Exception {
		
		csvRecords = csv.readDataFromActivityFeedCsv(csv.verifyDownloadFileName(), csvColValue);
		System.out.println("CSV records after first element::" + csvRecords);
		webRecords = webApp2.getActivityFeedWebRecords(record,titleValue);
		System.out.println("Web records" + webRecords);
		webApp2.validateWebActivityFeedInCsv(csvRecords,webRecords);
	
	}
	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	//	dialer.test();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void Outgoing_NO_Anawer_Incoming_Missed_By_Both_User_and_Forward_To_Device_And_verify_in_Activity_Report() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		try {
			assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
			} catch (AssertionError  e) {
			assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
			}
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
			assertEquals("No Answer", webApp.validateCallStatus());
		    } catch (AssertionError e) {
			assertEquals("Rejected", webApp.validateCallStatus());
		    }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");

		//validate in activity feed report
//	    csv.deleteAllFile("activityFeedReport"); 
//	    webApp2.clickOnCloseiButton();
//		webApp2.clickOnActivitFeed();
//		Common.pause(3);
//	   
//		webApp2.clickOnCheckBoxOption(record);
//		Common.pause(3);
//		assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//		webApp2.clickOnFileDownload();
//		
//		Common.pause(8);
//		
//		dashboard.ValidateActivityFeedReportOnGmail();
//		Common.pause(3);
//		assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//		webApp2.clickOnActivitFeed();
//		Common.pause(5);
//	
//		//validate caller-CSV with User-Web
//		validateRecord("caller","User");
//		
//		//validate callStatus-CSV with Status-Web
//		validateRecord("callStatus","Status");
//		
//		//validate forwardedOn-CSV with User-Web
//		validateRecord("forwardedOn","-");
//		
		
		
	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_Forward_To_Device_And_verify_in_Activity_Report() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
		
		//validate in activity feed report
//	    csv.deleteAllFile("activityFeedReport"); 
//	    webApp2.clickOnCloseiButton();
//		webApp2.clickOnActivitFeed();
//		Common.pause(3);
//	   
//		webApp2.clickOnCheckBoxOption(record);
//		Common.pause(3);
//		assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//		webApp2.clickOnFileDownload();
//		
//		Common.pause(8);
//		
//		dashboard.ValidateActivityFeedReportOnGmail();
//		Common.pause(3);
//		assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//		webApp2.clickOnActivitFeed();
//		Common.pause(5);
//	
//		//validate caller-CSV with User-Web
//		validateRecord("caller","User");
//		
//		//validate callStatus-CSV with Status-Web
//		validateRecord("callStatus","Status");
//		
//		//validate forwardedOn-CSV with User-Web
//		validateRecord("forwardedOn",number3);

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_Main_User_missed_FTD() throws Exception {
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp3.waitForDialerPage();
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void Outgoing_Reject_Incoming_Reject_By_Main_User_and_FTD_Sub_User_Logout_And_verify_in_Activity_Report() throws Exception {

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
        Common.pause(5);
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(5);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp3.clickOnIncomingRejectButton();

		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
		
		
		//validate in activity feed report
//	    csv.deleteAllFile("activityFeedReport"); 
//	    webApp2.clickOnCloseiButton();
//		webApp2.clickOnActivitFeed();
//		Common.pause(3);
//	   
//		webApp2.clickOnCheckBoxOption(record);
//		Common.pause(3);
//		assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//		webApp2.clickOnFileDownload();
//		
//		Common.pause(8);
//		
//		dashboard.ValidateActivityFeedReportOnGmail();
//		Common.pause(3);
//		assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//		webApp2.clickOnActivitFeed();
//		Common.pause(5);
//	
//		//validate caller-CSV with User-Web
//		validateRecord("caller","User");
//		
//		//validate callStatus-CSV with Status-Web
//		validateRecord("callStatus","Status");
//		
//		//validate forwardedOn-CSV with User-Web
//		validateRecord("forwardedOn","-");
	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Reject_By_Main_User_and_FTD_Welcome_Message_Is_ON() throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(5);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp3.clickOnIncomingRejectButton();

		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_Rejected_By_Main_User_and_FTD() throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(7);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(15);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Voicemail", phoneApp2.validateCallStatusInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		//assertEquals(callerName2, webApp2.validateCallerName()); 
		//assertEquals("Voicemail", webApp2.validateCallStatus());
		assertTrue("Voicemail".equalsIgnoreCase(webApp2.validateCallStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_Rejected_By_FTD() throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(5);
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(15);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		driver1.get(url.dialerSignIn());
		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName()); 
		//assertEquals(callerName2, webApp2.validateCallerName());
		assertTrue("Voicemail".equalsIgnoreCase(webApp2.validateCallStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Outgoing_Rejected_Incoming_Call_Not_Setup_user_Closed_FTD() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		webApp2.enterFTDnumber(number3);
		Thread.sleep(2000);

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		//assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("User is not available", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----"); // assertEquals(callCost1,webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		// assertEquals(outgoingCallPrice,webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_User_Closed_By_FTD() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		webApp2.enterFTDnumber(number3);
		Thread.sleep(2000);

		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(15);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName()); 
		//assertEquals(callerName2, webApp2.validateCallerName());
		assertTrue("Voicemail".equalsIgnoreCase(webApp2.validateCallStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Welcome_Message_FTD_is_Available_Both_User_logout() throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(2);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail());
		// assertEquals("Via " + departmentName2,
		// //phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Welcome message", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void IVR_Outgoing_Anawer_Incoming_Answer_By_Forward_To_Device() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void IVR_Outgoing_Anawer_Incoming_Voicemail_Reject_By_Forward_To_Device_Both_User_Logout() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(3000);
		phoneApp1.pressIVRKey1();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();

		Common.pause(15);
		phoneApp1.clickOnOutgoingHangupButton();
		driver.get(url.dialerSignIn());

		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Voicemail", phoneApp2.validateCallStatusInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName()); 
		//assertEquals(callerName2, webApp2.validateCallerName());
		assertTrue("Voicemail".equalsIgnoreCase(webApp2.validateCallStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_Forward_To_Device_OF_SubUser() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(number3);
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2SubUser = phoneApp2SubUser.getCallerName();

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2SubUser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Outgoing_No_Anawer_Incoming_Missed_By_Forward_To_Device_OF_SubUser() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(number3);
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
//		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.waitForDialerPage();
//		phoneApp3.clickOnIncomingHangupButton();
//		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		 } catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		 }
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2SubUser = phoneApp2SubUser.getCallerName();

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
		 } catch (AssertionError e) {
		assertEquals("Rejected", webApp.validateCallStatus());
		 }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Outgoing_No_Anawer_Incoming_Reject_By_Forward_To_Device_OF_SubUser_User_Logout() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(number3);
		Thread.sleep(2000);

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(7);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2SubUser = phoneApp2SubUser.getCallerName();

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2SubUser, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Outgoing_No_Anawer_Incoming_Voicemail_Reject_By_Forward_To_Device_OF_SubUser() throws Exception {

		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		Thread.sleep(2000);

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		Common.pause(7);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(15);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2SubUser = phoneApp2SubUser.getCallerName();

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2SubUser, webApp2.validateCallerName());
		assertTrue("Voicemail".equalsIgnoreCase(webApp2.validateCallStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_NO_Anawer_Incoming_Missed_By_Both_Users_and_FTD() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals("Incoming Via " + departmentName2, phoneApp2SubUser.validateCallingScreenIncomingVia());

		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		assertEquals(number2, phoneApp2SubUser.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		 } catch (AssertionError e) {
	     assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		 }
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		try {
		assertEquals("No Answer", webApp.validateCallStatus());
	  } catch (AssertionError e) {
		  assertEquals("Rejected", webApp.validateCallStatus()); 
	  }
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Reject_Incoming_Reject_By_FdTD_of_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomingRejectButton();
		Thread.sleep(3000);
		phoneApp4.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Rejected", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Completed_Incoming_Missed_By_FTD_of_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void Team_FixedOrder_Outgoing_Rejected_Incoming_Reject_By_FTD_of_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		Common.pause(3);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForDialerPage();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp3.waitForDialerPage();

		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomingRejectButton();
		phoneApp4.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_FTD_of_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(3);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();

			phoneApp3.waitForDialerPage();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.waitForDialerPage();
		} else {
			phoneApp4.waitForIncomingCallingScreen();

			phoneApp4.waitForDialerPage();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.waitForDialerPage();
		}

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_FTD_of_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomingRejectButton();

			phoneApp3.waitForDialerPage();
			phoneApp4.waitForIncomingCallingScreen();

			phoneApp4.clickOnIncomingRejectButton();
			phoneApp4.waitForDialerPage();

			loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
			Common.pause(3);
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp4.waitForIncomingCallingScreen();

			phoneApp4.clickOnIncomingRejectButton();

			phoneApp4.waitForDialerPage();

			phoneApp3.waitForIncomingCallingScreen();

			phoneApp3.clickOnIncomingRejectButton();
			phoneApp3.waitForDialerPage();

			loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
			Common.pause(3);
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();

		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		// assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}

	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Dialer_Logout_Outgoing_Completed_Incoming_Missed_By_Both_Users()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
//		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);

		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 24, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomingRejectButton();
		Thread.sleep(4000);
		phoneApp4.clickOnIncomingRejectButton();
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 25, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Dialer_Logout_Outgoing_Completed_Incoming_Missed_By_FTD_of_Both_Users()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();

		phoneApp3.waitForDialerPage();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outFTDCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void IVR_Team_Dialer_Logout_FixedOrder_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomingRejectButton();

		phoneApp3.waitForDialerPage();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomingRejectButton();
		phoneApp4.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 27, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Reject_By_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1000;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();

			phoneApp3.clickOnIncomingRejectButton();

			phoneApp3.waitForDialerPage();
			phoneApp4.waitForIncomingCallingScreen();
			Thread.sleep(3000);
			phoneApp4.clickOnIncomingRejectButton();

			loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
			Common.pause(3);
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);

			phoneApp2.clickOnSideMenu();
			callerName2 = phoneApp2.getCallerName();

			phoneApp2SubUser.waitForDialerPage();
			phoneApp2SubUser.clickOnSideMenu();
			callerName2Sub = phoneApp2SubUser.getCallerName();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomingRejectButton();

			phoneApp4.waitForDialerPage();

			phoneApp3.waitForIncomingCallingScreen();
			Thread.sleep(3000);

			phoneApp3.clickOnIncomingRejectButton();

			loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
			Common.pause(3);
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);

			phoneApp2SubUser.clickOnSideMenu();
			callerName2 = phoneApp2SubUser.getCallerName();

			phoneApp2.waitForDialerPage();
			phoneApp2.clickOnSideMenu();
			callerName2Sub = phoneApp2.getCallerName();
		}

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
//		assertEquals(callerName2Sub, webApp2.validateCallerName());

		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Rejected", webApp2.validateCallStatus());
		assertEquals(callCost1, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 28, retryAnalyzer = Retry.class)
	public void IVR_Team_RoundRobin_Outgoing_Completed_Incoming_Missed_By_Both_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.waitForDialerPage();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp3.waitForDialerPage();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.waitForDialerPage();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.waitForDialerPage();

		}

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outFTDCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 29, retryAnalyzer = Retry.class)
	public void Dialer_Logout_call_queue_user_busy_On_FTD_with_Incoming_ongoing_call_Incoming_queued_call_served_to_FTD_and_Accepted()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 30, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_Ringing_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		// phoneApp2.waitForIncomingCallingScreen();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp3.waitForDialerPage();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 31, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_FTD_Incoming_ongoing_call_Incoming_queued_call_served_to_User_and_Accepted()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 32, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_user_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account4Number1);
		phoneApp2.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();

		String callerName = phoneApp2.getCallerName();

		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 34, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ringing_Incoming_queued_call_served_to_FTD_and_Missed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account4Number1);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on Calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 35, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ringing_Incoming_queued_call_served_to_FTD_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account4Number1);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 36, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_1st_Forward_To_Device() throws Exception {
		driver4.get(url.usersPage());

		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber2(account5Number1);
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp5.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 37, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_2nd_Forward_To_Device() throws Exception {
		driver4.get(url.usersPage());

		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber2(account5Number1);
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp5.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 38, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_1st_Forward_To_Device_of_SubUser() throws Exception {
		driver4.get(url.usersPage());

		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		webApp2.enterFTDnumber2(account5Number1);
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp5.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2SubUser = phoneApp2SubUser.getCallerName();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2SubUser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 39, retryAnalyzer = Retry.class)
	public void Outgoing_Anawer_Incoming_Answer_By_2nd_Forward_To_Device_of_SubUser() throws Exception {
		driver4.get(url.usersPage());

		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Thread.sleep(2000);

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.deleteExtraFTDNumber();
		webApp2.enterFTDnumber(number3);
		webApp2.enterFTDnumber2(account5Number1);
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp5.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2SubUser = phoneApp2SubUser.getCallerName();

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2SubUser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}

	@Test(priority = 40, retryAnalyzer = Retry.class)
	public void Outgoing_Completed_Incoming_Completed_Answer_By_Forward_To_Device_OF_MainUser_MainUser_custom_available_Subuser_unavailable() throws Exception {

		
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("on", "off", "off", "custom", "off");
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", subuserID, false);
		webApp2.setUserSttingToggles("on", "off", "off", "custom", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);


		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

//		phoneApp1.validateDurationInCallingScreen(1);
//		Thread.sleep(2000);
//		phoneApp1.clickOnDialPad();
//		Thread.sleep(2000);
//		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(8000);
		//phoneApp4.waitForIncomingCallingScreen();
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);

		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}

	@Test(priority = 40, retryAnalyzer = Retry.class)
	public void Outgoing_No_Answer_Incoming_Missed_Miss_By_Forward_To_Device_Both_User_custom_available() throws Exception {

		
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, true);
		webApp2.setUserSttingToggles("on", "off", "off", "custom", "off");
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", subuserID, true);
		webApp2.setUserSttingToggles("on", "off", "off", "custom", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);


		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

//		phoneApp1.validateDurationInCallingScreen(1);
//		Thread.sleep(2000);
//		phoneApp1.clickOnDialPad();
//		Thread.sleep(2000);
//		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		} catch (AssertionError  e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		}
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);

		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		try {
			assertEquals("No Answer", webApp.validateCallStatus());
		    } catch (AssertionError e) {
			assertEquals("Rejected", webApp.validateCallStatus());
		    }
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 41, retryAnalyzer = Retry.class)
	public void Outgoing_No_Answer_Incoming_Missed_Miss_By_Forward_To_Device_of_Both_User_Number_custom_available() throws Exception {

		
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, true);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);


		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

//		phoneApp1.validateDurationInCallingScreen(1);
//		Thread.sleep(2000);
//		phoneApp1.clickOnDialPad();
//		Thread.sleep(2000);
//		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		} catch (AssertionError  e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		}
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
        
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		try {
			assertEquals("No Answer", webApp.validateCallStatus());
		    } catch (AssertionError e) {
			assertEquals("Rejected", webApp.validateCallStatus());
		    }
		assertEquals("-", webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	
	@Test(priority = 42, retryAnalyzer = Retry.class)
	public void Outgoing_No_Answer_Incoming_Missed_Miss_By_Forward_To_Device_of_Both_User_Number_custom_Unavailable() throws Exception {

		
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "custom", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);


		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();


		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(25000);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2SubUser.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("-", webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Voicemail", webApp2.validateCallStatus());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);

		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
	    assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 43, retryAnalyzer = Retry.class)
	public void simulteneously_Outgoing_COMPLETED_Incoming_COMPLETED_Answr_By_FTD_of_Main_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
        phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		phoneApp3.clickOnIncomingHangupButton();
		Thread.sleep(3000);
		//phoneApp4.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}
	@Test(priority = 44, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answer_By_FdTD_of_MAin_User() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp3.clickOnIncomingHangupButton();
		//phoneApp4.clickOnIncomingRejectButton();
		Thread.sleep(5000);
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}
	@Test(priority = 45, retryAnalyzer = Retry.class)
	public void Team_simulteneously_Outgoing_Completed_Incoming_Answer_By_FdTD_of_Sub_User() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(5000);
		phoneApp4.clickOnIncomingHangupButton();
		//phoneApp4.clickOnIncomingRejectButton();
		Thread.sleep(5000);
		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
		Common.pause(3);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
		Common.pause(3);

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not  Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not  Display----");
	}
	@Test(priority = 46, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answere_By_Ftd_Of_Main_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(4000);
		phoneApp3.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
	assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 47, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answere_By_Ftd_Of_SUb_Users() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp4.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
	assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 48, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answere_By_Ftd_Of_Main_Users_Sub_user_Always_CLosed() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp3.clickOnIncomimgAcceptCallButton();
		//phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp3.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
	assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 49, retryAnalyzer = Retry.class)
	public void IVR_Team_simulteneously_Outgoing_Completed_Incoming_Answere_By_Ftd_Of_Sub_Users_Main_user_Always_CLosed() throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(9000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		Thread.sleep(9000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnSimultaneously();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp4.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp4.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();

		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
	assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());

		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 50, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Answered_By_FTD_of_Main_Users_Missed_By_SUb_USer()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp3.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();


		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 51, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Answered_By_FTD_of_Sub_User_Missed_By_MAin_USer()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp4.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp4.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();


		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 52, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Answered_By_FTD_of_Main_Users_SUb_USer_Always_Closed()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp3.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();


		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 53, retryAnalyzer = Retry.class)
	public void IVR_Team_FixedOrder_Outgoing_Completed_Incoming_Answered_By_FTD_of_Sub_USer_MAin_USer_Always_CLosed()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(9000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		Thread.sleep(9000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp4.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp4.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();


		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 54, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_Ongoing_Call_Incoming_queued_call_served_to_user_FTD_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		 phoneApp2.waitForIncomingCallingScreen();
		 phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(5000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp4.waitForDialerPage();
		
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 55, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Incoming_Ongoing_call_Incoming_queued_call_served_to_FTD_and_Missed()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		 phoneApp2.waitForIncomingCallingScreen();
		 phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(5000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp4.waitForDialerPage();
		
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.waitForDialerPage();


	//	phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail(),
				"--Missed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Missed", webApp2.validateCallStatus(), "--Missed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	@Test(priority = 56, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_Ongoing_Call_Incoming_queued_call_served_to_user_FTD_and_Rejected()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		 phoneApp2.waitForIncomingCallingScreen();
		 phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(5000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp4.waitForDialerPage();
		
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();

		phoneApp3.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail(),
				"--Rejected call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 57, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_ACW_Incoming_queued_call_served_to_user_FTD_and_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Thread.sleep(2000);
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "on", "off", "open", "off");
		driver1.get(url.dialerSignIn());
		driver2.get(url.dialerSignIn());
		Common.pause(10);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		 phoneApp2.waitForIncomingCallingScreen();
		 phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(5000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp4.waitForDialerPage();
		Thread.sleep(2000);
		phoneApp2.clickOnEndACWButton();
		
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		Thread.sleep(10000);

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 58, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_incoming_Ongoing_Call_Incoming_queued_call_served_to_FTD_Rejected_User_missed_Voicemail()
			throws Exception {
		
		
		driver1.get(url.dialerSignIn());
		driver2.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp4.enterNumberinDialer(number);
		phoneApp4.clickOnDialButton();

		 phoneApp2.waitForIncomingCallingScreen();
		 phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(5000);
		phoneApp4.clickOnOutgoingHangupButton();
		phoneApp4.waitForDialerPage();
		
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();

		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		Thread.sleep(10000);
		
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	

	@Test(priority = 59, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_ongoing_call_Incoming_queued_call_served_to_FTD_Missed_and_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "on", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account4Number1);
		phoneApp2.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForIncomingCallingScreen();
		Thread.sleep(10000);
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Department name of dialer1 call log detail page--");
		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
	}

	
	@Test(priority = 60, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ringing_Incoming_queued_call_served_to_FTD_After_WelcomeMessage_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account4Number1);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	@Test(priority = 61, retryAnalyzer = Retry.class)
	public void call_queue_user_busy_with_Outgoing_Ringing_Incoming_queued_call_served_to_FTD_After_IVR_Accepted()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account4Number1);
		phoneApp2.clickOnDialButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		
		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();
		Thread.sleep(10000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls(), "--Number on dialer 1 all call page--");
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls(),
				"--Department name of dialer 1 all calls page--");
		assertEquals("Outgoing", phoneApp1.validatecallType(), "--Outgoing Call type in dialer 1--");

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail(), "--Number in call log detail of dialer1--");
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer1--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp1.validateDurationInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 1--");

		phoneApp2.clickOnSideMenu();
		String callerName = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls(), "--Number on dialer 2 all call page--");
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls(),
				"--Department name of dialer 2 all calls page--");
		assertEquals("Incoming", phoneApp2.validatecallType(), "--Incoming Call type in dialer 2--");

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail(), "--Number in call log detail of dialer2--");
		softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
				"--Duration of outgoing call in call detail page of dialer 2--");
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
				"--Completed call status call log detail of dialer2--");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail(),
				"--Department name of dialer2 call log detail page--");

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals(callerName, webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Clent number in web2 Activityfeed page--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost2, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	@Test(priority = 70, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Answered_By_FTD_of_Main_Users_SUb_USer_Always_Closed()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp3.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();


		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 71, retryAnalyzer = Retry.class)
	public void Team_RoundRobin_Outgoing_Completed_Incoming_Answered_By_FTD_of_Sub_USer_MAin_USer_Always_CLosed()
			throws Exception {

		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(9000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "closed", "off");
		Thread.sleep(9000);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp4.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Thread.sleep(7000);
		phoneApp4.clickOnIncomingHangupButton();
		Common.pause(3);
		phoneApp1.waitForDialerPage();


		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Sub = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "----webApp2 Call Type Should Incoming----");
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Sub, webApp2.validateCallerName());
		assertEquals(number2, webApp2.validateClientNumber());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(callCost2, webApp2.validateCallCost());

		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "----webApp Call Type Should Outgoing----");
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName, webApp.validateCallerName());
		assertEquals(number, webApp.validateClientNumber());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should Display----");

	}
	@Test(priority = 72, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_Robin_Outgoing_Completed_Incoming_Answered_By_Main_user_FTD_Missed_By_sub_user() throws Exception {
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(9000);
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			Thread.sleep(7000);
			phoneApp3.clickOnIncomingHangupButton();
			Common.pause(3);
			phoneApp3.waitForDialerPage();	
		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp4.waitForIncomingCallingScreen();
			Common.pause(10);
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			Thread.sleep(7000);
			phoneApp3.clickOnIncomingHangupButton();
			Common.pause(3);
			phoneApp3.waitForDialerPage();
		}
		
		Common.pause(5);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		//phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(CallerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
	
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	@Test(priority = 73, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Team_Robin_Outgoing_Completed_Incoming_Answered_By_Sub_user_FTD_Missed_By_Main_user() throws Exception {
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(9000);
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			Thread.sleep(7000);
			phoneApp4.clickOnIncomingHangupButton();
			Common.pause(3);
			phoneApp4.waitForDialerPage();	
		} else {
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp3.waitForIncomingCallingScreen();
			Common.pause(20);
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			Thread.sleep(7000);
			phoneApp4.clickOnIncomingHangupButton();
			Common.pause(3);
			phoneApp4.waitForDialerPage();
		}
		
		Common.pause(5);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		//phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());
		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());
		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());
		phoneApp2SubUser.clickOnRightArrow();
		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Subuser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 89, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_Robin_Outgoing_Completed_Incoming_Answered_By_Main_user_FTD_Missed_By_sub_user_And_Vice_Versa() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		driver4.get(url.usersPage());
		Thread.sleep(4000);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account5Number1);
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForDialerPage();	
		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			Common.pause(20);
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForDialerPage();
		}
		
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp3.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp3.pressIVRKey("2");
		
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp5.clickOnIncomingHangupButton();
		phoneApp4.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		//phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number3, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number3, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Subuser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentNameFor2ndEntry());
		assertEquals(CallerName2, webApp2.validateCallerNameFor2ndEntry());
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(), "---webApp2 Call Type Should Incoming----");
		//assertEquals(callCost2, webApp2.validateCallCostFor2ndEntry());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "----webApp2 Recording Should not Display----");
		
		
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 90, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void Robin_Outgoing_Completed_Incoming_Answered_By_Main_user_FTD_and_Sub_User_AlwaysClosed() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		webApp2.enterFTDnumber(account5Number1);
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");


		phoneApp2.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp4.clickOnIncomingHangupButton();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		
		
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 91, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void IVR_Team_Round_Robin_Outgoing_Completed_Incoming_Answered_By_Sub_user_FTD_and_Main_User_AlwaysClosed () throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "close", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account5Number1);
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp5.waitForIncomingCallingScreen();
		phoneApp5.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp5.clickOnIncomingHangupButton();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2subuser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		
		
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	@Test(priority = 92, groups = { "team_roundrobin" }, retryAnalyzer = Retry.class)
	public void  IVR_Team_Round_Robin_Outgoing_Completed_Incoming_Answered_By_Sub_user_FTD_Missed_By_Main_user() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("Team");
		Thread.sleep(4000);
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		driver4.get(url.usersPage());
		Thread.sleep(4000);
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account5Number1);
		Thread.sleep(4000);
		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(2000);
		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey("2");

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp4.waitForIncomingCallingScreen();
			Common.pause(20);
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp5.clickOnIncomimgAcceptCallButton();	
		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp5.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp5.clickOnIncomingHangupButton();
			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();
			phoneApp1.validateDurationInCallingScreen(1);
			Thread.sleep(2000);
			phoneApp1.clickOnDialPad();
			Thread.sleep(2000);
			phoneApp1.pressIVRKey("2");
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp4.waitForIncomingCallingScreen();
			Common.pause(20);
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp5.clickOnIncomimgAcceptCallButton();
		}
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp5.clickOnIncomingHangupButton();
		//phoneApp4.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(number, phoneApp1.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number, phoneApp1.validateNumberInCallDetail());

		// validate call status in Call detail page
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		// validate department name in Call detail page
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String CallerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		//phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);
		// validate Number in All Calls page
		assertEquals(number2, phoneApp2SubUser.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(number2, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2Subuser, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		
		
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should  Display----");
	}
	
	@Test(priority = 93, retryAnalyzer = Retry.class)
	public void Call_Recording_OFF_Outgoing_Completed_Incoming_Voicemail_Rejected_By_Main_User_and_FTD() throws Exception {
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.setCallRecordingToggle("off");	
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");

		webApp.numberspage();
		webApp.navigateToNumberSettingpage(number2);
		Common.pause(3);
		webApp.setCallRecordingToggle("off");	
		
		
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(7);
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(15);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		// assertEquals("Voicemail", phoneApp2.validateCallStatusInCallDetail());
		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		//assertEquals(callerName2, webApp2.validateCallerName()); 
		//assertEquals("Voicemail", webApp2.validateCallStatus());
		assertTrue("Voicemail".equalsIgnoreCase(webApp2.validateCallStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}
	
	@Test(priority = 94, retryAnalyzer = Retry.class)
	public void Call_Recording_OFF_Outgoing_Answer_Incoming_Answer_By_Forward_To_Device() throws Exception {
		
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(3);
		webApp2.setCallRecordingToggle("off");	
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		webApp.numberspage();
		webApp.navigateToNumberSettingpage(number2);
		Common.pause(3);
		webApp.setCallRecordingToggle("off");	
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		Thread.sleep(10000);
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());
		phoneApp1.clickOnRightArrow();
		assertEquals(number, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());
		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals(callerName2, webApp2.validateCallerName());
		assertEquals("Completed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
		assertEquals(callCost2, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");
		driver6.get(url.signIn());
		webApp.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp.validateDepartmentName());
		assertEquals(callerName1, webApp.validateCallerName());
		assertEquals("Completed", webApp.validateCallStatus());
		assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
		assertEquals(outgoingCallPrice, webApp.validateCallCost());
		webApp.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
	}
	
	@Test(priority =95, retryAnalyzer = Retry.class)
	public void verify_value_of_Show_CallHippo_Number_in_Incoming_Forwarded_Calls_true() throws Exception {
		
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.showCallHippoNumberIncomingForwardedCalls("on");
		Thread.sleep(2000);
		assertEquals(webApp2.validateshowCallHippoNumberIncomingForwardedCallsDescriptionIsDisplayed(),true);
	
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(4000);
		
		driver4.get(url.usersPage());
		Common.pause(3);
		webApp2.navigateToUserSettingPage(account2EmailMainUser);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
		webApp2.enterFTDnumber(account4Number1);
		Thread.sleep(2000);
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp4.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());
		
		
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
		assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
		
		phoneApp4.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName4, phoneApp4.validateCallingScreenIncomingVia());
		assertEquals(number, phoneApp4.validateCallingScreenIncomingNumber());
		
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp4.waitForDialerPage();

	}
	//custom_Ringing_timing
		@Test(priority =96, retryAnalyzer = Retry.class)
		public void verify_Forward_to_device_custom_Ringing_timing() throws Exception {
			System.out.print("Start......");
			driver4.get(url.numbersPage());
		
			webApp2.navigateToNumberSettingpage(number);
			
			Thread.sleep(9000);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			
			Common.pause(3);
			webApp2.customRingingTime("on");
			System.out.println("customRingingTime ON");
			Common.pause(3);
			webApp2.enterDuration_customRingingTime("20");
			System.out.println(webApp2.validateSuccessMessage());
			assertEquals(webApp2.validateSuccessMessage(), "Number updated successfully.");
			assertEquals(webApp2.validatecustomRingingTimeUploadedSuccessfully(), "20");
			
			//*********************************************
			
			phoneApp1.enterNumberinDialer(number);
			String departmentName = phoneApp1.validateDepartmentNameinDialpade();
			String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
			String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
//			phoneApp1.waitForDialerPage();
//			phoneApp3.waitForDialerPage();
			phoneApp3.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName3, phoneApp3.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp3.validateCallingScreenIncomingNumber());
			System.out.println("call connect.........");

			Common.pause(20); // 20 sec ringing
			assertEquals(phoneApp3.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
			assertEquals(phoneApp1.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");
			assertEquals(phoneApp2.validateDialerScreen(), true,"--validate dialer screen after completed custom incoming Ringing time--");

			Thread.sleep(10000);

			phoneApp1.clickOnSideMenu();
			String callerName1 = phoneApp1.getCallerName();
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number, phoneApp1.validateNumberInAllcalls());
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
			assertEquals("Outgoing", phoneApp1.validatecallType());

			phoneApp1.clickOnRightArrow();
			assertEquals(number, phoneApp1.validateNumberInCallDetail());
			try {
				assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
				} catch (AssertionError  e) {
				assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
				}
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number2, phoneApp2.validateNumberInAllcalls());
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
			assertEquals("Incoming", phoneApp2.validatecallType());

			phoneApp2.clickOnRightArrow();
			assertEquals(number2, phoneApp2.validateNumberInCallDetail());

			assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName());
			assertEquals("-", webApp2.validateCallerName());
			assertEquals("Missed", webApp2.validateCallStatus());
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
			assertEquals("-", webApp2.validateCallCost());
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName, webApp.validateDepartmentName());
			assertEquals(callerName1, webApp.validateCallerName());
			try {
				assertEquals("No Answer", webApp.validateCallStatus());
			    } catch (AssertionError e) {
				assertEquals("Rejected", webApp.validateCallStatus());
			    }
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
			assertEquals("-", webApp.validateCallCost());
			webApp.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
		}
		
		//Multiple FTD
		@Test(priority = 97, retryAnalyzer = Retry.class)
		public void Outgoing_No_Answer_Incoming_Missed_By_1st_Forward_To_Device_number_And_verify_in_Activity_Report() throws Exception {
			driver4.get(url.usersPage());

			Common.pause(3);
			webApp2.navigateToUserSettingPage(account2EmailMainUser);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
			webApp2.enterFTDnumber2(account5Number1);
			Thread.sleep(2000);

			phoneApp1.enterNumberinDialer(number);
			String departmentName = phoneApp1.validateDepartmentNameinDialpade();
			String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			//phoneApp5.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			//phoneApp5.clickOnIncomingHangupButton();
			phoneApp2.waitForDialerPage();
			phoneApp3.waitForDialerPage();
			phoneApp5.waitForDialerPage();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp1.clickOnSideMenu();
			String callerName1 = phoneApp1.getCallerName();
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number, phoneApp1.validateNumberInAllcalls());
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
			assertEquals("Outgoing", phoneApp1.validatecallType());

			phoneApp1.clickOnRightArrow();
			assertEquals(number, phoneApp1.validateNumberInCallDetail());
			try {
				assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
				} catch (AssertionError  e) {
				assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
				}
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number2, phoneApp2.validateNumberInAllcalls());
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
			assertEquals("Incoming", phoneApp2.validatecallType());

			phoneApp2.clickOnRightArrow();
			assertEquals(number2, phoneApp2.validateNumberInCallDetail());

			assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName());
			assertEquals("-", webApp2.validateCallerName());
			assertEquals("Missed", webApp2.validateCallStatus());
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
			assertEquals("-", webApp2.validateCallCost());
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName, webApp.validateDepartmentName());
			assertEquals(callerName1, webApp.validateCallerName());
			try {
				assertEquals("No Answer", webApp.validateCallStatus());
			    } catch (AssertionError e) {
				assertEquals("Rejected", webApp.validateCallStatus());
			    }
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
			assertEquals("-", webApp.validateCallCost());
			webApp.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
			
			//validate in activity feed report
//		    csv.deleteAllFile("activityFeedReport"); 
//		    webApp2.clickOnCloseiButton();
//			webApp2.clickOnActivitFeed();
//			Common.pause(3);
//		   
//			webApp2.clickOnCheckBoxOption(record);
//			Common.pause(3);
//			assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//			webApp2.clickOnFileDownload();
//			
//			Common.pause(8);
//			
//			dashboard.ValidateActivityFeedReportOnGmail();
//			Common.pause(3);
//			assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//			webApp2.clickOnActivitFeed();
//			Common.pause(5);
//		
//			//validate caller-CSV with User-Web
//			validateRecord("caller","User");
//			
//			//validate callStatus-CSV with Status-Web
//			validateRecord("callStatus","Status");
//			
//			//validate forwardedOn-CSV with User-Web
//			validateRecord("forwardedOn","-");
			

		}
		@Test(priority = 98, retryAnalyzer = Retry.class)
		public void Outgoing_Rejected_Incoming_Rejected_By_1st_Forward_To_Device_number_And_Incoming_Rejected_By_Last_Forward_To_Device_number_And_verify_in_Activity_Report() throws Exception {
			driver4.get(url.usersPage());

			Common.pause(3);
			webApp2.navigateToUserSettingPage(account2EmailMainUser);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
			webApp2.enterFTDnumber2(account5Number1);
			Thread.sleep(2000);

			phoneApp1.enterNumberinDialer(number);
			String departmentName = phoneApp1.validateDepartmentNameinDialpade();
			String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			
			//1.reject by First FTD number
			phoneApp3.clickOnIncomingRejectButton();
			Common.pause(5);
			phoneApp3.waitForDialerPage();
			
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			//phoneApp5.clickOnIncomingHangupButton();
			phoneApp2.waitForDialerPage();
			
			phoneApp5.waitForDialerPage();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp1.clickOnSideMenu();
			String callerName1 = phoneApp1.getCallerName();
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number, phoneApp1.validateNumberInAllcalls());
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
			assertEquals("Outgoing", phoneApp1.validatecallType());

			phoneApp1.clickOnRightArrow();
			assertEquals(number, phoneApp1.validateNumberInCallDetail());
			assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number2, phoneApp2.validateNumberInAllcalls());
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
			assertEquals("Incoming", phoneApp2.validatecallType());

			phoneApp2.clickOnRightArrow();
			assertEquals(number2, phoneApp2.validateNumberInCallDetail());

			assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName());
			assertEquals(callerName2, webApp2.validateCallerName());
			assertEquals("Rejected", webApp2.validateCallStatus());
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
			assertEquals("-", webApp2.validateCallCost());
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName, webApp.validateDepartmentName());
			assertEquals(callerName1, webApp.validateCallerName());
			
			assertEquals("Rejected", webApp.validateCallStatus());
			    
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
			assertEquals("-", webApp.validateCallCost());
			webApp.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
			
			//validate in activity feed report
//		    csv.deleteAllFile("activityFeedReport"); 
//		    webApp2.clickOnCloseiButton();
//			webApp2.clickOnActivitFeed();
//			Common.pause(3);
//		   
//			webApp2.clickOnCheckBoxOption(record);
//			Common.pause(3);
//			assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//			webApp2.clickOnFileDownload();
//			
//			Common.pause(8);
//			
//			dashboard.ValidateActivityFeedReportOnGmail();
//			Common.pause(3);
//			assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//			webApp2.clickOnActivitFeed();
//			Common.pause(5);
//		
//			//validate caller-CSV with User-Web
//			validateRecord("caller","User");
//			
//			//validate callStatus-CSV with Status-Web
//			validateRecord("callStatus","Status");
//			
//			//validate forwardedOn-CSV with User-Web
//			validateRecord("forwardedOn","-");
			
			
			//********************************** Action on Last Forwarded Device Number ******************************
			driver.get(url.dialerSignIn());
			phoneApp1.waitForDialerPage();
			driver1.get(url.dialerSignIn());
			phoneApp2.waitForDialerPage();
			
			phoneApp1.enterNumberinDialer(number);
			
			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			
			//2.reject by last FTD number
			phoneApp5.clickOnIncomingRejectButton();
			Common.pause(5);
			phoneApp5.waitForDialerPage();
			
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
	
			phoneApp2.waitForDialerPage();
			phoneApp3.waitForDialerPage();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp1.clickOnSideMenu();
			
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number, phoneApp1.validateNumberInAllcalls());
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
			assertEquals("Outgoing", phoneApp1.validatecallType());

			phoneApp1.clickOnRightArrow();
			assertEquals(number, phoneApp1.validateNumberInCallDetail());
			assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

			phoneApp2.clickOnSideMenu();
			
			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number2, phoneApp2.validateNumberInAllcalls());
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
			assertEquals("Incoming", phoneApp2.validatecallType());

			phoneApp2.clickOnRightArrow();
			assertEquals(number2, phoneApp2.validateNumberInCallDetail());

			assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName());
			assertEquals(callerName2, webApp2.validateCallerName());
			assertEquals("Rejected", webApp2.validateCallStatus());
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
			assertEquals("-", webApp2.validateCallCost());
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName, webApp.validateDepartmentName());
			assertEquals(callerName1, webApp.validateCallerName());
			
			assertEquals("Rejected", webApp.validateCallStatus());
			    
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
			assertEquals("-", webApp.validateCallCost());
			webApp.clickOniButton();
			Common.pause(5);
			assertEquals(false, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
			
			//validate in activity feed report
//		    csv.deleteAllFile("activityFeedReport"); 
//		    webApp2.clickOnCloseiButton();
//			webApp2.clickOnActivitFeed();
//			Common.pause(3);
//		   
//			webApp2.clickOnCheckBoxOption(record);
//			Common.pause(3);
//			assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//			webApp2.clickOnFileDownload();
//			
//			Common.pause(8);
//			
//			dashboard.ValidateActivityFeedReportOnGmail();
//			Common.pause(3);
//			assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//			webApp2.clickOnActivitFeed();
//			Common.pause(5);
//		
//			//validate caller-CSV with User-Web
//			validateRecord("caller","User");
//			
//			//validate callStatus-CSV with Status-Web
//			validateRecord("callStatus","Status");
//			
//			//validate forwardedOn-CSV with User-Web
//			validateRecord("forwardedOn","-");


		}
		@Test(priority = 99, retryAnalyzer = Retry.class)
		public void Outgoing_Completed_Incoming_Answer_By_1st_Forward_To_Device_number_And_Incoming_Answer_By_Last_Forward_To_Device_number_And_verify_in_Activity_Report() throws Exception {
			driver4.get(url.usersPage());

			Common.pause(3);
			webApp2.navigateToUserSettingPage(account2EmailMainUser);
			Thread.sleep(9000);
			webApp2.setUserSttingToggles("on", "off", "off", "open", "off");
			webApp2.enterFTDnumber2(account5Number1);
			Thread.sleep(2000);

			phoneApp1.enterNumberinDialer(number);
			String departmentName = phoneApp1.validateDepartmentNameinDialpade();
			String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			
			//1.Answer by First FTD number
			phoneApp3.clickOnIncomimgAcceptCallButton();
			Common.pause(7);
			phoneApp3.clickOnIncomingHangupButton();
			phoneApp2.waitForDialerPage();
			phoneApp3.waitForDialerPage();
			phoneApp5.waitForDialerPage();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp1.clickOnSideMenu();
			String callerName1 = phoneApp1.getCallerName();
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number, phoneApp1.validateNumberInAllcalls());
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
			assertEquals("Outgoing", phoneApp1.validatecallType());

			phoneApp1.clickOnRightArrow();
			assertEquals(number, phoneApp1.validateNumberInCallDetail());
			assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number2, phoneApp2.validateNumberInAllcalls());
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
			assertEquals("Incoming", phoneApp2.validatecallType());

			phoneApp2.clickOnRightArrow();
			assertEquals(number2, phoneApp2.validateNumberInCallDetail());

			assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName());
			assertEquals(callerName2, webApp2.validateCallerName());
			assertEquals("Completed", webApp2.validateCallStatus());
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
			assertEquals(callCost2, webApp2.validateCallCost());
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName, webApp.validateDepartmentName());
			assertEquals(callerName1, webApp.validateCallerName());
			assertEquals("Completed", webApp.validateCallStatus());
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
			assertEquals(outgoingCallPrice, webApp.validateCallCost());
			webApp.clickOniButton();
			Common.pause(5);
			assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
			
			//validate in activity feed report
//		    csv.deleteAllFile("activityFeedReport"); 
//		    webApp2.clickOnCloseiButton();
//			webApp2.clickOnActivitFeed();
//			Common.pause(3);
//		   
//			webApp2.clickOnCheckBoxOption(record);
//			Common.pause(3);
//			assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//			webApp2.clickOnFileDownload();
//			
//			Common.pause(8);
//			
//			dashboard.ValidateActivityFeedReportOnGmail();
//			Common.pause(3);
//			assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//			webApp2.clickOnActivitFeed();
//			Common.pause(5);
//		
//			//validate caller-CSV with User-Web
//			validateRecord("caller","User");
//			
//			//validate callStatus-CSV with Status-Web
//			validateRecord("callStatus","Status");
//			
//			//validate forwardedOn-CSV with User-Web
//			validateRecord("forwardedOn",number3);
			
			
			//********************************** Action on Last Forwarded Device Number ******************************
			driver.get(url.dialerSignIn());
			phoneApp1.waitForDialerPage();
			driver1.get(url.dialerSignIn());
			phoneApp2.waitForDialerPage();
			
			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp5.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			
			//2.Answer by Last FTD number
			phoneApp5.clickOnIncomimgAcceptCallButton();
			Common.pause(7);
			phoneApp5.clickOnIncomingHangupButton();
			phoneApp2.waitForDialerPage();
			phoneApp3.waitForDialerPage();
			phoneApp5.waitForDialerPage();
			phoneApp2SubUser.waitForDialerPage();

			phoneApp1.waitForDialerPage();

			Thread.sleep(10000);

			phoneApp1.clickOnSideMenu();
			
			phoneApp1.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number, phoneApp1.validateNumberInAllcalls());
			assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());
			assertEquals("Outgoing", phoneApp1.validatecallType());

			phoneApp1.clickOnRightArrow();
			assertEquals(number, phoneApp1.validateNumberInCallDetail());
			assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

			phoneApp2.clickOnSideMenu();
			
			phoneApp2.clickOnAllCalls();
			Thread.sleep(4000);
			assertEquals(number2, phoneApp2.validateNumberInAllcalls());
			assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
			assertEquals("Incoming", phoneApp2.validatecallType());

			phoneApp2.clickOnRightArrow();
			assertEquals(number2, phoneApp2.validateNumberInCallDetail());

			assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
			assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

			driver4.get(url.signIn());
			webApp2.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName2, webApp2.validateDepartmentName());
			assertEquals(callerName2, webApp2.validateCallerName());
			assertEquals("Completed", webApp2.validateCallStatus());
			assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(), "---webApp2 Call Type Should Incoming----");
			assertEquals(callCost2, webApp2.validateCallCost());
			webApp2.clickOniButton();
			Common.pause(5);
			assertEquals(true, webApp2.validateRecordingUrl(), "----webApp2 Recording Should not Display----");

			driver6.get(url.signIn());
			webApp.clickOnActivitFeed();
			Common.pause(5);
			assertEquals(departmentName, webApp.validateDepartmentName());
			assertEquals(callerName1, webApp.validateCallerName());
			assertEquals("Completed", webApp.validateCallStatus());
			assertEquals(true, webApp.validateOutgoingCallTypeIsDisplayed(), "---webApp Call Type Should Outgoing----");
			assertEquals(outgoingCallPrice, webApp.validateCallCost());
			webApp.clickOniButton();
			Common.pause(5);
			assertEquals(true, webApp.validateRecordingUrl(), "----webApp Recording Should not Display----");
			
			//validate in activity feed report
//		    csv.deleteAllFile("activityFeedReport"); 
//		    webApp2.clickOnCloseiButton();
//			webApp2.clickOnActivitFeed();
//			Common.pause(3);
//		   
//			webApp2.clickOnCheckBoxOption(record);
//			Common.pause(3);
//			assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
//			webApp2.clickOnFileDownload();
//			
//			Common.pause(8);
//			
//			dashboard.ValidateActivityFeedReportOnGmail();
//			Common.pause(3);
//			assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
//			webApp2.clickOnActivitFeed();
//			Common.pause(5);
//		
//			//validate caller-CSV with User-Web
//			validateRecord("caller","User");
//			
//			//validate callStatus-CSV with Status-Web
//			validateRecord("callStatus","Status");
//			
//			//validate forwardedOn-CSV with User-Web
//			validateRecord("forwardedOn",account5Number1);


		}
		
		

}
