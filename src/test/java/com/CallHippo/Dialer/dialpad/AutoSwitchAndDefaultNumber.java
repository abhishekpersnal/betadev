package com.CallHippo.Dialer.dialpad;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.TestBase;

public class AutoSwitchAndDefaultNumber {
	
	DialerLoginPage dialerloginpage;
	WebDriver driverphoneApp1;
	WebDriver driverphoneApp2;

	WebDriver driverWebApp2;
	WebToggleConfiguration webApp2;
	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerIndex phoneApp2Subuser;
	PropertiesFile url= new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data= new PropertiesFile("Data\\url Configuration.properties");

	public AutoSwitchAndDefaultNumber() throws Exception {
		
	}
	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2Number1 = data.getValue("account2Number1"); // account 2's number
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String account1Number1 = data.getValue("account1Number1");

	@BeforeTest
	public void initialization() throws Exception {
		
		driverphoneApp1 = TestBase.init_dialer();
		System.out.println("Opened phoneApp1 session");
		phoneApp1 = PageFactory.initElements(driverphoneApp1, DialerIndex.class);

		driverphoneApp2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driverphoneApp2, DialerIndex.class);
		dialerloginpage = PageFactory.initElements(driverphoneApp2, DialerLoginPage.class);
	
		
		driverWebApp2 = TestBase.init();
		System.out.println("Opened webapp2 session");
		webApp2 = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		
		
		try {
			try {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneApp1 signin page");
				loginDialer(phoneApp2, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}
			try {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}
			
			try {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}
			

			
			try {
			
				phoneApp1.deleteAllContactsFromDialer();
				phoneApp2.deleteAllContactsFromDialer();
				phoneApp2.afterCallWorkToggle("off");
				phoneApp2.globalConnectToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(5);
				System.out.println("loggedin webApp");
				
			}catch (Exception e) {	
				
				phoneApp1.deleteAllContactsFromDialer();
				phoneApp2.deleteAllContactsFromDialer();
				phoneApp2.afterCallWorkToggle("off");
				phoneApp2.globalConnectToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(5);
				System.out.println("loggedin webApp");
			
				Common.pause(5);
			}
		} catch (Exception e1) {
			String testname = "GlobalConnect Before Test ";
			
			Common.Screenshot(driverphoneApp2, testname, "PhoneApp2 Fail login");
			Common.Screenshot(driverphoneApp1, testname, "PhoneApp1 Fail login");
			Common.Screenshot(driverWebApp2, testname, "WebApp2 Fail login");
			
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}
	@BeforeMethod
	public void login() throws IOException, InterruptedException {
	try {
		  try {
			Common.pause(2);
			driverphoneApp2.navigate().to(url.dialerSignIn());
			driverphoneApp1.navigate().to(url.dialerSignIn());
			driverWebApp2.get(url.signIn());
		
			
			Common.pause(3);
			
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
	
		} catch (Exception e) {

			Common.pause(2);
			driverphoneApp2.navigate().to(url.dialerSignIn());	
			driverphoneApp1.navigate().to(url.dialerSignIn());	
			driverWebApp2.get(url.signIn());
			
			
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			
		} 
	}catch (Exception e2) {
			String testname = "GlobalConnect Before Method ";
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverphoneApp1, testname, "PhoneApp1 Fail login");
			Common.Screenshot(driverWebApp2, testname, "WebApp2 Fail login");
		
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverphoneApp2, testname, "PhoneApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp1, testname, "PhoneApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "WebApp2 Fail " + result.getMethod().getMethodName());
			
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			
		} else {
			String testname = "Pass";
			Common.Screenshot(driverphoneApp2, testname, "PhoneApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp1, testname, "PhoneApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
		//	System.out.println(testname + " - " + result.getMethod().getMethodName());
			
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driverphoneApp2.quit();
		driverphoneApp1.quit();
		driverWebApp2.quit();

	}	
	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_dialpad_after_login() throws Exception {
		
		Thread.sleep(9000);
		webApp2.numberspage();
		Thread.sleep(9000);
		String webDepartmentName=webApp2.getWebDepartmentName(account2Number1);
		Common.pause(5);
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true); //dial btn and dialpadtxt
		
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed()); //backbtn
		
		assertEquals(webDepartmentName, phoneApp2.validateDepartmentNameinDialpade()); //department name  
		  
		assertEquals(webApp2.getFlagFromWeb(), phoneApp2.getFlagFromDialer()); //flag  take from Web
				
		phoneApp2.enterNumberinDialer("+11069007290");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("America/Los_Angeles"));
		
		phoneApp2.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
	     assertEquals("+11069007290", phoneApp2.validateCallingScreenOutgoingNumber());
	     Common.pause(2);
	     phoneApp2.clickOnOutgoingHangupButton();
	     assertEquals(phoneApp2.validateDialerScreenDisplayed(),true);
	     
	     assertEquals("+1", phoneApp2.getNumberFromDialer()); //country code 
	     
	     assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("America/Los_Angeles")); //country time zone
		
	     // verify unsaved number
	     phoneApp2.enterNumberinDialer("+11069007290");
	     Common.pause(5);
	     assertEquals(true, phoneApp2.validateNewContactIsDisplayed()); //New contact
	     assertEquals(true, phoneApp2.validateSendMessageIsDisplayed()); //Send Message
	     
	     // verify Hard Reload dialer
	     System.out.print("refresh start");
	     phoneApp2.hardRefreshByKeyboard();
	     phoneApp2.waitForDialerPage();
	     assertEquals(true, phoneApp2.validateDialerScreenDisplayed()); //dialer page after reload
	     assertEquals("+1", phoneApp2.getNumberFromDialer());
	     
	     
	     //Alphabet should not Display in text box of dialer.
	     Common.pause(5);
	     phoneApp2.keyPressByKeyboard("c");
	     Common.pause(5);
	     phoneApp2.keyPressByKeyboard("1");
	     Common.pause(5);
	     phoneApp2.keyPressByKeyboard("a");
	     Common.pause(5);
	     phoneApp2.keyPressByKeyboard("l");
	     Common.pause(5);
	     assertEquals("+11", phoneApp2.getNumberFromDialer()); //only number 

	}
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_menu_panel_from_dialerPage() throws Exception {
		Thread.sleep(9000);
		webApp2.userspage();
		Thread.sleep(9000);
		String webUserName=webApp2.getusername(account2Email);
		Common.pause(3);
		String webEmail=webApp2.getWebEmail(account2Email);
		
		Common.pause(3);
		assertEquals(dialerloginpage.verifySlideMenuIconsAvailable(driverphoneApp2), true);
		assertEquals(webUserName, phoneApp2.getDialerUsername()); 
	
		assertTrue(webEmail.contains(phoneApp2.getDialerEmail()));
		
		
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Last_dial_number_on_dialer_and_Verify_number_After_Erase_the_number_from_dialer () throws Exception {
		
		phoneApp2.enterNumberinDialer(account2Number1);
		assertEquals(account2Number1, phoneApp2.getNumberFromDialer());
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account2Number1, phoneApp2.validateCallingScreenOutgoingNumber());
		 Common.pause(3);
	     phoneApp2.clickOnOutgoingHangupButton();
	     assertEquals(phoneApp2.validateDialerScreenDisplayed(),true);
	     
	     Common.pause(3);
	     phoneApp2.clickOnReDialButton();
	     Common.pause(3);
	     assertEquals(account2Number1, phoneApp2.getNumberFromDialer());
	     
	     phoneApp2.clearNumberinDialer();
		 Common.pause(3);
	     assertTrue(phoneApp2.getNumberFromDialer().isEmpty());
	     
	
	}
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Copy_paste_number_in_textbox_dialer_Click_on_dial_button_should_Allow_outgoing_call
	 () throws Exception {
		phoneApp2.enterNumberinDialer(account2Number1);
		assertEquals(account2Number1, phoneApp2.getNumberFromDialer());
		phoneApp2.ClickonAddContactOnDialer();
		phoneApp2.enterUserNameInContacts("+144@@3$$21%%43*& 153");
		phoneApp2.selectAllTextAndCopy();
		
		phoneApp2.ClickOnBackBtnEditcontactpage();
		Common.pause(3);
		assertEquals(phoneApp2.validateDialerScreenDisplayed(),true); 
		
		phoneApp2.clearNumberinDialer();
		Common.pause(3);
		assertTrue(phoneApp2.getNumberFromDialer().isEmpty());
		phoneApp2.pasteAllText();
		
		String num= phoneApp2.getNumberFromDialer();
		//verify discard unnecessary characters and special characters from number
		assertEquals(num, phoneApp2.getNumberFromDialer());
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(num, phoneApp2.validateCallingScreenOutgoingNumber());
		 Common.pause(3);
	     phoneApp2.clickOnOutgoingHangupButton();
	     assertEquals(phoneApp2.validateDialerScreenDisplayed(),true);
		
	}
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Search_saved_contact_from_dialer_and_make_outgoing_call() throws Exception {
       
		phoneApp2.globalConnectToggle("on");
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(6);
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed()); 
		
		phoneApp2.enterNumberinDialer("+910000000000");
		assertEquals("+910000000000", phoneApp2.getNumberFromDialer());
		phoneApp2.ClickonAddContactOnDialer();
		
		String name = phoneApp2.enterNameInContacts("Contact1");
		assertEquals(phoneApp2.ValidateNumberInNumberField("+910000000000"), true);
		
		String company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		Common.pause(5);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), "+910000000000");
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);
	
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
	
		phoneApp2.enterSomeNumberinDialer("+910000000000");
		assertEquals(phoneApp2.getsearchContactName(), name);
		assertEquals(phoneApp2.getsearchContactNumber(), "+910000000000");
		Common.pause(5);
		phoneApp2.clickOnsearchResult();
		assertEquals("+910000000000", phoneApp2.getNumberFromDialer());
		System.out.println(phoneApp2.getNumberFromDialer());
		Common.pause(5);
		//validate country date and time 
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
	
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals("+910000000000", phoneApp2.validateSubUserOutgoingNumberCallingScreen());
		
	}
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Outgoing_call_by_clicking_digits_from_dial_pad() throws Exception {
		
		phoneApp2.deleteAllContactsFromDialer();
		driverphoneApp2.get(url.dialerSignIn());
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		 //+16784986107
		 phoneApp2.enterNumberinDialer("+1");
	
		 phoneApp2.clickOnNumberFromDailpad(account1Number1);
		
//		 phoneApp2.clickOnNumberFromDailpad("7");
//		
//		 phoneApp2.clickOnNumberFromDailpad("8");
//		 assertEquals(true, phoneApp2.validateDialButtonIsEnabled());
//		 phoneApp2.clickOnNumberFromDailpad("4");
//		 phoneApp2.clickOnNumberFromDailpad("9");
//		 phoneApp2.clickOnNumberFromDailpad("8");
//		 phoneApp2.clickOnNumberFromDailpad("6");
//		 phoneApp2.clickOnNumberFromDailpad("1");
//		 phoneApp2.clickOnNumberFromDailpad("0");
//		 phoneApp2.clickOnNumberFromDailpad("7");
		 Common.pause(3);
	    // assertEquals("+16784986107", phoneApp2.getNumberFromDialer());
		 assertEquals(account1Number1, phoneApp2.getNumberFromDialer());
	     Common.pause(5);
	     assertEquals(true, phoneApp2.validateNewContactIsDisplayed()); //New contact
	     assertEquals(true, phoneApp2.validateSendMessageIsDisplayed()); //Send Message
	     
	     phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
	//	assertEquals("+16784986107", phoneApp2.validateCallingScreenOutgoingNumber());
			assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();
			
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(account2Number1, phoneApp1.validateCallingScreenIncomingNumber());

		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		assertEquals(phoneApp2.validateDialerScreenDisplayed(), true);
		 
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Outgoing_call_by_entering_numbers_from_keyboard() throws Exception {
		
		phoneApp2.deleteAllContactsFromDialer();
		driverphoneApp2.get(url.dialerSignIn());
		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		
		 phoneApp2.enterNumberinDialer("+1");
		 phoneApp2.keyPressByKeyboard(account1Number1);

//	     phoneApp2.keyPressByKeyboard("7");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("8");
//	     Common.pause(3);
//	     assertEquals(true, phoneApp2.validateDialButtonIsEnabled());
//	     phoneApp2.keyPressByKeyboard("4");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("9");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("8");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("6");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("1");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("0");
//	     Common.pause(3);
//	     phoneApp2.keyPressByKeyboard("7");
//	     Common.pause(3);
	  //   assertEquals("+16784986107", phoneApp2.getNumberFromDialer());
	     assertEquals(account1Number1, phoneApp2.getNumberFromDialer());
	     Common.pause(5);
	     assertEquals(true, phoneApp2.validateNewContactIsDisplayed()); //New contact
	     assertEquals(true, phoneApp2.validateSendMessageIsDisplayed()); //Send Message
	     
	     phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
	//	assertEquals("+16784986107", phoneApp2.validateCallingScreenOutgoingNumber());
		assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();
			
		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(account2Number1, phoneApp1.validateCallingScreenIncomingNumber());

		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		assertEquals(phoneApp2.validateDialerScreenDisplayed(), true);
	}
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_entering_and_Dailing_invalid_number_in_dialer() throws Exception {
		phoneApp2.enterNumberinDialer("52524");
		assertEquals("52524", phoneApp2.getNumberFromDialer());
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp2.validateCallingScreenOutgoingVia());
         
		String num= "+1"+"52524";
		// validate outgoing Number on outgoing calling screen
		assertEquals(num, phoneApp2.validateCallingScreenOutgoingNumber());
		 
		 //validate Warning message
		 assertEquals(true, phoneApp2.validateSuccessMessageForInvalidNumberInDilaer());
		 assertEquals(true, phoneApp2.validateDialerScreenDisplayed());
	
	}
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_open_dialer() throws Exception {
        phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());
		assertEquals(true, phoneApp2.validateDialButtonIsDisplayed());
	}
	
	
}
