package com.CallHippo.Dialer.contacts;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.AbstractPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.opencsv.CSVReader;

public class AddContactTest {
	DialerIndex phoneApp1;
	WebDriver driverphoneApp1;
	DialerIndex phoneApp2;
	WebDriver driverphoneApp2;
	DialerIndex phoneApp2Subuser;
	WebDriver driverphoneApp2Sub;

	WebToggleConfiguration webApp2;
	WebToggleConfiguration webApp1;
	WebDriver driverwebApp2;
	WebToggleConfiguration webApp2SubUser;
	HolidayPage webApp2HolidayPage;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel,csv;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number2 = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number1 = data.getValue("account1Number1"); // account 1's number
	String number3 = data.getValue("account3Number1"); // account 1's number
	String number4 = data.getValue("account4Number1"); // account 1's number
	String number5 = data.getValue("account5Number1"); // account 1's number

	String userID;
	String numberID;
	String contactName = "Cont" + Common.name();
	

//	String Name1 = "Cont"+name;
	String Name2 = "Contact2";
	String nameForOneNumber;
	String nameForTwoNumber;
	String nameforMainUser;
	String downloadPath=System.getProperty("user.dir") + "/download_files/";
	
	Common excelTestResult;
	
	
	ArrayList<String> csvNumbers = new ArrayList<String>();
	ArrayList<String> csvNames = new ArrayList<String>();


	public AddContactTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		csv= new Common();
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driverwebApp2 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driverwebApp2, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driverwebApp2, HolidayPage.class);

		driverphoneApp1 = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driverphoneApp1, DialerIndex.class);

		driverphoneApp2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driverphoneApp2, DialerIndex.class);

		driverphoneApp2Sub = TestBase.init_dialer();
		System.out.println("Opened phoneAap2Subuser session");
		phoneApp2Subuser = PageFactory.initElements(driverphoneApp2Sub, DialerIndex.class);

		try {
			try {
				driverwebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverwebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}
			try {
				driverphoneApp2Sub.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2subuser signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				System.out.println("loggedin phoneApp2subuser");
			} catch (Exception e) {
				driverphoneApp2Sub.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2subuser signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				System.out.println("loggedin phoneApp2subuser");
			}

			try {

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverphoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverphoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();
			}
		} catch (Exception e1) {
			String testname = "Add contact Before Method ";
			 Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverphoneApp2Sub, testname, "PhoneAppp3 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driverwebApp2.get(url.signIn());
			 driverphoneApp1.navigate().to(url.dialerSignIn());
			driverphoneApp2.navigate().to(url.dialerSignIn());
			driverphoneApp2Sub.navigate().to(url.dialerSignIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			phoneApp2.clickOnSideMenu();
			phoneApp2.clickOnCallPlannerLeftMenu();
			phoneApp2.waitforFirstEntryOfCallReminder();
			Common.pause(10);
			phoneApp2.deleteAllRemindersFromCallPlanner();

			phoneApp2.deleteAllContactsFromDialer();
			driverphoneApp2.get(url.dialerSignIn());
			
			phoneApp2.deleteAllSMS();
			driverphoneApp2.get(url.dialerSignIn());

			phoneApp2Subuser.deleteAllContactsFromDialer();
			driverphoneApp2Sub.get(url.dialerSignIn());

		} catch (Exception e) {

			try {
				Common.pause(2);
				driverwebApp2.get(url.signIn());
				driverphoneApp1.navigate().to(url.dialerSignIn());
				driverphoneApp2.navigate().to(url.dialerSignIn());
				driverphoneApp2Sub.navigate().to(url.dialerSignIn());

				Common.pause(3);
				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}

				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp2Subuser.deleteAllContactsFromDialer();
				driverphoneApp2Sub.get(url.dialerSignIn());

			} catch (Exception e1) {
				String testname = "Add Contact Before Method ";
				Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driverphoneApp2Sub, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2Sub, testname,
					"PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2Sub, testname,
					"PhoneAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebApp2, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		 driverphoneApp1.quit();
		driverphoneApp2.quit();
		driverphoneApp2Sub.quit();
		driverwebApp2.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	private void Add_contact_with_one_Number() throws Exception {
		String contactName = "Cont" + Common.name();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		nameForOneNumber = phoneApp2.enterNameInContacts(contactName);
		String Number = phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(number1);

		String Number2 = phoneApp2.EnternextNumberonAddContact(1, "+12345678900");
		phoneApp2.ClickOnSaveBtnAt(Number2);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(3);
		String Company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
		String Email = phoneApp2.EnterEmailOnAddContact(account2Email);

		// phoneApp2.clickOnTickGreenBtnToAddContactName();
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForOneNumber);
		assertEquals(phoneApp2.validateEmailIdOncontactDtlpage(), Email );
		assertEquals(phoneApp2.validateCompanyNameOncontactDtlpage(),Company );

	}

	private void Add_contact_with_Two_Number() throws Exception {
		String contactName = "Cont" + Common.name();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		nameForTwoNumber = phoneApp2.enterNameInContacts(contactName);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(number1);
		phoneApp2.EnternextNumberonAddContact(1, number3);
		phoneApp2.ClickOnSaveBtnAt(number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String Email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String Company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		// phoneApp2.clickOnTickGreenBtnToAddContactName();
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);
		assertEquals(phoneApp2.validateEmailIdOncontactDtlpage(), Email );
		assertEquals(phoneApp2.validateCompanyNameOncontactDtlpage(), Company);

	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_new_contact_By_Main_User() throws Exception {
		String contactName = "Cont" + Common.name();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		nameforMainUser = phoneApp2.enterNameInContacts(contactName);
		String Number = phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(5);
		phoneApp2.ClickOnAddBtnAt(number1);

		String Number2 = phoneApp2.EnternextNumberonAddContact(1, number3);
		phoneApp2.ClickOnSaveBtnAt(Number2);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		String Email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String Company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		Common.pause(1);
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameforMainUser);
		assertEquals(phoneApp2.validateEmailIdOncontactDtlpage(), Email);
		assertEquals(phoneApp2.validateCompanyNameOncontactDtlpage(), Company);

		phoneApp2Subuser.clickOnSideMenu();
		phoneApp2Subuser.clickoncontactpage();
		assertEquals(phoneApp2Subuser.validateNameOncontactpage(), nameforMainUser);

	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_contcat_from_Dialpad() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.enterNumberinDialer(number1);
		phoneApp2.ClickonAddContactOnDialer();
		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);

		String Name = phoneApp2.enterNameInContacts(contactName);
		Common.pause(5);
		assertEquals(phoneApp2.ValidateSaveBtnEnable(), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);

		String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
		String Cpmpany = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		phoneApp2.clickOnTickGreenBtnToAddContactName();

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);

	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_contact_from_All_calls_page() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		Common.pause(3);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
		phoneApp2.clickOnCallDetailPageAddButton();
		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
		
		String Name = phoneApp2.enterNameInContacts(contactName);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
		String Cpmpany = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);

	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_multiple_number_in_a_saved_Contact() throws Exception {

		Add_new_contact_By_Main_User();
		phoneApp2.ClickOnEditBtnOncontactpage();
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameforMainUser), true);

		phoneApp2.ClickOnAddBtnAt(number1);
		String Number2 = phoneApp2.EnternextNumberonAddContact(1, "+12345678900");
		phoneApp2.ClickOnSaveBtnAt(Number2);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(Number2);
		String Number3 = phoneApp2.EnternextNumberonAddContact(2, "+11234567000");
		phoneApp2.ClickOnSaveBtnAt(Number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(Number3);
		String Number4 = phoneApp2.EnternextNumberonAddContact(3, "+11235567000");
		phoneApp2.ClickOnSaveBtnAt(Number4);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(Number4);
		String Number5 = phoneApp2.EnternextNumberonAddContact(4, "+112345670040");
		phoneApp2.ClickOnSaveBtnAt(Number5);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		Common.pause(1);
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");

		Common.pause(3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameforMainUser);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), Number2);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(3), Number3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpageAFterscrool(4), Number4);
		assertEquals(phoneApp2.validateNumberOncontactDtlpageAFterscrool(5), Number5);

		phoneApp2Subuser.ClickonSpecificContact(nameforMainUser);
		assertEquals(phoneApp2Subuser.validateNameOncontactDtlpage(), nameforMainUser);
		assertEquals(phoneApp2Subuser.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2Subuser.validateNumberOncontactDtlpage(2), Number2);
		assertEquals(phoneApp2Subuser.validateNumberOncontactDtlpage(3), Number3);
		assertEquals(phoneApp2Subuser.validateNumberOncontactDtlpageAFterscrool(4), Number4);
		assertEquals(phoneApp2Subuser.validateNumberOncontactDtlpageAFterscrool(5), Number5);
		phoneApp2.ClickOnContactDtlCallBtnOnof(number1);
		assertEquals(phoneApp2.validateExtCallOutgoingNameCallingScreen(), nameforMainUser);
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), number1);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number1);
		phoneApp2.ValidateNameInNumberField(nameforMainUser);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameforMainUser);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number1);

	}

	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_contact_from_All_calls_page_with_two_unsaved_entry_in_all_calls_page() throws Exception {
		
		String contactName = "Cont" + Common.name();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		Common.pause(2);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp2.enterNumberinDialer(number4);
		phoneApp2.clickOnDialButton();
		Common.pause(2);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();

		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), number3);
		assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
		phoneApp2.clickOnCallDetailPageAddButton();
		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
		
		String Name = phoneApp2.enterNameInContacts(contactName);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
		phoneApp2.ClickOnSaveBtnAt(number4);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.invisibleValidationMessage();
		phoneApp2.ClickOnAddBtnAt(number4);
		phoneApp2.EnternextNumberonAddContact(1, number3);
		phoneApp2.ClickOnSaveBtnAt(number3);
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number4);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
		phoneApp2.ValidateNameInNumberField(Name);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name);
	
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
		assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
		
		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(webApp2.validateClientNumber(), Name);
		assertEquals(webApp2.validateClientNumberFor2ndentry(), Name);

	}

	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_contact_with_number_already_saved_in_another_contact() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String Name = phoneApp2.enterNameInContacts(contactName);
		String Number = phoneApp2.EnterNumberonAddContact(number3);
		phoneApp2.ClickOnSaveBtnAt(number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(5);
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		Common.pause(3);
		phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
		Common.pause(2);
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(10);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
		phoneApp2.clickOnRightArrow();
//		assertEquals(phoneApp2.validateAddNumberInCallDetail(), number3);
//		assertEquals(phoneApp2.validateNumberInCallDetail(), Name);

		phoneApp2.clickOnBackOnCallDetailPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(Name);
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
		phoneApp2.ValidateNameInNumberField(Name);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);
		driverphoneApp2.get(url.dialerSignIn());

		// ---------------------------------
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
		String contactName2 = "Cont" + Common.name();
		String Name2 = phoneApp2.enterNameInContacts(contactName2);
		phoneApp2.EnterNumberonAddContact(number4);
		phoneApp2.ClickOnSaveBtnAt(number4);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(number4);
		phoneApp2.EnternextNumberonAddContact(1, number3);
		phoneApp2.ClickOnSaveBtnAt(number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
		String Cpmpany = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		Common.pause(3);
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		Common.pause(3);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), Name2);
		phoneApp2.clickOnRightArrow();
//		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
//		assertEquals(phoneApp2.validateNameInCallDetail(), Name2);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickonsmspage();
		assertEquals(phoneApp2.validateNameOnSMSpage(), Name2);
		phoneApp2.ClickonSMSThraedOffirstthraed(Name2);
		Common.pause(5);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name2);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);

		// ----------------------------------------------------

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(Name2);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number4);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name2);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
		phoneApp2.ValidateNameInNumberField(Name2);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name2);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(Name2);
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
		phoneApp2.ValidateNameInNumberField(Name2);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name2);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number4);
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name2);
		assertEquals(phoneApp2.validateNumberInAllcalls(), Name2);

		driverwebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(webApp2.validateClientNumber(), Name2);
		assertEquals(webApp2.validateClientNumberFor2ndentry(), Name2);
	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Edit_contact_number_from_All_calls_page() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		Common.pause(3);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
		phoneApp2.clickOnCallDetailPageAddButton();
		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
		
		String Name = phoneApp2.enterNameInContacts(contactName);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
		String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);

		phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
		Common.pause(2);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
		phoneApp2.ValidateNameInNumberField(Name);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
		phoneApp2.clickOnRightArrow();
	//	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
		phoneApp2.clickOnCallDetailPageEditButton();
		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(Name), true);

		phoneApp2.EnterNumberonAddContact(number4);
		phoneApp2.ClickOnSaveBtnAt(number4);
		assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
		Common.pause(5);
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		Common.pause(3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number4);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);

		phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
		Common.pause(2);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
		phoneApp2.clickOnRightArrow();
//		assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), number3);
		phoneApp2.clickOnRightArrowFor2ndEntry();
//		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);

		driverphoneApp2.get(url.dialerSMSPage());
		phoneApp2.ClickonSMSThraedOffirstthraed(number3);
		assertEquals(phoneApp2.validateOnlyNumberOnSMSThread(), number3);

	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Edit_contact_number_from_Contact_Details_page() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		Common.pause(3);
		phoneApp2.clickOnIncomingHangupButton();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
		phoneApp2.clickOnCallDetailPageAddButton();
		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
		
		phoneApp2.enterNameInContacts(contactName);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		phoneApp2.ClickOnSaveBtnAt(number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(5);
		phoneApp2.ClickOnAddBtnAt(number3);
		phoneApp2.EnternextNumberonAddContact(1, number4);
		phoneApp2.ClickOnSaveBtnAt(number4);
		String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
		String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), contactName);
		phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
		Common.pause(2);
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(2);
		phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
		Common.pause(2);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
		Common.pause(2);
		phoneApp2.ValidateNameInNumberField(contactName);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), contactName);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(contactName);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), contactName);

		phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
		Common.pause(2);
		phoneApp2.ValidateNameInNumberField(contactName);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), contactName);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number4);

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), contactName);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNameInCallDetail(), contactName);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
		phoneApp2.clickOnBackOnCallDetailPage();

		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), contactName);
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(phoneApp2.validateNameInCallDetail(), contactName);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);

		phoneApp2.clickOnBackOnCallDetailPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(contactName);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), contactName);
		phoneApp2.ClickOnEditBtnOncontactpage();

		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);

		phoneApp2.EnternextNumberonAddContact(1, number5);
		phoneApp2.ClickOnSaveBtnAt(number5);
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		Common.pause(3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);

		driverphoneApp2.get(url.dialerSMSPage());
		phoneApp2.ClickonSMSThraedOffirstthraed(contactName);
		Common.pause(5);
		assertEquals(phoneApp2.validateNameOnSMSThread(), contactName);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);
		phoneApp2.clickOnBackOnCallDetailPage();
		phoneApp2.ClickonSpecificContact(contactName);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();

		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), contactName);
		phoneApp2.clickOnRightArrowFor2ndEntry();
        assertEquals(phoneApp2.validateNameInCallDetail(), contactName);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);

		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
		phoneApp2.clickCallBtnOnCallDetailsPage();
		assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp2.clickOnOutgoingHangupButton();

	}

//Edit contact name

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Edit_contact_name_from_Contact_Details_page() throws Exception {

		Add_contact_with_Two_Number();

		phoneApp2.ClickOnContactDtlCallBtnOnof(number1);
		assertEquals(number1, phoneApp2.validateCallingScreenOutgoingNumber());
		assertEquals(nameForTwoNumber, phoneApp2.validateExtCallOutgoingNameCallingScreen());
		Common.pause(1);
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(2);
		phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
		assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
		assertEquals(nameForTwoNumber, phoneApp2.validateExtCallOutgoingNameCallingScreen());
		Common.pause(2);
		phoneApp2.clickOnOutgoingHangupButton();

		// SMS
		phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
		Common.pause(2);
		phoneApp2.ValidateNameInNumberField(nameForTwoNumber);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);

		phoneApp2.ClickOnContactDtlSMSBtnOnof(number1);
		Common.pause(2);
		phoneApp2.ValidateNameInNumberField(nameForTwoNumber);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number1);

		// check name in Call logs
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(phoneApp2.validateNumberInAllcalls(), nameForTwoNumber);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNameInCallDetail(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), nameForTwoNumber);
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(phoneApp2.validateNameInCallDetail(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number1);

		// Edit contact name
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonSpecificContact(nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);
		phoneApp2.ClickOnEditBtnOncontactpage();
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		
		String Name2 = phoneApp2.enterNameInContacts(contactName);
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");
		Common.pause(5);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name2);
		phoneApp2.ClickOnBackBtnEditcontactpage();

		// Check Name after name update in All calls
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();

		assertEquals(phoneApp2.validateNumberInAllcalls(), Name2);
		phoneApp2.clickOnRightArrow();
		assertEquals(phoneApp2.validateNameInCallDetail(), Name2);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
		phoneApp2.clickOnBackOnCallDetailPage();
		assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name2);
		phoneApp2.clickOnRightArrowFor2ndEntry();
		assertEquals(phoneApp2.validateNameInCallDetail(), Name2);
		assertEquals(phoneApp2.validateNumberInCallDetail(), number1);

		// Update name in SMS page
		driverphoneApp2.get(url.dialerSMSPage());
		phoneApp2.ClickonSMSThraedOffirstthraed(Name2);
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name2);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number1);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.ClickonSMSThraedOf2ndthraed(Name2);
		Common.pause(10);

		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);
		assertEquals(phoneApp2.validateNameOnSMSThread(), Name2);


	}

	

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_new_Contact_page() throws Exception {

		Add_contact_with_one_Number();
		String name2 = phoneApp2.validateNameOncontactDtlpage();
		phoneApp2.ClickOnBackBtnEditcontactpage();

		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateAddContactButtonOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), name2);
		assertEquals(phoneApp2.getTwoLetterFromContactName(name2), phoneApp2.getTwoLetterInIconofContact(name2));

	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_name_with_Invalid_number_in_Add_Contact() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);

		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String Name = phoneApp2.enterNameInContacts(contactName);
		assertEquals(phoneApp2.ValidateSaveBtnIsEnabled(), "false");

		String Number = phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(3);
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		phoneApp2.ClickOnEditBtnOncontactpage();
		assertEquals(phoneApp2.ValidateNameInEditContacts(Name), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);

		String Number1 = phoneApp2.EnterInvalidNumberonAddContact("+12");
		phoneApp2.ClickOnSaveBtnAt("+12");
		assertEquals(phoneApp2.validationMessage(), "Please enter valid Number.");
	}

	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_name_with_Valid_number_Invalid_Email_Id() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);

		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		assertEquals(phoneApp2.ValidateSaveBtnIsEnabled(), "false");

		String number = phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(3);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
		// invalid email
		String email = phoneApp2.EnterEmailOnAddContact("test");
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_same_name_and_same_number_Duplicate_Contact() throws Exception {
		String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateAddContactButtonOncontactDisplaypage(), true);
		phoneApp2.ClickonAddcontact();

		// Duplicate Contact
		String duplicateName = phoneApp2.enterNameInContacts(name);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.EnterCompanyOnAddContact("Callhippo2");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), duplicateName);

	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_same_name_with_different_Number() throws Exception {
		 String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateAddContactButtonOncontactDisplaypage(), true);
		phoneApp2.ClickonAddcontact();

		// same name with different number Contact
		String duplicateName = phoneApp2.enterNameInContacts(name);
		phoneApp2.EnterNumberonAddContact(number3);
		phoneApp2.ClickOnSaveBtnAt(number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.EnterCompanyOnAddContact("Callhippo2");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), duplicateName);

	}

	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_different_name_with_same_Number() throws Exception {
		 String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateAddContactButtonOncontactDisplaypage(), true);
		phoneApp2.ClickonAddcontact();

		// different name with same number Contact
		 String contactName1 = "Cont" + Common.name();
		String differentName = phoneApp2.enterNameInContacts(contactName1);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.EnterCompanyOnAddContact("Callhippo2");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), differentName);

	}

	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_name_with_valid_number_with_country_code_in_Add_Contact() throws Exception {
		 String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);
	}

	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Delete_particular_number_from_contact() throws Exception {
		Add_contact_with_Two_Number();
		phoneApp2.ClickOnEditBtnOncontactpage();
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);

		phoneApp2.ClickOnDeleteBtnofEditContact(number1);
		Common.pause(3);
		assertEquals(phoneApp2.validateOnConfirmPopupYesButtonIsDisplayed(), true);
		assertEquals(phoneApp2.validateOnConfirmPopupCancelButtonIsDisplayed(), true);

		phoneApp2.clickOnConfirmPopupCancelButton();
		Common.pause(2);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);

		Common.pause(3);
		phoneApp2.ClickOnDeleteBtnofEditContact(number1);
		assertEquals(phoneApp2.validateOnConfirmPopupYesButtonIsDisplayed(), true);
		Common.pause(3);
		phoneApp2.clickOnConfirmPopupYesButton();
		assertEquals(phoneApp2.validationMessage(), "Number has been deleted Successfully");
		Common.pause(3);

		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);

	}

	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_deleted_number_in_Contact() throws Exception {

	//	verify_Delete_particular_number_from_contact();
		Add_contact_with_Two_Number();
		phoneApp2.ClickOnEditBtnOncontactpage();
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);

		phoneApp2.ClickOnDeleteBtnofEditContact(number1);
		Common.pause(3);
		assertEquals(phoneApp2.validateOnConfirmPopupYesButtonIsDisplayed(), true);
		assertEquals(phoneApp2.validateOnConfirmPopupCancelButtonIsDisplayed(), true);

		phoneApp2.clickOnConfirmPopupCancelButton();
		Common.pause(2);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);

		Common.pause(3);
		phoneApp2.ClickOnDeleteBtnofEditContact(number1);
		assertEquals(phoneApp2.validateOnConfirmPopupYesButtonIsDisplayed(), true);
		Common.pause(3);
		phoneApp2.clickOnConfirmPopupYesButton();
		assertEquals(phoneApp2.validationMessage(), "Number has been deleted Successfully");
		Common.pause(3);

		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");

		phoneApp2.ClickOnEditBtnOncontactpage();
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);
		phoneApp2.ClickOnAddBtnAt(number3);

		phoneApp2.EnternextNumberonAddContact(1, number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(3);

		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);

		Common.pause(3);
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validateEditBtnOncontactpage(), true);
		assertEquals(phoneApp2.validateDeleteBtnOncontactpage(), true);
	}

	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Add_contact_number_more_than_5_number() throws Exception {
		 String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String Name = phoneApp2.enterNameInContacts(contactName);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(number1);
		phoneApp2.EnternextNumberonAddContact(1, number3);
		phoneApp2.ClickOnSaveBtnAt(number3);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		phoneApp2.ClickOnAddBtnAt(number3);
		phoneApp2.EnternextNumberonAddContact(2, number4);
		phoneApp2.ClickOnSaveBtnAt(number4);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		phoneApp2.ClickOnAddBtnAt(number4);
		phoneApp2.EnternextNumberonAddContact(3, number5);
		phoneApp2.ClickOnSaveBtnAt(number5);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		phoneApp2.ClickOnAddBtnAt(number5);
		phoneApp2.EnternextNumberonAddContact(4, acc2Number2);
		phoneApp2.ClickOnSaveBtnAt(acc2Number2);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		assertEquals(phoneApp2.validateAddBtnAt(number3), true);

	}
//sms
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Send_sms_from_Compose_sms_page() throws Exception {
		Add_contact_with_Two_Number();
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");
		Common.pause(3);
		phoneApp2.clickOncontactIconatSMSthreadpage();
		Common.pause(2);
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(), nameForTwoNumber);
		phoneApp2.clickOnRightAerrowAtSMSThreadpage();
		Common.pause(2);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number1), true);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number3), true);
		Common.pause(2);
		phoneApp2.clickOnNumberAfterclickOnRightAerrowAtSMSThreadpage(number1);
		Common.pause(2);
		phoneApp2.clickOngreentikSMSthreadpage();

		Common.pause(3);
		phoneApp2.ValidateNameInNumberField(nameForTwoNumber);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		Common.pause(10);
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number1);
	}

	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Check_SMS_send_on_multiple_saved_number() throws Exception {
		//verify_Send_sms_from_Compose_sms_page();
		Add_contact_with_Two_Number();
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.clickOncontactIconatSMSthreadpage();
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(), nameForTwoNumber);
		phoneApp2.clickOnRightAerrowAtSMSThreadpage();
		Common.pause(2);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number1), true);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number3), true);
		Common.pause(2);
		phoneApp2.clickOnNumberAfterclickOnRightAerrowAtSMSThreadpage(number1);
		Common.pause(2);
		phoneApp2.clickOngreentikSMSthreadpage();

		Common.pause(3);
		phoneApp2.ValidateNameInNumberField(nameForTwoNumber);
		phoneApp2.enterMessage("Automation");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number1);
		
		

		Common.pause(3);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.clickOncontactIconatSMSthreadpage();
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(), nameForTwoNumber);
		phoneApp2.clickOnRightAerrowAtSMSThreadpage();
		Common.pause(2);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number1), true);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number3), true);
		Common.pause(2);
		phoneApp2.clickOnNumberAfterclickOnRightAerrowAtSMSThreadpage(number3);
		Common.pause(2);
		phoneApp2.clickOngreentikSMSthreadpage();

		Common.pause(3);
		phoneApp2.ValidateNameInNumberField(nameForTwoNumber);
		phoneApp2.enterMessage("Automation Test2");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForTwoNumber);
		assertEquals(phoneApp2.validateNumberOnSMSThread(), number3);

	}

	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Check_Contact_in_compose_sms_page_after_delete_of_one_number_from_multiple_contact()
			throws Exception {
		Add_contact_with_Two_Number();
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();

		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");
		Common.pause(2);
		phoneApp2.clickOnSMSCompose();
		phoneApp2.clickOncontactIconatSMSthreadpage();
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(), nameForTwoNumber);
		phoneApp2.clickOnRightAerrowAtSMSThreadpage();
		Common.pause(2);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number1), true);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number3), true);
		Common.pause(2);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickoncontactpage();
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForTwoNumber);
		phoneApp2.clickOnNameOncontactDisplaypage();

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);

		phoneApp2.clickOnCallDetailPageEditButton();
		phoneApp2.ClickOnDeleteBtnofEditContact(number1);
		Common.pause(3);
		assertEquals(phoneApp2.validateOnConfirmPopupYesButtonIsDisplayed(), true);
		assertEquals(phoneApp2.validateOnConfirmPopupCancelButtonIsDisplayed(), true);
		Common.pause(3);
		phoneApp2.clickOnConfirmPopupYesButton();
		assertEquals(phoneApp2.validationMessage(), "Number has been deleted Successfully");
		Common.pause(3);

		assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
		assertEquals(phoneApp2.ValidateNameInEditContacts(nameForTwoNumber), true);

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForTwoNumber);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.clickOncontactIconatSMSthreadpage();
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(), nameForTwoNumber);
		phoneApp2.clickOnRightAerrowAtSMSThreadpage();
		Common.pause(2);
		assertEquals(phoneApp2.validateNumberAfterclickOnRightAerrowAtSMSThreadpage(number3), true);

	}

	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Export_contact_from_web_with_contact() throws Exception {
		
		csv.deleteAllFile("contacts");

		Add_contact_with_Two_Number();

		webApp2.clickOnUserProfile();
		webApp2.clickOnExportContacts();
		webApp2.clickOnYesButton();
		Common.pause(20);
	

		assertTrue(csv.verifyDownloadFileName().contains("contacts"));
		
//		String filePath =downloadPath + csv.verifyDownloadFileName();
//		System.out.println(filePath);
		
		csvNames = csv.readDataFromCSV(csv.verifyDownloadFileName(), 0);
		assertEquals("name", csvNames.get(0).toString());
		assertEquals(nameForTwoNumber, csvNames.get(1).toString());
		assertEquals(nameForTwoNumber, csvNames.get(2).toString());
	
		csvNumbers = csv.readDataFromCSV(csv.verifyDownloadFileName(), 1);
		assertEquals("number", csvNumbers.get(0).toString());
		assertEquals(number1, csvNumbers.get(1).toString());
		assertEquals(number3, csvNumbers.get(2).toString());

		
	}
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Export_contact_from_web_without_contact() throws Exception {
		
		csv.deleteAllFile("contacts");

		webApp2.clickOnUserProfile();
		webApp2.clickOnExportContacts();
		webApp2.clickOnYesButton();
		Common.pause(20);
	

		assertTrue(csv.verifyDownloadFileName().contains("contacts"));
		
//		String filePath =downloadPath + csv.verifyDownloadFileName();
//		System.out.println(filePath);
		
		csvNames = csv.readDataFromCSV(csv.verifyDownloadFileName(), 0);
		System.out.println(csvNames.get(0).toString());
		assertEquals("name", csvNames.get(0).toString());
		
//		assertEquals(nameForTwoNumber, csvNames.get(1).toString());
//		assertEquals(nameForTwoNumber, csvNames.get(2).toString());
	
		csvNumbers = csv.readDataFromCSV(csv.verifyDownloadFileName(), 1);
		System.out.println(csvNumbers.get(0).toString());
		assertEquals("number", csvNumbers.get(0).toString());
		
//		assertEquals(number1, csvNumbers.get(1).toString());
//		assertEquals(number3, csvNumbers.get(2).toString());

		
	}

	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Check_success_messaage_after_adding_contact_from_dailpad() throws Exception {
		 String contactName = "Cont" + Common.name();
		assertEquals(phoneApp2.validateDialerScreenDisplayed(), true);
		phoneApp2.enterNumberinDialer(number1);
		assertEquals(number1, phoneApp2.getNumberFromDialer());
		phoneApp2.ClickonAddContactOnDialer();

		String name = phoneApp2.enterNameInContacts(contactName);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);

		Common.pause(3);
		String company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.clickOnTickGreenBtnToAddContactName1();

		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

	}

	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Search_contact_by_adding_name_which_is_saved_and_not_saved() throws Exception {
		Add_contact_with_one_Number();

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickoncontactpage();
		Common.pause(3);
		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForOneNumber);
		phoneApp2.clickOnSearchIconOncontactDisplaypage();
		phoneApp2.searchContactNameOncontactDisplaypage("Automation");
		Common.pause(2);
		assertEquals(phoneApp2.validateMessageForNoMatchContactOnContactDisplaypage(),
				"There is no contact with this name");

		phoneApp2.searchContactNameOncontactDisplaypage(nameForOneNumber);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForOneNumber);

	}

	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Clear_Search_Contact() throws Exception {
		//verify_Add_different_name_with_same_Number();
		 String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateAddContactButtonOncontactDisplaypage(), true);
		phoneApp2.ClickonAddcontact();

		// different name with same number Contact
		 String contactName1 = "Cont" + Common.name();
		String differentName = phoneApp2.enterNameInContacts(contactName1);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.EnterCompanyOnAddContact("Callhippo2");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), differentName);


		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(1), name);
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(2), differentName);
		phoneApp2.clickOnSearchIconOncontactDisplaypage();
		phoneApp2.searchContactNameOncontactDisplaypage(name);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), name);
		phoneApp2.clearSearchContact();
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(1), name);
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(2), differentName);

		phoneApp2.clickOnCloseIconOncontactDisplaypage();
		Common.pause(3);
		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);
	}

	@Test(priority = 28, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Edit_Email_Company_name() throws Exception {

		Add_contact_with_one_Number();
		phoneApp2.ClickOnEditBtnOncontactpage();
		String updatedEmail = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
		String Upadtedcompanyname = phoneApp2.EnterEmailOnAddContact("updateCallhippo");
		Common.pause(5);

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");
		assertEquals(phoneApp2.validateCompanyNameOncontactDtlpage(), Upadtedcompanyname);
		assertEquals(phoneApp2.validateEmailIdOncontactDtlpage(), updatedEmail);
	}

	@Test(priority = 29, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Search_Contact_by_number() throws Exception {
		Add_contact_with_one_Number();

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickoncontactpage();
		Common.pause(3);
		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForOneNumber);
		phoneApp2.clickOnSearchIconOncontactDisplaypage();
		phoneApp2.searchContactNameOncontactDisplaypage(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForOneNumber);

	}

	@Test(priority = 30, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_edited_contact_after_setting_call_reminder() throws Exception {
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driverphoneApp2.get(url.dialerSignIn());
		Add_contact_with_one_Number();
		phoneApp2.clickOnconDetailCallBtn1();

		assertTrue(phoneApp2.validateCallingScreenOutgoingVia().contains("Outgoing"));

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), number1);

		phoneApp2.clickOnOutgoingHangupButton();
		assertEquals(phoneApp2.validateCallReminderPopupIsDisplayed(), true);
		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		assertEquals(phoneApp2.validationMessage(), "Successfully set call reminder");
		Common.pause(5);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "Call Reminder");
		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), nameForOneNumber);
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number1);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), nameForOneNumber);
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number1);

		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickoncontactpage();
		Common.pause(3);
		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForOneNumber);
		phoneApp2.clickOnNameOncontactDisplaypage();

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForOneNumber);

		phoneApp2.clickOnCallDetailPageEditButton();
		Common.pause(5);
		phoneApp2.clearNameInContact();
		String nameEdit = phoneApp2.enterNameInContacts(contactName);
		// phoneApp2.ClickOnSaveBtnAt(number1);
		Common.pause(5);

		// phoneApp2.clickOnTickGreenBtnToAddContactName();
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameEdit);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), nameEdit);
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number1);
	}

	@Test(priority = 31, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_reminder_contact_after_delete_Contact_and_Add_contact_same_num_with_different_name()
			throws Exception {
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driverphoneApp2.get(url.dialerSignIn());
		Add_contact_with_one_Number();
		phoneApp2.clickOnconDetailCallBtn1();

		assertTrue(phoneApp2.validateCallingScreenOutgoingVia().contains("Outgoing"));

		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), number1);

		phoneApp2.clickOnOutgoingHangupButton();
		assertEquals(phoneApp2.validateCallReminderPopupIsDisplayed(), true);
		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		assertEquals(phoneApp2.validationMessage(), "Successfully set call reminder");
		Common.pause(5);
		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "Call Reminder");
		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), nameForOneNumber);
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number1);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), nameForOneNumber);
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number1);

		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickoncontactpage();
		Common.pause(3);
		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), nameForOneNumber);
		phoneApp2.clickOnNameOncontactDisplaypage();

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForOneNumber);
		phoneApp2.clickOnBackBtnDeletecontactpage();
		assertEquals(phoneApp2.validateOnConfirmPopupYesButtonIsDisplayed(), true);
		Common.pause(3);
		phoneApp2.clickOnConfirmPopupYesButton();
		assertEquals(phoneApp2.validationMessage(), "Contact has been deleted Successfully");
		Common.pause(3);

		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "Unknown Caller");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number1);

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		String name = phoneApp2.enterNameInContacts(contactName);
		String number = phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(3);
		String Email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String Cpmpany = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		phoneApp2.clickOnSideMenu();

		phoneApp2.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "Call Reminder");
		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), name);
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number);
	}

@Test(priority = 32, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_name_with_already_saved_name_with_different_number_from_Contact_Details_page  () throws Exception {

	String contactName = "Cont" + Common.name();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	String Name12 = phoneApp2.enterNameInContacts(contactName);
	String Number1 = phoneApp2.EnterNumberonAddContact(number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
//	phoneApp2.ClickOnAddBtnAt(number5);
	Common.pause(3);
	String Email2 = phoneApp2.EnterEmailOnAddContact(account2Email);
	String Company2 = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
	Common.pause(3);
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
    assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number1);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	driverphoneApp2.get(url.dialerSignIn());
    Common.pause(2);


	// Add contact with two number

	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String contactName1 = "Cont" + Common.name();

	String Name = phoneApp2.enterNameInContacts(contactName1);
	phoneApp2.EnterNumberonAddContact(number1);
	phoneApp2.ClickOnSaveBtnAt(number1);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	phoneApp2.ClickOnAddBtnAt(number1);
	phoneApp2.EnternextNumberonAddContact(1, number3);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	
	Common.pause(3);
	String Email = phoneApp2.EnterEmailOnAddContact(account2Email);
	String Cpmpany = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
	
	//phoneApp2.clickOnTickGreenBtnToAddContactName();
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
	
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlCallBtnOnof(number1);
	assertEquals(number1, phoneApp2.validateCallingScreenOutgoingNumber());
	assertEquals(Name, phoneApp2.validateExtCallOutgoingNameCallingScreen());
    Common.pause(1);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
	assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
	assertEquals(Name, phoneApp2.validateExtCallOutgoingNameCallingScreen());
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	
	// SMS
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number1);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number1);
	
	//check name in Call logs
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number1);
	
	//Edit contact name
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnEditBtnOncontactpage();
	assertEquals(phoneApp2.ValidateNameInEditContacts(Name), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.enterNameInContacts(Name12);
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");
	Common.pause(5);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	phoneApp2.ClickOnBackBtnEditcontactpage();
	
	//Check Name after name update in All calls
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name12);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name12);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name12);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name12);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number1);
	
	
	//Update name in SMS page
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(Name12);
	Common.pause(5);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name12);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number1);
	phoneApp2.ClickOnBackBtnEditcontactpage();
	phoneApp2.ClickonSMSThraedOf2ndthraed(Name12);
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name12);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);	

}

@Test(priority = 33, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_number_with_already_saved_number_with_different_name_from_Contact_Details_page () throws Exception {

	String contactName = "Cont" + Common.name();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	String Name12 = phoneApp2.enterNameInContacts("Name1");
	String Number1 = phoneApp2.EnterNumberonAddContact(number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
////	phoneApp2.ClickOnAddBtnAt(number5);
	  Common.pause(3);
	String Email2 = phoneApp2.EnterEmailOnAddContact(account2Email);
	String Company2 = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
	  Common.pause(3);
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
  

	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number1);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	driverphoneApp2.get(url.dialerSignIn());
    Common.pause(2);


	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	phoneApp2.ClickOnAddBtnAt(number3);
	phoneApp2.EnternextNumberonAddContact(1, number4);
	phoneApp2.ClickOnSaveBtnAt(number4);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	Common.pause(3);
	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickOnBackOnCallDetailPage();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnEditBtnOncontactpage();
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
	Common.pause(2);

	phoneApp2.EnternextNumberonAddContact(1, number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	Common.pause(3);

	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	Common.pause(3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);
	
	driverphoneApp2.get(url.dialerSMSPage());
	Common.pause(3);
	phoneApp2.ClickonSMSThraedOffirstthraed(Name);
	Common.pause(5);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickCallBtnOnCallDetailsPage();
	assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
	phoneApp2.clickOnOutgoingHangupButton();
	
	
}

@Test(priority = 34, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_number_and_name_from_Contact_Details_page () throws Exception {

	String contactName = "Cont" + Common.name();
	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");


	phoneApp2.ClickOnAddBtnAt(number3);
	phoneApp2.EnternextNumberonAddContact(1, number4);
	phoneApp2.ClickOnSaveBtnAt(number4);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	Common.pause(3);
	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickOnBackOnCallDetailPage();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnEditBtnOncontactpage();
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
	String editedName = phoneApp2.enterNameInContacts(Name2);
	phoneApp2.EnternextNumberonAddContact(1, number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	Common.pause(3);

	phoneApp2.clickOnTickGreenBtnToAddContactName();
	Common.pause(3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), editedName);

	
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(editedName);
	Common.pause(3);
	assertEquals(phoneApp2.validateNameOnSMSThread(),editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.ClickonSpecificContact(editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), editedName);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), editedName);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickCallBtnOnCallDetailsPage();
	assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
	phoneApp2.clickOnOutgoingHangupButton();
	
	
}

@Test(priority = 35, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_number_and_name_with_already_saved_entries_from_Contact_Details_page () throws Exception {

	String contactName = "Cont" + Common.name();

	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	String Name12 = phoneApp2.enterNameInContacts(Name2);
	String Number12 = phoneApp2.EnterNumberonAddContact(number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	Common.pause(3);
		//phoneApp2.clickOnTickGreenBtnToAddContactName();
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
	
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number12);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	Common.pause(3);
	driverphoneApp2.get(url.dialerSignIn());
	
	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	phoneApp2.ClickOnAddBtnAt(number3);
	phoneApp2.EnternextNumberonAddContact(1, number4);
	phoneApp2.ClickOnSaveBtnAt(number4);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	Common.pause(3);

	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickOnBackOnCallDetailPage();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnEditBtnOncontactpage();
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
	String editedName = phoneApp2.enterNameInContacts(Name2);
	phoneApp2.EnternextNumberonAddContact(1, number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	Common.pause(3);

	phoneApp2.clickOnTickGreenBtnToAddContactName();
	Common.pause(3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), editedName);

	
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(editedName);
	Common.pause(3);
	assertEquals(phoneApp2.validateNameOnSMSThread(),editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.ClickonSpecificContact(editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), editedName);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), editedName);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickCallBtnOnCallDetailsPage();
	assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
	phoneApp2.clickOnOutgoingHangupButton();

	
}


@Test(priority = 36, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_name_from_All_calls_page  () throws Exception {

	String contactName = "Cont" + Common.name();

	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName();
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	
	
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	phoneApp2.clickOnCallDetailPageEditButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
//	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNameInEditContacts(Name), true);
	String Name1 = phoneApp2.enterNameInContacts("Edited Contact");

	//phoneApp2.ClickOnSaveBtnAt(number4);
//	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name1);
	
	
	
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
	Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	
	phoneApp2.ClickOnBackBtnEditcontactpage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name1);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name1);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(Name1);
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name1);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);

}

@Test(priority = 37, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_name_with_already_saved_name_with_different_number_from_call_logs  () throws Exception {

	String contactName = "Cont" + Common.name();

	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	String Name12 = phoneApp2.enterNameInContacts(contactName);
	String Number1 = phoneApp2.EnterNumberonAddContact(number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
//	phoneApp2.ClickOnAddBtnAt(number5);
	  Common.pause(3);
	String Email2 = phoneApp2.EnterEmailOnAddContact(account2Email);
	String Company2 = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
	  Common.pause(3);
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
  

	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number1);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	driverphoneApp2.get(url.dialerSignIn());
    Common.pause(2);


	// Add contact with two number

	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String contactName1 = "Cont" + Common.name();

	String Name = phoneApp2.enterNameInContacts(contactName1);
	phoneApp2.EnterNumberonAddContact(number1);
	phoneApp2.ClickOnSaveBtnAt(number1);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	phoneApp2.ClickOnAddBtnAt(number1);
	phoneApp2.EnternextNumberonAddContact(1, number3);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	
	Common.pause(3);
	String Email = phoneApp2.EnterEmailOnAddContact(account2Email);
	String Cpmpany = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
	
	//phoneApp2.clickOnTickGreenBtnToAddContactName();
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
	
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlCallBtnOnof(number1);
	assertEquals(number1, phoneApp2.validateCallingScreenOutgoingNumber());
	assertEquals(Name, phoneApp2.validateExtCallOutgoingNameCallingScreen());
    Common.pause(1);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
	assertEquals(number3, phoneApp2.validateCallingScreenOutgoingNumber());
	assertEquals(Name, phoneApp2.validateExtCallOutgoingNameCallingScreen());
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	
	// SMS
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number1);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number1);
	
	//check name in Call logs
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number1);
	
	//Edit contact name

	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	phoneApp2.clickOnCallDetailPageEditButton();
//	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
//	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
//	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
//	phoneApp2.ClickOnEditBtnOncontactpage();
	assertEquals(phoneApp2.ValidateNameInEditContacts(Name), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number1), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.enterNameInContacts(Name12);
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been updated Successfully");
	Common.pause(5);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number3);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	phoneApp2.ClickOnBackBtnEditcontactpage();
	
	//Check Name after name update in All calls
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name12);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name12);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name12);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name12);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number1);
	
	
	//Update name in SMS page
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(Name12);
	Common.pause(5);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name12);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number1);
	phoneApp2.ClickOnBackBtnEditcontactpage();
	phoneApp2.ClickonSMSThraedOf2ndthraed(Name12);
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name12);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);	

}

@Test(priority = 38, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_number_with_already_saved_number_with_different_name_from_Call_Log_page () throws Exception {

	String contactName = "Cont" + Common.name();

	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	String Name12 = phoneApp2.enterNameInContacts("Name1");
	String Number1 = phoneApp2.EnterNumberonAddContact(number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
//	phoneApp2.ClickOnAddBtnAt(number5);
	  Common.pause(3);
	String Email2 = phoneApp2.EnterEmailOnAddContact(account2Email);
	String Company2 = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
	  Common.pause(3);
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
  

	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number1);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	driverphoneApp2.get(url.dialerSignIn());
    Common.pause(2);


	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	phoneApp2.ClickOnAddBtnAt(number3);
	phoneApp2.EnternextNumberonAddContact(1, number4);
	phoneApp2.ClickOnSaveBtnAt(number4);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	Common.pause(3);

	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickOnBackOnCallDetailPage();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	//Edit contact name
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
//	phoneApp2.clickOnCallDetailPageEditButton();
//	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
//	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
//	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.clickOnCallDetailPageEditButton();
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
	
	phoneApp2.EnternextNumberonAddContact(1, number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	Common.pause(3);

	phoneApp2.clickOnTickGreenBtnToAddContactName();
	Common.pause(3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);
	
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(Name);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickCallBtnOnCallDetailsPage();
	assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
	phoneApp2.clickOnOutgoingHangupButton();
	
	
}
@Test(priority = 39, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_number_and_name_from_call_log_page () throws Exception {

	String contactName = "Cont" + Common.name();

	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");


	phoneApp2.ClickOnAddBtnAt(number3);
	phoneApp2.EnternextNumberonAddContact(1, number4);
	phoneApp2.ClickOnSaveBtnAt(number4);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	Common.pause(3);
	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);

	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickOnBackOnCallDetailPage();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	//Edit contact 
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
//	phoneApp2.clickOnCallDetailPageEditButton();
//	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
//	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
//	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.clickOnCallDetailPageEditButton();
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
	String editedName = phoneApp2.enterNameInContacts(Name2);
	phoneApp2.EnternextNumberonAddContact(1, number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	Common.pause(3);

	phoneApp2.clickOnTickGreenBtnToAddContactName();
	Common.pause(3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), editedName);

	
	driverphoneApp2.get(url.dialerSMSPage());
	Common.pause(2);
	phoneApp2.ClickonSMSThraedOffirstthraed(editedName);
	assertEquals(phoneApp2.validateNameOnSMSThread(),editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.ClickonSpecificContact(editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), editedName);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), editedName);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickCallBtnOnCallDetailsPage();
	assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
	phoneApp2.clickOnOutgoingHangupButton();
}


@Test(priority = 40, retryAnalyzer = com.CallHippo.Init.Retry.class)
public void Edit_contact_number_and_name_with_already_saved_entries_from_call_log_page () throws Exception {

	String contactName = "Cont" + Common.name();

	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonAddcontact();
	
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	
	String Name12 = phoneApp2.enterNameInContacts(Name2);
	String Number12 = phoneApp2.EnterNumberonAddContact(number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
	Common.pause(3);
		//phoneApp2.clickOnTickGreenBtnToAddContactName();
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
	
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number12);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name12);
	Common.pause(3);
	driverphoneApp2.get(url.dialerSignIn());
	
	phoneApp2.enterNumberinDialer(number3);
	phoneApp2.clickOnDialButton();
	Common.pause(3);
	phoneApp2.clickOnIncomingHangupButton();
	
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number3);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	phoneApp2.clickOnCallDetailPageAddButton();
	assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
	assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
	assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);
	String Name = phoneApp2.enterNameInContacts(contactName);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	phoneApp2.ClickOnSaveBtnAt(number3);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	phoneApp2.ClickOnAddBtnAt(number3);
	phoneApp2.EnternextNumberonAddContact(1, number4);
	phoneApp2.ClickOnSaveBtnAt(number4);
	assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

	Common.pause(3);

	String Email = phoneApp2.EnterEmailOnAddContact(account2EmailSubUser);
	String Company = phoneApp2.EnterCompanyOnAddContact("Callhippo2");
	phoneApp2.clickOnTickGreenBtnToAddContactName1();
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number3);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	Common.pause(2);
	phoneApp2.ClickOnContactDtlCallBtnOnof(number4);
    Common.pause(2);
	phoneApp2.clickOnOutgoingHangupButton();
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number3);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);

	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickoncontactpage();
	phoneApp2.ClickonSpecificContact(Name);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number4);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), Name);
	
	phoneApp2.ClickOnContactDtlSMSBtnOnof(number4);
	Common.pause(2);
	phoneApp2.ValidateNameInNumberField(Name);
	phoneApp2.enterMessage("Automation");
	phoneApp2.clickOnSendMessageButton();
	Common.pause(10);

	assertEquals(phoneApp2.validateNameOnSMSThread(),Name);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number4);
	
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickOnBackOnCallDetailPage();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), Name);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	Common.pause(3);

	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	assertEquals(phoneApp2.validateNumberInAllcalls(), Name);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNameInCallDetail(), Name);
	phoneApp2.ClickOnEditBtnOncontactpage();
	
	assertEquals(phoneApp2.ValidateNumberInNumberField(number3), true);
	assertEquals(phoneApp2.ValidateNumberInNumberField(number4), true);
	String editedName = phoneApp2.enterNameInContacts(Name2);
	phoneApp2.EnternextNumberonAddContact(1, number5);
	phoneApp2.ClickOnSaveBtnAt(number5);
	assertEquals(phoneApp2.validationMessage(), "Number has been updated Successfully");
	Common.pause(3);

	phoneApp2.clickOnTickGreenBtnToAddContactName();
	Common.pause(3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number3);
	assertEquals(phoneApp2.validateNumberOncontactDtlpage(2), number5);
	assertEquals(phoneApp2.validateNameOncontactDtlpage(), editedName);

	
	driverphoneApp2.get(url.dialerSMSPage());
	phoneApp2.ClickonSMSThraedOffirstthraed(editedName);
	assertEquals(phoneApp2.validateNameOnSMSThread(),editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	phoneApp2.clickOnBackOnCallDetailPage();
	phoneApp2.ClickonSpecificContact(editedName);
	assertEquals(phoneApp2.validateNumberOnSMSThread(),number3);
	
	driverphoneApp2.get(url.dialerSignIn());
	phoneApp2.waitForDialerPage();
	phoneApp2.clickOnSideMenu();
	phoneApp2.clickOnAllCalls();
	
	assertEquals(phoneApp2.validateNumberInAllcallsFor2ndEntry(), editedName);
	phoneApp2.clickOnRightArrowFor2ndEntry();
	assertEquals(phoneApp2.validateNameInCallDetail(), editedName);
	assertEquals(phoneApp2.validateNumberInCallDetail(), number3);
	
	phoneApp2.clickOnBackOnCallDetailPage();
	assertEquals(phoneApp2.validateNumberInAllcalls(), number4);
	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.validateNumberInCallDetail(), number4);
	phoneApp2.clickCallBtnOnCallDetailsPage();
	assertEquals(number4, phoneApp2.validateCallingScreenOutgoingNumber());
	phoneApp2.clickOnOutgoingHangupButton();
	
	
}
}