package com.CallHippo.Dialer.settings.globalConnect;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.TestBase;


public class GlobalConnectTest {
	
	DialerLoginPage dialerloginpage;
	WebDriver driverphoneApp2;
	WebDriver driverphoneApp2Subuser;
	WebToggleConfiguration webApp2;
	DialerIndex phoneApp2;
	DialerIndex phoneApp2Subuser;
	PropertiesFile url= new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data= new PropertiesFile("Data\\url Configuration.properties");

	public GlobalConnectTest() throws Exception {
		
	}
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2Number1 = data.getValue("account2Number1"); // account 2's number
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");

	@BeforeTest
	public void initialization() throws Exception {

		driverphoneApp2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driverphoneApp2, DialerIndex.class);
		dialerloginpage = PageFactory.initElements(driverphoneApp2, DialerLoginPage.class);
	
		driverphoneApp2Subuser = TestBase.init_dialer();
		System.out.println("Opened phoneAap2Subuser session");
		phoneApp2Subuser = PageFactory.initElements(driverphoneApp2Subuser, DialerIndex.class);
		dialerloginpage = PageFactory.initElements(driverphoneApp2Subuser, DialerLoginPage.class);
		
		try {

			try {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}
			
			try {
				driverphoneApp2Subuser.get(url.dialerSignIn());
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driverphoneApp2Subuser.get(url.dialerSignIn());
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			
			try {
				phoneApp2.afterCallWorkToggle("off");
				phoneApp2.globalConnectToggle("off");
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(5);
				phoneApp2Subuser.afterCallWorkToggle("off");
				phoneApp2Subuser.globalConnectToggle("off");
				driverphoneApp2Subuser.get(url.dialerSignIn());
				Common.pause(5);
				
			}catch (Exception e) {	
				phoneApp2.afterCallWorkToggle("off");
				phoneApp2.globalConnectToggle("off");
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(5);
				phoneApp2Subuser.afterCallWorkToggle("off");
				phoneApp2Subuser.globalConnectToggle("off");
				driverphoneApp2Subuser.get(url.dialerSignIn());
				Common.pause(5);
			}
		} catch (Exception e1) {
			String testname = "GlobalConnect Before Test ";
			
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverphoneApp2Subuser, testname, "PhoneAppp2Subuser Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}
	@BeforeMethod
	public void login() throws IOException, InterruptedException {
	try {
		  try {
			Common.pause(2);
			driverphoneApp2.navigate().to(url.dialerSignIn());	
			driverphoneApp2Subuser.navigate().to(url.dialerSignIn());	
			
			Common.pause(3);
			
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

	
		} catch (Exception e) {

			Common.pause(2);
			driverphoneApp2.navigate().to(url.dialerSignIn());	
			driverphoneApp2Subuser.navigate().to(url.dialerSignIn());	
			
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			
		} 
	}catch (Exception e2) {
			String testname = "GlobalConnect Before Method ";
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverphoneApp2Subuser, testname, "PhoneAppp2Subuser Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverphoneApp2, testname, "PhoneApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2Subuser, testname, "PhoneApp2Subuser Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverphoneApp2, testname, "PhoneApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2Subuser, testname, "PhoneApp2Subuser Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driverphoneApp2.quit();
		driverphoneApp2Subuser.quit();
	}
	
	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
	}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_OFF_Verify_Time_Zone_Not_displaying() throws Exception {
		phoneApp2.globalConnectToggle("off");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), false);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), false);
	}
	
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_correctly() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
		phoneApp2.enterNumberinDialer("+16196321452");
		Common.pause(5);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("America/Los_Angeles"));
		
		phoneApp2.enterNumberinDialer("+44532462582");
		Common.pause(5);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("Europe/London"));
		
//		phoneApp2.enterNumberinDialer("+6196321452");
//		Common.pause(5);
//		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
//		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("Australia/Lord_Howe"));
		
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_After_Redial() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+16196321452");
		System.out.print("Entered number");	
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("America/Los_Angeles"));
		
		phoneApp2.clickOnDialButton();
		// validate outgoing Number on outgoing calling screen
	     assertEquals("+16196321452", phoneApp2.validateCallingScreenOutgoingNumber());
	     Common.pause(3);
	     phoneApp2.clickOnOutgoingHangupButton();
	     assertEquals(phoneApp2.validateDialerScreenDisplayed(),true);
	     
	     phoneApp2.clickOnReDialButton();
	     assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		 assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("America/Los_Angeles"));	    
    }
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_After_Clear_Number() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
		phoneApp2.clearNumberinDialer();
		assertEquals(phoneApp2.validateDialerScreenDisplayed(),false);
		Common.pause(3);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
}
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_After_enter_countrycode_without_plus() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
		phoneApp2.enterNumberinDialer("61");
		Common.pause(3);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertNotEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("Australia/Lord_Howe"));
		
	}
	//     subUser //
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_Subuser() throws Exception {
		phoneApp2Subuser.globalConnectToggle("on");
		
		assertEquals(phoneApp2Subuser.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2Subuser.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
		
		phoneApp2Subuser.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2Subuser.validateTimeZoneIsDisplayed(), true);
	}
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_OFF_Verify_Time_Zone_Not_displaying_Subuser() throws Exception {
		phoneApp2Subuser.globalConnectToggle("off");
		
		assertEquals(phoneApp2Subuser.validateGlobalConnectToggleIsOn(), false);
		
		driverphoneApp2Subuser.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
		
		phoneApp2Subuser.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2Subuser.validateTimeZoneIsDisplayed(), false);
	}
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_correctly_Subuser() throws Exception {
		phoneApp2Subuser.globalConnectToggle("on");
		
		assertEquals(phoneApp2Subuser.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2Subuser.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
		
		phoneApp2Subuser.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2Subuser.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2Subuser.getDateTimeFromDialer(), phoneApp2Subuser.getTimeAndDateCountryWise("IST"));
		
		phoneApp2Subuser.enterNumberinDialer("+16196321452");
		Common.pause(5);
		assertEquals(phoneApp2Subuser.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2Subuser.getDateTimeFromDialer(), phoneApp2Subuser.getTimeAndDateCountryWise("America/Los_Angeles"));
		
		phoneApp2Subuser.enterNumberinDialer("+44532462582");
		Common.pause(5);
		assertEquals(phoneApp2Subuser.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2Subuser.getDateTimeFromDialer(), phoneApp2Subuser.getTimeAndDateCountryWise("Europe/London"));
		
//		phoneApp2Subuser.enterNumberinDialer("+6196321452");
//		Common.pause(5);
//		assertEquals(phoneApp2Subuser.validateTimeZoneIsDisplayed(), true);
//		assertEquals(phoneApp2Subuser.getDateTimeFromDialer(), phoneApp2Subuser.getTimeAndDateCountryWise("Australia/Lord_Howe"));
		
	}
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_After_enter_Not_existing_countrycode() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
		phoneApp2.enterNumberinDialer("+99985");
		Common.pause(3);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
	}
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_After_enter_extension() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
		phoneApp2.enterNumberinDialer("101");
		Common.pause(3);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
	}
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void globalConnectToggle_ON_Verify_Time_Zone_displaying_After_enter_number_from_DialPad() throws Exception {
		phoneApp2.globalConnectToggle("on");
		
		assertEquals(phoneApp2.validateGlobalConnectToggleIsOn(), true);
		
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(5);
		assertEquals(dialerloginpage.verifyUserLoggedInDialerSuccessfully(), true);
	
		phoneApp2.enterNumberinDialer("+919898774455");
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
		phoneApp2.clearNumberinDialer();
		assertEquals(phoneApp2.validateDialerScreenDisplayed(),false);
		phoneApp2.clickOnNumberFromDailpad("+1256");
//		phoneApp2.clickOnNumberFromDailpad("2");
//		phoneApp2.clickOnNumberFromDailpad("5");
//		phoneApp2.clickOnNumberFromDailpad("6");
		Common.pause(3);
		assertEquals(phoneApp2.validateTimeZoneIsDisplayed(), true);
		assertEquals(phoneApp2.getDateTimeFromDialer(), phoneApp2.getTimeAndDateCountryWise("IST"));
		
	}
}
	