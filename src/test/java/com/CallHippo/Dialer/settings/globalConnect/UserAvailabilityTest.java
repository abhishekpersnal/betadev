package com.CallHippo.Dialer.settings.globalConnect;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.AbstractPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class UserAvailabilityTest {
	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;

	WebToggleConfiguration webApp2;
	WebDriver driver4;

	HolidayPage webApp2HolidayPage;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number2 = data.getValue("account1Number1"); // account 1's number

	String userID;
	String numberID;

	Common excelTestResult;

	public UserAvailabilityTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
	}

//	 before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);

		driver = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}

			try {

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);

				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= " + numberID);

				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
				webApp2.enterAWHM_message(""
						+ "You have called out of our business hours. Please call later You have called out of our business hours. Please call later You have called out of our business hours. ");
				Thread.sleep(3000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off", "Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(acc2Number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);

				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= " + numberID);

				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
				webApp2.enterAWHM_message(""
						+ "You have called out of our business hours. Please call later You have called out of our business hours. Please call later You have called out of our business hours. ");
				Thread.sleep(3000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off", "Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(acc2Number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}
		} catch (Exception e1) {
			String testname = "User Availability Before Method ";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driver4.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}

			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}

			driver4.get(url.numbersPage());
			webApp2.numberspage();

			webApp2.navigateToNumberSettingpage(number);
			Thread.sleep(9000);
			webApp2.setCallRecordingToggle("on");
			
			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			webApp2.makeDefaultNumber(number);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

			driver4.get(url.signIn());
		} catch (Exception e) {

			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}

				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}

				driver4.get(url.numbersPage());
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);
				Thread.sleep(9000);
				webApp2.setCallRecordingToggle("on");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				driver4.get(url.signIn());


			} catch (Exception e1) {
				String testname = "User Availability calling Before Method ";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver.quit();
		driver1.quit();
		driver4.quit();
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_User_Availability_Settings_is_same_as_web_app_setting_in_all_platforms() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");

		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Thread.sleep(3000);
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), false,
				"----verify User Availability Toggle should OFF----");

		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Thread.sleep(3000);
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");

	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_ALWAYS_OPENED_To_ALWAYS_CLOSED() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), false,
				"----verify User Availability Toggle should OFF----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
				"----verify Incoming Calling Screen Should Not Display----");
		phoneApp2.waitForDialerPage();

	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_ALWAYS_OPENED_To_CUSTOM_UNAVAILABLE() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		api.addCustomTimeSlot("user", userID, userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
				"----verify Incoming Calling Screen Should Not Display----");
		phoneApp2.waitForDialerPage();

	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_ALWAYS_OPENED_To_CUSTOM_AVAILABLE() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		api.addCustomTimeSlot("user", userID, userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true,
				"----verify Incoming Calling Screen Should Display----");
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
				
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_CUSTOM_To_ALWAYS_OPENED() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Common.pause(4);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(4);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true,
				"----verify Incoming Calling Screen Should Display----");
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
				
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_CUSTOM_To_ALWAYS_CLOSED() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Common.pause(3);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), false,
				"----verify User Availability Toggle should OFF----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
				"----verify Incoming Calling Screen Should Not Display----");
		phoneApp2.waitForDialerPage();

	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_ALWAYS_CLOSED_To_ALWAYS_OPENED() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(3);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true,
				"----verify Incoming Calling Screen Should Display----");
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
				
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_ALWAYS_CLOSED_To_CUSTOM_AVAILABLE() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		Common.pause(3);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(3);
		api.addCustomTimeSlot("user", userID, userID, true);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true,
				"----verify Incoming Calling Screen Should Display----");
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
				
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_App_Setting_User_Availability_From_ALWAYS_CLOSED_To_CUSTOM_UNAVAILABLE() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		Common.pause(3);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");
		Common.pause(3);
		api.addCustomTimeSlot("user", userID, userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
		Common.pause(3);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(phoneApp2.validateUserAvailabilityToggleIsOn(), true,
				"----verify User Availability Toggle should ON----");
		driver1.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
				"----verify Incoming Calling Screen Should Not Display----");
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_Setting_User_Availability_From_ON_To_OFF() throws Exception {
	
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.clickUserAvailabilityToggle("off");
		Common.pause(3);
		driver1.get(url.dialerSignIn());

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		Common.pause(3);
		assertEquals(webApp2.validateUserAvailabilityAlwaysClosed(), true,
				"----verify In Web App User Availability Should Always Closed----");
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
				"----verify Incoming Calling Screen Should Not Display----");
		phoneApp2.waitForDialerPage();
}
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_Setting_User_Availability_From_OFF_To_ON() throws Exception {
	
	
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.clickUserAvailabilityToggle("off");
		Common.pause(3);

		driver1.get(url.dialerSignIn());
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		Common.pause(3);
		assertEquals(webApp2.validateUserAvailabilityAlwaysClosed(), true,
				"----verify In Web App User Availability Should Always Closed----");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnSetting();
		phoneApp2.clickUserAvailabilityToggle("on");

		Common.pause(2);
		driver1.get(url.dialerSignIn());

		
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.makeDefaultNumber(number);
		Common.pause(3);
		assertEquals(webApp2.validateUserAvailabilityAlwaysOpened(), true,
				"----verify In Web App User Availability Should Always Open----");
		
		Common.pause(3);
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), true,
				"----verify Incoming Calling Screen Should Display----");
		// Validate IncomingVia on Incoming calling screen
				assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

				// validate Incoming Number on Incoming calling screen
				assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			
	
		phoneApp2.clickOnIncomimgAcceptCallButton();
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(10000);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	
		
	}	
		@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
		public void web_App_Setting_User_Availability_From_CUSTOM_To_Dialer_Setting_OFF() throws Exception {
			
			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			webApp2.makeDefaultNumber(number);
			Common.pause(3);
			webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");
			Common.pause(3);
			phoneApp2.clickOnSideMenu();
			phoneApp2.clickOnSetting();
			phoneApp2.clickUserAvailabilityToggle("off");
			Common.pause(3);
			driver1.get(url.dialerSignIn());

			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
		
			assertEquals(webApp2.validateUserAvailabilityAlwaysClosed(), true,
					"----verify In Web App User Availability Should Always Closed----");
			
			phoneApp1.enterNumberinDialer(number);
			String departmentName = phoneApp1.validateDepartmentNameinDialpade();
			String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

			phoneApp1.clickOnDialButton();
			assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
			assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

			assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
					"----verify Incoming Calling Screen Should Not Display----");
			phoneApp2.waitForDialerPage();	
	}
}