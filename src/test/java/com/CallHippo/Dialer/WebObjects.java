package com.CallHippo.Dialer;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WebObjects {

	@FindBy(xpath = "//input[@name='email']")
	WebElement email;
	@FindBy(xpath = "//input[@name='password']")
	WebElement password;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Sign in')]")
	WebElement login;
	@FindBy(xpath = "//li[@class='isoUser']")
	WebElement userDropdown;
	@FindBy(xpath = "(//span[contains(.,'Logout')])[1]")
	WebElement logout;
	@FindBy(xpath = "//a[contains(.,'Change Password')]")
	WebElement changePassword;
	
	@FindBy(xpath = "//div[@class='panelnumbername_switch bgwhiteimp']//span")
	WebElement webFlagName;
	
	@FindBy(xpath = "//a[@class='ant-btn shdemobtn']")
	WebElement btnScheduleDemo;
	
	@FindBy(xpath = "//h2[contains(.,'Select a Date & Time')]")
	List<WebElement> calender;
	
	@FindBy(xpath = "(//input[@type='checkbox'])[1]")
	WebElement selectAllCheckbox ;
	@FindBy(xpath = "//img[@alt='File Download']")
	WebElement fileDownload ;
	@FindBy(xpath = "//a[contains(.,'Activity Feed Reports')]")
	WebElement activityFeedReports ;
	@FindBy(xpath = "//h4[@class='mgright15 font-weight-bold']")
	WebElement selectedRecords ;
	@FindBy(xpath = "//span[@class='ant-table-column-title']")
	List<WebElement> title ;
	

	@FindBy(xpath = "//h3[contains(.,'User Details')]")
	WebElement userDetailsTxt;
	
	@FindBy(xpath = "(//div[@class='deparmentname_title'])[1]")
	WebElement departmentNameFor2ndNumber;
	
	@FindBy(xpath = "//a[@class='numblsnamemarg ng-binding'][contains(.,'+12314420001')]")
	WebElement settingIcon;
	@FindBy(xpath = "//a[@class='numblsnamemarg ng-binding'][contains(.,'+19377551237')]")
	WebElement settingIcon2;
	@FindBy(xpath = "//button[@test-automation='num_call_recording_switch']")
	WebElement callRecording;
	@FindBy(xpath = "//div[@test-automation='ivr_music_message_tab']")
	WebElement ivrTab;
	@FindBy(xpath = "//div[@test-automation='welcome_music_message_tab']")
	WebElement welcomemessageTab;
	@FindBy(xpath = "//button[@test-automation='num_welcome_message_switch']")
	WebElement welcomeMessage;
	@FindBy(xpath = "//h3[contains(.,'Sticky Agent')]/parent::div/child::div/child::div/child::div/child::div/child::button")
	WebElement Stickyagent;
	@FindBy(xpath = "//h3[contains(.,'Sticky Agent')]/parent::div//div[contains(@class,'deparmentname_description')]")
	WebElement stickyagentDescription;
	@FindBy(xpath = "//span[contains(.,'Strictly bind')]/parent::label/following-sibling::p")
	WebElement StrictlybindDescription;
	@FindBy(xpath = "//span[contains(.,'Loosely bind')]/parent::label/following-sibling::p")
	WebElement LooselybindDescription;
	@FindBy(xpath = "//label[contains(.,'Strictly bind')]/child::span/child::span")
	WebElement Looselybindradiobutton;
	@FindBy(xpath = "//span[@class='moduleSidebarSpan'][contains(.,'Sticky Agent')]")
	WebElement Stickysidemenu;	
	
	@FindBy(xpath = "//span[@class='moduleSidebarSpan'][contains(.,'Number Details')]") // add new
	WebElement numberDetailsidemenu;	
	@FindBy(xpath = "//span[contains(.,'Set custom ringing time for incoming calls')]//parent::div//parent::div//preceding-sibling::div//button[contains(@class,'ant-switch')]") // add new
	WebElement customRingingTime;	
	@FindBy(xpath = "//span[contains(.,'Set custom ringing time for incoming calls')]/../following-sibling::div/following-sibling::div//following-sibling::div[contains(@class,'cursorhand')]//i") // add new
	WebElement customRingingTime_duration_save_Edit;	
	@FindBy(id = "callRingingDuration") // add new
	WebElement customRingingTime_duration;
	@FindBy(xpath = "//div[contains(@class,\"ant-message-error\")]//span")// add new
	WebElement validationErrorMessage;
	@FindBy(xpath = "//span[contains(.,'Set custom ringing time for incoming calls')]/../following-sibling::div/following-sibling::div/div//p")// add new
	WebElement customRingingTime_durationValue;

	
	@FindBy(xpath = "//input[@test-automation='num_team_user_allocation_switch']")
	WebElement userAllocation;
	@FindBy(xpath = "//button[@test-automation='num_number_voicemail_switch']")
	WebElement numberVoicemail;
	@FindBy(xpath = "//button[@test-automation='num_after_work_hours_switch']")
	WebElement numberAfterWorkHours;
	@FindBy(xpath = "//button[@test-automation='num_voicemail_transcription_switch']")
	WebElement numberVoicemailTranscription;
	@FindBy(xpath = "//div[@test-automation='num_opening_always_opened']") WebElement numberAlwaysOpened;
	@FindBy(xpath = "//div[@test-automation='num_opening_custom']") WebElement numberCustom;
	@FindBy(xpath = "//div[@test-automation='num_opening_always_closed']") WebElement numberAlwaysClosed;
	@FindBy(xpath = "//div[@test-automation='num_team_allocation_tab']")
	WebElement numberTeamAllocation;
	@FindBy(xpath = "//div[@test-automation='num_user_allocation_tab']")
	WebElement numberUserAllocation;
	@FindBy(xpath = "//button[@test-automation='ivr_music_message_switch']")
	WebElement numberIVR;
	@FindBy(xpath = "//button[@test-automation='num_show_number_switch']")
	WebElement numberShowFTDCall;
	@FindBy(xpath = "//div[@class='deparmentname_description'][contains(.,'show the CallHippo number during an incoming call')]")
	WebElement numberShowFTDCallDescription;
	
	@FindBy(xpath = "//button[@test-automation='num_call_queue_switch']")
	WebElement numberCallqueue;
	@FindBy(xpath = "//button[@test-automation='num_call_queue_wait_music_switch']")
	WebElement numberCalQueueMusic;
	@FindBy(xpath = "//button[@test-automation='user_user_voicemail_switch']")
	WebElement uservoicemail;
	@FindBy(xpath = "//input[@test-automation='user_activity_feed_switch']")
	WebElement userActivityFeed;
	@FindBy(xpath = "//div[@test-automation='user_available_always_opened']") WebElement userAlwaysOpened;
	@FindBy(xpath = "//div[@test-automation='user_available_custom']") WebElement userCustom;
	@FindBy(xpath = "//div[@test-automation='user_available_always_closed']") WebElement userAlwaysClosed;
	@FindBy(xpath = "//button[@test-automation='user_after_call_work_switch']")
	WebElement userAfterCallwork;
	@FindBy(xpath = "(//button[@test-automation='user_forward_to_device_switch'])[1]")
	WebElement userForwardToDevice;
	@FindBy(xpath = "//span[contains(.,'Forward-to-device')]/parent::div/following-sibling::div//i[contains(.,'edit')]")
	WebElement FTDEditButton;
	@FindBy(xpath = "//input[@placeholder='Forward to device']")
	WebElement FTDTextBox;
	@FindBy(xpath = "(//input[@placeholder='Forward to device'])[2]")
	WebElement FTDTextBox2;
	@FindBy(xpath = "//span[contains(.,'Forward-to-device')]/parent::div/following-sibling::div//i[@class='material-icons'][contains(.,'save')]")
	WebElement FTDSaveButton;
	@FindBy(xpath = "(//span[contains(.,'Forward-to-device')]/parent::div/following-sibling::div//i[@class='material-icons'][contains(.,'save')])[2]")
	WebElement FTDSaveButton2;
	@FindBy(xpath = "(//a[@class='teamMemberNameSpan'])[1]")
	WebElement userSettingButton;
	@FindBy(xpath = "(//input[@type='radio'])/parent::span/following-sibling::span[contains(text(),'Greeting')]")
	WebElement awhmGreeting;
	@FindBy(xpath = "(//input[@type='radio'])/parent::span/following-sibling::span[contains(text(),'Voicemail')]")
	WebElement awhmVoicemail;
	@FindBy(xpath = "(//div[@class='deparmentname_title'])[3]")
	WebElement ExtensionNumber;
	@FindBy(xpath = "(//span[contains(@class,'alctboxfnsize ng-binding')])[1]")
	WebElement UsernameOnnumbersetting1;
	@FindBy(xpath = "(//span[contains(@class,'alctboxfnsize ng-binding')])[2]")
	WebElement UsernameOnnumbersetting2;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement DeallocateUser;
	@FindBy(xpath = "//input[@test-automation='num_call_queue_edit_btn']")
	WebElement callqueueDurationEditButton;
	@FindBy(xpath = "//input[@test-automation='num_call_queue_duration_input']")
	WebElement callqueueDurationTextbox;
	@FindBy(xpath = "//input[@test-automation='num_call_queue_update_btn']")
	WebElement callqueueDurationSaveButton;

	@FindBy(xpath = "//input[@value='simultaneously']")
	WebElement simultaneously;
	@FindBy(xpath = "//input[@value='fixedorder']")
	WebElement fixedorder;
	@FindBy(xpath = "//input[@value='roundrobin']")
	WebElement roundrobin;
	@FindBy(xpath = "//button[contains(.,'Update')]")
	WebElement updateButton;

	@FindBy(xpath = "//button[@class='btn btn-default  btn-primary']")
	WebElement yesButtong;
	@FindBy(xpath = "//a[@href='/activityFeed']")
	WebElement activityfeed;
	
	@FindBy(xpath = "//div[@role='tab'][contains(.,'SMS')]")
	WebElement smsTabactivityfeed;
	@FindBy(xpath = "//a[@class='ant-anchor-link-title'][contains(.,'smsSMS Limit')]")
	WebElement smsLimit;
	@FindBy(xpath = "//section[@id='smsSipSetting']//div[@class='panelnumbername_content']/div")
	WebElement smsLimitContent;
	
	@FindBy(xpath = "//tr[2]//img[@test-automation='activity_feed_outgoing_icon']")
	WebElement outgoingIcon;
	@FindBy(xpath = "//tr[3]//img[@test-automation='activity_feed_outgoing_icon']")
	WebElement outgoingIcon2ndEntry;
	@FindBy(xpath = "//tr[4]//img[@test-automation='activity_feed_outgoing_icon']")
	WebElement outgoingIcon3rdEntry;
	@FindBy(xpath = "//tr[2]//img[@test-automation='activity_feed_incoming_icon']")
	WebElement incomingIcon;
	@FindBy(xpath = "//tr[3]//img[@test-automation='activity_feed_incoming_icon']")
	WebElement incomingIconFor2ndEntry;
	@FindBy(xpath = "//tr[4]//img[@test-automation='activity_feed_incoming_icon']")
	WebElement incomingIconFor3rdEntry;
	@FindBy(xpath = "//tr[2]//span[@test-automation='activity_feed_department']")
	WebElement departmentName;
	@FindBy(xpath = "//tr[3]//span[@test-automation='activity_feed_department']")
	WebElement departmentNameFor2ndEntry;
	@FindBy(xpath = "//tr[4]//span[@test-automation='activity_feed_department']")
	WebElement departmentNameFor3rdEntry;
	@FindBy(xpath = "//tr[2]//span[@test-automation='activity_feed_caller']")
	WebElement callerName;
	@FindBy(xpath = "//tr[2]//td[4][contains(.,'offline_bolt')]")
	WebElement powerDialerIcon;
	@FindBy(xpath = "//tr[3]//td[4][contains(.,'offline_bolt')]")
	WebElement powerDialerIcon2ndentry;
	@FindBy(xpath = "//tr[4]//td[4][contains(.,'offline_bolt')]")
	WebElement powerDialerIcon3rdentry;
	@FindBy(xpath = "//tr[3]//span[@test-automation='activity_feed_caller']")
	WebElement callerNameFor2ndEntry;
	@FindBy(xpath = "//tr[4]//span[@test-automation='activity_feed_caller']")
	WebElement callerNameFor3ndEntry;
	@FindBy(xpath = "//tr[2]//span[@test-automation='activity_feed_client']")
	WebElement clientNumber;
	@FindBy(xpath = "//tr[3]//span[@test-automation='activity_feed_client']")
	WebElement clientNumberFor2ndentry;
	@FindBy(xpath = "//tr[4]//span[@test-automation='activity_feed_client']")
	WebElement clientNumberFor3rdentry;
	@FindBy(xpath = "//tr[2]//span[@test-automation='activity_feed_call_status']")
	WebElement status;
	@FindBy(xpath = "//span[@test-automation='activity_feed_call_status']")
	WebElement callStatus;
	@FindBy(xpath = "(//span[@test-automation='activity_feed_call_status'])[2]")
	WebElement statusFor2ndEntry;
	@FindBy(xpath = "(//span[@test-automation='activity_feed_call_status'])[3]")
	WebElement statusFor3rdEntry;

	@FindBy(xpath = "(//span[contains(@test-automation,'activity_feed_call_status')])[1]")
	WebElement statusCallNotSetup;
	@FindBy(xpath = "(//i[contains(.,'info')])[2]")
	WebElement statusItagBtn;
	@FindBy(xpath = "//div[@class='ant-tooltip-inner']")
	WebElement statusItag;

	@FindBy(xpath = "//tr[2]//td/span[@test-automation='activity_feed_duration']")
	WebElement duration;
	@FindBy(xpath = "//tr[2]//span[@test-automation='activity_feed_call_cost']")
	WebElement callCost;
	@FindBy(xpath = "(//span[contains(@test-automation,'activity_feed_call_cost')])[2]")
	WebElement callCostFor2ndEntry;
	@FindBy(xpath = "(//span[contains(@test-automation,'activity_feed_call_cost')])[2]")
	WebElement callCostFor3rdEntry;

	@FindBy(xpath = "//tr[2]//i[@class='material-icons infopopopen']")
	WebElement iButton;
	@FindBy(xpath = "//i[@class='material-icons infopopopen']")
	List<WebElement> iTagButton;
	@FindBy(xpath = "//audio[contains(@class,'react-audio-player test_automation_activity_feed_drawer_record_audio activityFeedbackaudio')]")
	List<WebElement> callRecordUrl;
	@FindBy(xpath = "//p[contains(text(),'no notes')]")
	List<WebElement> noCallNotes;
	@FindBy(xpath = "//p[contains(text(),'no tags')]")
	List<WebElement> noCallTags;
	
	@FindBy(xpath = "//p[contains(@test-automation,'activity_feed_drawer_notes')]")
	List<WebElement> callNotes;
	@FindBy(xpath = "//span[contains(@class,'calltaglabel')]")
	List<WebElement> callTags;
	@FindBy(xpath = "//tr[@class='ant-table-row ant-table-row-level-0'][@data-row-key[not(contains(., 'feedDate'))]]")
	List<WebElement> noOfRecord;
	@FindBy(xpath = "//p[contains(@class,'ant-empty-description')]")
	WebElement noDataInActivityFeed;
	
	@FindBy(xpath = "//tr[4]//i[@class='material-icons infopopopen']")
	WebElement iButton3rdentry;
	@FindBy(xpath = "//button[@aria-label='Close']/i")
	WebElement closeiButton;
	@FindBy(xpath = "(//i[contains(@class,'material-icons infopopopen')])[2]")
	WebElement iButtonFor2ndEntry;
	@FindBy(xpath = "(//span[contains(.,'play_circle_filled')])[1]")
	WebElement playCircleButton;
	@FindBy(xpath = "(//span[contains(.,'play_circle_filled')])[2]")
	WebElement playCircleButtonFor2ndEntry;
	@FindBy(xpath = "//p[contains(@test-automation,'activity_feed_drawer_no_record_audio')]")
	WebElement recordingUrl;
	@FindBy(xpath = "//p[contains(@test-automation,'activity_feed_drawer_no_record_audio')]")
	WebElement recordingUrlFor2ndEntry;
	@FindBy(xpath = "//p[contains(@test-automation,'activity_feed_drawer_no_record_audio')]")
	WebElement recordingUrlFor3rdEntry;
	@FindBy(xpath = "//button[@class=\"ant-drawer-close\"][1]")
	WebElement crossButtonForClosingiTagPopup;
	@FindBy(xpath = "//span[@class='material-icons recplay'][contains(.,'link')][1]")
	WebElement linkButton;
	@FindBy(xpath = "(//span[@class='material-icons  activityFeedbacksideicon'][contains(.,'sticky_note_2')])[1]")
	WebElement noteButton;

	@FindBy(xpath = "//h3[contains(.,'Department Details')]")
	WebElement deptNameAndNumberTxt;

	@FindBy(xpath = "//button[@test-automation='num_call_queue_edit_btn']")
	WebElement callQueueEditBtn;

	@FindBy(xpath = "//div[@test-automation='num_call_queue_update_btn']")
	WebElement callQueueUpdateBtn;

	@FindBy(xpath = "//div[text()='User Extension ']/../div[contains(@class,'title')]/span")
	WebElement userExtensionTxt;
	
	@FindBy(xpath = "//i[@class='material-icons g_reminder_remove_icon_style'][contains(@id,'close')][contains(.,'clear')]")
	WebElement closelowbalancepopup;
	

	@FindBy(xpath = "//div[contains(text(),'User Extension')]/../../..//following-sibling::div//i[@class='material-icons'][contains(.,'edit')]")
	WebElement EditExtensionTxt;
	
	@FindBy(xpath = "//div[contains(text(),'User Extension')]/..//input[@class[contains(.,'editUserInputstng')]]")
	WebElement EnterNewExtensionTxt;
	
	@FindBy(xpath = "//div[contains(text(),'User Extension')]/../../..//following-sibling::div//i[@class='material-icons'][contains(.,'save')]")
	WebElement SaveExtensionTxt;
	
	@FindBy(xpath = "//h3[contains(.,'Forward-to-device')]/parent::div//i[@class='material-icons'][contains(.,'add')]")
	WebElement addButtonOfFTD;

	@FindBy(xpath = "//a[contains(.,'Settings')]")
	WebElement leftMenu_setting;
	
	@FindBy(xpath = "//span[contains(.,'SDAP')]")
	WebElement sdap_Txt;
	
	@FindBy(xpath = "//a[contains(.,'Plan and Billing')]")
	WebElement leftMenu_Plan_Billing;
	
	
	@FindBy(xpath = "//span[contains(.,'After Work Hours Message')]/../following-sibling::div/following-sibling::div/following-sibling::div/following-sibling::div/child::div/child::div/child::i")
	WebElement AWHM_Msg_save_Edit;
	
	@FindBy(xpath = "//span[contains(.,'After Work Hours Message')]/../following-sibling::div/following-sibling::div/following-sibling::div/following-sibling::div/child::div/child::div/input")
	WebElement AWHM_Msg;
	
	

	@FindBy(xpath = "//span[contains(.,'Your Plan')]")
	WebElement yourPlan_Txt;

	@FindBy(xpath = "//h3[contains(.,'Price Cap')]")
	WebElement priceLimitTxt;
	
	@FindBy(xpath = "//h3[contains(.,'Call Blocking')]")
	WebElement callBlocking_Txt;
	@FindBy(xpath = "//button[@class='ant-btn billingAddEmailBtn']")
	WebElement btnAddnumberBlackList;
	
	@FindBy(xpath = "//input[@placeholder='Enter Number']")
	WebElement txtnumberForBlackList;
	@FindBy(xpath = "//section[@id='callBlocking']//button[@class[contains(.,'ant-btn billingSaveEmailBtn')]]")
	WebElement btnSaveAddnumberBlackList;
	
	@FindBy(xpath = "//div[contains(@class,'ant-message-success')]/span")
	WebElement AddcampaignSuccessmessage;
	

	// gmail xpaths
	@FindBy(xpath = "//input[@id='identifierId']")
	WebElement gemail;
	@FindBy(xpath = "//*[@id=\"identifierNext\"]/div/button")
	WebElement gemailNext;
	@FindBy(xpath = "//input[@name='password']")
	WebElement gpassword;
	@FindBy(xpath = "//*[@id=\"passwordNext\"]/div/button")
	WebElement gpasswordNext;
	@FindBy(xpath = "//span[contains(text(),'CallHippo: Magic Link')]")
	WebElement mailtitle;
	@FindBy(xpath = "//a[contains(.,'Magic login')]")
	WebElement magicLogin;
	
	@FindBy(xpath = "//section[@id='priceLimit']//button[@class[contains(.,'priceLimitEditBtn')]]")
	List<WebElement> outgoing_pricelimit_edit_Btn;
	
	@FindBy(xpath = "//section[@id='priceLimit']//div[@class[contains(.,'billingAddEmailInput')]]/input[@placeholder='Enter a value between $0.01 to $100']")
	WebElement outgoing_pricelimit_placeHolderTxt;
	
	@FindBy(xpath = "//section[@id='priceLimit']//button[@class[contains(.,'ant-btn billingSaveEmailBtn')]][@disabled[contains(.,'')]]")
	WebElement outgoing_pricelimit_save_disable_Btn;
	
	@FindBy(xpath = "//section[@id='priceLimit']//button[@class[contains(.,'ant-btn billingSaveEmailBtn')]]")
	WebElement outgoing_pricelimit_save_enable_Btn;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	WebElement validateSuccessMsg;

	
	@FindBy(xpath = "//span[@class='sidebarCreditSpan'][contains(.,'Credit:')]")
    WebElement left_panel_total_credit;
	
	@FindBy(xpath = "//div[contains(text(),'Available Credits')]/../div/span")
    WebElement setting_available_credit;
	
	@FindBy(xpath = "//a[contains(.,'Dashboard')]")
	WebElement left_panel_dashboard;
	
	@FindBy(xpath = "//h5[@class='titleboxdashicne'][contains(.,'Call Summary')]")
	WebElement dashboard_callSummary_txt;
	
	@FindBy(xpath = "(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[1]")
	WebElement itag_notes_icon_1stEntry;
	
	@FindBy(xpath = "(//span[@class[contains(.,'activityFeedbacksideicon')]][contains(.,'sticky_note_2')])[2]")
	WebElement itag_notes_icon_2ndEntry;
	
	@FindBy(xpath = "//p[@test-automation='activity_feed_drawer_notes']")
	WebElement itag_notes_text;
	
	@FindBy(xpath = "//p[@test-automation='activity_feed_drawer_empty_notes']")
	WebElement itag_EmptyNotes_text;
	
	@FindBy(xpath = "//button[contains(.,'Yes')] | //button[contains(.,'YES')]")
	WebElement yesButon;
	

	@FindBy(xpath = "//button[@type='button'][contains(.,'Create Team')]")
	WebElement createTeamBtn;
	

	@FindBy(xpath = "//input[contains(@name,'teamName')]")
	WebElement Enterteamname;
	
	@FindBy(xpath = "//button[contains(.,'Create')]")
	WebElement createTeam;
	
	
	
	@FindBy(xpath = "(//button[@type='button'][contains(.,'Delete')])[1]") WebElement voidTheCallBtn;
	@FindBy(xpath = "(//button[@type='button'][contains(.,'Call')])[1]") WebElement makeTheCallBtn;
	@FindBy(xpath = "//tbody/tr[1]/td[1]/span") WebElement nameOnCallPlanner;
	@FindBy(xpath = "//tbody/tr[1]/td[2]/span") WebElement contactNameOnCallPlanner;
	@FindBy(xpath = "//tbody/tr[1]/td[3]/span") WebElement contactNumberOnCallPlanner;
	@FindBy(xpath = "(//td[@class='columnWidth']/child::span)[1]") WebElement TimeOfRemInWeb;
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Completed')]") WebElement completedOnFromCallPlanner;
	@FindBy(xpath = "//a[@href='/callplanner']") WebElement callPlannerSideMenu;
	@FindBy(xpath = "//p[@class='ant-empty-description'][contains(.,'No Data')]") WebElement noDataOnCallPlanner;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]") WebElement yesButtonOfCallplanner;
	@FindBy(xpath = "//div[@class=\"ant-message\"]/span//span") WebElement validationMessage;
	@FindBy(xpath = "//p[contains(.,'Open Dialer')]") WebElement open_dialer_button;
	@FindBy(xpath = "//button[@type='button'][contains(.,'filter_alt')]") WebElement web_livecall_filter_button;
	@FindBy(xpath = "//h6[@class='lvTitle'][contains(.,'Users/Teams')]") WebElement web_UserTeam_filter_Popup;
	@FindBy(xpath = "//div[contains(@class,'ant-select-selection__placeholder')]") WebElement web_filter_Search;
	@FindBy(xpath = "//span[contains(@class,'ant-select-selection__clear')]") WebElement Clear_WebFilter_search;
	@FindBy(xpath = "//input[@class='ant-select-search__field']") WebElement Enter_WebFilter_search;
	@FindBy(xpath = "//h5/span[contains(.,'minutes')]") WebElement dashboardFreeIncomingMinutes;
	
	
	
	
	
	// Integration xpaths
	
	@FindBy(xpath = "//a[@href='/integrations'][contains(.,'Integrations')]") WebElement sideMenuIntegrationLink;
	@FindBy(xpath = "//h3[@class='sectionTitle'][contains(.,'CRM')]") WebElement sectionTitleCRM;
	@FindBy(xpath = "//span[@class='integrationsName'][contains(.,'Pipedrive')]/..//button[@type='button'][contains(.,'+ INTEGRATE')]") WebElement PipeDriveIntegrationButton;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Connect Now')]") WebElement connectNowButton;
	@FindBy(xpath = "//input[@id='login']") WebElement pipeDriveEmail;
	@FindBy(xpath = "//input[@id='password']") WebElement pipeDrivePassword;
	@FindBy(xpath = "//button[contains(.,'Log in')]") WebElement pipeDriveLoginButton;
	@FindBy(xpath = "(//button[@data-type='continue'][contains(.,'Continue to the App')])[2]") WebElement continueTotheApp;
	@FindBy(xpath = "//button[@type='button'][contains(.,'SKIP')]") WebElement skipButton;
	@FindBy(xpath = "//span[@class='integrationsName'][contains(.,'Pipedrive')]/..//i[contains(@class,'icon checkDeleteIcon')]") WebElement checkicon;
	@FindBy(xpath = "//a[@href='/contacts']") WebElement contactIconPipedrive;
	
	
	@FindBy(xpath = "//span[@class='moduleSidebarSpan'][contains(.,'compare_arrowsREST API')]") WebElement restAPISubMenu;
	@FindBy(xpath = "//span[contains(.,'Your personal API token')]/../../../../..//p") WebElement restAPI;
	
	@FindBy(xpath = "//span[contains(.,'Zoho.in')]") WebElement selectZohoDomainRadioBtn;
	@FindBy(xpath = "//button[@class='btn'][contains(text(),'Accept')]") WebElement zohoCRMAcceptBtn;
	@FindBy(xpath = "//button/i[@class[contains(.,'plus')]]") WebElement connectZohoEmailBtn;
	@FindBy(xpath = "//input[@id='login_id']") WebElement insertZohoEmail;
	@FindBy(xpath = "//button[@id='nextbtn']/span[contains(.,'Next')]") WebElement zohoEmailNextBtn;
	@FindBy(xpath = "//input[@id='password']") WebElement insertZohoPassword;
	@FindBy(xpath = "//button[@id='nextbtn']/span[contains(.,'Sign in')]") WebElement zohoSignInBtn;
	
	
	
	// FreshDesk integration xpaths
	
	@FindBy(xpath = "//span[@class='moduleSidebarSpan'][contains(.,'HelpDesk')]") WebElement helpdeskLink;
	@FindBy(xpath = "//span[@class='integrationsName'][contains(.,'Freshdesk')]/..//button[@type='button'][contains(.,'+ INTEGRATE')]") WebElement freshDeskIntegrationButton;
	@FindBy(xpath = "//input[contains(@id,'apikey')]") WebElement freshdeskApiKey;
	@FindBy(xpath = "//input[@id='domain']") WebElement freshdeskDomainName;
	@FindBy(xpath = "//button[@type='submit'][contains(.,'SAVE')]") WebElement freshdeskSaveButton;
	@FindBy(xpath = "//span[@class='integrationsName'][contains(.,'Freshdesk')]/..//i[contains(@class,'icon checkDeleteIcon')]") WebElement FreshDeskcheckicon;


	
	@FindBy(xpath = "//span[@class='usrname_topbar']")
	WebElement userProfile;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Export Contacts')]")
	WebElement exportContacts;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement btnYes;
	
	@FindBy(xpath = "//div[@test-automation='activity_feed_calllog_date']")
	List<WebElement> callog_date;
	@FindBy(xpath = "//button[@class='ant-btn filterBtn']//img")
	WebElement affilter;
	@FindBy (xpath = "//div[@id='rcDialogTitle0'][contains(.,'Filters')]")
	WebElement afpopupFilterTitle;
	@FindBy (xpath = "//label[@title='Select Callers:']/parent::div/following-sibling::div")
	WebElement afpopupSelectCaller;
	@FindBy (xpath = "//label[@title='Select Callers:']/parent::div/following-sibling::div//div[@unselectable='on'][contains(.,'Please select')]")
	WebElement afpopupSelectCallerDropdown;
	@FindBy (xpath = "//li[@role='option'][contains(.,'Every One')]")
	WebElement afpopupCallerDropdownEveryOne;
	@FindBy (xpath = "//li[@role='option'][contains(.,'Users')]")
	WebElement afpopupCallerDropdownUsers;
	@FindBy (xpath = "//li[@role='option'][contains(.,'Teams')]")
	WebElement afpopupCallerDropdownTeams;
	
	
	@FindBy (xpath = "//label[@title='Select Users:']/parent::div/following-sibling::div//div[@unselectable='on'][contains(.,'Please select')]")
	List<WebElement> afpopupSelectUsersDropdown;
	@FindBy (xpath = "//label[@title='Select Teams:']/parent::div/following-sibling::div//div[@unselectable='on'][contains(.,'Please select')]")
	List<WebElement> afpopupSelectTeamsDropdown;
	@FindBy (xpath = "//label[@title='Call Status:']/parent::div/following-sibling::div//div[@unselectable='on'][contains(.,'Please select')]")
	WebElement afpopupCallStatusDropdown;
	
	@FindBy (xpath = "//li[@role='option'][contains(.,'Call not setup')]")
	WebElement afpopupCallStatusCallNotSetup;
	
	
	
	@FindBy (xpath = "//label[@title='Tags:']/parent::div/following-sibling::div//div[@unselectable='on'][contains(.,'Please select')]")
	WebElement afpopupTagsDropdown;
	@FindBy (xpath = "//li[@role='option'][contains(.,'main user team')]")
	WebElement afpopupTagsDropdownMainUserTeam;
	@FindBy (xpath = "//li[@role='option'][contains(.,'subuser team')]")
	WebElement afpopupTagsDropdownSubuserTeam;
	@FindBy (xpath = "//li[@role='option'][contains(.,'Team')]")
	WebElement afpopupTagsDropdownTeam;
	
	@FindBy (xpath = "//label[@title='From Number:']/parent::div/following-sibling::div//input")
	WebElement afpopupFromNumberTextbox;
	@FindBy (xpath = "//label[@title='To Number :']/parent::div/following-sibling::div//input")
	WebElement afpopupToNumberTextbox;
	@FindBy (xpath = "//label[@title='Power Dialer :']/parent::div/following-sibling::div//div[@title='All Call Logs']")
	WebElement afpopupPowerDialerDropdown;
	@FindBy (xpath = "//label[@title='Select Campaign:']/parent::div/following-sibling::div//div[text()='Please select']")
	WebElement afpopupSelectCampaignDropdown;
	@FindBy (xpath = "//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='Both']")
	List<WebElement> afpopupCallTypeBoth;
	@FindBy (xpath = "//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='Incoming']")
	List<WebElement> afpopupCallTypeIncoming;
	@FindBy (xpath = "//label[@title='Call Type :']/parent::div/following-sibling::div//input[@value='Outgoing']")
	List<WebElement> afpopupCallTypeOutgoing;
	@FindBy(xpath = "//label[@title='From Date :']/parent::div/following-sibling::div//input[@placeholder='Select date']")
	WebElement afpopupFromDate;
	@FindBy(xpath = "//label[@title='To Date :']/parent::div/following-sibling::div//input[@placeholder='Select date']")
	WebElement afpopupToDate;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement afpopupApplyButton;
	@FindBy(xpath = "//button[@class='ant-modal-close']")
	WebElement afpopupCloseButton;
	
	@FindBy(xpath = "(//li[@role='option'][contains(.,'Check All')])[1]")
	WebElement afpopupCheckAll;
	@FindBy(xpath = "(//li[@role='option'][contains(.,'Check All')])[2]")
	WebElement afpopupCheckAllForTag;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Call not setup')]")
	WebElement afpopupCallNotSetup;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Cancelled')]")
	WebElement afpopupCancelled;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Completed')]")
	WebElement afpopupCompleted;
	@FindBy(xpath = "//li[@role='option'][contains(.,'IVR message')]")
	WebElement afpopupIVRmessage;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Missed')]")
	WebElement afpopupMissed;
	@FindBy(xpath = "//li[@role='option'][contains(.,'No Answer')]")
	WebElement afpopupNoAnswer;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Rejected')]")
	WebElement afpopupRejected;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Unavailable')]")
	WebElement afpopupUnavailable;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Voicemail')]")
	WebElement afpopupVoicemail;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Welcome message')]")
	WebElement afpopupWelcomemessage;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Only Power Dialer')]")
	WebElement afpopupOnlyPowerDialer;
	@FindBy(xpath = "//li[@role='option'][contains(.,'Without Power Dialer')]")
	WebElement afpopupWithoutPowerDialer;

	@FindBy(xpath = "//div[@class='fileUploadNote']/p[@class='fileUploadError']") WebElement fileUploadErrorMsg;
	
	
	
}
