package com.CallHippo.Dialer;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;


public class DialerIndex extends DialerObjects {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait1;
	WebDriverWait wait2;
	Actions action;
	JavascriptExecutor js;

	public DialerIndex(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait1 = new WebDriverWait(this.driver, 1);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		js = (JavascriptExecutor) driver;
	}

	public void wait_reactjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;

		if (js.executeScript("return document.readyState").toString().equals("complete")) {
			System.out.println("Page Is loaded.");
			return;
		}

		for (int i = 0; i < 60; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			// To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}
	}

	public String getTitle() {
		String signupTitle = driver.getTitle();
		return signupTitle;
	}

	public String validateforgotPasswordPageTitle() {
		wait.until(ExpectedConditions.titleContains("Forgot Password | Callhippo.com"));
		return driver.getTitle();
	}

	public boolean validateDialerAppLoggedinPage() {
		try {
			if (email.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public void enterEmail(String email) {
		
		wait.until(ExpectedConditions.visibilityOf(this.email));
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void navigateUrlto(String url) {
		driver.get(url);
	}

	public void enterPassword(String password) {
		wait.until(ExpectedConditions.visibilityOf(this.password));
		this.password.clear();
		this.password.sendKeys(password);
	}

	public void clickOnLogin() {
		wait.until(ExpectedConditions.visibilityOf(login));
		login.click();
	}

	public void clickOnLogout() {
		wait.until(ExpectedConditions.visibilityOf(logout));
		Common.pause(5);
		logout.click();
	}

	public String getDialerUsername() {		
		wait.until(ExpectedConditions.visibilityOf(dialUserName));
		System.out.println(dialUserName.getText());
		return dialUserName.getText();
	}
	public String getDialerEmail() {		
		wait.until(ExpectedConditions.visibilityOf(dialEmail));
		String email=dialEmail.getText();
		return email.substring(0, email.length() - 3);
	}
	public String getFlagFromDialer() {		
		wait.until(ExpectedConditions.visibilityOf(dialFlagName));
		return dialFlagName.getAttribute("class");
	}
	public void enterNumberinDialer(String number) {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(backBtn));
			while (backBtn.isDisplayed() == true) {
				backBtn.click();
			}
		} catch (Exception e) {
		}

		wait.until(ExpectedConditions.visibilityOf(numberTextbox));

		numberTextbox.sendKeys(number);
	}
	public void enterSomeNumberinDialer(String number) {
		
		try {
			wait.until(ExpectedConditions.elementToBeClickable(backBtn));
			while (backBtn.isDisplayed() == true) {
				backBtn.click();
			}
		} catch (Exception e) {
		}
		
		wait.until(ExpectedConditions.visibilityOf(numberTextbox));
		
		numberTextbox.sendKeys(number.substring(0,4));
	}
	public String getsearchContactName() {		
		wait.until(ExpectedConditions.visibilityOf(searchContactName));
		return searchContactName.getText();
	}
	public String getsearchContactNumber() {		
		wait.until(ExpectedConditions.visibilityOf(searchContactNumber));
		return searchContactNumber.getText();
	}
	public void clickOnsearchResult() {		
		wait.until(ExpectedConditions.visibilityOf(searchContactName));
		 searchContactName.click();
	}
	public String getNumberFromDialer() {		
		wait.until(ExpectedConditions.visibilityOf(numberTextbox));
		return numberTextbox.getAttribute("value");
	}
	
	public void selectAllTextAndCopy() {
		wait.until(ExpectedConditions.visibilityOf(addContactName));
		addContactName.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		addContactName.sendKeys(Keys.chord(Keys.CONTROL, "c"));
	}
	
	public void pasteAllText() {
		wait.until(ExpectedConditions.visibilityOf(numberTextbox));
		numberTextbox.click();
		numberTextbox.sendKeys(Keys.chord(Keys.CONTROL, "v"));
	}
	
	public boolean validateNewContactIsDisplayed() {
		try {
			if (txtNewContact.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}
	public boolean validateSendMessageIsDisplayed() {
		try {
			if (txtSendMessage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}
	public void hardRefreshByKeyboard() {
		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL)
		.keyDown(Keys.SHIFT)
		.sendKeys("r")
		.keyUp(Keys.SHIFT)
		.keyUp(Keys.CONTROL).build().perform();
		
	}
	public void keyPressByKeyboard(String accNum) {
		char[] ch = accNum.toCharArray();
		 for(int i=2;i<ch.length;i++){  
			 System.out.print(ch[i]);  
			 Actions action = new Actions(driver);
				action.sendKeys(String.valueOf(ch[i])).build().perform();
				 Common.pause(3);
			 } 
	}


	
	public void clearNumberinDialer() {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(backBtn));
			while (backBtn.isDisplayed() == true) {
				backBtn.click();
			}
		} catch (Exception e) {
		}

	}
	public boolean validateTimeZoneIsDisplayed() {
		try {
			if (timeZone.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}
	public String getDateTimeFromDialer() {
		wait.until(ExpectedConditions.visibilityOf(dateTimeZone));
		System.out.println("from dialer:"+dateTimeZone.getText());
		return dateTimeZone.getText();
	}
	public void clickOnNumberFromDailpad(String accNum) {
		 char[] ch = accNum.toCharArray();
		 for(int i=2;i<ch.length;i++){  
			 System.out.print(ch[i]);  
			 WebElement numKey= driver.findElement(By.xpath("//div[contains(@class,'numtype_design')][text()='"+ch[i]+"']"));
				wait.until(ExpectedConditions.visibilityOf(numKey));
				numKey.click();
				Common.pause(2);
			 }  

	}
	public String moveOnNumberFromDailpadAndgetColor(String num) {
		WebElement numKey= driver.findElement(By.xpath("//div[@class='numtype_design cursor tcenter'][text()='"+num+"']"));
		wait.until(ExpectedConditions.visibilityOf(numKey));
		Actions action = new Actions(driver);
		action.moveToElement(numKey).perform();
		System.out.println("Color of a digit after mouse hover : "	+ numKey.getCssValue("color"));
		return numKey.getCssValue("color");
	}
	public String getDayNumberSuffix(String day) {
	    if (Integer.parseInt(day) >= 11 && Integer.parseInt(day) <= 13) {
	        return "th";
	    }
	    switch (Integer.parseInt(day) % 10) {
	    case 1:
	        return "st";
	    case 2:
	        return "nd";
	    case 3:
	        return "rd";
	    default:
	        return "th";
	    }
	}
	public String getTimeAndDateCountryWise(String timeZoneValue) {
		String day;
	       Date date = new Date();
	       TimeZone timeZone = TimeZone.getTimeZone(timeZoneValue);
	      //DateFormat dateFormat = new SimpleDateFormat("hh:mm aa, ");
	      DateFormat dateFormat = new SimpleDateFormat("hh:mm aa, ");
	      
	      Calendar cal = Calendar.getInstance(timeZone);
	      dateFormat.setTimeZone(cal.getTimeZone());

	       DateFormat todayDateFormat = new SimpleDateFormat("dd");
	      
	       todayDateFormat.setTimeZone(timeZone);
	     
	       if(todayDateFormat.format(date).substring(0,1).equals("0")) {
	       day= todayDateFormat.format(date).replace("0", "");
	       }else {
	    	   day= todayDateFormat.format(date);
	       }	  

	       DateFormat requiredDayFormat = new SimpleDateFormat(" MMM, E");
			  
	      requiredDayFormat.setTimeZone(timeZone);

	     // String currentTimeAndDate = dateFormat.format(cal.getTime())+todayDateFormat.format(date)+getDayNumberSuffix(todayDateFormat.format(date))+requiredDayFormat.format(date);
	      String currentTimeAndDate = dateFormat.format(cal.getTime()).toLowerCase()+day+getDayNumberSuffix(todayDateFormat.format(date))+requiredDayFormat.format(date);
	System.out.println("From TimeZone:"+currentTimeAndDate);
	     return currentTimeAndDate;
	
	}

	public void clickOnDialButton() {
		wait.until(ExpectedConditions.visibilityOf(dialButton));
		dialButton.click();
	}
	public boolean validateDialButtonIsEnabled() {
		wait.until(ExpectedConditions.visibilityOf(dialButton));
		try {
			if (dialButton.isEnabled()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		
	}
	public void clickOnReDialButton() {
		wait.until(ExpectedConditions.visibilityOf(redialButton));
		redialButton.click();
	}
	public boolean validateDialButtonIsDisplayed() {
		
		try {
			if (redialButton.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		
	}
	public void clickDialButtonOnCalldtlPage() {
		wait.until(ExpectedConditions.visibilityOf(dialCallDtlPage));
		dialCallDtlPage.click();
	}
	public String validateCallingScreenOutgoingVia() {
		wait.until(ExpectedConditions.visibilityOf(oCallingScreenOutgoingVia));
		return oCallingScreenOutgoingVia.getText();
	}

	public String validateCallingScreenOutgoingNumber() {
		wait.until(ExpectedConditions.visibilityOf(oCallingScreenOutgoingNumber));
		return oCallingScreenOutgoingNumber.getText();
	}
	
	public String validateSubUserOutgoingNumberCallingScreen() {
		wait.until(ExpectedConditions.visibilityOf(subUserOutgoingNumberCallingScreen));
		return subUserOutgoingNumberCallingScreen.getText();
	}
	
	public String validateSubUserIncomingNumberCallingScreen() {
		wait.until(ExpectedConditions.visibilityOf(subUserIncomingNumberCallingScreen));
		return subUserIncomingNumberCallingScreen.getText();
	}

	public String validateExtCallOutgoingNameCallingScreen() {
		wait.until(ExpectedConditions.visibilityOf(nameOnOutgoingCallingScreen));
		return nameOnOutgoingCallingScreen.getText();
	}
	
	public String validateExtCallIncomingNameCallingScreen() {
		wait.until(ExpectedConditions.visibilityOf(nameOnIncomingCallingScreen));
		return nameOnIncomingCallingScreen.getText();
	}
	
	public String validateCallingScreenIncomingVia() {
		wait.until(ExpectedConditions.visibilityOf(oCallingScreenIncomingVia));
		return oCallingScreenIncomingVia.getText();
	}

	public String validateCallingScreenIncomingNumber() {
		wait.until(ExpectedConditions.visibilityOf(oCallingScreenIncomingNumber));
		return oCallingScreenIncomingNumber.getText();
	}

	public void clickOnOutgoingHangupButton() {
		wait.until(ExpectedConditions.visibilityOf(oHangupbtn));
		oHangupbtn.click();
	}

	public void clickOnIncomingHangupButton() {
		wait.until(ExpectedConditions.visibilityOf(oHangupbtn));
		oHangupbtn.click();
	}

	public void clickOnIncomingRejectButton() {
		wait.until(ExpectedConditions.visibilityOf(iRejectbtn));
		iRejectbtn.click();
	}

	public void clickOnDialPad() {
		wait.until(ExpectedConditions.visibilityOf(dialpadButtonEnabled));
		dialpadButtonEnabled.click();
	}

	public void pressIVRKey1() {
		pressKey1.sendKeys("1");
	}

	public void pressIVRKey(String IVROption) {
		pressKey1.sendKeys(IVROption);
	}

	public boolean verifyDisabledMuteHoldForward() {
		try {
			if (disableMuteHoldForward.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateDisabledNoteButton() {
		System.out.println("verify note button");
		try {
			if (noteButton.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateDisabledDialpadButton() {
		System.out.println("verify Dialpad button");
		try {
			if (dialpadButton.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void waitForDialerPage() {
		wait.until(ExpectedConditions.visibilityOf(backBtn));

	}

	public boolean validateDialerScreenDisplayed() {
		try {
			if (backBtn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean validateDialerScreen() {
		List<WebElement> backButton=driver.findElements(By.xpath( "//*[@id=\"backSpace\"]"));
		try {
			if (backButton.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean validateRecordingPlayButtonDisplay() {
		try {
			if (recordingPlayBtn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean validateAudioProgressBarDisplay() {
		try {
			if (audioProgressBar.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean validateAudioRecordingDurationDisplay() {
		try {
			if (audioRecordingDuration.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void clickOnSideMenu() {
		wait.until(ExpectedConditions.visibilityOf(menuSlider));
		menuSlider.click();
	}
	public boolean validateSideMenuIsDisplayed() {
		if(menuSlider.isDisplayed()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean ValidateOnSideMenuBar(String Modulename ) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//li[contains(@class,'nav_chmenu_list')][contains(.,'"+Modulename+"')]"))));
		return true;
	}
	
	public void clickOnSideMenuDialPad() {
		wait.until(ExpectedConditions.visibilityOf(sideMenuDialPad));
		sideMenuDialPad.click();
	}

	public void clickOnAllCalls() {
		wait.until(ExpectedConditions.visibilityOf(allCalls));
		allCalls.click();
		wait.until(ExpectedConditions.visibilityOf(callLogs));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", callLogs);
		Common.pause(1);
	}
	
	public void clickOnAllCallsInbox() {
		wait.until(ExpectedConditions.visibilityOf(allCalls));
		allCalls.click();
		wait.until(ExpectedConditions.visibilityOf(inboxLogs));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", inboxLogs);
		Common.pause(1);
	}

	public String validateViaNumberInAllCalls() {
		wait.until(ExpectedConditions.visibilityOf(allCallsVia));
		return allCallsVia.getText();
	}
	
	public String validateViaNumberInAllCallsFor2ndEntry() {
		wait.until(ExpectedConditions.visibilityOf(allCallsViaFor2ndEntry));
		return allCallsViaFor2ndEntry.getText();
	}

	public String validateNumberInAllcalls() {
		wait.until(ExpectedConditions.visibilityOf(allCallsNUmber));
		return allCallsNUmber.getText();
	}
	
	public String validateNumberInInboxCalls() {
		wait.until(ExpectedConditions.visibilityOf(inboxNumber));
		return inboxNumber.getText();
	}
	
	public boolean validateTagInInboxdetailspage(String tagname) {
		try {
		js.executeScript("arguments[0].scrollIntoView();", dialerTagName);
		WebElement tag = 	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='calloogtagndiv']//span[@class='ant-tag'][contains(.,'"+tagname+"')]"))));
		return true;
		}catch (Exception e) {
			return false;
		}
	
	
	}
	
	public String validateNameOnSMSThread() {
		wait.until(ExpectedConditions.visibilityOf(NameatSMSthreadpage));
		return NameatSMSthreadpage.getText();
	}
	
	public String validateOnlyNumberOnSMSThread() {
		wait.until(ExpectedConditions.visibilityOf(NameatSMSthreadpage));
		return NameatSMSthreadpage.getText();
	}
	
	public String validateNumberOnSMSThread() {
		wait.until(ExpectedConditions.visibilityOf(NumberatSMSthreadpage));
		return NumberatSMSthreadpage.getText();
	}
	
	public String validateNameOnSMSpage() {
		wait.until(ExpectedConditions.visibilityOf(NameatSMSpage));
		return NameatSMSpage.getText();
	}
	
	public void ClickonSMSThraedOffirstthraed(String NameNumber) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//span[contains(@class,'block smsThreadName')][contains(.,'"+NameNumber+"')]")))).click();
	}
	
	public void ClickonSMSThraedOf2ndthraed(String NameNumber) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "(//span[contains(@class,'block smsThreadName')][contains(.,'"+NameNumber+"')])[2]")))).click();
	}
	
	public String validateNameOncontactpage() {
		wait.until(ExpectedConditions.visibilityOf(NameOncontactpage));
		return NameOncontactpage.getText();
	}
	
	public String validateNameOncontactDtlpage() {
		wait.until(ExpectedConditions.visibilityOf(NameOncontactDtlpage));
		return NameOncontactDtlpage.getText();
	}
	public String validateCompanyNameOncontactDtlpage() {
		wait.until(ExpectedConditions.visibilityOf(companyname));
		return companyname.getText();
	}
	
	public String validateEmailIdOncontactDtlpage() {
		wait.until(ExpectedConditions.visibilityOf(emailID));
		return emailID.getText();
	}
	
	public String validateNameOncontactDisplaypage() {
		wait.until(ExpectedConditions.visibilityOf(nameOncontactdisplaypage));
		return nameOncontactdisplaypage.getText();
	}
	public String validateNameOnContactDisplaypage(int i) {
		WebElement name = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[@class='t__ellipsis d__block lognum'])["+i+"]"))));
		return name.getText();
		}
	public void clickOnNameOncontactDisplaypage() {
		wait.until(ExpectedConditions.visibilityOf(nameOncontactdisplaypage));
		 nameOncontactdisplaypage.click();
	}
	public void clickOnSearchIconOncontactDisplaypage() {
		wait.until(ExpectedConditions.visibilityOf(searchIconOncontactdisplaypage));
		searchIconOncontactdisplaypage.click();
	}
	public void clickOnCloseIconOncontactDisplaypage() {
		wait.until(ExpectedConditions.visibilityOf(closeIconOncontactdisplaypage));
		closeIconOncontactdisplaypage.click();
	}
	public boolean validateSearchIconOncontactDisplaypage() {
		try {
			if (searchIconOncontactdisplaypage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public String searchContactNameOncontactDisplaypage(String Name) {
		wait.until(ExpectedConditions.visibilityOf(searchContact)).click();
		searchContact.clear();
		wait.until(ExpectedConditions.visibilityOf(searchContact)).sendKeys(Name);;
		return Name;	
	}
	public void clearSearchContact() {
		
		try {
			wait.until(ExpectedConditions.elementToBeClickable(searchContact));
			while (!searchContact.getAttribute("value").isEmpty()) {
				Common.pause(2);
				searchContact.sendKeys(Keys.BACK_SPACE);
			}
		} catch (Exception e) {
		}
	
	}
	public String validateMessageForNoMatchContactOnContactDisplaypage() {
		wait.until(ExpectedConditions.visibilityOf(txtForNoContact));
		return txtForNoContact.getText();
	}
	public boolean validateContactsTitleOncontactDisplaypage() {
		try {
			if (titleOncontactdisplaypage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public boolean validateAddContactButtonOncontactDisplaypage() {
		try {
			if (btnAddContact.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void clickOnSMSCompose() {
		wait.until(ExpectedConditions.visibilityOf(btnAddContact));
		btnAddContact.click();
	}
	public boolean validateSMSComposeButtonOnSMSPageIsDisplayed() {
		try {
			if (btnAddContact.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public boolean validateSubMenuDropdownOnSMSPage() {
		try {
			if (optionSMSPage.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void clickonSubMenuDropdownOnSMSPage() {
		
	 WebElement optionSMSPage= driver.findElement(By.xpath("//button[@id='dropdown-basic']"));
		try {
			wait.until(ExpectedConditions.visibilityOf(optionSMSPage)).click();
			}catch (Exception e) {
				// TODO: handle exception
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", optionSMSPage);
			}
	}
	public boolean validateoptionSelectAllSMSPage() {
		try {
			if (optionSelectAllSMSPage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void clickonoptionSelectAllOnSMSPage() {
		wait.until(ExpectedConditions.visibilityOf(optionSelectAllSMSPage)).click();
	}
	
	public boolean validateoptionSelectSMSPage() {
		try {
			if (optionSelectSMSPage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void clickonoptionSelectOnSMSPage() {
		wait.until(ExpectedConditions.visibilityOf(optionSelectSMSPage));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", optionSelectSMSPage);
		
	}
	
	public boolean validateAllThreadsCheckboxToSelect() {
		boolean result = false;
		
		for(int i=1;i<=checkboxToSelectSMSPage.size();i++) {
			try {
				WebElement checkboxToSelectSMSPage= driver.findElement(By.xpath("//div[contains(@class,'sms_namechwid')]//following-sibling::div[contains(@class,'smscheckboxctch')]['"+i+"']"));
				if (checkboxToSelectSMSPage.isDisplayed()) {
					result= true;
				} else {
					result= false;
				}
			} catch (Exception e) {
				return result;
			}
		}
		return result;
	}
	public void clickonAnyoneUnreadSMS() {
		wait.until(ExpectedConditions.visibilityOf(checkboxToSelectSMSPage.get(0))).click();
	}
	
	public boolean validateMarksAsReadSMSPage() {
		try {
			if (btnMarkAsRead.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void clickonMarksAsReadSMSPage() {
		WebElement btnMarkAsRead= driver.findElement(By.xpath("//div[contains(@class,'marreadopdiv_innerdiv')]//p[text()='MARK AS READ']"));
		wait.until(ExpectedConditions.visibilityOf(btnMarkAsRead)).click();
	}
	public void clickonDeleteSMSPage() {
		wait.until(ExpectedConditions.visibilityOf(optionSDeleteSMSPage)).click();
	}
	public boolean validateDeleteSMSPage() {
		try {
			if (btnDelete.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean validateYesButtonDeleteSMSPage() {
		try {
			if (btnYesOnDeleteSMS.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void clickonCancelDeleteSMSPage() {
		WebElement btnCancelOnDeleteSMS= driver.findElement(By.xpath("//button[contains(@class,'nochtn')]"));
		wait.until(ExpectedConditions.visibilityOf(btnCancelOnDeleteSMS));
		JavascriptExecutor executor = (JavascriptExecutor)driver;			
		executor.executeScript("arguments[0].click();", btnCancelOnDeleteSMS);	
	}
	public boolean validateCancelButtonDeleteSMSPage() {
		try {
			if (btnCancelOnDeleteSMS.size()>0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public boolean validatenumberTextBoxOnSMSComposePage() {
		try {
			if (numberTextBoxSMSPage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public boolean validateSelectOnSMSPage() {
		try {
			if (numberTextBoxSMSPage.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public String enterNumberWithoutCountrycodeOnSMSPage(String number){
		wait.until(ExpectedConditions.visibilityOf(numberTextBoxSMSPage));
		 String numberWithoutCountrycode=number.substring(2,number.length());
		  System.out.print("number Without Countrycode"+numberWithoutCountrycode);
		numberTextBoxSMSPage.clear();
		numberTextBoxSMSPage.sendKeys(numberWithoutCountrycode);
		numberTextBoxSMSPage.sendKeys(Keys.ENTER);
		return numberWithoutCountrycode;
	}
	public void enterNumberOnSMSPage(String number){
		wait.until(ExpectedConditions.visibilityOf(numberTextBoxSMSPage));
		numberTextBoxSMSPage.clear();
		numberTextBoxSMSPage.sendKeys(number);
		numberTextBoxSMSPage.sendKeys(Keys.ENTER);
	}
	public boolean validatemessageTextareaOnSMSComposePage() {
		try {
			if (messageTextarea.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public void enterMessageOnSMSPage(String message) {
		wait.until(ExpectedConditions.visibilityOf(messageTextarea)).sendKeys(message);
		
	}
	public String getTwoLetterFromContactNameOnSMSPage(String name) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'sms_namechwid')][contains(.,'"+name+"')]/preceding-sibling::div/child::div"))));	
		String firsttwoletter=name.substring(0,2).toUpperCase();
		System.out.println(firsttwoletter);
		return firsttwoletter;
	}
	public String getTwoLetterInIconofContactOnSMSPage(String name) {
		String twoLetter= wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'sms_namechwid')][contains(.,'"+name+"')]/preceding-sibling::div/child::div")))).getText();
			return twoLetter;
		}
	public boolean validateConatctNameOnSMSPageIsDisplayed(String name) {
		List<WebElement> element = driver.findElements(By.xpath("//span[contains(@class,'smsThreadName')][contains(.,'"+name+"')]"));
		if(element.size()>0) {
			return true;
		}else {
			return false;
			}
	
	}
	public String validateConatctNameOnSMSPage(String name) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(@class,'smsThreadName')][contains(.,'"+name+"')]")))).getText();
	
	}
	public boolean validateConatctNameDisplayOnSMSPage() {
		try {
		 wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(@class,'smsThreadName')]"))));
		    return true;
		}catch (Exception e) {
			return false;
		}
	
	}
	public void clickOnSMSThreadOnSMSPage(String name) {
		 wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(@class,'smsThreadName')][contains(.,'"+name+"')]")))).click();
		
	}
	public String validateSMSOnSMSPage(String name) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'"+name+"')]//following-sibling::span[contains(@class,'smsShortMessage')]")))).getText();
	
	}

	public boolean validateUnreadSMSOnSMSPage(String name) {
		String classnameForUnreadMessage = wait
				.until(ExpectedConditions.visibilityOf(driver
						.findElement(By.xpath("//span[contains(@class,'smsThreadName')][contains(.,'" + name + "')]"))))
				.getAttribute("class");
		if (classnameForUnreadMessage.contains("unreadmsgtitle")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateCallhippoSymbolIconOnSMSPage(String number) {
		Common.pause(2);
		List<WebElement> callhippoSymbolIcon =
				driver.findElements(By.xpath("//div[contains(@class,'cursor padrightzero sms_namechwid')][contains(.,'"
						+ number + "')]//preceding-sibling::div/child::div"));
		if (callhippoSymbolIcon.size()>0) {
			return true;
		} else {
			return false;
		}
	}
	public boolean validateMessageOnSMSThread(String message) {
		Common.pause(3);
		WebElement outgoingmessage =
				driver.findElement(By.xpath("//div[contains(@class,'sms_li me')][contains(.,'"
						+ message + "')]"));
		if (outgoingmessage.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	public boolean validateIncomingMessageOnSMSThread(String message) {
		Common.pause(5);
	    List<WebElement> incomingmessage =driver.findElements(By.xpath("//div[contains(@class,'sms_li them')][contains(.,'"+ message + "')]"));
		System.out.println("incomingmessage size......"+incomingmessage.size());
	    if (incomingmessage.size()>0) {
			return true;
		} else {
			return false;
		}
	}
	public boolean validateButtonDepartmentlistOnSMSCompose() {
	Common.pause(2);
		if (button_departmentlist.size()>0) {
			return true;
		} else {
			return false;
		}
	}
	public void clickonButtonDepartmentlistOnSMSCompose() {
		wait.until(ExpectedConditions.visibilityOf(button_departmentlist.get(0))).click();
	}
	public void selectNumberFromDepartmentlistOnSMSCompose(String number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[contains(@class,'contactnameleftpad')]/p[contains(.,'"+number+"')]")))).click();
	}
	
	public void clickoncancelButtonnumberOrcontactTofield(String ContactName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'"+ContactName+"')]//span[@class='sc-ifAKCX jHwUkX']")))).click();
	}
	public boolean validateParticularMessageSMSchatIsDisplayed(String message) {
		Common.pause(2);
		List<WebElement> SMS=driver.findElements(By.xpath("//div[contains(@class,'sms_li')]//div[contains(.,'"+message+"')]"));
			if (SMS.size()>0) {
				return true;
			} else {
				return false;
			}
		}
	public void clickOnParticularMessage(String message) {

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'sms_li')]//div[contains(.,'"+message+"')]")))).click();
			
	}
	public boolean validateTimeofParticularSMSchatIsDisplayed(String message) {
		Common.pause(2);
		List<WebElement> timeofSMS=driver.findElements(By.xpath("//div[text()='"+message+"']/parent::div/parent::li/div[contains(@class,'timestamp block')]"));
			if (timeofSMS.size()>0) {
				return true;
			} else {
				return false;
			}
		}
	
	
	//continue...

	
	public void ClickonSpecificContact(String ContactName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[contains(@class,'contactnameleftpad')]/span[contains(.,'"+ContactName+"')]")))).click();
	}
	
	public String validateNumberOncontactDtlpage(int i) {
	WebElement Number = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[contains(@class,'conDetailNumSpan')])["+i+"]"))));
	return Number.getText();
	}
	
	public String validateNumberOncontactDtlpageAFterscrool(int i) {
		WebElement Number = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[contains(@class,'conDetailNumSpan')])["+i+"]"))));
		js.executeAsyncScript("arguments[0].scrollIntoView();", Number);
		return Number.getText();
		}
	
	
	
	public boolean validaterequiredFieldIconforName() {
		wait.until(ExpectedConditions.visibilityOf(FullnamerequiredIcon));
		return true;
	}
	
	public boolean validaterequiredFieldIconforcampany() {
		try{
		wait2.until(ExpectedConditions.visibilityOf(CompanyNamerequiredIcon));

		if (CompanyNamerequiredIcon.isDisplayed()) {
			return true;
		} 
		else {
			return false;
		}
		}catch (Exception e) {
			return false;
		}
	}
	
	public boolean validaterequiredFieldIconforEmail() {
		try{
			wait2.until(ExpectedConditions.visibilityOf(EmailrequiredIcon));

			if (EmailrequiredIcon.isDisplayed()) {
				return true;
			} 
			else {
				return false;
			}
			}catch (Exception e) {
				return false;
			}
		} 
	
	public boolean validaterequiredFieldIconforNumber() {
		wait.until(ExpectedConditions.visibilityOf(phonenumberequiredIcon));
		return true;
	}
	
	public boolean validatehumanIcononEditcontactpage() {
		wait.until(ExpectedConditions.visibilityOf(Editcontactpagehumanicon));
		return true;
	}
	public boolean validateEditBtnOncontactpage() {
		wait.until(ExpectedConditions.visibilityOf(EditbtnOnAllCalldtlpage));
		return true;
	}
	public boolean validateDeleteBtnOncontactpage() {
		wait.until(ExpectedConditions.visibilityOf(contactDeleteIcon));
		return true;
	}
	
	public void ClickOnEditBtnOncontactpage() {
		wait.until(ExpectedConditions.visibilityOf(EditbtnOnAllCalldtlpage)).click();
	}
	
	public void ClickOnBackBtnEditcontactpage() {
		wait.until(ExpectedConditions.visibilityOf(EditcontactpageBackbutton)).click();
	}
	public void clickOnBackBtnDeletecontactpage() {
		wait.until(ExpectedConditions.visibilityOf(contactDeleteIcon)).click();
	}
	
	public void ClickOnSMSBtnEditcontactpage() {
		wait.until(ExpectedConditions.visibilityOf(SMSbtnOnAllCalldtlpage)).click();
	}
	
	public String validatenameOnNavigationTitle() {
		wait.until(ExpectedConditions.visibilityOf(NameatSMSthreadpage));
		return NameatSMSthreadpage.getText();
	}

	public boolean validatecontactIconatSMSthreadpageIsDisplayed() {

		if (contactIconatSMSthreadpage.isDisplayed()) {
			return true;
		} else {

			return false;
		}
	}
	public void clickOncontactIconatSMSthreadpage() {
		wait.until(ExpectedConditions.visibilityOf(contactIconatSMSthreadpage));
		Actions action = new Actions(driver);
		action.click(contactIconatSMSthreadpage).build().perform();
		
	}
	public boolean validatecontactNameatSMSthreadpage(String name) {
		List<WebElement> elementName = driver.findElements(By.xpath("//span[@class='t__ellipsis d__block mw__90 smsContactName onlyName'][contains(.,'"+name+"')]"));
		
		if(elementName.size()>0) {
			return true;
		}else {
			return false;
		}
	}
	public String validatecontactNameatSMSthreadpage() {
		wait.until(ExpectedConditions.visibilityOf(contactNameatSMSthreadpage));
		return contactNameatSMSthreadpage.getText();
	}
	public void clickOnRightAerrowAtSMSThreadpage() {
		wait.until(ExpectedConditions.visibilityOf(rightaerrowatSMSthreadpage));
		rightaerrowatSMSthreadpage.click();
	}
	
	public void validateEmailAddonContactDtlPage() {
		wait.until(ExpectedConditions.visibilityOf(rightaerrowatSMSthreadpage));
		rightaerrowatSMSthreadpage.click();
	}
	
	public boolean validateNumberAfterclickOnRightAerrowAtSMSThreadpage(String number) {
		WebElement num= driver.findElement(By.xpath("//label/span[text()='"+number+"']"));
		if(num.isDisplayed()) {
			return true;
		}else {
			
		return false;	
	}

		}
	public void clickOnNumberAfterclickOnRightAerrowAtSMSThreadpage(String number) {
		
		WebElement element=driver.findElement(By.xpath(""
				+ "//label/span[text()='"+number+"']"));
		wait.until(ExpectedConditions.visibilityOf(element));
		element.click();
		
	}
	
	
	public void clickOngreentikSMSthreadpage() {
		wait.until(ExpectedConditions.visibilityOf(greentikSMSthreadpage));
		 greentikSMSthreadpage.click();
		 System.out.print("click on tik.");
	}
	public String validateNumberOnSMSthreadPage() {
		wait.until(ExpectedConditions.visibilityOf(NumberatSMSthreadpage));
		return NumberatSMSthreadpage.getText();
	}
	public String EnterNumberonAddContact(String Name) {
		wait.until(ExpectedConditions.visibilityOf(EnterNumber)).click();
		EnterNumber.sendKeys(Keys.CONTROL,"a",Keys.BACK_SPACE);
		wait.until(ExpectedConditions.visibilityOf(EnterNumber)).sendKeys(Name);;
		return Name;	
	}
	public String EnterInvalidNumberonAddContact(String Name) {
		wait.until(ExpectedConditions.visibilityOf(EnterNumber)).click();
		EnterNumber.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		wait.until(ExpectedConditions.visibilityOf(EnterNumber)).sendKeys(Name);;
		return Name;	
	}
	
	public boolean ValidateSaveBtnDisable() {
		wait2.until(ExpectedConditions.visibilityOf(SaveBtnDisable));
		if(SaveBtnDisable.isDisplayed()) {
			return true;
		}else {
			
		return false;	
	}
}
	public boolean ValidateSaveBtnEnable() {
		wait2.until(ExpectedConditions.visibilityOf(saveBtnEnable));
		if(saveBtnEnable.isDisplayed()) {
			return true;
		}else {
			
			return false;	
		}
	}
	public boolean ValidatetickGreenBtnDisable() {
		wait2.until(ExpectedConditions.visibilityOf(tickGreenBtnDisableToAddContactName));
		
		String classes = tickGreenBtnDisableToAddContactName.getAttribute("class");
		boolean isDisabled = classes.contains("divDisable");
		if(isDisabled) {
			return true;
			}else {
				
				return false;	
			}
	
	}
	public String ValidateSaveBtnIsEnabled() {
		wait2.until(ExpectedConditions.visibilityOf(SaveBtnDisable));
		return SaveBtnDisable.getAttribute("focusable");
		
	}
	
	public String EnternextNumberonAddContact(int index, String number) {
		WebElement element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[contains(@placeholder,'Phone Number')][@id='"+index+"']"))));
		element.click();
		element.sendKeys(Keys.CONTROL,"a",Keys.BACK_SPACE);
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(number);;
		return number;	
	}
	
	public void ClickOnSaveBtnAt(String Number) {
	
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[@placeholder='Phone Number'][contains(@value,'"+Number+"')]/following-sibling::div//*[local-name()='svg'][1]")))).click();
		}
	public void ClickOnAddBtnAt(String Number) {
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[@placeholder='Phone Number'][contains(@value,'"+Number+"')]/following-sibling::div//*[local-name()='svg'][1]")))).click();	
	}
	public boolean validateAddBtnAt(String Number) {
		Common.pause(3);
		
		List<WebElement> addbtn=driver.findElements(By.xpath("//input[@placeholder='Phone Number'][contains(@value,'"+Number+"')]/following-sibling::div//*[local-name()='svg']/*[local-name()='path' and contains(@d,'M19')]"));
		if(addbtn.size()==0) {
			return true;
		}else {
			return false;
		}
		}
	public void ClickOnDeleteBtnAt(String Number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[@placeholder='Phone Number'][contains(@value,'"+Number+"')]/following-sibling::div//*[local-name()='svg'][1]")))).click();	
	}
	
	public void ClickOnDeleteBtnofEditContact(String Number) {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[@placeholder='Phone Number'][contains(@value,'"+Number+"')]/following-sibling::div//*[local-name()='svg'][1]")))).click();	
	}catch (Exception e) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[@placeholder='Phone Number'][contains(@value,'"+Number+"')]/following-sibling::div//*[local-name()='svg'][2]")))).click();
	}
}
	
	public void userTeamAllocation(String value) throws InterruptedException {
		Dimension Size = wait.until(ExpectedConditions.visibilityOf(EnterNumber)).getSize();

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[@id='"+Size+"']/following-sibling::div//*[local-name()='svg'][2]")))).click();

	}
	
	public void ClickOnContactDtlCallBtnOnof(String Number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//span[contains(@class,'conDetailNumSpan')][contains(.,'"+Number+"')]/parent::div/following-sibling::div//span[contains(@class,'conDetailNumActionBtn')][1]//*[local-name()='svg']")))).click();
	}
	
	public void ClickOnContactDtlSMSBtnOnof(String Number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//span[contains(@class,'conDetailNumSpan')][contains(.,'"+Number+"')]/parent::div/following-sibling::div//span[contains(@class,'conDetailNumActionBtn')][2]//*[local-name()='svg']")))).click();
	}
	
	public void ClickOnContactDtlRmndrBtnOnof(String Number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//span[contains(@class,'conDetailNumSpan')][contains(.,'"+Number+"')]/parent::div/following-sibling::div//span[contains(@class,'conDetailNumActionBtn')][3]//*[local-name()='svg']")))).click();
	}
	
	public void ClickOnContactDtlBockBtnOnof(String Number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//span[contains(@class,'conDetailNumSpan')][contains(.,'"+Number+"')]/parent::div/following-sibling::div//span[contains(@class,'conDetailNumActionBtn')][4]//*[local-name()='svg']")))).click();
	}

	
	
	
	
	public void ClickonAddcontact() {
		wait.until(ExpectedConditions.visibilityOf(Addcontact)).click();;
		
	}
	
	public String EnterCompanyOnAddContact(String Name) {
		wait.until(ExpectedConditions.visibilityOf(EnterCompanyName)).clear();
		EnterCompanyName.sendKeys(Name);
		return Name;
	}
	
	public String EnterEmailOnAddContact(String email) {
		wait.until(ExpectedConditions.visibilityOf(EnterEmailID)).clear();
		EnterEmailID.sendKeys(email);
		return email;
	}
	
	public String validateNumberInAllcallsFor2ndEntry() {
		wait.until(ExpectedConditions.visibilityOf(allCallsNUmberFor2ndEntry));
		return allCallsNUmberFor2ndEntry.getText();
	}

	public void ClickOnRecentCallActivity(String number) {
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[@title='" + number + "'])[1]"))));
		driver.findElement(By.xpath("(//span[@title='" + number + "'])[1]")).click();
	}
	
	public void completedcalls(int i) {
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Calls Completed : " + i + "')]"))));
		//driver.findElement(By.xpath("(//span[@title='" + number + "'])[1]")).click()
	}
	
	public String getTimeFromAllCalls() {
		
		String time = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[@test-automation='call_log_via_num']/following-sibling::div/span[@test-automation='call_log_call_time'])[1]")))).getText();
		
		String date = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//span[@test-automation='call_log_name_or_num']/ancestor::div[@class='bottomzero']/parent::li/div/span)[1]")))).getText();
	
		System.out.println(date + time.substring(time.indexOf("t")+1));
		return date + time.substring(time.indexOf("t")+1);
		
		
	}
	

	public String validatecallType() {

		wait.until(ExpectedConditions.visibilityOf(allCallsCallType));
		return allCallsCallType.getText();

	}
	
	public String validatecallTypeFor2ndentry() {

		wait.until(ExpectedConditions.visibilityOf(allCallsCallTypeFor2ndentry));
		return allCallsCallTypeFor2ndentry.getText();

	}

	public void clickOnRightArrow() {
		rightArrow.click();
	}
	
	public void clickOnInboxRightArrow() {
		rightArrowOfInboxPage.click();
	}
	
	public void clickOnRightArrowFor2ndEntry() {
		rightArrowFor2ndEntry.click();
	}

	public void clickOnBackOnCallDetailPage() {
		callDetailBackArrow.click();
	}
	
	public String validateNumberInCallDetail() {
		wait.until(ExpectedConditions.visibilityOf(numberinCallDetail));
		return numberinCallDetail.getText();
	}
	public String validateAddNumberInCallDetail() {
		wait.until(ExpectedConditions.visibilityOf(AddNumberinCallDetail));
		return AddNumberinCallDetail.getText();
	}
	
	public String validateNumberInCallDetail1() {
		wait.until(ExpectedConditions.visibilityOf(ExtnumberinCallDetail));
		return ExtnumberinCallDetail.getText();
	}
	
	
	public String validateNameInCallDetail() {
		wait.until(ExpectedConditions.visibilityOf(nameinCallDetail));
		return nameinCallDetail.getText();
	}
	public String validateCallStatusInCallDetail() {
		wait.until(ExpectedConditions.visibilityOf(callStatus));
		return callStatus.getText();
	}
	
	public String getCallerName() {
		wait.until(ExpectedConditions.visibilityOf(callerName));
		return callerName.getText();
	}

	public String validateDepartmentNameinDialpade() {
		wait.until(ExpectedConditions.visibilityOf(departmentNameInDialpade));
		return departmentNameInDialpade.getText();
	}

	public String validateDepartmentNameinCallDetail() {
		wait.until(ExpectedConditions.visibilityOf(departmentNameInCallDetail));
		return departmentNameInCallDetail.getText();
	}

	public String validateDepartmentNameInCallingScreen(String departmentName) {
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("(//span[contains(.,'Outgoing " + departmentName + "')])[2]"))));
		return driver.findElement(By.xpath("(//span[contains(.,'Outgoing " + departmentName + "')])[2]")).getText();

	}

	public String validateNumberInCallingScreen(String number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h3[contains(.,'" + number + "')]"))));
		return driver.findElement(By.xpath("//h3[contains(.,'" + number + "')]")).getText();

	}
	
	public void validateNameandNumberDataOnLeftSide(String nameOrnumber, String EnternameAndnumber) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[contains(@class,'ant-collapse')]/child::div/div[contains(.,'"+nameOrnumber+"')]/child::span")))).click();;
		driver.findElement(By.xpath(""
				+ "//div[contains(@class,'ant-collapse')]/child::div/div[contains(.,'"+nameOrnumber+"')]/following-sibling::div/div[contains(.,'"+EnternameAndnumber+"')]"));

	}

	public String validateDurationInCallingScreen(int secondsDiff) {
		wait.until(ExpectedConditions.visibilityOf(callDurationOnCallingScreen));
		String actualDuration = callDurationOnCallingScreen.getText();
		System.out.println(" result 1 = " + actualDuration);
		String subString = actualDuration.substring(actualDuration.indexOf(":")+1,actualDuration.indexOf(":")+3 );
		System.out.println("SubString : --->>> "+subString);
		int value = Integer.parseInt(subString) + secondsDiff;
		String incrementalDuration = String.valueOf(value);
		return "00:" + incrementalDuration;

	}

	public String validateDurationInCallDetail() {
		wait.until(ExpectedConditions.visibilityOf(callDurationInCallDetail));
		String callDureationTxt = callDurationInCallDetail.getText();
		String newCallDureationTxt = callDureationTxt.substring(callDureationTxt.indexOf(0) + 1, callDureationTxt.indexOf(":") + 3);
		System.out.println("newCallDureationTxt: " + newCallDureationTxt);
		return newCallDureationTxt;
	}
	
	public boolean validateCallDurationonDisplayOnCallingScreen() {
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//span[@test-automation='call_screen_duration']"));
		if (ele.size() != 0) {
			elementIsAvailable = true;
		}else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}
	
	public boolean validateDurationInCallDetailformissed() {
		WebElement callDuration = driver.findElement(By.xpath("//div[@test-automation='call_detail_duration'][contains(.,'Mins')]"));
		try {
			if (callDuration.isDisplayed()) {
				return true;
			} 
			else {
				return false;
			}
		} 
		catch (Exception e) {
			return false;
		}

	}
	public boolean ValidateDurationonCalldetailspage() {
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//div[@test-automation='call_detail_duration'][contains(.,'Mins')]"));
		System.out.println("---ele.size()--"+ele.size());
		if (ele.size() != 0) {
			elementIsAvailable = true;
		}else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}

	public void clickOnIncomimgAcceptCallButton() {
		wait.until(ExpectedConditions.visibilityOf(iAcceptCall));
		iAcceptCall.click();
	}
	

	public boolean validateIncomingCallingScreenIsdisplayed() {
		try {
			if (iAcceptCall.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}
	public boolean validateSessionButtononDialer() {
		Common.pause(3);
		try {
			if (sessionbutton.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	public void waitForIncomingCallingScreen() {
		wait.until(ExpectedConditions.visibilityOf(iAcceptCall));
	}
	public void validatecompletedcallsinDialer() {
		wait.until(ExpectedConditions.visibilityOf(CallCompleted));
	}

	public void clickOnEndACWButton() {
		wait.until(ExpectedConditions.visibilityOf(endACW));
		endACW.click();
	}
	public void selectDepartmentNumber(String number) {
		wait.until(ExpectedConditions.visibilityOf(departmentDropdown)).click();
		Common.pause(1);
		driver.findElement(By.xpath("//p[@class='smallline'][contains(.,'"+number+"')]/parent::div/parent::div/parent::div")).click();
		Common.pause(1);
	}

	public void waitForLastCallDetailPopuppDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(lastCallDetailStatus));
	}
	
	public String validateLastCallDetailDateAndTime() {
		return wait.until(ExpectedConditions.visibilityOf(lastCallDetailDateAndTime)).getText();
	}

	public String validateLastCallDetailStatus() {
		wait.until(ExpectedConditions.visibilityOf(lastCallDetailStatus));
		return lastCallDetailStatus.getText();
	}

	public String validateLastCallDetailUserName() {
		wait.until(ExpectedConditions.visibilityOf(lastCallDetailUserName));
		return lastCallDetailUserName.getText();
	}

	public boolean validateLastCallDetailIncomingIcon() {

		if (lastCallDetailIncomingIcon.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateLastCallDetailOutgoingIcon() {

		if (lastCallDetailOutgoingIcon.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	public void clickOnSetting() {
		wait.until(ExpectedConditions.visibilityOf(setting));
		setting.click();
	}

	private void clickLastCallDetailToggle(String value) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(lastCallDetailToggle));

		if (value.equals("on")) {

			if (lastCallDetailToggle.isSelected()) {

			} else {

				clickablelastCallDetailToggle.click();
			}

		} else {
			if (lastCallDetailToggle.isSelected()) {

				clickablelastCallDetailToggle.click();
			}
		}
	}
	
	private void clickOnCallReminderToggle(String value) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(callReminderToggle));

		if (value.equals("on")) {

			if (callReminderToggle.isSelected()) {

			} else {

				clickableCallReminderToggle.click();
			}

		} else {
			if (callReminderToggle.isSelected()) {

				clickableCallReminderToggle.click();
			}
		}
	}
	
	public void lastCallDetailToggle(String value) throws InterruptedException {
		clickOnSideMenu();
		clickOnSetting();
		Thread.sleep(5000);
		clickLastCallDetailToggle(value);
	}
	
	public void callReminderToggle(String value) throws InterruptedException {
		clickOnSideMenu();
		clickOnSetting();
		Thread.sleep(5000);
		clickOnCallReminderToggle(value);
	}
	
	public void afterCallWorkToggle(String value) throws InterruptedException {
		clickOnSideMenu();
		clickOnSetting();
		Thread.sleep(5000);
		clickAfterCallWorkToggle(value);
		Common.pause(3);
	}
	
	public void clickAfterCallWorkToggle(String value) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(afterCallWorkToggle));

		if (value.equals("on")) {

			if (afterCallWorkToggle.isSelected()) {

			} else {

				clickAfterCallWorkToggle.click();
			}

		} else {
			if (afterCallWorkToggle.isSelected()) {

				clickAfterCallWorkToggle.click();
			}
		}
	}
	public void globalConnectToggle(String value) throws InterruptedException {
		clickOnSideMenu();
		clickOnSetting();
		Thread.sleep(5000);
		clickGlobalConnectToggle(value);
		Common.pause(3);
	}
	
	public boolean validateDefaultNumberSectionIsDisplayed() {
		clickOnSideMenu();
		clickOnSetting();
		Common.pause(5);
		
		if(driver.findElements(By.xpath("//p[normalize-space()='Default Number']")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public void selectDefaultNumber(String number ){
		clickOnSideMenu();
		clickOnSetting();
		Common.pause(5);
		
		//p[normalize-space()='Default Number']/../../../../following-sibling::div/div/div
		
		//p[normalize-space()='+12058592303']/../../following-sibling::div//input
		
	}
	
	public void clickGlobalConnectToggle(String value) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(globalConnectToggle));

		if (value.equals("on")) {

			if (globalConnectToggle.isSelected()) {

			} else {

				clickGlobalConnectToggle.click();
			}

		} else {
			if (globalConnectToggle.isSelected()) {

				clickGlobalConnectToggle.click();
			}
		}
	}
	
	public boolean validateAutoSwitchNumberCheckBoxIsDisplay() {
		boolean value;
		wait.until(ExpectedConditions.visibilityOf(providerName)).click();
		Common.pause(2);
//		value = autoSwitchNumberCheckBox.isDisplayed();
		try {
			wait1.until(ExpectedConditions.visibilityOf(autoSwitchNumberCheckBox));
			value=true;
		}catch (Exception e) {
			value=false;
		} 
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(closeProviderNamePopup)).click();
		wait.until(ExpectedConditions.visibilityOf(providerName));
		return value;
	}
	
	public void autoSwitchNumberCheckBox(String value) {
		wait.until(ExpectedConditions.visibilityOf(providerName)).click();
		wait.until(ExpectedConditions.visibilityOf(autoSwitchNumberCheckBox));
		
		if(value.equals("on")) {
			if(autoSwitchNumberCheckBox.isSelected()) {
				
			}else {
				autoSwitchNumberCheckBox.click();
			}
		}else {
			if(autoSwitchNumberCheckBox.isSelected()) {
				autoSwitchNumberCheckBox.click();
			}
		}
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(closeProviderNamePopup)).click();
		wait.until(ExpectedConditions.visibilityOf(providerName));
	}
	
public boolean validateSuccessMessageDilaerToggle() {	
		if (validationMessage().contains("Updated Successfully")) {
			return true;
		} else {
			return false;
		}
	}
public boolean validateSuccessMessageForInvalidNumberInDilaer() {
	
	if (validationMessage().contains("The dialed number might be invalid")) {
		return true;
	} else {
		return false;
	}
}

	public boolean validateGlobalConnectToggleIsOn() {
		wait.until(ExpectedConditions.visibilityOf(globalConnectToggle));
		if (globalConnectToggle.isSelected()) {
			return true;
		} else {
			return false;
		}
	}
	public boolean validateAfterCallWorkToggleIsOn() {
		wait.until(ExpectedConditions.visibilityOf(afterCallWorkToggle));
		if (afterCallWorkToggle.isSelected()) {
			return true;
		} else {
			return false;
		}
	}

	public void clickOnForwardButton() {
		wait.until(ExpectedConditions.visibilityOf(forwardButton));
		forwardButton.click();
	}

	public boolean validateFDSearchBoxIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(fdUserSearchbox));
		if (fdUserSearchbox.isDisplayed()) {
			return true;
		} else {
			return false;
		}

	}

	public void enterUserNameInFDserchbox(String userName) {
		wait.until(ExpectedConditions.visibilityOf(fdUserSearchbox));
		fdUserSearchbox.sendKeys(userName);
	}

	public void clickOnFDuser() {
		wait.until(ExpectedConditions.visibilityOf(fdSearchedUser));
		fdSearchedUser.click();
	}

	public void clickOnTransferNowButton() {
		wait.until(ExpectedConditions.visibilityOf(fdTransferNowButton));
		fdTransferNowButton.click();
	}

	public void clickOnWarmTransferButton() {
		wait.until(ExpectedConditions.visibilityOf(fdWarmTransferButton));
		fdWarmTransferButton.click();
	}

	// <------------------------------- credit methods ------------------------>

	public String getCreditValueFromLeftPanel() {
		String creditTxt = left_panel_total_credit.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(".") + 3);
		System.out.println("++++++" + creditVal);
		return creditVal;
	}
	
	public void selectAddCreditDropdownValue(String creditValue) {
		wait.until(ExpectedConditions.visibilityOf(click_dialer_addCredit_dropdown));
		click_dialer_addCredit_dropdown.click();
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='ant-select-item-option-content'][text()='$"+creditValue+"']")))).click();
	}
	
	public void clickOnAddCreditButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addCredit_button)).click();
	}
	
	public void clickOnConfirmPopupYesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(yesButton)).click();;
	}
	public boolean validateOnConfirmPopupYesButtonIsDisplayed() {
		if (yesButton.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	public void clickOnConfirmPopupCancelButton() {
		wait.until(ExpectedConditions.elementToBeClickable(cancelButton)).click();;
	}
	public boolean validateOnConfirmPopupCancelButtonIsDisplayed() {
		if (cancelButton.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		System.out.println(validationMessage.getText());
		return validationMessage.getText();
	}
	public String ExtvalidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(ExtvalidationMessage));
		return ExtvalidationMessage.getText();
	}
	public void invisibleValidationMessage() {
		boolean toggle = true;
		while(toggle) {
			try {
				validationMessage.click();
			} catch (Exception e1) {
				break;
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}
		}
		
	}

// <---------------------------------------SMS module methods--------------------->

	public void clickOnComposeMessageButton() {
		wait.until(ExpectedConditions.visibilityOf(composeMassage)).click();
	}

	public void enterNumberInComposeMessageNumberAddressBar(String number) {
		wait.until(ExpectedConditions.visibilityOf(numberAdressTextbox)).sendKeys(number);
	}

	public void enterMessage(String message) {
		wait.until(ExpectedConditions.visibilityOf(messageTextarea)).sendKeys(message);
	}
	public boolean validateSendMessageButtonOnSMScomposePage() {
		if(sendMessage.isDisplayed()) {
			return true;
		}else {
			return false;
		}
	}
	public void clickOnSendMessageButtonOnDialpad() {
	
		wait.until(ExpectedConditions.elementToBeClickable(txtSendMessageOnDialpad)).click();
	}

	public void clickOnSendMessageButton() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.visibilityOf(sendMessage));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", sendMessage);
	}

	public void clickRecentNumberRightArrow(String number) {
		wait.until(ExpectedConditions.visibilityOf(driver
				.findElement(By.xpath("(//span[@test-automation='call_log_name_or_num'])[contains(@title,'" + number
						+ "')][1]/ancestor::span//following-sibling::a[@test-automation='call_log_detail_btn'][1]"))))
				.click();
	}
	
	public void clickOnBlackListButton() {
		wait.until(ExpectedConditions.elementToBeClickable(blackListButton)).click();
	}
	
	public String getBlackListConfirmMessage() {
		wait.until(ExpectedConditions.visibilityOf(blackListConfirmMessage));
		return blackListConfirmMessage.getText();
	}
	
	public void clickYesToConfirmBlackListNumber() {
		wait.until(ExpectedConditions.elementToBeClickable(clickYesBlackListPopup)).click();
	}
	
	public void clickUnBlockButton() {
		wait.until(ExpectedConditions.elementToBeClickable(unBlcokButton)).click();
	}
	
	public String getUnBlockConfirmMessage() {
		wait.until(ExpectedConditions.visibilityOf(unblockConfirmMessage));
		return unblockConfirmMessage.getText();
	}
	
	
	public void verifySdap() {
//		wait.until(ExpectedConditions.visibilityOf(sdap_icon));
	}
	
	public void clickCallBtnOnCallDetailsPage() {
		wait.until(ExpectedConditions.elementToBeClickable(calldetailPage_callbutton)).click();
	}
	
	public void clickOnLeftArrow() {
		wait.until(ExpectedConditions.elementToBeClickable(leftArrow)).click();
	}
	
	public void clickOnAllCallsNumber() {
		wait.until(ExpectedConditions.elementToBeClickable(allCallsNUmber)).click();
	}
	
	public void clickOndialDisableButton() {
		wait.until(ExpectedConditions.elementToBeClickable(dialDisableButton)).click();
	}
	
	public void clickOnCallDetailsReminderBtn() {
		wait.until(ExpectedConditions.elementToBeClickable(calldetailReminderBtn)).click();
	}
	
	public void selectValueForReminder() {
		Common.pause(5);
    	action.moveToElement(calldetailReminderBtn,10,60).click().build().perform();
	}
	
	public void clickOnCallPlannerLeftMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(callPlannerLeftMenu)).click();
	}
	
	public void deleteAllRemindersFromCallPlanner() {
		Common.pause(2);
		
		int inum = driver.findElements(By.xpath("//div[@class=\"reminderBtnWrapper\"]//*[local-name()='svg'][2]")).size();
		System.out.println(inum);
		if (inum > 0) {
			for (int i = 0; i < inum; i++) {
				driver.findElement(By.xpath("(//div[@class=\"reminderBtnWrapper\"])[1]//*[local-name()='svg'][2]")).click();
				Common.pause(3);
				clickOnConfirmPopupYesButton();
				Common.pause(7);
				}
				
			}
			Common.pause(5);
		}

	public void deleteAllSMS() {
		Common.pause(2);
		clickOnSideMenu();
		Common.pause(1);
		clickonsmspage();
		Common.pause(7);
		
		if(optionSMSPage.size()>0) {
			wait.until(ExpectedConditions.elementToBeClickable(optionSMSPage.get(0))).click();
		
			wait.until(ExpectedConditions.elementToBeClickable(optionSelectAllSMSPage)).click();	
			wait.until(ExpectedConditions.elementToBeClickable(optionSDeleteSMSPage)).click();	
			clickOnConfirmPopupYesButton();
			Common.pause(7);
			System.out.print("Delete all SMS");
		}
		}
	public void clickOnCallBtnOnCallReminderPage() {
		wait.until(ExpectedConditions.elementToBeClickable(callBtnOnCallReminderPage)).click();
	}
	
	public void clickOnRemainderBtnOnReminderPage(String time ) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(callReminderBtnReminderPage)).click();
		
		WebElement element = driver.findElement(By.xpath("//li[contains(.,'"+time+"')]"));
		js.executeScript("arguments[0].click();", element);
	}
	
	public String deleteReminderConfirmMessage() {
		wait.until(ExpectedConditions.visibilityOf(deleteReminderConfirmMessage));
		return deleteReminderConfirmMessage.getText();
	}
	
	public String noCallsInCallReminder() {
		wait.until(ExpectedConditions.visibilityOf(noCallsInCallReminder));
		return noCallsInCallReminder.getText();
	}
	
	public boolean ValidateNoCallsInCallReminder() {
		
			Common.pause(5);
			List<WebElement> elements = driver.findElements(
					By.xpath("//span[contains(@test-automation,'call_plan_schedule_call_name')]"));
			System.out.println("---elements.size()--" + elements.size());
			Common.pause(2);

			if (elements.size() <= 0) {
	               return true;
			}else {
		           return false;
	        }
		}
	
	public boolean callDetailBlackListIconNotVisibleold() {
		try {
			if (blackListButton.isDisplayed()) {
				System.out.println("In");
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean callDetailBlackListIconNotVisible() {
		Common.pause(4);
		if(driver.findElements(By.xpath("//div[contains(@style,'none')]/span/span[contains(.,'BLACKLIST')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean callDetailSMSIconNotVisible() {
		if (driver.findElements(By.xpath(
				"//span[@test-automation='call_detail_sms_btn'][contains(@style,'none')]//span[contains(.,'SMS')]"))
				.size()>0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void clickOncallDetailSMSIconButton() {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[@test-automation='call_detail_sms_btn']//span[contains(.,'SMS')]")))).click();
	}
	public void clickOncallDetailSMSButton() {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("(//span[@class='conDetailNumActionBtn'])[2]")))).click();
	}
	public void clickOnCallDetailPageAddButton() {
		wait.until(ExpectedConditions.elementToBeClickable(callDetailPageAddButton)).click();
	}
	
	public void clickOnCallDetailPageEditButton() {
		wait.until(ExpectedConditions.elementToBeClickable(callDetailPageEditButton)).click();
	}
	
	public void clickblockbtnOnCondetailspage() {
		wait.until(ExpectedConditions.visibilityOf(blockbtnOnCondtlpage));
		blockbtnOnCondtlpage.click();
	}
	
	public void enterUserNameInContacts(String userName) {
		wait.until(ExpectedConditions.visibilityOf(addContactName));
		addContactName.sendKeys(userName);
	}
public void clearNameInContact() {
		
		try {
			wait.until(ExpectedConditions.elementToBeClickable(addContactName));
			while (!addContactName.getAttribute("value").isEmpty()) {
				Common.pause(2);
				addContactName.sendKeys(Keys.BACK_SPACE);
			}
		} catch (Exception e) {
		}
	
	}
	public String enterNameInContacts(String userName) {
		wait.until(ExpectedConditions.visibilityOf(addContactName));
		addContactName.clear();
		addContactName.sendKeys(userName);
		return userName;
	}
	public boolean ValidateNameInEditContacts(String Name) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[contains(@placeholder,'Full Name')][contains(@value,'"+Name+"')]"))));
		return true;
		
	}
	public void AddContact(String userName) {
		wait.until(ExpectedConditions.visibilityOf(newContact));
		newContact.click();
		addContactName.sendKeys(userName);
		clickOnTickGreenBtnToAddContactName();
	}
	
	public void ClickonAddContactOnDialer() {
		wait.until(ExpectedConditions.visibilityOf(newContact));
		newContact.click();
	}
	
	public boolean ValidateNumberInNumberField(String number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[contains(@placeholder,'Phone Number')][contains(@value,'"+number+"')]"))));
		return true;
	}
	
	public boolean ValidateNumberInNumberFieldIsDisplayed(String number) {
		WebElement num= driver.findElement(By.xpath(""
				+ "//input[contains(@placeholder,'Phone Number')][contains(@value,'"+number+"')]"));
		if(num.isDisplayed()) {
		return true;
		}else {
			return false;
		}
	}
	
	public boolean ValidateNextNumberInNumberField(int indexof, String number) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//input[contains(@placeholder,'Phone Number')][@id='"+indexof+"'][contains(@value,'"+number+"')]"))));
		return true;
	}
	
	public String ValidateNameInNumberField(String name) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='sc-htpNat izJlxh'][contains(.,'"+name+"')]"))));
		return name;
	}
	
	public String getTwoLetterInIconofContact(String name) {
	String twoLetter= wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'contactnameleftpad')][contains(.,'"+name+"')]/parent::a/parent::div/preceding-sibling::div/child::div//div")))).getText();
		return twoLetter;
	}
	public String getTwoLetterFromContactName(String name) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[contains(@class,'contactnameleftpad')][contains(.,'"+name+"')]/parent::a/parent::div/preceding-sibling::div/child::div//div"))));	
		String firsttwoletter=name.substring(0,2).toUpperCase();
		System.out.println(firsttwoletter);
		return firsttwoletter;
	}
	
	
	
	
	public void clickOnSaveBtnToAddContactName() {
		wait.until(ExpectedConditions.elementToBeClickable(saveBtnToAddContactName)).click();
	}
	
	public void clickOnTickGreenBtnToAddContactName() {
		wait.until(ExpectedConditions.elementToBeClickable(tickGreenBtnToAddContactName)).click();
	
	}
	public void clickOnTickGreenBtnToAddContactName1() {
		wait.until(ExpectedConditions.elementToBeClickable(tickGreenBtnToAddContactName1)).click();
	}

	public void clickOnconDetailCallBtn() {
		wait.until(ExpectedConditions.elementToBeClickable(conDetailCallBtn)).click();
	}
	public void clickOnconDetailCallBtn1() {
		wait.until(ExpectedConditions.elementToBeClickable(conDetailCallBtn1)).click();
	}
	
	public String getContactDeleteConfirmMessage() {
		wait.until(ExpectedConditions.visibilityOf(contactDeleteConfirmMessage));
		return contactDeleteConfirmMessage.getText();
	}
	
	public void clickOnAccountsLeftMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(accountsLeftMenu)).click();
	}
	public void clickoncontactpage() {
		wait.until(ExpectedConditions.elementToBeClickable(opencontactpage)).click();
	}
	public void clickonsmspage() {
		wait.until(ExpectedConditions.elementToBeClickable(openSMSPage)).click();
	}
	
	public boolean validateContactFromContactList(String contactName) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'"+contactName+"')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@FindBy (xpath = "(//span[contains(@class,'t__ellipsis d__block lognum')])[1]")
	WebElement contac1;

	public void deleteAllContactsFromDialer() {
		Common.pause(2);
		clickOnSideMenu();
		Common.pause(1);
		clickoncontactpage();
		Common.pause(10);
		
		boolean isElementDisplayed=true;
		try {
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//div[@class[contains(.,'contact')]]//ul/li[1]"))));
			isElementDisplayed=true;
		} catch (Exception e) {
			isElementDisplayed=false;
		}
		

		

		if (isElementDisplayed==true) {
			List<WebElement> elementList = driver
					.findElements(By.xpath("//div[@class[contains(.,'contactnameleftpad')]]/span"));
			for (int i = 0; i <= elementList.size(); i++) {
				
				List<WebElement> newElementList = driver
						.findElements(By.xpath("//div[@class[contains(.,'contactnameleftpad')]]/span"));
				if (newElementList.size() > 0) {
					newElementList.get(0).click();
					Common.pause(1);
					contactDeleteIcon.click();
					assertEquals(getContactDeleteConfirmMessage(), "Are you sure you want to delete this contact ?");
					clickYesToConfirmBlackListNumber();
					assertEquals(validationMessage(), "Contact has been deleted Successfully");
					Common.pause(3);
				}
			}
			Common.pause(1);
		}
	}

	public boolean callReminderPopupIsDisplayed() {
		try {
			wait2.until(ExpectedConditions.visibilityOf(callReminderDropDown));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void clickOnCallReminderDropdown() {
		wait.until(ExpectedConditions.visibilityOf(callReminderDropDown)).click();
	}
	
	public void clickOnCallReminderCancelButton() {
		try {
		wait.until(ExpectedConditions.visibilityOf(callReminderCancel)).click();
		}catch (Exception e) {
		}
	}
	
	public boolean validateCallReminderPopupIsDisplayed() {
		
		try {
			wait2.until(ExpectedConditions.visibilityOf(callReminderDropDown));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void setCallReminder(String time) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'"+time+"')]")))).click();
	}
	
	public boolean validateCallReminderDropdownOptions(String time) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'"+time+"')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public String GetDateAndTimeOfRemainder() {
		String DateAndTime = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@test-automation='call_plan_schedule_call_date_time']")))).getText();
		return DateAndTime;
	}
	
	public void GetsuccessMsgforResceduleremainder() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'Reminder updated successfully')]")))).click();
		//return DateAndTime;
	}
	
	
	/*********** ACW   ******************/
	
	public boolean validateDialerACWDurationVisible() {
		wait.until(ExpectedConditions.visibilityOf(dialer_acw_duration));
		if (dialer_acw_duration.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	public boolean validateAfterCallWorkTitle() {
		wait.until(ExpectedConditions.visibilityOf(dialer_Acw_Screen_Title));
		if (dialer_Acw_Screen_Title.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	public boolean validateACWNoteVisible() {
		if (driver
				.findElements(By.xpath(
						"//div[@class[contains(.,'notaddicondiv')]]//following-sibling::div[contains(.,'Add Note')]"))
				.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validateFieldsOnEndACWPage(String number) {
		wait.until(ExpectedConditions.visibilityOf(dialer_acw_duration));
		if (dialer_acw_duration.isDisplayed()
				//&& acw_add_note.isDisplayed()
				&& acw_phone_icon.isDisplayed()
				&& acw_phone_number.getText().equalsIgnoreCase(number)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void insertCallNotes(String callNotes) {
		dialer_callNotes.sendKeys(callNotes);
	}
	public void insertTAG(String callNotes) {
		dialer_callNotes.sendKeys(callNotes);
	}
	
	
	public void insertACWNote(String acwNote) {
		acw_textarea.sendKeys(acwNote);
	}
	
	public void clickEndACWButton() {
		wait.until(ExpectedConditions.visibilityOf(acw_endAfterWorkBtn)).click();
	}
	
	public String validateFirstEntryOfCallReminder() {
		return wait.until(ExpectedConditions.visibilityOf(firstEntryofCallreminder)).getText();
	}
	
	public String validateSecondEntryOfCallReminder() {
		return wait.until(ExpectedConditions.visibilityOf(secondEntryofCallreminder)).getText();
	}
	
	public String validateThirdEntryOfCallReminder() {
		return wait.until(ExpectedConditions.visibilityOf(thirdEntryofCallreminder)).getText();
	}
	
	public String validateSavedNameFirstEntryOfCallReminder() {
		return wait.until(ExpectedConditions.visibilityOf(SavedFirstEntryofCallreminder)).getText();
	}
	
	public String validateSavedContactSecondEntryOfCallReminder() {
		return wait.until(ExpectedConditions.visibilityOf(SavedSecondEntryofCallreminder)).getText();
	}
	
	public String validateSavedContactThirdEntryOfCallReminder() {
		return wait.until(ExpectedConditions.visibilityOf(SavedThirdEntryofCallreminder)).getText();
	}
	
	
	public void waitforFirstEntryOfCallReminder() {
		 
		try {
			wait2.until(ExpectedConditions.visibilityOf(firstEntryofCallreminder));
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(noCallsInCallReminder));
		}

	}
	
	public void clickOnCompltedOnCallPlanner() {
		wait.until(ExpectedConditions.visibilityOf(completedOn)).click();
	}
	
	public String validateFirstEntryOfCompletedOnOfCallPlanner() {
		
		return wait.until(ExpectedConditions.visibilityOf(firstEntryOfCompletedOn)).getText();
		
	} 
	
	public String validateBlockCountryError() {
		wait.until(ExpectedConditions.visibilityOf(BlockCountryErrorMessage));
		return BlockCountryErrorMessage.getText();
	}

	public boolean muteButtonIsDisabled() {
		if(driver.findElements(By.xpath("//div[@test-automation='call_screen_mute_btn'][contains(@class,'div_disabled')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	
	public boolean holdButtonIsDisabled() {
		if(driver.findElements(By.xpath("//div[@test-automation='call_screen_hold_btn'][contains(@class,'div_disabled')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean dialpadButtonIsDisabled() {
		if(driver.findElements(By.xpath("//*[@test-automation='call_screen_dialpad_btn'][contains(@class,'div_disabled')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean recordingButtonIsDisabled() {
		if(driver.findElements(By.xpath("//*[@test-automation='call_screen_mute_btn']/parent::div[contains(@class,'div_disabled')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isRecordingButtonDisplay() {
		return driver.findElement(By.xpath("//label[@test-automation='call_screen_mute_btn']")).isDisplayed();
	}
	
	public boolean isMuteButtonDisplay() {
		return driver.findElement(By.xpath("//div[@test-automation='call_screen_mute_btn']")).isDisplayed();
	}
	
	public void selectTagFromDropdown(String tagname) {
		WebElement selectTag = driver.findElement(By.xpath(""
				+ "//span[@class='rdw-suggestion-dropdown']/span[contains(.,'"+tagname+"')]"));
		 Actions action = new Actions(driver);
			action.click(selectTag).build().perform();
	}
	
	public boolean tagIsDisplaying(String tagname) {
		try {
		driver.findElement(By.xpath(""
				+ "//span[@class='rdw-suggestion-dropdown']/span[contains(.,'"+tagname+"')]")).isDisplayed();
		return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean tagiconIsDisplaying() {
		return driver.findElement(By.xpath(""
				+ "(//span[@class='material-icons inboxmatic'][contains(.,'local_offer')])[2]")).isDisplayed();
	}
	
	public boolean validateTagLogIsInBoldFormate() {

		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//span[@test-automation='inbox_call_log_name_or_num'][1]"))));

		String attributeName = driver
				.findElement(By.xpath("(//span[@test-automation='inbox_call_log_name_or_num'][1])/..//span"))
				.getAttribute("class");

	
			if (attributeName.contains("unreadmsgtitle")) {
				return true;

			} else {
				return false;
			}

			

	}
	
	
	public boolean isHoldButtonDisplay() {
		return driver.findElement(By.xpath("//div[@test-automation='call_screen_hold_btn']")).isDisplayed();
	}
	public boolean isForwardButtonDisplay() {
		return driver.findElement(By.xpath("//label[@for='dial_sub_user_input '][contains(.,'forwardForward')]")).isDisplayed();
	}
	
	public boolean isDialPadButtonDisplay() {
		return driver.findElement(By.xpath("//label[contains(@test-automation,'call_screen_dialpad_btn')]")).isDisplayed();
	}
	
	public boolean isNoteButtonDisplay() {
		return driver.findElement(By.xpath("//label[@test-automation='call_screen_note_btn'][contains(.,'note_addNote')]")).isDisplayed();
	}
	
	public boolean noteButtonIsDisabled() {
		if(driver.findElements(By.xpath("//*[@test-automation='call_screen_note_btn'][contains(@class,'div_disabled')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean forwardButtonIsDisabled() {
		if(driver.findElements(By.xpath("//*[@for='dial_sub_user_input '][contains(@class,'div_disabled')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean ExtOutgoingScreenRecordingButtonIsDisabled() {
		if(driver.findElements(By.xpath("//label[contains(@disabled,'')]/span[contains(.,'RECORDING')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean ExtOutgoingScreenForwardButtonIsDisabled() {
		if(driver.findElements(By.xpath("//label[contains(@class,'div_disabled')]//i[contains(.,'forward')]")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	

	public void clickOnUserstatus() {
		wait.until(ExpectedConditions.visibilityOf(LiveCall));
		LiveCall.click();
		wait.until(ExpectedConditions.visibilityOf(Userstatustab));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", Userstatustab);
		Common.pause(1);
	}
	
	public void clickOnLivecalls() {
		wait.until(ExpectedConditions.visibilityOf(LiveCall));
		LiveCall.click();
		wait.until(ExpectedConditions.visibilityOf(livecalls));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", livecalls);
		Common.pause(1);
	}
	
	public void clickonfiltericon() {
		wait.until(ExpectedConditions.visibilityOf(filtericon)).click();
   }
	public boolean validatefiltericon() {
		try {
		wait.until(ExpectedConditions.visibilityOf(filtericon));
		return true;
		}catch (Exception e) {
		return false;
		}
  }
	
	public void SelectUserOrTeam(String userorteam) {
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//div[@class='ant-select-item-option-content'][contains(.,'"+userorteam+"')]"))))
				.click();

  }
	
	public boolean validateuserorteam(String userorteam) {
		try{
			wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath("//div[@class='ant-select-item-option-content'][contains(.,'"+userorteam+"')]"))));
		    return true;
		}catch (Exception e) {
			return false;
  }		

  }
	
	public void enterUserOrTeam(String userorteam) {
		wait.until(ExpectedConditions.visibilityOf(searchUsersTeams)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(clearsearchfiels)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(searchUsersTeams)).clear();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(searchUsersTeams)).sendKeys(userorteam);
		Common.pause(2);
	}
	
	public void statusofcallinDialer(String status) {
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//div[@class='oncall-text'][contains(.,'"+status+"')]"))))
				.click();
	}
	
//--->User Availability>----

	public boolean validateUserAvailabilityToggleIsOn() {
		wait.until(ExpectedConditions.visibilityOf(userAvailabilityToggle));
		if (userAvailabilityToggle.isSelected()) {
			return true;
		} else {
			return false;
		}
	}
	
	public void clickUserAvailabilityToggle(String value) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(userAvailabilityToggle));

		if (value.equals("on")) {

			if (userAvailabilityToggle.isSelected()) {

			} else {

				clickUserAvailabilityToggle.click();
			}

		} else {
			if (userAvailabilityToggle.isSelected()) {

				clickUserAvailabilityToggle.click();
			}else {
		}
	}
	
	}
	

	public void clickOnNumberInAllcalls() {
		wait.until(ExpectedConditions.visibilityOf(allCallsNUmber)).click();
	}
	public void clickoncalllogfilterdrpdown() {
		 wait.until(ExpectedConditions.visibilityOf(calllogfilterdrpdown)).click();
	}
	
	public void clickOnSpecificdropdown(String filterName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//li[contains(@class,'ant-dropdown-menu')][contains(.,'"+filterName+"')]")))).click();
		
		
	}
	
	
	
	public String filterTagsisDiaplaying(String filterName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'ant-dropdown-menu-submenu-title')][contains(.,'"+filterName+"')]"))));
		return filterName;
		
	}
	
	public void selectFilter(String tagname) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
					+ "(//div[contains(@class,'ant-dropdown-menu-light ant-dropdown-menu-submenu-placement-rightTop ')])/child::ul/child::li[contains(.,'"+tagname+"')]")))).click();
	}
	
	
	public void validateTagIsPresentInACWScreen(String tagname) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='addtagwrapper']/child::div/child::div[@class='ant-select-selector'][contains(.,'"+tagname+"')]")))).click();
	
}
	
	public void selectDeselectSpecificTagFromACWScreen(String tagname,boolean trueOrFalse ) {
		try {
		Common.pause(5);
		WebElement dropdown =  wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='addtagwrapper']/child::div/child::span//span[contains(@aria-label,'down')]/../preceding-sibling::div[@class='ant-select-selector'][contains(.,'')]"))));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='rc-virtual-list']//div[@class='rc-virtual-list-holder-inner']/div[@label='"+tagname+"'][@aria-selected='"+trueOrFalse+"']")))).click();
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
					+ "//div[@class='rc-virtual-list']//div[@class='rc-virtual-list-holder-inner']/div[@label='"+tagname+"'][@aria-selected='"+trueOrFalse+"']")))).click();
		}
}
	
	public boolean ValidateTagsonACWscreenAndSelected(String tagname,boolean trueOrFalse) {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='addtagwrapper']/child::div/child::span//span[contains(@aria-label,'down')]/../preceding-sibling::div[@class='ant-select-selector'][contains(.,'')]")))).click();
		
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='rc-virtual-list']//div[@class='rc-virtual-list-holder-inner']/div[@label='"+tagname+"'][@aria-selected='"+trueOrFalse+"']"))));
		return true;
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//div[@class='rc-virtual-list']//div[@class='rc-virtual-list-holder-inner']/div[@label='"+tagname+"'][@aria-selected='"+trueOrFalse+"']"))));
		}
		return true;
	}
	
	public void closeDropDown() {

		Common.pause(1);
		action.sendKeys(Keys.ESCAPE).build().perform();
		Common.pause(1);
	}
	
	
}
