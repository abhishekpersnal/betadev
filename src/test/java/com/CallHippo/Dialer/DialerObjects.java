package com.CallHippo.Dialer;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DialerObjects {

	@FindBy(xpath = "//input[@name='email']")
	WebElement email;

	@FindBy(xpath = "//input[@name='password']")
	WebElement password;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Sign In')]")
	WebElement login;
	@FindBy(xpath = "//span[contains(.,'New Contact')]")
	WebElement newContact;
	@FindBy(xpath = "//*[@id=\"backSpace\"]")
	WebElement backBtn;
	@FindBy(xpath = "//p[@class='t__ellipsis m__zero_auto mw__90 weightbold mbottomzero']")
	WebElement dialUserName;
	@FindBy(xpath = "//p[@class='t__ellipsis m__zero_auto mw__90 psmlfnt text-muted']")
	WebElement dialEmail;
	@FindBy(xpath = "//span[@class='flag_icndesign flgdrp']//span")
	WebElement dialFlagName;
	@FindBy(xpath = "(//div[@class='crt_contact_div cursor'])[1]")
	WebElement txtNewContact;
	@FindBy(xpath = "//a[@href='/contact/add']//following::div[@class='crt_contact_div cursor']")
	WebElement txtSendMessage;
	@FindBy(xpath = "//span[@class='fnweightmidum blckttclr'][contains(.,'Send Message')]")
	WebElement txtSendMessageOnDialpad;
	
	
	@FindBy(xpath = "//span[contains(@class,'srchconnamefix')]")
	WebElement searchContactName;
	@FindBy(xpath = "//p[contains(@class,'serchconnumber')]")
	WebElement searchContactNumber;
	
	@FindBy(id = "searchContact")
	WebElement searchContact;
	@FindBy(xpath = "(//div[contains(@class,'disabled')])[1]/span")
	WebElement txtForNoContact;
	
	@FindBy(xpath = "//div[contains(@class,'lgoutnweter cursor')]")
	WebElement logout;
	@FindBy(id = "nameInput")
	WebElement numberTextbox;
	@FindBy(xpath = "//div[@class='callbtn_ndiv callbtn_green cursor'][@title='Dial']")
	WebElement dialButton;
	@FindBy(xpath = "//div[@class='callbtn_ndiv disblnwchqbtn cursor'][@title='Dial']")
	WebElement redialButton;
	@FindBy(xpath = "//span[@class='contdetalnmallfont'][contains(.,'Call')]")
	WebElement dialCallDtlPage;
	@FindBy(xpath = "//a[contains(@class,'callendndesbtn tglmm')]")
	WebElement oHangupbtn;
	@FindBy(xpath = "//div[@class='callendndesbtn  rmvunderline tglmm']")
	WebElement iRejectbtn;
	@FindBy(xpath = "//span[@test-automation='call_screen_duration']")
	WebElement callDurationOnCallingScreen;
	@FindBy(xpath = "//div[contains(@class,'callbtn_green rmvunderline tglmm')]")
	WebElement iAcceptCall;
	@FindBy(xpath = "//div[@class='pwbtnstl cursor'][contains(.,'Session')]")
	WebElement sessionbutton;
	@FindBy(xpath = "//p[contains(.,'Calls Completed')]")
    WebElement CallCompleted;
	@FindBy(xpath = "//div[@class='waves-effect waves-light btn endafterworkbtn chflex chflexcenter']")
	WebElement endACW;
	@FindBy(xpath = "//div[@class='text-left numselection_newdiv topshadow_ndesing']") WebElement departmentDropdown;
	@FindBy(xpath = "//span[@class='t__ellipsis mw__70 numselectext_ndesing lognum dialProviderName']")
	WebElement homepagerender;
	@FindBy(xpath = "//span[contains(@class,'dialProviderName')]")
	WebElement providerName;
	@FindBy(xpath = "//input[@id='switch_checkbox']")
	WebElement autoSwitchNumberCheckBox;
	@FindBy(xpath = "//button[@type='button'][@class='close']")
	WebElement closeProviderNamePopup;
	@FindBy(xpath = "//p[normalize-space()='Default Number']/../../../../following-sibling::div/div/div")
	WebElement defaultNumberPopup;
	@FindBy(xpath = "//span[@test-automation='call_screen_outgoing_via']")
	WebElement oCallingScreenOutgoingVia;
	@FindBy(xpath = "//*[@test-automation='call_screen_outgoing_number']")
	WebElement oCallingScreenOutgoingNumber;
	@FindBy(xpath = "//*[@test-automation='call_screen_outgoing_number']")
	WebElement subUserOutgoingNumberCallingScreen;
	@FindBy(xpath = "//span[@test-automation='call_screen_incoming_number']")
	WebElement subUserIncomingNumberCallingScreen;
	@FindBy(xpath = "//h3[@test-automation='call_screen_outgoing_name']/span")
	WebElement nameOnOutgoingCallingScreen;
	@FindBy(xpath = "//h3[@test-automation='call_screen_incoming_name']/span")
	WebElement nameOnIncomingCallingScreen;
	@FindBy(xpath = "//span[@test-automation='call_screen_incoming_via']")
	WebElement oCallingScreenIncomingVia;
	@FindBy(xpath = "//h3[@test-automation='call_screen_incoming_name']")
	WebElement oCallingScreenIncomingNumber;
	@FindBy(xpath = "//h3[@test-automation='call_screen_outgoing_name']")
	WebElement OutgoingNameCallingScreen;

	@FindBy(id = "btnDraw")
	WebElement menuSlider;
	@FindBy(xpath = "//span[contains(@class,'ndesing lognum dialProviderName')]")
	WebElement departmentNameInDialpade;
	
	@FindBy(xpath = "//div[@class='usernicon_ch']//following-sibling::a/p[1]")
	WebElement callerName;

	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Dialpad')]")
	WebElement sideMenuDialPad;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'All calls')]")
	WebElement allCalls;
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Call Logs')]") WebElement callLogs;
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Inbox')]") WebElement inboxLogs;
	@FindBy(xpath = "(//span[@test-automation='call_log_via_num'])[1]")
	WebElement allCallsVia;
	
	@FindBy(xpath = "(//span[@test-automation='call_log_via_num'])[2]")
	WebElement allCallsViaFor2ndEntry;
	
	
	@FindBy(xpath = "(//span[@test-automation='call_log_name_or_num'])[1]")
	WebElement allCallsNUmber;
	
	@FindBy(xpath = "(//span[@test-automation='inbox_call_log_name_or_num'])[1]")
	WebElement inboxNumber;
	
	@FindBy(xpath = "//div[@class='calloogtagndiv']//span[@class='ant-tag']") WebElement dialerTagName;
	
	@FindBy(xpath = "(//span[@test-automation='call_log_name_or_num'])[2]")
	WebElement allCallsNUmberFor2ndEntry;

	@FindBy(xpath = "(//span[@test-automation='call_log_call_type'])[1]")
	WebElement allCallsCallType;
	
	@FindBy(xpath = "(//span[@test-automation='call_log_call_type'])[2]")
	WebElement allCallsCallTypeFor2ndentry;
	
	@FindBy(xpath = "(//a[@test-automation='call_log_detail_btn'])[1]")
	WebElement rightArrow;
	
	@FindBy(xpath = "(//a[contains(@test-automation,'inbox_call_log_detail_btn')])[1]")
	WebElement rightArrowOfInboxPage;
	
	@FindBy(xpath = "(//a[@test-automation='call_log_detail_btn'])[2]")
	WebElement rightArrowFor2ndEntry;

	@FindBy(xpath = "(//li[@class[contains(.,'backnewbtn')]])[1]")
	WebElement callDetailBackArrow;
	
	@FindBy(xpath = "//span[contains(@class,'navtittle_span')]")
	WebElement NameatSMSthreadpage;
	
	@FindBy(xpath = "//span[contains(@class,'navsubtittle_span')]")
	WebElement NumberatSMSthreadpage;
	
	
	@FindBy(xpath = "//div[contains(@class,'smscontact_bgdesign')]")
	WebElement contactIconatSMSthreadpage;
	@FindBy(xpath = "//span[@class='t__ellipsis d__block mw__90 smsContactName onlyName']")
	WebElement contactNameatSMSthreadpage;
	@FindBy(xpath = "//span[contains(@class,'anticon-right')]")
	WebElement rightaerrowatSMSthreadpage;
	@FindBy(xpath = "(//span[contains(@class,'EmailCompanySpan')])[1]")
	WebElement companyname;
	@FindBy(xpath = "(//span[contains(@class,'EmailCompanySpan')])[2]")
	WebElement emailID;
	
	@FindBy(xpath = "//div[contains(@class,'smscontact_bgdesign')]")
	WebElement greentikSMSthreadpage;
	
	
	@FindBy(xpath = "//span[contains(@class,'block smsThreadName')]")
	WebElement NameatSMSpage;
	
	@FindBy(xpath = "//div[contains(@class,'contactnameleftpad')]/span")
	WebElement NameOncontactpage;
	
	@FindBy(xpath = "//span[contains(@class,'conDetailNumSpan')]")
	WebElement NumberOncontactpage;
	
	@FindBy(xpath = "//h3[contains(@class,'conddtname')]")
	WebElement NameOncontactDtlpage;
	
	@FindBy(xpath = "//span[@class='t__ellipsis d__block lognum']")
	WebElement nameOncontactdisplaypage;
	
	@FindBy(xpath = "//div[contains(@class,'searchic')]")
	WebElement searchIconOncontactdisplaypage;
	@FindBy(id = "hideserachdiv")
	WebElement closeIconOncontactdisplaypage;
	
	@FindBy(xpath = "//span[contains(@class,'t__ellipsis d__block navtittle_span')][contains(.,'Contacts')]")
	WebElement titleOncontactdisplaypage;
	
	@FindBy(xpath = "//div[@class='callbtn_design']")
	WebElement btnAddContact;
	
	
	
	
	
	
	
	
	// Call detail page
	@FindBy(xpath = "//*[@test-automation='call_detail_num'][2]")
	WebElement AddNumberinCallDetail;
	@FindBy(xpath = "//*[@test-automation='call_detail_num']")
	WebElement numberinCallDetail;
	@FindBy(xpath = "//p[@test-automation='call_detail_num']")
	WebElement ExtnumberinCallDetail;
	@FindBy(xpath = "//*[contains(@test-automation,'call_detail_name')]")
	WebElement nameinCallDetail;
	@FindBy(xpath = "//div[@test-automation='call_detail_status']")
	WebElement callStatus;
	@FindBy(xpath = "//span[@class='t__ellipsis']")
	WebElement departmentNameInCallDetail;
	@FindBy(xpath = "//div[@class='calltimenewsw']")
	WebElement callDurationInCallDetail;
	@FindBy(xpath = "//html[@class='nprogress-busy']")
	WebElement progressbar;
	@FindBy(xpath = "(//button[@aria-label='Play'])[1]")
	WebElement recordingPlayBtn;

	@FindBy(xpath = "(//div[contains(@aria-label,'Audio Progress Control')])[1]")
	WebElement audioProgressBar;
	
	@FindBy(xpath = "(//div[contains(@id,'rhap_current-time')])[1]")
	WebElement audioRecordingDuration;
	
	// ongoing calling screen
	@FindBy(xpath = "//div[@class='firstoptiondiv div_disabled']")
	WebElement disableMuteHoldForward;
	@FindBy(xpath = "//div[@test-automation='call_screen_mute_btn']")
	WebElement muteButton;
	@FindBy(xpath = "//div[@test-automation='call_screen_hold_btn']")
	WebElement holdButton;
	@FindBy(xpath = "//label[@test-automation='call_screen_forward_btn']")
	WebElement forwardButton;
	@FindBy(xpath = "//label[@test-automation='call_screen_note_btn'][@class='clropgrey optionindivwid cursor false div_disabled']")
	WebElement noteButton;
	@FindBy(xpath = "//label[@test-automation='call_screen_dialpad_btn'][@class='clropgrey optionindivwid cursor false div_disabled']")
	WebElement dialpadButton;

	@FindBy(xpath = "//label[@test-automation='call_screen_dialpad_btn']")
	WebElement dialpadButtonEnabled;

	@FindBy(xpath = "//input[@id='nameDialpadInput']")
	WebElement pressKey1;

	@FindBy(xpath = "//span[@test-automation='last_call_popup_status']")
	WebElement lastCallDetailStatus;
	@FindBy(xpath = "//div[@test-automation='last_call_popup_incoming_icon']")
	WebElement lastCallDetailIncomingIcon;
	@FindBy(xpath = "//div[@test-automation='last_call_popup_outgoing_icon']")
	WebElement lastCallDetailOutgoingIcon;
	@FindBy(xpath = "//span[@test-automation='last_call_popup_name']")
	WebElement lastCallDetailUserName;
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Settings')]")
	WebElement setting;
	@FindBy(xpath = "//input[@test-automation='setting_last_call_details_switch']")
	WebElement lastCallDetailToggle;
	@FindBy(xpath = "//input[@test-automation='setting_call_reminder_switch']")
	WebElement callReminderToggle;

	@FindBy(xpath = "//input[@test-automation='setting_last_call_details_switch']/parent::div")
	WebElement clickablelastCallDetailToggle;
	@FindBy(xpath = "//input[@test-automation='setting_call_reminder_switch']/parent::div")
	WebElement clickableCallReminderToggle;
	
	@FindBy(xpath = "//input[@test-automation='setting_after_call_work_switch']")
	WebElement afterCallWorkToggle;
	
	@FindBy(xpath = "//input[@test-automation='setting_global_connect_switch']")
	WebElement globalConnectToggle;
	
	@FindBy(xpath = "//input[@test-automation='setting_user_availability_switch']")
	WebElement userAvailabilityToggle;
	
	@FindBy(xpath = "//input[@test-automation='setting_user_availability_switch']/parent::div")
	WebElement clickUserAvailabilityToggle;
	
	@FindBy(xpath = "//input[@test-automation='setting_after_call_work_switch']/parent::div")
	WebElement clickAfterCallWorkToggle;
	
	@FindBy(xpath = "//input[@test-automation='setting_global_connect_switch']/parent::div")
	WebElement clickGlobalConnectToggle;
	
	@FindBy(xpath = "//div[@class='timezoneDiv_ndesing']")
	WebElement timeZone;
	
	@FindBy(xpath = "//span[@class='timezonelabelspan_ndesign']")
	WebElement dateTimeZone;

	@FindBy(xpath = "//input[@test-automation='forward_popup_sub_user_input']")
	WebElement fdUserSearchbox;
	@FindBy(xpath = "//p[@test-automation='forward_popup_sub_user_name']")
	WebElement fdSearchedUser;
	@FindBy(xpath = "//div[@test-automation='forward_popup_sub_user_online']")
	WebElement fdSearchedUserStatus;
	@FindBy(xpath = "//div[contains(@test-automation,'transfer_popup_transfer_now_btn')]")
	WebElement fdTransferNowButton;
	@FindBy(xpath = "//div[contains(@test-automation,'transfer_popup_transfer_now_btn')]")
	WebElement fdWarmTransferButton;

	@FindBy(xpath = "//div[@class='totalcredit ng-binding'][contains(.,'Credit :')]")
	WebElement left_panel_total_credit;

	@FindBy(xpath = "//div[@class='callbtn_design']")
	WebElement composeMassage;
	@FindBy(xpath = "//input[@placeholder='Type Number with country code']")
	WebElement numberAdressTextbox;
	@FindBy(xpath = "//textarea[@id='newMessage']")
	WebElement messageTextarea;
	@FindBy(xpath = "//i[@title='Send']")
	WebElement sendMessage;
	@FindBy(xpath = "//div[@class='smscompseldep_flag']")
	List<WebElement>button_departmentlist;

	@FindBy(xpath = "//div[@class[contains(.,'browser-default crditselectop')]]")
	WebElement click_dialer_addCredit_dropdown;
	
	@FindBy(xpath = "//h3[@class='creditdisdispl']")
	WebElement dialer_setting_available_credit;
	
	@FindBy(xpath = "//a[contains(.,'Add Credits')]")
	WebElement addCredit_button;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'YES')]")
	WebElement yesButton;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'CANCEL')]")
	WebElement cancelButton;
	
	@FindBy(xpath = "//div[@class='toast-body']/span")
	WebElement validationMessage;
	
	@FindBy(xpath = "(//div[@class='toast-body']/span)[2]")
	WebElement ExtvalidationMessage;
	
	@FindBy(xpath = "//p[contains(.,'Are you sure you want to  add this number to blacklist ?')]")
	WebElement blackListConfirmMessage;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'YES')]")
	WebElement clickYesBlackListPopup;
	
	@FindBy(xpath = "(//span[contains(.,'BLACKLIST')])[1]")
	WebElement blackListButton;
	
	@FindBy(xpath = "//span[contains(.,'block')]/parent::span[contains(.,'block')]")
	WebElement invisible_blackListBtn;
	
	@FindBy(xpath = "//div[@class[contains(.,'reminderSetWrapper')]]/span[contains(.,'Call')]")
	WebElement calldetailPage_callbutton;
	
	@FindBy(xpath = "(//span[contains(.,'UNBLOCK')])[1]")
	WebElement unBlcokButton;
	
	@FindBy(xpath = "//p[contains(.,'Are you sure you want to  remove this number from blacklist ?')]")
	WebElement unblockConfirmMessage;
	
	@FindBy(xpath = "//img[@src='./static/media/sdap.e50074ff.svg']")
	WebElement sdap_icon;
	
	@FindBy(xpath = "//li[@class[contains(.,'backnewbtnli')]]")
	WebElement leftArrow;
	
	@FindBy(xpath = "//div[@title='Dial'][@class[contains(.,'dis')]]")
	WebElement dialDisableButton;
	
	@FindBy(xpath = "//span[@class='contdetalnmallfont'][contains(.,'Reminder')]")
	WebElement calldetailReminderBtn;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Call Planner')]")
	WebElement callPlannerLeftMenu;
	
	@FindBy(xpath = "(//div[@class='reminderBtnWrapper'])[1]//*[local-name()='svg'][@class='MuiSvgIcon-root reminderBtnSvg reminderCallBtn'][1]")
	WebElement callBtnOnCallReminderPage;
	
	@FindBy(xpath = "(//div[@class='reminderBtnWrapper'])[1]//*[local-name()='svg'][@class='MuiSvgIcon-root reminderBtnSvg reminderCallBtn cursor']")
	WebElement callReminderBtnReminderPage;
	
	
	@FindBy(xpath = "//p[contains(.,'Are you sure you want to delete this call reminder ?')]")
	WebElement deleteReminderConfirmMessage;
	
	@FindBy(xpath = "//div[contains(@style,'display: block')]//span[contains(.,'You have no scheduled calls')]")
	WebElement noCallsInCallReminder;
	
	@FindBy(xpath = "//span[contains(.,'Add')]")
	WebElement callDetailPageAddButton;
	
	@FindBy(xpath = "//span[contains(.,'Edit')]")
	WebElement callDetailPageEditButton;
	
	@FindBy(xpath = "//input[contains(@id,'contactName')]")
	WebElement addContactName;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Phone Number')]")
	WebElement EnterNumber;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Company')]")
	WebElement EnterCompanyName;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Email')]")
	WebElement EnterEmailID;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Full Name')]/preceding-sibling::span[contains(.,'*')]")
	WebElement FullnamerequiredIcon;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Phone')]/preceding-sibling::span[contains(.,'*')]")
	WebElement phonenumberequiredIcon;
	
	@FindBy(xpath = "//input[contains(@placeholder,'company')]/preceding-sibling::span[contains(.,'*')]")
	WebElement CompanyNamerequiredIcon;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Email')]/preceding-sibling::span[contains(.,'*')]")
	WebElement EmailrequiredIcon;
	
	@FindBy(xpath = "//span[contains(@class,'editcnt_chnew_ic')]")
	WebElement Editcontactpagehumanicon;
	
	@FindBy(xpath = "//*[local-name()='svg'][contains(@class,'_contact_add_disabled')]")
	WebElement SaveBtnDisable;
	
	@FindBy(xpath = "//*[local-name()='svg'][contains(@class,'MuiSvgIcon-root cursor')]")
	WebElement saveBtnEnable;
	
	@FindBy(xpath = "//li[contains(@class,'backnewbtnli_li cursor')]")
	WebElement EditcontactpageBackbutton;
	
	@FindBy(xpath = "//span[@class='contdetalnmallfont'][contains(.,'Edit')]")
	WebElement EditbtnOnAllCalldtlpage;
	
	@FindBy(xpath = "//span[@class='contdetalnmallfont'][contains(.,'SMS')]")
	WebElement SMSbtnOnAllCalldtlpage;
	
	@FindBy(xpath = "//i[@class[contains(.,'contact_btn_add false')]]")
	WebElement saveBtnToAddContactName;
	
	@FindBy(xpath = "//div[@class[contains(.,'tickgreenrtl')]]")
	WebElement tickGreenBtnToAddContactName;
	
	@FindBy(xpath = "//div[@class[contains(.,'savechnewsd')]]")
	WebElement tickGreenBtnDisableToAddContactName;
	
	@FindBy(xpath = "//div[@class='savechnewsd cursor fleft tickgreenrtl']")
	WebElement tickGreenBtnToAddContactName1;
	
	@FindBy(xpath = "//span[@class='conDetailNumActionBtn']/i[contains(.,'call')]")
	WebElement conDetailCallBtn;
	
	@FindBy(xpath = "(//span[@class='conDetailNumActionBtn'])[4]")
	WebElement blockbtnOnCondtlpage;
	
	@FindBy(xpath = "//span[@class='conDetailNumActionBtn'][1]")
	WebElement conDetailCallBtn1;
	
	
	@FindBy(xpath = "//span[@class='contdetalnmallfont'][contains(.,'Delete')]")
	WebElement contactDeleteIcon;
	
	@FindBy(xpath = "//a[@href='/contacts']//i[contains(.,'account_circle')]")
	WebElement accountsLeftMenu;
	
	@FindBy(xpath = "//li[@class='nav_chmenu_list drp cursor'][contains(.,'Contacts')]")
	WebElement opencontactpage;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'SMS')]")
	WebElement openSMSPage;
	
	@FindBy(xpath = "//button[@id='dropdown-basic']")
	List<WebElement> optionSMSPage;
	
	@FindBy(xpath =   "//input[@class='sc-bxivhb jiLHBF']")
	WebElement numberTextBoxSMSPage;
	@FindBy(xpath =   "//a[@href='#'][contains(.,'Select All')]")
	WebElement optionSelectAllSMSPage;
	@FindBy(xpath =   "//a[@href='#'][text()='Select']")
	WebElement optionSelectSMSPage;
	@FindBy(xpath =   "//div[contains(@class,'sms_namechwid')]//following-sibling::div[contains(@class,'smscheckboxctch')]")
	List<WebElement> checkboxToSelectSMSPage;
	@FindBy(xpath =   "//div[contains(@class,'marreadopdiv_innerdiv')]//p[text()='MARK AS READ']")
	List<WebElement> btnMarkAsRead;
	@FindBy(xpath =   "//div[contains(@class,'marreadopdiv_innerdiv')]//p[text()='DELETE']")
	List<WebElement> btnDelete;
	
	@FindBy(xpath =   "//button[contains(@class,'yeschtn')]")
	List<WebElement> btnYesOnDeleteSMS;
	@FindBy(xpath =   "//button[contains(@class,'nochtn')]")
	List<WebElement> btnCancelOnDeleteSMS;
	
	@FindBy(xpath =   "//p[@class='marreadopdiv_text bottomzero'][contains(.,'DELETE')]")
	WebElement optionSDeleteSMSPage;
	
	@FindBy(xpath = "//p[contains(.,'Are you sure you want to delete this contact ?')]")
	WebElement contactDeleteConfirmMessage;
	
	@FindBy(xpath = "//div[@class='dialer_noti_btn_call_back']") WebElement callReminderDropDown;
	@FindBy(xpath = "//div[@class='dialer_noti_btn_cancel']") WebElement callReminderCancel;
	
	@FindBy(xpath = "//div[@class='DraftEditor-editorContainer']//span")
	WebElement dialer_callNotes;
	
	@FindBy(xpath = "//div[@class='brdrcountdown']//div[@class='afterclduration']")
	WebElement dialer_acw_duration;
	
	@FindBy(xpath = "//span[contains(.,'After Call Work')]")
	WebElement dialer_Acw_Screen_Title;
	
	@FindBy(xpath = "//div[@class[contains(.,'notaddicondiv')]]//following-sibling::div[contains(.,'Add Note')]")
	WebElement acw_add_note;
	
	@FindBy(xpath = "//div[@class[contains(.,'endcallreprtdiv')]]/div")
	WebElement acw_phone_icon;
	
	@FindBy(xpath = "//div[@class[contains(.,'endcallreprt')]]//following-sibling::div/span[2]")
	WebElement acw_phone_number;
	
	@FindBy(xpath = "//textarea[contains(@class,'aftworkrtextarea')]")
	WebElement acw_textarea;
	
	@FindBy(xpath = "//div[@class[contains(.,'endafterworkbtn')]]")
	WebElement acw_endAfterWorkBtn;
	
	@FindBy(xpath = "//input[@id='0']/following-sibling::div//*[local-name()='svg'][1]")
	WebElement AddEditSaveDeletenumber1;
	@FindBy(xpath = "//input[@id='4']/following-sibling::div//*[local-name()='svg'][2]")
	WebElement Deletenumber5;
	@FindBy(xpath = "//i[contains(@class,'contactaddicon_svg')]")
	WebElement Addcontact;
	
	
	
	
	@FindBy(xpath = "(//span[contains(@test-automation,'num')])[1]") WebElement firstEntryofCallreminder;
	@FindBy(xpath = "(//span[contains(@test-automation,'num')])[2]") WebElement secondEntryofCallreminder;
	@FindBy(xpath = "(//span[contains(@test-automation,'num')])[3]") WebElement thirdEntryofCallreminder;
	@FindBy(xpath = "(//span[contains(@test-automation,'name')])[1]") WebElement SavedFirstEntryofCallreminder;
	@FindBy(xpath = "(//span[contains(@test-automation,'name')])[1]") WebElement SavedSecondEntryofCallreminder;
	@FindBy(xpath = "(//span[contains(@test-automation,'name')])[1]") WebElement SavedThirdEntryofCallreminder;
	
	@FindBy(xpath = "(//span[@test-automation='call_plan_completed_call_num'])[1]") WebElement firstEntryOfCompletedOn;
	@FindBy(xpath = "//input[@test-automation='call_plan_completed_on_tab']/following-sibling::label") WebElement completedOn; 
	
	@FindBy(xpath = "//span[contains(.,'You are not allowed to make outgoing calls on this country, contact support for enable calling')]")
	WebElement BlockCountryErrorMessage;
	
	@FindBy(xpath = "//li[@class='drp nav_chmenu_list cursor'][contains(.,'Live Calls')]")
	WebElement LiveCall;
	
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Live Calls')]")
	WebElement livecalls;
	
	@FindBy(xpath = "//div[@role='tab'][contains(.,'Live Calls')]")
	WebElement Userstatustab;
	
	@FindBy(xpath = "//span[contains(.,'No Live Calls Available')]")
	WebElement nolivecalls;
	
	@FindBy(xpath = "//i[@class='material-icons lbFilterIcon'][contains(.,'filter_alt')]")
	WebElement filtericon;
	
	@FindBy(xpath = "//h6[@class='selectTitle text-center'][contains(.,'Users/Teams')]")
	WebElement UsersTeamstab;
	
	@FindBy(xpath = "//span[@class='ant-select-selection-placeholder'][contains(.,'Search')]")
	WebElement searchUsersTeams;
	
	@FindBy(xpath = "//span[@class='anticon anticon-close-circle']")
	WebElement clearsearchfiels;
	
	@FindBy(xpath = "//div[@test-automation='last_call_popup_date_time']")
	WebElement lastCallDetailDateAndTime;
	
	@FindBy(xpath = "//a[contains(@class,'calllogfilterdrpdown')][contains(.,'more_vert')]") 
	WebElement calllogfilterdrpdown;
	
	
	
	
}
	