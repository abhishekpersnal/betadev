package com.CallHippo.Dialer.sms;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.credit.CreditWebSettingPage;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

import com.CallHippo.web.numberAndUserSubscription.AddNumberPage;
import com.CallHippo.web.numberAndUserSubscription.InviteUserPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class SMSTest {

	DialerIndex phoneApp1;
	WebDriver driverphoneApp1;
	DialerIndex phoneApp2;
	WebDriver driverphoneApp2;
	DialerIndex phoneApp2Subuser;
	WebDriver driverphoneApp2Sub;

	WebToggleConfiguration webApp2;
	WebToggleConfiguration webApp1;
	WebToggleConfiguration webMail;
	WebDriver driverwebApp2;
	WebDriver driverwebApp1;
	WebDriver driverwebMail;
	WebToggleConfiguration webApp2SubUser;
	HolidayPage webApp2HolidayPage;
	//SignupPage registrationPage;
	//InviteUserPage inviteUserPageObj;
	//AddNumberPage addNumberPage;
	//LoginPage loginpage;
	SignupPage registrationPage1;
	InviteUserPage inviteUserPageObj1;
	AddNumberPage addNumberPage1;
	LoginPage loginpage1;
	XLSReader planPrice;
	CreditWebSettingPage creditWebSettingPage;

	RestAPI creditAPI;
	RestAPI planPriceAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String acc2Number1 = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number1 = data.getValue("account1Number1"); // account 1's number
	String number3 = data.getValue("account3Number1"); // account 1's number
	String number4 = data.getValue("account4Number1"); // account 1's number
	String number5 = data.getValue("account5Number1"); // account 1's number
	String smsCost1 = Double.toString(Double.parseDouble(data.getValue("oneSMScharge")));;
	String smsCost2 = Double.toString(Double.parseDouble(data.getValue("twoSMScharge")));;
	String smsCost3 = Double.toString(Double.parseDouble(data.getValue("threeSMScharge")));;

	String userID, userIDWebApp1, subuserID, sub1userIDWebApp1, sub2userIDWebApp1,userIDwebMail;
	String numberID;
	String contactName = "Cont" + Common.name();

	String nameForOneNumber;
	String nameforMainUser;
	String gmailUser = data.getValue("gmailUser");
	String callerName2 = null;
	String planPriceBronzeMonthly,planPriceSilverMonthly,planPricePlatinumMonthly;
	String planPriceBronzeAnnually,planPriceSilverAnnually,planPricePlatinumAnnually;
	
	public SMSTest() throws Exception {

		excel = new Common(data.getValue("environment") + "\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		planPriceAPI= new RestAPI();
		
	}

	@BeforeTest
	public void initialization() throws Exception {	
	
		    planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
			
			planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
		
			planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
			
			planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
			
			planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
		
			planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
			
			
		driverwebApp1 = TestBase.init();
		System.out.println("Opened webApp1 session");
		webApp1 = PageFactory.initElements(driverwebApp1, WebToggleConfiguration.class);
		
		driverwebApp2 = TestBase.init();
		System.out.println("Opened webApp2 session");
		webApp2 = PageFactory.initElements(driverwebApp2, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driverwebApp2, HolidayPage.class);
		creditWebSettingPage = PageFactory.initElements(driverwebApp2, CreditWebSettingPage.class);
		
		driverphoneApp1 = TestBase.init_dialer();
		System.out.println("Opened phoneApp1 session");
		phoneApp1 = PageFactory.initElements(driverphoneApp1, DialerIndex.class);

		driverphoneApp2 = TestBase.init_dialer();
		System.out.println("Opened phoneApp2 session");
		phoneApp2 = PageFactory.initElements(driverphoneApp2, DialerIndex.class);

		driverphoneApp2Sub = TestBase.init_dialer();
		System.out.println("Opened phoneApp2Subuser session");
		phoneApp2Subuser = PageFactory.initElements(driverphoneApp2Sub, DialerIndex.class);

		try {
			try {
				driverwebApp2.get(url.signIn());
				Common.pause(2);
				System.out.println("Opened webapp2 signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverwebApp2.get(url.signIn());
				Common.pause(2);
				System.out.println("Opened webapp2 signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driverwebApp1.get(url.signIn());
				Common.pause(2);
				System.out.println("Opened WEbApp1 signin Page");
				loginWeb(webApp1, account1Email, account1Password);
			} catch (Exception e) {
				driverwebApp1.get(url.signIn());
				Common.pause(2);
				System.out.println("Opened WEbApp1 signin Page");
				loginWeb(webApp1, account1Email, account1Password);
			}

			try {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(2);
				System.out.println("Opened phoneApp1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneApp1");
			} catch (Exception e) {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(2);
				System.out.println("Opened phoneApp1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneApp1");
			}

			try {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(2);
				System.out.println("Opened phoneApp2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneApp2");
			} catch (Exception e) {
				driverphoneApp2.get(url.dialerSignIn());
			    Common.pause(2);
				System.out.println("Opened phoneApp2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneApp2");
			}
			try {
				driverphoneApp2Sub.get(url.dialerSignIn());
				Common.pause(2);
				System.out.println("Opened phoneApp2subuser signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				System.out.println("loggedin phoneApp2subuser");
			} catch (Exception e) {
				driverphoneApp2Sub.get(url.dialerSignIn());
				Common.pause(2);
				System.out.println("Opened phoneApp2subuser signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				System.out.println("loggedin phoneApp2subuser");
			}

			try {

				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(5000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(acc2Number1);
				Thread.sleep(5000);
				
				webApp2.userTeamAllocation("users");
				
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(7000);
				webApp2.makeDefaultNumber(acc2Number1);
			
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(7000);
				webApp2.makeDefaultNumber(acc2Number1);
				

				driverphoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());



				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2.clickLeftMenuPlanAndBilling();
				creditWebSettingPage.autoRechargeToggle("off");

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				Thread.sleep(5000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(acc2Number1);
				Thread.sleep(7000);
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(7000);
				webApp2.makeDefaultNumber(acc2Number1);
			
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(7000);
				webApp2.makeDefaultNumber(acc2Number1);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverphoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2.clickLeftMenuPlanAndBilling();
				creditWebSettingPage.autoRechargeToggle("off");
		}
			
	} catch (Exception e1) {
			String testname = "Add contact Before Method ";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverphoneApp2Sub, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driverwebMail, testname, "DriverWeb Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
	
	try {
		
		driverwebMail = TestBase.init();
		System.out.println("Opened webmail session");
		webMail = PageFactory.initElements(driverwebMail, WebToggleConfiguration.class);
		inviteUserPageObj1 = PageFactory.initElements(driverwebMail, InviteUserPage.class);
		addNumberPage1 = PageFactory.initElements(driverwebMail, AddNumberPage.class);
		loginpage1 = PageFactory.initElements(driverwebMail, LoginPage.class);
		registrationPage1 = PageFactory.initElements(driverwebMail, SignupPage.class);
		
		
			Common.pause(2);
			driverwebApp2.get(url.signIn());
			driverwebApp1.get(url.signIn());
			driverphoneApp1.navigate().to(url.dialerSignIn());
			driverphoneApp2.navigate().to(url.dialerSignIn());
			driverphoneApp2Sub.navigate().to(url.dialerSignIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp1.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			
			Common.pause(3);

			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(7000);

			String Url = driverwebApp2.getCurrentUrl();
			userID = webApp2.getUserId(Url);
			System.out.println("userID:" + userID);
			creditAPI.updateSMSCredit(userID, "100", "100", "100", "100", "1000");
			creditAPI.UpdateCredit("500", "0", "0", userID);

			Common.pause(3);
			webApp1.userspage();
			webApp1.navigateToUserSettingPage(account1Email);
			Thread.sleep(7000);

			String Url1 = driverwebApp1.getCurrentUrl();
			userIDWebApp1 = webApp1.getUserId(Url1);
			System.out.println("userIDWebApp1:" + userIDWebApp1);

			creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "100", "100", "1000");
			creditAPI.UpdateCredit("500", "0", "0", userIDWebApp1);
			
	      webApp2.clickLeftMenuDashboard();
	      Common.pause(2);
	      webApp2.clickLeftMenuSetting();
	      Common.pause(2);
	      webApp2.scrollToCallBlockingTxt();
	      Common.pause(1);
	      webApp2.deleteAllNumbersFromBlackList();
	      Common.pause(2);
	       creditAPI.deleteRechargeActivity(userID);
			webApp2.clickLeftMenuPlanAndBilling();
		    creditWebSettingPage.autoRechargeToggle("off");

			phoneApp2.deleteAllContactsFromDialer();
			driverphoneApp2.get(url.dialerSignIn());

			phoneApp2.deleteAllSMS();
			driverphoneApp2.get(url.dialerSignIn());

			phoneApp2Subuser.deleteAllContactsFromDialer();
			driverphoneApp2Sub.get(url.dialerSignIn());

			phoneApp1.deleteAllSMS();
			driverphoneApp1.get(url.dialerSignIn());
	
		} catch (Exception e) {
			driverwebMail = TestBase.init();
			System.out.println("Opened webmail session");
			webMail = PageFactory.initElements(driverwebMail, WebToggleConfiguration.class);
			inviteUserPageObj1 = PageFactory.initElements(driverwebMail, InviteUserPage.class);
			addNumberPage1 = PageFactory.initElements(driverwebMail, AddNumberPage.class);
			loginpage1 = PageFactory.initElements(driverwebMail, LoginPage.class);
			registrationPage1 = PageFactory.initElements(driverwebMail, SignupPage.class);
			try {
				Common.pause(2);
				driverwebApp2.get(url.signIn());
				driverwebApp1.get(url.signIn());
				driverphoneApp1.navigate().to(url.dialerSignIn());
				driverphoneApp2.navigate().to(url.dialerSignIn());
				driverphoneApp2Sub.navigate().to(url.dialerSignIn());

				Common.pause(3);
				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Subuser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}
				Common.pause(3);

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(7000);

				String Url = driverwebApp2.getCurrentUrl();
				userID = webApp2.getUserId(Url);
				System.out.println("userID:" + userID);

				Common.pause(3);
				webApp1.userspage();
				webApp1.navigateToUserSettingPage(account1Email);
				Thread.sleep(7000);
				String Url1 = driverwebApp1.getCurrentUrl();
				userIDWebApp1 = webApp1.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);

				creditAPI.updateSMSCredit(userID, "100", "100", "100", "100", "1000");
				creditAPI.UpdateCredit("500", "0", "0", userID);

				creditAPI.UpdateCredit("500", "0", "0", userIDWebApp1);

	
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				
	            webApp2.clickLeftMenuPlanAndBilling();
	            creditAPI.deleteRechargeActivity(userID);
				creditWebSettingPage.autoRechargeToggle("off");
				Common.pause(2);
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp2Subuser.deleteAllContactsFromDialer();
				driverphoneApp2Sub.get(url.dialerSignIn());
				
				phoneApp2.deleteAllSMS();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp1.deleteAllSMS();
				driverphoneApp1.get(url.dialerSignIn());			
		} catch (Exception e1) {
				String testname = "Add Contact Before Method ";
				Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driverphoneApp2Sub, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail login");
				Common.Screenshot(driverwebMail, testname, "webmail Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2Sub, testname,
					"PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebMail, testname, "webappmail Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2Sub, testname,
					"PhoneAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebApp2, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverwebMail, testname, "webappmail Pass " + result.getMethod().getMethodName());
		  System.out.println(testname + " - " + result.getMethod().getMethodName());

		}
	driverwebMail.quit();
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driverphoneApp1.quit();
		driverphoneApp2.quit();
		driverphoneApp2Sub.quit();
		driverwebApp2.quit();
		driverwebApp1.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}

	private String signUpUser() throws Exception {
		try {
			driverwebMail.get(url.livesignUp());
		}catch(Exception e) {
			driverwebMail.get(url.livesignUp());
		}
		Common.pause(1);
       System.out.println("----Welcome to signup");
		
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage1.clickOn_website_header_signUp_Btn();
		registrationPage1.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage1.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage1.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage1.enter_website_signUp_popUp_workEmail(email);
		registrationPage1.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage1.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
	    registrationPage1.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage1.getTextMainTitle(),  "Thank you for Signing up.");

		return email;
	}

	private void LoginGmailAndConfirmYourMail() throws Exception {

		//registrationPage.ValidateVerifyButtonOnGmail();
		registrationPage1.validateVerifyButtonOnGmail();

		String actualTitle = inviteUserPageObj1.validateTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		registrationPage1.verifyWelcomeToCallHippoPopup();
		registrationPage1.closeAddnumberPopup();
	}

	private void add_contact_with_one_Number() throws Exception {
		String contactName = "Cont" + Common.name();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		nameForOneNumber = phoneApp2.enterNameInContacts(contactName);
		String Number = phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		phoneApp2.ClickOnAddBtnAt(number1);

		String Number2 = phoneApp2.EnternextNumberonAddContact(1, "+12345678900");
		phoneApp2.ClickOnSaveBtnAt(Number2);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		Common.pause(3);
		String Company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");
		String Email = phoneApp2.EnterEmailOnAddContact(account2Email);

		// phoneApp2.clickOnTickGreenBtnToAddContactName();
		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), Number);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), nameForOneNumber);
		assertEquals(phoneApp2.validateEmailIdOncontactDtlpage(), Email);
		assertEquals(phoneApp2.validateCompanyNameOncontactDtlpage(), Company);

	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_No_any_SMS_on_SMS_page() throws Exception {
		phoneApp2.deleteAllSMS();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();

		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), false, " --validate sub menu drop down On SMS Page");
	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_some_SMS_available_on_SMS_page_for_saved_and_unsaved_number_verify_receive_sms_activity_sms_feed_record()
			throws Exception {
		System.out.println("start test case...");
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();

		// unsaved number
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message1, " --validate message On SMS Page");

		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), true,
				" --validate Callhippo Symbol Icon for unsaved number On SMS Page");

		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		// check in activity feed on phoneApp2 :Receive SMS from new number: unread-read

		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		phoneApp2.clickOnSMSThreadOnSMSPage(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message1), true,
				" --validate incoming Message On SMS Thread");

		// after read
		driverwebApp2.navigate().refresh();
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Read", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// Saved number
		// **** add contact with one number in phoneApp2 ****
		driverphoneApp2.get(url.dialerSignIn());
		add_contact_with_one_Number();
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message2);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(nameForOneNumber), nameForOneNumber,
				" --validate contact name On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(nameForOneNumber), message2, " --validate message On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.getTwoLetterFromContactNameOnSMSPage(nameForOneNumber),
				phoneApp2.getTwoLetterInIconofContactOnSMSPage(nameForOneNumber),
				" --validate icon with characters of saved name On SMS Page");

		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(nameForOneNumber), true,
				" --validate unread message On SMS Page");

		// check in activity feed on phoneApp2 :Receive SMS from saved contact :
		// unread-read

		webApp2.numberspage();
		Common.pause(5);
		String smsToText1 = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), nameForOneNumber, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText1, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		phoneApp2.clickOnSMSThreadOnSMSPage(nameForOneNumber);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForOneNumber);
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message2), true,
				" --validate incoming Message On SMS Thread");

		// after read
		driverwebApp2.navigate().refresh();
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), nameForOneNumber, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText1, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Read", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_validation_On_Compose_sms_Page_by_entering_more_than_one_number_And_by_selected_more_than_one_saved_contact()
			throws Exception {
		System.out.println("start test case...");

		String contactName = "Cont" + Common.name();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickoncontactpage();
		phoneApp2.ClickonAddcontact();

		assertEquals(phoneApp2.validatehumanIcononEditcontactpage(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforName(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforNumber(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforEmail(), false);
		assertEquals(phoneApp2.ValidateSaveBtnDisable(), true);
		assertEquals(phoneApp2.validaterequiredFieldIconforcampany(), false);

		String name = phoneApp2.enterNameInContacts(contactName);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		String email = phoneApp2.EnterEmailOnAddContact(account2Email);
		String company = phoneApp2.EnterCompanyOnAddContact("CAllhippo1");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), name);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateAddContactButtonOncontactDisplaypage(), true);
		phoneApp2.ClickonAddcontact();

		// different name with same number Contact
		String contactName1 = "Cont" + Common.name();
		String differentName = phoneApp2.enterNameInContacts(contactName1);
		Common.pause(2);
		assertEquals(phoneApp2.ValidatetickGreenBtnDisable(), true);
		phoneApp2.EnterNumberonAddContact(number1);
		phoneApp2.ClickOnSaveBtnAt(number1);
		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");

		Common.pause(3);
		phoneApp2.EnterEmailOnAddContact(account2Email);
		phoneApp2.EnterCompanyOnAddContact("Callhippo2");

		phoneApp2.clickOnTickGreenBtnToAddContactName1();
		assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");

		assertEquals(phoneApp2.validateNumberOncontactDtlpage(1), number1);
		assertEquals(phoneApp2.validateNameOncontactDtlpage(), differentName);

		phoneApp2.ClickOnBackBtnEditcontactpage();
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(1), name);
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(2), differentName);
		phoneApp2.clickOnSearchIconOncontactDisplaypage();
		phoneApp2.searchContactNameOncontactDisplaypage(name);
		assertEquals(phoneApp2.validateNameOncontactDisplaypage(), name);
		phoneApp2.clearSearchContact();
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(1), name);
		assertEquals(phoneApp2.validateNameOnContactDisplaypage(2), differentName);

		phoneApp2.clickOnCloseIconOncontactDisplaypage();
		Common.pause(3);
		assertEquals(phoneApp2.validateContactsTitleOncontactDisplaypage(), true);
		assertEquals(phoneApp2.validateSearchIconOncontactDisplaypage(), true);

		System.out.print("---Added different name with same Contact number--- ");

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");

		Common.pause(2);
		assertEquals(phoneApp2.validateButtonDepartmentlistOnSMSCompose(), true,
				"--validate Button Department list On SMS Compose Page");
		assertEquals(phoneApp2.validatecontactIconatSMSthreadpageIsDisplayed(), true,
				"--validate Button to open all contact on SMS Compose Page");
		assertEquals(phoneApp2.validatemessageTextareaOnSMSComposePage(), true,
				"--validate Message Textarea on SMS Compose Page");
		assertEquals(phoneApp2.validatenumberTextBoxOnSMSComposePage(), true,
				"--validate Number TextBox on SMS Compose Page");
		assertEquals(phoneApp2.validateSendMessageButtonOnSMScomposePage(), true,
				"--validate Send Message Button on SMS Compose Page");

		phoneApp2.clickOncontactIconatSMSthreadpage();
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(), contactName);
		phoneApp2.ClickonSpecificContact(contactName);
		Common.pause(2);
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(contactName), false);

		Common.pause(2);
		phoneApp2.ClickonSpecificContact(contactName1);

		assertEquals(phoneApp2.validationMessage(), "You can't send message to more than 1 recipient at the same time");
		Common.pause(2);
		phoneApp2.clickoncancelButtonnumberOrcontactTofield(contactName);
		assertEquals(phoneApp2.validatecontactNameatSMSthreadpage(contactName), true,
				"Contact should be remove from 'TO' list and added in All Contacts list");

		phoneApp2.clickOngreentikSMSthreadpage();
		// Contact list should be close
		assertEquals(phoneApp2.validatenumberTextBoxOnSMSComposePage(), true,
				"--validate Number TextBox on SMS Compose Page");
		assertEquals(phoneApp2.validatecontactIconatSMSthreadpageIsDisplayed(), true,
				"--validate Button to open all contact on SMS Compose Page");

		// with more than one number enter in number field

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");

		phoneApp2.enterNumberOnSMSPage(number1);
		Common.pause(3);
		phoneApp2.enterNumberOnSMSPage(number3);
		assertEquals(phoneApp2.validationMessage(), "You can't send message to more than 1 recipient at the same time");
		Common.pause(2);
		phoneApp2.clickoncancelButtonnumberOrcontactTofield(number1);
		// number list should be close
		assertEquals(phoneApp2.validatenumberTextBoxOnSMSComposePage(), true,
				"--validate Number TextBox on SMS Compose Page");

	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_validation_for_Send_SMS_Without_Entering_Country_code_And_invalid_number() throws Exception {
		System.out.println("start test case...");
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");

		Common.pause(2);
		String numberWithoutCountrycode = phoneApp2.enterNumberWithoutCountrycodeOnSMSPage(number1);

		Common.pause(3);
		String message2 = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message2);
		Common.pause(3);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You can send sms only on national numbers");
		phoneApp2.clickoncancelButtonnumberOrcontactTofield(numberWithoutCountrycode);
		Common.pause(8);
		phoneApp2.enterNumberOnSMSPage("+236");
		Common.pause(3);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Enter a valid number");

	}

	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_validate_Time_of_Particular_SMS_chat_IsDisplayed_of_saved_unsaved_contact() throws Exception {
		System.out.println("start test case...");
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();

		// unsaved number from App1 to App2
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// from App2 to App1
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message2);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(number1), number1);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateMessageOnSMSThread(message2), true, " --validate Message On SMS Thread");

		phoneApp2.clickOnParticularMessage(message1);
		assertEquals(phoneApp2.validateTimeofParticularSMSchatIsDisplayed(message1), true,
				" --validate Time of particular sms chat will display");
		Common.pause(2);
		phoneApp2.clickOnParticularMessage(message2);
		assertEquals(phoneApp2.validateTimeofParticularSMSchatIsDisplayed(message1), false,
				" --validate Time of particular sms chat will display");
		assertEquals(phoneApp2.validateTimeofParticularSMSchatIsDisplayed(message2), true,
				" --validate Time of particular sms chat will display");

		Common.pause(3);
		String message3 = "Automation3" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message3);
		Common.pause(3);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateMessageOnSMSThread(message3), true, " --validate Message On SMS Thread");

		// second message sent from App1 to App2
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message4 = "Automation4" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message4);
		Common.pause(3);
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message4), true, " --validate Message On SMS Thread");

		// second message Received from App2 to App1
		driverphoneApp2.get(url.dialerSignIn());
		driverphoneApp2.navigate().refresh();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");
		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		phoneApp2.ClickonSpecificContact(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateParticularMessageSMSchatIsDisplayed(message4), true,
				" --validate Message On SMS Thread");

		phoneApp2.clickOnParticularMessage(message4);
		assertEquals(phoneApp2.validateTimeofParticularSMSchatIsDisplayed(message4), true,
				" --validate Time of particular sms chat will display");

		phoneApp2.ClickOnBackBtnEditcontactpage();

		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1, " --validate contact name On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message4, " --validate message On SMS Page");
	}

	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Send_SMS_with_default_department_and_with_Different_department_and_verify_activity_sms_feed_with_default_department()
			throws Exception {
		System.out.println("start test case...");
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		// Send_SMS_with_default_department
		phoneApp2.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message1);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(number1), number1);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check on activity feed record for SMS sent
		webApp2.numberspage();
		Common.pause(5);
		String smsfromText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), number1, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// Send_SMS_with_different_department- US department-UK number

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.clickonButtonDepartmentlistOnSMSCompose();
		phoneApp2.selectNumberFromDepartmentlistOnSMSCompose(acc2Number2);
		phoneApp2.enterNumberOnSMSPage("+441483694170");
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message2);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField("+441483694170"), "+441483694170");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You can send sms only on national numbers");

	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_mark_as_read_And_sms_delete() throws Exception {
		System.out.println("start test case...");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();

		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");
		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		phoneApp2.clickonSubMenuDropdownOnSMSPage();
		assertEquals(phoneApp2.validateoptionSelectAllSMSPage(), true, " --validate option SelectAll On SMS Page");
		assertEquals(phoneApp2.validateoptionSelectSMSPage(), true, " --validate option Select On SMS Page");

		// select
		phoneApp2.clickonoptionSelectOnSMSPage();
        Common.pause(2);
		assertEquals(phoneApp2.validateAllThreadsCheckboxToSelect(), true,	" --validate All Threads Checkbox To Select On SMS Page");

		phoneApp2.clickonAnyoneUnreadSMS();
		assertEquals(phoneApp2.validateMarksAsReadSMSPage(), true, " --validate Marks as read displayed On SMS Page");
		assertEquals(phoneApp2.validateDeleteSMSPage(), true, " --validate delete displayed On SMS Page");
		phoneApp2.clickonMarksAsReadSMSPage();
		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), false, " --validate unread message On SMS Page");

		// select all
		System.out.println( " --select all  ");
		phoneApp2.clickonSubMenuDropdownOnSMSPage();
		assertEquals(phoneApp2.validateoptionSelectAllSMSPage(), true, " --validate option SelectAll On SMS Page");
		assertEquals(phoneApp2.validateoptionSelectSMSPage(), true, " --validate option Select On SMS Page");
		Common.pause(3);
		phoneApp2.clickonoptionSelectAllOnSMSPage();
		assertEquals(phoneApp2.validateMarksAsReadSMSPage(), true, " --validate Marks as read displayed On SMS Page");
		assertEquals(phoneApp2.validateDeleteSMSPage(), true, " --validate delete displayed On SMS Page");
		phoneApp2.clickonMarksAsReadSMSPage();
		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), false, " --validate unread message On SMS Page");

		// SMS delete
		phoneApp2.clickonSubMenuDropdownOnSMSPage();
		phoneApp2.clickonoptionSelectOnSMSPage();

		assertEquals(phoneApp2.validateAllThreadsCheckboxToSelect(), true,
				" --validate All Threads Checkbox To Select On SMS Page");

		phoneApp2.clickonAnyoneUnreadSMS();
		assertEquals(phoneApp2.validateMarksAsReadSMSPage(), true, " --validate Marks as read displayed On SMS Page");
		assertEquals(phoneApp2.validateDeleteSMSPage(), true, " --validate delete displayed On SMS Page");

		phoneApp2.clickonDeleteSMSPage();
		assertEquals(phoneApp2.validateYesButtonDeleteSMSPage(), true, " --validate Yes Button Delete On SMS Page");
		assertEquals(phoneApp2.validateCancelButtonDeleteSMSPage(), true,
				" --validate Cancel Button Delete On SMS Page");

		phoneApp2.clickonCancelDeleteSMSPage();
		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message1, " --validate message On SMS Page");

		Common.pause(2);
		phoneApp2.clickonDeleteSMSPage();
		phoneApp2.clickOnConfirmPopupYesButton();
		Common.pause(3);
		assertEquals(phoneApp2.validateConatctNameOnSMSPageIsDisplayed(number1), false,
				" --validate contactname or number On SMS Page");

	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Send_SMS_from_Call_Log_And__Send_SMS_from_Contact() throws Exception {
		System.out.println("start test case...");
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number1);
		Common.pause(3);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOncallDetailSMSIconButton();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message1);
		Common.pause(3);

		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// Send SMS from Contact
		Common.pause(5);
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		add_contact_with_one_Number();
		phoneApp2.clickOncallDetailSMSButton();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message2);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(nameForOneNumber), nameForOneNumber,
				" --validate number On SMS Thread");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp2.validateNameOnSMSThread(), nameForOneNumber);
		assertEquals(phoneApp2.validateMessageOnSMSThread(message2), true, " --validate Message On SMS Thread");
	}

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Send_SMS_2sms_left_for_day() throws Exception {
		System.out.println("start test case...");

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url2 = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url2);
		System.out.println("userID:" + userID);
		creditAPI.updateSMSCredit(userID, "100", "0", "100", "100", "1000");

		Common.pause(3);
		webApp1.userspage();
		webApp1.navigateToUserSettingPage(account1Email);
		Thread.sleep(9000);

		String Url1 = driverwebApp1.getCurrentUrl();
		userIDWebApp1 = webApp1.getUserId(Url1);
		System.out.println("userID:" + userIDWebApp1);
		creditAPI.UpdateCredit("0", "0", "0", userIDWebApp1);

		creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "0", "100", "100");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDWebApp1, "usedSms"), 0);

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		assertEquals(creditAPI.getAvailableSMSCredit(userIDWebApp1, "usedSms"), 1);

		// verify in phoneApp2
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1, " --validate number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message1, " --validate message On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		// check in activity feed on phoneApp2 :Receive SMS from saved contact :
		// unread-read

		webApp2.numberspage();
		Common.pause(5);
		String smsToText1 = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText1, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), "-", "---validate SMS cost");

		assertEquals(creditAPI.getAvailableSMSCredit(userID, "usedIncomingSms"), 1, "---validate used IncomingSms");

		int todaySmsUsedsinweb1 = creditAPI.getAvailableSMSCredit(userIDWebApp1, "todaySmsUsed");

		creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "0", "100", Integer.toString(todaySmsUsedsinweb1));

		System.out.println("send Message form phoneapp1 to phoneapp2 with 2 message charcter count");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message2 = "With CallHippo, you can buy a business phone system and running in less than 3 minutes from anywhere in the world. Save time by sending pre-recorded messages to multiple people. With voice broadcasting, you can reach your customers even if they are presently unavailable. The message gets stored as a voicemail which they can hear out at their convenient time."
				+ excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message2);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		// Your daily SMS limit is currently set to 19. For increasing the limit, please
		// contact support@callhippo.com.
		assertEquals(phoneApp1.validationMessage(),
				"Your daily SMS limit is currently set to " + Integer.toString(todaySmsUsedsinweb1)
						+ ". For increasing the limit, please contact support@callhippo.com.",
				" --validate Validation for SMS limit.");

	}

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_send_sms_when_No_sms_left_for_day_main_user_And_send_sms_when_No_sms_left_for_day_sub_user()
			throws Exception {
		System.out.println("start test case...");
		// 1. Send sms :: No sms left for day MainUser
		
		Common.pause(5);
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String message = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(number1), number1);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(),"Message Sent Successfully");
		
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.UpdateCredit("0", "0", "0", userID);

		int todaySmsUsedsinweb2 = creditAPI.getAvailableSMSCredit(userID, "todaySmsUsed");

		String todaySmsUsedcount = Integer.toString(todaySmsUsedsinweb2);
		System.out.println("todaySmsUsedcount:" + todaySmsUsedcount);

		creditAPI.updateSMSCredit(userID, "100", "100", "0", "100", todaySmsUsedcount);

		Common.pause(5);
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message1);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(number1), number1);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(),
				"Your daily SMS limit is currently set to " + todaySmsUsedcount
						+ ". For increasing the limit, please contact support@callhippo.com.",
				" --validate Validation for SMS limit.");
		Common.pause(5);
		driverphoneApp2Sub.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2Subuser.waitForDialerPage();
		phoneApp2Subuser.clickOnSideMenu();
		Common.pause(2);
		phoneApp2Subuser.clickonsmspage();
		phoneApp2Subuser.clickOnSMSCompose();
		assertEquals(phoneApp2Subuser.validatenameOnNavigationTitle(), "SMS");

		phoneApp2Subuser.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String messageSubuser = "AutomationSubuser" + excel.randomPassword();
		phoneApp2Subuser.enterMessageOnSMSPage(messageSubuser);
		Common.pause(3);
		assertEquals(phoneApp2Subuser.ValidateNameInNumberField(number1), number1);
		phoneApp2Subuser.clickOnSendMessageButton();
		assertEquals(phoneApp2Subuser.validationMessage(), "Message Sent Successfully");

		// 2. Send sms :: No sms left for day Subuser

		System.out.println("start test case for sub user...");
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);

		String subUrl = driverwebApp2.getCurrentUrl();
		subuserID = webApp2.getUserId(subUrl);
		System.out.println("subuserID:" + subuserID);
		creditAPI.UpdateCredit("0", "0", "0", subuserID);
		int subans = creditAPI.getAvailableSMSCredit(subuserID, "todaySmsUsed");
		System.out.println("subansans....." + subans);
		String todaySmsUsedcountsub = Integer.toString(subans);
		System.out.println("todaySmsUsedcount:" + todaySmsUsedcountsub);

		creditAPI.updateSMSCredit(subuserID, "100", "100", "0", "100", todaySmsUsedcountsub);

		Common.pause(5);
		driverphoneApp2Sub.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2Subuser.waitForDialerPage();
		phoneApp2Subuser.clickOnSideMenu();
		Common.pause(2);
		phoneApp2Subuser.clickonsmspage();
		phoneApp2Subuser.clickOnSMSCompose();
		assertEquals(phoneApp2Subuser.validatenameOnNavigationTitle(), "SMS");

		phoneApp2Subuser.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String messageSubuser2 = "AutomationSubuser" + excel.randomPassword();
		phoneApp2Subuser.enterMessageOnSMSPage(messageSubuser2);
		Common.pause(3);
		assertEquals(phoneApp2Subuser.ValidateNameInNumberField(number1), number1);
		phoneApp2Subuser.clickOnSendMessageButton();
		assertEquals(phoneApp2Subuser.validationMessage(),
				"Your daily SMS limit is currently set to " + todaySmsUsedcountsub
						+ ". For increasing the limit, please contact support@callhippo.com.",
				" --validate Validation for SMS limit.");

	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Send_SMS_with_different_place_when_having_low_credit_Free_Outgoing_SMS_finished()
			throws Exception {
		System.out.println("start test case...");
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url2 = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url2);
		System.out.println("userID:" + userID);
		creditAPI.UpdateCredit("0", "0", "0", userID);
		creditAPI.updateSMSCredit(userID, "0", "0", "0", "0", "100");

		int usedoutSms = creditAPI.getAvailableSMSCredit(userID, "usedSms");
		System.out.println("usedIncomingSms:" + usedoutSms);// 0
		// 1. Send SMS from compose SMS when having low credit

		System.out.println("send Message form phoneapp1 to phoneapp2 from compose SMS");
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		phoneApp2.enterNumberOnSMSPage(number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp2.ValidateNameInNumberField(number1));
		assertEquals(phoneApp2.ValidateNameInNumberField(number1), number1, " --validate number On SMS Thread");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You have insufficient credits.");

		assertEquals(creditAPI.getAvailableSMSCredit(userID, "usedSms"), 0, "--validate used outgoing sms by API");

		// 2. SMS from call logs when having low credit

		System.out.println("send Message form phoneapp1 to phoneapp2 from call logs");
		Common.pause(2);
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOncallDetailSMSIconButton();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message2);
		Common.pause(3);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You have insufficient credits.");
		assertEquals(creditAPI.getAvailableSMSCredit(userID, "usedSms"), 0, "--validate used outgoing sms by API");

		// 3.SMS from contact details when having low credit
		System.out.println("send Message form phoneapp1 to phoneapp2 from contact details");

		Common.pause(5);
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		add_contact_with_one_Number();
		phoneApp2.clickOncallDetailSMSButton();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		Common.pause(3);
		String message3 = "Automation3" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message3);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(nameForOneNumber), nameForOneNumber,
				" --validate number On SMS Thread");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You have insufficient credits.");
		assertEquals(creditAPI.getAvailableSMSCredit(userID, "usedSms"), 0, "--validate used outgoing sms by API");

		// 4. SMS from DialPad when having low credit
		System.out.println("send Message form phoneapp1 to phoneapp2 from  dailpad ");

		Common.pause(5);
		driverphoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2.enterNumberinDialer(number1);
		phoneApp2.clickOnSendMessageButtonOnDialpad();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");

		Common.pause(3);
		String message4 = "Automation4" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message4);
		Common.pause(3);
		assertEquals(phoneApp2.ValidateNameInNumberField(number1), number1, " --validate number On SMS Thread");
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You have insufficient credits.");

		assertEquals(creditAPI.getAvailableSMSCredit(userID, "usedSms"), 0, "--validate used outgoing sms by API");
	}

	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_100_incoming_sms() throws Exception {
		System.out.println("start test case...");

		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.updateSMSCredit(userID, "100", "0", "100", "0", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), "-", "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost1, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		int usedIncomingSms = creditAPI.getAvailableSMSCredit(userID, "usedIncomingSms");
		System.out.println("usedIncomingSms:" + usedIncomingSms);
		assertEquals(usedIncomingSms, 1);

	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_incoming_SMS_with_more_than_160_character_in_Free_incoming_SMS() throws Exception {
		System.out.println("start test case...");

		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);

		creditAPI.updateSMSCredit(userID, "100", "0", "100", "0", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "With CallHippo, you can buy a business phone system and running in less than 3 minutes from anywhere in the world. Save time by sending pre-recorded messages to multiple people."
				+ excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), "-", "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost2, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost2);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost2);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		int usedIncomingSms = creditAPI.getAvailableSMSCredit(userID, "usedIncomingSms");
		System.out.println("usedIncomingSms:" + usedIncomingSms);
		assertEquals(usedIncomingSms, 2);

	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Charges_When_User_Remains_1_free_SMS_And_getting_SMS_More_Than_160_Character() throws Exception {
		System.out.println("start test case...");

		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.updateSMSCredit(userID, "100", "99", "0", "100", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "With CallHippo, you can buy a business phone system and running in less than 3 minutes from anywhere in the world. Save time by sending pre-recorded messages to multiple people."
				+ excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost2, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, smsCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, smsCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost2);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost2);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_charges_in_incoming_SMS() throws Exception {
		System.out.println("start test case...");

		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.updateSMSCredit(userID, "100", "100", "0", "100", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost1, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, smsCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, smsCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_charges_in_incoming_SMS_more_than_160_character() throws Exception {
		System.out.println("start test case...");
		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		// (String userId, String incomingSms, String usedIngoingSms, String
		// usedOutgoingSms,String outgoingSms, String smsPerDay)
		creditAPI.updateSMSCredit(userID, "100", "100", "0", "100", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "With CallHippo, you can buy a business phone system and running in less than 3 minutes from anywhere in the world. Save time by sending pre-recorded messages to multiple people."
				+ excel.randomPassword();

		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost2, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost2, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, smsCost2);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, smsCost2);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost2);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost2);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_charges_by_receiving_SMS_with_multiple_of_160() throws Exception {
		System.out.println("start test case...");

		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		// (String userId, String incomingSms, String usedIngoingSms, String
		// usedOutgoingSms,String outgoingSms, String smsPerDay)
		creditAPI.updateSMSCredit(userID, "100", "100", "0", "100", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "With CallHippo, you can buy a business phone system and running in less than 3 minutes from anywhere in the world. Save time by sending pre-recorded messages to multiple people. With voice broadcasting, you can reach your customers even if they are presently unavailable. The message gets stored as a voicemail which they can hear out at their convenient time."
				+ excel.randomPassword();

		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost3, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost3, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, smsCost3);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, smsCost3);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost3);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost3);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	}

	// 5750
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Free_SMS_0_And_user_0_credit_and_receiving_SMS_for_not_saved_contact_And_Saved_contact()
			throws Exception {
		System.out.println("start test case...");

		// 1. Not saved contact
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(7000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.UpdateCredit("0", "0", "0", userID);
		// (String userId, String incomingSms, String usedIngoingSms, String
		// usedOutgoingSms,String outgoingSms, String smsPerDay)
		creditAPI.updateSMSCredit(userID, "100", "100", "100", "100", "1000");

		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// verify in phoneApp2
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameDisplayOnSMSPage(),false);


		// credit validate
		Common.pause(3);
		webApp2.clickLeftMenuDashboard();
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValAfter = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValAfter, "0.00", "--validate 0 credit");
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueAfter = webApp2.availableCreditValue();
		assertEquals(settingcreditValueAfter, "0.00", "-- validate 0 credit");

		assertEquals(leftPanelcreditValAfter, settingcreditValueAfter);
		// left to validate charges in db

		// 2. saved contact
		Common.pause(3);
		creditAPI.UpdateCredit("0", "0", "0", userID);
		// (String userId, String incomingSms, String usedIngoingSms, String
		// usedOutgoingSms,String outgoingSms, String smsPerDay)
		creditAPI.updateSMSCredit(userID, "100", "100", "100", "100", "1000");

		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValBefore1 = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore1 = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore1, settingcreditValueBefore1);

		driverphoneApp2.get(url.dialerSignIn());
		add_contact_with_one_Number();

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message2);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// verify in phoneApp2
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameDisplayOnSMSPage(),false);
		

		// credit validate
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValAfter1 = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValAfter1, "0.00", "--validate 0 credit");
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueAfter1 = webApp2.availableCreditValue();
		assertEquals(settingcreditValueAfter1, "0.00", "-- validate 0 credit");

		assertEquals(leftPanelcreditValAfter1, settingcreditValueAfter1);
		// left to validate charges in db

	}

	@Test(enabled = false,priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_existing_SMS_thread_when_Free_SMS_0_And_user_0_credit_and_receiving_SMS() throws Exception {
		System.out.println("start test case...");
		// unsaved number
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		System.out.println("---message1"+message1);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread"); // Send SMS by entering country code
																													

		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message1, " --validate message On SMS Page");

		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), true,
				" --validate Callhippo Symbol Icon for unsaved number On SMS Page");

		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		// check in activity feed on phoneApp2 :Receive SMS from new number

		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		phoneApp2.clickOnSMSThreadOnSMSPage(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message1), true,
				" --validate incoming Message On SMS Thread");

		// after read
		webApp2.numberspage();
		Common.pause(5);
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Read", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.UpdateCredit("0", "0", "0", userID);
		creditAPI.updateSMSCredit(userID, "100", "100", "100", "100", "1000");

		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message2);
		System.out.println("---message2"+message2);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message2), true, " --validate Message On SMS Thread"); // Send  SMS  by  entering Country code
		 																									
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message2, " --validate message On SMS Page");

		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), true,
				" --validate Callhippo Symbol Icon for unsaved number On SMS Page");

		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		phoneApp2.clickOnSMSThreadOnSMSPage(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message1), true,
				" --validate incoming Message On SMS Thread");
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message2), false,
				" --validate incoming Message On SMS Thread");

		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValAfter1 = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValAfter1, "0.00", "--validate 0 credit");
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueAfter1 = webApp2.availableCreditValue();
		assertEquals(settingcreditValueAfter1, "0.00", "-- validate 0 credit");

		assertEquals(leftPanelcreditValAfter1, settingcreditValueAfter1);
	}

	@Test(enabled = false, priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_thread_OPEN_and_receiving_SMS_when_Free_SMS_0_And_user_0_credit_and_receiving_SMS()
			throws Exception {
		System.out.println("start test case...");
		// unsaved number
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message1, " --validate message On SMS Page");

		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), true,
				" --validate Callhippo Symbol Icon for unsaved number On SMS Page");

		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		// check in activity feed on phoneApp2 :Receive SMS from new number

		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		phoneApp2.clickOnSMSThreadOnSMSPage(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message1), true,
				" --validate incoming Message On SMS Thread");

		// after read
		driverwebApp2.navigate().refresh();
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Read", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.UpdateCredit("0", "0", "0", userID);
		creditAPI.updateSMSCredit(userID, "100", "100", "100", "100", "1000");

		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message2);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message2), true, " --validate Message On SMS Thread");

		// open conversion for acc2Number1
		driverwebApp2.navigate().refresh();
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message1), true,
				" --validate incoming Message On SMS Thread");
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message2), false,
				" --validate incoming Message On SMS Thread");

		phoneApp2.ClickOnBackBtnEditcontactpage();
		Common.pause(3);
		assertEquals(phoneApp2.validateSideMenuIsDisplayed(), true, " --validate Side menu On SMS Page");
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");
		Common.pause(2);
		assertEquals(phoneApp2.validateSMSComposeButtonOnSMSPageIsDisplayed(), true,
				" --validate SMS Compose Button On SMS Page");
		assertEquals(phoneApp2.validateSubMenuDropdownOnSMSPage(), true, " --validate sub menu drop down On SMS Page");

		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message2, " --validate message On SMS Page");

		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), true,
				" --validate Callhippo Symbol Icon for unsaved number On SMS Page");

		assertEquals(phoneApp2.validateUnreadSMSOnSMSPage(number1), true, " --validate unread message On SMS Page");

		phoneApp2.clickOnSMSThreadOnSMSPage(number1);
		Common.pause(2);
		assertEquals(phoneApp2.validateNameOnSMSThread(), number1);
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message1), true,
				" --validate incoming Message On SMS Thread");
		assertEquals(phoneApp2.validateIncomingMessageOnSMSThread(message2), false,
				" --validate incoming Message On SMS Thread");

		// validate credit
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		String leftPanelcreditValAfter1 = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValAfter1, "0.00", "--validate 0 credit");
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueAfter1 = webApp2.availableCreditValue();
		assertEquals(settingcreditValueAfter1, "0.00", "-- validate 0 credit");

		assertEquals(leftPanelcreditValAfter1, settingcreditValueAfter1);
	}

	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Auto_recharge_functionality_with_Incoming_SMS() throws Exception {
		System.out.println("start test case...");
		 Common.pause(3);
		creditAPI.deleteRechargeActivity(userID);
		creditAPI.UpdateCredit("5.001", "0", "0", userID);
		Common.pause(3);
		logout(webApp2);
		loginWeb(webApp2, account2Email, account2Password);
		 Common.pause(3);
		creditWebSettingPage.clickLeftMenuPlanAndBilling();
		creditWebSettingPage.autoRechargeToggle("on");
		creditWebSettingPage.selectBalanceFallsBelow("5");
		creditWebSettingPage.selectRechargeCredit("100");
		creditWebSettingPage.clickSaveRechargeButton();
        assertEquals(webApp2.validationMessage(), "Auto recharge value changed successfully");
        
        Common.pause(2);
		String settingcreditValueBefore = creditWebSettingPage.availableCreditValue();
		double charge = Double.parseDouble(smsCost1);
		float newCharge = new Float(charge);
		String settingcreditValueAfter = creditWebSettingPage
				.validateCreditAddedSuccessfullySMS(settingcreditValueBefore, "100", newCharge);
         System.out.println("----settingcreditValueAfter"+settingcreditValueAfter);
		driverphoneApp1.get(url.dialerSMSPage());
		phoneApp1.clickOnComposeMessageButton();
		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneApp1.ValidateNameInNumberField(acc2Number1);
		phoneApp1.clickOnSendMessageButton();
		assertEquals("Message Sent Successfully", phoneApp1.validationMessage());
		
		Common.pause(2);
		creditWebSettingPage.clickLeftMenuDashboard();
		Common.pause(2);
		creditWebSettingPage.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = creditWebSettingPage.availableCreditValue();
		System.out.println("----settingcreditValueUpdated"+settingcreditValueUpdated);

		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost1, "---validate SMS cost");

	}

	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_New_signup_with_bronze_monthly() throws Exception {
		System.out.println("start test case...");
		registrationPage1.deleteAllMailForSMS();
		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverwebMail.get(url.numbersPage());
		Common.pause(2);
		addNumberPage1.clickOnAddNumberInNumbersPage();
		Common.pause(2);
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		Common.pause(2);
		addNumberPage1.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage1.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage1.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage1.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage1.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage1.clickOnCheckoutButton();
		
         String priceAfterCheckout = addNumberPage1.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, "1"));
		assertTrue(addNumberPage1.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage1.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage1.addDetailsInCard();
		addNumberPage1.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webMail);
		Common.pause(3);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage1.loginSuccessfully());
		assertEquals(addNumberPage1.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage1.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		String Url1 = driverwebMail.getCurrentUrl();
		userIDwebMail = webMail.getUserId(Url1);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 3. invite 2 sub users
		webMail.userspage();
		inviteUserPageObj1.clickOnInviteUserButton();
		String date = excel.date();
		Common.pause(1);
		String date1 = excel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj1.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj1.enterInviteUserEmail(email2);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj1.clickInviteNUserButton();

		// process the payment
		webMail.clickonyesbutton();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj1.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webMail);
		Common.pause(20);

		String invitedUserName1 = "Invited User1";
		String invitedUserName2 = "Invited User2";

		registrationPage1.validateSecondLastInvitationButton();
		inviteUserPageObj1.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj1.enterSubUserNewPassword(excel.getdata(0, 27, 2));
		inviteUserPageObj1.clickOnSubUserSubmitButton();
		registrationPage1.verifyWelcomeToCallHippoPopup();
		logout(webMail);
		Common.pause(30);

		registrationPage1.OpenInvitationMailForSecondUser();
		inviteUserPageObj1.enterSubUserFullName(invitedUserName2);
		inviteUserPageObj1.enterSubUserNewPassword(excel.getdata(0, 27, 2));
		inviteUserPageObj1.clickOnSubUserSubmitButton();
		registrationPage1.verifyWelcomeToCallHippoPopup();
		logout(webMail);
		Common.pause(2);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage1.closeAddnumberPopup();
		Common.pause(1);
		webMail.userspage();
		Common.pause(3);

		// verify both invited user are active
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email1), true);
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email2), true);

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println(" mainuser after invite 2 user:");
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 3. delete subuser2;
		webMail.userspage();
		Common.pause(1);
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email2), true);
		inviteUserPageObj1.deleteInvitedSubUser(email2);
		Common.pause(1);
		assertEquals(inviteUserPageObj1.verifyDeleteUserPopup(), true);
		inviteUserPageObj1.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj1.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		// after delete subuser1 validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println("first sms count:" + webMail.getTextSMSLimit());
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		//seat count need to  verify
		addNumberPage1.openAccountDetailsPopup();
		Common.pause(2);
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		
		String actualPriceFromManageSubscription1 = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("2")))); 
		
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterDeletedOneUser = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterDeletedOneUser, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("2")), "validate seat count after deleted 1 sub users");
		String seatiTagValue3 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue3,
				"The charges will be calculated based on the total seats.");
		System.out.println(" mainuser after delete one subuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
	
         //4.add one number
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased1 = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased1);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		 webMail.clickOnYesButton();
	
        //after add one number validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println("first sms count:" + webMail.getTextSMSLimit());
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		System.out.println(" mainuser after  add one number:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
	
	
	}
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_New_signup_with_bronze_anually() throws Exception {
		System.out.println("start test case...");
		registrationPage1.deleteAllMailForSMS();
		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		addNumberPage1.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage1.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage1.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));
		addNumberPage1.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage1.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage1.getSeatCount();
		assertEquals(seatValueInNumberPopup,data.getValue("seatcount"));
		addNumberPage1.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage1.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage1.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage1.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage1.addDetailsInCard();
		addNumberPage1.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webMail);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage1.loginSuccessfully());
		assertEquals(addNumberPage1.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage1.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually,data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");


		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		String Url1 = driverwebMail.getCurrentUrl();
		userIDwebMail = webMail.getUserId(Url1);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		 assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
			assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
			
		assertTrue(webMail.getTextSMSLimit().contains("200"));
       
		// 3. invite 2 sub users
		webMail.userspage();
		inviteUserPageObj1.clickOnInviteUserButton();
		String date = excel.date();
		Common.pause(1);
		String date1 = excel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj1.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj1.enterInviteUserEmail(email2);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj1.clickInviteNUserButton();

		// process the payment
		webMail.clickonyesbutton();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj1.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webMail);
		Common.pause(20);

		String invitedUserName1 = "Invited User1";
		String invitedUserName2 = "Invited User2";

		registrationPage1.validateSecondLastInvitationButton();
		inviteUserPageObj1.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj1.enterSubUserNewPassword(excel.getdata(0, 27, 2));
		inviteUserPageObj1.clickOnSubUserSubmitButton();
		registrationPage1.verifyWelcomeToCallHippoPopup();
		logout(webMail);
		Common.pause(30);

		registrationPage1.OpenInvitationMailForSecondUser();
		inviteUserPageObj1.enterSubUserFullName(invitedUserName2);
		inviteUserPageObj1.enterSubUserNewPassword(excel.getdata(0, 27, 2));
		inviteUserPageObj1.clickOnSubUserSubmitButton();
		registrationPage1.verifyWelcomeToCallHippoPopup();
		logout(webMail);
		Common.pause(2);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage1.closeAddnumberPopup();
		Common.pause(1);
		webMail.userspage();
		Common.pause(2);

		// verify both invited user are active
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email1), true);
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email2), true);

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println(" mainuser after invite 2 user:");
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 3. delete subuser2;
		webMail.userspage();
		Common.pause(1);
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email2), true);
		inviteUserPageObj1.deleteInvitedSubUser(email2);
		Common.pause(1);
		assertEquals(inviteUserPageObj1.verifyDeleteUserPopup(), true);
		inviteUserPageObj1.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj1.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		// after delete subuser1 validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println("first sms count:" + webMail.getTextSMSLimit());
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		//seat count need to  verify
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterDeletedOneUser = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterDeletedOneUser, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("2")), "validate seat count after deleted 1 sub users");
		String seatiTagValue3 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue3,
				"The charges will be calculated based on the total seats.");
		System.out.println(" mainuser after delete one subuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
	
         //4.add one number
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased1 = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased1);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		 webMail.clickOnYesButton();
	
        //after add one number validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println("first sms count:" + webMail.getTextSMSLimit());
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		System.out.println(" mainuser after  add one number:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
	}
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_New_signup_with_silver_monthly() throws Exception {
		System.out.println("start test case...");
		registrationPage1.deleteAllMailForSMS();
		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		addNumberPage1.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage1.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);

		String seatiTagValueInNumberPopup = addNumberPage1.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage1.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage1.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage1.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage1.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage1.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage1.addDetailsInCard();
		addNumberPage1.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webMail);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage1.loginSuccessfully());
		webMail.clickLeftMenuPlanAndBilling();
		driverwebMail.navigate().to(url.signIn());
		assertEquals(addNumberPage1.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage1.getFreeIncomingMinutesFromDashboard(), addNumberPage1.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), "1"));
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly,data.getValue("seatcount")));

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		String Url1 = driverwebMail.getCurrentUrl();
		userIDwebMail = webMail.getUserId(Url1);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		
		assertTrue(webMail.getTextSMSLimit().contains("200"));
        assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 4. invite 1 sub users and validate Seat count and SMS count
		webMail.userspage();
		inviteUserPageObj1.clickOnInviteUserButton();
		String date = excel.date();
		Common.pause(1);
		String date1 = excel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj1.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);

		inviteUserPageObj1.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj1.clickInviteNUserButton();
		webMail.clickonyesbutton();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj1.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription1 = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("1"))));
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterAdded3rdUser = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterAdded3rdUser, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("1")), "validate seat count after invited 1 sub users");
		String seatiTagValue3 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue3,
				"The charges will be calculated based on the total seats.");

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		System.out.println("first sms count:" + webMail.getTextSMSLimit());

		System.out.println(" mainuser after invite 1 user:");
				assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
				assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
				assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
	}
	
	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_New_signup_with_silver_anually() throws Exception {
		System.out.println("start test case...");
		registrationPage1.deleteAllMailForSMS();
		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		addNumberPage1.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage1.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage1.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));

		String seatiTagValueInNumberPopup = addNumberPage1.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage1.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage1.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage1.getPlanPriceFromYourOrderPopup().replace(",", "");
		assertEquals(priceAfterCheckout, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage1.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		
		addNumberPage1.clickOnypProceedToCheckoutFromYourOrderPopup();
		Common.pause(2);
		addNumberPage1.addDetailsInCard();
		addNumberPage1.clickOnNotNowImDonePopup();
		Common.pause(7);
		logout(webMail);
		Common.pause(5);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage1.loginSuccessfully());
		webMail.clickLeftMenuPlanAndBilling();
		driverwebMail.navigate().to(url.signIn());
		assertEquals(addNumberPage1.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage1.getFreeIncomingMinutesFromDashboard(), addNumberPage1.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"), "1"));
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage1.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription, addNumberPage1.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually,data.getValue("seatcount")));

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		String Url1 = driverwebMail.getCurrentUrl();
		userIDwebMail = webMail.getUserId(Url1);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		assertTrue(webMail.getTextSMSLimit().contains("200"));
	    assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 3. invite 1 sub users and validate  seat count SMS count
		webMail.userspage();
		inviteUserPageObj1.clickOnInviteUserButton();
		String date = excel.date();
		Common.pause(1);
		String date1 = excel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj1.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj1.clickInviteNUserButton();
		
		// process the payment
		webMail.clickonyesbutton();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj1.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		logout(webMail);
		Common.pause(20);

		String invitedUserName1 = "Invited User1";
		
		registrationPage1.validateSecondLastInvitationButton();
		inviteUserPageObj1.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj1.enterSubUserNewPassword(excel.getdata(0, 27, 2));
		inviteUserPageObj1.clickOnSubUserSubmitButton();
		registrationPage1.verifyWelcomeToCallHippoPopup();
		logout(webMail);
		Common.pause(2);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage1.closeAddnumberPopup();
		Common.pause(1);
		webMail.userspage();
		Common.pause(2);

		// verify  invited user are active
		assertEquals(inviteUserPageObj1.verifyUserInvitedSuccessfully(email1), true);
	
		//  after invite one subuser verify seat count in plan and billing page
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("1")), "validate seat count after invite oneuser");			
			String seatiTagValue1 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
						"The charges will be calculated based on the total seats.");
		
		//verify sms count
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		webMail.clickOnSMSLimit();
		System.out.println("validation for mainuser after invite 1 subuser:");
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");	
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		  //4.add one number
			driverwebMail.get(url.numbersPage());
			addNumberPage1.clickOnAddNumberInNumbersPage();
			addNumberPage1.clickOnCountry("United States");
			String numberBeforePurchased1 = addNumberPage1.getFirstNumberOfNumberListPage();
			System.out.println(numberBeforePurchased1);
			addNumberPage1.clickOnFirstNumberOfNumberListPage();
			webMail.clickOnYesButton();

			// after add one number validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
			Common.pause(3);
			webMail.userspage();
			Common.pause(5);
			webMail.navigateToUserSettingPage(signUpUserEmailId);
			webMail.clickOnSMSLimit();
			System.out.println("validation for mainuser after add one number:");
			assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
			assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
			assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
	}
	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void verify_SMS_New_signup_with_Platinum_Monthly() throws Exception {
		registrationPage1.deleteAllMailForSMS();
		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		addNumberPage1.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage1.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		addNumberPage1.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage1.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage1.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage1.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage1.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage1.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage1.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage1.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage1.addDetailsInCard();
		addNumberPage1.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webMail);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage1.loginSuccessfully());
		webMail.clickLeftMenuPlanAndBilling();
		driverwebMail.navigate().to(url.signIn());
		assertEquals(addNumberPage1.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage1.getFreeIncomingMinutesFromDashboard(), addNumberPage1.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), "1"));
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage1.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		String Url1 = driverwebMail.getCurrentUrl();
		userIDwebMail = webMail.getUserId(Url1);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		System.out.println(webMail.getTextSMSLimit());
		assertTrue(webMail.getTextSMSLimit().contains("200"));
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 4. invite 1 sub users and validate seat count and SMS count in account details
		webMail.userspage();
		inviteUserPageObj1.clickOnInviteUserButton();
		String date = excel.date();
		Common.pause(1);

		String email1 = gmailUser + "+" + date + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj1.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj1.clickInviteNUserButton();
		webMail.clickonyesbutton();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj1.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription1 = addNumberPage1.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1, addNumberPage1.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("1"))));
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterAdded3rdUser = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterAdded3rdUser, Integer.toString(Integer.parseInt(data.getValue("seatcount")) +Integer.parseInt("1")), "validate seat count after invited 1 sub users");
		String seatiTagValue3 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue3,
				"The charges will be calculated based on the total seats.");

		Common.pause(3);
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,
				"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,
				"--validate free Incoming Sms mainuser:");
		assertTrue(webMail.getTextSMSLimit().contains("200"));

		// 4.add one number
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased1 = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased1);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		webMail.clickOnYesButton();

		// after add one number validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();
		System.out.println(" mainuser after  add one number:");
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,
				"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,
				"--validate free Incoming Sms mainuser:");

	}

	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SMS_New_signup_with_Platinum_anually() throws Exception {
		System.out.println("start test case...");
		registrationPage1.deleteAllMailForSMS();
		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverwebMail.get(url.numbersPage());
		addNumberPage1.clickOnAddNumberInNumbersPage();
		addNumberPage1.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage1.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage1.clickOnFirstNumberOfNumberListPage();
		addNumberPage1.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage1.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage1.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));
		addNumberPage1.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage1.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage1.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage1.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage1.getPlanPriceFromYourOrderPopup().replace(",", "");
		assertEquals(priceAfterCheckout, addNumberPage1.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage1.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage1.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage1.addDetailsInCard();
		addNumberPage1.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webMail);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage1.loginSuccessfully());
		webMail.clickLeftMenuPlanAndBilling();
		driverwebMail.navigate().to(url.signIn());
		assertEquals(addNumberPage1.getFreeIncomingMinutesFromDashboard(), addNumberPage1.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"), "1"));
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage1.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription, addNumberPage1.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		String Url1 = driverwebMail.getCurrentUrl();
		userIDwebMail = webMail.getUserId(Url1);
		System.out.println("userIDwebMail:" + userIDwebMail);
		webMail.clickOnSMSLimit();
		assertTrue(webMail.getTextSMSLimit().contains("200"));
        assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,"--validate free Incoming Sms mainuser:");
		
		// 3. update seat count grater than users added in account ,increase MRR seat count to 3 from setting page [Seat count = 3] 
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		inviteUserPageObj1.enterSeatCount("3");
		try {
			assertTrue(
					inviteUserPageObj1.validatePopupMessageWithChargeInBillingPage(
							planPricePlatinumAnnually),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			// assertEquals(addNumberPage1.roundof(inviteUserPageObj1.getChargesOfPopupMessageInBillingPage()),
			// planPrice.getField("annually", "select * from Sheet1 where
			// plan='Platinum'"));
		}
		inviteUserPageObj1.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj1.validateSuccessMessage(), "Total seat updated to 3");
		addNumberPage1.openAccountDetailsPopup();
		assertTrue(addNumberPage1.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription2 = addNumberPage1.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription2, addNumberPage1.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, "3"));
		driverwebMail.navigate().to(url.signIn());
		webMail.clickLeftMenuPlanAndBilling();
		String seatCountUpdated = inviteUserPageObj1.verifySeatCount();
		assertEquals(seatCountUpdated, "3", "validate seat count after updated seat count");
		String seatiTagValue4 = inviteUserPageObj1.getTotalSeatiTagValue();
		assertEquals(seatiTagValue4,
				"The charges will be calculated based on the total seats.");

		// after increase seat count validate mainUser smsLimit, freeoutgoingsms and freeincomingSms
		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		System.out.println("freeSms mainuser:" + creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"));
		webMail.clickOnSMSLimit();

		System.out.println(" mainuser after update seat count  :");
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,
				"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,
				"--validate free Incoming Sms mainuser:");

		// 4. invite 1 sub users and validate SMS count
		webMail.userspage();
		inviteUserPageObj1.clickOnInviteUserButton();
		String date = excel.date();
		Common.pause(1);
		String date1 = excel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj1.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj1.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj1.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj1.clickInviteNUserButton();

		// process the payment
		// webMail.clickonyesbutton();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj1.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		logout(webMail);
		Common.pause(20);

		String invitedUserName1 = "Invited User1";

		registrationPage1.validateSecondLastInvitationButton();
		inviteUserPageObj1.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj1.enterSubUserNewPassword(excel.getdata(0, 27, 2));
		inviteUserPageObj1.clickOnSubUserSubmitButton();
		registrationPage1.verifyWelcomeToCallHippoPopup();
		Common.pause(5);
		logout(webMail);
		loginWeb(webMail, signUpUserEmailId, excel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage1.closeAddnumberPopup();
		Common.pause(1);
		webMail.userspage();
		Common.pause(2);

		Common.pause(3);
		webMail.userspage();
		Common.pause(5);
		webMail.navigateToUserSettingPage(signUpUserEmailId);
		webMail.clickOnSMSLimit();
		System.out.println(" mainuser after invite 1 user:");
		assertTrue(webMail.getTextSMSLimit().contains("200"), "--validate sms limit.");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeSms"), 0,
				"--validate outgoing freeSms mainuser:");
		assertEquals(creditAPI.getAvailableSMSCredit(userIDwebMail, "freeIncomingSms"), 0,
				"--validate free Incoming Sms mainuser:");

	}

	@Test(priority = 28, retryAnalyzer = Retry.class)
	public void verify_SMS_from_blocked_number() throws Exception {
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2.clickbtnAddnumberBlackList();
		Common.pause(1);
		webApp2.enterNumberToAddInBlackList(number3);
		Common.pause(2);
		webApp2.clickbtnSaveAddnumberBlackList();
		assertEquals(webApp2.validateSuccessMessage(), "Number added To Blacklist successfully");
		
		System.out.println("start test case...");
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		phoneApp2.clickOnSMSCompose();
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS", "--validate SMS title On SMS Page");

		Common.pause(2);
	    phoneApp2.enterNumberOnSMSPage(number3);

		Common.pause(3);
		String message2 = "Automation1" + excel.randomPassword();
		phoneApp2.enterMessageOnSMSPage(message2);
		Common.pause(3);
		phoneApp2.clickOnSendMessageButton();
		assertEquals(phoneApp2.validationMessage(), "You have blocked this number");
	
	}
	@Test(priority = 29, retryAnalyzer = Retry.class)
	public void verify_SMS_from_allocated_and_deallocated_number_to_user() throws Exception {
		//allocated number to user
		System.out.println("start test case...");

		// ++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url = driverwebApp2.getCurrentUrl();
		userID = webApp2.getUserId(Url);
		System.out.println("userID:" + userID);
		creditAPI.updateSMSCredit(userID, "100", "100", "0", "100", "1000");

		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message1);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message1), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost1, "---validate SMS cost");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, smsCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, smsCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				smsCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				smsCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//deallocated number to user
		
		Common.pause(3);
		webApp2.clickLeftMenuSetting();
		webApp2.clickLeftMenuDashboard();
		assertEquals(webApp2.getFreeIncomingMinutesFromDashboard(), "0",
				"Validate Free Incoming Minutes on Dashboard.");
		String leftPanelcreditValBefore1 = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore1 = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore1, settingcreditValueBefore1);

		Common.pause(3);
		webApp1.clickLeftMenuSetting();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide1 = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide1 = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide1, settingcreditValueBeforeOutgoingSide1);

		Common.pause(3);
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number1);
		webApp2.userTeamAllocation("users");
		Common.pause(3);
	//	webApp2.deallocatenUserFromNumber();
		webApp2.DeselectSpecificUserOrTeam(callerName2, "Yes");
		Thread.sleep(1000);
		
		System.out.println("send Message form phoneapp1 to phoneapp2");
		driverphoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		Common.pause(2);
		phoneApp1.clickonsmspage();
		phoneApp1.clickOnSMSCompose();
		assertEquals(phoneApp1.validatenameOnNavigationTitle(), "SMS");

		phoneApp1.enterNumberOnSMSPage(acc2Number1);
		Common.pause(3);
		String message2 = "Automation2" + excel.randomPassword();
		phoneApp1.enterMessageOnSMSPage(message2);
		Common.pause(3);
		System.out.print(phoneApp1.ValidateNameInNumberField(acc2Number1));
		assertEquals(phoneApp1.ValidateNameInNumberField(acc2Number1), acc2Number1, " --validate number On SMS Thread");
		phoneApp1.clickOnSendMessageButton();
		assertEquals(phoneApp1.validationMessage(), "Message Sent Successfully");
		assertEquals(phoneApp1.validateNameOnSMSThread(), acc2Number1);
		assertEquals(phoneApp1.validateMessageOnSMSThread(message2), true, " --validate Message On SMS Thread");

		// check in activity feed on webApp2
		Common.pause(3);
		webApp2.numberspage();
		Common.pause(5);
		String smsToText1 = webApp2.getTextForSMSTo();

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		webApp2.clickOnSMSTabActivityFeed();
		assertEquals(webApp2.validateSMSType(), "Incoming", "---validate SMS Type");
		assertEquals(webApp2.validateSMSFrom(), number1, "---validate SMS From");
		assertEquals(webApp2.validateSMSTo(), smsToText1, "---validate SMS To");
		assertEquals(webApp2.validateSMSStatus(), "Unread", "---validate SMS Status");
		assertEquals(webApp2.validateSMSCost(), smsCost1, "---validate SMS cost");

		// check in activity feed on webApp1
		Common.pause(3);
		webApp1.numberspage();
		Common.pause(5);
		String smsfromText1 = webApp1.getTextForSMSTo();

		webApp1.clickOnActivitFeed();
		Common.pause(5);
		webApp1.clickOnSMSTabActivityFeed();
		assertEquals(webApp1.validateSMSType(), "Outgoing", "---validate SMS Type");
		assertEquals(webApp1.validateSMSFrom(), smsfromText1, "---validate SMS From");
		assertEquals(webApp1.validateSMSTo(), acc2Number1, "---validate SMS To");
		assertEquals(webApp1.validateSMSStatus(), "Sent", "---validate SMS Status");
		assertEquals(webApp1.validateSMSCost(), smsCost1, "---validate SMS cost");

		
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();
		Common.pause(2);
		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");
	    Common.pause(2);
		assertEquals(phoneApp2.validateConatctNameOnSMSPageIsDisplayed(number1), false," --validate contact number On SMS Page");
	
		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), false," --validate Callhippo Symbol Icon for unsaved number On SMS Page");
	
//		phoneApp2.ClickonSpecificContact(number1);
//		Common.pause(2);
//		assertEquals(phoneApp2.validateParticularMessageSMSchatIsDisplayed(message2), true," --validate Message On SMS Thread");
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Common.pause(3);
		driverwebApp2.get(url.signIn());
		String settingcreditValueAfter2 = webApp2.validateCreditDeduction(settingcreditValueAfter, smsCost1);
		String leftPanelcreditValueAfter2 = webApp2.validateCreditDeduction(leftPanelcreditValueAfter, smsCost1);
		Common.pause(2);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated2 = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated2 = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated2, leftPanelcreditValueAfter2);
		assertEquals(settingcreditValueUpdated2, settingcreditValueAfter2);

		Common.pause(3);
		driverwebApp1.get(url.signIn());
		String settingcreditValueAfter3 = webApp1.validateCreditDeduction(settingcreditValueAfter1,
				smsCost1);
		String leftPanelcreditValueAfter3 = webApp1.validateCreditDeduction(leftPanelcreditValueAfter1,
				smsCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated3 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated3 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated3, leftPanelcreditValueAfter3);
		assertEquals(settingcreditValueUpdated3, settingcreditValueAfter3);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		Common.pause(3);
		driverwebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number1);
		webApp2.userTeamAllocation("users");
	//	webApp2.allocatenUserFromNumber();
		Common.pause(2);
		webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
		Thread.sleep(1000);
	
		driverphoneApp2.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		phoneApp2.clickonsmspage();

		assertEquals(phoneApp2.validatenameOnNavigationTitle(), "SMS");
	
		assertEquals(phoneApp2.validateConatctNameOnSMSPage(number1), number1,
				" --validate contact number On SMS Page");
		assertEquals(phoneApp2.validateSMSOnSMSPage(number1), message2, " --validate message On SMS Page");

		assertEquals(phoneApp2.validateCallhippoSymbolIconOnSMSPage(number1), true,
				" --validate Callhippo Symbol Icon for unsaved number On SMS Page");
	}
	
}
