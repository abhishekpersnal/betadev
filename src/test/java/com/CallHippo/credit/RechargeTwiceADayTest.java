package com.CallHippo.credit;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;

public class RechargeTwiceADayTest {
	WebDriver driverWebApp1;
	WebDriver driverWebApp2;
	WebDriver driverPhoneApp1;

	static Common excel;
	PropertiesFile url;

	DialerLoginPage phoneAppMainuser;
	RestAPI creditAPI;

	WebToggleConfiguration webAppMainUserLogin;
	WebToggleConfiguration webAppSubUserLogin;
	DialerIndex phoneAppMainUserLogin;

	CreditWebSettingPage webAppMainUser;
	CreditWebSettingPage webAppSubuser;

	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	String account1Email = data.getValue("addCreditAccount1");
	String account1Password = data.getValue("masterPassword");
	String account1Subuser1 = data.getValue("addCreditAccount1SubUser1");

	String mainUserID;
	String subUserID;
	
	public RechargeTwiceADayTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(data.getValue("environment")+"\\Forgot Password.xlsx");
		creditAPI = new RestAPI();

	}

	@BeforeTest
	public void createSessions() throws IOException, InterruptedException {
		try {
			try {
				driverWebApp1 = TestBase.init2();
				driverWebApp1.get(url.signIn());
				Common.pause(4);
				webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
				webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}catch(Exception e) {
				driverWebApp1 = TestBase.init2();
				driverWebApp1.get(url.signIn());
				Common.pause(4);
				webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
				webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}
			try {
				driverWebApp2 = TestBase.init2();
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				webAppSubUserLogin = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
				webAppSubuser = PageFactory.initElements(driverWebApp2, CreditWebSettingPage.class);
				webAppSubUserLogin.verifyEmail();
				loginWeb(webAppSubUserLogin, account1Subuser1, account1Password);
			}catch(Exception e) {
				driverWebApp2 = TestBase.init2();
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				webAppSubUserLogin = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
				webAppSubuser = PageFactory.initElements(driverWebApp2, CreditWebSettingPage.class);
				webAppSubUserLogin.verifyEmail();
				loginWeb(webAppSubUserLogin, account1Subuser1, account1Password);
			}
			try {
				driverPhoneApp1 = TestBase.init_dialer();
				driverPhoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				phoneAppMainUserLogin = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);
				loginDialer(phoneAppMainUserLogin, account1Email, account1Password);
				phoneAppMainUserLogin.waitForDialerPage();
				driverPhoneApp1.get(url.dialerCreditPage());
				Common.pause(3);
			}catch(Exception e) {
				driverPhoneApp1 = TestBase.init_dialer();
				driverPhoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				phoneAppMainUserLogin = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);
				loginDialer(phoneAppMainUserLogin, account1Email, account1Password);
				phoneAppMainUserLogin.waitForDialerPage();
				driverPhoneApp1.get(url.dialerCreditPage());
				Common.pause(3);
			}
			
			webAppMainUserLogin.userspage();
			webAppMainUserLogin.navigateToUserSettingPage(account1Email);
			Thread.sleep(9000);
			String Url = driverWebApp1.getCurrentUrl();
			mainUserID = webAppMainUserLogin.getUserId(Url);
			
			webAppSubUserLogin.userspage();
			webAppSubUserLogin.navigateToUserSettingPage(account1Subuser1);
			Thread.sleep(9000);
			String Url1 = driverWebApp2.getCurrentUrl();
			subUserID = webAppSubUserLogin.getUserId(Url1);
			
		} catch (Exception e) {
			e.printStackTrace();
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","RechargeTwiceADayTest - Before Test - createSessions");
			Common.Screenshot(driverWebApp2," WebApp2 Fail ","RechargeTwiceADayTest - Before Test - createSessions");
			Common.Screenshot(driverPhoneApp1," driverPhoneApp1 Fail ","RechargeTwiceADayTest - Before Test - createSessions");
			System.out.println("----Need to check issue - RechargeTwiceADayTest - Before Test - createSessions----");
		}
		

	}

	@AfterTest(alwaysRun = true)
	public void endSessions() {
		driverWebApp1.quit();
		driverWebApp2.quit();
		driverPhoneApp1.quit();

	}

	@BeforeMethod
	public void initialization() throws IOException, InterruptedException {
		try {
			driverWebApp1.get(url.signIn());
			driverWebApp2.get(url.signIn());
			
			if (webAppMainUserLogin.validateWebAppLoggedinPage() == true ) {
				 loginWeb(webAppMainUserLogin, account1Email, account1Password);
				 Thread.sleep(9000);
			}
			if (webAppMainUserLogin.validateWebAppLoggedinPage() == true ) {
				loginWeb(webAppMainUserLogin, account1Subuser1, account1Password);
				 Thread.sleep(9000);
			}
			
			if (phoneAppMainUserLogin.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneAppMainUserLogin, account1Email, account1Password);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","RechargeTwiceADayTest - Before method - initialization");
			Common.Screenshot(driverWebApp2," WebApp2 Fail ","RechargeTwiceADayTest - Before method - initialization");
			Common.Screenshot(driverPhoneApp1," driverPhoneApp1 Fail ","RechargeTwiceADayTest - Before method - initialization");
			System.out.println("----Need to check issue - RechargeTwiceADayTest - Before method - initialization----");
		}
		
		creditAPI.deleteRechargeActivity(mainUserID);
		creditAPI.deleteRechargeActivity(subUserID);

	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, " Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, " Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void recharge_thrice_a_day_in_Web_Main_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppMainUser.errorMessage());
		webAppMainUser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void recharge_thrice_a_day_in_Web_sub_user() {
		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppSubuser.successMessage());
		webAppSubuser.waitUntilInvisibleSucessMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppSubuser.successMessage());
		webAppSubuser.waitUntilInvisibleSucessMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppSubuser.errorMessage());
		webAppSubuser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void recharge_thrice_a_day_in_Dialer_main_user() throws InterruptedException {

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("You can't recharge thrice in a day", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_main_user_2nd_sub_user_3rd_main_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppSubuser.successMessage());
		webAppSubuser.waitUntilInvisibleSucessMessage();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppMainUser.errorMessage());
		webAppMainUser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_main_user_2nd_sub_user_3rd_sub_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppSubuser.successMessage());
		webAppSubuser.waitUntilInvisibleSucessMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppSubuser.errorMessage());
		webAppSubuser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_sub_user_2nd_sub_user_3rd_main_user() {
		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppSubuser.successMessage());
		webAppSubuser.waitUntilInvisibleSucessMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppSubuser.successMessage());
		webAppSubuser.waitUntilInvisibleSucessMessage();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppMainUser.errorMessage());
		webAppMainUser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_main_user_2nd_dialer_main_user_3rd_main_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppMainUser.errorMessage());
		webAppMainUser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_main_user_2nd_dialer_main_user_3rd_sub_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppSubuser.errorMessage());
		webAppSubuser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_main_user_2nd_dialer_main_user_3rd_dialer_main_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("You can't recharge thrice in a day", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();
	}

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_sub_user_2nd_dialer_main_user_3rd_main_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("You can't recharge thrice in a day", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();
	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_sub_user_2nd_dialer_main_user_3rd_sub_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppSubuser.errorMessage());
		webAppSubuser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_sub_user_2nd_dialer_main_user_3rd_dialer_main_user() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("You can't recharge thrice in a day", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();
	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_dialer_main_user_2nd_dialer_main_user_3rd_main_user() {

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

//		
		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppMainUser.errorMessage());
		webAppMainUser.waitUntilInvisibleErrorMessage();
	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void first_recharge_dialer_main_user_2nd_dialer_main_user_3rd_sub_user() {

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainuser.clickOnAddCreditButton();
		phoneAppMainuser.clickOnYesButton();
		assertEquals("$30 Credits successfully added to your account", phoneAppMainuser.validationMessage());
		phoneAppMainuser.invisibleValidationMessage();

		webAppSubuser.clickLeftMenuPlanAndBilling();
		webAppSubuser.selectAddCreditDropdownValue("30");
		webAppSubuser.clickAddCreditButton();
		webAppSubuser.clickOnYesButtonValidation();
		assertEquals("You can't recharge thrice in a day", webAppSubuser.errorMessage());
		webAppSubuser.waitUntilInvisibleErrorMessage();
	}
	
	
	
	

}
