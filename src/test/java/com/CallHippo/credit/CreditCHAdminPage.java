package com.CallHippo.credit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.PropertiesFile;

public class CreditCHAdminPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	WebDriver driver1;
	PropertiesFile credential;

	@FindBy(xpath = "//input[@id='email']")
	private WebElement email;
	@FindBy(xpath = "//button[contains(.,'SUBMIT')]")
	private WebElement submitButton;
	@FindBy(xpath = "//input[@id='password']")
	private WebElement otp;
	@FindBy(xpath = "//button[contains(.,'Login')]")
	private WebElement loginButton;
	@FindBy(xpath = "//a[contains(.,'User Profile')]")
	private WebElement userProfile;
	@FindBy(xpath = "//input[@id='autoCompleteAdmins_value']")
	private WebElement userSearchBox;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Search')]")
	private WebElement searchButton;
	@FindBy(xpath = "//div[@class='dataRow'][2]/div[@class='dataCell ng-binding'][3]")
	private WebElement credit;
	@FindBy(xpath = "//input[@id='creditsToAdd']")
	private WebElement addCreditTextBox;
	@FindBy(xpath = "//input[@id='creditAddReason']")
	private WebElement addCreditReasion;
	@FindBy(xpath = "//button[contains(.,'Add Credits')]")
	private WebElement addCreditButton;

	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	private WebElement validationMessage;

	public CreditCHAdminPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 30);
		action = new Actions(this.driver);

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public void loginToCHAdmin() {
		enterEmail("qa@callhippo.com");
		clickOnSubmitButton();
		enterOTP("1004");
		clickOnLoginButton();

	}

	public void enterEmail(String email) {
		wait.until(ExpectedConditions.visibilityOf(this.email));
		this.email.sendKeys(email);
	}

	public void clickOnSubmitButton() {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.click();
	}

	public void enterOTP(String otp) {
		wait.until(ExpectedConditions.visibilityOf(this.otp));
		this.otp.sendKeys(otp);
	}

	public void clickOnLoginButton() {
		wait.until(ExpectedConditions.elementToBeClickable(loginButton));
		loginButton.click();
	}

	public void clickOnUserProfileLink() {
		wait.until(ExpectedConditions.elementToBeClickable(userProfile));
		userProfile.click();
	}

	public void enterUserInSearchBox(String userSearch) {
		wait.until(ExpectedConditions.visibilityOf(userSearchBox));
		userSearchBox.sendKeys(userSearch);
		action.sendKeys(userSearchBox, Keys.ARROW_DOWN).build().perform();
		action.sendKeys(userSearchBox, Keys.ENTER).build().perform();
	}

	public void clickonSearchButton() {
		wait.until(ExpectedConditions.elementToBeClickable(searchButton));
		searchButton.click();
	}

	public String availableCredit() {
		wait.until(ExpectedConditions.visibilityOf(credit));
		return credit.getText().substring(0, credit.getText().indexOf(".") + 3);

	}

	public void enterCredit(String credit) {
		wait.until(ExpectedConditions.visibilityOf(addCreditTextBox)).sendKeys(credit);
	}

	public void enterCreditReasion(String reasion) {
		wait.until(ExpectedConditions.visibilityOf(addCreditReasion)).sendKeys(reasion);
	}

	public void clickOnAddCreditButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addCreditButton)).click();
	}

	public String validationMessage() {
		return wait.until(ExpectedConditions.visibilityOf(validationMessage)).getText();
	}
	
	public void addCredit(String credit) {
		enterCredit(credit);
		enterCreditReasion("test");
		clickOnAddCreditButton();
	}
	

}
