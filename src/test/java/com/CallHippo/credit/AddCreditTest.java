package com.CallHippo.credit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.authentication.LoginPage;

public class AddCreditTest {

	WebDriver driverWebApp1;
	WebDriver driverWebApp2;
	static Common excel;
	PropertiesFile url;
	//DialerLoginPage loginpage;
	RestAPI creditAPI;
	WebToggleConfiguration webAppMainUserLogin;
	CreditWebSettingPage webAppMainUser;
	DialerIndex phoneAppMainUserLogin;
	DialerLoginPage phoneAppMainuser;
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");
	String userIDWebApp1;
	
	String account1Email = data.getValue("addCreditAccount1");
	String account1Password = data.getValue("masterPassword");
	
	public AddCreditTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(data.getValue("environment")+"\\Forgot Password.xlsx");
		creditAPI= new RestAPI();
		
	}
	
	@BeforeTest
	public void createSessions() throws IOException {
		try {
			driverWebApp1 = TestBase.init2();
			driverWebApp1.get(url.signIn());
			Common.pause(4);
			webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
			webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
			phoneAppMainUserLogin = PageFactory.initElements(driverWebApp1,DialerIndex.class);
			try {
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}catch(Exception e) {
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}
			
			try {
				Common.pause(3);
				webAppMainUserLogin.userspage();
				webAppMainUserLogin.navigateToUserSettingPage(account1Email);
				Thread.sleep(9000);

				String Url1 = driverWebApp1.getCurrentUrl();
				userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);
				
				creditAPI.deleteRechargeActivity(userIDWebApp1);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(3);
				webAppMainUser.scrollToBillingAndCardTxt();
				Common.pause(3);
				webAppMainUser.updateCardDetailWithSuccessCard();
				Common.pause(3);
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				webAppMainUser.scrollToYourPlanTxt();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				webAppMainUser.selectAddCreditDropdownValue("30");
				webAppMainUser.clickAddCreditButton();
				webAppMainUser.clickOnYesButtonValidation();
				assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
				//webAppMainUser.successMessage();
				Common.pause(2);
				
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
			} catch (Exception e) {
				Common.pause(3);
				webAppMainUserLogin.userspage();
				webAppMainUserLogin.navigateToUserSettingPage(account1Email);
				Thread.sleep(9000);

				String Url1 = driverWebApp1.getCurrentUrl();
				userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);
				
				creditAPI.deleteRechargeActivity(userIDWebApp1);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(3);
				webAppMainUser.scrollToBillingAndCardTxt();
				Common.pause(3);
				webAppMainUser.updateCardDetailWithSuccessCard();
				Common.pause(3);
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				webAppMainUser.scrollToYourPlanTxt();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				webAppMainUser.selectAddCreditDropdownValue("30");
				webAppMainUser.clickAddCreditButton();
				webAppMainUser.clickOnYesButtonValidation();
				assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
				//webAppMainUser.successMessage();
				Common.pause(2);
				
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
			}
		} catch (Exception e) {
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","AddCreditTest - Before Test - createSessions");
			System.out.println("----Need to check issue - AddCreditTest - Before Test - createSessions----");
		} 
	}
	
	@AfterTest(alwaysRun = true)
	public void endSessions() {
		driverWebApp1.quit();
	}
	
	
	@BeforeMethod
	public void initialization() throws IOException, InterruptedException {
		
		try {
			 //driverWebApp1.get(url.signIn());
			 Common.pause(3);
			 
			if (webAppMainUserLogin.validateWebAppLoggedinPage() == true ) {
				 loginWeb(webAppMainUserLogin, account1Email, account1Password);
				 Thread.sleep(9000);
			}
		} catch (Exception e) {
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","AddCreditTest - Before method - initialization");
			System.out.println("----Need to check issue - AddCreditTest - Before method - initialization----");
		} 
		Common.pause(3);
		webAppMainUserLogin.userspage();
		webAppMainUserLogin.navigateToUserSettingPage(account1Email);
		Thread.sleep(9000);

		String Url1 = driverWebApp1.getCurrentUrl();
		userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
		System.out.println("userIDWebApp1:" + userIDWebApp1);
		
		creditAPI.deleteRechargeActivity(userIDWebApp1);
		
	
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		
	}
	
	
	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}
	
	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}


	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_credit_by_30() {
		
		String leftPanelcreditValBefore = webAppMainUser.getCreditValueFromLeftPanel();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueBefore----"+settingcreditValueBefore);
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("30");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfully(settingcreditValueBefore, "30");
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueAfter----"+settingcreditValueAfter);
		System.out.println("-----settingcreditValueUpdated----"+settingcreditValueUpdated);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_credit_by_50() {
		
		String leftPanelcreditValBefore = webAppMainUser.getCreditValueFromLeftPanel();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webAppMainUser.selectAddCreditDropdownValue("50");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("50");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$50 credits is successfully added to your account.", webAppMainUser.successMessage());
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfully(settingcreditValueBefore, "50");
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_credit_by_100() {
		
		String leftPanelcreditValBefore = webAppMainUser.getCreditValueFromLeftPanel();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webAppMainUser.selectAddCreditDropdownValue("100");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("100");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$100 credits is successfully added to your account.", webAppMainUser.successMessage());
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfully(settingcreditValueBefore,"100");
		// assertTrue(webAppMainUser.validateCreditAddedSuccessfullyMessage("100"));
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_credit_by_200() {
		
		String leftPanelcreditValBefore = webAppMainUser.getCreditValueFromLeftPanel();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webAppMainUser.selectAddCreditDropdownValue("200");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("200");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$200 credits is successfully added to your account.", webAppMainUser.successMessage());
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfully(settingcreditValueBefore,"200");
		// assertTrue(webAppMainUser.validateCreditAddedSuccessfullyMessage("200"));
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_credit_by_500() {
		
		String leftPanelcreditValBefore = webAppMainUser.getCreditValueFromLeftPanel();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webAppMainUser.selectAddCreditDropdownValue("500");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("500");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$500 credits is successfully added to your account.", webAppMainUser.successMessage());
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfully(settingcreditValueBefore,"500");
		// assertTrue(webAppMainUser.validateCreditAddedSuccessfullyMessage("500"));
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_credit_by_1000() {
		
		String leftPanelcreditValBefore = webAppMainUser.getCreditValueFromLeftPanel();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		webAppMainUser.selectAddCreditDropdownValue("1000");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("1000");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$1000 credits is successfully added to your account.", webAppMainUser.successMessage());
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfully(settingcreditValueBefore,"1000");
		// assertTrue(webAppMainUser.validateCreditAddedSuccessfullyMessage("1000"));
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void check_validation_for_more_credit() {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		
		webAppMainUser.selectAddCreditDropdownValue("More");
		/*
		webAppMainUser.clickAddCreditButton();
		assertEquals(webAppMainUser.errorMessage(), "Please Enter credit value greater than $100");
		webAppMainUser.errorMessage();
		
		webAppMainUser.enterMoreCreditValue("0");
		webAppMainUser.clickAddCreditButton();
		assertEquals(webAppMainUser.validationMessage(),"Please Enter credit value greater than $100");
		webAppMainUser.invisibleValidationMessage();
		*/
		webAppMainUser.clearMoreCreditValue();
		webAppMainUser.enterMoreCreditValue("1");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals(webAppMainUser.errorMessage(),"Recharge amount should not be less than 10.");
		//webAppMainUser.invisibleErrorMessage();
		
		Common.pause(3);
		webAppMainUser.clearMoreCreditValue();
		webAppMainUser.enterMoreCreditValue("9");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals(webAppMainUser.errorMessage(),"Recharge amount should not be less than 10.");
		//webAppMainUser.invisibleErrorMessage();
		/*
		webAppMainUser.clearMoreCreditValue();
		webAppMainUser.enterMoreCreditValue("2001");
		webAppMainUser.clickAddCreditButton();
		assertEquals(webAppMainUser.errorMessage(),"Please Enter credit value upto $2000");
		webAppMainUser.errorMessage();
		*/
		Common.pause(3);
		webAppMainUser.clearMoreCreditValue();
		webAppMainUser.enterMoreCreditValue("2000");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$2000 credits is successfully added to your account.", webAppMainUser.successMessage());
		//assertTrue(webAppMainUser.validateCreditAddedSuccessfullyMessage("2000"));
		/*
		webAppMainUser.selectAddCreditDropdownValue("more");
		webAppMainUser.enterMinusValuseForMoreCredit();
		webAppMainUser.clickAddCreditButton();
		assertEquals(webAppMainUser.validationMessage(), "Please Enter credit value greater than $100");
		webAppMainUser.invisibleValidationMessage();
		*/
	}
	
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_add_credit_validation_for_TransactionError_card () {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(2);
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("30");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("Your Payment is declined", webAppMainUser.errorMessage());
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_add_credit_validation_for_InsufficiantBalance_card () {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithInsufficiantBalanceCard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(2);
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("30");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("Your Payment is declined", webAppMainUser.errorMessage());
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void web_add_credit_with_valid_card () {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(2);
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.validateAreYouSureMessage("30");
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		
	}
	

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Check_menu_button_at_credit_page_of_dialer () throws IOException {
		
		driverWebApp1.get(url.dialerSignIn());
		Common.pause(3);
		phoneAppMainUserLogin.waitForDialerPage();
		driverWebApp1.get(url.dialerCreditPage());
		Common.pause(1);
		
		phoneAppMainUserLogin.clickOnSideMenu();
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Dialpad"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Live Calls"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("All calls/Inbox"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Contacts"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("SMS"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Call Planner"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Settings"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Credits"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Leader Board"), true);
		assertEquals(phoneAppMainUserLogin.ValidateOnSideMenuBar("Feedback"), true);
		
}

	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void check_validation_credit_falls_below_5() throws IOException {
	System.out.println("start test");
		driverWebApp1.navigate().refresh();
		creditAPI.UpdateCredit("5.00", "0", "0",userIDWebApp1);
		
		Common.pause(5);
		logout(webAppMainUserLogin);
		Common.pause(5);
		loginWeb(webAppMainUserLogin, account1Email, account1Password);
		/*
		webAppMainUser.clickLeftMenuPlanAndBilling();
		driverWebApp1.navigate().refresh();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(5);
		driverWebApp1.get(url.signIn());
		driverWebApp1.navigate().refresh();
		*/
		Common.pause(5);
		webAppMainUser.clickLeftMenuDashboard();
		assertEquals(webAppMainUser.validateMessageOnceCreditFallsBelow5(), true);
	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_add_credit_by_30() throws IOException, InterruptedException {
		SoftAssert softAss = new SoftAssert();
		try {
		driverWebApp2 = TestBase.init2();
		driverWebApp2.get(url.signIn());
		Common.pause(4);
		webAppMainUserLogin = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		webAppMainUser = PageFactory.initElements(driverWebApp2, CreditWebSettingPage.class);
		phoneAppMainUserLogin = PageFactory.initElements(driverWebApp2,DialerIndex.class);
		try {
			webAppMainUserLogin.verifyEmail();
			loginWeb(webAppMainUserLogin, account1Email, account1Password);
		}catch(Exception e) {
			webAppMainUserLogin.verifyEmail();
			loginWeb(webAppMainUserLogin, account1Email, account1Password);
		}
		Common.pause(3);
		webAppMainUserLogin.userspage();
		webAppMainUserLogin.navigateToUserSettingPage(account1Email);
		Thread.sleep(9000);
		String Url1 = driverWebApp1.getCurrentUrl();
		userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
		System.out.println("userIDWebApp1:" + userIDWebApp1);
	
		driverWebApp2.get(url.dialerSignIn());
		Common.pause(3);
		phoneAppMainUserLogin.waitForDialerPage();
		driverWebApp2.get(url.dialerCreditPage());
		Common.pause(1);
		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		Common.pause(2);
		softAss.assertEquals("$30 Credits successfully added to your account", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		
		phoneAppMainUserLogin.selectAddCreditDropdownValue("50");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("$50 Credits successfully added to your account", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		
		creditAPI.deleteRechargeActivity(userIDWebApp1);
		Common.pause(2);
		phoneAppMainUserLogin.selectAddCreditDropdownValue("100");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("$100 Credits successfully added to your account", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		Common.pause(2);
		phoneAppMainUserLogin.selectAddCreditDropdownValue("200");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("$200 Credits successfully added to your account", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		driverWebApp2.quit();
		softAss.assertAll();
		}catch(AssertionError e) {
			driverWebApp2.quit();
			softAss.assertAll();
		}
	}
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_add_credit_validation_for_InsufficiantBalance_card () {
		SoftAssert softAss = new SoftAssert();
		try {
			driverWebApp2 = TestBase.init2();
			driverWebApp2.get(url.signIn());
			Common.pause(4);
			webAppMainUserLogin = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
			webAppMainUser = PageFactory.initElements(driverWebApp2, CreditWebSettingPage.class);
			phoneAppMainUserLogin = PageFactory.initElements(driverWebApp2,DialerIndex.class);
			try {
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}catch(Exception e) {
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}
			
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithInsufficiantBalanceCard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuDashboard();
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(2);

		driverWebApp2.get(url.dialerSignIn());
		Common.pause(3);
		phoneAppMainUserLogin.waitForDialerPage();
		driverWebApp2.get(url.dialerCreditPage());
		Common.pause(1);
	
		phoneAppMainUserLogin.selectAddCreditDropdownValue("30");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("Your Payment is declined", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		
		phoneAppMainUserLogin.selectAddCreditDropdownValue("50");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("Your Payment is declined", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		
		
		phoneAppMainUserLogin.selectAddCreditDropdownValue("100");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("Your Payment is declined", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		
		phoneAppMainUserLogin.selectAddCreditDropdownValue("200");
		phoneAppMainUserLogin.clickOnAddCreditButton();
		phoneAppMainUserLogin.clickOnConfirmPopupYesButton();
		softAss.assertEquals("Your Payment is declined", phoneAppMainUserLogin.validationMessage());
		phoneAppMainUserLogin.invisibleValidationMessage();
		softAss.assertAll();
		driverWebApp2.quit();
		}catch(Exception e) {
			driverWebApp2.quit();
		}
	}
		
}
