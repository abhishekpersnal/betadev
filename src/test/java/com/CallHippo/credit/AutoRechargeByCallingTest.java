package com.CallHippo.credit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class AutoRechargeByCallingTest {

	public static WebDriver driverWebApp1;
	public static WebDriver driverPhoneApp1;
	public static WebDriver driverPhoneApp2;
	public static WebDriver driverPhoneApp3;

	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerIndex phoneApp3;

	DialerLoginPage phoneAppMainuser;
	RestAPI creditAPI;
	WebToggleConfiguration webAppMainUserLogin; 
	CreditWebSettingPage webAppMainUser;
	HolidayPage webApp2HolidayPage;


	static Common excel;
	PropertiesFile url;
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	String account1Email = data.getValue("addCreditAccount1");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("addCreditAccount2");
	String account3Email = data.getValue("addCreditAccount1SubUser1");
	String number = data.getValue("addCreditAccount2Number1");
	String account1Number1 = data.getValue("addCreditAccount1Number1");
	String userIDWebApp1;
	Common excelTestResult;

	public AutoRechargeByCallingTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(data.getValue("environment")+"\\Forgot Password.xlsx");
		creditAPI = new RestAPI();

		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}

	@BeforeTest
	public void createSessions() throws IOException {
		try {
			try {
				driverWebApp1 = TestBase.init2();
				driverWebApp1.get(url.signIn());
				Common.pause(2);
				webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
				webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
				webApp2HolidayPage = PageFactory.initElements(driverWebApp1, HolidayPage.class);
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}catch(Exception e) {
				driverWebApp1.get(url.signIn());
				Common.pause(2);
				webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
				webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
				webApp2HolidayPage = PageFactory.initElements(driverWebApp1, HolidayPage.class);
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);	
			}
	
			try {
				driverPhoneApp1 = TestBase.init_dialer();
				driverPhoneApp1.get(url.dialerSignIn());
				Common.pause(2);
				phoneApp1 = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);
				loginDialer(phoneApp1, account1Email, account1Password);
				phoneApp1.waitForDialerPage();
			}catch(Exception e) {
				driverPhoneApp1 = TestBase.init_dialer();
				driverPhoneApp1.get(url.dialerSignIn());
				Common.pause(2);
				phoneApp1 = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);
				loginDialer(phoneApp1, account1Email, account1Password);
				phoneApp1.waitForDialerPage();
			}
	
			try {
				driverPhoneApp2 = TestBase.init_dialer();
				driverPhoneApp2.get(url.dialerSignIn());
				//Common.pause(2);
				phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp2, DialerLoginPage.class);
				loginDialer(phoneApp2, account2Email, account1Password);
				phoneApp2.waitForDialerPage();
			}catch(Exception e) {
				driverPhoneApp2 = TestBase.init_dialer();
				driverPhoneApp2.get(url.dialerSignIn());
				//Common.pause(2);
				phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp2, DialerLoginPage.class);
				loginDialer(phoneApp2, account2Email, account1Password);
				phoneApp2.waitForDialerPage();
			}
			
			try {
				driverPhoneApp3 = TestBase.init_dialer();
				driverPhoneApp3.get(url.dialerSignIn());
				//Common.pause(2);
				phoneApp3 = PageFactory.initElements(driverPhoneApp3, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp3, DialerLoginPage.class);
				loginDialer(phoneApp3, account3Email, account1Password);
				phoneApp3.waitForDialerPage();
			}catch(Exception e) {
				driverPhoneApp3 = TestBase.init_dialer();
				driverPhoneApp3.get(url.dialerSignIn());
				//Common.pause(2);
				phoneApp3 = PageFactory.initElements(driverPhoneApp3, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp3, DialerLoginPage.class);
				loginDialer(phoneApp3, account3Email, account1Password);
				phoneApp3.waitForDialerPage();
			}
	
			try {
				Common.pause(3);
				webAppMainUserLogin.userspage();
				webAppMainUserLogin.navigateToUserSettingPage(account1Email);
				Thread.sleep(9000);

				String Url1 = driverWebApp1.getCurrentUrl();
				userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);
				
				creditAPI.deleteRechargeActivity(userIDWebApp1);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(3);
				webAppMainUser.scrollToBillingAndCardTxt();
				Common.pause(3);
				webAppMainUser.updateCardDetailWithSuccessCard();
				Common.pause(3);
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				webAppMainUser.scrollToYourPlanTxt();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				webAppMainUser.selectAddCreditDropdownValue("30");
				webAppMainUser.clickAddCreditButton();
				webAppMainUser.clickOnYesButtonValidation();
				assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
				//webAppMainUser.successMessage();
				Common.pause(2);
				webAppMainUser.clickLeftMenuSetting();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
			} catch (Exception e) {
				Common.pause(3);
				webAppMainUserLogin.userspage();
				webAppMainUserLogin.navigateToUserSettingPage(account1Email);
				Thread.sleep(9000);

				String Url1 = driverWebApp1.getCurrentUrl();
				userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);
				
				creditAPI.deleteRechargeActivity(userIDWebApp1);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(3);
				webAppMainUser.scrollToBillingAndCardTxt();
				Common.pause(3);
				webAppMainUser.updateCardDetailWithSuccessCard();
				Common.pause(3);
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				webAppMainUser.scrollToYourPlanTxt();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				webAppMainUser.selectAddCreditDropdownValue("30");
				webAppMainUser.clickAddCreditButton();
				webAppMainUser.clickOnYesButtonValidation();
				assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
				//webAppMainUser.successMessage();
				Common.pause(2);
				webAppMainUser.clickLeftMenuSetting();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
			}
		} catch (Exception e) {
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","Before method - createSessions");
			Common.Screenshot(driverPhoneApp1, " PhoneApp1 Fail ","AutoRechargeByCallingTest - Before Test - createSessions");
			Common.Screenshot(driverPhoneApp2, " PhoneApp2 Fail ","AutoRechargeByCallingTest - Before Test - createSessions");
			Common.Screenshot(driverPhoneApp3, " PhoneApp3 Fail ","AutoRechargeByCallingTest - Before Test - createSessions");
			System.out.println("----Need to check issue - AutoRechargeByCallingTest - Before Test - createSessions----");
		} 
	}

	@AfterTest(alwaysRun = true)
	public void endSessions() {
		 driverWebApp1.quit();
		 driverPhoneApp1.quit();
		 driverPhoneApp2.quit();
		 driverPhoneApp3.quit();
	}

	@BeforeMethod
	public void initialization() throws IOException, InterruptedException {
		
		 try {
			 driverWebApp1.get(url.signIn());
			 driverPhoneApp1.get(url.dialerSignIn());
			 driverPhoneApp2.get(url.dialerSignIn());
			 driverPhoneApp3.get(url.dialerSignIn());
			 Common.pause(3);
			 
			if (webAppMainUserLogin.validateWebAppLoggedinPage() == true ) {
				 loginWeb(webAppMainUserLogin, account1Email, account1Password);
				 Thread.sleep(9000);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp1, account1Email, account1Password);
			}
				
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp2, account2Email, account1Password);
			}
				
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp3, account3Email, account1Password);
			}
		} catch (Exception e) {
			
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","Before method - initialization");
			Common.Screenshot(driverPhoneApp1, " PhoneApp1 Fail ","AutoRechargeByCallingTest - Before method - initialization");
			Common.Screenshot(driverPhoneApp2, " PhoneApp2 Fail ","AutoRechargeByCallingTest - Before method - initialization");
			Common.Screenshot(driverPhoneApp3, " PhoneApp3 Fail ","AutoRechargeByCallingTest - Before method - initialization");
			System.out.println("----Need to check issue - AutoRechargeByCallingTest - Before method - initialization----");
		} 
		 
		 
			Common.pause(3);
			webAppMainUserLogin.userspage();
			webAppMainUserLogin.navigateToUserSettingPage(account1Email);
			Thread.sleep(9000);

			String Url1 = driverWebApp1.getCurrentUrl();
			userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
			System.out.println("userIDWebApp1:" + userIDWebApp1);
			
			creditAPI.deleteRechargeActivity(userIDWebApp1);
		// driverPhoneApp1.get("https://jenkins-phone.callhippo.com/credits");

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		webAppMainUser.clickLeftMenuDashboard();

		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " WebApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, " PhoneApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp3, testname, " PhoneApp3 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " WebApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp3 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_calling_auto_recharge_Toggle_is_ON_falls_bellow_5_recharge_30_valid_card() throws Exception {
		System.out.println("welcome");

		creditAPI.UpdateCredit("5.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueBefore----" + settingcreditValueBefore);
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_calling_auto_recharge_Toggle_is_ON_falls_bellow_50_recharge_50_valid_card() throws Exception {
		System.out.println("welcome");

		creditAPI.UpdateCredit("50.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("50");
		webAppMainUser.selectRechargeCredit("50");
		webAppMainUser.clickSaveRechargeButton();

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueBefore----" + settingcreditValueBefore);
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"50", newCharge);
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_calling_auto_recharge_Toggle_is_OFF_falls_bellow_5_recharge_30_valid_card() throws Exception {
		System.out.println("welcome");

		creditAPI.UpdateCredit("5.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		webAppMainUser.autoRechargeToggle("off");

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}



	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void incoming_calling_auto_recharge_Toggle_is_ON_falls_bellow_5_recharge_30_valid_card() throws Exception {
		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		creditAPI.UpdateCredit("5.001", "0", "0");

		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(3);
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();


		creditAPI.UpdateCredit("5.001", "0", "0");
		webAppMainUser.clickLeftMenuPlanAndBilling();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp2.enterNumberinDialer(account1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);
		System.out.println("-----settingcreditValueUpdated----" + settingcreditValueUpdated);
		assertEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}


	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void auto_recharge_two_calls_hangUp_at_same_time() throws Exception {
		
		
		phoneApp2.waitForDialerPage();
		
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(3);
		webAppMainUser.scrollToBillingAndCardTxt();
		Common.pause(3);
		webAppMainUser.updateCardDetailWithSuccessCard();
		Common.pause(3);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
				
		System.out.println("welcome");

		creditAPI.UpdateCredit("5.001", "0", "0");
		
		Common.pause(1);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueBefore----" + settingcreditValueBefore);
		double charge = 0.040;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		logout(webAppMainUserLogin);
		Common.pause(3);
		loginWeb(webAppMainUserLogin, account2Email, account1Password);
		webAppMainUser.clickLeftMenuDashboard();
		driverWebApp1.get(url.numbersPage());
		webAppMainUserLogin.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webAppMainUserLogin.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webAppMainUserLogin.enterCallQueueDuration("300");

		logout(webAppMainUserLogin);
		Common.pause(3);
		loginWeb(webAppMainUserLogin, account1Email, account1Password);
		webAppMainUser.clickLeftMenuDashboard();
		
		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.validateDurationInCallingScreen(2);
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp3.validateCallingScreenOutgoingNumber());
		
		phoneApp3.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
		
	}

	@Test(enabled = false, priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void auto_recharge_while_previous_call_missed_activity_feed_number_XXXX_format() throws Exception {

		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		creditAPI.UpdateCredit("0", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		webAppMainUser.autoRechargeToggle("off");

		phoneApp2.enterNumberinDialer(account1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		Thread.sleep(9000);

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickOnActivityFeed();
		Common.pause(10);
		assertEquals(webAppMainUser.getNumberFromActivityFeed(), "+12-XXX-XXX-XXXX");

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		// webAppMainUser.autoRechargeToggle("off");

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp2.enterNumberinDialer(account1Number1);
		String departmentName_ = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName_2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		Thread.sleep(9000);

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickOnActivityFeed();
		Common.pause(10);
		assertEquals(webAppMainUser.getNumberFromActivityFeed(), number);
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_calling_auto_recharge_Toggle_is_ON_falls_bellow_5_recharge_30_invalid_card() throws Exception {
		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		creditAPI.UpdateCredit("5.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();

		//webAppMainUser.updateCardDetailWithTransactionErrorCard();
		//webAppMainUser.scrollToYourPlanTxt();

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_calling_auto_recharge_Toggle_is_OFF_falls_bellow_5_recharge_30_invalid_card()
			throws Exception {
		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		creditAPI.UpdateCredit("5.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		webAppMainUser.autoRechargeToggle("off");

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.015;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void incoming_calling_auto_recharge_Toggle_is_ON_falls_bellow_5_recharge_30_invalid_card() throws Exception {
		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(1);

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		webAppMainUser.scrollToYourPlanTxt();

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();

		webAppMainUser.clickLeftMenuDashboard();
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		creditAPI.UpdateCredit("5.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp2.enterNumberinDialer(account1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}

	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_call_auto_recharge_failed_due_to_previous_recharge_fail() throws Exception {

		// Auto recharge for failed transaction with invalid card 

		System.out.println("welcome");
		

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);

		creditAPI.UpdateCredit("5.001", "0", "0");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		webAppMainUser.scrollToYourPlanTxt();
		
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		// webAppMainUser.autoRechargeToggle("off");

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.015;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);

		// Auto recharge check for valid card details - failed due to previous transaction failed 

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		creditAPI.UpdateCredit("5.001", "0", "0");
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(1);
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();

		settingcreditValueBefore = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueBefore----" + settingcreditValueBefore);
		charge = 0.020;
		newCharge = new Float(charge);
		ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore, "30",
				newCharge);
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp1.enterNumberinDialer(number);
		departmentName = phoneApp1.validateDepartmentNameinDialpade();
		departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);

	}
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void incoming_call_auto_recharge_failed_due_to_previous_recharge_fail() throws Exception {
		// Auto recharge for failed transaction with invalid card 

		System.out.println("welcome");

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		creditAPI.UpdateCredit("5.001", "0", "0");
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(1);
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		// webAppMainUser.autoRechargeToggle("off");

		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = 0.015;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp2.enterNumberinDialer(account1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);

		// Auto recharge check for valid card details - failed due to previous transaction failed 

		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		creditAPI.UpdateCredit("5.001", "0", "0");
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(2);
		webAppMainUser.scrollToYourPlanTxt();
		Common.pause(1);
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();

		settingcreditValueBefore = webAppMainUser.availableCreditValue();
		System.out.println("-----settingcreditValueBefore----" + settingcreditValueBefore);
		charge = 0.020;
		newCharge = new Float(charge);
		ExpectedsettingcreditValue = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore, "30",
				newCharge);
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);

		phoneApp2.enterNumberinDialer(account1Number1);
		departmentName = phoneApp1.validateDepartmentNameinDialpade();
		departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}
	
	
}

