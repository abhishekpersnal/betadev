package com.CallHippo.credit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;

public class AutoRechargeBySMSTest {
	WebDriver driverWebApp1;
	WebDriver driverPhoneApp1;

	static Common excel;
	PropertiesFile url;

	DialerLoginPage phoneAppMainuser;
	RestAPI creditAPI;

	WebToggleConfiguration webAppMainUserLogin;
	DialerIndex phoneAppMainUserLogin;

	CreditWebSettingPage webAppMainUser;

	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	String account1Email = data.getValue("addCreditAccount1");
	String account1Password = data.getValue("masterPassword");
	String account2Number = data.getValue("addCreditAccount2Number1");
	String oneSMScharge = data.getValue("oneSMScharge");
	String userIDWebApp1;

	Common excelTestResult;

	public AutoRechargeBySMSTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(data.getValue("environment")+"\\Forgot Password.xlsx");
		creditAPI = new RestAPI(); 

		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}

	@BeforeTest
	public void createSessions() throws IOException {
		try {
			driverWebApp1 = TestBase.init2();
			try {
				
				driverWebApp1.get(url.signIn());
				Common.pause(4);
				webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
				webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}catch(Exception e) {
				driverWebApp1.get(url.signIn());
				Common.pause(4);
				webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
				webAppMainUser = PageFactory.initElements(driverWebApp1, CreditWebSettingPage.class);
				webAppMainUserLogin.verifyEmail();
				loginWeb(webAppMainUserLogin, account1Email, account1Password);
			}
			
			driverPhoneApp1 = TestBase.init_dialer();
			try {
				
				driverPhoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				phoneAppMainUserLogin = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);
				loginDialer(phoneAppMainUserLogin, account1Email, account1Password);
				phoneAppMainUserLogin.waitForDialerPage();
			}catch(Exception e) {
				driverPhoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				phoneAppMainUserLogin = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
				phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);
				loginDialer(phoneAppMainUserLogin, account1Email, account1Password);
				phoneAppMainUserLogin.waitForDialerPage();
			}
	
			try {
				Common.pause(3);
				webAppMainUserLogin.userspage();
				webAppMainUserLogin.navigateToUserSettingPage(account1Email);
				Thread.sleep(9000);

				String Url1 = driverWebApp1.getCurrentUrl();
				userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);

				creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "100", "100", "1000");
				
				creditAPI.deleteRechargeActivity(userIDWebApp1);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(3);
				webAppMainUser.scrollToBillingAndCardTxt();
				Common.pause(3);
				webAppMainUser.updateCardDetailWithSuccessCard();
				Common.pause(3);
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				webAppMainUser.scrollToYourPlanTxt();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				webAppMainUser.selectAddCreditDropdownValue("30");
				webAppMainUser.clickAddCreditButton();
				webAppMainUser.clickOnYesButtonValidation();
				assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
				Common.pause(2);
				//webAppMainUser.successMessage();
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
			} catch (Exception e) {
				Common.pause(3);
				webAppMainUserLogin.userspage();
				webAppMainUserLogin.navigateToUserSettingPage(account1Email);
				Thread.sleep(9000);

				String Url1 = driverWebApp1.getCurrentUrl();
				userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
				System.out.println("userIDWebApp1:" + userIDWebApp1);

				creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "100", "100", "1000");
				
				creditAPI.deleteRechargeActivity(userIDWebApp1);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(3);
				webAppMainUser.scrollToBillingAndCardTxt();
				Common.pause(3);
				webAppMainUser.updateCardDetailWithSuccessCard();
				Common.pause(3);
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
	
				webAppMainUser.clickLeftMenuPlanAndBilling();
				Common.pause(2);
				webAppMainUser.scrollToYourPlanTxt();
				Common.pause(2);
				webAppMainUser.clickLeftMenuPlanAndBilling();
				webAppMainUser.selectAddCreditDropdownValue("30");
				webAppMainUser.clickAddCreditButton();
				webAppMainUser.clickOnYesButtonValidation();
				assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
				Common.pause(2);
				//webAppMainUser.successMessage();
				webAppMainUser.clickLeftMenuDashboard();
				Common.pause(2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","AutoRechargeBySMSTest - Before Test - createSessions");
			Common.Screenshot(driverPhoneApp1, " PhoneApp1 Fail ","AutoRechargeBySMSTest - Before Test - createSessions");
			System.out.println("----Need to check issue - AutoRechargeBySMSTest - Before Test - createSessions----");
		}
	}

	@AfterTest(alwaysRun = true)
	public void endSessions() {
		driverWebApp1.quit();
		driverPhoneApp1.quit();

	}

	@BeforeMethod
	public void initialization() throws IOException, InterruptedException {

		
		try {
			driverPhoneApp1.get(url.dialerCreditPage());
			Common.pause(3);
			
			if (webAppMainUserLogin.validateWebAppLoggedinPage() == true ) {
				 loginWeb(webAppMainUserLogin, account1Email, account1Password);
				 Thread.sleep(9000);
			}
			
			if (phoneAppMainUserLogin.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneAppMainUserLogin, account1Email, account1Password);
			}
			Common.pause(3);
			webAppMainUserLogin.userspage();
			webAppMainUserLogin.navigateToUserSettingPage(account1Email);
			Thread.sleep(9000);

			String Url1 = driverWebApp1.getCurrentUrl();
			userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
			System.out.println("userIDWebApp1:" + userIDWebApp1);

			creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "100", "100", "1000");
			
			creditAPI.deleteRechargeActivity(userIDWebApp1);
			
		} catch (Exception e) {
			Common.pause(3);
			webAppMainUserLogin.userspage();
			webAppMainUserLogin.navigateToUserSettingPage(account1Email);
			Thread.sleep(9000);

			String Url1 = driverWebApp1.getCurrentUrl();
			userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
			System.out.println("userIDWebApp1:" + userIDWebApp1);

			creditAPI.updateSMSCredit(userIDWebApp1, "100", "100", "100", "100", "1000");
			
			creditAPI.deleteRechargeActivity(userIDWebApp1);
			
			e.printStackTrace();
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","AutoRechargeBySMSTest - Before method - initialization");
			Common.Screenshot(driverPhoneApp1, " PhoneApp1 Fail ","AutoRechargeBySMSTest - Before method - initialization");
			System.out.println("----Need to check issue - AutoRechargeBySMSTest - Before method - initialization ----");
		}
		
		
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " WebApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
			
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " WebApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp1 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		
		try {
			webAppMainUser.clickLeftMenuDashboard();
		} catch (Exception e) {
			e.printStackTrace();
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " WebApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, " PhoneApp1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		}

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}
	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_5_recharge_30() throws Exception {
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		Common.pause(3);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, account1Email, account1Password);
		 Common.pause(3);
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_5_recharge_50() throws Exception {
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("50");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"50", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_5_recharge_100() throws Exception {
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("100");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"100", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_5_recharge_200() throws Exception {
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("200");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"200", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_10_recharge_30() throws Exception {
		creditAPI.UpdateCredit("10.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("10");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_10_recharge_50() throws Exception {
		creditAPI.UpdateCredit("10.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("10");
		webAppMainUser.selectRechargeCredit("50");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"50", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_10_recharge_100() throws Exception {
		creditAPI.UpdateCredit("10.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("10");
		webAppMainUser.selectRechargeCredit("100");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"100", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_10_recharge_200() throws Exception {
		creditAPI.UpdateCredit("10.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("10");
		webAppMainUser.selectRechargeCredit("200");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"200", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_20_recharge_30() throws Exception {
		creditAPI.UpdateCredit("20.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("20");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_20_recharge_50() throws Exception {
		creditAPI.UpdateCredit("20.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("20");
		webAppMainUser.selectRechargeCredit("50");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"50", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_20_recharge_100() throws Exception {
		creditAPI.UpdateCredit("20.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("20");
		webAppMainUser.selectRechargeCredit("100");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"100", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_20_recharge_200() throws Exception {
		creditAPI.UpdateCredit("20.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("20");
		webAppMainUser.selectRechargeCredit("200");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"200", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_50_recharge_30() throws Exception {
		creditAPI.UpdateCredit("50.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("50");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_50_recharge_50() throws Exception {
		creditAPI.UpdateCredit("50.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("50");
		webAppMainUser.selectRechargeCredit("50");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"50", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_50_recharge_100() throws Exception {
		creditAPI.UpdateCredit("50.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("50");
		webAppMainUser.selectRechargeCredit("100");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"100", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_falls_bellow_50_recharge_200() throws Exception {
		creditAPI.UpdateCredit("50.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("50");
		webAppMainUser.selectRechargeCredit("200");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"200", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Auto_recharge_Toggle_is_OFF_falls_bellow_5_recharge_30() throws Exception {
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		webAppMainUser.autoRechargeToggle("off");
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void recharge_mannually_and_Auto_recharge_falls_bellow_5_recharge_30() throws Exception {
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void recharge_mannually_2_times_and_Auto_recharge_falls_bellow_5_recharge_30() throws Exception {
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
	}
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void long_Message_Auto_recharge_falls_bellow_10_recharge_200() throws InterruptedException, Exception {
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("10.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("10");
		webAppMainUser.selectRechargeCredit("200");
		webAppMainUser.clickSaveRechargeButton();
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = (Double.parseDouble(oneSMScharge)*2);
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"200", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		phoneAppMainUserLogin.enterMessageOnSMSPage("Test automation long message Test automation long message Test automation long message Test automation long message Test automation long message Test automation long message");
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void transaction_Error_card_Auto_recharge_falls_bellow_5_recharge_30() throws InterruptedException, IOException {
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.00", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToBillingAndCardTxt();
		Common.pause(1);
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		phoneAppMainUserLogin.enterMessageOnSMSPage("Test automation long message");
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void insufficiant_Balance_Error_card_Auto_recharge_falls_bellow_5_recharge_30() throws InterruptedException, IOException {
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.00", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToBillingAndCardTxt();
		Common.pause(1);
		webAppMainUser.updateCardDetailWithInsufficiantBalanceCard();
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.scrollToYourPlanTxt();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		phoneAppMainUserLogin.enterMessageOnSMSPage("Test automation long message");
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void previus_auto_recharge_fail_and_Auto_recharge_falls_bellow_5_recharge_30() throws Exception {
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		//webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		//webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		phoneAppMainUserLogin.enterMessageOnSMSPage("Test automation long message");
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		
		settingcreditValueBefore = webAppMainUser.availableCreditValue();
		charge = Double.parseDouble(oneSMScharge);;
		newCharge = new Float(charge);
		settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
	}
	
	
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void previus_auto_recharge_fail_and_recharge_mannually() throws Exception {
		Common.pause(3);
		webAppMainUserLogin.userspage();
		webAppMainUserLogin.navigateToUserSettingPage(account1Email);
		Common.pause(9);

		String Url1 = driverWebApp1.getCurrentUrl();
		userIDWebApp1 = webAppMainUserLogin.getUserId(Url1);
		System.out.println("userIDWebApp1:" + userIDWebApp1);
		creditAPI.deleteRechargeActivity(userIDWebApp1);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithTransactionErrorCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		//webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.autoRechargeToggle("on");
		webAppMainUser.selectBalanceFallsBelow("5");
		webAppMainUser.selectRechargeCredit("30");
		webAppMainUser.clickSaveRechargeButton();
		
		//webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		String settingcreditValueBefore = webAppMainUser.availableCreditValue();
		double charge = Double.parseDouble(oneSMScharge);;
		float newCharge = new Float(charge);
		String settingcreditValueAfter = webAppMainUser.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,"30", newCharge);
		
		
		driverPhoneApp1.get(url.dialerSMSPage());
		phoneAppMainUserLogin.clickOnComposeMessageButton();
		phoneAppMainUserLogin.enterNumberOnSMSPage(account2Number);
		Common.pause(3);
		String message1 = "Automation1" + excel.randomPassword();
		phoneAppMainUserLogin.enterMessageOnSMSPage(message1);
		Common.pause(3);
		phoneAppMainUserLogin.ValidateNameInNumberField(account2Number);
		phoneAppMainUserLogin.clickOnSendMessageButton();	
		assertEquals("Message Sent Successfully", phoneAppMainuser.validationMessage());
		
		
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		webAppMainUser.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webAppMainUser.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, settingcreditValueAfter);
		
		Common.pause(1);
		webAppMainUser.scrollToBillingAndCardTxt();
		webAppMainUser.updateCardDetailWithSuccessCard();
		webAppMainUser.clickLeftMenuDashboard();
		Common.pause(2);
		
		
		creditAPI.UpdateCredit("5.001", "0", "0",userIDWebApp1);
		
		webAppMainUser.clickLeftMenuPlanAndBilling();
		webAppMainUser.selectAddCreditDropdownValue("30");
		webAppMainUser.clickAddCreditButton();
		webAppMainUser.clickOnYesButtonValidation();
		assertEquals("$30 credits is successfully added to your account.", webAppMainUser.successMessage());
		webAppMainUser.waitUntilInvisibleSucessMessage();
		
	}
	
	

}
