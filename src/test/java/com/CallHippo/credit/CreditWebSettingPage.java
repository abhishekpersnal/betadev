package com.CallHippo.credit;

import static org.testng.Assert.expectThrows;

import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;

public class CreditWebSettingPage {

	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	JavascriptExecutor js;

	public CreditWebSettingPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 30);
		action = new Actions(this.driver);
		js = (JavascriptExecutor) driver; 
	}

	@FindBy(xpath = "//a[contains(.,'Dashboard')]")
	private WebElement left_panel_dashboard;
	
	@FindBy(xpath = "//h5[@class='titleboxdashicne'][contains(.,'Call Summary')]")
	private WebElement dashboard_callSummary_txt;

	@FindBy(xpath = "//span[@class='sidebarCreditSpan'][contains(.,'Credit:')]")
	private WebElement left_panel_total_credit;

	@FindBy(xpath = "//a[contains(.,'Setting')]")
	private WebElement leftMenu_setting;
	
	@FindBy(xpath = "//span[contains(.,'SDAP')]")
	private WebElement sdap_Txt;
	
	@FindBy(xpath = "//a[contains(.,'Plan and Billing')]")
	private WebElement leftMenu_Plan_Billing;

	@FindBy(xpath = "//div[contains(text(),'Available Credits')]/../div/span")
	private WebElement setting_available_credit;

	@FindBy(xpath = "//label[contains(.,'Credits:')]/../div/div[@role='combobox']")
	private WebElement click_addCredit_dropdown;
	
	@FindBy(xpath = "//div[@class[contains(.,'test_automation_setting_add_credit_dropdown')]]/div/ul")
	private WebElement select_addCredit_dropdown;

	@FindBy(xpath = "(//button[contains(.,'Add')])[1]")
	private WebElement addCredit_button;

	@FindBy(xpath = "//button[contains(@test-automation,'setting_auto_recharge_switch')]")
	private WebElement toggle_auto_recharge;

	@FindBy(xpath = "//input[@placeholder='Enter credit amount']")
	private WebElement enter_credit;

	@FindBy(xpath = "//input[contains(@class,'ng-not-empty')][@placeholder='Enter Credit']")
	private WebElement credit_entered;

	@FindBy(xpath = "(//label[contains(.,'When your balance falls')]//following-sibling::div/div)[1]")
	private WebElement click_balance_falls_below;
	
	@FindBy(xpath = "//div[@class[contains(.,'test_automation_setting_credit_below_dropdown')]]/div/ul")
	private WebElement select_balance_falls_below;

	@FindBy(xpath = "(//label[contains(.,'Recharge For')]//following-sibling::div/div)[1]")
	private WebElement click_recharge_credit;
	
	@FindBy(xpath = "//div[@class[contains(.,'test_automation_setting_recharge_for_dropdown')]]/div/ul")
	private WebElement select_recharge_credit;

	@FindBy(xpath = "//div[@class='planCreditDDWrapper']//button[contains(.,'Save')]")
	private WebElement save_recharge_button;

	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	private WebElement validationMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement errorMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span")
	WebElement infoMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	WebElement successMessage;

	@FindBy(xpath = "//div[@class='avcredits_disbox']/h3[@class='creditdisdispl']")
	private WebElement dialer_available_credit;

	@FindBy(xpath = "//a[contains(.,'Add Credits')]")
	private WebElement dialer_addCredit_button;

	@FindBy(xpath = "//button[contains(.,'Yes')]")
	private WebElement yesButton;

	@FindBy(xpath = "//button[contains(.,'No')]")
	private WebElement noButton;

	@FindBy(xpath = "(//h4[@class='modal-title ng-binding'][contains(.,'Add Credits')])[1]")
	private WebElement popUpAddCreditText;

	@FindBy(xpath = "//p[@ng-if='options.message']")
	private WebElement addCreditPopupConfirmationText;

	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	WebElement validateMessage;

	@FindBy(xpath = "//button[contains(.,'Update Credit Card')]")
	WebElement web_update_creditCard_button;

	@FindBy(xpath = "//span[@aria-label='Click to edit current payment'][contains(.,'Edit Payment Method')]")
	WebElement web_edit_payment_method;
	
	@FindBy(xpath = "//span[@class='cb-label'][contains(.,'Primary')]")
	WebElement Click_on_primary_card;

	@FindBy(xpath = "//input[@id='number']")
	WebElement web_creditCard_number;

	@FindBy(xpath = "//input[@id='cvv']")
	WebElement web_card_cvv_num;

	@FindBy(xpath = "//span[@class='cb-button__text'][contains(.,'Update')]")
	WebElement web_credit_update_button;

	@FindBy(xpath = "//i[contains(@class,'close-icon')]")
	WebElement card_update_popup_close_button;

	@FindBy(xpath = "//span[contains(.,'Your Plan')]")
	WebElement yourPlan_Txt;

	@FindBy(xpath = "//button[contains(.,'Complete authentication')]")
	WebElement completeAuthentication_button;

	@FindBy(xpath = "//h3[contains(.,'Billing & Cards')]")
	private WebElement billingAndCardText;

	@FindBy(xpath = "//span[contains(.,'You may miss an important call or get disconnected | You just have')]")
	WebElement credit_falls_below_validation_msg;

	@FindBy(xpath = "//a[@href='/activityFeed'][contains(.,'graphic_eqActivity Feed')]")
	WebElement activiryfeed;
	
	@FindBy(xpath = "(//span[contains(@test-automation,'activity_feed_client')])[1]") WebElement numberFromWebActivityFeed;

	public void clickLeftMenuDashboard() {
		wait.until(ExpectedConditions.elementToBeClickable(left_panel_dashboard));
		left_panel_dashboard.click();
		wait.until(ExpectedConditions.elementToBeClickable(dashboard_callSummary_txt));
		Common.pause(1);
	}

	public String getCreditValueFromLeftPanel() {
		wait.until(ExpectedConditions.visibilityOf(left_panel_total_credit));
		String creditTxt = left_panel_total_credit.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(".") + 3);
		System.out.println("++++++" + creditVal);
		return creditVal;
	}

	public String availableCreditValue() {
		String creditTxt = setting_available_credit.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(".") + 3);
		System.out.println("++++++" + creditVal);
		return creditVal;
	}

//	public void clickLeftMenuSetting() {
//		wait.until(ExpectedConditions.visibilityOf(leftMenu_setting));
//		leftMenu_setting.click();
//		wait.until(ExpectedConditions.elementToBeClickable(yourPlan_Txt));
//		Common.pause(1);
//		yourPlan_Txt.click();
//	}
	
	public void clickLeftMenuSetting() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_setting));
		leftMenu_setting.click();
		wait.until(ExpectedConditions.elementToBeClickable(sdap_Txt));
		Common.pause(1);
	}
	
	public void clickLeftMenuPlanAndBilling() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_Plan_Billing));
		leftMenu_Plan_Billing.click();
		wait.until(ExpectedConditions.elementToBeClickable(yourPlan_Txt));
		Common.pause(1);
		yourPlan_Txt.click();
	}
	
	

	public void selectAddCreditDropdownValue(String creditValue) {
		// wait.until(ExpectedConditions.visibilityOf(select_addCredit_dropdown));
		//Select credit = new Select(select_addCredit_dropdown);
		//credit.selectByValue(creditValue);
		//credit.selectByVisibleText(creditValue);
		wait.until(ExpectedConditions.visibilityOf(click_addCredit_dropdown));
		Common.pause(2);
		click_addCredit_dropdown.click();
		wait.until(ExpectedConditions.visibilityOf(select_addCredit_dropdown));
		Common.selectDropdownValue(select_addCredit_dropdown, creditValue); 
	}

	public void clearMoreCreditValue() {
		/*
		try {
			while (!enter_credit.getAttribute("value").isEmpty()) {
				enter_credit.sendKeys(Keys.BACK_SPACE);
				
				 * moreCreditlength = credit_entered.getAttribute("value").length(); if
				 * (moreCreditlength==0) { break; }
				 
			}
		} catch (Exception e) {

		}
		*/
		enter_credit.clear();
	}

	public void enterMoreCreditValue(String value) {
		wait.until(ExpectedConditions.elementToBeClickable(enter_credit));
		enter_credit.click();
		enter_credit.sendKeys(value);
	}

	public void enterMinusValuseForMoreCredit() {
		wait.until(ExpectedConditions.elementToBeClickable(enter_credit));
		enter_credit.click();
		for (int i = 0; i < 10; i++) {
			enter_credit.sendKeys(Keys.ARROW_DOWN);
		}
	}

	public void clickAddCreditButton() {
//		wait.until(ExpectedConditions.elementToBeClickable(enter_credit));
		wait.until(ExpectedConditions.elementToBeClickable(addCredit_button));
		addCredit_button.click();
	}

	/*
	public void autoRechargeToggle(String value) {

		if (value.contentEquals("on")) {

			if (toggle_auto_recharge.isSelected()) {

			} else {
				js.executeScript("$(\"[test-automation='setting_auto_recharge_switch']\").click();");
			}
		} else {
			if (toggle_auto_recharge.isSelected()) {
				js.executeScript("$(\"[test-automation='setting_auto_recharge_switch']\").click();");
			} else {

			}
		}
	}
	*/
	public void autoRechargeToggle(String value) {
		
		wait.until(ExpectedConditions.visibilityOf(toggle_auto_recharge));
		if (value.contentEquals("on")) {

			if (toggle_auto_recharge.getAttribute("aria-checked").equals("true")) {

			} else {
				Common.pause(5);
				toggle_auto_recharge.click();
			}
		} else {
			if (toggle_auto_recharge.getAttribute("aria-checked").equals("true")) {
				Common.pause(5);
				toggle_auto_recharge.click();
			} else {

			}
		}
	}
	

	public void selectBalanceFallsBelow(String creditValue) {
		//Select credit = new Select(balance_falls_below);
		//credit.selectByValue(creditValue);
		wait.until(ExpectedConditions.visibilityOf(click_balance_falls_below));
	    Common.pause(5);
		click_balance_falls_below.click();
		wait.until(ExpectedConditions.visibilityOf(select_balance_falls_below));
		Common.selectDropdownValue(select_balance_falls_below, creditValue); 
	}

	public void selectRechargeCredit(String creditValue) {
		//Select credit = new Select(select_recharge_credit);
		//credit.selectByValue(creditValue);
		wait.until(ExpectedConditions.visibilityOf(click_recharge_credit));
		click_recharge_credit.click();
		wait.until(ExpectedConditions.visibilityOf(select_recharge_credit));
		Common.selectDropdownValue(select_recharge_credit, creditValue);
	}

	public void clickSaveRechargeButton() {
		wait.until(ExpectedConditions.visibilityOf(save_recharge_button));
		wait.until(ExpectedConditions.elementToBeClickable(save_recharge_button));
		save_recharge_button.click();
	}

	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}
	
	public String successMessage() {
		wait.until(ExpectedConditions.visibilityOf(successMessage));
		Common.pause(2);
		return successMessage.getText();
	}
	
	public String errorMessage() {
		wait.until(ExpectedConditions.visibilityOf(errorMessage));
		Common.pause(2);
		return errorMessage.getText();
	}
	

	public void invisibleValidationMessage() {
//		wait.until(ExpectedConditions.invisibilityOf(validationMessage));
		boolean toggle = true;
		while (toggle) {
			try {
				validationMessage.click();
			} catch (Exception e1) {
				break;
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
			}
		}
	}
	
	public void waitUntilInvisibleSucessMessage() {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class[contains(.,'ant-message-error')]]/span")));
	}
	
	public void waitUntilInvisibleErrorMessage() {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class[contains(.,'ant-message-error')]]/span")));
	}
	

	public String checkValidationMessageForCredits(String creditValue) {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}

	public String verifyValidationForMoreCredits(String creditValue) {
		enter_credit.sendKeys(Keys.ARROW_UP);
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}

	public String getAvailableCredit() {
		wait.until(ExpectedConditions.visibilityOf(dialer_available_credit));
		String creditTxt = dialer_available_credit.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(".") + 3);
		return creditVal;
	}

	public void clickDialerAddCreditButton() {
		wait.until(ExpectedConditions.elementToBeClickable(dialer_addCredit_button));
		dialer_addCredit_button.click();
	}

	public void clickOnYesButtonValidation() {
		wait.until(ExpectedConditions.visibilityOf(yesButton));
		yesButton.click();
	}

	public void clickOnNoButtonValidation() {
		wait.until(ExpectedConditions.visibilityOf(noButton));
		noButton.click();
	}

	public String getCreditValueFromMessage() {
		String creditTxt = addCreditPopupConfirmationText.getText();
		String creditVal = creditTxt.substring(creditTxt.indexOf("$") + 1, creditTxt.indexOf(" ") + 3);
		System.out.println("++++++" + creditVal);
		return creditVal;
	}

	public boolean validateAreYouSureMessage(String value) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"(//p[contains(.,'Are you sure you want to add $" + value + " credits to your account ?')])[1]"))))
				.isDisplayed();
	}

	public String validateCreditAddedSuccessfully(String settingcreditValueBefore, String addCredit) {
		float floatSettingPageCreditVal;
		float floatAddCredit;

		if (settingcreditValueBefore.contains(",")) {
			settingcreditValueBefore = settingcreditValueBefore.replace(",", "");
			floatSettingPageCreditVal = Float.parseFloat(settingcreditValueBefore);
			floatAddCredit = Float.parseFloat(addCredit);
		} else {
			floatSettingPageCreditVal = Float.parseFloat(settingcreditValueBefore);
			floatAddCredit = Float.parseFloat(addCredit);
		}

		floatSettingPageCreditVal = floatSettingPageCreditVal + floatAddCredit;
		String SettingPageCreditValStr = String.format("%.2f", floatSettingPageCreditVal);

		if (SettingPageCreditValStr.length() >= 7) {

			String str1 = SettingPageCreditValStr.substring(SettingPageCreditValStr.indexOf(".") - 3,
					SettingPageCreditValStr.indexOf(".") + 3);
			String str2 = SettingPageCreditValStr.substring(0, SettingPageCreditValStr.indexOf(".") - 3);
			SettingPageCreditValStr = str2 + "," + str1;
		}
		return SettingPageCreditValStr;
	}

	public boolean validateCreditAddedSuccessfullyMessage(String value) {
		Common.pause(2);
		return wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[@class[contains(.,'ant-message-success')]]/span[contains(.,'$"
						+ value + " credits is successfully added to your account.')]"))))
				.isDisplayed();
	}

	public String validateCreditAddedSuccessfullySMS(String settingcreditValueBefore, String addCredit, float charge) {
		float floatSettingPageCreditVal;
		float floatAddCredit;

		if (settingcreditValueBefore.contains(",")) {
			settingcreditValueBefore = settingcreditValueBefore.replace(",", "");
			floatSettingPageCreditVal = Float.parseFloat(settingcreditValueBefore);
			floatAddCredit = Float.parseFloat(addCredit) - charge;
		} else {
			floatSettingPageCreditVal = Float.parseFloat(settingcreditValueBefore);
			floatAddCredit = Float.parseFloat(addCredit) - charge;
		}

		floatSettingPageCreditVal = floatSettingPageCreditVal + floatAddCredit;
		String SettingPageCreditValStr = String.format("%.2f", floatSettingPageCreditVal);

		if (SettingPageCreditValStr.length() >= 7) {

			String str1 = SettingPageCreditValStr.substring(SettingPageCreditValStr.indexOf(".") - 3,
					SettingPageCreditValStr.indexOf(".") + 3);
			String str2 = SettingPageCreditValStr.substring(0, SettingPageCreditValStr.indexOf(".") - 3);
			SettingPageCreditValStr = str2 + "," + str1;
		}
		return SettingPageCreditValStr;
	}

	public void switchToFrameUpdateCreditCard() {
		Common.pause(1);
		driver.switchTo().frame("cb-frame");
	}

	public void updateCardDetailWithSuccessCard() {
		wait.until(ExpectedConditions.elementToBeClickable(web_update_creditCard_button)).click();
		switchToFrameUpdateCreditCard();
		wait.until(ExpectedConditions.visibilityOf(Click_on_primary_card)).click();
		wait.until(ExpectedConditions.visibilityOf(web_edit_payment_method)).click();
		wait.until(ExpectedConditions.elementToBeClickable(web_creditCard_number)).click();
		web_creditCard_number.sendKeys("4242424242424242");
		web_card_cvv_num.sendKeys("123");
		wait.until(ExpectedConditions.elementToBeClickable(web_credit_update_button)).click();
		wait.until(ExpectedConditions.visibilityOf(web_edit_payment_method));
		card_update_popup_close_button.click();
		driver.switchTo().defaultContent();
	}

	public void updateCardDetailWithTransactionErrorCard() {
		wait.until(ExpectedConditions.elementToBeClickable(web_update_creditCard_button)).click();
		switchToFrameUpdateCreditCard();
		wait.until(ExpectedConditions.visibilityOf(Click_on_primary_card)).click();
		wait.until(ExpectedConditions.visibilityOf(web_edit_payment_method)).click();
		wait.until(ExpectedConditions.elementToBeClickable(web_creditCard_number)).click();
		web_creditCard_number.sendKeys("4000000000000341");
		web_card_cvv_num.sendKeys("123");
		wait.until(ExpectedConditions.elementToBeClickable(web_credit_update_button)).click();
		wait.until(ExpectedConditions.visibilityOf(web_edit_payment_method));
		card_update_popup_close_button.click();
		driver.switchTo().defaultContent();
	}

	public void updateCardDetailWithInsufficiantBalanceCard() {
		wait.until(ExpectedConditions.elementToBeClickable(web_update_creditCard_button)).click();
		switchToFrameUpdateCreditCard();
		wait.until(ExpectedConditions.visibilityOf(Click_on_primary_card)).click();
		wait.until(ExpectedConditions.visibilityOf(web_edit_payment_method)).click();
		wait.until(ExpectedConditions.elementToBeClickable(web_creditCard_number)).click();
		web_creditCard_number.sendKeys("4000008260003178");
		wait.until(ExpectedConditions.visibilityOf(web_card_cvv_num));
		web_card_cvv_num.sendKeys("123");
		wait.until(ExpectedConditions.elementToBeClickable(web_credit_update_button)).click();

		int i = 0;
		while (driver.findElements(By.tagName("iframe")).size() != 8) {
			Common.pause(1);
			if (i == 60) {
				break;
			}
			i++;
		}
		System.out.println("first frame: " + driver.findElements(By.tagName("iframe")).size());

		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));

		System.out.println("second frame: " + driver.findElements(By.tagName("iframe")).size());
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("challengeFrame"));
		Common.pause(1);

		System.out.println("third frame: " + driver.findElements(By.tagName("iframe")).size());
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		System.out.println("forth frame: " + driver.findElements(By.tagName("iframe")).size());

		wait.until(ExpectedConditions.visibilityOf(completeAuthentication_button));
		completeAuthentication_button.click();

		driver.switchTo().parentFrame();
		driver.switchTo().parentFrame();
		driver.switchTo().parentFrame();

		// wait.until(ExpectedConditions.elementToBeClickable(completeAuthentication_button)).click();
		wait.until(ExpectedConditions.visibilityOf(web_edit_payment_method));
		card_update_popup_close_button.click();
		driver.switchTo().defaultContent();
	}

	public void scrollToYourPlanTxt() {
		wait.until(ExpectedConditions.visibilityOf(web_update_creditCard_button));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", yourPlan_Txt);
	}

	public void scrollToBillingAndCardTxt() {
		wait.until(ExpectedConditions.visibilityOf(billingAndCardText));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", billingAndCardText);
	}

	public boolean validateMessageOnceCreditFallsBelow5() {
		return wait.until(ExpectedConditions.visibilityOf(credit_falls_below_validation_msg)).isDisplayed();
	}

	public void clickOnActivityFeed() {
		wait.until(ExpectedConditions.visibilityOf(activiryfeed));
		activiryfeed.click();
	}
	
	public String getNumberFromActivityFeed() {
		wait.until(ExpectedConditions.visibilityOf(numberFromWebActivityFeed));
		return numberFromWebActivityFeed.getText();
	}

}
