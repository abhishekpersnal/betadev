package com.CallHippo.web.settings.Tags;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class CallTagPage {

	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;

	public CallTagPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		js = (JavascriptExecutor) driver;
	}
	
	@FindBy(xpath = "//a[contains(.,'Dashboard')]")
	private WebElement left_panel_dashboard;
	
	@FindBy(xpath = "//h5[@class='titleboxdashicne'][contains(.,'Call Summary')]")
	private WebElement dashboard_callSummary_txt;

	

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	private WebElement successMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement errorMessage;
	
	@FindBy(xpath = "//section[contains(@id,'tags')]//i[@class='material-icons'][contains(.,'edit')]")
	private WebElement editTagMainButton;
	@FindBy(xpath = "//section[contains(@id,'tags')]//i[@class='material-icons'][contains(.,'close')]")
	private WebElement closeTagMainButton;
	
	@FindBy(xpath = "//span[@class[contains(.,'addtagbtn')]][contains(.,'Add Custom Tag')]") private WebElement addTag;
	@FindBy(xpath = "//input[@class[contains(.,'ant-input-sm')]]") private WebElement enterTagName;
	@FindBy(xpath = "//span[@class='ant-tag edittagdiv']/div/input[@class='ant-input ant-input-sm']") private WebElement enterNewTagName;
	
	
	@FindBy(xpath = "//section[contains(@id,'tags')]//button[@type='button'][contains(.,'save')]") private WebElement saveButton;
	@FindBy(xpath = "//section[contains(@id,'tags')]//button[@type='button'][contains(.,'close')]") private WebElement closeButton;
	@FindBy(xpath = "//div[@class='ant-message-custom-content ant-message-success']") private WebElement successValidationMsg;
	@FindBy(xpath = "//span[contains(.,'local_offerTags')]") private WebElement tagSubmenu;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Edit')]") private WebElement mainTagEdit;
	@FindBy(xpath = "//button[@type='button'][contains(.,'close')]") private WebElement mainTagClose;
	
	@FindBy(xpath = "//tbody//div[@class='ant-select-selection__rendered']") private WebElement allocationDropdown;

	@FindBy(xpath = "//section[contains(@id,'tags')]//p[@class='deparmentname_description']") private WebElement tagsDiscription;
	@FindBy(xpath = "//h2[@class='subtitle'][contains(.,'Subscription Details')]") private WebElement planpopup;
	
	
	
	
	
	

	
	
	public void clickOnTagSubMenu() {
		wait.until(ExpectedConditions.visibilityOf(tagSubmenu));
		js.executeScript("arguments[0].scrollIntoView();", tagSubmenu);
		Common.pause(2);
		tagSubmenu.click();

	}
	public void clickOnAddTagButton() {
		wait.until(ExpectedConditions.visibilityOf(addTag));
		js.executeScript("arguments[0].click();", addTag);
		
	}
	public void validateplanPopup() {
		 wait.until(ExpectedConditions.visibilityOf(planpopup));	
		}
	
	
	public void enterTagName(String tagname) {
		wait.until(ExpectedConditions.visibilityOf(this.enterTagName)).clear();
		wait.until(ExpectedConditions.visibilityOf(this.enterTagName)).sendKeys(tagname);
		Common.pause(2);
		saveButton.click();
	}
	
	public String validateTagsDiscription() {
		return wait.until(ExpectedConditions.visibilityOf(tagsDiscription)).getText();
		
	}
	
	
	
	
	
	
	public void clickLeftMenuDashboard() {
		wait.until(ExpectedConditions.elementToBeClickable(left_panel_dashboard));
		left_panel_dashboard.click();
		wait.until(ExpectedConditions.elementToBeClickable(dashboard_callSummary_txt));
		Common.pause(1);
	}
	
	public String validateSuccessValidationMsg() {
		return wait.until(ExpectedConditions.visibilityOf(successMessage)).getText();
		
	}
	
	
	
	public String validateErrorValidationMsg() {
		return wait.until(ExpectedConditions.visibilityOf(errorMessage)).getText();
		
	}
	public String getFirstLetterOfTag(String tagname) {
		System.out.println(" result 1 = " + tagname);
		String subString = tagname.substring(0,1);
		System.out.println("SubString : --->>> "+subString);
		return subString;
	}
	
	public void deleteAllAddedTags() {
		Common.pause(2);
		editMainTagName();
		Common.pause(2);
		List<WebElement> elements = driver.findElements(
				By.xpath("//span[@class='ant-tag edittagdiv']/*[@class[contains(.,'close')]]"));
		System.out.println("---elements.size()--" + elements.size());

		if (elements.size() > 0) {
			// for (WebElement element : elements) {
			for (int i = 0; i <= elements.size(); i++) {
				System.out.println("==== i ===" + i);
				List<WebElement> elements1 = driver.findElements(
						By.xpath("//span[@class='ant-tag edittagdiv']/*[@class[contains(.,'close')]]"));
				if (elements1.size() > 0) {
					elements1.get(0).click();
				}
				Common.pause(3);

			}
			Common.pause(1);
		}
	}
	
	
	public boolean validateIsTagDisplayInWeb(String tagName) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
					+ "//div[@class='tagbordertop']//span[@class='calltagwrapper']/span[contains(.,'"+tagName+"')]"))));
			return true;
		} catch (Exception e) {

			return false;
		}
	}
	
	public void clickOnAllocationDropDown() {
		wait.until(ExpectedConditions.visibilityOf(allocationDropdown)).click();
	}
	
	public void selectAllocation(String value) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//li[@role=\"option\"][contains(.,'"+value+"')]")))).click();
		Common.pause(2);
	}

	
	
	public void editMainTagName() {
		
		try {
		System.out.println("Edit button1");
		wait.until(ExpectedConditions.visibilityOf(editTagMainButton));
		System.out.println("Edit button2");
		editTagMainButton.click();
		}catch (Exception e) {
		js.executeScript("arguments[0].click();", editTagMainButton);
		}
    }
	public void closeMainTagName() {
		System.out.println("close button1");
		wait.until(ExpectedConditions.visibilityOf(closeTagMainButton));
		System.out.println("close button1");
		closeTagMainButton.click();
     }
	
	public boolean editTagName(String oldTag, String newTag) {
		editMainTagName();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
					+ "//span[@class='ant-tag edittagdiv'][contains(.,'"+oldTag+"')]/*[@class[contains(.,'edit')]]")))).click();
			Common.pause(2);
			enterNewTagName.sendKeys(Keys.CONTROL,"a",Keys.BACK_SPACE);
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(enterNewTagName)).sendKeys(newTag);
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
					+ "//button[@type='button'][contains(.,'save')]/../button")))).click();
			return true;
	
	}
	
	public void deleteTagName(String tagname) {
		editMainTagName();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
				+ "//span[@class='ant-tag edittagdiv'][contains(.,'"+tagname+"')]/*[@class[contains(.,'close')]]")))).click();

}
	
}
