package com.CallHippo.web.settings.Tags;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.web.powerDialer.PowerDialerPage;
import com.CallHippo.web.user.settings.AfterCallWorkPage;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;

public class CallTagTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	DialerIndex phoneApp4;
	WebDriver driverPhoneApp4;
	DialerIndex phoneApp2SubUser;
	WebDriver driver2sub;

	WebToggleConfiguration webApp2;
	WebToggleConfiguration webApp1;
	CallTagPage webApp2Tag;
	CallTagPage webApp1Tag;
	LiveCallPage webApp2LivePage;
	WebDriver driver4;
	PowerDialerPage web2PowerDialerPage;
	AfterCallWorkPage acwPageApp;
	CallingCallCostTest callingCallCostobj;
	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number2 = data.getValue("account2Number1"); // account 2's number
	String acc2number2 = data.getValue("account2Number2"); // account 2's number 2
	String number = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");

	String account4Email = data.getValue("account4MainEmail");
	String account4Password = data.getValue("masterPassword");

	String number3 = data.getValue("account3Number1"); // account 3's number
	String number4 = data.getValue("account4Number1"); // account 4's number

	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	static Common powerDialer1NUmberExcel;
	int sheetNumber = 0;
	int secondRow = 1;
	int secondColumn = 1;

	Common excelTestResult;
	
    String outgoingCallPriceAcc3Num1,outgoingCallPriceAcc1Num1;
    
	public CallTagTest() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		powerDialer1NUmberExcel = new Common("powerDialer\\powerdialer1.xlsx");
		callingCallCostobj = new CallingCallCostTest();
	}

	@BeforeTest
	public void initialization() throws Exception {
		powerDialer1NUmberExcel.write(sheetNumber, secondRow, secondColumn, number);
		driver4 = TestBase.init();
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2Tag = PageFactory.initElements(driver4, CallTagPage.class);
		webApp2LivePage = PageFactory.initElements(driver4, LiveCallPage.class);
		web2PowerDialerPage = PageFactory.initElements(driver4, PowerDialerPage.class);
		acwPageApp = PageFactory.initElements(driver4, AfterCallWorkPage.class);
		
		driver2 = TestBase.init();
		webApp1 = PageFactory.initElements(driver2, WebToggleConfiguration.class);
		webApp1Tag = PageFactory.initElements(driver2, CallTagPage.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

	    driver2sub = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver2sub, DialerIndex.class);
		
		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPriceAcc1Num1=callingCallCostobj.callCost(number,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPriceAcc1Num1);
		
		outgoingCallPriceAcc3Num1=callingCallCostobj.callCost(number3,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPriceAcc3Num1);
		try {
			try {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			try {
				driver2.get(url.signIn());
				loginWeb(webApp1, account1Email, account3Password);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.signIn());
				loginWeb(webApp1, account1Email, account3Password);
				Common.pause(3);
			}

			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			try {
				driver2sub.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver2sub.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			
			

		} catch (Exception e1) {
			String testname = "Call Queue Before Test";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver2sub, testname, "PhoneAppp6 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}

	@BeforeMethod
	public void login() throws Exception {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver.get(url.dialerSignIn());
			driver1.get(url.dialerSignIn());
			driver2.get(url.signIn());
			driver2sub.get(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2Tag.clickOnTagSubMenu();
			webApp2Tag.deleteAllAddedTags();

			driver4.get(url.usersPage());
			webApp2.navigateToUserSettingPage(account2Email);
			Common.pause(5);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number2);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(acc2number2);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			driver.get(url.dialerSignIn());
			driver1.get(url.dialerSignIn());
			driver2.get(url.signIn());
			phoneApp2.waitForDialerPage();
			phoneApp2SubUser.waitForDialerPage();

		} catch (Exception e) {
			try {
				Common.pause(3);
				Common.pause(3);
				driver4.get(url.signIn());
				driver.get(url.dialerSignIn());
				driver1.get(url.dialerSignIn());
				driver2.get(url.signIn());
				driver2sub.get(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}
				driver4.get(url.signIn());
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2Tag.clickOnTagSubMenu();
				webApp2Tag.deleteAllAddedTags();

				driver4.get(url.usersPage());
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(5);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number2);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			} catch (Exception e1) {
				String testname = "Call Queue Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver2sub, testname, "PhoneAppp4 Fail login");
				

				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver4, testname, "WebApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2sub, testname, "PhoneAppp2sub Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver4, testname, "WebApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2sub, testname, "PhoneAppp2sub Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() throws IOException {

		try {
			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2Tag.clickOnTagSubMenu();
			webApp2Tag.deleteAllAddedTags();
		} catch (IOException e) {
			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2Tag.clickOnTagSubMenu();
			webApp2Tag.deleteAllAddedTags();
		} finally {
			driver.quit();
			driver1.quit();
			driver2.quit();
			driver4.quit();
			driver2sub.quit();
		}
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}
	
	
	
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void View_taging_feature_In_WebApp() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		assertEquals(webApp2Tag.validateTagsDiscription(),
				"Notify the relevant users, about this call, by adding @ followed by the Team name or User Name. To know more,Click here");
		webApp2Tag.clickOnAddTagButton();
		String Tag = "Tag"+Common.tagName();
		webApp2Tag.enterTagName(Tag);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");

		
	}
	
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Add_another_tag_name_with_same_name() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String Tag = "Tag"+Common.tagName();
		webApp2Tag.enterTagName(Tag);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        Common.pause(3);
		webApp2Tag.clickOnAddTagButton();
		webApp2Tag.enterTagName(Tag);
		assertEquals(webApp2Tag.validateErrorValidationMsg(), "Tag is already exist");

	}
	
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void Add_another_tag_name_with_Different_name() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String Tag = "Tag"+Common.tagName();
		webApp2Tag.enterTagName(Tag);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        Common.pause(3);
		webApp2Tag.clickOnAddTagButton();
		String Tag1 = "Tag"+Common.tagName();
		webApp2Tag.enterTagName(Tag1);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		Common.pause(3);
		webApp2Tag.clickOnAddTagButton();
		String Tag2 = "Tag"+Common.tagName();
		webApp2Tag.enterTagName(Tag2);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");

	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void Add_another_tag_name_with_User_name() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		webApp2Tag.enterTagName(Callername);
		assertEquals(webApp2Tag.validateErrorValidationMsg(), "Tag is already exist");

	}

	
	
	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Verify_Verify_Tag_name_In_Shared_inbox() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		assertTrue(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
	}
		
	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_name_in_Incoming_Call_In_web() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		assertTrue(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("@"+tagName, webApp2.validateItagNotes(1));

	}
	
	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_name_in_Outgoing_Call() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		phoneApp2.validateIncomingCallingScreenIsdisplayed();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		//assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		webApp2.validateTagInWeb(tagName);
		

	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_name_with_CallNote_in_Incoming_Call() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("Outoing_Call_Tag ");
		Common.pause(2);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		assertTrue(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPrice, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("@"+tagName, webApp2.validateItagNotes(1));

	}
	
	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_name_with_CallNote_in_Outgoing_Call() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		phoneApp2.validateIncomingCallingScreenIsdisplayed();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("Outoing_Call_Tag ");
		Common.pause(2);
		phoneApp2.insertTAG("@");
		phoneApp2.insertTAG(firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		//assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Outoing_Call_Tag "+"@"+tagName, webApp2.validateItagNotes(1));
		webApp2.validateTagInWeb(tagName);
		

	}
	
	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_in_PowerDialer_Call() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		web2PowerDialerPage.clickOnPowerDialerSideMenu();
		web2PowerDialerPage.clickOnAddCampaignButton();
		web2PowerDialerPage.enterCampaignName("Campaign 2");
		web2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		web2PowerDialerPage.selectAssignee(callerName2);
		web2PowerDialerPage.selectFromNumber(number2);
		web2PowerDialerPage.autoVoicemailDrop("off");
		web2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		web2PowerDialerPage.campaignDescription("Test automation");
		web2PowerDialerPage.clickOnSaveButton();

		assertEquals(web2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		web2PowerDialerPage.campaignActivation("on");
		web2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("Outoing_Call_Tag ");
		Common.pause(2);
		phoneApp2.insertTAG("@");
		phoneApp2.insertTAG(firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		 //validate Number in All Calls page
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertTrue(phoneApp2.validateTagInInboxdetailspage(tagName)); 
//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Outoing_Call_Tag "+"@"+tagName, webApp2.validateItagNotes(1));
		webApp2.validateTagInWeb(tagName);

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		

	}
	
	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_in_Internal_Call() throws Exception {
	
		
	webApp2.clickLeftMenuSetting();
	Common.pause(2);
	webApp2Tag.clickOnTagSubMenu();
	webApp2Tag.clickOnAddTagButton();
	String tagName = "QA"+Common.tagName();
	String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
	
	webApp2Tag.enterTagName(tagName);
	assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
	driver4.get(url.usersPage());
	webApp2.navigateToUserSettingPage(account2Email);
	Common.pause(9);
	String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
	System.out.println(account2MainUserExtension);
	webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
	Common.pause(2);

	driver4.get(url.usersPage());
	webApp2.navigateToUserSettingPage(account2EmailSubUser);
	Common.pause(9);
	String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
	System.out.println(account2SubUserExtension);
	webApp2.setUserSttingToggles("off", "on", "off", "open", "off");
	Common.pause(2);

	phoneApp2.clickOnSideMenu();
	String mainusername = phoneApp2.getCallerName();
	String firstLetterOfuserNametag = webApp2Tag.getFirstLetterOfTag(mainusername);
	Common.pause(2);
	phoneApp2.clickOnSideMenuDialPad();
	
	phoneApp2SubUser.clickOnSideMenu();
	String subusername = phoneApp2SubUser.getCallerName();
	Common.pause(2);
	phoneApp2SubUser.clickOnSideMenuDialPad();
	
	phoneApp2.enterNumberinDialer(account2SubUserExtension);
	phoneApp2.clickOnDialButton();

	assertEquals(phoneApp2.validateCallingScreenOutgoingVia(), "Outgoing");
	assertEquals(phoneApp2.validateExtCallOutgoingNameCallingScreen(), subusername);
	assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(), account2SubUserExtension);
	
	
	Common.pause(1);
	phoneApp2SubUser.waitForIncomingCallingScreen();
	phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
	String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(1);
	Common.pause(2);
	assertEquals(phoneApp2SubUser.validateExtCallIncomingNameCallingScreen(), mainusername);
	assertEquals(phoneApp2SubUser.validateSubUserIncomingNumberCallingScreen(), account2MainUserExtension);
	assertEquals(phoneApp2SubUser.validateCallDurationonDisplayOnCallingScreen(), true);
	assertEquals(phoneApp2SubUser.ExtOutgoingScreenRecordingButtonIsDisabled(), true);
	assertEquals(phoneApp2SubUser.ExtOutgoingScreenForwardButtonIsDisabled(), true);
	
	Thread.sleep(8000);

	assertEquals(phoneApp2.validateCallDurationonDisplayOnCallingScreen(), true);
	assertEquals(phoneApp2.ExtOutgoingScreenRecordingButtonIsDisabled(), true);
	assertEquals(phoneApp2.ExtOutgoingScreenForwardButtonIsDisabled(), true);
	
	Common.pause(5);
	phoneApp2.insertCallNotes("Outoing_Call_Tag ");
	Common.pause(2);
	phoneApp2.insertTAG("@");
	phoneApp2.insertTAG(firstLetterOfTag);
	Common.pause(5);
	assertFalse(phoneApp2.tagIsDisplaying(tagName));
	//assertFalse(phoneApp2.tagIsDisplaying(Callername));
	Common.pause(2);
	phoneApp2SubUser.insertCallNotes("Subuser call Notes ");
	Common.pause(2);
	phoneApp2SubUser.insertTAG("@");
	phoneApp2SubUser.insertTAG(firstLetterOfuserNametag);
	Common.pause(5);
	assertFalse(phoneApp2SubUser.tagIsDisplaying(mainusername));
	//assertFalse(phoneApp2.tagIsDisplaying(Callername));
	Common.pause(2);
	
	phoneApp2SubUser.clickOnIncomingHangupButton();
	phoneApp2SubUser.waitForDialerPage();
	phoneApp2.waitForDialerPage();
	Thread.sleep(10000);

	phoneApp2.clickOnSideMenu();
	String mainUserCallerName = phoneApp2.getCallerName();
	phoneApp2.clickOnAllCalls();
	Thread.sleep(5000);

	// validate Number in All Calls page For 2nd Entry(1st queued call)
	assertEquals(account2MainUserExtension, phoneApp2.validateNumberInAllcallsFor2ndEntry(),
			"--Number on dialer 2 all call page--");

	// validate call type in all calls page For 2nd Entry(1st queued call)
	assertEquals("Incoming", phoneApp2.validatecallTypeFor2ndentry(), "--Incoming Call type in dialer 2--");

	phoneApp2.clickOnRightArrowFor2ndEntry();
	 
	// validate Number on call details page For 2nd Entry(1st queued call)
	assertEquals(account2MainUserExtension, phoneApp2.validateNumberInCallDetail(),
			"--Number in call log detail of dialer2--");

	// validate duration in call Details page
	softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
			"--Duration of outgoing call in call detail page of dialer 2--");

	// validate Status For 2nd Entry(1st queued call)
	assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail(),
			"--Completed call status call log detail of dialer2--");

	assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
	assertEquals(phoneApp2.callDetailSMSIconNotVisible(), true);
	assertFalse(phoneApp2.validateTagInInboxdetailspage(tagName));
	phoneApp2.clickOnBackOnCallDetailPage();

	// validate Number in All Calls page
	assertEquals(account2SubUserExtension, phoneApp2.validateNumberInAllcalls());

	// validate call type in all calls page
	assertEquals("Outgoing", phoneApp2.validatecallType());

	phoneApp2.clickOnRightArrow();
	assertEquals(phoneApp2.callDetailBlackListIconNotVisible(), true);
	assertEquals(phoneApp2.callDetailSMSIconNotVisible(), true);
	Thread.sleep(4000);

	// validate Number on call details page
	assertEquals(account2SubUserExtension, phoneApp2.validateNumberInCallDetail());

	// validate duration in call Details page
	softAssert.assertEquals(durationOnOutgoingCallScreen + " Mins", phoneApp2.validateDurationInCallDetail(),
			"--Duration of outgoing call in call detail page of dialer 2--");

	// validate call status in Call detail page
	assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
	assertFalse(phoneApp2.validateTagInInboxdetailspage(tagName)); 
	phoneApp2SubUser.clickOnSideMenu();
	String subUserCallerName = phoneApp2SubUser.getCallerName();
	phoneApp2SubUser.clickOnAllCalls();
	Thread.sleep(4000);
	// validate Number in All Calls page
	assertEquals(account2MainUserExtension, phoneApp2SubUser.validateNumberInAllcalls());

	// validate call type in all calls page
	assertEquals("Incoming", phoneApp2SubUser.validatecallType());

	phoneApp2SubUser.clickOnRightArrow();
	assertEquals(phoneApp2SubUser.callDetailBlackListIconNotVisible(), true);
	assertEquals(phoneApp2SubUser.callDetailSMSIconNotVisible(), true);
	Thread.sleep(4000);

	// validate Number on call details page
	assertEquals(account2MainUserExtension, phoneApp2SubUser.validateNumberInCallDetail());

	assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
	assertFalse(phoneApp2.validateTagInInboxdetailspage(tagName)); 
	driver4.get(url.signIn());
	webApp2.clickOnActivitFeed();
	Common.pause(5);

	assertEquals("-", webApp2.validateDepartmentNameFor2ndEntry(), "--Department name in web2 Activityfeed page--");
	assertEquals(subUserCallerName, webApp2.validateCallerNameFor2ndEntry(),
			"--Caller name in web2 Activityfeed page--");
	assertEquals(mainUserCallerName, webApp2.validateClientNumberFor2ndentry(),
			"--Client number in web2 Activityfeed page--");
	assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(),
			"--Completed call status in web2 Activityfeed page--");
	assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry(),
			"Incoming call type in web2 Activityfeed page--");
	assertEquals("-", webApp2.validateCallCostFor2ndEntry(), "--Call cost in web2 Activityfeed page--");
	webApp2.clickOniButtonFor2ndEntry();
	Common.pause(5);
	assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");
	webApp2.clickOnCallNotesBtn(2);
	Common.pause(2);
	assertEquals("Subuser call Notes "+"@"+firstLetterOfuserNametag, webApp2.validateItagNotes(1));

	driver4.get(url.signIn());
	webApp2.clickOnActivitFeed();
	Common.pause(5);
	assertEquals("-", webApp2.validateDepartmentName(), "----verify departmrnt name in webApp2");
	assertEquals(mainUserCallerName, webApp2.validateCallerName(), "-----verify caller name in webApp2");
	assertEquals(subUserCallerName, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");

	assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webApp2---");
	assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
	assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webApp2-----");
	webApp2.clickOniButton();
	Common.pause(5);
	assertEquals(true, webApp2.validateRecordingUrl(),
			"-----verify call recording- recording not availble--------");
	webApp2.clickOnCallNotesBtn(1);
	Common.pause(2);
	assertEquals("Outoing_Call_Tag "+"@"+firstLetterOfTag, webApp2.validateItagNotes(1));


	}
	
	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void Verify_when_No_TAG_name_Is_added_in_Incoming_Call() throws Exception {
		
		

		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@");
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(Callername));
		assertTrue(phoneApp2.tagIsDisplaying("Team"));
		assertTrue(phoneApp2.tagIsDisplaying("main user team"));
		assertTrue(phoneApp2.tagIsDisplaying("subuser team"));
		
		phoneApp2.selectTagFromDropdown(Callername);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(Callername);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("@"+Callername, webApp2.validateItagNotes(1));

	}
	
	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void Verify_Add_Call_note_without_tag_added_in_Incoming_Call() throws Exception {
		
		

		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("Add_Call_note_without_tag_added ");
		phoneApp2.insertCallNotes("@");
		
		
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(false, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(number, phoneApp2.validateNumberInAllcalls());
		phoneApp2.clickOnRightArrow();
		assertFalse(phoneApp2.validateTagInInboxdetailspage(""));

		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note_without_tag_added @", webApp2.validateItagNotes(1));

	}
	
	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Verify_Filter_Calls_With_Tag() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");

		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("Add_Call_note_without_tag_added ");
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		
		
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnCallReminderCancelButton();
		phoneApp2.clickoncalllogfilterdrpdown();
		phoneApp2.clickOnSpecificdropdown("Tag Filter");
		phoneApp2.selectFilter(tagName);
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals(number, phoneApp2.validateNumberInAllcalls());
		phoneApp2.clickOnRightArrow();
		assertTrue(phoneApp2.validateTagInInboxdetailspage(tagName));

		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note_without_tag_added @"+tagName, webApp2.validateItagNotes(1));

	}
	
	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Verify_Delete_Added_tag_name() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		driver4.get(url.signIn());
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
		driver1.get(url.dialerSignIn());
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		//assertEquals("@"+tagName, webApp2.validateItagNotes(1));
		driver4.get(url.signIn());
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.deleteTagName(tagName);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(false, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		assertTrue(phoneApp2.validateTagInInboxdetailspage(tagName));
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertFalse(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));

	}
	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Verify_Change_tag_name_and_Add_tag_name() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
		driver1.get(url.dialerSignIn());
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
	//	assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("@"+tagName, webApp2.validateItagNotes(1));
		driver4.get(url.signIn());
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		String tagName1 = "QA"+Common.tagName();
		webApp2Tag.getFirstLetterOfTag(tagName1);
		webApp2Tag.editTagName(tagName,tagName1);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag updated successfully");
		
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(false, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		assertTrue(phoneApp2.validateTagInInboxdetailspage(tagName));
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName1));
		assertTrue(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName1);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName1);

	}
	
	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void Verify_Change_tag_name_while_user_is_on_Call() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		driver4.get(url.signIn());
		Common.pause(5);
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		String tagName1 = "QA"+Common.tagName();
		String firstLetterOfTag1 = webApp2Tag.getFirstLetterOfTag(tagName1);
		webApp2Tag.editTagName(tagName, tagName1);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag updated successfully");
		
		phoneApp2.insertCallNotes("@"+firstLetterOfTag1);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		assertFalse(phoneApp2.tagIsDisplaying(tagName1));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName1);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("@"+tagName, webApp2.validateItagNotes(1));
		

	}
	
	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void Verify_Filter_Callby_tag_when_Tag_Is_not_available_in_plan () throws Exception {
		
		webApp1.clickLeftMenuSetting();
		Common.pause(2);
		webApp1Tag.clickOnTagSubMenu();
		webApp1Tag.clickOnAddTagButton();
		webApp1Tag.validateplanPopup();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCallsInbox();
		phoneApp1.clickoncalllogfilterdrpdown();
		phoneApp1.clickOnSpecificdropdown("Tag Filter");
		assertEquals(phoneApp1.validationMessage(),"Call Tagging feature is not available in your plan.");
	}
	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_Call_Tag_with_ACW_tag_name_in_Incoming() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		Thread.sleep(500);

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp2.insertCallNotes("Add_Call_note ");
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.validateTagIsPresentInACWScreen(tagName);
		phoneApp2.closeDropDown();
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(tagName, true));

		phoneApp2.insertACWNote("Outgoing_Completed");
		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note @"+tagName+" Outgoing_Completed", webApp2.validateItagNotes(1));

		
		
	}
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_Call_Tag_with_ACW_Note_in_Outgoing() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		Thread.sleep(500);

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		phoneApp2.insertCallNotes("Add_Call_note ");
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.validateTagIsPresentInACWScreen(tagName);
		phoneApp2.closeDropDown();
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(tagName,true));

		phoneApp2.insertACWNote("Incoming_Completed");
		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note @"+tagName+" Incoming_Completed", webApp2.validateItagNotes(1));

		
		
	}
	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void Verify_Verify_Tag_name_In_Shasred_inbox_in_subuser() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		assertTrue(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
	}
	
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_ACW_tag_name_in_Outgoing() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		Thread.sleep(500);

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.validateTagIsPresentInACWScreen("");
		phoneApp2.closeDropDown();
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(tagName,false));
        phoneApp2.selectDeselectSpecificTagFromACWScreen(tagName, false);
        Thread.sleep(1000);
		phoneApp2.insertACWNote("Incoming_Completed");
		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Incoming_Completed", webApp2.validateItagNotes(1));
		assertTrue(webApp2Tag.validateIsTagDisplayInWeb(tagName));

		
		
	}
	
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_ACW_tag_name_in_Incoming() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		Thread.sleep(500);

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.validateTagIsPresentInACWScreen("");
		phoneApp2.closeDropDown();
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(tagName,false));
        phoneApp2.selectDeselectSpecificTagFromACWScreen(tagName, false);
        Thread.sleep(1000);
		phoneApp2.insertACWNote("Incoming_Completed");
		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		//assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Incoming_Completed", webApp2.validateItagNotes(1));
		assertTrue(webApp2Tag.validateIsTagDisplayInWeb(tagName));

		
		
	}
	
	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_name_with_CallNote_in_Incoming_Call_Subuser() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2SubUser.clickOnSideMenu();
		String Callername = phoneApp2SubUser.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.enterNumberinDialer(number);
		phoneApp2SubUser.clickOnDialButton();
		phoneApp1.validateIncomingCallingScreenIsdisplayed();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2SubUser.insertCallNotes("Outoing_Call_Tag ");
		Common.pause(2);
		phoneApp2SubUser.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2SubUser.tagIsDisplaying(tagName));
		assertFalse(phoneApp2SubUser.tagIsDisplaying(Callername));
		
		phoneApp2SubUser.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2SubUser.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2SubUser.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2SubUser.validateTagLogIsInBoldFormate());
		phoneApp2SubUser.clickOnInboxRightArrow();
		phoneApp2SubUser.validateTagInInboxdetailspage(tagName);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("@"+tagName, webApp2.validateItagNotes(1));

	}
	
	@Test(priority = 24, retryAnalyzer = Retry.class)
	public void Verify_by_adding_perticular_TAG_name_with_CallNote_in_Outgoing_Call_Subuser() throws Exception {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number2);
		phoneApp1.clickOnDialButton();
		phoneApp2.validateIncomingCallingScreenIsdisplayed();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.insertCallNotes("Outoing_Call_Tag ");
		Common.pause(2);
		phoneApp2.insertTAG("@");
		phoneApp2.insertTAG(firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		//assertFalse(phoneApp2.tagIsDisplaying(Callername));
		
		phoneApp2.selectTagFromDropdown(tagName);
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		//assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Outoing_Call_Tag "+"@"+tagName, webApp2.validateItagNotes(1));
		webApp2.validateTagInWeb(tagName);
		

	}
	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void AddAdd_Tag_on_call_and_remove_from_ACW_screen_incoming() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number2);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		Thread.sleep(500);

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

	    phoneApp2.validateDurationInCallingScreen(2);
	    phoneApp2.insertCallNotes("Add_Call_note ");
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		phoneApp2.selectTagFromDropdown(tagName);
		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.validateTagIsPresentInACWScreen("");
		phoneApp2.closeDropDown();
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(tagName,true));
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(Callername,false));
        phoneApp2.selectDeselectSpecificTagFromACWScreen(tagName, true);
        phoneApp2.selectDeselectSpecificTagFromACWScreen(Callername, false);
        Thread.sleep(1000);
		phoneApp2.insertACWNote("Incoming_Completed");
		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
		phoneApp2.clickOnBackOnCallDetailPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		//assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note "+"@"+tagName+" Incoming_Completed", webApp2.validateItagNotes(1));
		assertTrue(webApp2Tag.validateIsTagDisplayInWeb(Callername));
		assertEquals(webApp2Tag.validateIsTagDisplayInWeb(tagName),false);

		
		
	}
	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void AddAdd_Tag_on_call_and_remove_from_ACW_screen_Outgoing() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnDialButton();
		Thread.sleep(500);

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

	    phoneApp2.validateDurationInCallingScreen(2);
	    phoneApp2.insertCallNotes("Add_Call_note ");
		Common.pause(5);
		phoneApp2.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2.tagIsDisplaying(tagName));
		phoneApp2.selectTagFromDropdown(tagName);
		Thread.sleep(9000);

		phoneApp2.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.validateTagIsPresentInACWScreen("");
		phoneApp2.closeDropDown();
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(tagName,true));
		assertTrue(phoneApp2.ValidateTagsonACWscreenAndSelected(Callername,false));
        phoneApp2.selectDeselectSpecificTagFromACWScreen(tagName, true);
        phoneApp2.selectDeselectSpecificTagFromACWScreen(Callername, false);
        Thread.sleep(1000);
		phoneApp2.insertACWNote("Incoming_Completed");
		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2.validateTagLogIsInBoldFormate());
		phoneApp2.clickOnInboxRightArrow();
		phoneApp2.validateTagInInboxdetailspage(tagName);
		phoneApp2.clickOnBackOnCallDetailPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note "+"@"+tagName+" Incoming_Completed", webApp2.validateItagNotes(1));
		assertTrue(webApp2Tag.validateIsTagDisplayInWeb(Callername));
		assertEquals(webApp2Tag.validateIsTagDisplayInWeb(tagName),false);

		
		
	}
	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void AddAdd_Tag_on_call_and_remove_from_ACW_screen_Outgoing_Subuser() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver2sub.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2SubUser.waitForDialerPage();
		phoneApp2SubUser.enterNumberinDialer(number);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2SubUser.clickOnDialButton();
		Thread.sleep(500);

		phoneApp1.waitForIncomingCallingScreen();

		phoneApp1.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.validateDurationInCallingScreen(2);
		phoneApp2SubUser.insertCallNotes("Add_Call_note ");
		Common.pause(5);
		phoneApp2SubUser.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2SubUser.tagIsDisplaying(tagName));
		phoneApp2SubUser.selectTagFromDropdown(tagName);
		Thread.sleep(9000);

		phoneApp2SubUser.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2SubUser.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2SubUser.validateAfterCallWorkTitle());

		phoneApp2SubUser.validateTagIsPresentInACWScreen("");
		phoneApp2SubUser.closeDropDown();
		assertTrue(phoneApp2SubUser.ValidateTagsonACWscreenAndSelected(tagName,true));
		assertTrue(phoneApp2SubUser.ValidateTagsonACWscreenAndSelected(Callername,false));
		phoneApp2SubUser.selectDeselectSpecificTagFromACWScreen(tagName, true);
		phoneApp2SubUser.selectDeselectSpecificTagFromACWScreen(Callername, false);
        Thread.sleep(1000);
        phoneApp2SubUser.insertACWNote("Outgoing_Subuser_Completed");
        phoneApp2SubUser.clickEndACWButton();

        phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2SubUser.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2SubUser.validateTagLogIsInBoldFormate());
		phoneApp2SubUser.clickOnInboxRightArrow();
		phoneApp2SubUser.validateTagInInboxdetailspage(tagName);
		phoneApp2SubUser.clickOnBackOnCallDetailPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		String status = phoneApp2SubUser.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
	//	assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note "+"@"+tagName+" Outgoing_Subuser_Completed", webApp2.validateItagNotes(1));
		assertTrue(webApp2Tag.validateIsTagDisplayInWeb(Callername));
		assertEquals(webApp2Tag.validateIsTagDisplayInWeb(tagName),false);

		
		
	}
	@Test(priority = 28, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void AddAdd_Tag_on_call_and_remove_from_ACW_screen_Incoming_Subuser() throws Exception {

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2Tag.clickOnTagSubMenu();
		webApp2Tag.clickOnAddTagButton();
		String tagName = "QA"+Common.tagName();
		String firstLetterOfTag = webApp2Tag.getFirstLetterOfTag(tagName);
		webApp2Tag.enterTagName(tagName);
		assertEquals(webApp2Tag.validateSuccessValidationMsg(), "Tag added successfully");
        webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp.setAfterCallWorkDuration("5");
		Common.pause(3);
		driver2sub.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		Thread.sleep(2000);
		phoneApp2.clickOnSideMenu();
		String Callername = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2SubUser.waitForDialerPage();
		phoneApp1.enterNumberinDialer(number);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		Thread.sleep(500);

		phoneApp2SubUser.waitForIncomingCallingScreen();

		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.validateDurationInCallingScreen(2);
		phoneApp2SubUser.insertCallNotes("Add_Call_note ");
		Common.pause(5);
		phoneApp2SubUser.insertCallNotes("@"+firstLetterOfTag);
		Common.pause(5);
		assertTrue(phoneApp2SubUser.tagIsDisplaying(tagName));
		phoneApp2SubUser.selectTagFromDropdown(tagName);
		Thread.sleep(9000);

		phoneApp2SubUser.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp2SubUser.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2SubUser.validateAfterCallWorkTitle());

		phoneApp2SubUser.validateTagIsPresentInACWScreen("");
		phoneApp2SubUser.closeDropDown();
		assertTrue(phoneApp2SubUser.ValidateTagsonACWscreenAndSelected(tagName,true));
		assertTrue(phoneApp2SubUser.ValidateTagsonACWscreenAndSelected(Callername,false));
		phoneApp2SubUser.selectDeselectSpecificTagFromACWScreen(tagName, true);
		phoneApp2SubUser.selectDeselectSpecificTagFromACWScreen(Callername, false);
        Thread.sleep(1000);
        phoneApp2SubUser.insertACWNote("Incoming_Subuser_Completed");
        phoneApp2SubUser.clickEndACWButton();

        phoneApp2SubUser.waitForDialerPage();

		Thread.sleep(10000);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCallsInbox();
		assertEquals(number, phoneApp2SubUser.validateNumberInInboxCalls());
		assertEquals(true, phoneApp2SubUser.validateTagLogIsInBoldFormate());
		phoneApp2SubUser.clickOnInboxRightArrow();
		phoneApp2SubUser.validateTagInInboxdetailspage(tagName);
		phoneApp2SubUser.clickOnBackOnCallDetailPage();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(number, phoneApp2SubUser.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2SubUser.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp2SubUser.validatecallType());

		phoneApp2SubUser.clickOnRightArrow();

		assertEquals(number, phoneApp2SubUser.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2SubUser.validateCallStatusInCallDetail());
		String status = phoneApp2SubUser.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2SubUser.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(Callername, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		//assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Add_Call_note "+"@"+tagName+" Incoming_Subuser_Completed", webApp2.validateItagNotes(1));
		assertTrue(webApp2Tag.validateIsTagDisplayInWeb(Callername));
		assertEquals(webApp2Tag.validateIsTagDisplayInWeb(tagName),false);

		
		
	}
	
}