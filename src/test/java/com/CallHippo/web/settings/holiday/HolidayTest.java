package com.CallHippo.web.settings.holiday;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;

public class HolidayTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	DialerIndex phoneApp4;
	WebDriver driverPhoneApp4;
	DialerIndex phoneApp2SubUser;
	WebDriver driver6;

	WebToggleConfiguration webApp2;
	HolidayPage webApp2HolidayPage;
	LiveCallPage webApp2LivePage;
	WebDriver driver4;

	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String number1 = data.getValue("account2Number2"); // account 2's number 2
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");

	String account4Email = data.getValue("account4MainEmail");
	String account4Password = data.getValue("masterPassword");

	String number3 = data.getValue("account3Number1"); // account 3's number
	String number4 = data.getValue("account4Number1"); // account 4's number

	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	Common excelTestResult;

	public HolidayTest() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}

	@BeforeTest
	public void initialization() throws Exception {
		driver4 = TestBase.init();
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);
		webApp2LivePage = PageFactory.initElements(driver4, LiveCallPage.class);
		

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		driverPhoneApp4 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driverPhoneApp4, DialerIndex.class);

		driver6 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver6, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}

			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			try {
				driver6.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver6.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			try {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			try {

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());
				
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();


			} catch (InterruptedException e) {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());
				
				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}

		} catch (Exception e1) {
			String testname = "Call Queue Before Test";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driver6, testname, "PhoneAppp6 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}

	@BeforeMethod
	public void login() throws Exception {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver.get(url.dialerSignIn());
			driver1.get(url.dialerSignIn());
			driver2.get(url.dialerSignIn());
			driverPhoneApp4.get(url.dialerSignIn());
			driver6.get(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}

			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}
			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2HolidayPage.clickOnHolidaySubMenu();
			webApp2HolidayPage.deleteAllHolidayEntries();

			driver4.get(url.usersPage());
			webApp2.navigateToUserSettingPage(account2Email);
			Common.pause(5);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number1);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

		} catch (Exception e) {
			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver.get(url.dialerSignIn());
				driver1.get(url.dialerSignIn());
				driver2.get(url.dialerSignIn());
				driverPhoneApp4.get(url.dialerSignIn());
				driver6.get(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}

				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}
				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account4Email, account4Password);
					Common.pause(3);
				}
				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}

				driver4.get(url.signIn());
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();

				driver4.get(url.usersPage());
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(5);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

			} catch (Exception e1) {
				String testname = "Call Queue Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driver6, testname, "PhoneAppp6 Fail login");

				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp6 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp6 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() throws IOException {

		try {
			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2HolidayPage.clickOnHolidaySubMenu();
			webApp2HolidayPage.deleteAllHolidayEntries();
		} catch (IOException e) {
			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2HolidayPage.clickOnHolidaySubMenu();
			webApp2HolidayPage.deleteAllHolidayEntries();
		} finally {
			driver.quit();
			driver1.quit();
			driver2.quit();
			driver4.quit();
			driverPhoneApp4.quit();
			driver6.quit();
		}
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void add_Holiday_with_Everyone() {
		
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for everyone");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for everyone"), true,
				"holiday for everyone is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidayEditButton();
		webApp2HolidayPage.enterHolidayName("edited everyone");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday edited successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("edited everyone"), true,
				"holiday for everyone is not added");

		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("added new");
		webApp2HolidayPage.clickOnHolidaySaveButton();

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidayDeleteButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday deleted successfully");

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void add_Holiday_with_Users() throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");
        try {
		assertEquals(webApp2HolidayPage.validateMoreHyperLink(), true);
        }catch(AssertionError  e) {
        webApp2HolidayPage.validateAssignuser(callerName2, callerName2Subuser);
        }
		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidayEditButton();
		webApp2HolidayPage.enterHolidayName("edited users");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Teams");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday edited successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("edited users"), true,
				"holiday for everyone is not added");

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void add_Holiday_with_Teams() {
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for Teams");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Teams");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for Teams"), true,
				"holiday for Teams is not added");

		driver4.navigate().refresh();
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidayEditButton();
		webApp2HolidayPage.enterHolidayName("edited Team");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday edited successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("edited Team"), true,
				"holiday for everyone is not added");

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_Everyone() throws Exception {
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_Users_all_selected() throws Exception {
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for everyone");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for everyone"), true,
				"holiday for everyone is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_Teams_all_selected() throws Exception {
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for Teams");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Teams");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for Teams"), true,
				"holiday for Teams is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_Everyone_Holiday_Greeting_is_ON() throws Exception {
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");
		Common.pause(5);
		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(4);
		webApp2HolidayPage.clickOnHolidayGreetingRadioButton();

		
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		phoneApp2.clickOnSideMenu();
		String callerNameOfAccount2 = phoneApp2.getCallerName();
		driver1.navigate().to(url.dialerSignIn());
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		
		webApp2LivePage.waitForChangeCallStatusTo("1", "Holiday");
		assertEquals(webApp2LivePage.validateCallType("1", "Incoming"), true, "validate in incoming call type icon is display.");
		assertEquals(webApp2LivePage.validateAgentName("1"), "");
		assertEquals(webApp2LivePage.validateFrom("1"), number2);
		assertEquals(webApp2LivePage.validateTo("1"), number);
		assertEquals(webApp2LivePage.validateCallStatus("1"), "Holiday");
		assertEquals(webApp2LivePage.validateHangupButton("1"), false, "validate hangup button is display.");
		
		
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);
		
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_incoming_voicemail_Holiday_with_Everyone_Holiday_voicemail_is_ON() throws Exception {
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		Common.pause(5);

		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(5);

		webApp2HolidayPage.clickOnHolidayVoicemailRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(9);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Voice Mail", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_Everyone_all_Users_are_Loggedout() throws Exception {

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);

		loginDialer(phoneApp2, account2Email, account2Password);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void verify_incoming_Voicemail_Holiday_with_Everyone_Number_voicemail_is_ON() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for everyone");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for everyone"), true,
				"holiday for everyone is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		Common.pause(5);

		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(5);

		webApp2HolidayPage.clickOnHolidayVoicemailRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(9);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Voice Mail", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void verify_incoming_Voicemail_Holiday_with_Team_Number_voicemail_is_ON() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for Teams");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Teams");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for Teams"), true,
				"holiday for Teams is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		Common.pause(5);

		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(5);

		webApp2HolidayPage.clickOnHolidayVoicemailRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(9);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Voice Mail", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_Team_Holiday_Greeting_is_ON() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("Team");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for Teams");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Teams");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for Teams"), true,
				"holiday for Teams is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");
		Common.pause(5);
		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(4);
		webApp2HolidayPage.clickOnHolidayGreetingRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void verify_incoming_Voicemail_Holiday_with_User_Number_voicemail_is_ON() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");

		Common.pause(5);

		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(5);

		webApp2HolidayPage.clickOnHolidayVoicemailRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(9);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("Voice Mail", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void verify_incoming_callNotSetup_Holiday_with_users_Holiday_Greeting_is_ON() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");
		Common.pause(5);
		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(4);
		webApp2HolidayPage.clickOnHolidayGreetingRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	
	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Incoming_User_is_on_holiday_Holiday_greeting_Call_hangup_while_Holiday_greeting_script_is_playing() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");
		Common.pause(5);
		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(4);
		webApp2HolidayPage.clickOnHolidayGreetingRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(3);
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals(callCost1, webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}
	
	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Incoming_User_is_on_holiday_Holiday_Voicemail_Call_hangup_while_Holiday_greeting_script_is_playing() throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number); 
		Thread.sleep(10000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		webApp2.userTeamAllocation("users");
		Thread.sleep(2000);

		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2HolidayPage.clickOnHolidaySubMenu();
		webApp2HolidayPage.clickOnHolidayButton();
		webApp2HolidayPage.enterHolidayName("holiday for user");
		webApp2HolidayPage.clickOnAllocationDropDown();
		webApp2HolidayPage.selectAllocation("Users");
		webApp2HolidayPage.clickOnHolidaySaveButton();
		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
				"holiday for Users is not added");

		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");
		Common.pause(5);
		webApp2HolidayPage.settingHolidayToggle("on");
		Common.pause(4);
		webApp2HolidayPage.clickOnHolidayVoicemailRadioButton();

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.enterNumberinDialer(number);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");
		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");
		
		phoneApp1.validateDurationInCallingScreen(1);
		Thread.sleep(3);
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available due to Holiday", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(),
				"--Department name in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
		assertEquals("User is not available due to Holiday", webApp2.validateCallStatus(),
				"--Completed call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
				"Incoming call type in web2 Activityfeed page--");
		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
		webApp2.clickStatusItagBtn();
		assertEquals("Call not setup", webApp2.validateStatusItag(),
				"--Call not setup with reason in web2 Activityfeed page--");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");

	}

	// @Test(priority = 1, retryAnalyzer = Retry.class)
//	public void verify_incoming_callNotSetup_Holiday_with_Everyone_Number_IVR_is_ON_and_User_Voicemail_ON() throws Exception {
//		driver4.get(url.numbersPage());
//		webApp2.navigateToNumberSettingpage(number);
//		Thread.sleep(5000);
//		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
//		Thread.sleep(2000);
//		
//		driver4.get(url.usersPage());
//		webApp2.navigateToUserSettingPage(account2Email);
//		
//		Thread.sleep(9000);
//		webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
//		Common.pause(2);
//		
//		webApp2.clickLeftMenuSetting();
//		Common.pause(2);
//		webApp2HolidayPage.clickOnHolidaySubMenu();
//		webApp2HolidayPage.clickOnHolidayButton();
//		webApp2HolidayPage.enterHolidayName("holiday for user");
//		webApp2HolidayPage.clickOnAllocationDropDown();
//		webApp2HolidayPage.selectAllocation("Users");
//		webApp2HolidayPage.clickOnHolidaySaveButton();
//		assertEquals(webApp2HolidayPage.validateSuccessValidationMsg(), "Holiday added successfully.");
//		assertEquals(webApp2HolidayPage.validateIsHolidayAdded("holiday for user"), true,
//				"holiday for Users is not added");
//
//		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Holiday"), true, "verify title of Holiday");
//		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Date"), true, "verify title of Date");
//		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Assign"), true, "verify title of Assign");
//		assertEquals(webApp2HolidayPage.validateHolidaySectionTitle("Action"), true, "verify title of Action");
//
//		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
//
//		phoneApp1.enterNumberinDialer(number);
//		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
//		phoneApp1.clickOnDialButton();
//		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia(),
//				"--Department name of dialer1--");
//		assertEquals(number, phoneApp1.validateCallingScreenOutgoingNumber(),
//				"--Number on calling screen of dialer1--");
//		
//		phoneApp1.validateDurationInCallingScreen(1);
//
//		Thread.sleep(2000);
//
//		phoneApp1.clickOnDialPad();
//		Thread.sleep(2000);
//		phoneApp1.pressIVRKey1();
//		
//		Thread.sleep(10000);
//
//		phoneApp2.clickOnSideMenu();
//		String callerName2 = phoneApp2.getCallerName();
//		phoneApp2.clickOnAllCalls();
//		Thread.sleep(4000);
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
//		assertEquals("Incoming", phoneApp2.validatecallType());
//
//		phoneApp2.clickOnRightArrow();
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());
//		assertEquals("Call not setup", phoneApp2.validateCallStatusInCallDetail());
//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
//
//		driver4.get(url.signIn());
//		webApp2.clickOnActivitFeed();
//		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(),
//				"--Department name in web2 Activityfeed page--");
//		assertEquals("-", webApp2.validateCallerName(), "--Caller name in web2 Activityfeed page--");
//		assertEquals(number2, webApp2.validateClientNumber(), "--Client number in web2 Activityfeed page--");
//		assertEquals("Call not setup", webApp2.validateCallStatus(),
//				"--Completed call status in web2 Activityfeed page--");
//		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed(),
//				"Incoming call type in web2 Activityfeed page--");
//		assertEquals("-", webApp2.validateCallCost(), "--Call cost in web2 Activityfeed page--");
//		webApp2.clickStatusItagBtn();
//		assertEquals("User is not available due to Holiday", webApp2.validateStatusItag(),
//				"--Call not setup with reason in web2 Activityfeed page--");
//		webApp2.clickOniButton();
//		Common.pause(5);
//		assertEquals(false, webApp2.validateRecordingUrl(), "--Recording URL in web2 Activityfeed page--");
//
//	}

}