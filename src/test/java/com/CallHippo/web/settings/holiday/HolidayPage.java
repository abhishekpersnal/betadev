package com.CallHippo.web.settings.holiday;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class HolidayPage {

	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;

	public HolidayPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		js = (JavascriptExecutor) driver;
	}
	
	@FindBy(xpath = "//a[contains(.,'Dashboard')]")
	private WebElement left_panel_dashboard;
	
	@FindBy(xpath = "//h5[@class='titleboxdashicne'][contains(.,'Call Summary')]")
	private WebElement dashboard_callSummary_txt;

	

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	private WebElement successMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement errorMessage;
	
	@FindBy(xpath = "//button[contains(.,'Add Holiday')]") private WebElement addHoliday;
	@FindBy(xpath = "//input[@placeholder='Enter Holiday Name']") private WebElement holidayName;
	@FindBy(xpath = "//button[contains(.,'Save')]") private WebElement holidaySavebtn;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]") private WebElement yesButton;
	@FindBy(xpath = "//div[@class='ant-message-custom-content ant-message-success']") private WebElement successValidationMsg;
	@FindBy(xpath = "//span[contains(.,'terrainHolidays')]") private WebElement holidaySubMenu;
	@FindBy(xpath = "//button[@test-automation='setting_holiday_message_switch']") private WebElement holidayToggle;
	@FindBy(xpath = "//tbody//div[@class='ant-select-selection__rendered']") private WebElement allocationDropdown;
	@FindBy(xpath = "//div[@class='holidaySwitchContent']//span[contains(.,'Voicemail')]/preceding-sibling::span/span") private WebElement holidayVoicemailRedioButton;
	@FindBy(xpath = "//div[@class='holidaySwitchContent']//span[contains(.,'Greeting')]/preceding-sibling::span/span") private WebElement holidayGreetingRedioButton;
	@FindBy(xpath = "//span[@class='holidayActionbtnTd']//i[@class='material-icons'][contains(.,'edit')]") WebElement holidayEdit;
	@FindBy(xpath = "//span[@class='holidayActionbtnTd']//i[@class='material-icons'][contains(.,'delete_forever')]") WebElement holidayDelete;
	@FindBy(xpath = "//span[@class='holidayTableMoreBtn'][contains(.,'More')]") WebElement holidayMoreHyperlink;
	
	
	
	
	
	

	
	
	public void clickOnHolidaySubMenu() {
		wait.until(ExpectedConditions.visibilityOf(holidaySubMenu));
		js.executeScript("arguments[0].scrollIntoView();", holidaySubMenu);
		Common.pause(2);
		holidaySubMenu.click();

	}
	public void clickOnHolidayButton() {
		wait.until(ExpectedConditions.visibilityOf(addHoliday));
		js.executeScript("arguments[0].click();", addHoliday);
		
	}
	
	
	public void enterHolidayName(String holidayName) {
		wait.until(ExpectedConditions.visibilityOf(this.holidayName)).clear();
		wait.until(ExpectedConditions.visibilityOf(this.holidayName)).sendKeys(holidayName);
	}
	
	public void clickOnHolidaySaveButton() {
		wait.until(ExpectedConditions.visibilityOf(holidaySavebtn)).click();
		
	}
	
	
	public void clickLeftMenuDashboard() {
		wait.until(ExpectedConditions.elementToBeClickable(left_panel_dashboard));
		left_panel_dashboard.click();
		wait.until(ExpectedConditions.elementToBeClickable(dashboard_callSummary_txt));
		Common.pause(1);
	}
	
	public String validateSuccessValidationMsg() {
		return wait.until(ExpectedConditions.visibilityOf(successMessage)).getText();
		
	}
	
	public void deleteAllHolidayEntries() {
		Common.pause(2);
		List<WebElement> elements = driver.findElements(
				By.xpath("//span[@class='holidayActionbtnTd']//i[@class='material-icons'][contains(.,'delete_forever')]"));
		System.out.println("---elements.size()--" + elements.size());

		if (elements.size() > 0) {
			// for (WebElement element : elements) {
			for (int i = 0; i <= elements.size(); i++) {
				System.out.println("==== i ===" + i);
				List<WebElement> elements1 = driver.findElements(
						By.xpath("//span[@class='holidayActionbtnTd']//i[@class='material-icons'][contains(.,'delete_forever')]"));
				if (elements1.size() > 0) {
					elements1.get(0).click();
					wait.until(ExpectedConditions.visibilityOf(yesButton)).click();
				}
				Common.pause(3);

			}
			Common.pause(1);
		}
	}
	
	public void settingHolidayToggle(String value) {

		wait.until(ExpectedConditions.visibilityOf(holidayToggle));
		if (value.contentEquals("on")) {

			if (holidayToggle.getAttribute("aria-checked").equals("true")) {

			} else {
				holidayToggle.click();
			}
		} else {
			if (holidayToggle.getAttribute("aria-checked").equals("true")) {
				holidayToggle.click();
			} else {

			}
		}

	}
	
	public boolean validateIsHolidayAdded(String holidayName) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//td[@title='"+holidayName+"']"))));
			return true;
		} catch (Exception e) {

			return false;
		}
	}
	
	public void clickOnAllocationDropDown() {
		wait.until(ExpectedConditions.visibilityOf(allocationDropdown)).click();
	}
	
	public void selectAllocation(String value) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//li[@role=\"option\"][contains(.,'"+value+"')]")))).click();
		Common.pause(2);
	}
	
	public void DeselectUser(String User) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='holidayModalEmail'][contains(.,'"+User+"')]")))).click();
		Common.pause(2);
	}
	
	

	public boolean validateHolidaySectionTitle(String title) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='ant-table-column-title'][contains(.,'"+title+"')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void clickOnHolidayVoicemailRadioButton() {
//		wait.until(ExpectedConditions.visibilityOf(holidayVoicemailRedioButton)).click();
		try {
			js.executeAsyncScript("arguments[0].click();", holidayVoicemailRedioButton);
		} catch (Exception e) {
		}
	}
	
	public void clickOnHolidayGreetingRadioButton() {
//		wait.until(ExpectedConditions.visibilityOf(holidayVoicemailRedioButton)).click();
		try {
			js.executeAsyncScript("arguments[0].click();", holidayGreetingRedioButton);
		} catch (Exception e) {
		}
	}
	
	public void clickOnHolidayEditButton() {
		wait.until(ExpectedConditions.visibilityOf(holidayEdit)).click();
	}
	
	public void clickOnHolidayDeleteButton() {
		wait.until(ExpectedConditions.visibilityOf(holidayDelete)).click();
		wait.until(ExpectedConditions.visibilityOf(yesButton)).click();
	}
	
	public boolean validateMoreHyperLink() {
		try {
			wait.until(ExpectedConditions.visibilityOf(holidayMoreHyperlink));
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public boolean validateAssignuser(String user1, String user2) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='holidayAsignMoreText'][contains(.,'"+user1+","+user2+"')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
}
