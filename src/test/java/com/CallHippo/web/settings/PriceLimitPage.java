package com.CallHippo.web.settings;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class PriceLimitPage {

	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;

	public PriceLimitPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		js = (JavascriptExecutor) driver;
	}
	
	@FindBy(xpath = "//section[@id='priceLimit']//i[@class[contains(.,'material-icons')]][contains(.,'attach_money')]")
	WebElement pricelimit_icon;
	
	@FindBy(xpath = "//section[@id='priceLimit']//div[@class[contains(.,'deparmentname_title')]]/span[contains(.,'Outgoing Call Price Limit')]")
	WebElement outgoing_pricelimit_txt;
	
	@FindBy(xpath = "//section[@id='priceLimit']//div[@class[contains(.,'deparmentname_description')]]")
	WebElement outgoing_pricelimit_desc;
	
	@FindBy(xpath = "//section[@id='priceLimit']//div[@class[contains(.,'billingAddEmailInput')]]/input[@placeholder='Enter a value between $0.01 to $100']")
	WebElement outgoing_pricelimit_placeHolderTxt;
	
	@FindBy(xpath = "//section[@id='priceLimit']//button[@class[contains(.,'ant-btn billingSaveEmailBtn')]][@disabled[contains(.,'')]]")
	WebElement outgoing_pricelimit_save_disable_Btn;
	
	@FindBy(xpath = "//section[@id='priceLimit']//button[@class[contains(.,'ant-btn billingSaveEmailBtn')]]")
	WebElement outgoing_pricelimit_save_enable_Btn;
	
	@FindBy(xpath = "//section[@id='priceLimit']//div[@class[contains(.,'restrictedCallsNote')]]//span")
	WebElement outgoing_pricelimit_notes;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement validateErrorMsg;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span")
	private WebElement validateInfoMsg;
	
	@FindBy(xpath = "//h2[@class='subtitle'][contains(.,'Subscription Details')]")
	WebElement upgradepopup;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	private WebElement validateSuccessMsg;
	
	@FindBy(xpath = "//section[@id='priceLimit']//button[@class[contains(.,'priceLimitEditBtn')]]")
	List<WebElement> outgoing_pricelimit_edit_Btn;
	
	
	
	public boolean validatePriceLimitIcon() {
		try {
			if (pricelimit_icon.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public String validateOutgoingPricelimitTxt() {
		wait.until(ExpectedConditions.visibilityOf(outgoing_pricelimit_txt));
		return outgoing_pricelimit_txt.getText();
	}
	
	public String validateOutgoingPricelimitDesc() {
		wait.until(ExpectedConditions.visibilityOf(outgoing_pricelimit_desc));
		return outgoing_pricelimit_desc.getText();
	}
	
	public boolean validateOutgoingPricelimitPlaceHolderTxt() {
		try {
			if (outgoing_pricelimit_placeHolderTxt.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public String validateOutgoingPricelimitNotes() {
		wait.until(ExpectedConditions.visibilityOf(outgoing_pricelimit_notes));
		return outgoing_pricelimit_notes.getText();
	}
	
	public boolean validatePriceLimitSaveButtonIsDisable() {
		try {
			if (outgoing_pricelimit_save_disable_Btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public void enterPriceLimit(String priceLimitVal) {
		wait.until(ExpectedConditions.visibilityOf(outgoing_pricelimit_placeHolderTxt));
		outgoing_pricelimit_placeHolderTxt.clear();
		outgoing_pricelimit_placeHolderTxt.sendKeys(priceLimitVal);
	}
	
	public void clickOnPriceLimitSaveButton() {
		wait.until(ExpectedConditions.elementToBeClickable(outgoing_pricelimit_save_enable_Btn));
		outgoing_pricelimit_save_enable_Btn.click();
	}
	
	public String validateErrorMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateErrorMsg));
		return validateErrorMsg.getText();
	}
	
	public String validateInfoMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateInfoMsg));
		return validateInfoMsg.getText();
	}
	
	public void validateupgradepopup() {
		wait.until(ExpectedConditions.visibilityOf(upgradepopup));
	}
	
	public String validateSuccessMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateSuccessMsg));
		return validateSuccessMsg.getText();
	}
	
	public String validateTitle() {
		Common.pause(2);
		wait.until(ExpectedConditions.titleContains("Callhippo.com"));
		return driver.getTitle();
	}
	
	public void removePriceLimit() {
		Common.pause(2);
		System.out.println("---outgoing_pricelimit_edit_Btn.size()--" + outgoing_pricelimit_edit_Btn.size());

		if (outgoing_pricelimit_edit_Btn.size() > 0) {
			for (int i = 0; i <= outgoing_pricelimit_edit_Btn.size(); i++) {
					outgoing_pricelimit_edit_Btn.get(0).click();
					String priceLimitVal = outgoing_pricelimit_placeHolderTxt.getAttribute("value");
					for (int j=0 ; j<priceLimitVal.length();j++)
					{
						outgoing_pricelimit_placeHolderTxt.sendKeys(Keys.BACK_SPACE);
					}
					Common.pause(1);
					outgoing_pricelimit_save_enable_Btn.click();
					Common.pause(1);
					assertEquals(validateSuccessMessage(), "Outgoing call price removed successfully.");
				Common.pause(2);
			}
			Common.pause(1);
		}
	}
	
	public void updatePriceLimit() {
		Common.pause(2);
		System.out.println("---outgoing_pricelimit_edit_Btn.size()--" + outgoing_pricelimit_edit_Btn.size());

		if (outgoing_pricelimit_edit_Btn.size() > 0) {
			String priceLimitValueTxt = driver.findElement(By.xpath("//section[@id='priceLimit']//div[@class[contains(.,'billingAddEmailText')]]/p")).getText();
			System.out.println("----priceLimitVal---"+priceLimitValueTxt);
			outgoing_pricelimit_edit_Btn.get(0).click();
			String priceLimitVal = outgoing_pricelimit_placeHolderTxt.getAttribute("value");
			for (int j=0 ; j<priceLimitVal.length();j++)
			{
				outgoing_pricelimit_placeHolderTxt.sendKeys(Keys.BACK_SPACE);
			}
			enterPriceLimit("2.0");
			clickOnPriceLimitSaveButton();
		}
	}
}
