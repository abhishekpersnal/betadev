package com.CallHippo.web.settings;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.numberAndUserSubscription.AddNumberPage;

public class PriceLimitTest {

	WebDriver driverMail;
	WebDriver driverWebApp1;
	WebDriver driverWebApp2;
	WebDriver driverPhoneApp1;
	WebDriver driverPhoneApp2;
	
	SignupPage dashboard;
	WebToggleConfiguration webAppMainUserLogin;
	WebToggleConfiguration webApp2UserLogin;
	PriceLimitPage webApp1;
	PriceLimitPage webApp2;
	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerLoginPage phoneAppMainuser;
	
	Common signUpExcel;
	SignupPage registrationPage;
	AddNumberPage addNumberPage;
	//XLSReader planPrice;
	RestAPI planPriceAPI;
	PropertiesFile url;
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");
	
	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account1Number = data.getValue("account1Number1");
	String account2Number = data.getValue("account2Number1");
	
	String gmailUser = data.getValue("gmailUser");
	String getNumber;
	String planPriceBronzeMonthly,planPriceSilverMonthly,planPricePlatinumMonthly;
	String planPriceBronzeAnnually,planPriceSilverAnnually,planPricePlatinumAnnually;
	
	Common excelTestResult;

	public PriceLimitTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		signUpExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		//planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		planPriceAPI= new RestAPI();
	}
	
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			//driver.get(url);
			try {
				dashboard.deleteAllMail();
			}catch(Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail," Gmail issue ","BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}
	
	@BeforeTest
	public void createSessions() throws IOException {
		deleteAllMails();
		    planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
			
			planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
		
			planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
			
			planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
			
			planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
		
			planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
			
		
		// Web2 Main user session login
		driverWebApp2 = TestBase.init2();
		webApp2UserLogin = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		webApp2 = PageFactory.initElements(driverWebApp2, PriceLimitPage.class);
		registrationPage = PageFactory.initElements(driverWebApp2, SignupPage.class);
		addNumberPage = PageFactory.initElements(driverWebApp2, AddNumberPage.class);

		// Dialer Main user session login
		driverPhoneApp1 = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
		phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);

		// Web1 user session login
		driverWebApp1 = TestBase.init2();
		webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
		webApp1 = PageFactory.initElements(driverWebApp1, PriceLimitPage.class);

		// Dialer Second user session login
		driverPhoneApp2 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);
		phoneAppMainuser = PageFactory.initElements(driverPhoneApp2, DialerLoginPage.class);
			
		try {
			driverWebApp2.get(url.signIn());
			loginWeb(webApp2UserLogin, account2Email, account1Password);
			Common.pause(3);
			
		} catch (Exception e) {
			driverWebApp2.get(url.signIn());
			loginWeb(webApp2UserLogin, account2Email, account1Password);
			Common.pause(3);
		}
		
		try {
			driverPhoneApp2.get(url.dialerSignIn());
			loginDialer(phoneApp2, account2Email, account1Password);
			phoneApp2.waitForDialerPage();
		} catch (Exception e) {
			driverPhoneApp2.get(url.dialerSignIn());
			loginDialer(phoneApp2, account2Email, account1Password);
			phoneApp2.waitForDialerPage();
		}
		
		try {
			driverWebApp1.get(url.signIn());
			loginWeb(webAppMainUserLogin, account1Email, account1Password);
			Common.pause(3);
		} catch (Exception e) {
			driverWebApp1.get(url.signIn());
			loginWeb(webAppMainUserLogin, account1Email, account1Password);
			Common.pause(3);
		}

		try {
			driverPhoneApp1.get(url.dialerSignIn());
			loginDialer(phoneApp1, account1Email, account1Password);
			phoneApp1.waitForDialerPage();
		} catch (Exception e) {
			driverPhoneApp1.get(url.dialerSignIn());
			loginDialer(phoneApp1, account1Email, account1Password);
			phoneApp1.waitForDialerPage();
		}
	}
	
	@AfterTest(alwaysRun = true)
	public void endSessions() {
		driverWebApp1.quit();
		driverWebApp2.quit();
		driverPhoneApp1.quit();
		driverPhoneApp2.quit();
	}
	
	@BeforeMethod
	public void initialization() throws IOException {
		try {
			 driverWebApp1.navigate().to(url.signIn());
			 driverWebApp2.navigate().to(url.signIn());
			 driverPhoneApp1.navigate().to(url.dialerSignIn());
			 driverPhoneApp2.navigate().to(url.dialerSignIn());
			 Common.pause(3);
			 
			if (webAppMainUserLogin.validateWebAppLoggedinPage() == true ) {
				 loginWeb(webAppMainUserLogin, account1Email, account1Password);
				 Thread.sleep(9000);
			}
			if (webApp2UserLogin.validateWebAppLoggedinPage() == true ) {
				loginWeb(webApp2UserLogin, account2Email, account1Password);
				 Thread.sleep(9000);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp1, account1Email, account1Password);
				 Common.pause(3);
			}
				
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp2, account2Email, account1Password);
				 Common.pause(3);
			}
			webApp2UserLogin.clickLeftMenuSetting();
			webApp2UserLogin.scrollToPriceLimitTxt();
			webApp2.removePriceLimit();
			Common.pause(2);
		} catch (Exception e) {
			Common.Screenshot(driverWebApp1," WebApp1 Fail ","Before method - initialization");
			Common.Screenshot(driverWebApp2," WebApp2 Fail ","Before method - initialization");
			Common.Screenshot(driverPhoneApp1, " PhoneApp1 Fail ","Before method - initialization");
			Common.Screenshot(driverPhoneApp2, " PhoneApp2 Fail ","Before method - initialization");
			System.out.println("----Need to check issue - PriceLimitTest - Before method - initialization----");
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, "webApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "webApp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, "phoneApp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "phoneApp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
			
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, "webApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "webApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, "phoneApp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "phoneApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		}
	}
	
	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}
	
	private String signUpUser() throws Exception {
		driverWebApp2.get(url.livesignUp());
		Common.pause(1);
		String date = signUpExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---email1---"+email1);
		
		//Common.setPropertyData("Data\\url Configuration.properties", "inviteUserNumberNotAssign", email);
		
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signUpExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signUpExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signUpExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email1);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signUpExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signUpExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
		
		return email1;
	}
	
	private void LoginGmailAndConfirmYourMail() throws Exception {
		
		registrationPage.ValidateVerifyButtonOnGmail1();
		
		String actualTitle = webApp2.validateTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);
		
		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();
	}
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_SettingsPage_PriceLimit_Fields() {
		assertEquals(webApp2.validatePriceLimitIcon(),true);
		assertEquals(webApp2.validateOutgoingPricelimitTxt(),"Outgoing Call Price Limit");
		assertEquals(webApp2.validateOutgoingPricelimitDesc(),"Any calls costing more than the below-mentioned per minute call rates will not go through. To know more,Click here");
		assertEquals(webApp2.validateOutgoingPricelimitPlaceHolderTxt(),true);
		assertEquals(webApp2.validateOutgoingPricelimitNotes(),"Your monthly calling credit limit is set to $10000. If you want to increase it, mail us at support@callhippo.com.");
		assertEquals(webApp2.validatePriceLimitSaveButtonIsDisable(),true);
	}
	
	@DataProvider(name = "SaveButtonIsDisable")
	public static Object[] invalidPriceLimit() {

		return new Object[] {" ", "0",  "0.", "0. ", "0.0", "0.00", "0.000", "0  ","~!@#$%^&*()_+?><:;{}[]","ABCDEFGHIJKLMNOPQRSTUVWXYZ"};

	}

	@Test(priority = 2, dataProvider = "SaveButtonIsDisable", retryAnalyzer = Retry.class)
	public void verify_Invalid_PriceLimit_Validation(String priceLimitValue) {
		webApp2.enterPriceLimit(priceLimitValue);
		assertEquals(webApp2.validatePriceLimitSaveButtonIsDisable(),true);
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Error_message_for_basicPlan() throws Exception {
		
		logout(webApp2UserLogin);
		Common.pause(3);

		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();

		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		webApp2.validateupgradepopup();
		registrationPage.closeAddnumberPopup();
		
		registrationPage.userLogout();
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Success_message_for_bronze_Monthly_Plan() throws Exception {
		
		logout(webApp2UserLogin);
		Common.pause(3);
		
		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----"+signUpUserEmailId);
		
		LoginGmailAndConfirmYourMail();
		
		driverWebApp2.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(8);
		addNumberPage.clickOnCountry("United States");
		Common.pause(2);
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		
		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();
		
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, "1"));
		
//		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"), "----plan name");
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"), "----plan name");
//		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("monthly"),"----plan period");
		
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		
		addNumberPage.clickOnNotNowImDonePopup();
//		addNumberPage.clickOnSaveNumberButton();
		
		Common.pause(3);
		driverWebApp2.get(url.numbersPage());
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));
		
		registrationPage.userLogout();
		
		loginWeb(webApp2UserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
				
		registrationPage.closeAddnumberPopup();
		
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		
		
		String actualResult;
		String expectedResult;
		
		actualResult = webApp2.validateSuccessMessage();
		expectedResult = "Outgoing call price updated successfully.";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Success_message_for_bronze_Annually_Plan() throws Exception {
		
		logout(webApp2UserLogin);
		Common.pause(3);
		
		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----"+signUpUserEmailId);
		
		LoginGmailAndConfirmYourMail();
		
		driverWebApp2.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(8);
		addNumberPage.clickOnCountry("United States");
		Common.pause(2);
		
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, "1"));

		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"), "----plan name");
		//	assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("annually"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
//		addNumberPage.clickOnSaveNumberButton();
		Common.pause(3);
		driverWebApp2.get(url.numbersPage());
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));
		
		registrationPage.userLogout();
		
		loginWeb(webApp2UserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
				
		registrationPage.closeAddnumberPopup();
		
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		
		
		String actualResult;
		String expectedResult;
		
		actualResult = webApp2.validateSuccessMessage();
		expectedResult = "Outgoing call price updated successfully.";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Success_message_for_silver_Monthly_Plan() throws Exception {
		
		logout(webApp2UserLogin);
		Common.pause(3);
		
		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----"+signUpUserEmailId);
		
		LoginGmailAndConfirmYourMail();
		
		driverWebApp2.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(8);
		addNumberPage.clickOnCountry("United States");
		Common.pause(2);
		
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);

//		addNumberPage.selectSilverPlan(); by default silver plan is selected (changed from Sprint 27)
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPriceSilverMonthly);

		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Silver"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"), "----plan name");
		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("monthly"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
//		addNumberPage.clickOnSaveNumberButton();
		Common.pause(3);
		driverWebApp2.get(url.numbersPage());
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));
		
		registrationPage.userLogout();
		
		loginWeb(webApp2UserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
				
		registrationPage.closeAddnumberPopup();
		
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		
		String actualResult;
		String expectedResult;
		
		actualResult = webApp2.validateSuccessMessage();
		expectedResult = "Outgoing call price updated successfully.";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Success_message_for_silver_Annually_Plan() throws Exception {
		
		logout(webApp2UserLogin);
		Common.pause(3);
		
		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----"+signUpUserEmailId);
		
		LoginGmailAndConfirmYourMail();
		
		driverWebApp2.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(8);
		addNumberPage.clickOnCountry("United States");
		Common.pause(2);
		
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));

//		addNumberPage.selectSilverPlan();  by default selver plan is selected. (changed from CAL sprint 27)
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPriceSilverAnnually);

		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Silver"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"), "----plan name");
		//	assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("annually"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
//		addNumberPage.clickOnSaveNumberButton();
		Common.pause(3);
		driverWebApp2.get(url.numbersPage());
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));
		
		registrationPage.userLogout();
		
		loginWeb(webApp2UserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
				
		registrationPage.closeAddnumberPopup();
		
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		
		String actualResult;
		String expectedResult;
		
		actualResult = webApp2.validateSuccessMessage();
		expectedResult = "Outgoing call price updated successfully.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Success_message_for_Platinum_Annually_Plan() throws Exception {
		
		logout(webApp2UserLogin);
		Common.pause(3);
		
		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----"+signUpUserEmailId);
		
		LoginGmailAndConfirmYourMail();
		
		driverWebApp2.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(8);
		addNumberPage.clickOnCountry("United States");
		Common.pause(2);
		
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println("+++++numberBeforePurchased==="+numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));

		addNumberPage.selectPlatinumPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPricePlatinumAnnually);

		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Platinum"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"), "----plan name");
		//	assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("annually"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
//		addNumberPage.clickOnSaveNumberButton();
		Common.pause(3);
		driverWebApp2.get(url.numbersPage());
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));
		
		registrationPage.userLogout();
		
		loginWeb(webApp2UserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
				
		registrationPage.closeAddnumberPopup();
		
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		
		String actualResult;
		String expectedResult;
		
		actualResult = webApp2.validateSuccessMessage();
		expectedResult = "Outgoing call price updated successfully.";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Update_Message_for_Platinum_Annually_Plan() throws Exception {
        
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		webApp2.enterPriceLimit("1.0");
		webApp2.clickOnPriceLimitSaveButton();
		webApp2.updatePriceLimit();
		assertEquals(webApp2.validateSuccessMessage(), "Outgoing call price updated successfully.");
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_validate_PriceLimit_Error_Message_Call_From_DialPad() throws Exception {
		
		registrationPage.userLogout();
		loginWeb(webApp2UserLogin, account2Email, data.getValue("masterPassword"));
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		webApp2.enterPriceLimit("0.020");
		webApp2.clickOnPriceLimitSaveButton();
		assertEquals(webApp2.validateSuccessMessage(), "Outgoing call price updated successfully.");
		
		webApp2UserLogin.deleteAllNumbersFromBlackList();
		
		
		phoneApp2.enterNumberinDialer(account1Number);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		
		String Number = phoneApp2.validateCallingScreenOutgoingNumber();
		String Via = phoneApp2.validateCallingScreenOutgoingVia();
		// validate outgoing Number on outgoing calling screen
		assertEquals(Number,account1Number);
		// Validate outgoingVia on outgoing calling screen
		assertEquals(Via,"Outgoing Via " + departmentName2);
		
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(account1Number, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(account1Number, phoneApp2.validateNumberInCallDetail());

		assertEquals("Call not setup", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(account1Number);
		phoneApp2.clickOnDialButton();
		
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailOutgoingIcon());
		phoneApp2.waitForDialerPage();
		Thread.sleep(2000);
		
		driverPhoneApp1.get(url.dialerSignIn());
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(2000);
		phoneApp1.enterNumberinDialer(account2Number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailOutgoingIcon());
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		
	}
	
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_validate_PriceLimit_Error_Message_Call_From_Redial() throws Exception {
		registrationPage.userLogout();
		loginWeb(webApp2UserLogin, account2Email, data.getValue("masterPassword"));
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		webApp2.enterPriceLimit("0.020");
		webApp2.clickOnPriceLimitSaveButton();
		assertEquals(webApp2.validateSuccessMessage(), "Outgoing call price updated successfully.");
		
		webApp2UserLogin.deleteAllNumbersFromBlackList();
		
		
		phoneApp2.clickOndialDisableButton();
		Common.pause(1);
		phoneApp2.clickOnDialButton();
		Common.pause(2);
		
	}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_validate_PriceLimit_Error_Message_Call_From_CallDetailPage() throws Exception {
		registrationPage.userLogout();
		loginWeb(webApp2UserLogin, account2Email, data.getValue("masterPassword"));
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		webApp2.enterPriceLimit("0.020");
		webApp2.clickOnPriceLimitSaveButton();
		assertEquals(webApp2.validateSuccessMessage(), "Outgoing call price updated successfully.");
		
		webApp2UserLogin.deleteAllNumbersFromBlackList();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		Common.pause(2);
		phoneApp2.clickCallBtnOnCallDetailsPage();
		Common.pause(2);
	}
	
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_validate_PriceLimit_Error_Message_Call_From_AllCalls() throws Exception {
		registrationPage.userLogout();
		loginWeb(webApp2UserLogin, account2Email, data.getValue("masterPassword"));
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		webApp2.enterPriceLimit("0.020");
		webApp2.clickOnPriceLimitSaveButton();
		assertEquals(webApp2.validateSuccessMessage(), "Outgoing call price updated successfully.");
		
		webApp2UserLogin.deleteAllNumbersFromBlackList();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnAllCallsNumber();
		Common.pause(2);
	}
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void dialer_validate_PriceLimit_Error_Message_Call_From_CallPlanner() throws Exception {
		registrationPage.userLogout();
		loginWeb(webApp2UserLogin, account2Email, data.getValue("masterPassword"));
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2UserLogin.scrollToPriceLimitTxt();
		webApp2.removePriceLimit();
		webApp2.enterPriceLimit("0.020");
		webApp2.clickOnPriceLimitSaveButton();
		assertEquals(webApp2.validateSuccessMessage(), "Outgoing call price updated successfully.");
		
		webApp2UserLogin.deleteAllNumbersFromBlackList();
		
		phoneApp2.clickOnSideMenu();
		Common.pause(1);
		phoneApp2.clickOnCallPlannerLeftMenu();
		phoneApp2.deleteAllRemindersFromCallPlanner();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		
	    phoneApp2.clickOnCallDetailsReminderBtn();
		phoneApp2.selectValueForReminder();
		assertEquals(phoneApp2.validationMessage(), "Successfully set call reminder");
		phoneApp2.clickOnLeftArrow();
		Common.pause(1);
		
		phoneApp2.clickOnSideMenu();
		Common.pause(1);
		phoneApp2.clickOnCallPlannerLeftMenu();
		Common.pause(2);
		phoneApp2.clickOnCallBtnOnCallReminderPage();
	}
	
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_PriceLimit_Is_Not_Set() throws Exception {
		registrationPage.userLogout();
		loginWeb(webApp2UserLogin, account2Email, data.getValue("masterPassword"));
		webApp2UserLogin.clickLeftMenuSetting();
		webApp2UserLogin.scrollToPriceLimitTxt();
		assertEquals(webApp2.validatePriceLimitIcon(),true);
		assertEquals(webApp2.validateOutgoingPricelimitTxt(),"Outgoing Call Price Limit");
		assertEquals(webApp2.validateOutgoingPricelimitDesc(),"Any calls costing more than the below-mentioned per minute call rates will not go through. To know more,Click here");
		assertEquals(webApp2.validateOutgoingPricelimitPlaceHolderTxt(),true);
		assertEquals(webApp2.validateOutgoingPricelimitNotes(),"Your monthly calling credit limit is set to $10000. If you want to increase it, mail us at support@callhippo.com.");
		assertEquals(webApp2.validatePriceLimitSaveButtonIsDisable(),true);
	}
	
}
