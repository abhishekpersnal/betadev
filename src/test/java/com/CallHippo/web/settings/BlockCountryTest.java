package com.CallHippo.web.settings;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
//import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.AbstractPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;


public class BlockCountryTest
{
	DialerIndex phoneApp1;
	WebDriver driverPhoneApp1;
	DialerIndex phoneApp2;
	WebDriver driverPhoneApp2;
	WebToggleConfiguration webApp1;
	WebDriver driverWebApp1;
	WebToggleConfiguration webApp2;
	WebDriver driverWebApp2;
	RestAPI blockCountryAPI;
	static Common excel;
	RestAPI countryBlockAPI;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");
	
	
//	blockCountryAPI= new RestAPI();
	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2Number1 = data.getValue("account2Number1"); // account 2's number
	String account1Number1 = data.getValue("account1Number1"); // account 1's number
//	String blockAfghanistan = data.getValue("AfghanistanNumber1");

	String callCost1 = data.getValue("oneMinuteCallCost");
    String callCost2 = data.getValue("twoMinuteCallCost");
    String account2UserId = data.getValue("UserId");
    
    

	public BlockCountryTest() throws Exception {
		
		excel = new Common("Data\\BlockCountry1.xlsx");
		softAssert = new SoftAssert();
		countryBlockAPI= new RestAPI();
	}


	// before test is opening all needed sessions 
	@BeforeTest
	public void initialization() throws Exception {
		
	
		driverPhoneApp1 = TestBase.init();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
		
//		driverWebApp2 = TestBase.init();
//		System.out.println("Opened webaap2 session");
//		webApp2 = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
//		
//		driverPhoneApp2 = TestBase.init();
//		System.out.println("Opened phoneAap2 session");
//		phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);
		
	try {
//		try {
//			driverWebApp1.get(url.signIn());
//			Common.pause(4);
//			System.out.println("Opened webaap signin Page");
//			loginWeb(webApp1, account2Email, account1Password);
//		}catch(Exception e) {
//			driverWebApp1.get(url.signIn());
//			Common.pause(4);
//			System.out.println("Opened webaap signin Page");
//			loginWeb(webApp1, account2Email, account1Password);
//		}
		
		try {
			driverPhoneApp1.get(url.dialerSignIn());
			Common.pause(4);
			System.out.println("Opened phoneAap1 signin page");
			loginDialer(phoneApp1, account1Email, account1Password);
			System.out.println("loggedin phoneAap1");
		}catch(Exception e) {
			driverPhoneApp1.get(url.dialerSignIn());
			Common.pause(4);
			System.out.println("Opened phoneAap1 signin page");
			loginDialer(phoneApp1, account1Email, account1Password);
			System.out.println("loggedin phoneAap1");
		}
		
		
//		
//		try {
//			System.out.println("loggedin webApp");
//			Thread.sleep(9000);
//			webApp1.numberspage();
			
//			webApp1.navigateToNumberSettingpage(account2Number1);
//			Thread.sleep(9000);
//			webApp1.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//			webApp1.userTeamAllocation("users");
//			Thread.sleep(2000);
//			webApp1.userspage();
//			webApp1.navigateToUserSettingPage(account2Email);
//			Thread.sleep(9000);
//			webApp1.setUserSttingToggles("off", "off", "off", "open", "off");
	
//		}catch(Exception e) {
//			System.out.println("loggedin webApp");
//			Thread.sleep(9000);
//			webApp1.numberspage();
//			
//			webApp1.navigateToNumberSettingpage(account2Number1);
//			Thread.sleep(9000);
//			webApp1.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//			webApp1.userTeamAllocation("users");
//			Thread.sleep(2000);
//			webApp1.userspage();
//			webApp1.navigateToUserSettingPage(account2Email);
//			Thread.sleep(9000);
//			webApp1.setUserSttingToggles("off", "off", "off", "open", "off");
	
//		}
	}catch(Exception e) {
		String testname = "Normal calling Before Method ";
		Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Fail login");
//		Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Fail login");
//		Common.Screenshot(driverWebApp1, testname, "webAap1 Fail login");
//		Common.Screenshot(driverWebApp2, testname, "webAap2 Fail login");
		System.out.println("\n"+testname+" has been fail... \n");
	}
}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			
			driverPhoneApp1.navigate().to(url.dialerSignIn());
//			driverPhoneApp2.navigate().to(url.dialerSignIn());
//			driverWebApp1.get(url.signIn());
//			driverWebApp2.get(url.signIn());
//			Common.pause(3);
//			 if (webApp1.validateWebAppLoggedinPage() == true) {
//			 loginWeb(webApp2, account2Email, account2Password);
//			 Common.pause(3);
//				 }
			 if (phoneApp1.validateDialerAppLoggedinPage() == true) {
			 loginDialer(phoneApp1, account2Email, account2Password);
			 Common.pause(3);
			 }
		} catch (Exception e) {
			
			try {
				Common.pause(3);
				driverPhoneApp1.navigate().to(url.dialerSignIn());
		//		driverPhoneApp2.navigate().to(url.dialerSignIn());
		//		driverWebApp1.get(url.signIn());
		//		driverWebApp2.get(url.signIn());
				Common.pause(3);

//				 if (webApp1.validateWebAppLoggedinPage() == true) {
//				 loginWeb(webApp1, account2Email, account2Password);
//				 Common.pause(3);
//				 }
				 if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp1, account2Email, account2Password);
				 Common.pause(3);
				 }
			} catch (Exception e1) {
				String testname = "Normal calling Before Method ";
				Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Fail login");
	//			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driverWebApp1, testname, "webAap1 Fail login");
	//			Common.Screenshot(driverWebApp2, testname, "webAap2 Fail login");
				System.out.println("\n"+testname+" has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
	//		Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
	//		Common.Screenshot(driverWebApp1, testname, "webApp1 Fail " + result.getMethod().getMethodName());
	//		Common.Screenshot(driverWebApp2, testname, "webApp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - "+result.getMethod().getMethodName());
			
		} else {
			String testname = "Pass";
			Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
//			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
//			Common.Screenshot(driverWebApp1, testname, "webApp1 Pass " + result.getMethod().getMethodName());
//			Common.Screenshot(driverWebApp2, testname, "webAap2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - "+result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driverPhoneApp1.quit();
//		driverPhoneApp2.quit();
//		driverWebApp1.quit();
//		driverWebApp2.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	@DataProvider(name = "BlockCountry")
	public static Object[][] BlockCountry() {
		return new Object[][] { { excel.getdata(0, 1, 1), excel.getdata(0, 1, 2), excel.getdata(0, 1, 3) },
				{ excel.getdata(0, 2, 1), excel.getdata(0, 2, 2), excel.getdata(0, 2, 3) },
				{ excel.getdata(0, 3, 1), excel.getdata(0, 3, 2), excel.getdata(0, 3, 3) } };
	}
	
	
	@Test(priority = 1, dataProvider = "BlockCountry", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void CallFromDailer_BlockCountry(String countryName, String countryCode,  String number)throws Exception {
	

		countryBlockAPI.blockCountryAPI(account2UserId, countryName , countryCode);
		
		Thread.sleep(2000);
		driverPhoneApp1.get(url.dialerSignIn());
		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		System.out.println("=====Outgoing Call to check Block API ::"+ account2Number1);
		assertEquals("You are not allowed to make outgoing calls on this country, contact support for enable calling", phoneApp1.validateBlockCountryError());
		
	}
	
	@Test(priority = 2, dataProvider = "BlockCountry", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void CallFromDailer_UnBlockCountry(String countryName, String countryCode,  String number)throws Exception {
		
		countryBlockAPI.unblockCountryAPI(account2UserId, countryName , countryCode);
		
		Thread.sleep(2000);
		driverPhoneApp1.get(url.dialerSignIn());
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.enterNumberinDialer(number);
		System.out.println("=====");
		phoneApp1.clickOnDialButton();
		System.out.println("===Outgoing Call to check UnBlock API ::"+ number);
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());
		Thread.sleep(3000);
		phoneApp1.clickOnOutgoingHangupButton();
	
	}
	
}