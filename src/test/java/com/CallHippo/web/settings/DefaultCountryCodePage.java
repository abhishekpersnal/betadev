package com.CallHippo.web.settings;

import static org.testng.Assert.expectThrows;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class DefaultCountryCodePage {

	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;

	public DefaultCountryCodePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		js = (JavascriptExecutor) driver;
	}
	
	@FindBy(xpath = "//a[contains(.,'Dashboard')]")
	private WebElement left_panel_dashboard;
	
	@FindBy(xpath = "//h5[@class='titleboxdashicne'][contains(.,'Call Summary')]")
	private WebElement dashboard_callSummary_txt;

	@FindBy(xpath = "//a[contains(.,'Setting')]")
	private WebElement leftMenu_setting;

	@FindBy(xpath = "//span[contains(.,'Your Plan')]")
	WebElement yourPlan_Txt;

	@FindBy(xpath = "//span[contains(.,'SDAP')]")
	WebElement sdap_Txt;
	
	@FindBy(xpath = "//span[contains(.,'library_booksDefault Country   Code')]")
	WebElement submenuDefaultCountryCode;
	
	@FindBy(xpath = "//h3[contains(.,'Default Country Code')]")
	WebElement defaultCountryCodeSection;
	
	@FindBy(xpath = "//h3[contains(.,'Default Country Code')]/..//div[@class='deparmentname_description']")
	WebElement description;
	
	@FindBy(xpath = "//div[@class='defaultCCPhoneInputWrapper react-tel-input']/input")
	WebElement countrycodeTextBox;
	
	@FindBy(xpath = "//section[@id='defaultCountryCode']//button[1]")
	WebElement saveButton;
	
	@FindBy(xpath = "//section[@id='defaultCountryCode']//button[2]")
	WebElement crossButton;

	@FindBy(xpath = "//section[@id='defaultCountryCode']//button[@class='ant-btn billingTrashEmailBtn']")
	WebElement deleteButton;
	
	public void clickLeftMenuDashboard() {
		wait.until(ExpectedConditions.elementToBeClickable(left_panel_dashboard));
		left_panel_dashboard.click();
		wait.until(ExpectedConditions.elementToBeClickable(dashboard_callSummary_txt));
		Common.pause(1);
	}
	
	public void clickLeftMenuSetting() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_setting));
		leftMenu_setting.click();
		wait.until(ExpectedConditions.elementToBeClickable(sdap_Txt));
		Common.pause(1);
	}

	public void clickOnDefaultCountryCodeSideMenu() {
		wait.until(ExpectedConditions.visibilityOf(submenuDefaultCountryCode));
		js.executeScript("arguments[0].scrollIntoView();", submenuDefaultCountryCode);
		
		submenuDefaultCountryCode.click();
		Common.pause(2);
	}
	
	public void ScrollToDefaultCountryCodeSection() {
		wait.until(ExpectedConditions.visibilityOf(defaultCountryCodeSection));
		js.executeScript("arguments[0].scrollIntoView();", defaultCountryCodeSection);
		Common.pause(2);
	}
	
	public boolean validateDefaultCountryCodeSectionIsDisplayed() {
		
		if(defaultCountryCodeSection.isDisplayed()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	
	public String validatedDescriptionOfDefaultCountryCode() {
		String descriptions =  wait.until(ExpectedConditions.visibilityOf(description)).getText();
		System.out.println(descriptions);
		return descriptions;
	}
	public void selectCountryName(String countryName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[contains(@class,'defaultCountryCodeSaveBtn')]")))).click();
		Common.pause(1);
		
		Actions action = new Actions(driver);
		
		action.moveToElement(driver.findElement(By.xpath("//span[normalize-space()='"+countryName+"']"))).build().perform();
		driver.findElement(By.xpath("//span[normalize-space()='"+countryName+"']")).click();
		Common.pause(2);
	}
	
	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton)).click();
	}
	

	public void clickOnCrossButton() {
		wait.until(ExpectedConditions.visibilityOf(crossButton)).click();
	}
	
	public void clickOnDeleteButton() {
		
		try {
			deleteButton.click();
		} catch (Exception e) {
		}
	}
	
	public void deleteCountryCode() {
		Common.pause(2);
		List<WebElement> editButtonList = driver.findElements(
				By.xpath("//button[@class='ant-btn defaultCountryCodeSaveBtn'][contains(.,'edit')]"));
		
		List<WebElement> deleteButtonList = driver.findElements(
				By.xpath("//button[@class='ant-btn billingTrashEmailBtn'][contains(.,'delete_forever')]"));

		if (editButtonList.size() > 0 && deleteButtonList.size() > 0) {
			for (int i = 0; i <= deleteButtonList.size(); i++) {
				List<WebElement> deleteButtonListNew = driver.findElements(
						By.xpath("//button[@class='ant-btn billingTrashEmailBtn'][contains(.,'delete_forever')]"));
				if (deleteButtonListNew.size() > 0) {
					deleteButtonListNew.get(0).click();
				}
				Common.pause(2);

			}
			Common.pause(1);
		}
	}
	

}
