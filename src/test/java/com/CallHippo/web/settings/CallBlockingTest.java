package com.CallHippo.web.settings;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.credit.CreditWebSettingPage;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class CallBlockingTest {

	WebDriver driverWebApp1;
	WebDriver driverWebApp2;
	WebDriver driverWebApp3;
	
	WebDriver driverPhoneApp1;
	WebDriver driverPhoneApp2;
	WebDriver driverPhoneApp3;
	
	WebToggleConfiguration webApp1Account;
	WebToggleConfiguration webApp2SubuserAccount;
	WebToggleConfiguration webApp2Account;
	
	CallBlockingPage webApp1;
	CallBlockingPage webApp2;
	CallBlockingPage webApp3;
	
	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerIndex phoneApp3;
	
	DialerLoginPage phoneAppMainuser;
	
	HolidayPage webApp2HolidayPage;

	RestAPI creditAPI;
	PropertiesFile url;
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");
	
	String account1Email = data.getValue("account1MainEmail");
	String account1SubEmail = data.getValue("account1SubEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account1Number1 = data.getValue("account1Number1");
	String account2Number1 = data.getValue("account2Number1");
	
	Common excelTestResult;

	public CallBlockingTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		creditAPI = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}
	

	
	@BeforeTest
	public void createSessions() throws IOException, InterruptedException {
		// Web Main user session login
		driverWebApp1 = TestBase.init2();
		driverWebApp1.get(url.signIn());
		Common.pause(4);
		webApp1Account = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
		webApp1 = PageFactory.initElements(driverWebApp1, CallBlockingPage.class);
		
		// Web Second user session login
		driverWebApp2 = TestBase.init2();
		driverWebApp2.get(url.signIn());
		Common.pause(4);
		webApp2Account = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		webApp2 = PageFactory.initElements(driverWebApp2, CallBlockingPage.class);
		webApp2HolidayPage = PageFactory.initElements(driverWebApp2, HolidayPage.class);

		
		// Dialer Main user session login
		driverPhoneApp1 = TestBase.init_dialer();
		driverPhoneApp1.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp1 = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);
//		phoneAppMainuser = PageFactory.initElements(driverPhoneApp1, DialerLoginPage.class);

		// Dialer Second user session login
		driverPhoneApp2 = TestBase.init_dialer();
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);
//		phoneAppMainuser = PageFactory.initElements(driverPhoneApp2, DialerLoginPage.class);
		
		try {
			loginWeb(webApp1Account, account1Email, account1Password);
			Common.pause(3);
			
		} catch (Exception e) {
			loginWeb(webApp1Account, account1Email, account1Password);
			Common.pause(3);
		}

		try {
			loginDialer(phoneApp1, account1Email, account1Password);
			phoneApp1.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp1, account1Email, account1Password);
			phoneApp1.waitForDialerPage();
		}
		
		try {
			loginWeb(webApp2Account, account2Email, account1Password);
			Common.pause(3);
			
		} catch (Exception e) {
			loginWeb(webApp2Account, account2Email, account1Password);
			Common.pause(3);
		}

		try {
			loginDialer(phoneApp2, account2Email, account1Password);
			phoneApp2.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp2, account2Email, account1Password);
			phoneApp2.waitForDialerPage();
		}
		
		try {
			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			driverPhoneApp2.get(url.dialerSignIn());

			
			System.out.println("loggedin webApp");
			Thread.sleep(9000);
			webApp2Account.numberspage();

			webApp2Account.navigateToNumberSettingpage(account2Number1);

			Thread.sleep(9000);
			
			webApp2Account.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
			Thread.sleep(2000);
			webApp2Account.selectMessageType("Welcome message","Text");
			webApp2Account.selectMessageType("Voicemail","Text");
			webApp2Account.clickonAWHMVoicemail();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.clickonAWHMGreeting();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.selectMessageType("Wait Music","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
			webApp2Account.selectMessageType("IVR","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
			Thread.sleep(3000);
			
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2Account.userTeamAllocation("users");
			webApp2Account.Stickyagent("off","Strictly");
			Common.pause(3);
			webApp2Account.customRingingTime("off"); 
			System.out.println("customRingingTime OFF");
			Common.pause(3);
			webApp2Account.selectSpecificUserOrTeam(callerName2, "Yes");
			Common.pause(3);
			
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			
			webApp2Account.setUserSttingToggles("off", "off", "on", "open", "off");
			webApp2Account.selectMessageType("Voicemail","Text");
			Common.pause(2);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(2);
			
			webApp2Account.makeDefaultNumber(account2Number1);

			String Url = driverWebApp2.getCurrentUrl();
			creditAPI.UpdateCredit("500", "0", "0", webApp2Account.getUserId(Url));
			creditAPI.UpdateCreditCountyWisezero(webApp2Account.getUserId(Url));

			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2EmailSubUser);
			Thread.sleep(9000);
			webApp2Account.makeDefaultNumber(account2Number1);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");

			driverPhoneApp2.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
			driverPhoneApp2.get(url.dialerSignIn());

			phoneApp1.afterCallWorkToggle("off");
			Common.pause(3);
			driverPhoneApp1.get(url.dialerSignIn());
			phoneApp2.afterCallWorkToggle("off");
			Common.pause(3);
			driverPhoneApp2.get(url.dialerSignIn());
			Common.pause(2);
			driverWebApp2.get(url.signIn());
			webApp2Account.clickLeftMenuSetting();
			Common.pause(2);
			webApp2HolidayPage.clickOnHolidaySubMenu();
			webApp2HolidayPage.deleteAllHolidayEntries();
			webApp2Account.scrollToPriceLimitTxt();
			webApp2Account.removePriceLimit();

		} catch (Exception e) {
			
			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			driverPhoneApp2.get(url.dialerSignIn());

			
			System.out.println("loggedin webApp");
			Thread.sleep(9000);
			webApp2Account.numberspage();

			webApp2Account.navigateToNumberSettingpage(account2Number1);

			Thread.sleep(9000);
			
			webApp2Account.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
			Thread.sleep(2000);
			webApp2Account.selectMessageType("Welcome message","Text");
			webApp2Account.selectMessageType("Voicemail","Text");
			webApp2Account.clickonAWHMVoicemail();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.clickonAWHMGreeting();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.selectMessageType("Wait Music","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
			webApp2Account.selectMessageType("IVR","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
			Thread.sleep(3000);
			
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2Account.userTeamAllocation("users");
			webApp2Account.Stickyagent("off","Strictly");
			Common.pause(3);
			webApp2Account.customRingingTime("off"); 
			System.out.println("customRingingTime OFF");
			Common.pause(3);
			webApp2Account.selectSpecificUserOrTeam(callerName2, "Yes");
			Common.pause(3);
			
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			
			webApp2Account.setUserSttingToggles("off", "off", "on", "open", "off");
			webApp2Account.selectMessageType("Voicemail","Text");
			Common.pause(2);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(2);
			
			webApp2Account.makeDefaultNumber(account2Number1);

			String Url = driverWebApp2.getCurrentUrl();
			creditAPI.UpdateCredit("500", "0", "0", webApp2Account.getUserId(Url));
			creditAPI.UpdateCreditCountyWisezero(webApp2Account.getUserId(Url));

			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2EmailSubUser);
			Thread.sleep(9000);
			webApp2Account.makeDefaultNumber(account2Number1);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");

			driverPhoneApp2.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
			driverPhoneApp2.get(url.dialerSignIn());

			phoneApp1.afterCallWorkToggle("off");
			Common.pause(3);
			driverPhoneApp1.get(url.dialerSignIn());
			phoneApp2.afterCallWorkToggle("off");
			Common.pause(3);
			driverPhoneApp2.get(url.dialerSignIn());
			Common.pause(2);
			driverWebApp2.get(url.signIn());
			webApp2Account.clickLeftMenuSetting();
			Common.pause(2);
			webApp2HolidayPage.clickOnHolidaySubMenu();
			webApp2HolidayPage.deleteAllHolidayEntries();
			webApp2Account.scrollToPriceLimitTxt();
			webApp2Account.removePriceLimit();
		}
		
	}
	
	@AfterTest(alwaysRun = true)
	public void endSessions() {
		
		try {
			webApp2.clickLeftMenuDashboard();
			Common.pause(2);
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2.scrollToCallBlockingTxt();
			Common.pause(1);
			webApp2.deleteAllNumbersFromBlackList();
		}catch(Exception e) {
			webApp2.clickLeftMenuDashboard();
			Common.pause(2);
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2.scrollToCallBlockingTxt();
			Common.pause(1);
			webApp2.deleteAllNumbersFromBlackList();
		}finally {
		driverWebApp1.quit();
		driverWebApp2.quit();
		driverPhoneApp1.quit();
		driverPhoneApp2.quit();
		}
	}
	@BeforeMethod
	public void initialization() throws Exception {
		try {
			 driverWebApp1.navigate().to(url.signIn());
			 driverWebApp2.navigate().to(url.signIn());
			 driverPhoneApp1.navigate().to(url.dialerSignIn());
			 driverPhoneApp2.navigate().to(url.dialerSignIn());
			 Common.pause(3);
			
			if (webApp1Account.validateWebAppLoggedinPage() == true ) {
				 loginWeb(webApp1Account, account1Email, account1Password);
				 Thread.sleep(9000);
			}
			if (webApp2Account.validateWebAppLoggedinPage() == true ) {
				loginWeb(webApp2Account, account2Email, account1Password);
				 Thread.sleep(9000);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp1, account1Email, account1Password);
			}
				
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp2, account2Email, account1Password);
			}
			
			webApp2.clickLeftMenuDashboard();
			Common.pause(2);
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2.scrollToCallBlockingTxt();
			Common.pause(1);
			webApp2.deleteAllNumbersFromBlackList();
			
			phoneApp1.deleteAllContactsFromDialer();
			phoneApp2.deleteAllContactsFromDialer();
			
			driverPhoneApp1.navigate().to(url.dialerSignIn());
			driverPhoneApp2.navigate().to(url.dialerSignIn());
			
		} catch (Exception e) {
			Common.Screenshot(driverWebApp1," WebApp1 - Fail - ","Before method - initialization");
			Common.Screenshot(driverWebApp2," WebApp2 - Fail - ","Before method - initialization");
			Common.Screenshot(driverPhoneApp1, " PhoneApp1 - Fail - ","CallBlockingTest - Before method - initialization");
			Common.Screenshot(driverPhoneApp2, " PhoneApp2 - Fail - ","CallBlockingTest - Before method - initialization");
			System.out.println("----Need to check issue - CallBlockingTest - Before method - initialization----");
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, "webApp1 - Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "webApp2 - Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, "phoneApp1 - Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "phoneApp2 - Fail - " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, "webApp1 - Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "webApp2 - Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, "phoneApp1 - Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "phoneApp2 - Pass - " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}
	
	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_CallBlocking_Block_SettingsPage() throws IOException {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.verifyFieldsInCallBlocking();
	}
		
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_textBox_display_toAddnumber() {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_able_toAddnumber_withOut_countryCode() {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox("3513004405");
		webApp2.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2.verifySuccessMessage());
		webApp2.deleteBlackListNumber("3513004405");
		assertEquals("Number Removed From Blacklist successfully", webApp2.verifySuccessMessage());
		Common.pause(2);
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_saveBtn_disabled_for_empty_number() {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox("");
		assertFalse(webApp2.verifySaveBtnIsDisabled());
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_multiple_number() {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox("3513004405");
		webApp2.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2.verifySuccessMessage());
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox("351300440");
		webApp2.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2.verifySuccessMessage());
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class, dependsOnMethods = "add_multiple_number")
	public void verify_multiple_number_delete_from_blackList() throws InterruptedException {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox("3513004405");
		webApp2.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2.verifySuccessMessage());
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox("351300440");
		webApp2.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2.verifySuccessMessage());
		
		webApp2.deleteBlackListNumber("3513004405");
		assertEquals("Number Removed From Blacklist successfully", webApp2.verifySuccessMessage());
		Common.pause(2);
		webApp2.deleteBlackListNumber("351300440");
		assertEquals("Number Removed From Blacklist successfully", webApp2.verifySuccessMessage());
		Common.pause(2);
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_special_character_not_allow_in_number() {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		
		webApp2.enterNumberIntoTextBox("!@#$%^&*()_|}{:?><~");
		assertTrue(webApp2.verifySpecialCharacterNotAbleToEnter());
		assertFalse(webApp2.verifySaveBtnIsDisabled());
		Common.pause(2);
		webApp2.enterNumberIntoTextBox("123###$%^&4342");
		assertTrue(webApp2.verifySpecialCharacterNotAbleToEnter());
		assertTrue(webApp2.verifySaveBtnIsDisabled());
		Common.pause(2);
	}
	
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validation_for_duplicate_number() {
		// webApp2.clickLeftMenuSetting(); 
		// webApp2.scrollToCallBlockingTxt();
 		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		
		webApp2.enterNumberIntoTextBox("3513004405");
		webApp2.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2.verifySuccessMessage());
		Common.pause(2);
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		webApp2.enterNumberIntoTextBox("3513004405");
		webApp2.clickOnSaveButton();
		assertEquals("Number already present", webApp2.errorMessage());
		Common.pause(2);
	}

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Web_Add_1stAcc_Number_To_Callblocking_1stAcc_Outgoing_Rejected_2ndAcc_Incoming_CallNotSetup() throws IOException, InterruptedException {
		
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox(account1Number1);
		webApp2.clickOnSaveButton();
		assertEquals( webApp2.verifySuccessMessage(),"Number added To Blacklist successfully");
		Common.pause(2);
		
		phoneApp1.enterNumberinDialer(account2Number1);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		
		assertEquals(phoneApp1.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName1);
		assertEquals(phoneApp1.validateCallingScreenOutgoingNumber(),account2Number1);
		
		Thread.sleep(5000);
		
//		driverPhoneApp2.navigate().to(url.dialerSignIn());
		
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		Common.pause(10);
		
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);
		assertEquals(phoneApp1.validatecallType(),"Outgoing");

		phoneApp1.clickOnRightArrow();

		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);
		assertEquals(phoneApp1.validateCallStatusInCallDetail(), "Rejected");
		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(phoneApp2.validateNumberInAllcalls(),account1Number1);
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);
		assertEquals(phoneApp2.validatecallType(),"Incoming");

		phoneApp2.clickOnRightArrow();

		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Number Blocked");
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		webApp2Account.clickOnActivitFeed();
		Common.pause(10);
		assertEquals(webApp2Account.validateIncomingCallTypeIsDisplayed(),true,"validate calltype ");
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2, "validate department name ");
		assertEquals(webApp2Account.validateClientNumber(),account1Number1, "validate client number");
		assertEquals(webApp2Account.validateCallStatus(),"Number Blocked", "validate status ");
		
		webApp1Account.clickOnActivitFeed();
		Common.pause(10);
		assertEquals(webApp1Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp1Account.validateDepartmentName(),departmentName1);
		assertEquals(webApp1Account.validateClientNumber(),account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Rejected");
		
	}

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Web_Add_1stAcc_Number_To_Callblocking_2ndAcc_Outgoing_1stAcc_Incoming_Validate_Message() throws Exception{
		
		webApp2.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2.verifyEnterNumberTextBoxDisplay());
		webApp2.enterNumberIntoTextBox(account1Number1);
		webApp2.clickOnSaveButton();
		assertEquals( webApp2.verifySuccessMessage(),"Number added To Blacklist successfully");
		Common.pause(2);
		
		phoneApp2.enterNumberinDialer(account1Number1);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		
		//phoneApp1.doWebPushNotificationTesting();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		Thread.sleep(1000);
		
		//String text = phoneApp1.notificationMessage();
		
	}
			
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Web_Remove_1stAcc_Number_From_Callblocking_1stAcc_Outgoing_2ndAcc_Incoming() throws IOException, InterruptedException {
		
//		webApp1.clickOnPlustBtnToAddBlackListNumber();
//		assertTrue(webApp1.verifyEnterNumberTextBoxDisplay());
//		webApp1.enterNumberIntoTextBox(data.getValue("account2Number1"));
//		webApp1.clickOnSaveButton();
//		assertEquals( webApp1.verifySuccessMessage(),"Number added To Blacklist successfully");
//		Common.pause(2);
		
		webApp2.clickLeftMenuSetting();
		webApp2.scrollToCallBlockingTxt();
		Common.pause(1);
		
		webApp2Account.deleteAllNumbersFromBlackList();
		
		phoneApp1.enterNumberinDialer(account2Number1);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();
		
		assertEquals(phoneApp1.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName1);
		
		assertEquals(phoneApp1.validateCallingScreenOutgoingNumber(),account2Number1);
		
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(5000);
		
		assertEquals( phoneApp1.validateNumberInAllcalls(), account2Number1);

		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		assertEquals(phoneApp1.validatecallType(),"Outgoing");

		phoneApp1.clickOnRightArrow();

		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(phoneApp2.validateNumberInAllcalls(),account1Number1);

		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		assertEquals(phoneApp2.validatecallType(),"Incoming");

		phoneApp2.clickOnRightArrow();

		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);
		
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp1Account.validateDepartmentName(),departmentName1);
		assertEquals(webApp1Account.validateClientNumber(),account2Number1);
		assertEquals(webApp1Account.validateCallStatus(), "Completed");

		driverWebApp2.navigate().to(url.signIn());
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp2Account.validateDepartmentName(), departmentName2);
		assertEquals(webApp2Account.validateClientNumber(), account1Number1);
		assertEquals(webApp2Account.validateCallStatus(),"Completed");
		
	}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Web_Remove_1stAcc_Number_From_Callblocking_2ndAcc_Outgoing_1stAcc_Incoming() throws IOException, InterruptedException {
		
		webApp1.clickLeftMenuSetting();
		webApp1.scrollToCallBlockingTxt();
		Common.pause(1);
		
		webApp1Account.deleteAllNumbersFromBlackList();
		
		
		phoneApp2.enterNumberinDialer(account1Number1);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		Common.pause(1);
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(),true);
		assertEquals(webApp1Account.validateDepartmentName(),departmentName1);
		assertEquals(webApp1Account.validateClientNumber(),account2Number1);
		assertEquals(webApp1Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp2.navigate().to(url.signIn());
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(), true);
		assertEquals(webApp2Account.validateDepartmentName(), departmentName2);
		assertEquals(webApp2Account.validateClientNumber(), account1Number1);
		assertEquals(webApp2Account.validateCallStatus(),"Completed");
		
	}
		
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Verify_Blacklist_Number_Added_In_Dialer_Display_In_Web_Settings() throws IOException, InterruptedException {
		
//		webApp1.clickLeftMenuSetting();
//		webApp1.scrollToCallBlockingTxt();
//		Common.pause(1);
//		webApp1.deleteAllNumbersFromBlackList();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuSetting();
		webApp2.scrollToCallBlockingTxt();
		assertTrue(webApp2.verifyBlackListedNumberIsAvailable(account1Number1));
	}
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Verify_Unblock_Number_From_Dialer_Not_Display_In_Web_Settings() throws IOException, InterruptedException {
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		Thread.sleep(2000);
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(3);
		phoneApp2.clickUnBlockButton();
		Common.pause(2);
		assertEquals(phoneApp2.getUnBlockConfirmMessage(), "Are you sure you want to remove this number from blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully removed number from blacklist.");
		Common.pause(2);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuSetting();		
		webApp2.scrollToCallBlockingTxt();
		
		assertFalse(webApp2.verifyBlackListedNumberIsAvailable(account1Number1));
	}
		
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_Add_1stAcc_Number_To_BlackList_Call_From_CallDetailPage_1stAcc_Outgoing_Rejected_2ndAcc_Incoming_CallNotSetup() throws IOException, InterruptedException {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		Common.pause(1);
		
		webApp2Account.deleteAllNumbersFromBlackList();
		
		
		phoneApp2.enterNumberinDialer(account1Number1);
		Common.pause(2);
		phoneApp2.clickOnDialButton();
		
		phoneApp1.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		Common.pause(2);
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp1.enterNumberinDialer(account2Number1);

		phoneApp1.clickOnDialButton();
		
		assertEquals(phoneApp1.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName1);
		assertEquals(phoneApp1.validateCallingScreenOutgoingNumber(),account2Number1);
		
		Thread.sleep(5000);
		
		//driverPhoneApp2.navigate().to(url.dialerSignIn());
		
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);
		assertEquals(phoneApp1.validatecallType(),"Outgoing");

		phoneApp1.clickOnRightArrow();

		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);
		assertEquals(phoneApp1.validateCallStatusInCallDetail(), "Rejected");
		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		driverWebApp1.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp1Account.validateDepartmentName(),departmentName1);
		assertEquals(webApp1Account.validateClientNumber(),account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Rejected");
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(phoneApp2.validateNumberInAllcalls(),account1Number1);
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);
		assertEquals(phoneApp2.validatecallType(),"Incoming");

		phoneApp2.clickOnRightArrow();

		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Number Blocked");
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);
		
		driverWebApp2.navigate().to(url.signIn());
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateIncomingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(),"Number Blocked");
	}
	
	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Add_1stAcc_Number_To_BlackList_1stuser_Outgoing_From_CallDetailsPage() throws IOException, InterruptedException {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		Common.pause(1);
		
		webApp2Account.deleteAllNumbersFromBlackList();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		Common.pause(2);
		phoneApp2.clickOnDialButton();
		
		phoneApp1.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp1.clickOnRightArrow();
		phoneApp1.clickCallBtnOnCallDetailsPage();

		Thread.sleep(2000);
		
		//Verify notification - This number is restricted for Outgoing calls
	}
	
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Add_1stAcc_Number_To_BlackList_2ndAcc_Outgoing_From_AllCalls() throws IOException, InterruptedException {
//		webApp2.clickLeftMenuSetting();
//		webApp2.scrollToCallBlockingTxt();
		Common.pause(1);
		
		webApp2Account.deleteAllNumbersFromBlackList();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		Common.pause(2);
		phoneApp2.clickOnDialButton();
		
		phoneApp1.waitForIncomingCallingScreen();
		Common.pause(3);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		phoneApp2.clickOnLeftArrow();
		Common.pause(3);
		phoneApp2.clickOnAllCallsNumber();
		Common.pause(2);

		//Verify notification - This number is restricted for Outgoing calls
	}
	
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Add_1stAcc_Number_To_BlackList_2ndAcc_Outgoing_From_Dialpad() throws IOException, InterruptedException {
//		webApp1.clickLeftMenuSetting();
//		webApp1.scrollToCallBlockingTxt();
//		Common.pause(1);
		
		webApp2Account.deleteAllNumbersFromBlackList();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		Common.pause(2);
		
		phoneApp2.enterNumberinDialer(account2Number1);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account2Number1);
		
		Thread.sleep(1000);
	}
		
	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Add_2ndUser_Number_To_BlackList_1stuser_Outgoing_Using_Redial() throws IOException, InterruptedException {
		
//		webApp1.clickLeftMenuSetting();
//		webApp1.scrollToCallBlockingTxt();
//		Common.pause(1);
//		webApp1.deleteAllNumbersFromBlackList();
//		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		Common.pause(2);
		
		phoneApp2.clickOndialDisableButton();
		Common.pause(2);
		phoneApp2.clickOnDialButton();
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		Thread.sleep(1000);
	}
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Unblock_2ndUser_Number_1stuser_Outgoing_2ndUser_Incoming_From_CalldetailsPage() throws IOException, InterruptedException {
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(3);

		phoneApp2.clickUnBlockButton();
		assertEquals(phoneApp2.getUnBlockConfirmMessage(), "Are you sure you want to remove this number from blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully removed number from blacklist.");
		Common.pause(2);
		
		phoneApp2.clickCallBtnOnCallDetailsPage();
		Common.pause(2);
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		Thread.sleep(10000);
		
		phoneApp2.clickOnLeftArrow();
		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp1.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp1Account.validateDepartmentName(), departmentName1);
		assertEquals(webApp1Account.validateClientNumber(), account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Completed");
	}
	
	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Unblock_2ndUser_Number_1stuser_Outgoing_2ndUser_Incoming_From_AllCalls() throws IOException, InterruptedException {
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(3);

		phoneApp2.clickUnBlockButton();
		assertEquals(phoneApp2.getUnBlockConfirmMessage(), "Are you sure you want to remove this number from blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully removed number from blacklist.");
		Common.pause(2);
		
		phoneApp2.clickOnLeftArrow();
		Common.pause(1);
		phoneApp2.clickOnAllCallsNumber();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		
		Thread.sleep(10000);
		
		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp1.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp1Account.validateDepartmentName(), departmentName1);
		assertEquals(webApp1Account.validateClientNumber(), account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Completed");
	}
	
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Unblock_2ndUser_Number_1stuser_Outgoing_2ndUser_Incoming_From_CallPlanner() throws IOException, InterruptedException {
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		
		phoneApp2.clickOnSideMenu();
		Common.pause(1);
		phoneApp2.clickOnCallPlannerLeftMenu();
		phoneApp2.deleteAllRemindersFromCallPlanner();
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(3);

		phoneApp2.clickUnBlockButton();
		assertEquals(phoneApp2.getUnBlockConfirmMessage(), "Are you sure you want to remove this number from blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully removed number from blacklist.");
		Common.pause(2);
		
		phoneApp2.clickOnCallDetailsReminderBtn();
		phoneApp2.selectValueForReminder();
		assertEquals(phoneApp2.validationMessage(), "Successfully set call reminder");
		phoneApp2.clickOnLeftArrow();
		Common.pause(1);
		
		phoneApp2.clickOnSideMenu();
		Common.pause(1);
		phoneApp2.clickOnCallPlannerLeftMenu();
		Common.pause(2);
		phoneApp2.clickOnCallBtnOnCallReminderPage();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		//phoneApp1.waitForDialerPage();
		assertEquals(phoneApp2.noCallsInCallReminder(),"You have no scheduled calls");
		
		Thread.sleep(10000);
		
		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp2.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp1Account.validateDepartmentName(), departmentName1);
		assertEquals(webApp1Account.validateClientNumber(), account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Completed");
		
	}
	
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Unblock_2ndUser_Number_1stuser_Outgoing_2ndUser_Incoming_From_DialPad() throws IOException, InterruptedException {
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(3);

		phoneApp2.clickUnBlockButton();
		assertEquals(phoneApp2.getUnBlockConfirmMessage(), "Are you sure you want to remove this number from blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully removed number from blacklist.");
		Common.pause(2);
		
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		Common.pause(2);
		
		
		phoneApp2.enterNumberinDialer(account1Number1);

		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp1.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp1Account.validateDepartmentName(), departmentName1);
		assertEquals(webApp1Account.validateClientNumber(), account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Completed");
	}
	
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Unblock_2ndUser_Number_1stuser_Outgoing_2ndUser_Incoming_Using_Redial() throws IOException, InterruptedException {
		
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Thread.sleep(1000);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickOnBlackListButton();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(2);
		
		driverPhoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2.clickOndialDisableButton();
		Common.pause(1);
		phoneApp2.clickOnDialButton();
		Common.pause(3);;
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);
		phoneApp2.clickOnRightArrow();
		phoneApp2.clickUnBlockButton();
		assertEquals(phoneApp2.getUnBlockConfirmMessage(), "Are you sure you want to remove this number from blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully removed number from blacklist.");
		
		driverPhoneApp2.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOndialDisableButton();
		Common.pause(1);
		phoneApp2.clickOnDialButton();
		Common.pause(3);
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp1.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp1Account.validateDepartmentName(), departmentName1);
		assertEquals(webApp1Account.validateClientNumber(), account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Completed");
	}


	/*@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_Unblock_2ndUser_Number_1stuser_Incoming_2ndUser_Outgoing() throws IOException, InterruptedException {
		
		try {
		driverWebApp3 = TestBase.init2();
		driverWebApp3.get(url.signIn());
		Common.pause(4);
		webApp2SubuserAccount = PageFactory.initElements(driverWebApp3, WebToggleConfiguration.class);
		webApp2 = PageFactory.initElements(driverWebApp3, CallBlockingPage.class);
		try {
			loginWeb(webApp2SubuserAccount, account2EmailSubUser, account1Password);
			Common.pause(3);
			
		} catch (Exception e) {
			loginWeb(webApp2SubuserAccount, account2EmailSubUser, account1Password);
			Common.pause(3);
		}
		
		driverPhoneApp3 = TestBase.init2();
		driverPhoneApp3.get(url.dialerSignIn());
		phoneApp3 = PageFactory.initElements(driverPhoneApp3, DialerIndex.class);
		phoneAppMainuser = PageFactory.initElements(driverPhoneApp3, DialerLoginPage.class);
		try {
			loginDialer(phoneApp3, account2EmailSubUser, account1Password);
			phoneApp2.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp3, account2EmailSubUser, account1Password);
			phoneApp3.waitForDialerPage();
		}
		
		webApp2Account.userspage();
		Common.pause(3);
		webApp2Account.navigateToUserSettingPage(account1Email);
		String MainUserNumberExtension = "+"+webApp2Account.getUserNumberExtension();
		webApp1Account.userspage();
		webApp1Account.navigateToUserSettingPage(account1SubEmail);
		String SubUserNumberExtension = "+"+webApp2Account.getUserNumberExtension();
		
		String departmentName1 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp3.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(SubUserNumberExtension);

		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing");
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateSubUserOutgoingNumberCallingScreen(),SubUserNumberExtension);
		
		Common.pause(1);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp1.validateNumberInAllcalls(), SubUserNumberExtension);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Outgoing");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),SubUserNumberExtension);

		// validate call status in Call detail page
		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		
		phoneApp3.clickOnSideMenu();
		String callerName2 = phoneApp3.getCallerName();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp3.validateNumberInAllcalls(),MainUserNumberExtension);

		// validate via number in All Calls page
		assertEquals(phoneApp3.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp3.validatecallType(),"Incoming");

		phoneApp3.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp3.validateNumberInCallDetail(),MainUserNumberExtension);

		assertEquals(phoneApp3.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp3.validateCallStatusInCallDetail();

		// Validate details in webApp1 main user
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp1Account.validateDepartmentName(),"-");
		assertEquals(webApp1Account.validateClientNumber(),callerName2);
		assertEquals(webApp1Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp3.navigate().to(url.signIn());
		webApp2SubuserAccount.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2SubuserAccount.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp2SubuserAccount.validateDepartmentName(),"-");
		assertEquals(webApp2SubuserAccount.validateClientNumber(), callerName1);
		assertEquals(webApp2SubuserAccount.validateCallStatus(),"Completed");
		
		driverWebApp3.quit();
		driverPhoneApp3.quit();
		} catch (Exception e) {
			driverWebApp3.quit();
			driverPhoneApp3.quit();
			assertTrue(false);
			
			
		}
	}
	*/
	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Dialer_CallDetailPage_BlackList_Icon_Not_Visible_For_Internal_Extension_Calling() throws IOException, InterruptedException {
	
		driverWebApp3 = TestBase.init2();
		driverWebApp3.get(url.signIn());
		Common.pause(4);
		webApp2SubuserAccount = PageFactory.initElements(driverWebApp3, WebToggleConfiguration.class);
		//webApp2 = PageFactory.initElements(driverWebApp3, CallBlockingPage.class);
		try {
			loginWeb(webApp2SubuserAccount, account2EmailSubUser, account1Password);
			Common.pause(3);
			
		} catch (Exception e) {
			loginWeb(webApp2SubuserAccount, account2EmailSubUser, account1Password);
			Common.pause(3);
		}
		
		driverPhoneApp3 = TestBase.init2();
		driverPhoneApp3.get(url.dialerSignIn());
		phoneApp3 = PageFactory.initElements(driverPhoneApp3, DialerIndex.class);
		phoneAppMainuser = PageFactory.initElements(driverPhoneApp3, DialerLoginPage.class);
		try {
			loginDialer(phoneApp3, account2EmailSubUser, account1Password);
			phoneApp2.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp3, account2EmailSubUser, account1Password);
			phoneApp3.waitForDialerPage();
		}
		
		webApp2Account.userspage();
		Common.pause(3);
		webApp2Account.navigateToUserSettingPage(account2Email);
		String MainUserNumberExtension = "+"+webApp2Account.getUserNumberExtension();
		webApp2Account.userspage();
		webApp2Account.navigateToUserSettingPage(account2EmailSubUser);
		String SubUserNumberExtension = "+"+webApp2Account.getUserNumberExtension();
		
		String departmentName1 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp3.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(SubUserNumberExtension);

		phoneApp2.clickOnDialButton();
		
		Common.pause(1);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);

		Thread.sleep(8000);

		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();

		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);

		phoneApp2.clickOnRightArrow();
		
		assertEquals(phoneApp2.callDetailBlackListIconNotVisible(),true);
		
		driverWebApp3.quit();
		driverPhoneApp3.quit();
	}
	
	
	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void add_number_to_contacts_blackList() throws IOException, InterruptedException {
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		phoneApp2.enterNumberinDialer(account1Number1);

		
		phoneApp2.clickOnDialButton();
		
		// Validate outgoingVia on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingVia(),"Outgoing Via " + departmentName2);
		
		// validate outgoing Number on outgoing calling screen
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(),account1Number1);
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp2.validateDurationInCallingScreen(2);
		
		Thread.sleep(8000);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp2.clickOnSideMenu();
		
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(5000);
		
		// validate Number in All Calls page
		assertEquals( phoneApp2.validateNumberInAllcalls(), account1Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp2.validateViaNumberInAllCalls(),departmentName2);

		// validate call type in all calls page
		assertEquals(phoneApp2.validatecallType(),"Outgoing");

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp2.validateNumberInCallDetail(),account1Number1);

		// validate call status in Call detail page
		assertEquals(phoneApp2.validateCallStatusInCallDetail(),"Completed");
		
		assertEquals(phoneApp2.validateDepartmentNameinCallDetail(),"Via " + departmentName2);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
		assertEquals(phoneApp1.validateNumberInAllcalls(),account2Number1);

		// validate via number in All Calls page
		assertEquals(phoneApp1.validateViaNumberInAllCalls(),departmentName1);

		// validate call type in all calls page
		assertEquals(phoneApp1.validatecallType(),"Incoming");

		phoneApp1.clickOnRightArrow();

		// validate Number on call details page
		assertEquals(phoneApp1.validateNumberInCallDetail(),account2Number1);

		assertEquals(phoneApp1.validateCallStatusInCallDetail(),"Completed");
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals(phoneApp1.validateDepartmentNameinCallDetail(),"Via " + departmentName1);
		
		// Validate details in webApp1 main user
		webApp2Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2Account.validateOutgoingCallTypeIsDisplayed(),true);
		assertEquals(webApp2Account.validateDepartmentName(),departmentName2);
		assertEquals(webApp2Account.validateClientNumber(),account1Number1);
		assertEquals(webApp2Account.validateCallStatus(), "Completed");

		// Validate details in webApp2 main user
		driverWebApp1.navigate().to(url.signIn());
		webApp1Account.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp1Account.validateIncomingCallTypeIsDisplayed(), true);
		assertEquals(webApp1Account.validateDepartmentName(), departmentName1);
		assertEquals(webApp1Account.validateClientNumber(), account2Number1);
		assertEquals(webApp1Account.validateCallStatus(),"Completed");
		
		phoneApp2.clickOnCallDetailPageAddButton();
		
		phoneApp2.enterUserNameInContacts("user2");
		
//		phoneApp2.clickOnSaveBtnToAddContactName();
//		assertEquals(phoneApp2.validationMessage(), "Number has been added Successfully");
		
		phoneApp2.clickOnTickGreenBtnToAddContactName();
		//assertEquals(phoneApp2.validationMessage(), "Contact has been added Successfully");
		
		Common.pause(3);
		phoneApp2.clickblockbtnOnCondetailspage();
		assertEquals(phoneApp2.getBlackListConfirmMessage(), "Are you sure you want to add this number to blacklist ?");
		phoneApp2.clickYesToConfirmBlackListNumber();
		assertEquals(phoneApp2.validationMessage(), "Successfully number blacklisted.");
		Common.pause(3);
		
		phoneApp2.clickOnconDetailCallBtn1();
	}
	
}
