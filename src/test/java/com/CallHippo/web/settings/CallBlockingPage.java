package com.CallHippo.web.settings;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class CallBlockingPage {

	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;

	public CallBlockingPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		js = (JavascriptExecutor) driver;
	}
	
	@FindBy(xpath = "//a[contains(.,'Dashboard')]")
	private WebElement left_panel_dashboard;
	
	@FindBy(xpath = "//h5[@class='titleboxdashicne'][contains(.,'Call Summary')]")
	private WebElement dashboard_callSummary_txt;

	@FindBy(xpath = "//a[contains(.,'Setting')]")
	private WebElement leftMenu_setting;

	@FindBy(xpath = "//span[contains(.,'Your Plan')]")
	WebElement yourPlan_Txt;

	@FindBy(xpath = "//h3[contains(.,'Call Blocking')]")
	WebElement callBlocking_Txt;

	@FindBy(xpath = "//section[@id='callBlocking']//i[@class[contains(.,'material-icons')]][contains(.,'not_interested')]")
	WebElement restricted_incoming_number_icon;

	@FindBy(xpath = "//section[@id='callBlocking']//div[@class[contains(.,'deparmentname_title')]]/span[contains(.,'Restrict calls from specific callers')]")
	WebElement restricted_incoming_number_Txt;

	@FindBy(xpath = "//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmailText')]]/p[contains(.,'Blacklist')]")
	WebElement add_number_to_blacklist_Txt;

	@FindBy(xpath = "//section[@id='callBlocking']//button[@class[contains(.,'billingAddEmailBtn')]]//i[contains(.,'add_circle')]")
	WebElement add_number_blackListBtn_icon;

	@FindBy(xpath = "//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmailInput')]]/input[contains(@placeholder,'Enter Number')]")
	WebElement enter_number_input_textBox;

	@FindBy(xpath = "//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmail')]]//button[contains(@class,'billingSaveEmailBtn')][contains(disabled,'')]")
	WebElement blackList_number_save_disabled;

	@FindBy(xpath = "//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmail')]]//button[contains(@class,'billingSaveEmailBtn')]")
	WebElement blackList_number_save_enabled;

	@FindBy(xpath = "//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmail')]]//button[contains(@class,'billingTrashEmailBtn')]")
	WebElement blackList_number_deleteBtn;

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	WebElement successMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement errorMessage;
	
	@FindBy(xpath = "//span[contains(.,'SDAP')]")
	WebElement sdap_Txt;

	
	public void clickLeftMenuDashboard() {
		wait.until(ExpectedConditions.elementToBeClickable(left_panel_dashboard));
		left_panel_dashboard.click();
		wait.until(ExpectedConditions.elementToBeClickable(dashboard_callSummary_txt));
		Common.pause(1);
	}
	
	public void clickLeftMenuSetting() {
		wait.until(ExpectedConditions.visibilityOf(leftMenu_setting));
		leftMenu_setting.click();
		wait.until(ExpectedConditions.elementToBeClickable(sdap_Txt));
		Common.pause(1);
	}

	public void scrollToCallBlockingTxt() {
		wait.until(ExpectedConditions.visibilityOf(callBlocking_Txt));
		js.executeScript("arguments[0].scrollIntoView();", callBlocking_Txt);
	}

	public boolean verifyFieldsInCallBlocking() {
		boolean allFieldsAvailable;
		if (restricted_incoming_number_icon.isDisplayed()
				&& restricted_incoming_number_Txt.getText().equalsIgnoreCase("Restrict Calls From Specific Callers")
				&& add_number_to_blacklist_Txt.getText().equalsIgnoreCase("Add Number To Blacklist")
				&& add_number_blackListBtn_icon.isDisplayed()) {
			allFieldsAvailable = true;
		} else {
			allFieldsAvailable = false;
		}
		return allFieldsAvailable;
	}

	public void clickOnPlustBtnToAddBlackListNumber() {
		wait.until(ExpectedConditions.visibilityOf(add_number_blackListBtn_icon));
		js.executeScript("arguments[0].click();", add_number_blackListBtn_icon);
	}

	public boolean verifyEnterNumberTextBoxDisplay() {
		return wait.until(ExpectedConditions.visibilityOf(enter_number_input_textBox)).isDisplayed();
	}

	public void enterNumberIntoTextBox(String number) {
		enter_number_input_textBox.sendKeys(number);
	}

	public boolean verifySaveBtnIsDisabled() {
		Common.pause(1);
		return wait.until(ExpectedConditions.visibilityOf(blackList_number_save_disabled)).isEnabled();
	}

	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.elementToBeClickable(blackList_number_save_enabled)).click();
	}

	public String verifySuccessMessage() {
		wait.until(ExpectedConditions.visibilityOf(successMessage));
		Common.pause(2);
		return successMessage.getText();
	}
	
	public String errorMessage() {
		wait.until(ExpectedConditions.visibilityOf(errorMessage));
		Common.pause(2);
		return errorMessage.getText();
	}

	public void deleteBlackListNumber(String number) {
		driver.findElement(
				By.xpath("//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmailText')]]/b[text()='+"
						+ number + "']/..//following-sibling::div/button[contains(@class,'billingTrashEmailBtn')]"))
				.click();
	}

	public boolean verifySpecialCharacterNotAbleToEnter() {
		Common.pause(1);
		return wait.until(ExpectedConditions.visibilityOf(enter_number_input_textBox)).getText().equalsIgnoreCase("");
	}

	public void deleteAllNumbersFromBlackList() {
		Common.pause(2);
		List<WebElement> elements = driver.findElements(
				By.xpath("//section[@id='callBlocking']//button[@class[contains(.,'billingTrashEmailBtn')]]"));
		System.out.println("---elements.size()--" + elements.size());

		if (elements.size() > 0) {
			// for (WebElement element : elements) {
			for (int i = 0; i <= elements.size(); i++) {
				System.out.println("==== i ===" + i);
				List<WebElement> elements1 = driver.findElements(
						By.xpath("//section[@id='callBlocking']//button[@class[contains(.,'billingTrashEmailBtn')]]"));
				if (elements1.size() > 0) {
					elements1.get(0).click();
				}
				Common.pause(2);

			}
			Common.pause(1);
		}
	}
	
	public boolean verifyBlackListedNumberIsAvailable(String number) {
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//section[@id='callBlocking']//div[@class[contains(.,'billingAddEmailText')]]/b[text()='"+number+"']"));
		System.out.println("---BlackListEle.size()--"+ele.size());
		if (ele.size() != 0) {
			elementIsAvailable = true;
		}else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}
	

}
