package com.CallHippo.web.settings;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class DefaultCountryCodeTest {

	WebDriver driverWebApp2;
	
	WebDriver driverPhoneApp2;
	
	WebToggleConfiguration webApp2Account;
	
	DefaultCountryCodePage webApp2;
	
	DialerIndex phoneApp2;
	
	DialerLoginPage phoneAppMainuser;
	

	RestAPI creditAPI;
	PropertiesFile url;
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");
	
	String account1Email = data.getValue("account1MainEmail");
	String account1SubEmail = data.getValue("account1SubEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account1Number1 = data.getValue("account1Number1");
	String account2Number1 = data.getValue("account2Number1");
	
	Common excelTestResult;

	public DefaultCountryCodeTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		creditAPI = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}
	

	
	@BeforeTest
	public void createSessions() throws IOException, InterruptedException {
		driverWebApp2 = TestBase.init2();
		driverWebApp2.get(url.signIn());
		Common.pause(4);
		webApp2Account = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		webApp2 = PageFactory.initElements(driverWebApp2, DefaultCountryCodePage.class);

		driverPhoneApp2 = TestBase.init_dialer();
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);
		
		
		try {
			loginWeb(webApp2Account, account2Email, account1Password);
			Common.pause(3);
			
		} catch (Exception e) {
			loginWeb(webApp2Account, account2Email, account1Password);
			Common.pause(3);
		}

		try {
			loginDialer(phoneApp2, account2Email, account1Password);
			phoneApp2.waitForDialerPage();
		} catch (Exception e) {
			loginDialer(phoneApp2, account2Email, account1Password);
			phoneApp2.waitForDialerPage();
		}
		
		try {
			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			driverPhoneApp2.get(url.dialerSignIn());

			
			System.out.println("loggedin webApp");
			Thread.sleep(9000);
			webApp2Account.numberspage();

			webApp2Account.navigateToNumberSettingpage(account2Number1);

			Thread.sleep(9000);
			
			webApp2Account.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
			Thread.sleep(2000);
			webApp2Account.selectMessageType("Welcome message","Text");
			webApp2Account.selectMessageType("Voicemail","Text");
			webApp2Account.clickonAWHMVoicemail();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.clickonAWHMGreeting();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.selectMessageType("Wait Music","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
			webApp2Account.selectMessageType("IVR","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
			Thread.sleep(3000);
			
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2Account.userTeamAllocation("users");
			webApp2Account.Stickyagent("off","Strictly");
			Common.pause(3);
			webApp2Account.customRingingTime("off"); 
			System.out.println("customRingingTime OFF");
			Common.pause(3);
			webApp2Account.selectSpecificUserOrTeam(callerName2, "Yes");
			Common.pause(3);
			
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			
			webApp2Account.setUserSttingToggles("off", "off", "on", "open", "off");
			webApp2Account.selectMessageType("Voicemail","Text");
			Common.pause(2);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(2);
			
			webApp2Account.makeDefaultNumber(account2Number1);

			String Url = driverWebApp2.getCurrentUrl();
			creditAPI.UpdateCredit("500", "0", "0", webApp2Account.getUserId(Url));
			creditAPI.UpdateCreditCountyWisezero(webApp2Account.getUserId(Url));

			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2EmailSubUser);
			Thread.sleep(9000);
			webApp2Account.makeDefaultNumber(account2Number1);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");

			driverPhoneApp2.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
			driverPhoneApp2.get(url.dialerSignIn());

			phoneApp2.afterCallWorkToggle("off");
			Common.pause(3);
			driverPhoneApp2.get(url.dialerSignIn());
			Common.pause(2);
			driverWebApp2.get(url.signIn());
			webApp2Account.clickLeftMenuSetting();
			Common.pause(2);
			webApp2Account.scrollToPriceLimitTxt();
			webApp2Account.removePriceLimit();
			webApp2Account.ScrollToDefaultCountryCodeSection();
			webApp2Account.deleteCountryCode();

		} catch (Exception e) {
			
			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			driverPhoneApp2.get(url.dialerSignIn());

			
			System.out.println("loggedin webApp");
			Thread.sleep(9000);
			webApp2Account.numberspage();

			webApp2Account.navigateToNumberSettingpage(account2Number1);

			Thread.sleep(9000);
			
			webApp2Account.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
			Thread.sleep(2000);
			webApp2Account.selectMessageType("Welcome message","Text");
			webApp2Account.selectMessageType("Voicemail","Text");
			webApp2Account.clickonAWHMVoicemail();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.clickonAWHMGreeting();
			webApp2Account.selectMessageType("After Work Hours Message","Text");
			webApp2Account.selectMessageType("Wait Music","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
			webApp2Account.selectMessageType("IVR","Text");
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
			Thread.sleep(3000);
			
			webApp2Account.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2Account.userTeamAllocation("users");
			webApp2Account.Stickyagent("off","Strictly");
			Common.pause(3);
			webApp2Account.customRingingTime("off"); 
			System.out.println("customRingingTime OFF");
			Common.pause(3);
			webApp2Account.selectSpecificUserOrTeam(callerName2, "Yes");
			Common.pause(3);
			
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			
			webApp2Account.setUserSttingToggles("off", "off", "on", "open", "off");
			webApp2Account.selectMessageType("Voicemail","Text");
			Common.pause(2);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(2);
			
			webApp2Account.makeDefaultNumber(account2Number1);

			String Url = driverWebApp2.getCurrentUrl();
			creditAPI.UpdateCredit("500", "0", "0", webApp2Account.getUserId(Url));
			creditAPI.UpdateCreditCountyWisezero(webApp2Account.getUserId(Url));

			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");
			webApp2Account.userspage();
			webApp2Account.navigateToUserSettingPage(account2EmailSubUser);
			Thread.sleep(9000);
			webApp2Account.makeDefaultNumber(account2Number1);
			webApp2Account.setUserSttingToggles("off", "off", "off", "open", "off");

			driverPhoneApp2.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
			driverPhoneApp2.get(url.dialerSignIn());

			phoneApp2.afterCallWorkToggle("off");
			Common.pause(3);
			driverPhoneApp2.get(url.dialerSignIn());
			Common.pause(2);
			driverWebApp2.get(url.signIn());
			webApp2Account.clickLeftMenuSetting();
			Common.pause(2);
			webApp2Account.scrollToPriceLimitTxt();
			webApp2Account.removePriceLimit();
			webApp2Account.ScrollToDefaultCountryCodeSection();
			webApp2Account.deleteCountryCode();
		}
		
	}
	
	@AfterTest(alwaysRun = true)
	public void endSessions() {
		
		driverWebApp2.quit();
		driverPhoneApp2.quit();
	}
	
	@BeforeMethod
	public void initialization() throws Exception {
		try {
			 driverWebApp2.navigate().to(url.signIn());
			 driverPhoneApp2.navigate().to(url.dialerSignIn());
			 Common.pause(3);
			
			if (webApp2Account.validateWebAppLoggedinPage() == true ) {
				loginWeb(webApp2Account, account2Email, account1Password);
				 Thread.sleep(9000);
			}
				
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				 loginDialer(phoneApp2, account2Email, account1Password);
			}
			
			webApp2.clickLeftMenuSetting();
			webApp2.clickOnDefaultCountryCodeSideMenu();
			webApp2.ScrollToDefaultCountryCodeSection();
			webApp2.deleteCountryCode();
			
			phoneApp2.deleteAllContactsFromDialer();
			Common.pause(2);
			driverPhoneApp2.navigate().to(url.dialerSignIn());
			Common.pause(3);
		
		} catch (Exception e) {
			Common.Screenshot(driverWebApp2," WebApp2 - Fail - ","Before method - initialization");
			Common.Screenshot(driverPhoneApp2, " PhoneApp2 - Fail - ","CallBlockingTest - Before method - initialization");
			System.out.println("----Need to check issue - CallBlockingTest - Before method - initialization----");
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		if (ITestResult.FAILURE 
				== result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp2, testname, "webApp2 - Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "phoneApp2 - Fail - " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp2, testname, "webApp2 - Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "phoneApp2 - Pass - " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}
	
	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Default_Country_Code_section_is_Displayed_In_Setting_page() throws IOException {

		assertTrue(webApp2.validateDefaultCountryCodeSectionIsDisplayed());
		assertEquals(webApp2.validatedDescriptionOfDefaultCountryCode(), "Add a country code before the number for your call to connect.Enter the country code, which is frequently used to make calls.\n"
				+ "To know more,Click here");
	}
		
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_OFF_enter_number_without_countryCode() throws IOException {
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_OFF_enter_number_without_countryCode_and_with_plus_sign() throws IOException {

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("+2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_OFF_enter_number_without_countryCode_and_again_call_from_call_logs() throws IOException {

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("+2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Common.pause(4);
		assertEquals("+12560000000", phoneApp2.validateNumberInAllcalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());
		
		phoneApp2.clickOnAllCallsNumber();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(4);
		
		phoneApp2.clickOnRightArrow();

		assertEquals("+12560000000", phoneApp2.validateNumberInCallDetail());
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_ON_enter_number_without_countryCode() throws IOException {

		webApp2.selectCountryName("India");
		Common.pause(5);
		phoneApp2.hardRefreshByKeyboard();
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_ON_enter_number_without_countryCode_and_with_plus_sign() throws IOException {
		webApp2.selectCountryName("India");
		Common.pause(5);
		phoneApp2.hardRefreshByKeyboard();
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("+2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_ON_enter_number_without_countryCode_and_again_call_from_call_logs() throws IOException {

		webApp2.selectCountryName("India");
		Common.pause(5);
		phoneApp2.hardRefreshByKeyboard();
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("+2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Common.pause(4);
		assertEquals("+912560000000", phoneApp2.validateNumberInAllcalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());
		
		phoneApp2.clickOnAllCallsNumber();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(4);
		
		phoneApp2.clickOnRightArrow();

		assertEquals("+912560000000", phoneApp2.validateNumberInCallDetail());
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
	}
	
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_OFF_enter_number_without_countryCode_Number_is_saved() throws IOException {

		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.AddContact("2AccountMainuser");
		
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_ON_enter_number_without_countryCode_Number_is_saved() throws IOException {

		webApp2.selectCountryName("India");

		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.AddContact("2AccountMainuser");
		
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_ON_enter_number_without_countryCode_and_again_call_from_call_logs_with_with_number_is_saved() throws IOException {

		webApp2.selectCountryName("India");
		Common.pause(5);
		phoneApp2.hardRefreshByKeyboard();
		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.AddContact("2AccountMainuser");
		Common.pause(5);
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("+2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Common.pause(4);
		
		assertEquals("+912560000000", phoneApp2.validateNumberInAllcalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());
		
		phoneApp2.clickOnAllCallsNumber();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+912560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(4);
		
		phoneApp2.clickOnRightArrow();

		assertEquals("+912560000000", phoneApp2.validateNumberInCallDetail());
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
	}
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void country_code_toggle_OFF_enter_number_without_countryCode_and_again_call_from_call_logs_with_with_number_is_saved() throws IOException {

		phoneApp2.enterNumberinDialer("2560000000");
		phoneApp2.AddContact("2AccountMainuser");
		Common.pause(5);
		driverPhoneApp2.navigate().to(url.dialerSignIn());
		
		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), account1Number1);
		
		phoneApp2.waitForDialerPage();
		
		phoneApp2.enterNumberinDialer("+2560000000");
		phoneApp2.clickOnDialButton();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Common.pause(4);
		
		assertEquals("2AccountMainuser", phoneApp2.validateNumberInAllcalls());

		assertEquals("Outgoing", phoneApp2.validatecallType());
		
		phoneApp2.clickOnAllCallsNumber();
		
		assertEquals(phoneApp2.validateCallingScreenOutgoingNumber(), "+12560000000" );
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(4);
		
		phoneApp2.clickOnRightArrow();

		assertEquals("+12560000000", phoneApp2.validateNumberInCallDetail());
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
		
	}
}
