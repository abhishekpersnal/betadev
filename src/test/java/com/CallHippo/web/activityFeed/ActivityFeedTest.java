package com.CallHippo.web.activityFeed;

import static org.testng.Assert.assertEquals;
import java.util.List;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;
import com.CallHippo.web.numberAndUserSubscription.AddNumberPage;
import com.CallHippo.web.numberAndUserSubscription.InviteUserPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class ActivityFeedTest {
	

	WebToggleConfiguration webApp2;
	
	WebDriver driverwebApp2;
	SignupPage dashboard;
	SignupPage dashboardMail;
	static Common csv;
	WebDriver driverMail;

	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2EmailSubUser2 = data.getValue("account2Sub2Email");
	
	String acc2Number1 = data.getValue("account2Number1"); // account 2's number
	
	String acc2Number2 = data.getValue("account2Number2");
	
	String acc1Number1 = data.getValue("account1Number1"); // account 1's number
	int record=5;
	ArrayList<String> csvRecords = new ArrayList<String>();
	ArrayList<String> webRecords= new ArrayList<String>();
	
	String mainUserName,subUserName,subUser2Name,departmentNameForacc1Number2,departmentNameForacc2Number2;
	
	public ActivityFeedTest() throws Exception {
		softAssert = new SoftAssert();
		csv= new Common();
	}
	

	@BeforeTest
	public void initialization() throws Exception {
		try {
			driverwebApp2 = TestBase.init();
			System.out.println("Opened webaap session");
			webApp2 = PageFactory.initElements(driverwebApp2, WebToggleConfiguration.class);
			dashboard = PageFactory.initElements(driverwebApp2, SignupPage.class);
			try {
				driverwebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverwebApp2.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}	
				
		} catch (Exception e) {
			String testname = "Add contact Before Method ";
			 Common.Screenshot(driverwebApp2, testname, "driverwebApp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");	
			}
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		 mainUserName = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println("main user name:" + mainUserName);

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		 subUserName = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println("sub user name:"+subUserName);

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser2);
		Common.pause(9);
		 subUser2Name = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println("sub user2 name:"+subUser2Name);

		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(acc2Number1);
		Common.pause(9);
		 departmentNameForacc1Number2 = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println(departmentNameForacc1Number2);

		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Common.pause(9);
		 departmentNameForacc2Number2 = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println(departmentNameForacc2Number2);
	}

	@BeforeMethod

	public void deleteAllMails() throws IOException {
//		try {
//			driverMail = TestBase.init3();
//			dashboardMail = PageFactory.initElements(driverMail, SignupPage.class);
//			try {
//				dashboardMail.deleteAllMail();
//			} catch (Exception e) {
//				dashboardMail.deleteAllMail();
//			}
//		} catch (Exception e) {
//			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
//			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
//		}
//		driverMail.quit();
//		dashboard = PageFactory.initElements(driverwebApp2, SignupPage.class);
	try {
			driverwebApp2.get(url.signIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
		} catch (Exception e) {
			// TODO: handle exception
			driverwebApp2.get(url.signIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
		}		
	}
	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	private void validateRecord(String csvColValue,String titleValue) throws Exception {
		
		csvRecords = csv.readDataFromActivityFeedCsv(csv.verifyDownloadFileName(), csvColValue);
		
		System.out.println("CSV records after first element::" + csvRecords);
		webRecords = webApp2.getActivityFeedWebRecords(record,titleValue);
		System.out.println("Web records" + webRecords);
		System.out.println("Way 1.......... ");
		webApp2.validateWebActivityFeedInCsv(csvRecords,webRecords);
	
	}
	
	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverwebApp2, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
		   Common.Screenshot(driverwebApp2, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());	
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

	//	driverwebApp2.quit();

	}
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_file_Download() throws Exception {
		
		csv.deleteAllFile("activityFeedReport"); 
	
		webApp2.clickOnActivitFeed();
		Common.pause(3);
	
		webApp2.clickOnCheckBoxOption(record);
		Common.pause(3);
		assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
		webApp2.clickOnFileDownload();
		
		Common.pause(8);
		
		dashboard.ValidateActivityFeedReportOnGmail();
		Common.pause(3);
		assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
		webApp2.clickOnActivitFeed();
		Common.pause(5);
	
	webApp2.validateCallRecording_NoteInCsv(record,"awsRecordingUrl");
		webApp2.validateNoteInCsv(record,"callNotes");
		validateRecord("callType","Calltype");		
		validateRecord("countryName","Department");
		validateRecord("caller","User");
		//validateRecord("from","Client");
		validateRecord("callStatus","Status");
		validateRecord("time","Time");
		validateRecord("callDuration","Duration");
	    validateRecord("callCost","Cost");
		
	}
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Activity_Feed_Filters_UI() throws Exception {
	
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		
		assertEquals( webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);		
		
		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);
		
		assertEquals( webApp2.validatActivityFeedFilterPopupIsDispayed(), true);	

		assertEquals( webApp2.validateSelectCallerDropdownInAFFPopup(), true);	
		softAssert.assertEquals(webApp2.validateSelectCallerDropdownInAFFPopup(), true);
		
		webApp2.clickOnSelectCallerDropdownInAFFPopup();
		softAssert.assertEquals(webApp2.validateCallerDropdownEveryOneInAFFPopup(), true);
		softAssert.assertEquals(webApp2.validateCallerDropdownUsersInAFFPopup(), true);
		softAssert.assertEquals(webApp2.validateCallerDropdownTeamsInAFFPopup(), true);
		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
		softAssert.assertEquals( webApp2.validateSelectUsersDropdownInAFFPopup(), false);	
		softAssert.assertEquals( webApp2.validateCallStatusDropdownInAFFPopup(), true);
		
		webApp2.clickOnSelectCallerInAFFPopup();
		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		softAssert.assertEquals( webApp2.validateSelectUsersDropdownInAFFPopup(), true);
		
		webApp2.clickOnSelectCallerInAFFPopup();
		webApp2.clickOnCallerDropdownTeamsInAFFPopup();
		Common.pause(2);
		softAssert.assertEquals( webApp2.validateCallerDropdownTeamsInAFFPopup(), true);	
		
		assertEquals( webApp2.validateCallStatusDropdownInAFFPopup(), true);	
		webApp2.clickOnCallStatusDropdownInAFFPopup();
				
		softAssert.assertEquals( webApp2.validateCallNotSetupInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateCancelledInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateCompletedInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateIVRmessageInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateMissedInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateNoAnswerInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateRejectedInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateUnavailableInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateVoicemailInAFFPopup(), true);	
		softAssert.assertEquals( webApp2.validateWelcomemessageInAFFPopup(), true);	
		
		assertEquals( webApp2.validateFromNumberInAFFPopup(), true);	
		assertEquals( webApp2.validateToNumberInAFFPopup(), true);	
		assertEquals( webApp2.validatePowerDialerDropdownInAFFPopup(), true);
		
		webApp2.clickOnSelectCallerInAFFPopup();
		webApp2.clickOnPowerDialerDropdownInAFFPopup();
		softAssert.assertEquals( webApp2.validateOnlyPowerDialerInAFFPopup(), true);
		softAssert.assertEquals( webApp2.validateWithoutPowerDialerInAFFPopup(), true);
		
		assertEquals( webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);	
		assertEquals( webApp2.validateCallTypeIncomingRedioButtonInAFFPopup(), true);	
		assertEquals( webApp2.validateCallTypeOutgoingRedioButtonInAFFPopup(), true);	
		webApp2.clickOnPowerDialerDropdownInAFFPopup();
		assertEquals( webApp2.validateFromDateInAFFPopup(), true);	
		assertEquals( webApp2.validateTodDateInAFFPopup(), true);	
		assertEquals( webApp2.validateApplyButtonInAFFPopup(), true);	
		assertEquals( webApp2.validateCloseButtonInAFFPopup(), true);
		webApp2.clickOnCloseButtonInAFFPopup();
}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_given_selection_1 () throws Exception {
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String mainUserName = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println("main user name:"+mainUserName);
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		String subUserName = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println(subUserName);
		
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2EmailSubUser2);
		Common.pause(9);
		String subUser2Name = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println(subUser2Name);
		
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(acc2Number1);
		Common.pause(9);
		String departmentNameForacc1Number2 = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println(departmentNameForacc1Number2);
		
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Common.pause(9);
		String departmentNameForacc2Number2 = webApp2.validateDepartmentNameFor2ndNumber();
		System.out.println(departmentNameForacc2Number2);
		
		
		ArrayList<String> filterTagRecords= new ArrayList<String>();
		ArrayList<String> filterStatusRecords= new ArrayList<String>();
		ArrayList<String> filterClientRecords= new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords= new ArrayList<String>();
		ArrayList<String> filterUserRecords= new ArrayList<String>();
		String fromNumber = null;
		String toNumber = null;
		webApp2.clickOnActivitFeed();

		Common.pause(5);
		
//		if((webApp2.getCallTypeFromWeb()).contains("outgoing")) {
//			fromNumber=webApp2.getNumberFromDepartmentForWeb(1);
//			toNumber=webApp2.getNumberFromclientForWeb();
//		}else if((webApp2.getCallTypeFromWeb()).contains("incoming")) {
//			fromNumber=webApp2.getNumberFromclientForWeb();
//			toNumber=webApp2.getNumberFromDepartmentForWeb(2);
//		}
		if((webApp2.getCallTypeFromWeb()).contains("outgoing")) {
			fromNumber=acc2Number1;
			toNumber=acc1Number1;
		}else if((webApp2.getCallTypeFromWeb()).contains("incoming")) {
			fromNumber=acc1Number1;
			toNumber=acc2Number1;
		}
		
		assertEquals( webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);		
		
		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);
		
		assertEquals( webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
			
		webApp2.clickOnSelectCallerDropdownInAFFPopup();
	
//		softAssert.assertEquals(webApp2.validateCallerDropdownEveryOneInAFFPopup(), true);
//		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
		
		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		webApp2.clickOnUserDropdownInAFFPopup();
		filterUserRecords.add(webApp2.getUserNameInAFFPopup(mainUserName));
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		assertEquals( webApp2.validateCallStatusDropdownInAFFPopup(), true);	
	
		webApp2.clickOnCallStatusDropdownInAFFPopup();
		softAssert.assertEquals( webApp2.validateCheckAllInAFFPopup(), true);
		
		//filterStatusRecords.addAll(webApp2.getStatusAllInAFFPopup("Check All"));
		
		//for particular specified status
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Missed"));
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
	   // System.out.println(webApp2.getTagNameInAFFPopup("ABHISHEK PANDEY"));
		//tag name as a user name
//	   filterTagRecords.add(webApp2.getTagNameInAFFPopup(mainUserName));
//	   filterTagRecords.add(webApp2.getTagNameInAFFPopup(subUserName));
//	   filterTagRecords.add(webApp2.getTagNameInAFFPopup(subUser2Name));
		//tag name as a team name
		
	   filterTagRecords.add(webApp2.getTagNameInAFFPopup(mainUserName,2));
	//   filterTagRecords.add(webApp2.getTagNameInAFFPopup("main user team"));
	   filterTagRecords.add(webApp2.getTagNameInAFFPopup("subuser team",2));
	   filterTagRecords.add(webApp2.getTagNameInAFFPopup("Team",2));
	   
	    webApp2.clickOnFilterTitleInAFFPopup();
	    
	    //enter from and to number
	    webApp2.enterFromNumberInAFFPopup(fromNumber);
	    webApp2.enterToNumberInAFFPopup(toNumber);
	    
	    filterClientRecords.add(fromNumber);
	    filterClientRecords.add(toNumber);
	    
	//select call type
	    assertEquals( webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);	
	    
	    webApp2.clickOnCallTypeIncomingRedioButtonInAFFPopup();
	    
	 //   filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));
	    
	    filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Incoming"));
	    //webApp2.enterFromDateInAFFPopup("17/05/2021");
	    //webApp2.enterToDateInAFFPopup("17/06/2021");
	    
		Common.pause(5);
		webApp2.clickOnApplyButtonInAFFPopup();
		
		Common.pause(5);
		int record=webApp2.getNoOfRecord();
		
		System.out.println("size of Record:"+record);
		System.out.println("filterStatusRecords of record:"+filterStatusRecords);
		System.out.println("filterUserRecords of record:"+filterUserRecords);
		Common.pause(3);
		webApp2.validateRecordOnWeb(record,filterUserRecords,"User");
		webApp2.validateRecordOnWeb(record,filterCallTypeRecords,"Calltype");
		webApp2.validateRecordOnWeb(record,filterStatusRecords,"Status");
	//	webApp2.validateRecordOnWeb(record,filterClientRecords,"Client");
		webApp2.validateTagsOnWeb(record,filterTagRecords);
	
	}
	//21/06/2021
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_is_everyone_Status_is_CheckAll_Tags_is_CheckAll_FromNumber_Yes_ToNumber_Yes_CallType_both () throws Exception {
	
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
	    String fromNumber = null;
		String toNumber = null;
		webApp2.clickOnActivitFeed();

		Common.pause(5);
		if ((webApp2.getCallTypeFromWeb()).contains("outgoing")) {
			fromNumber = acc2Number1;
			toNumber = acc1Number1;
		} else if ((webApp2.getCallTypeFromWeb()).contains("incoming")) {
			fromNumber = acc1Number1;
			toNumber = acc2Number1;
		}

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
			
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		softAssert.assertEquals(webApp2.validateCheckAllInAFFPopup(), true);

		filterStatusRecords.addAll(webApp2.getStatusAllInAFFPopup("Check All"));

		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		webApp2.getTagNameInAFFPopup("Check All",2);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	

		webApp2.clickOnFilterTitleInAFFPopup();

		// enter from and to number
		webApp2.enterFromNumberInAFFPopup(fromNumber);
		webApp2.enterToNumberInAFFPopup(toNumber);

		filterClientRecords.add(fromNumber);
		filterClientRecords.add(toNumber);

		// select call type
		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);
		
		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterAllUserRecords of record:" + filterAllUserRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		//System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		Common.pause(3);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		//webApp2.validateRecordOnWeb(record, filterClientRecords, "Client");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);
	}
		
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_is_AllUser_Status_is_Completed_Tags_is_CheckAll_FromNumber_Yes_ToNumber_No_CallType_both () throws Exception {
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		
	    String fromNumber = null;
		String toNumber = null;
		webApp2.clickOnActivitFeed();

		Common.pause(5);
		if ((webApp2.getCallTypeFromWeb()).contains("outgoing")) {
			fromNumber = acc2Number1;
			toNumber = acc1Number1;
		} else if ((webApp2.getCallTypeFromWeb()).contains("incoming")) {
			fromNumber = acc1Number1;
			toNumber = acc2Number1;
		}

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		webApp2.clickOnSelectCallerDropdownInAFFPopup();
		
		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(1);
		webApp2.clickOnUserDropdownInAFFPopup();
		webApp2.getUserNameInAFFPopup("Check All");
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		softAssert.assertEquals(webApp2.validateCheckAllInAFFPopup(), true);

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
		Common.pause(2);
		webApp2.clickOnFilterTitleInAFFPopup();
	
		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(5);
		webApp2.getTagNameInAFFPopup("Check All",3);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	
		
		webApp2.clickOnFilterTitleInAFFPopup();

		// enter from and to number
		webApp2.enterFromNumberInAFFPopup(fromNumber);

		filterClientRecords.add(fromNumber);
		
		// select call type
		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);
		
		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));
		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);
		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(7);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterAllUserRecords of record:" + filterAllUserRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
	webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
	//	webApp2.validateRecordOnWeb(record, filterClientRecords, "Client");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);
	
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_is_mainUser_Status_is_CompletedAndCallNotSetUp_Tags_is_No_FromNumber_No_ToNumber_No_CallType_both () throws Exception {
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		
		webApp2.clickOnActivitFeed();

		Common.pause(3);
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		webApp2.clickOnSelectCallerDropdownInAFFPopup();
		Common.pause(1);
		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		webApp2.clickOnUserDropdownInAFFPopup();
		filterAllUserRecords.add (webApp2.getUserNameInAFFPopup(mainUserName));
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		softAssert.assertEquals(webApp2.validateCheckAllInAFFPopup(), true);

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Call not setup"));

		// select call type
		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);

		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterAllUserRecords of record:" + filterAllUserRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");


	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_noSelect_Status_is_Missed_Tags_is_No_FromNumber_Yes_ToNumber_Yes_CallType_both  () throws Exception {
		
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		String fromNumber = null;
		String toNumber = null;
		webApp2.clickOnActivitFeed();

		Common.pause(5);
		if ((webApp2.getCallTypeFromWeb()).contains("outgoing")) {
			fromNumber = acc2Number1;
			toNumber = acc1Number1;
		} else if ((webApp2.getCallTypeFromWeb()).contains("incoming")) {
			fromNumber = acc1Number1;
			toNumber = acc2Number1;
		}

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
		
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Missed"));
		
		webApp2.clickOnFilterTitleInAFFPopup();

		// enter from and to number
		webApp2.enterFromNumberInAFFPopup(acc1Number1);
		webApp2.enterToNumberInAFFPopup(acc2Number1);

		filterClientRecords.add(acc2Number1);
		filterClientRecords.add(acc1Number1);
		webApp2.clickOnFilterTitleInAFFPopup();

		// select call type
		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));
		
		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
	//	webApp2.validateRecordOnWeb(record, filterClientRecords, "Client");
	
	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_noSelect_Status_is_Missed_Tags_is_No_FromNumber_No_ToNumber_Yes_CallType_Incoming() throws Exception { //no data is coming with  this selection

		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();

		Common.pause(5);
		
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
//		webApp2.clickOnSelectCallerDropdownInAFFPopup();
//		Common.pause(1);
//		webApp2.clickOnCallerDropdownUsersInAFFPopup();
//		webApp2.clickOnUserDropdownInAFFPopup();
//		filterAllUserRecords.add (webApp2.getUserNameInAFFPopup(subUserName));
//		
//		webApp2.clickOnFilterTitleInAFFPopup();
//		Common.pause(2);
//		
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Missed"));
		webApp2.clickOnFilterTitleInAFFPopup();
	
		// enter from and to number
	
		webApp2.enterToNumberInAFFPopup(acc2Number1);
		
		filterClientRecords.add(acc2Number1);
		webApp2.clickOnFilterTitleInAFFPopup();

		// select call type
		assertEquals(webApp2.validateCallTypeIncomingRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Incoming"));

		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();
	
		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		//System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		//webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		//webApp2.validateRecordOnWeb(record, filterClientRecords, "Client");
	}
	// 8. Dynamic tag : pending
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void  validate_Activity_feed_filter_with_caller_noSelect_Status_is_IVR_Message_Tags_is_No_FromNumber_No_ToNumber_No_CallType_Incoming () throws Exception {
	
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
	
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		webApp2.clickOnActivitFeed();

		Common.pause(5);
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("IVR message"));
		webApp2.clickOnFilterTitleInAFFPopup();
	
		// select call type
		assertEquals(webApp2.validateCallTypeIncomingRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Incoming"));

		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();
		Common.pause(5);
		int record = webApp2.getNoOfRecord();
	
		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
	
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_subUserTeam_Status_is_NoAnwserAndRejected_Tags_is_No_FromNumber_Yes_ToNumber_Yes_CallType_both() throws Exception {
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();
		Common.pause(1);

		webApp2.clickOnCallerDropdownTeamsInAFFPopup();
		webApp2.clickOnTeamDropdownInAFFPopup();
		
		webApp2.getTeamNameInAFFPopup("subuser team");
		filterAllUserRecords.add (subUserName);
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("No Answer"));
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Rejected"));
		webApp2.clickOnFilterTitleInAFFPopup();
		
		// enter from and to number
		webApp2.enterFromNumberInAFFPopup(acc2Number1);
		webApp2.enterToNumberInAFFPopup(acc1Number1);

		filterClientRecords.add(acc2Number1);
		filterClientRecords.add(acc1Number1);
		webApp2.clickOnFilterTitleInAFFPopup();

	
		// select call type

		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();
	
		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		//webApp2.validateRecordOnWeb(record, filterClientRecords, "Client");
	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_noSelect_Status_is_Unavailable_Tags_is_No_FromNumber_No_ToNumber_No_CallType_both () throws Exception {
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
	
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		//filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
	
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();
		Common.pause(5);
	
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Unavailable"));
		webApp2.clickOnFilterTitleInAFFPopup();
		
		//filterAllUserRecords.add("-"); // assert with  - due to selection of Unavailable status
		// select call type

		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();
	
		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		//System.out.println("filterAllUserRecords of record:" + filterAllUserRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		//webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
	}
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_noSelect_Status_is_Voicemail_Tags_is_No_FromNumber_No_ToNumber_No_CallType_Incoming () throws Exception {
		//ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();
		
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Voicemail"));
		webApp2.clickOnFilterTitleInAFFPopup();

		// select call type
		assertEquals(webApp2.validateCallTypeIncomingRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Incoming"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();
	
		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		//System.out.println("filterAllUserRecords of record:" + filterAllUserRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		//webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
	}
	
    //22/6/21
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_everyone_Status_is_Welcome_Message_Tags_is_No_FromNumber_No_ToNumber_Yes_CallType_Both () throws Exception {
		
	//	ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		// call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Welcome message"));
		webApp2.clickOnFilterTitleInAFFPopup();
		
		// enter from and to number

		webApp2.enterToNumberInAFFPopup(acc2Number1);

		filterClientRecords.add(acc2Number1);
		webApp2.clickOnFilterTitleInAFFPopup();
		
		// select call type

		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();
	
		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		
		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		//webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
	}
	//team test case
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_allTeam_Status_is_Completed_Tags_is_Check_All_FromNumber_No_ToNumber_No_CallType_Both () throws Exception {
		
		ArrayList<String> filterAllTeamRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownTeamsInAFFPopup();
		webApp2.clickOnTeamDropdownInAFFPopup();
		
		webApp2.getTeamNameInAFFPopup("Check All");	
		filterAllTeamRecords.add("Old");filterAllTeamRecords.add(mainUserName);filterAllTeamRecords.add(subUserName);filterAllTeamRecords.add(subUser2Name);
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		// call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		webApp2.getTagNameInAFFPopup("Check All",3);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	
		Common.pause(2);
		webApp2.clickOnFilterTitleInAFFPopup();
		
		// select call type

		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllTeamRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);

	}
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_allUsers_Status_is_Completed_Tags_is_MainUser_FromNumber_No_ToNumber_No_CallType_Both () throws Exception {
		
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();
	
		Common.pause(5);
	
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		webApp2.clickOnUserDropdownInAFFPopup();
		
		webApp2.getUserNameInAFFPopup("Check All");
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
			
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		// call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
	
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		allTagForCheckAll.add(webApp2.getTagNameInAFFPopup(mainUserName,1));
		Common.pause(3);
		webApp2.clickOnFilterTitleInAFFPopup();
		
		// select call type

		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);

	}
	
	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_subUsers_Status_is_MissedCompletedWelcomemessage_Tags_is_subUser_FromNumber_No_ToNumber_No_CallType_Both () throws Exception {
		
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		webApp2.clickOnUserDropdownInAFFPopup();
		
		filterAllUserRecords.add(webApp2.getUserNameInAFFPopup(subUserName));
			
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		// call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Missed"));
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Welcome message"));
	
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		allTagForCheckAll.add(webApp2.getTagNameInAFFPopup(subUserName,1));
				
		webApp2.clickOnFilterTitleInAFFPopup();
		
		// select call type

	    assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);
		
		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));


		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		//System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);

	}
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class) //pending
	public void validate_Activity_feed_filter_with_caller_mainuserteam_Status_is_noSelect_Tags_is_mainUserAndsubUser_FromNumber_No_ToNumber_No_CallType_Incoming () throws Exception {
			
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");

		webApp2.clickOnActivitFeed();
		Common.pause(5);
	
		webApp2.clickOnActivitFeed();

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownTeamsInAFFPopup();
		webApp2.clickOnTeamDropdownInAFFPopup();
		
		webApp2.getTeamNameInAFFPopup("main user team");
		filterAllUserRecords.add (mainUserName);
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		allTagForCheckAll.add(webApp2.getTagNameInAFFPopup(mainUserName,1));
		allTagForCheckAll.add(webApp2.getTagNameInAFFPopup(subUserName,1));
				
		webApp2.clickOnFilterTitleInAFFPopup();
	
		// select call type
		assertEquals(webApp2.validateCallTypeIncomingRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Incoming"));

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		//System.out.println("filterClientRecords of record:" + filterClientRecords);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);

	}
	
	//power dialer test case
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_everyone_Status_is_noSelect_Tags_is_Check_All_FromNumber_No_ToNumber_No_only_power_dialer () throws Exception {
		
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();
		
		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
	
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownEveryOneInAFFPopup();
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		webApp2.getTagNameInAFFPopup("Check All",1);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	
	
		webApp2.clickOnFilterTitleInAFFPopup();
		
		//power dialer
		webApp2.clickOnPowerDialerDropdownInAFFPopup();
		webApp2.clickOnOnlyPowerDialerDropdownInAFFPopup();
		Common.pause(3);
		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
	
		System.out.println("filterAllUserRecords of record:" + filterAllUserRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);
		webApp2.validatePowerDialerRecordOnWeb(record);

	}
	
	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_allTeam_Status_is_Completed_Tags_is_Check_All_FromNumber_Yes_ToNumber_No_only_power_dialer_with_campaign_1()
			throws Exception {

		ArrayList<String> filterAllTeamRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
	
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();

		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");

		webApp2.clickOnActivitFeed();
		Common.pause(5);

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownTeamsInAFFPopup();
		webApp2.clickOnTeamDropdownInAFFPopup();
		
		webApp2.getTeamNameInAFFPopup("Check All");
	  filterAllTeamRecords.add(mainUserName);	filterAllTeamRecords.add(subUserName);	filterAllTeamRecords.add(subUser2Name);
			
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		// call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
	
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		webApp2.getTagNameInAFFPopup("Check All",3);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	
		
		webApp2.clickOnFilterTitleInAFFPopup();
		//power dialer
		webApp2.clickOnPowerDialerDropdownInAFFPopup();
		webApp2.clickOnOnlyPowerDialerDropdownInAFFPopup();
		webApp2.selectCampaignforPowerDialerDropdownInAFFPopup("auto camp1");
	
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnApplyButtonInAFFPopup();
		
		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterClientRecords of record:" + filterAllTeamRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		webApp2.validateRecordOnWeb(record, filterAllTeamRecords, "User");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);
		webApp2.validatePowerDialerRecordOnWeb(record);
	}
	
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_allUsers_Status_is_CompletedAndNoAnswer_Tags_is_Check_All_FromNumber_No_ToNumber_No_only_power_dialer_with_allcampaign()
			throws Exception {
		
		csv.deleteAllFile("activityFeedReport"); 
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
	
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
	
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();

		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
	
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		
		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(1);
		webApp2.clickOnUserDropdownInAFFPopup();
		webApp2.getUserNameInAFFPopup("Check All");

		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
			
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		
		// call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Completed"));
		filterStatusRecords.add(webApp2.getStatusInAFFPopup("No Answer"));
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		webApp2.getTagNameInAFFPopup("Check All",3);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	
		
		webApp2.clickOnFilterTitleInAFFPopup();
		//power dialer
		webApp2.clickOnPowerDialerDropdownInAFFPopup();
		webApp2.clickOnOnlyPowerDialerDropdownInAFFPopup();
		webApp2.selectCampaignforPowerDialerDropdownInAFFPopup("Check All");
	
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterClientRecords of record:" + filterAllUserRecords);
		System.out.println("filterStatusRecords of record:" + filterStatusRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterStatusRecords, "Status");
		webApp2.validateRecordOnWeb(record, filterAllUserRecords, "User");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);
		webApp2.validatePowerDialerRecordOnWeb(record);
		
		//validate power dialer entry in csv 
		Common.pause(5);
		webApp2.clickOnCheckBoxOption(record);
		Common.pause(3);
		assertTrue( webApp2.getSelectedRecords().contains(String.valueOf(record)));	 
		webApp2.clickOnFileDownload();
		
		Common.pause(10);
		
		dashboard.ValidateActivityFeedReportOnGmail();
		Common.pause(3);
		assertTrue(csv.verifyDownloadFileName().contains("activityFeedReport"));
		
		Common.pause(5);
	
		csvRecords = csv.readDataFromActivityFeedCsv(csv.verifyDownloadFileName(), "powerDialerCall");
		
	    System.out.print(csvRecords);
		assertEquals(csvRecords, "true");
		
	}
	
	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_mainuserteam_Status_is_noselect_Tags_is_Check_All_FromNumber_Yes_ToNumber_No_without_power_dialer_CallType_Both()
			throws Exception {

		ArrayList<String> filterAllTeamRecords = new ArrayList<String>();

		ArrayList<String> filterClientRecords = new ArrayList<String>();
		ArrayList<String> filterDepartmentRecords = new ArrayList<String>();
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
	
		ArrayList<String> allTagForCheckAll = new ArrayList<String>();

		filterDepartmentRecords.add(departmentNameForacc1Number2);filterDepartmentRecords.add("-");
		String fromNumber = null;
		String toNumber = null;

		webApp2.clickOnActivitFeed();
		Common.pause(5);
		if ((webApp2.getCallTypeFromWeb()).contains("outgoing")) {
			fromNumber = acc2Number1;
			toNumber = acc1Number1;
		} else if ((webApp2.getCallTypeFromWeb()).contains("incoming")) {
			fromNumber = acc1Number1;
			toNumber = acc2Number1;
		}

		webApp2.clickOnActivitFeed();

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);

		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownTeamsInAFFPopup();
		webApp2.clickOnTeamDropdownInAFFPopup();
		
		webApp2.getTeamNameInAFFPopup("main user team");
			filterAllTeamRecords.add(mainUserName);

		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);

		webApp2.clickOnTagsDropdownInAFFPopup();
		Common.pause(2);
		webApp2.getTagNameInAFFPopup("Check All",2);
		allTagForCheckAll.add("Old");allTagForCheckAll.add(mainUserName);allTagForCheckAll.add(subUserName);allTagForCheckAll.add(subUser2Name);allTagForCheckAll.add("main user team"); allTagForCheckAll.add("subuser team");allTagForCheckAll.add("Team");	
	
	    //enter from and to number
	    webApp2.enterFromNumberInAFFPopup(acc2Number1);
	  
	    filterClientRecords.add(acc2Number1);
		
		webApp2.clickOnFilterTitleInAFFPopup();
		//power dialer
		webApp2.clickOnPowerDialerDropdownInAFFPopup();
		webApp2.clickOnWithoutPowerDialerDropdownInAFFPopup();
		
		
		//add one campaign here
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		// select call type

	    assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);
		
		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));


		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		System.out.println("filterDepartmentRecords of record:" + filterDepartmentRecords);
		System.out.println("filterAllTeamRecords of record:" + filterAllTeamRecords);
		System.out.println("allTagForCheckAll of record:" + allTagForCheckAll);
		System.out.println("filterCallTypeRecords of record:" + filterCallTypeRecords);
		Common.pause(3);

		webApp2.validateRecordOnWeb(record, filterDepartmentRecords, "Department");
		webApp2.validateRecordOnWeb(record, filterAllTeamRecords, "User");
		webApp2.validateRecordOnWeb(record, filterCallTypeRecords, "Calltype");
		webApp2.validateTagsOnWeb(record, allTagForCheckAll);
		webApp2.validateWithoutPowerDialerRecordOnWeb(record);

	}
	
	
	//Missed, VoiceMail, Call not setup with No data and , Unavailable, on hold
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_is_AllUsers_Status_is_Missed_Tags_is_No_FromNumber_No_ToNumber_No_CallType_both  () throws Exception {
		
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
	
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		
		webApp2.clickOnActivitFeed();

		Common.pause(5);

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
		
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(1);
		webApp2.clickOnUserDropdownInAFFPopup();
		webApp2.getUserNameInAFFPopup("Check All");
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Missed"));
		
		webApp2.clickOnFilterTitleInAFFPopup();

		// select call type
		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));
		
		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		assertEquals(record, 0,"No Records are available in activity feed.");
		assertEquals(webApp2.validateNoDataTextInActivityFeed(),true,"No Data text is available in activity feed.");
	}
	
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class) 
	public void validate_Activity_feed_filter_with_caller_is_AllUsers_Status_is_Voicemail_Tags_is_No_FromNumber_No_ToNumber_No_CallType_Outgoing  () throws Exception {
		
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
	
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		
		webApp2.clickOnActivitFeed();

		Common.pause(5);

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
		
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(1);
		webApp2.clickOnUserDropdownInAFFPopup();
		webApp2.getUserNameInAFFPopup("Check All");
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Voicemail"));
		
		webApp2.clickOnFilterTitleInAFFPopup();

		// select call type
		assertEquals(webApp2.validateCallTypeIncomingRedioButtonInAFFPopup(), true);

		filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Outgoing"));
		
		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
	
		assertEquals(record, 0,"No Records are available in activity feed.");
		assertEquals(webApp2.validateNoDataTextInActivityFeed(),true,"No Data text is available in activity feed.");
	}
	
	
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Activity_feed_filter_with_caller_is_AllUsers_Status_is_Call_not_setup_Tags_is_No_FromNumber_No_ToNumber_No_CallType_Incoming  () throws Exception {
		
		ArrayList<String> filterStatusRecords = new ArrayList<String>();
	
		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
		
		webApp2.clickOnActivitFeed();

		Common.pause(5);

		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);

		webApp2.clickOnFilterIconOnActivityFeedPage();
		Common.pause(3);

		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
		
		webApp2.clickOnSelectCallerDropdownInAFFPopup();

		webApp2.clickOnCallerDropdownUsersInAFFPopup();
		
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(1);
		webApp2.clickOnUserDropdownInAFFPopup();
		webApp2.getUserNameInAFFPopup("Check All");
		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
		webApp2.clickOnFilterTitleInAFFPopup();
		Common.pause(2);
		//call status
		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);

		webApp2.clickOnCallStatusDropdownInAFFPopup();

		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Call not setup"));
		
		webApp2.clickOnFilterTitleInAFFPopup();

		// select call type
		webApp2.clickOnCallTypeIncomingRedioButtonInAFFPopup();
		    
       filterCallTypeRecords.add(webApp2.getCallTypeInAFFPopup("Incoming"));
		
		webApp2.enterFromDateInAFFPopup(-1, -5);
		webApp2.enterToDateInAFFPopup(0, 0);

		webApp2.clickOnApplyButtonInAFFPopup();

		Common.pause(5);
		int record = webApp2.getNoOfRecord();

		System.out.println("size of Record:" + record);
		assertEquals(record, 0,"No Records are available in activity feed.");
		assertEquals(webApp2.validateNoDataTextInActivityFeed(),true,"No Data text is available in activity feed.");
	}
//	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class) //on hold
//	public void validate_Activity_feed_filter_with_caller_is_AllUsers_Status_is_Unavailable_Tags_is_No_FromNumber_No_ToNumber_No_CallType_both  () throws Exception {
//		
//		ArrayList<String> filterStatusRecords = new ArrayList<String>();
//	
//		ArrayList<String> filterCallTypeRecords = new ArrayList<String>();
//		ArrayList<String> filterAllUserRecords = new ArrayList<String>();
//		
//		webApp2.clickOnActivitFeed();
//
//		Common.pause(5);
//
//		assertEquals(webApp2.validateFilterIconIsDisplayedInActivityFeedPage(), true);
//
//		webApp2.clickOnFilterIconOnActivityFeedPage();
//		Common.pause(3);
//
//		assertEquals(webApp2.validatActivityFeedFilterPopupIsDispayed(), true);
//		
//		webApp2.clickOnSelectCallerDropdownInAFFPopup();
//
//		webApp2.clickOnCallerDropdownUsersInAFFPopup();
//		
//		webApp2.clickOnFilterTitleInAFFPopup();
//		Common.pause(1);
//		webApp2.clickOnUserDropdownInAFFPopup();
//		webApp2.getUserNameInAFFPopup("Check All");
//		filterAllUserRecords.add(mainUserName);filterAllUserRecords.add(subUserName);filterAllUserRecords.add(subUser2Name);
//		webApp2.clickOnFilterTitleInAFFPopup();
//		Common.pause(2);
//		//call status
//		assertEquals(webApp2.validateCallStatusDropdownInAFFPopup(), true);
//
//		webApp2.clickOnCallStatusDropdownInAFFPopup();
//
//		filterStatusRecords.add(webApp2.getStatusInAFFPopup("Unavailable"));
//		
//		webApp2.clickOnFilterTitleInAFFPopup();
//
//		// select call type
//		assertEquals(webApp2.validateCallTypeBothRedioButtonInAFFPopup(), true);
//
//		filterCallTypeRecords.addAll(webApp2.getCallTypeBothInAFFPopup("Both"));
//		
//		webApp2.enterFromDateInAFFPopup(-1, -5);
//		webApp2.enterToDateInAFFPopup(0, 0);
//
//		webApp2.clickOnApplyButtonInAFFPopup();
//
//		Common.pause(5);
//		int record = webApp2.getNoOfRecord();
//
//		System.out.println("size of Record:" + record);
//		assertEquals(record, 0,"No Records are available in activity feed.");
//		assertEquals(webApp2.validateNoDataTextInActivityFeed(),true,"No Data text is available in activity feed.");
//	}
	
	
}



