package com.CallHippo.web.addDeleteTeam;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.expectThrows;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.google.inject.spi.Element;
//import com.google.errorprone.annotations.IncompatibleModifiers;

public class AddDeleteTeamPage  {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait2;
	Actions action;

	PropertiesFile url;
	JavascriptExecutor js;
	PropertiesFile credential;

	public AddDeleteTeamPage(WebDriver driver) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		url = new PropertiesFile("Data\\url Configuration.properties");
		js = (JavascriptExecutor) driver;

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}




	@FindBy(xpath = "//h2[contains(.,'Teams')]")
 WebElement teamPageHeader;
	
	@FindBy(xpath = "//p[@class='deparmentname_description']")
	WebElement teamPageDescription;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Create Team')]")
	WebElement createTeam;
	@FindBy(xpath = "//h1[contains(.,'Create Team')]")
	WebElement creatTeamHeader;
	@FindBy(xpath = "//h1[contains(.,'Edit Team')]")
	WebElement editButtonHeader;
	@FindBy(xpath = "//input[@name='teamName']")
	WebElement teamName;
	
	@FindBy(xpath = "//span[contains(.,'delete_forever')]")
	WebElement deleteTeam;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement yesButton;
	
	@FindBy(xpath = "//p[contains(.,'Are you sure you want to delete this record?')]")
	WebElement deleteTeamPopupMessage;
	@FindBy(xpath = "(//span[contains(.,'Please delete the IVR(s) associated with this team before deleting this team.')])[2]")
	WebElement validationMessageWhenTeamIsAssignedInIVR;
	@FindBy(xpath = "//i[contains(.,'settings')]")
	WebElement teamsEditButton;
	@FindBy(xpath = "(//div[contains(@class,'listSortEmail')])[1]")
	WebElement firstUserTeam;
	@FindBy(xpath = "(//div[contains(@class,'listSortEmail')])[2]")
	WebElement secondUserTeam;
	@FindBy(xpath = "(//div[contains(@class,'listSortEmail')])[3]")
	WebElement thirdUserTeam;
	@FindBy(xpath = "//input[contains(@value,'simultaneously')]")
	WebElement simultaneousButton;
	@FindBy(xpath = "//input[@value='fixedorder']")
	WebElement fixedOrder;
	@FindBy(xpath = "//input[@value='roundrobin']")
	WebElement roundRobin;
	@FindBy(xpath = "//button[@type='submit']")
	
	WebElement submitButton;
	@FindBy(xpath = "(//span[contains(.,'Team created successfully')])[2]")
	WebElement addTeamSuccessMessage;
	@FindBy(xpath = "(//div[contains(.,'Team with this name is already exists, please use another name.')])[5]")
	WebElement validationMessageForDuplicateTeamName;
	@FindBy(xpath = "//input[contains(@class,'ant-input searchinttext')]")
	WebElement searchOptionInTeamPage;
	@FindBy(xpath = "(//div[contains(.,'Team deleted successfully')])[5]")
	WebElement deleteTeamSuccessMessage;
	@FindBy(xpath = "//a[@class='teamMemberNameSpan']")
	WebElement teamNameInList;
	@FindBy(xpath = "//p[contains(.,'The call will go to all selected users in simultaneous order...')]")
	WebElement simultaneousDescription;
	@FindBy(xpath = "//h4[contains(.,'Simultaneous Order')]")
	WebElement simultaneousSideHeading;
	@FindBy(xpath = "//h4[contains(.,'Fixed Order')]")
	WebElement fixedorderSideHeading;
	@FindBy(xpath = "//p[contains(.,'Call will go to same priority users at once. Ex: if user1, user2, user3 has priority 1, user4 and user5 has priority 2. user6 and user7 has priority 3. Call will ring to user1, user2 and user3 on same time, after that call will ring to user4 and user5 at same time and after that to user6 and user7 at same time.')]")
	WebElement fixedordernDescription;
	@FindBy(xpath = "(//i[contains(.,'add')])[1]")
	WebElement addButtoninIVR;
	@FindBy(xpath = "//input[@id='pressInputValueAdd']")
	WebElement enterValueInIvrField;
	@FindBy(xpath = "//div[@class='ivrsection']//div[@class='ivrconainer fwidth mgtop15']/child::div//*[@class[contains(.,'gutter-row padzero mgbtm10')]]/child::div/div/div[@class[contains(.,'ivrdrpdwnwid ant-select')]]")
    WebElement openDropdownForIVR;
	@FindBy(xpath = "//div[@class='ivrsection']//div[@class='ivrconainer fwidth mgtop15']/child::div//*[@class[contains(.,'gutter-row padzero mgbtm10')]]/child::div/div/div[2]/child::div/div")
    WebElement openDropdownForNumUserTeam;
	
	@FindBy(xpath = "(//div[contains(.,'Select team')])[36]")
	WebElement teamListInIvr;
	@FindBy(xpath = "//li[@role='option'][contains(.,'testteam')]")
	WebElement selectTeamInIVR;
	@FindBy(xpath = "(//span[contains(.,'Please delete the IVR(s) associated with this team before deleting this team.')])[2]")
	WebElement validationMessageTeamInIVR;
	
	
	@FindBy(xpath = "(//p[@class='mgright5'][contains(.,'Press')]/following-sibling::p[@class='ivrListDigit mgright5'][contains(.,'1')]/parent::div/parent::li/parent::ul/parent::div/following-sibling::div/div[@class='circlestmdl ivr_msg_savefn ivrdltbtn fleft'][contains(.,'delete_forever')]")
	WebElement deleteAssignedIVR;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement deleteUser_popup_yesButton;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement deleteUser_popup_YESButton;
	@FindBy(xpath = "//div[@class='ivrsection']//div[@class='ivrconainer fwidth mgtop15']/child::div//*[contains(@class,'gutter-row padzero mgbtm10')]/child::div/child::div/i[contains(.,'save')]")
	WebElement clickOnIvrSaveBtn;
//	public String getTitle() {
//		wait.until(ExpectedConditions.titleContains("Teams | Callhippo.com"));
//		String signupTitle = driver.getTitle();
//		return signupTitle;
//	}
	
	
	public String simultaneousSideHeading() {
		wait.until(ExpectedConditions.visibilityOf(simultaneousSideHeading));
		return simultaneousSideHeading.getText();
	}
	public String fixedorderSideHeading() {
		wait.until(ExpectedConditions.visibilityOf(fixedorderSideHeading));
		return fixedorderSideHeading.getText();
	}
	public String fixedOrderDescription() {
		wait.until(ExpectedConditions.visibilityOf(fixedordernDescription));
		return fixedordernDescription.getText();
	}
	public String simultaneousDescription() {
		wait.until(ExpectedConditions.visibilityOf(simultaneousDescription));
		return simultaneousDescription.getText();
	}
	public String teamPageHeader() {
		wait.until(ExpectedConditions.visibilityOf(teamPageHeader));
		return teamPageHeader.getText();
	}
	public String createTeamPageHeader() {
		wait.until(ExpectedConditions.visibilityOf(creatTeamHeader));
		return creatTeamHeader.getText();
	}
	public String editTeamHeader() {
		wait.until(ExpectedConditions.visibilityOf(editButtonHeader));
		return editButtonHeader.getText();
	}
	public String teamNameinPage() {
		wait.until(ExpectedConditions.visibilityOf(teamNameInList));
		return teamNameInList.getText();
	}
	public String validatePageTitleDescription() {
		wait.until(ExpectedConditions.visibilityOf(teamPageDescription));
		return teamPageDescription.getText();
	}	
	public void clickOnCreateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createTeam));
		createTeam.click();
		
	}	
	public void clickOnSubmitButton() {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.click();
		
	}	
		
public String enterTeamName(String teamName) {
			wait.until(ExpectedConditions.visibilityOf(this.teamName));
			Common.pause(3);
			wait.until(ExpectedConditions.visibilityOf(this.teamName)).clear();
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(this.teamName)).sendKeys(teamName);
			return teamName;
		}
public void clickOnDeleteTeamButton() {
	wait.until(ExpectedConditions.elementToBeClickable(deleteTeam));
	deleteTeam.click();
	Common.pause(2);
}
public void clickOnDeleteTeamYesButton() {
			wait.until(ExpectedConditions.elementToBeClickable(yesButton));
			yesButton.click();
			Common.pause(2);
		}
public String getDeleteTeamComfirmationMessage() {
	return wait.until(ExpectedConditions.visibilityOf(deleteTeamPopupMessage)).getText();
	
	}
public void clickOnEditButton() {
	wait.until(ExpectedConditions.visibilityOf(teamsEditButton)).click();
}
public void clickOnFirstUserTeam() {
	wait.until(ExpectedConditions.elementToBeClickable(firstUserTeam));
	firstUserTeam.click();
	Common.pause(2);
}
public void clickOnSecondUserTeam() {
	wait.until(ExpectedConditions.elementToBeClickable(secondUserTeam));
	secondUserTeam.click();
	Common.pause(2);
}
public void clickOnThirdUserTeam() {
	wait.until(ExpectedConditions.elementToBeClickable(thirdUserTeam));
	thirdUserTeam.click();
	Common.pause(2);		
}	
public void clickOnSimultaneously() {
	Common.pause(3);
	simultaneousButton.click();
}
public void clickOnFixedOrder() {
	Common.pause(3);
	fixedOrder.click();
}
public void clickOnRoundRobin() {
	Common.pause(3);
	roundRobin.click();
}
public String validateAddTeamSuccessmessage() {
	return wait.until(ExpectedConditions.visibilityOf(addTeamSuccessMessage)).getText();
	
}
public String validateDeleteTeamSuccessmessage() {
	return wait.until(ExpectedConditions.visibilityOf(deleteTeamSuccessMessage)).getText();
}
public String validateDuplicateTeamNameValidationMessage() {
	return wait.until(ExpectedConditions.visibilityOf(validationMessageForDuplicateTeamName)).getText();
	
}
//validationMessageTeamInIVR
public String validateWhenDeleteTeamWhichIsAssignedInIVRTeamNameV() {
	return wait.until(ExpectedConditions.visibilityOf(validationMessageTeamInIVR)).getText();
}
//public String validateCampaignsName() {
//	return wait.until(ExpectedConditions.visibilityOf(campaignsTitle)).getText();
//}		
public void enterTeamNameInSearchBox(String searchOptionInTeamPage) {
	wait.until(ExpectedConditions.visibilityOf(this.searchOptionInTeamPage));
	Common.pause(3);
	wait.until(ExpectedConditions.visibilityOf(this.searchOptionInTeamPage)).clear();
	Common.pause(2);
	wait.until(ExpectedConditions.visibilityOf(this.searchOptionInTeamPage)).sendKeys(searchOptionInTeamPage);
	Common.pause(5);
}
public void deleteAllTeams() {
	boolean isElementDisplayed = true;
	try {
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("(//span[contains(.,'delete_forever')])[1]"))));
		isElementDisplayed = true;
	} catch (Exception e) {
		isElementDisplayed = false;
	}

	if (isElementDisplayed == true) {
		List<WebElement> elementList = driver.findElements(By.xpath("//span[contains(.,'delete_forever')]"));
		for (int i = 0; i <= elementList.size(); i++) {

			List<WebElement> newElementList = driver.findElements(By.xpath("//span[contains(.,'delete_forever')]"));
			if (newElementList.size() > 0) {
//				newElementList.get(0).click();
				Common.pause(1);
				deleteTeam.click();
				Common.pause(1);
				clickOnDeleteTeamYesButton();
				Common.pause(3);
			}
		}
		Common.pause(1);
	}
}
public void clickOnAddButtonIVR() {
	wait.until(ExpectedConditions.elementToBeClickable(addButtoninIVR));
	addButtoninIVR.click();
	Common.pause(2);
}
public void enterValueInIvr(String enterValueInIvrField) {
	wait.until(ExpectedConditions.visibilityOf(this.enterValueInIvrField));
	Common.pause(3);
	wait.until(ExpectedConditions.visibilityOf(this.enterValueInIvrField)).clear();
	Common.pause(2);
	wait.until(ExpectedConditions.visibilityOf(this.enterValueInIvrField)).sendKeys(enterValueInIvrField);
//
}		
public void clickOnDropdownInIVR(String numUserTeam) {
	wait.until(ExpectedConditions.visibilityOf(openDropdownForIVR));
	openDropdownForIVR.click();
	wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+numUserTeam+"')]"))))
			.click();
	Common.pause(2);	
}		
public void clickOnDeleteBtnOfnumUserTeam(String numUserTeam) {
	wait.until(ExpectedConditions.visibilityOf(openDropdownForIVR));
	openDropdownForIVR.click();
	wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath(""
					+ "(//p//div[contains(.,'Team')])/../../../../../following-sibling::div//i[contains(.,'delete_forever')]")))).click();
		
	Common.pause(2);	
}		
public void clickOnDropdownNumUserTeam(String numUserTeam) {
	wait.until(ExpectedConditions.visibilityOf(openDropdownForNumUserTeam));
	openDropdownForNumUserTeam.click();
	wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+numUserTeam+"')]"))))
			.click();
	Common.pause(2);	
}		

public void viewTeamListInIVR() {
	wait.until(ExpectedConditions.elementToBeClickable(teamListInIvr));
	teamListInIvr.click();
	Common.pause(2);
	
	//selectTeamInIVR
}
public void addTeamInIVRDropdown() {
	wait.until(ExpectedConditions.elementToBeClickable(selectTeamInIVR));
	selectTeamInIVR.click();
	Common.pause(2);
	//saveButtonInIvr
}

public void ClickonDeltebuttonofNumUserTeam(String numTeamUser) {
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
			+ "(//p//div[contains(.,'"+numTeamUser+"')])/../../../../../following-sibling::div//i[contains(.,'delete_forever')]")))).click();
	Common.pause(2);
	clickOnYESButton();
	}
public void DeleteSpecificTeam(String Teamname) {
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
			"//tr[contains(@class,'ant-table-row')]/child::td//a[contains(.,'"+Teamname+"')]/parent::td/following-sibling::td/following-sibling::td/child::div/child::a//following-sibling::span")))).click();
	Common.pause(2);
	clickOnYesButton();
}
////tr[contains(@class,'ant-table-row')]/child::td//a[contains(.,'aaaaaaaa')]
public boolean validateSpecificTeamOnTeamPage(String Teamname) {
	try {
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
			"//tr[contains(@class,'ant-table-row')]/child::td//a[contains(.,'"+Teamname+"')]"))));
	Common.pause(2);
	return true;
	}catch (Exception e) {
		return false;
	}
}
public void clickOnYesButton() {
	wait.until(ExpectedConditions.elementToBeClickable(deleteUser_popup_yesButton)).click();

}
public void clickOnYESButton() {
	wait.until(ExpectedConditions.elementToBeClickable(deleteUser_popup_YESButton)).click();
}
public void SaveIvrOption() {
	wait.until(ExpectedConditions.elementToBeClickable(clickOnIvrSaveBtn)).click();
}
}