package com.CallHippo.web.addDeleteTeam;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

//import org.omg.CORBA.ORB;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.*;
import com.CallHippo.web.authentication.ChangePasswordPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class AddDeleteTeamTest {
	AddDeleteTeamPage addDeleteTeamPage;

	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp2SubUser;
	WebDriver driver2;
	DialerIndex phoneApp2Sub2;
	WebDriver driver3;
	WebToggleConfiguration webApp2;
	WebDriver driver4;

	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2EmailMainUser = data.getValue("account2MainEmail");
	String account2PasswordMainUser = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2EmailSub2 = data.getValue("account2Sub2Email");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String number2 = data.getValue("account1Number1"); // account 1's number
	String account5Email = data.getValue("account5MainEmail");
	String number5 = data.getValue("account5Number1");
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String callerName2 = null;
	String callerName2Sub = null;
	String CallerName2Sub2 = null;
	String numberID;
	String userID;
	String subuserID;

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	
	
	// Team01_06_20_31_40
	
	Common excelTestResult;

	public AddDeleteTeamTest() throws Exception {
		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();

		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}

	@BeforeTest
	
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		addDeleteTeamPage = PageFactory.initElements(driver4, AddDeleteTeamPage.class);
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver2, DialerIndex.class);
		
		driver3 = TestBase.init_dialer();
		phoneApp2Sub2 = PageFactory.initElements(driver3, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
				
			} catch (Exception e) {
				driver4.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			try {
				driver3.get(url.dialerSignIn());
				loginDialer(phoneApp2Sub2, account2EmailSub2, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver3.get(url.dialerSignIn());
				loginDialer(phoneApp2Sub2, account2EmailSub2, account2PasswordSubUser);
				Common.pause(3);
			}

			try {

				phoneApp2.clickOnSideMenu();
				callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				callerName2Sub = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

			} catch (Exception e) {

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver2.get(url.dialerSignIn());

			}
		} catch (Exception e1) {
			String testname = "Team Calling Test - Before Method";
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp2Suser Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driver3, testname, "PhoneApp2Sub2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			driver3.get(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}

			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			if (phoneApp2Sub2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Sub2, account2EmailSub2, account2PasswordSubUser);
				Common.pause(3);
			}
//			driver4.get(url.teamPage());
//			addDeleteTeamPage.deleteAllTeams();

		} catch (Exception e) {
			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				driver3.get(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2EmailMainUser, account2PasswordMainUser);
					Common.pause(3);
				}

				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);

				}
				if (phoneApp2Sub2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Sub2, account2EmailSub2, account2PasswordSubUser);
					Common.pause(3);
				}

//				driver4.get(url.teamPage());
//				addDeleteTeamPage.deleteAllTeams();

			} catch (Exception e1) {
				String testname = "Team Calling Test - Before Method";

				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver3, testname, "PhoneApp2Sub2 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");

				System.out.println("\n" + testname + " has been fail... \n");
			}
		}

	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";

			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "PhoneAppp2Sub2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "PhoneAppp2Sub2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver1.quit();
		driver2.quit();
		driver3.quit();
		driver4.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void Verify_Description_on_Team_Page() throws Exception {
		driver4.get(url.teamPage());
		Common.pause(5);
		String actualResult1 = addDeleteTeamPage.validatePageTitleDescription();
		String expectedResult1 = "Different teams created in your account are shown below. You can also make changes to a Team by clicking on the gear icon. To know more, Click here";
		assertEquals(actualResult1, expectedResult1);
		String actualResult = addDeleteTeamPage.teamPageHeader();
		String expectedResult = "Teams";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Create_Team() throws Exception {
		driver4.get(url.teamPage());// navigate to team page
		
		addDeleteTeamPage.clickOnCreateButton();
		String actualResult = addDeleteTeamPage.createTeamPageHeader();
		String expectedResult = "Create Team";
		assertEquals(actualResult, expectedResult);
		String team = "team"+Common.teamName();
		 addDeleteTeamPage.enterTeamName(team);//store value 
		
		 addDeleteTeamPage.clickOnSimultaneously();
		addDeleteTeamPage.clickOnFirstUserTeam();
		addDeleteTeamPage.clickOnSecondUserTeam();
		addDeleteTeamPage.clickOnThirdUserTeam();
		addDeleteTeamPage.clickOnSubmitButton();
		String actualResult1 = addDeleteTeamPage.validateAddTeamSuccessmessage();
		String expectedResult1 = "Team created successfully";
		assertEquals(actualResult1, expectedResult1);
		Common.pause(5);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		String actualResult2 = addDeleteTeamPage.teamNameinPage();
		String expectedResult2 = team;
		assertEquals(actualResult2, expectedResult2);
		Common.pause(5);
		addDeleteTeamPage.DeleteSpecificTeam(team);
		String actualResult3 = addDeleteTeamPage.validateDeleteTeamSuccessmessage();
		String expectedResult3 = "Team deleted successfully";
		assertEquals(actualResult3, expectedResult3);
		System.out.println(team);
		assertEquals(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team),false);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void Delete_Team() throws Exception {
		driver4.get(url.teamPage());
		addDeleteTeamPage.clickOnCreateButton();
		String actualResult = addDeleteTeamPage.createTeamPageHeader();
		String expectedResult = "Create Team";
		assertEquals(actualResult, expectedResult);
		String team = "team"+Common.teamName();
		 addDeleteTeamPage.enterTeamName(team);//store valu
		addDeleteTeamPage.clickOnSimultaneously();
		addDeleteTeamPage.clickOnFirstUserTeam();
		addDeleteTeamPage.clickOnSubmitButton();
		Common.pause(5);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		addDeleteTeamPage.DeleteSpecificTeam(team);
		String actualResult3 = addDeleteTeamPage.validateDeleteTeamSuccessmessage();
		String expectedResult3 = "Team deleted successfully";
		assertEquals(actualResult3, expectedResult3);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void Verify_Validation_Message_When_Team_Assigned_In_IVR() throws Exception {
		driver4.get(url.teamPage());
		addDeleteTeamPage.clickOnCreateButton();
		String actualResult = addDeleteTeamPage.createTeamPageHeader();
		String team = "team"+Common.teamName();
		 addDeleteTeamPage.enterTeamName(team);//store valu
		addDeleteTeamPage.clickOnSimultaneously();
		addDeleteTeamPage.clickOnFirstUserTeam();
		addDeleteTeamPage.clickOnSubmitButton();
		Common.pause(5);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		addDeleteTeamPage.clickOnAddButtonIVR();	
		addDeleteTeamPage.enterValueInIvr("4");
		Common.pause(5);
		addDeleteTeamPage.clickOnDropdownInIVR("Team");
		Common.pause(5);
		addDeleteTeamPage.clickOnDropdownNumUserTeam(team);
		addDeleteTeamPage.SaveIvrOption();
	
		Thread.sleep(2000);
		driver4.get(url.teamPage());
		Common.pause(5);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		addDeleteTeamPage.clickOnDeleteTeamButton();
		Common.pause(3);
		addDeleteTeamPage.clickOnDeleteTeamYesButton();
		String actualResult1 = addDeleteTeamPage.validateWhenDeleteTeamWhichIsAssignedInIVRTeamNameV();
		String expectedResult = "Please delete the IVR(s) associated with this team before deleting this team.";
		assertEquals(actualResult1, expectedResult);
		Common.pause(5);
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		addDeleteTeamPage.ClickonDeltebuttonofNumUserTeam(team);
		Thread.sleep(2000);
		driver4.get(url.teamPage());
		Common.pause(5);
		addDeleteTeamPage.DeleteSpecificTeam(team);
		String actualResult3 = addDeleteTeamPage.validateDeleteTeamSuccessmessage();
		String expectedResult3 = "Team deleted successfully";
		assertEquals(actualResult3, expectedResult3);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Verify_Validation_Edit_Team() throws Exception {
		driver4.get(url.teamPage());
		addDeleteTeamPage.clickOnCreateButton();
		String actualResult = addDeleteTeamPage.createTeamPageHeader();
		String team = "team"+Common.teamName();
		 addDeleteTeamPage.enterTeamName(team);//store valu
		addDeleteTeamPage.clickOnSimultaneously();
		addDeleteTeamPage.clickOnFirstUserTeam();
		addDeleteTeamPage.clickOnSubmitButton();
		Common.pause(5);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		addDeleteTeamPage.clickOnEditButton();
		String actualResult1 = addDeleteTeamPage.editTeamHeader();
		String expectedResult = "Edit Team";
		assertEquals(actualResult1, expectedResult);
		Common.pause(3);
		driver4.get(url.teamPage());
		Common.pause(5);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		addDeleteTeamPage.DeleteSpecificTeam(team);
		String actualResult3 = addDeleteTeamPage.validateDeleteTeamSuccessmessage();
		String expectedResult3 = "Team deleted successfully";
		assertEquals(actualResult3, expectedResult3);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Verify_Validation_When_Team_Already_Exist_With_Same_Name() throws Exception {
		driver4.get(url.teamPage());
		addDeleteTeamPage.clickOnCreateButton();
		String actualResult = addDeleteTeamPage.createTeamPageHeader();
		String team = "team"+Common.teamName();
		 addDeleteTeamPage.enterTeamName(team);//store valu
		addDeleteTeamPage.clickOnSimultaneously();
		addDeleteTeamPage.clickOnFirstUserTeam();
		addDeleteTeamPage.clickOnSubmitButton();
		String actualResult1 = addDeleteTeamPage.validateAddTeamSuccessmessage();
		String expectedResult = "Team created successfully";
		assertEquals(actualResult1, expectedResult);
		Common.pause(4);
		addDeleteTeamPage.clickOnCreateButton();
		addDeleteTeamPage.enterTeamName(team);
		addDeleteTeamPage.clickOnSimultaneously();
		addDeleteTeamPage.clickOnFirstUserTeam();
		addDeleteTeamPage.clickOnSubmitButton();
		String actualResult11 = addDeleteTeamPage.validateDuplicateTeamNameValidationMessage();
		String expectedResult1 = "Team with this name is already exists, please use another name.";
		assertEquals(actualResult11, expectedResult1);
		driver4.get(url.teamPage());
		Common.pause(5);
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		addDeleteTeamPage.DeleteSpecificTeam(team);
		String actualResult3 = addDeleteTeamPage.validateDeleteTeamSuccessmessage();
		String expectedResult3 = "Team deleted successfully";
		assertEquals(actualResult3, expectedResult3);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
		addDeleteTeamPage.enterTeamNameInSearchBox(team);
		assertFalse(addDeleteTeamPage.validateSpecificTeamOnTeamPage(team));
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Verify_Description_In_Team_Page_For_All_Three_Team() throws Exception {
		driver4.get(url.teamPage());
		addDeleteTeamPage.clickOnCreateButton();
		// addDeleteTeamPage.enterTeamName("ADITYA");
		addDeleteTeamPage.clickOnSimultaneously();
		Common.pause(5);
		String actualResult1 = addDeleteTeamPage.simultaneousSideHeading();
		String expectedResult1 = "Simultaneous Order";
		assertEquals(actualResult1, expectedResult1);
		String actualResult2 = addDeleteTeamPage.simultaneousDescription();
		String expectedResult2 = "The call will go to all selected users in simultaneous order...";
		assertEquals(actualResult2, expectedResult2);

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Verify_Description_Fixed_Ordr_Team() throws Exception {
		driver4.get(url.teamPage());
		addDeleteTeamPage.clickOnCreateButton();
		addDeleteTeamPage.clickOnFixedOrder();
		String actualResult3 = addDeleteTeamPage.fixedorderSideHeading();
		String expectedResult3 = "Fixed Order";
		assertEquals(actualResult3, expectedResult3);
		String actualResult4 = addDeleteTeamPage.fixedOrderDescription();
		String expectedResult4 = "Call will go to same priority users at once. Ex: if user1, user2, user3 has priority 1, user4 and user5 has priority 2. user6 and user7 has priority 3. Call will ring to user1, user2 and user3 on same time, after that call will ring to user4 and user5 at same time and after that to user6 and user7 at same time.";
		assertEquals(actualResult4, expectedResult4);

	}
}