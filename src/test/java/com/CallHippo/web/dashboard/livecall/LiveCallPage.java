package com.CallHippo.web.dashboard.livecall;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.expectThrows;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class LiveCallPage {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait1;
	WebDriverWait wait2;
	Actions action;
	JavascriptExecutor js;

	public LiveCallPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait1 = new WebDriverWait(this.driver, 20);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		js = (JavascriptExecutor) driver;
	}

	@FindBy(xpath = "//div[contains(text(),'Live Calls')]")
	WebElement livecallTab;
	
	@FindBy(xpath = "//div[normalize-space()='Dashboard']")
	WebElement DashboardTab;
	
	@FindBy(xpath = "//img[contains(@alt,'NoLiveCall')]")
	WebElement noLiveCall;

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement errorMessage;
	
	@FindBy(xpath = "//table//tr/th[2]")
	WebElement headerAgent;
	
	@FindBy(xpath = "//table//tr/th[3]")
	WebElement headerFrom;
	
	@FindBy(xpath = "//table//tr/th[4]")
	WebElement headerTo;
	
	@FindBy(xpath = "//table//tr/th[5]")
	WebElement headerStartTime;
	
	@FindBy(xpath = "//table//tr/th[6]")
	WebElement headerStatusInfo;
	
	@FindBy(xpath = "//table//tr/th[7]")
	WebElement headerActionInfo;
	
	@FindBy(xpath = "//h3[contains(.,'After Call Work')]")
	WebElement acw_Txt;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//i[@Class='material-icons'][contains(.,'edit')]")
	WebElement acw_duration_edit_icon;
	
	@FindBy(xpath = "//button[@test-automation='user_after_call_work_switch']")
	WebElement userAfterCallwork;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//input[@title[contains(.,'After Call Work')]]")
	WebElement acw_duration_textBox;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//i[@Class='material-icons'][contains(.,'save')]")
	WebElement acw_duration_save_icon;
	
	public void clickOnLiveCallTab() {
		wait.until(ExpectedConditions.visibilityOf(livecallTab)).click();
	}
	
	public void clickOnDashboardTab() {
		wait.until(ExpectedConditions.visibilityOf(DashboardTab)).click();
	}

	public boolean validateThereIsNoLiveCall() {
		wait2.until(ExpectedConditions.visibilityOf(noLiveCall));
		return true;
	}
	
	public boolean validateThereIsNoLiveCallsinweb() {
		try {
		wait2.until(ExpectedConditions.visibilityOf(noLiveCall));
		return true;
		}catch (Exception e) {
			return false;
		}
	}

	public boolean validateCallType(String row, String callType) {

		if (callType.contains("Outgoing")) {
			wait2.until(ExpectedConditions.visibilityOf(
					driver.findElement(By.xpath("//tbody/tr[" + row + "]//img[contains(@alt,'outbound')]"))));
			return true;

		} else if (callType.contains("Incoming")) {
			wait2.until(ExpectedConditions.visibilityOf(
					driver.findElement(By.xpath("//tbody/tr[" + row + "]//img[contains(@class,'incomingIcon')]"))));
			return true;
		} else {
			return false;
		}

	}

	public String validateAgentName(String row) {
		return wait2
				.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[2]"))))
				.getText();
	}
	
	public boolean validateAgentinLivecalls(String row) {
		try { 
		wait2.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[2]"))))
				.getText();
		    return true;
		}catch (Exception e) {
			return false;
		}
	}

	public String validateFrom(String row) {
		return wait2
				.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[3]"))))
				.getText();
	}

	public String validateTo(String row) {
		return wait2
				.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[4]"))))
				.getText();
	}

	public String validateCallStatus(String row) {
		return wait2.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[6]/span"))))
				.getText();
	}

	public void waitForChangeCallStatusTo(String row, String callstatus) {
		wait2.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[6]/span[contains(.,'" + callstatus + "')]"))));
	}

	public void waitForChangeAgentNameTo(String row, String agentName) {
		wait2.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[2][contains(.,'" + agentName + "')]"))));

	}

	public void clickOnHangup(String row) {
		wait2.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Hangup Call')]"))))
				.click();
	}

	public void clickOnJoinCall(String row) {
		wait2.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Join Call')]"))))
				.click();
	}

	public void clickOnBargeCall(String row) {
		wait2.until(ExpectedConditions.elementToBeClickable(
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Barge Call')]"))))
				.click();
	}
	
	public boolean validateBargeCallBtnIsDisable(String row) {
		Common.pause(2);
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@class,'i_disabled')][contains(@title,'Barge Call')]"));
		if (ele.size() != 0) {
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}
	
	public void waitforBargeCallBtnIsEnable(String row) {
		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@class,'i_disabled')][contains(@title,'Barge Call')]")).size();
		int waitcount = 1;
		while(size>0) {
			waitcount = waitcount+1;
			Common.pause(1);
			if(waitcount == 35) {
				break;
			}
		}
	}
	
	public boolean validateBargeCallBtnIsEnable(String row) {
		boolean elementIsEnable;
		List<WebElement> ele = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Barge Call')]"));
		if (ele.size() != 0) {
			elementIsEnable = true;
		} else {
			elementIsEnable = false;
		}
		return elementIsEnable;
	}

	public boolean validateJoinCallBtnIsDisable(String row) {
		Common.pause(2);
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@class,'i_disabled')][contains(@title,'Join Call')]"));
		if (ele.size() != 0) {
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}
	
	public void waitforJoinCallBtnIsEnable(String row) {
		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@class,'i_disabled')][contains(@title,'Join Call')]")).size();
		int waitcount = 1;
		while(size>0) {
			waitcount = waitcount+1;
			Common.pause(1);
			if(waitcount == 35) {
				break;
			}
		}
	}
	
	public boolean validateJoinCallBtnIsEnable(String row) {
		boolean elementIsEnable;
		List<WebElement> ele = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Join Call')]"));
		if (ele.size() != 0) {
			elementIsEnable = true;
		} else {
			elementIsEnable = false;
		}
		return elementIsEnable;
	}
	
	public void clickOnCallWhisper(String row) {
		wait2.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Call Whisper')]"))))
				.click();
	}
	
	public boolean validateCallWhisperBtnIsDisable(String row) {
		Common.pause(2);
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@class,'i_disabled')][contains(@title,'Call Whisper')]"));
		if (ele.size() != 0) {
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}
	
	public void waitforCallWhisperBtnIsEnable(String row) {
		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@class,'i_disabled')][contains(@title,'Call Whisper')]")).size();
		int waitcount = 1;
		while(size>0) {
			waitcount = waitcount+1;
			Common.pause(1);
			if(waitcount == 35) {
				break;
			}
		}
	}
	
	public boolean validateCallWhisperBtnIsEnable(String row) {
		boolean elementIsEnable;
		List<WebElement> ele = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Call Whisper')]"));
		if (ele.size() != 0) {
			elementIsEnable = true;
		} else {
			elementIsEnable = false;
		}
		return elementIsEnable;
	}

	public boolean validateHangupButton(String row) {

		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]")).size();

		if (size > 0) {

			try {
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Hangup Call')]"));
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		} 

//		wait2.until(ExpectedConditions.visibilityOf(
//				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Hangup Call')]"))));
//		return true;
	}

	public boolean validateJoinCallButton(String row) {

		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]")).size();

		if (size > 0) {

			try {
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Join Call')]"));
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		}

//		wait2.until(ExpectedConditions.visibilityOf(
//				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Join Call')]"))));
//		return true;
	}

	public boolean validateBargeCallButton(String row) {
		
		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]")).size();

		if (size > 0) {

			try {
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Barge Call')]"));
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		}
		
//		wait2.until(ExpectedConditions.visibilityOf(
//				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Barge Call')]"))));
//		return true;
	}

	public boolean validateCallWhisper(String row) {
		int size = driver.findElements(By.xpath("//tbody/tr[" + row + "]/td[7]")).size();

		if (size > 0) {

			try {
				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Call Whisper')]"));
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		}
		
//		wait2.until(ExpectedConditions.visibilityOf(
//				driver.findElement(By.xpath("//tbody/tr[" + row + "]/td[7]//i[contains(@title,'Call Whisper')]"))));
//		return true;
	}
	
	public String errorMessage() {
		wait.until(ExpectedConditions.visibilityOf(errorMessage));
		Common.pause(2);
		return errorMessage.getText();
	}
	
	public void verifyLiveCallAgentStatus(String agentname, String status) {
		boolean elementIsAvailable;
		System.out.println("----agentname---"+agentname);
		System.out.println("----status---"+status);
		List<WebElement> agentstatusSize=null;
		if (status.equalsIgnoreCase("Available")) {
			agentstatusSize = wait1.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//div[@class[contains(.,'livecallstatuspad')]]/div/p[contains(.,'"+agentname+"')]/../span[@class='idealtimelive']"))));
		}
		if (status.equalsIgnoreCase("On Call")) {
			agentstatusSize = wait1.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//div[@class[contains(.,'livecallstatuspad')]]/div/p[contains(.,'"+agentname+"')]/../span[@class='idealtimeoncallstatus']"))));
		}
		if (agentstatusSize.size() > 0) {
			elementIsAvailable = true;
		}else {
			elementIsAvailable = false;
		}
		assertTrue(elementIsAvailable);
	}
	
	
	public boolean validateAgentHeaderIsDisplayed() {

		if(wait.until(ExpectedConditions.visibilityOf(headerAgent)).getText().contains("Agent")){
			return true;
		}else {
			return false;
		}
		
	}
	

	public boolean validateFromHeaderIsDisplayed() {

		if(wait.until(ExpectedConditions.visibilityOf(headerFrom)).getText().contains("From")){
			return true;
		}else {
			return false;
		}
		
	}

	public boolean validateToHeaderIsDisplayed() {

		if(wait.until(ExpectedConditions.visibilityOf(headerTo)).getText().contains("To")){
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean validateStartTimeHeaderIsDisplayed() {

		if(wait.until(ExpectedConditions.visibilityOf(headerStartTime)).getText().contains("Start Time")){
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean validateStatusHeaderIsDisplayed() {

		System.out.println(wait.until(ExpectedConditions.visibilityOf(headerStatusInfo)).getText());
		System.out.println(wait.until(ExpectedConditions.visibilityOf(headerActionInfo)).getText());
		if(wait.until(ExpectedConditions.visibilityOf(headerStatusInfo)).getText().contains("Status")){
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean validateActionHeaderIsDisplayed() {

//		action.moveToElement(driver.findElement(By.xpath("//table//tr/th[6]//i")));
//		Common.pause(3);
//		System.out.println(	driver.findElement(By.xpath("//div[contains(@class,'alwayesopencontainer awlopencnt')][1]")).getText());
		if(wait.until(ExpectedConditions.visibilityOf(headerActionInfo)).getText().contains("Action")){
			return true;
		}else {
			return false;
		}
	}
	
	//p[contains(.,'Total Inbound Calls')]/parent::div/parent::div/following-sibling::div/p
	
	
	public String getTotalInboundCall() {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Total Inbound Calls')]/parent::div/parent::div/following-sibling::div/p")))).getText();
	}
	
	public String getTotalOutboundCall() {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Total Outbound Calls')]/parent::div/parent::div/following-sibling::div/p")))).getText();
	}
	
	public String getTotalMissedCall() {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Total Missed Calls')]/parent::div/parent::div/following-sibling::div/p")))).getText();
	}
	
	public String getAverageCallDuration() {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Average Call')]/parent::div/parent::div/following-sibling::div/p")))).getText();
	}
	
	public String getLivCall() {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Total Live Calls')]/parent::div/parent::div/following-sibling::div/p")))).getText();
	}
	
	public String getAvailableAgent() {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Available')]/parent::div/parent::div/following-sibling::div/p")))).getText();
	}
	
	public boolean validateUserIsLoggedIn(String userName) {
		
		if(driver.findElements(By.xpath("//p[contains(@class,'mnfnttiterch')][text()='"+userName+"']")).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	public void scrollToACWTxt() {
		wait.until(ExpectedConditions.visibilityOf(acw_Txt));
		js.executeScript("arguments[0].scrollIntoView();", acw_Txt);
	}
	
	public void setAfterCallWorkDuration(String duration) {

		if (userAfterCallwork.getAttribute("aria-checked").equals("true")) {
			acw_duration_edit_icon.click();
			Common.pause(1);
			int durationText = acw_duration_textBox.getAttribute("value").length();
			for(int i = 0; i < durationText; i++){
				acw_duration_textBox.sendKeys(Keys.BACK_SPACE);
			}
			Common.pause(1);
			acw_duration_textBox.sendKeys(duration);
			acw_duration_save_icon.click();
		}
	}
}
