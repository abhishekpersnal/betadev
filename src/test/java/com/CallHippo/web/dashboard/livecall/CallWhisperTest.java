package com.CallHippo.web.dashboard.livecall;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;

public class CallWhisperTest {

	
	WebDriver driver;
	WebDriver driver1;
	WebDriver driver2;
	WebDriver driver3;
	WebDriver driver4;
	WebDriver driver5;
	WebDriver driver6;
	
	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerIndex phoneApp3;
	DialerIndex phoneApp4;
	DialerIndex phoneApp5;
	DialerIndex webApp2sDialer;
	DialerIndex webApp2sSubUserDialer;
	
	WebToggleConfiguration webApp2MainUser;
	WebToggleConfiguration webApp2SubUser;
	
	LiveCallPage liveCallPageWeb2MainUser;
	LiveCallPage liveCallPageWeb2SubUser;
	HolidayPage webApp2HolidayPage;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String account2EmailSubUser2 = data.getValue("account2Sub2Email");
	String account2PasswordSubUser2 = data.getValue("masterPassword");
	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	
	String acc1Number1 = data.getValue("account1Number1"); // account 1's number1
	String acc2Number1 = data.getValue("account2Number1"); // account 2's number1
	String acc2Number2 = data.getValue("account2Number2"); // account 2's number2
	String acc3Number1 = data.getValue("account3Number1"); // account 3's number1

	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String userID;
	String numberID;
	
	Common excelTestResult;
	
	public CallWhisperTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		webApp2MainUser = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		liveCallPageWeb2MainUser = PageFactory.initElements(driver4, LiveCallPage.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);
		webApp2sDialer = PageFactory.initElements(driver4, DialerIndex.class);
		
		driver3 = TestBase.init();
		webApp2SubUser = PageFactory.initElements(driver3, WebToggleConfiguration.class);
		liveCallPageWeb2SubUser = PageFactory.initElements(driver3, LiveCallPage.class);
		webApp2sSubUserDialer = PageFactory.initElements(driver3, DialerIndex.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);
		
		driver5 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driver5, DialerIndex.class);
		
		driver6 = TestBase.init_dialer();
		phoneApp5 = PageFactory.initElements(driver6, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2MainUser, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2MainUser, account2Email, account2Password);
			}

			try {
				driver3.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			} catch (Exception e) {
				driver3.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			}

			try {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp1, account1Email, account1Password);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp1, account1Email, account1Password);
			}

			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2, account2Email, account2Password);
			}
			try {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp3, account2EmailSubUser, account2PasswordSubUser);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp3, account2EmailSubUser, account2PasswordSubUser);
			}
			try {
				driver5.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp4, account3Email, account3Password);
			} catch (Exception e) {
				driver5.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp4, account3Email, account3Password);
			}
			try {
				driver6.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp5, account2EmailSubUser2, account2PasswordSubUser);
			} catch (Exception e) {
				driver6.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp5, account2EmailSubUser2, account2PasswordSubUser);
			}
			

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());
				
				phoneApp3.clickOnSideMenu();
				String callerName2subuser = phoneApp3.getCallerName();
				driver2.get(url.dialerSignIn());
				
				phoneApp5.clickOnSideMenu();
				String callerName2sub2 = phoneApp5.getCallerName();
				driver6.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2MainUser.numberspage();

				webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
				
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2MainUser.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				Thread.sleep(9000);
				
				webApp2MainUser.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2MainUser.selectMessageType("Welcome message","Text");
				webApp2MainUser.selectMessageType("Voicemail","Text");
				webApp2MainUser.clickonAWHMVoicemail();
				webApp2MainUser.selectMessageType("After Work Hours Message","Text");
				webApp2MainUser.clickonAWHMGreeting();
				webApp2MainUser.selectMessageType("After Work Hours Message","Text");
				webApp2MainUser.selectMessageType("Wait Music","Text");
				webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2MainUser.selectMessageType("IVR","Text");
				webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2MainUser.userTeamAllocation("users");
				webApp2MainUser.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2MainUser.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2MainUser.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2MainUser.selectSpecificUserOrTeam(callerName2subuser, "Yes");
				webApp2MainUser.selectSpecificUserOrTeam(callerName2sub2, "Yes");
				Common.pause(3);
				
				webApp2MainUser.userspage();
				webApp2MainUser.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2MainUser.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2MainUser.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2MainUser.makeDefaultNumber(acc2Number1);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2MainUser.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2MainUser.getUserId(Url));
				userID = webApp2MainUser.getUserId(Url);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				
				webApp2MainUser.userspage();
				webApp2MainUser.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2MainUser.makeDefaultNumber(acc2Number1);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2MainUser.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2MainUser.clickLeftMenuSetting();
				Common.pause(2);
				webApp2MainUser.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2MainUser.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2MainUser.scrollToPriceLimitTxt();
				webApp2MainUser.removePriceLimit();

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());
				
				phoneApp3.clickOnSideMenu();
				String callerName2subuser = phoneApp3.getCallerName();
				driver2.get(url.dialerSignIn());
				
				phoneApp5.clickOnSideMenu();
				String callerName2sub2 = phoneApp5.getCallerName();
				driver6.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2MainUser.numberspage();

				webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
				
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2MainUser.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				Thread.sleep(9000);
				
				webApp2MainUser.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2MainUser.selectMessageType("Welcome message","Text");
				webApp2MainUser.selectMessageType("Voicemail","Text");
				webApp2MainUser.clickonAWHMVoicemail();
				webApp2MainUser.selectMessageType("After Work Hours Message","Text");
				webApp2MainUser.clickonAWHMGreeting();
				webApp2MainUser.selectMessageType("After Work Hours Message","Text");
				webApp2MainUser.selectMessageType("Wait Music","Text");
				webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2MainUser.selectMessageType("IVR","Text");
				webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2MainUser.userTeamAllocation("users");
				webApp2MainUser.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2MainUser.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2MainUser.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2MainUser.selectSpecificUserOrTeam(callerName2subuser, "Yes");
				webApp2MainUser.selectSpecificUserOrTeam(callerName2sub2, "Yes");
				Common.pause(3);
				
				webApp2MainUser.userspage();
				webApp2MainUser.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2MainUser.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2MainUser.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2MainUser.makeDefaultNumber(acc2Number1);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2MainUser.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2MainUser.getUserId(Url));
				userID = webApp2MainUser.getUserId(Url);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				
				webApp2MainUser.userspage();
				webApp2MainUser.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2MainUser.makeDefaultNumber(acc2Number1);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2MainUser.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2MainUser.clickLeftMenuSetting();
				Common.pause(2);
				webApp2MainUser.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2MainUser.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2MainUser.scrollToPriceLimitTxt();
				webApp2MainUser.removePriceLimit();
			}
		} catch (Exception e1) {
			String testname = "Whisper calling Before Test.. ";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver5, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driver6, testname, "PhoneAppp5 Fail login");
			Common.Screenshot(driver4, testname, "webApp2MainUser Fail login");
			Common.Screenshot(driver3, testname, "webApp2SubUser Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
		
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driver3.get(url.signIn());
			driver4.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			driver5.navigate().to(url.dialerSignIn());
			driver6.navigate().to(url.dialerSignIn());
			Common.pause(3);
			if (webApp2MainUser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2MainUser, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account3Email, account3Password);
				Common.pause(3);
			}
			if (phoneApp5.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account2EmailSubUser2, account3Password);
				Common.pause(3);
			}
			
			driver4.get(url.usersPage());
			webApp2MainUser.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
			Thread.sleep(2000);
			webApp2MainUser.makeDefaultNumber(acc2Number1);
			driver4.get(url.usersPage());
			Common.pause(2);
			webApp2MainUser.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2MainUser.makeDefaultNumber(acc2Number1);
			driver4.get(url.usersPage());
			Common.pause(2);
			webApp2MainUser.navigateToUserSettingPage(account2EmailSubUser2);
			Common.pause(9);
			webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);
			webApp2MainUser.makeDefaultNumber(acc2Number1);
			
			phoneApp1.afterCallWorkToggle("off");
			driver.get(url.dialerSignIn());
			
		} catch (Exception e) {

			try {
				Common.pause(3);
				driver3.get(url.signIn());
				driver4.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				Common.pause(3);

				if (webApp2MainUser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2MainUser, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}
				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account3Email, account3Password);
					Common.pause(3);
				}
				if (phoneApp5.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp5, account2EmailSubUser2, account3Password);
					Common.pause(3);
				}
				
				driver4.get(url.usersPage());
				webApp2MainUser.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				Thread.sleep(2000);
				webApp2MainUser.makeDefaultNumber(acc2Number1);
				driver4.get(url.usersPage());
				Common.pause(2);
				webApp2MainUser.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2MainUser.makeDefaultNumber(acc2Number1);
				driver4.get(url.usersPage());
				Common.pause(2);
				webApp2MainUser.navigateToUserSettingPage(account2EmailSubUser2);
				Common.pause(9);
				webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				webApp2MainUser.makeDefaultNumber(acc2Number1);
				
				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
			} catch (Exception e1) {
				String testname = "Whisper calling Before Method ";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver5, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driver6, testname, "PhoneAppp5 Fail login");
				Common.Screenshot(driver4, testname, "webApp2MainUser Fail login");
				Common.Screenshot(driver3, testname, "webApp2SubUser1 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp5 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "webApp2MainUser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "webApp2SubUser1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp5 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "webApp2MainUser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "webApp2SubUser1 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driver3.quit();
		driver5.quit();
		driver6.quit();
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_callWhisperBtn_for_mainuser_in_liveCall_Section() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnHangup("1");
		Common.pause(3);

		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");

	}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void check_callWhisper_functionality() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		//click whisper call when incoming whisper call is ringing and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		//Again click whisper call when user already on whisper call and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		
		phoneApp3.clickOnOutgoingHangupButton();
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_callWhisper_Btn_for_subuser_in_liveCall_Section() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp2.enterNumberinDialer(acc1Number1);

		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2SubUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2SubUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp3.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.clickOnIncomimgAcceptCallButton();
		
		Common.pause(3);
		
		liveCallPageWeb2SubUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2SubUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.clickOnHangup("1");
		Common.pause(2);
		
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp3.waitForDialerPage();
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_whisperCall_while_mainuser_already_on_whisperCall() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		//click whisper call when incoming whisper call is ringing and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		//Again click whisper call when user already on whisper call and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		
		phoneApp3.clickOnOutgoingHangupButton();
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_while_mainuser_already_on_bargeCall() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnBargeCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforBargeCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_while_mainuser_already_on_joinCall() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnJoinCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforJoinCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_whisperCall_while_incoming_whisperCall_ringing() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		//click call whisper when incoming barge call is ringing and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_while_barge_incoming_call_ringing() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnBargeCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforBargeCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsEnable("1"));
		
		//click whisper call when incoming barge call is ringing and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_while_join_incoming_call_ringing() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnJoinCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforJoinCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsEnable("1"));
		
		//click whisper call when incoming join call is ringing and check validation.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_after_incoming_whispercall_rejected() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_after_incoming_bargecall_rejected() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnBargeCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforBargeCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_after_incoming_joincall_rejected() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnJoinCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforJoinCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_after_incoming_whispercall_missed() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_after_incoming_bargecall_missed() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnBargeCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforBargeCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsEnable("1"));
		
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void click_callwhishper_after_incoming_joincall_missed() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver2.get(url.dialerSignIn());

//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnJoinCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforJoinCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsEnable("1"));
		
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void callwhishper_while_mainuser_and_subuser_already_on_call() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp2.enterNumberinDialer(acc1Number1);

		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		phoneApp3.enterNumberinDialer(acc3Number1);
		phoneApp3.clickOnDialButton();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		Common.pause(2);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2SubUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		//Click whisper call when subuser already on call and check validation.
		liveCallPageWeb2SubUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2SubUser.errorMessage(),"You are already on call.");
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_callwhishper_functionality_while_ACW_on_in_both_users() throws Exception {

		Common.pause(3);
		
		driver4.get(url.usersPage());
		webApp2MainUser.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2MainUser.setUserSttingToggles("off", "on", "off", "open", "off");
		Common.pause(2);
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(3);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		driver3.get(url.usersPage());
		webApp2SubUser.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2SubUser.setUserSttingToggles("off", "on", "off", "open", "off");
		liveCallPageWeb2SubUser.scrollToACWTxt();
		liveCallPageWeb2SubUser.setAfterCallWorkDuration("10");
		Common.pause(2);
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
				
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnLogout();
		loginDialer(phoneApp3, account2EmailSubUser, account2PasswordSubUser);
		phoneApp3.waitForDialerPage();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(3);
		phoneApp3.clickOnEndACWButton();
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		driver4.get(url.usersPage());
		webApp2MainUser.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);
		
		driver3.get(url.usersPage());
		webApp2SubUser.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2SubUser.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);
	}
	
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void callwhishper_while_userA_ACW_is_running() throws Exception {

		Common.pause(3);
		
		driver4.get(url.usersPage());
		webApp2MainUser.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2MainUser.setUserSttingToggles("off", "on", "off", "open", "off");
		liveCallPageWeb2MainUser.scrollToACWTxt();
		liveCallPageWeb2MainUser.setAfterCallWorkDuration("10");
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		loginDialer(phoneApp2, account2Email, account2Password);
		phoneApp2.waitForDialerPage();
		
		Common.pause(2);
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(3);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(3);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		driver3.get(url.usersPage());
		webApp2SubUser.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		webApp2SubUser.setUserSttingToggles("off", "on", "off", "open", "off");
		liveCallPageWeb2SubUser.scrollToACWTxt();
		liveCallPageWeb2SubUser.setAfterCallWorkDuration("10");
		
		phoneApp3.clickOnSideMenu();
		phoneApp3.clickOnLogout();
		loginDialer(phoneApp3, account2EmailSubUser, account2PasswordSubUser);
		phoneApp3.waitForDialerPage();
		
		Common.pause(2);
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(3);
		System.out.println("click on live call tab..");
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.waitForDialerPage();
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp2.enterNumberinDialer(acc1Number1);
		phoneApp2.clickOnDialButton();

		phoneApp1.waitForIncomingCallingScreen();

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(2);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Common.pause(2);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		Common.pause(2);
		
		phoneApp2.clickOnOutgoingHangupButton();
		Common.pause(2);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);
		phoneApp3.clickOnDialButton();
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2SubUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"Your Acw screen is running");
		
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(2);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "After Call Work");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "After Call Work");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), false, "validate hangup button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp2.clickOnEndACWButton();
		Common.pause(2);
		
		phoneApp3.clickOnEndACWButton();
		Common.pause(2);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		driver4.get(url.usersPage());
		webApp2MainUser.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		liveCallPageWeb2MainUser.scrollToACWTxt();
		liveCallPageWeb2MainUser.setAfterCallWorkDuration("5");
		webApp2MainUser.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);
		
		driver3.get(url.usersPage());
		webApp2SubUser.navigateToUserSettingPage(account2EmailSubUser);
		Thread.sleep(9000);
		liveCallPageWeb2SubUser.scrollToACWTxt();
		liveCallPageWeb2SubUser.setAfterCallWorkDuration("5");
		webApp2SubUser.setUserSttingToggles("off", "off", "off", "open", "off");
		Common.pause(2);
	}
	
	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void check_callWhisperBtn_not_display_while_mainuser_barge_their_own_call() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp2.enterNumberinDialer(acc1Number1);

		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void whisper_two_calls_at_same_time() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUser1CallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp4.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		
		phoneApp5.enterNumberinDialer(acc3Number1);
		phoneApp5.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName5, phoneApp5.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc3Number1, phoneApp5.validateCallingScreenOutgoingNumber());

		phoneApp4.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName4, phoneApp4.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp4.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp4.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen4 = phoneApp4.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);

		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(3);
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("2"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp5.clickOnOutgoingHangupButton();
		
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		phoneApp5.waitForDialerPage();

	}
	
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void whisper_two_conversations_one_by_one() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUser1CallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp4.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		
		phoneApp5.enterNumberinDialer(acc3Number1);
		phoneApp5.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName5, phoneApp5.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc3Number1, phoneApp5.validateCallingScreenOutgoingNumber());

		phoneApp4.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName4, phoneApp4.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp4.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp4.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen4 = phoneApp4.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);

		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(3);
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("2"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp5.clickOnOutgoingHangupButton();
		
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		phoneApp5.waitForDialerPage();
	}
	
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void simultaneously_barge_and_whisper_two_conversation() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUser1CallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp4.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		
		phoneApp5.enterNumberinDialer(acc3Number1);
		phoneApp5.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName5, phoneApp5.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc3Number1, phoneApp5.validateCallingScreenOutgoingNumber());

		phoneApp4.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName4, phoneApp4.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp4.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp4.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen4 = phoneApp4.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.clickOnBargeCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);

		liveCallPageWeb2MainUser.waitforBargeCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateBargeCallBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(3);
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("2"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp5.clickOnOutgoingHangupButton();
		
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		phoneApp5.waitForDialerPage();

	}
	
	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void simultaneously_join_and_whisper_two_conversation() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUser1CallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		String departmentName4 = phoneApp4.validateDepartmentNameinDialpade();
		String departmentName5 = phoneApp5.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);
		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "Available");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		
		phoneApp5.enterNumberinDialer(acc3Number1);
		phoneApp5.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName5, phoneApp5.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc3Number1, phoneApp5.validateCallingScreenOutgoingNumber());

		phoneApp4.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName4, phoneApp4.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp4.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp4.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen4 = phoneApp4.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUser1CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), subUser2CallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc3Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.clickOnJoinCall("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);

		liveCallPageWeb2MainUser.waitforJoinCallBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateJoinCallBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(3);
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("2"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("2");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser1CallerName, "On Call");
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(subUser2CallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertEquals(liveCallPageWeb2MainUser.errorMessage(),"You are already on call.");
		Common.pause(3);
		
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp5.clickOnOutgoingHangupButton();
		
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		phoneApp5.waitForDialerPage();
	}
	
	@Test(priority = 26, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_incoming_call_goto_InQueue_while_both_users_on_call() throws Exception {

		Common.pause(3);
		
		driver4.get(url.numbersPage());
		webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
		Thread.sleep(9000);
		webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Common.pause(2);
		webApp2MainUser.enterCallQueueDuration("300");
		Common.pause(2);
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(3);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		Common.pause(2);
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnLogout();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp4.enterNumberinDialer(acc2Number1);
		phoneApp4.clickOnDialButton();
		
		Common.pause(30);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Incoming");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), "");
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc3Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc2Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "In Queue");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("2", "Incoming");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), "");
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc3Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc2Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "In Queue");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		phoneApp2.clickOnIncomingHangupButton();;
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(3);
		
		phoneApp4.clickOnOutgoingHangupButton();
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		driver4.get(url.numbersPage());
		webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
		Thread.sleep(9000);
		webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
	}
	
	@Test(priority = 27, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void userA_serve_queue_call_after_hangup_whisperCall_callQueue_ON() throws Exception {

		Common.pause(3);
		
		driver4.get(url.numbersPage());
		webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
		Thread.sleep(9000);
		webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Common.pause(2);
		webApp2MainUser.enterCallQueueDuration("300");
		Common.pause(2);
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(3);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		Common.pause(2);
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnLogout();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		phoneApp4.enterNumberinDialer(acc2Number1);
		phoneApp4.clickOnDialButton();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Incoming");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), "");
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc3Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc2Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "In Queue");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("2", "Incoming");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), "");
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc3Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc2Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "In Queue");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "In Queue");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		phoneApp2.clickOnIncomingHangupButton();;
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		liveCallPageWeb2MainUser.validateCallType("2", "Incoming");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("2"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("2"), acc3Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("2"), acc2Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("2"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("2"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("2"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2SubUser.validateCallType("2", "Incoming");
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("2"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("2"), acc3Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("2"), acc2Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("2", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("2"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("2"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("2"),  true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("2"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("2"), true, "validate call whishper button is display.");
		
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(3);
		
		phoneApp4.clickOnOutgoingHangupButton();
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		driver4.get(url.numbersPage());
		webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
		Thread.sleep(9000);
		webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
	}
	
	@Test(priority = 28, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void userA_whisperCall_callQueue_OFF() throws Exception {

		Common.pause(3);
		
		driver4.get(url.numbersPage());
		webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
		Thread.sleep(9000);
		webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(3);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		Common.pause(2);
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
//		driver6.get(url.dialerSignIn());
		phoneApp5.clickOnSideMenu();
		Common.pause(2);
		String subUser2CallerName = phoneApp5.getCallerName();
		Common.pause(2);
		phoneApp5.clickOnLogout();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsDisable("1"));
		
		phoneApp2.waitForIncomingCallingScreen();
		
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		
		liveCallPageWeb2MainUser.waitforCallWhisperBtnIsEnable("1");
		assertTrue(liveCallPageWeb2MainUser.validateCallWhisperBtnIsEnable("1"));
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		phoneApp4.enterNumberinDialer(acc2Number1);
		phoneApp4.clickOnDialButton();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		phoneApp2.clickOnIncomingHangupButton();;
		
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(3);
		
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp4.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		driver4.get(url.numbersPage());
		webApp2MainUser.navigateToNumberSettingpage(acc2Number1);
		Thread.sleep(9000);
		webApp2MainUser.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
	}
	
	@Test(priority = 29, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void callWhishper_while_mainuser_dialer_loggedOut() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnLogout();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnSideMenuDialPad();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
		
		phoneApp3.enterNumberinDialer(acc1Number1);

		phoneApp3.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName3, phoneApp3.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp3.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "On Call");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), subUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), false, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), false, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), false, "validate call whishper button is display.");
		
		//click call whisper while mainuser dialer loggedOut.
		liveCallPageWeb2MainUser.clickOnCallWhisper("1");
		Thread.sleep(10000);
		webApp2MainUser.switchToWindowBasedOnTitle("CallHippo Dialer");
		webApp2sDialer.waitForIncomingCallingScreen();
		webApp2sDialer.clickOnIncomingRejectButton();
		driver4.close();
		webApp2MainUser.switchToWindowBasedOnTitle("Dashboard | Callhippo.com");
		Common.pause(3);
		
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2SubUser.verifyLiveCallAgentStatus(subUserCallerName, "Available");
	}
	
	@Test(priority = 30, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void callWhisper_while_subuser_dialer_loggedOut() throws Exception {

		Common.pause(3);
		
		webApp2MainUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2MainUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2MainUser.validateThereIsNoLiveCall();
		
		webApp2SubUser.clickLeftMenuDashboard();
		Common.pause(2);
		liveCallPageWeb2SubUser.clickOnLiveCallTab();
		Common.pause(2);
		liveCallPageWeb2SubUser.validateThereIsNoLiveCall();
		
//		driver1.get(url.dialerSignIn());
		phoneApp2.clickOnSideMenu();
		Common.pause(2);
		String mainUserCallerName = phoneApp2.getCallerName();
		Common.pause(2);
		phoneApp2.clickOnSideMenuDialPad();
		
//		driver2.get(url.dialerSignIn());
		phoneApp3.clickOnSideMenu();
		Common.pause(2);
		String subUserCallerName = phoneApp3.getCallerName();
		Common.pause(2);
		phoneApp3.clickOnLogout();
		
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		
		phoneApp2.enterNumberinDialer(acc1Number1);

		phoneApp2.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName2, phoneApp2.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(acc1Number1, phoneApp2.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName1, phoneApp1.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(phoneApp1.validateCallingScreenIncomingNumber(), acc2Number1);

		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		Common.pause(5);
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "Ringing");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		
		phoneApp1.clickOnIncomimgAcceptCallButton();
		
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		
		Thread.sleep(3000);
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "On Call");
		
		liveCallPageWeb2MainUser.validateCallType("1", "Outgoing");
		assertEquals(liveCallPageWeb2MainUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2MainUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2MainUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2MainUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2MainUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2MainUser.validateJoinCallButton("1"), false, "validate Join call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateBargeCallButton("1"), false, "validate Barge call button is not display.");
		assertEquals(liveCallPageWeb2MainUser.validateCallWhisper("1"), false, "validate call whishper button is not display.");
		
		assertEquals(liveCallPageWeb2SubUser.validateAgentName("1"), mainUserCallerName);
		assertEquals(liveCallPageWeb2SubUser.validateFrom("1"), acc2Number1);
		assertEquals(liveCallPageWeb2SubUser.validateTo("1"), acc1Number1);
		liveCallPageWeb2SubUser.waitForChangeCallStatusTo("1", "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateCallStatus("1"), "On Call");
		assertEquals(liveCallPageWeb2SubUser.validateHangupButton("1"), true, "validate hangup button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateJoinCallButton("1"), true, "validate Join call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateBargeCallButton("1"), true, "validate Barge call button is display.");
		assertEquals(liveCallPageWeb2SubUser.validateCallWhisper("1"), true, "validate call whishper button is display.");
		
		//click call whisper while subuser not loggedIn.
		liveCallPageWeb2SubUser.clickOnCallWhisper("1");
		Thread.sleep(10000);
		webApp2SubUser.switchToWindowBasedOnTitle("CallHippo Dialer");
		Thread.sleep(2000);
		webApp2sSubUserDialer.waitForIncomingCallingScreen();
		webApp2sSubUserDialer.clickOnIncomingRejectButton();
		driver3.close();
		webApp2SubUser.switchToWindowBasedOnTitle("Dashboard | Callhippo.com");
		Common.pause(3);
		
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		
		liveCallPageWeb2MainUser.verifyLiveCallAgentStatus(mainUserCallerName, "Available");
		
	}
}
