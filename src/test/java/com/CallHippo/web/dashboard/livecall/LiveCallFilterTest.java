package com.CallHippo.web.dashboard.livecall;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

import java.io.IOException;

import org.apache.commons.exec.LogOutputStream;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;

public class LiveCallFilterTest {

	
	WebDriver driverphoneApp1;
	WebDriver driverphoneApp2;
	WebDriver driverphoneApp2sub1;
	WebDriver driverphoneApp2sub2;
	WebDriver driverphoneApp3;
	WebDriver driverphoneApp4;
	WebDriver driverphoneApp5;
	WebDriver driverWebApp2;
	WebDriver driverWebApp2Subuser1;
	
	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerIndex phoneApp2Sub1;
	DialerIndex phoneApp2Sub2;
	DialerIndex phoneApp3;
	DialerIndex phoneApp4;
	DialerIndex phoneApp5;
	
	WebToggleConfiguration webApp2;
	WebToggleConfiguration webApp2SubUser;
	HolidayPage holidaywebApp;
	
	
	LiveCallPage Web2liveCallPage;
	LiveCallPage Web2SubuserliveCallPage;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String account2EmailSubUser2 = data.getValue("account2Sub2Email");
	String account2PasswordSubUser2 = data.getValue("masterPassword");
	String account3Email = data.getValue("account3MainEmail");
	String account5Email = data.getValue("account5MainEmail");
	String account4Email1 = data.getValue("account4MainEmail");
	String account3Password = data.getValue("masterPassword");
	
	String acc1Number1 = data.getValue("account1Number1"); // account 1's number1
	String acc2Number1 = data.getValue("account2Number1"); // account 2's number1
	String acc2Number2 = data.getValue("account2Number2"); // account 2's number2
	String acc3Number1 = data.getValue("account3Number1"); // account 3's number1
	String acc4Number1 = data.getValue("account4Number1"); // account 3's number1
	String acc5Number1 = data.getValue("account5Number1"); // account 3's number1

	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String userID;
	String numberID;
	
	Common excelTestResult;
	
	public LiveCallFilterTest() throws Exception {
		
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization () throws Exception {

		driverWebApp2 = TestBase.init();
		webApp2 = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		Web2liveCallPage = PageFactory.initElements(driverWebApp2, LiveCallPage.class);
		holidaywebApp = PageFactory.initElements(driverWebApp2, HolidayPage.class);

		driverWebApp2Subuser1 = TestBase.init();
		webApp2SubUser = PageFactory.initElements(driverWebApp2Subuser1, WebToggleConfiguration.class);
		Web2SubuserliveCallPage = PageFactory.initElements(driverWebApp2Subuser1, LiveCallPage.class);

		driverphoneApp1 = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driverphoneApp1, DialerIndex.class);

		driverphoneApp2 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driverphoneApp2, DialerIndex.class);

		driverphoneApp2sub1 = TestBase.init_dialer();
		phoneApp2Sub1 = PageFactory.initElements(driverphoneApp2sub1, DialerIndex.class);
		
		driverphoneApp2sub2 = TestBase.init_dialer();
		phoneApp2Sub2 = PageFactory.initElements(driverphoneApp2sub2, DialerIndex.class);
		
		driverphoneApp3 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driverphoneApp3, DialerIndex.class);
		
		driverphoneApp4 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driverphoneApp4, DialerIndex.class);
		
		driverphoneApp5 = TestBase.init_dialer();
		phoneApp5 = PageFactory.initElements(driverphoneApp5, DialerIndex.class);

		try {
			try {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverWebApp2.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driverWebApp2Subuser1.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			} catch (Exception e) {
				driverWebApp2Subuser1.get(url.signIn());
				Common.pause(4);
				loginWeb(webApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			}

			try {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp1, account1Email, account1Password);
			} catch (Exception e) {
				driverphoneApp1.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp1, account1Email, account1Password);
			}

			try {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2, account2Email, account2Password);
			} catch (Exception e) {
				driverphoneApp2.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2, account2Email, account2Password);
			}
			try {
				driverphoneApp2sub1.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2Sub1, account2EmailSubUser, account2PasswordSubUser);
			} catch (Exception e) {
				driverphoneApp2sub1.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2Sub1, account2EmailSubUser, account2PasswordSubUser);
			}
			try {
				driverphoneApp2sub2.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2Sub2, account2EmailSubUser2, account2PasswordSubUser);
			} catch (Exception e) {
				driverphoneApp2sub2.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp2Sub2, account2EmailSubUser2, account2PasswordSubUser);
			}
			try {
				driverphoneApp3.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp3, account3Email, account3Password);
			} catch (Exception e) {
				driverphoneApp3.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp3, account3Email, account3Password);
			}
			try {
				driverphoneApp4.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp4, account4Email1, account2PasswordSubUser);
			} catch (Exception e) {
				driverphoneApp4.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp4, account4Email1, account2PasswordSubUser);
			}
			
			try {
				driverphoneApp5.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp5, account5Email, account2PasswordSubUser);
			} catch (Exception e) {
				driverphoneApp5.get(url.dialerSignIn());
				Common.pause(4);
				loginDialer(phoneApp5, account5Email, account2PasswordSubUser);
			}
			

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());
				
				phoneApp2Sub1.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Sub1.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());
				
				phoneApp2Sub2.clickOnSideMenu();
				String callerName2Sub2 = phoneApp2Sub2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(acc2Number1);
				
				String Url1 = driverWebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Sub2, "Yes");
				Common.pause(3);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(acc2Number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(acc2Number1);

				String Url = driverWebApp2.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(acc2Number1);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverphoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverphoneApp2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				holidaywebApp.clickOnHolidaySubMenu();
				holidaywebApp.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());
				
				phoneApp2Sub1.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Sub1.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());
				
				phoneApp2Sub2.clickOnSideMenu();
				String callerName2Sub2 = phoneApp2Sub2.getCallerName();
				driverphoneApp2.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(acc2Number1);
				
				String Url1 = driverWebApp2.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Sub2, "Yes");
				Common.pause(3);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(acc2Number2);
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(acc2Number1);

				String Url = driverWebApp2.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(acc2Number1);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverphoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverphoneApp2.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driverphoneApp1.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverphoneApp2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				holidaywebApp.clickOnHolidaySubMenu();
				holidaywebApp.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}
		} catch (Exception e1) {
			String testname = "Live call filter Before Test.. ";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driverphoneApp2sub1, testname, "PhoneAppp2subu1 Fail login");
			Common.Screenshot(driverphoneApp2sub1, testname, "PhoneAppp2sub2 Fail login");
			Common.Screenshot(driverphoneApp3, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driverphoneApp4, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driverphoneApp5, testname, "PhoneAppp5 Fail login");
			Common.Screenshot(driverWebApp2, testname, "webApp2 Fail login");
			Common.Screenshot(driverWebApp2Subuser1, testname, "webApp2SubUser Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
		
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driverWebApp2.get(url.signIn());
			driverWebApp2Subuser1.get(url.signIn());
			driverphoneApp1.navigate().to(url.dialerSignIn());
			driverphoneApp2.navigate().to(url.dialerSignIn());
			driverphoneApp2sub1.navigate().to(url.dialerSignIn());
			driverphoneApp2sub2.navigate().to(url.dialerSignIn());
			driverphoneApp3.navigate().to(url.dialerSignIn());
			driverphoneApp4.navigate().to(url.dialerSignIn());
			driverphoneApp5.navigate().to(url.dialerSignIn());
			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2SubUser, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Sub1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Sub1, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Sub2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Sub2, account2EmailSubUser2, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account2PasswordSubUser);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email1, account3Password);
				Common.pause(3);
			}
			if (phoneApp5.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account5Email, account3Password);
				Common.pause(3);
		}
		} catch (Exception e) {

			try {
				Common.pause(2);
			driverWebApp2.get(url.signIn());
			driverWebApp2Subuser1.get(url.signIn());
			driverphoneApp1.navigate().to(url.dialerSignIn());
			driverphoneApp2.navigate().to(url.dialerSignIn());
			driverphoneApp2sub1.navigate().to(url.dialerSignIn());
			driverphoneApp2sub2.navigate().to(url.dialerSignIn());
			driverphoneApp3.navigate().to(url.dialerSignIn());
			driverphoneApp4.navigate().to(url.dialerSignIn());
			driverphoneApp5.navigate().to(url.dialerSignIn());
			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2SubUser, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Sub1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Sub1, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Sub2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Sub2, account2EmailSubUser2, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account2PasswordSubUser);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account4Email1, account3Password);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp5, account5Email, account3Password);
				Common.pause(3);
			}
			} catch (Exception e1) {
				String testname = "Live Call filter Before Method ";
				Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driverphoneApp2sub1, testname, "PhoneAppp2subu1 Fail login");
				Common.Screenshot(driverphoneApp2sub1, testname, "PhoneAppp2sub2 Fail login");
				Common.Screenshot(driverphoneApp3, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driverphoneApp4, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driverphoneApp5, testname, "PhoneAppp5 Fail login");
				Common.Screenshot(driverWebApp2, testname, "webApp2 Fail login");
				Common.Screenshot(driverWebApp2Subuser1, testname, "webApp2SubUser Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2sub1, testname, "PhoneAppp2sub1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp3, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp5, testname, "PhoneAppp5 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2sub2, testname, "PhoneAppp2sub2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2Subuser1, testname, "WebAppp2Sub1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driverphoneApp1, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2sub1, testname, "PhoneAppp2sub1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp3, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp4, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp5, testname, "PhoneAppp5 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverphoneApp2sub2, testname, "PhoneAppp2sub2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "WebAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2Subuser1, testname, "WebAppp2Sub1 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driverWebApp2.quit();
		driverWebApp2Subuser1.quit();
		driverphoneApp1.quit();
		driverphoneApp2.quit();
		driverphoneApp2sub1.quit();
		driverphoneApp2sub2.quit();
		driverphoneApp3.quit();
		driverphoneApp4.quit();
		driverphoneApp5.quit();
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	public void loginWeb(WebToggleConfiguration web) {

		web.clickOnLogOut();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_liveCall_Filter_in_Web() throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(webApp2.ValidateFilterpopupWeblivecalls(), true);
		assertEquals(webApp2.ValidateFilterSearchbarinWeblivecalls(), true);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		Common.pause(2);
		Web2liveCallPage.clickOnDashboardTab();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), false);
		
}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void apply_filter_against_User_subuser_in_Live_call_in_Mainuser_Web () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp2Sub1.enterNumberinDialer(acc1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2Sub1.clickOnDialButton();
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(Web2liveCallPage.validateAgentName("1"),userOrteam);
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp2Sub1.clickOnOutgoingHangupButton();
		phoneApp2Sub1.waitForDialerPage();
		
		
}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void apply_filter_against_Team_subuser_Call_Going_on_in_Live_call_in_Mainuser_Web () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp2Sub1.enterNumberinDialer(acc1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2Sub1.clickOnDialButton();
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2Sub1);
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp2Sub1.clickOnOutgoingHangupButton();
		phoneApp2Sub1.waitForDialerPage();
		
		
}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Apply_filter_by_user_when_no_any_live_calls_present_against_userandteam  () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		Common.pause(2);
		String user1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(user1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		Common.pause(2);
		String Team = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(Team);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		webApp2.ClearWeblivecallsearchfilter();
		
		
}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Apply_filter_by_Select_multiple_user_and_apply_filter   () throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		
		phoneApp2.enterNumberinDialer(acc1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2Sub2.enterNumberinDialer(acc4Number1);
		phoneApp2Sub2.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2Sub1.enterNumberinDialer(acc3Number1);
		phoneApp2Sub1.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2);
		assertEquals(Web2liveCallPage.validateAgentName("2"),callerName2Sub2);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("3"), false);
		webApp2.ClearWeblivecallsearchfilter();
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(Web2liveCallPage.validateAgentName("2"),callerName2Sub1);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2Sub2);
		assertFalse(Web2liveCallPage.validateAgentinLivecalls("3"));
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp4.clickOnIncomingHangupButton();
		
		
}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Apply_filter_by_Select_multiple_Team_and_apply_filter   () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		
		phoneApp2.enterNumberinDialer(acc1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2Sub2.enterNumberinDialer(acc4Number1);
		phoneApp2Sub2.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2Sub1.enterNumberinDialer(acc3Number1);
		phoneApp2Sub1.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		webApp2.selectUserOrTeamInWeblivecallFilter("subuser team");
		webApp2.selectUserOrTeamInWeblivecallFilter("main user team");
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2);
		assertEquals(Web2liveCallPage.validateAgentName("2"),callerName2Sub1);
		assertFalse(Web2liveCallPage.validateAgentinLivecalls("3"));
		webApp2.ClearWeblivecallsearchfilter();
		webApp2.selectUserOrTeamInWeblivecallFilter("subuser team");
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2Sub1);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp4.clickOnIncomingHangupButton();
		
		
		
		
}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Apply_filter_by_Select_multiple_Team_And_User_and_apply_filter   () throws Exception {
		

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		
		phoneApp2.enterNumberinDialer(acc1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2Sub2.enterNumberinDialer(acc4Number1);
		phoneApp2Sub2.clickOnDialButton();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2Sub1.enterNumberinDialer(acc3Number1);
		phoneApp2Sub1.clickOnDialButton();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		webApp2.selectUserOrTeamInWeblivecallFilter("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2);
		assertEquals(Web2liveCallPage.validateAgentName("3"),callerName2Sub1);
		assertEquals(Web2liveCallPage.validateAgentName("2"),callerName2Sub2);
		webApp2.ClearWeblivecallsearchfilter();
		webApp2.selectUserOrTeamInWeblivecallFilter("subuser team");
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(Web2liveCallPage.validateAgentName("2"),callerName2Sub1);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2Sub2);
		phoneApp1.clickOnIncomingHangupButton();
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp4.clickOnIncomingHangupButton();
		Common.pause(5);
		
		
}
	
//	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
//	public void Apply_scrolling_and_check_when_multiple_live_calls_are_on_going    () throws Exception {
//		
//
//		phoneApp2.clickOnSideMenu();
//		String callerName2 = phoneApp2.getCallerName();
//		driverphoneApp2.get(url.dialerSignIn());
//		
//		phoneApp2Sub1.clickOnSideMenu();
//		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
//		driverphoneApp2sub1.get(url.dialerSignIn());
//		
//		phoneApp2Sub2.clickOnSideMenu();
//		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
//		driverphoneApp2sub2.get(url.dialerSignIn()); 
//		
//		driverWebApp2.get(url.usersPage());
//		webApp2.navigateToNumberSettingpage(acc2Number1);
//		Common.pause(9);
//		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
//		
//		driverWebApp2.get(url.usersPage());
//		webApp2.navigateToUserSettingPage(account2Email);
//		Thread.sleep(9000);
//		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
//		webApp2.userTeamAllocation("users");
//		Common.pause(5);
//		webApp2.enterCallQueueDuration("300");
//
//		driverWebApp2.get(url.usersPage());
//		webApp2.navigateToUserSettingPage(account2EmailSubUser);
//		Common.pause(5);
//		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
//		
//		phoneApp2.enterNumberinDialer(account2MainUserExtension);
//		phoneApp2Sub2.enterNumberinDialer(acc1Number1);
//		phoneApp3.enterNumberinDialer(acc2Number1);
//		phoneApp4.enterNumberinDialer(acc2Number1);
//		phoneApp4.enterNumberinDialer(acc2Number1);
//		phoneApp5.enterNumberinDialer(acc2Number1);
//		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
//		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
//		phoneApp2.clickOnDialButton();
//		phoneApp2Sub1.waitForIncomingCallingScreen();
//		phoneApp2Sub1.clickOnIncomimgAcceptCallButton();
//		phoneApp2Sub2.clickOnDialButton();
//		phoneApp1.waitForIncomingCallingScreen();
//		phoneApp1.clickOnIncomimgAcceptCallButton();
//		phoneApp3.clickOnDialButton();
//		phoneApp3.validateDurationInCallingScreen(2);
//		phoneApp4.clickOnDialButton();
//		phoneApp4.validateDurationInCallingScreen(2);
//		phoneApp5.clickOnDialButton();
//		phoneApp5.validateDurationInCallingScreen(2);
//		webApp2.clickLeftMenuDashboard();
//		Common.pause(2);
//		Web2liveCallPage.clickOnLiveCallTab();
//		Common.pause(2);
//}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Verify_search_functionality_in_live_alls_page () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(webApp2.ValidateFilterpopupWeblivecalls(), true);
		assertEquals(webApp2.ValidateFilterSearchbarinWeblivecalls(), true);
		String user0 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(user0), true);
		String user1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(user1), true);
		String user2 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub2);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(user2), true);
		String Team1 = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(Team1), true);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		Common.pause(2);
		Web2liveCallPage.clickOnDashboardTab();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), false);
		
}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_search_functionality_when_few_users_have_been_selected () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(5);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		phoneApp2.clickOnDialButton();
		phoneApp2Sub1.clickOnIncomimgAcceptCallButton();
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub1);
//		assertEquals(Web2liveCallPage.validateAgentName(""),callerName2);
//		assertEquals(Web2liveCallPage.validateAgentName("2"),callerName2Sub1);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("1"),true);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("2"),true);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("3"),false);
		
//		String userOrteam2 = webApp2.searchUserOrTeaminWebFiltersearch(callerName2Sub2);
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2Sub2);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("1"),true);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("2"),true);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("3"),false);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp2Sub1.waitForDialerPage();
}
	

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Filter_is_applied_for_XYZ_user_then_call_is_running_by_that_user () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		phoneApp2.enterNumberinDialer(acc1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(Web2liveCallPage.validateAgentName("1"),callerName2);
		assertEquals(Web2liveCallPage.validateAgentinLivecalls("2"),false);
		
		
		String userOrteam2 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		
		String userOrteam3 = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam3);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp1.waitForDialerPage();
		
}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Enter_name_of_user_in_search_who_is_not_available_in_account () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch("!@#$");
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam1), false);
		
		String userOrteam2 = webApp2.enterUserOrTeaminWebFiltersearch("abc");
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam2), false);
		
		String userOrteam3 = webApp2.enterUserOrTeaminWebFiltersearch("98598");
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam3), false);
		
		String userOrteam4 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam4), true);

}
	
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Check_filter_while_call_is_running_without_user_name_Welcome_message_IVR () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(2);
		
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(Web2liveCallPage.validateAgentName("1"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("1"), "Welcome Msg");
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp3.waitForDialerPage();
		
		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.userTeamAllocation("users");
		
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(2);

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		
		Common.pause(2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(Web2liveCallPage.validateAgentName("1"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("1"), "IVR");
		
		String team1 = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(team1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp3.clickOnOutgoingHangupButton();
		
}
	
	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Check_filter_while_call_is_running_without_user_name_Voicemail_And_AWHM () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "close", "off", "off", "off");
		webApp2.userTeamAllocation("users");

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(2);
		Common.pause(3);
		assertEquals(Web2liveCallPage.validateAgentName("1"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("1"), "Voicemail");
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		
		
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		String user2 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(user2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp3.clickOnOutgoingHangupButton();
		
		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		webApp2.SetNumberSettingToggles("off", "off", "on", "off", "custom", "off", "off", "off");
		webApp2.enterAWHM_message(""
				+ "You have called out of our business hours.Please call later You have called out of our business hours. Please call later You have called out of our business hours.  Please call later You have called out of our business hours. Please call later You have called out of our business hours. ");
		Thread.sleep(3000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Thread.sleep(9000);
		String Url1 = driverWebApp2.getCurrentUrl();
		numberID = webApp2.getNumberId(Url1);
		api.addCustomTimeSlot("number", numberID, userID, false);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "custom", "off", "off", "off");
		webApp2.numberAfterWorkHourMessage("on");
		webApp2.clickonAWHMGreeting();
		webApp2.userTeamAllocation("users");
		
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(2);

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(2);
		Common.pause(2);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(Web2liveCallPage.validateAgentName("1"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("1"), "AWH Greeting");
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		
		String team1 = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(team1);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam1), false);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp3.waitForDialerPage();
		
}
	
	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Check_filter_while_call_is_running_without_user_name_Ringing_call_queue () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();

		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(5);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(acc2Number2);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");
		Common.pause(5);
		webApp2.userTeamAllocation("users");
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		phoneApp2.clickOnDialButton();
		phoneApp2Sub1.waitForIncomingCallingScreen();
		phoneApp2Sub1.clickOnIncomimgAcceptCallButton();
		
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp2Sub2.waitForIncomingCallingScreen();
		
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
	
		assertEquals(Web2liveCallPage.validateAgentName("3"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("3"), "Ringing");
		phoneApp2Sub2.clickOnIncomimgAcceptCallButton();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		String user2 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(user2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		webApp2.ClearWeblivecallsearchfilter();
		
		phoneApp4.enterNumberinDialer(acc2Number2);
		phoneApp4.clickOnDialButton();
		phoneApp4.validateDurationInCallingScreen(2);
		assertEquals(Web2liveCallPage.validateAgentName("4"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("4"), "In Queue");
		String team1 = webApp2.enterUserOrTeaminWebFiltersearch("Team");
		webApp2.selectUserOrTeamInWeblivecallFilter(team1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp4.clickOnOutgoingHangupButton();
		
}
	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Check_filter_while_call_is_running_without_user_name_Holiday () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		holidaywebApp.clickOnHolidaySubMenu();
		holidaywebApp.clickOnHolidayButton();
		holidaywebApp.enterHolidayName("holiday for everyone");
		holidaywebApp.clickOnHolidaySaveButton();
		assertEquals(holidaywebApp.validateSuccessValidationMsg(), "Holiday added successfully.");
		Common.pause(5);
		holidaywebApp.settingHolidayToggle("on");
		Common.pause(4);
		holidaywebApp.clickOnHolidayGreetingRadioButton();
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp3.enterNumberinDialer(acc2Number2);
		phoneApp3.clickOnDialButton();
		phoneApp3.validateDurationInCallingScreen(2);
		Common.pause(3);
		assertEquals(Web2liveCallPage.validateAgentName("1"), "");
		assertEquals(Web2liveCallPage.validateCallStatus("1"), "Holiday");
//		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		
		webApp2.selectUserOrTeamInWeblivecallFilter(callerName2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		String user2 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(user2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);	
		phoneApp3.waitForDialerPage();
}
	
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_liveCall_Filter_in_WebSubuser() throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());

		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(webApp2.ValidateFilterpopupWeblivecalls(), true);
		assertEquals(webApp2.ValidateFilterSearchbarinWeblivecalls(), true);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		Common.pause(2);
		Web2liveCallPage.clickOnDashboardTab();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), false);
		
}
	
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void apply_filter_against_User_subuser_in_Live_call_in_MainSubuser_Web () throws Exception {
		
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driverphoneApp2.get(url.dialerSignIn());
		
		phoneApp2Sub1.clickOnSideMenu();
		String callerName2Sub1 = phoneApp2Sub1.getCallerName();
		driverphoneApp2sub1.get(url.dialerSignIn());
		
		phoneApp2Sub2.clickOnSideMenu();
		String callerName2Sub2 = phoneApp2Sub2.getCallerName();
		driverphoneApp2sub2.get(url.dialerSignIn());
		
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		phoneApp2Sub1.enterNumberinDialer(acc1Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2Sub1.clickOnDialButton();
		
		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam = webApp2.enterUserOrTeaminWebFiltersearch(callerName2Sub1);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), false);
		assertEquals(Web2liveCallPage.validateAgentName("1"),userOrteam);
		String userOrteam1 = webApp2.enterUserOrTeaminWebFiltersearch(callerName2);
		webApp2.selectUserOrTeamInWeblivecallFilter(userOrteam1);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		phoneApp2Sub1.clickOnOutgoingHangupButton();
		phoneApp2Sub1.waitForDialerPage();
		
		
}
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Add_delete_Team_verify_in_live_call_filter () throws Exception {
		
		driverWebApp2.get(url.teamPage());
		String Team = webApp2.AddnewTeam("live filter");
		webApp2.Allocateuserinteam(account2Email);
		webApp2.Allocateuserinteam(account2EmailSubUser);
		webApp2.Allocateuserinteam(account2EmailSubUser2);
		webApp2.clickOnSimultaneously();
		webApp2.CreateTeam();
		
		webApp2.clickOnLogOut();
		loginWeb(webApp2, account2Email, account2Password);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam = webApp2.enterUserOrTeaminWebFiltersearch(Team);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam), true);
		
		driverWebApp2.get(url.teamPage());
		webApp2.DeleteSpecificTeam(Team);
		webApp2.clickonYesbuttonDeleteTeam();
		webApp2.clickOnLogOut();
		loginWeb(webApp2, account2Email, account2Password);
		
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webApp2.ValidateFilterIconinWeblivecalls(), true);
		webApp2.enterUserOrTeaminWebFiltersearch(Team);
		assertEquals(webApp2.ValidateUserOrTeamInWeblivecallFilter(userOrteam), false);

}
}
