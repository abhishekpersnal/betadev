package com.CallHippo.web.dashboard.livecall;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;

public class LiveCallTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	WebDriver driver3;

	WebToggleConfiguration webApp2;
	LiveCallPage webApp2LivePage;
	LiveCallPage webApp1LivePage;
	WebToggleConfiguration webApp1;
	WebDriver driver4;

	WebDriver driver5;
	LiveCallPage webApp2SubUserLivePage;
	WebToggleConfiguration webApp2SubUser;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");

	String userID;
	String numberID;

	Common excelTestResult;

	String webapp2UserID;
	String webapp2SubUserID;
	String apiToken;
	
	public LiveCallTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2LivePage = PageFactory.initElements(driver4, LiveCallPage.class);

		driver3 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp1 = PageFactory.initElements(driver3, WebToggleConfiguration.class);
		webApp1LivePage = PageFactory.initElements(driver3, LiveCallPage.class);

		driver5 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2SubUser = PageFactory.initElements(driver5, WebToggleConfiguration.class);
		webApp2SubUserLivePage = PageFactory.initElements(driver5, LiveCallPage.class);

		driver = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap3 session");
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driver3.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp1, account1Email, account1Password);
			} catch (Exception e) {
				driver3.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp1, account1Email, account1Password);
			}

			try {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2SubUser, account2EmailSubUser, account1Password);
			} catch (Exception e) {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2SubUser, account2EmailSubUser, account1Password);
			}

			try {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}
			try {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);
				
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= "+ numberID);
				
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				Common.pause(3);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				String UrlmainUser = driver4.getCurrentUrl();
				webapp2UserID = webApp2.getUserId(UrlmainUser);
				
				webApp2SubUser.userspage();
				webApp2SubUser.navigateToUserSettingPage(account2Email);
				String Url2 = driver5.getCurrentUrl();
				webapp2SubUserID = webApp2.getUserId(Url2);
				
				webApp2.clickOnIntegrationLinkFromSideMenu();
				 apiToken = webApp2.getRestAPIToken();

			} catch (Exception e) {
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);

				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getUserId(Url1);
				
				Thread.sleep(9000);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Thread.sleep(2000);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				String UrlmainUser = driver4.getCurrentUrl();
				webapp2UserID = webApp2.getUserId(UrlmainUser);
				
				webApp2SubUser.userspage();
				webApp2SubUser.navigateToUserSettingPage(account2Email);
				String Url2 = driver5.getCurrentUrl();
				webapp2SubUserID = webApp2.getUserId(Url2);
				
				webApp2.clickOnIntegrationLinkFromSideMenu();
				 apiToken = webApp2.getRestAPIToken();

			}
		} catch (Exception e1) {
			String testname = "Live Calls Before Test ";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driver3.get(url.signIn());
			driver4.get(url.signIn());
			driver5.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2SubUser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}
			if (webApp1.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp1, account1Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
		} catch (Exception e) {

			try {
				Common.pause(3);
				driver3.get(url.signIn());
				driver4.get(url.signIn());
				driver5.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (webApp2SubUser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2SubUser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}
				if (webApp1.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp1, account1Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}
			} catch (Exception e1) {
				String testname = "Live calls Before Method ";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
				Common.Screenshot(driver5, testname, "WebAppp2SubUser Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "WebAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver3, testname, "WebAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driver3.quit();
		driver5.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verifyHeadersInlivecall() throws Exception {

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();

		driver3.get(url.signIn());
		webApp1LivePage.clickOnLiveCallTab();

		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		Common.pause(10);

		assertEquals(webApp1LivePage.validateThereIsNoLiveCall(), true);
		assertEquals(webApp2LivePage.validateThereIsNoLiveCall(), true);

		assertEquals(webApp1LivePage.validateAgentHeaderIsDisplayed(), true);
		assertEquals(webApp1LivePage.validateFromHeaderIsDisplayed(), true);
		assertEquals(webApp1LivePage.validateToHeaderIsDisplayed(), true);
		assertEquals(webApp1LivePage.validateStartTimeHeaderIsDisplayed(), true);
		assertEquals(webApp1LivePage.validateStatusHeaderIsDisplayed(), true);
		assertEquals(webApp1LivePage.validateActionHeaderIsDisplayed(), true);

		assertEquals(webApp2LivePage.validateAgentHeaderIsDisplayed(), true);
		assertEquals(webApp2LivePage.validateFromHeaderIsDisplayed(), true);
		assertEquals(webApp2LivePage.validateToHeaderIsDisplayed(), true);
		assertEquals(webApp2LivePage.validateStartTimeHeaderIsDisplayed(), true);
		assertEquals(webApp2LivePage.validateStatusHeaderIsDisplayed(), true);
		assertEquals(webApp2LivePage.validateActionHeaderIsDisplayed(), true);

		assertEquals(webApp2SubUserLivePage.validateAgentHeaderIsDisplayed(), true);
		assertEquals(webApp2SubUserLivePage.validateFromHeaderIsDisplayed(), true);
		assertEquals(webApp2SubUserLivePage.validateToHeaderIsDisplayed(), true);
		assertEquals(webApp2SubUserLivePage.validateStartTimeHeaderIsDisplayed(), true);
		assertEquals(webApp2SubUserLivePage.validateStatusHeaderIsDisplayed(), true);
		assertEquals(webApp2SubUserLivePage.validateActionHeaderIsDisplayed(), true);

	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validateInboundCallCount() throws Exception {

		System.out.println(webapp2UserID + "\n"+apiToken);
		String expectedTotalInboundCalls = api.gettotalInboundCallsFromLiveCallPage(apiToken, webapp2UserID);
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2LivePage.getTotalInboundCall(), expectedTotalInboundCalls);
		String expectedTotalInboundCallsSubuser = api.gettotalInboundCallsFromLiveCallPage(apiToken, webapp2SubUserID);
		
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2SubUserLivePage.getTotalInboundCall(), expectedTotalInboundCallsSubuser);
	
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validateOutboundCallCount() throws Exception {

		String expectedTotalOutboundCalls = api.gettotalOutboundCallsFromLiveCallPage(apiToken, webapp2UserID);
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2LivePage.getTotalOutboundCall(), expectedTotalOutboundCalls);
	
		String expectedTotalOutboundCallsSubUser = api.gettotalOutboundCallsFromLiveCallPage(apiToken, webapp2SubUserID);
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2SubUserLivePage.getTotalOutboundCall(), expectedTotalOutboundCallsSubUser);
	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validateMissedCallCount() throws Exception {

		String expectedTotalMissedCalls = api.gettotalMissedCallsFromLiveCallPage(apiToken, webapp2UserID);
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2LivePage.getTotalMissedCall(), expectedTotalMissedCalls);
	
		String expectedTotalMissedCallsSubuser = api.gettotalMissedCallsFromLiveCallPage(apiToken, webapp2SubUserID);
		
		driver5.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2SubUserLivePage.getTotalMissedCall(), expectedTotalMissedCallsSubuser);
		
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validateAverageCallDuration() throws Exception {

		String expectedCallDuration = api.gettotalCallDurationFromLiveCallPage(apiToken, webapp2UserID);
		
		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2LivePage.getAverageCallDuration(), expectedCallDuration);
	
		
		String expectedCallDurationSubuser = api.gettotalCallDurationFromLiveCallPage(apiToken, webapp2SubUserID);
		
		driver4.get(url.signIn());
		webApp2SubUserLivePage.clickOnLiveCallTab();
		
		Common.pause(10);
		assertEquals(webApp2SubUserLivePage.getAverageCallDuration(), expectedCallDurationSubuser);
	
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verifyUserIsLoggedIn() throws IOException {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		driver4.get(url.signIn());
		webApp2LivePage.clickOnLiveCallTab();

		Common.pause(9);
		assertEquals(webApp2LivePage.validateUserIsLoggedIn(callerName2), true);
		assertEquals(webApp2LivePage.getAvailableAgent(), "1");
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		
		Common.pause(9);
		assertEquals(webApp2LivePage.validateUserIsLoggedIn(callerName2), false);
		assertEquals(webApp2LivePage.getAvailableAgent(), "0");
		
	}

	
	
}
