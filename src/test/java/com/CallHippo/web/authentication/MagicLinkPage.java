package com.CallHippo.web.authentication;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class MagicLinkPage {


	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	WebDriver driver1;
	PropertiesFile credential;

	@FindBy(name = "email")
	WebElement email;
	@FindBy(xpath = "//button[contains(.,'Send Magic Link')]")
	WebElement magicLinkbtn;
	@FindBy(xpath = "//div[@class='chMagicLinkWrapper']/label/span/input[@type='checkbox']")
	WebElement magicLinkCheckbox;
	@FindBy(xpath = "//input[contains(@placeholder,'Email')]//following-sibling::div[contains(.,'Enter a valid email.')]")
	WebElement enterValidEmail;
	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	WebElement emailValidationMsg;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement errorMessage;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span")
	WebElement infoMessage;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-warning')]]/span")
	WebElement warningMessage;
	@FindBy(xpath = "(//iframe[@src[contains(.,'recaptcha')]])[1]")
	WebElement googleCaptcha;
	

	// gmail xpaths
	@FindBy(xpath = "//input[@id='identifierId']")
	WebElement gemail;
	@FindBy(xpath = "//*[@id=\"identifierNext\"]/div/button")
	WebElement gemailNext;
	@FindBy(xpath = "//input[@name='password']")
	WebElement gpassword;
	@FindBy(xpath = "//*[@id=\"passwordNext\"]/div/button")
	WebElement gpasswordNext;
	@FindBy(xpath = "//span[contains(text(),'CallHippo: Magic Link')]")
	WebElement mailtitle;
	@FindBy(xpath = "//a[contains(.,'Magic login')]")
	WebElement magicLogin;

	public MagicLinkPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		action = new Actions(this.driver);
		
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public String getTitle() {
		String signupTitle = driver.getTitle();
		return signupTitle;
	}

	public void enterEmail(String email) {
		this.email.sendKeys(email);
	}

	public String IsMagicLinkButtonEnabled() {
		return magicLinkbtn.getText();
	}

	public void clickOnMagicLink() {
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(googleCaptcha));
		//driver.switchTo().defaultContent();
		Common.pause(2);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", magicLinkbtn);
	}

	public void clickOMagicLinkCheckbox() {

		if (magicLinkCheckbox.isSelected() == false) {
			magicLinkCheckbox.click();
		}
	}

	public String verifyValidEmailValidationMsg() {
		return enterValidEmail.getText();
	}
	
	public String errorMessage() {
		wait.until(ExpectedConditions.visibilityOf(errorMessage));
		return errorMessage.getText();
	}
	
	public String infoMessage() {
		wait.until(ExpectedConditions.visibilityOf(infoMessage));
		return infoMessage.getText();
	}

	public String validateErrormessage() {

		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);

				wait.until(ExpectedConditions.titleIs("Login | Callhippo.com"));
				// System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				Common.pause(1);
				driver.switchTo().window(childWindow);
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class[contains(.,'ant-message-error')]]/span"))));
				return driver.findElement(By.xpath("//div[@class[contains(.,'ant-message-error')]]/span")).getText();
				// driver.close();
			}
		}
		return mainWindow;
	}

	public String validateErrormessageOfBlockedUser() {

		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);

				wait.until(ExpectedConditions.titleIs("Login | Callhippo.com"));
				// System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'ant-message-custom-content ant-message-error')]")));
				
				String errorMessageTxt = driver.switchTo().window(childWindow)
				.findElement(By.xpath("//div[contains(@class,'ant-message-custom-content ant-message-error')]")).getText(); 
				
				Common.pause(2);
				
				return errorMessageTxt;
				
				// driver.close();
			}
		}
		return mainWindow;
	}

	// ---------------------------Start Gmail Login functions ---------------------

	public void validateMagicLickExpired() {
		gloginGmail();
		openMail();
		gClickOnMagicLoginlink();
		// ggetTitle();

	}

	public void ValidateMagicLinOnGmail() {
		gloginGmail();
		openMail();
		gClickOnRecentMagicLoginlink();
		// ggetTitle();
	}

	public void gloginGmail() {

		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gemail.sendKeys(credential.gmailId());
		} catch (IOException e1) {
		}
		gemailNext.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		try {
			gpassword.sendKeys(credential.gmailPassword());
		} catch (IOException e1) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void gClickOnRecentMagicLoginlink() {
		
		int attempt=0;
		int MAX_ATTEMPTS = 5;
		
		while( attempt < MAX_ATTEMPTS ) {
		    try {
		    	Common.pause(2);
		    	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Magic login')]"))));
				int size = driver.findElements(By.xpath("//a[contains(.,'Magic login')]")).size();
				System.out.println(size);
		    	if (size < 2) {
					WebElement magicLogin1 = driver.findElement(By.xpath("//a[contains(.,'Magic login')]"));
					wait.until(ExpectedConditions.elementToBeClickable(magicLogin1));
					//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(magicLogin1)));
					magicLogin1.click();
				} else {
					WebElement magicLogin2 = driver.findElements(By.xpath("//a[contains(.,'Magic login')]")).get(size - 1);
					wait.until(ExpectedConditions.elementToBeClickable(magicLogin2));
					//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(magicLogin2)));
					Common.pause(1);
					magicLogin2.click();
					//driver.findElements(By.xpath("//a[contains(.,'Magic login')]")).get(size - 1).click();

				}
		    	break;
		    }
		    catch (Exception e )
		    {
		    	System.out.println(attempt);
		      //if ( error.message.includes( "StaleElementReference" ) )
		    	attempt++;
		    }
		  }
	}

	public void gClickOnMagicLoginlink() {
		int attempt=0;
		int MAX_ATTEMPTS = 5;
		
		while( attempt < MAX_ATTEMPTS ) {
		    try {
		    	Common.pause(2);
		    	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Magic login')]"))));
		    	int size = driver.findElements(By.xpath("//a[contains(.,'Magic login')]")).size();
		    	System.out.println(size);

				if (size > 0) {
					driver.findElement(By.xpath("//a[contains(.,'Magic login')]")).click();
				} else {
					Common.pause(1);
					driver.findElements(By.xpath("//a[contains(.,'Magic login')]")).get(size - 1).click();
			
				}
				break;
		    }catch (Exception e )
		    {
		    	System.out.println(attempt);
		      //if ( error.message.includes( "StaleElementReference" ) )
		    	attempt++;
		    }
		}
	}

	public String ggetTitle() {
		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				Common.pause(1);
				wait.until(ExpectedConditions.titleIs("Dashboard | Callhippo.com"));
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		return mainWindow;
	}

	public void openMail() {
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo: Magic Link')]")).get(1).click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(
							driver.findElements(By.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo: Magic Link')]")).get(1)));
					driver.findElements(By.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo: Magic Link')]")).get(1).click();
				}

				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	// ---------------------------Start Gmail Login functions ---------------------



}
