package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;

public class ChangeEmailTest {
	SignupPage registrationPage;
	SignupPage dashboard;
	SignupTest signupTest;
	LoginPage loginpage;
	WebDriver driver;
	WebDriver driverMail;
	static Common excel;
	PropertiesFile url;
	String gmailUser;
	WebToggleConfiguration webAppMainUserLogin;

	public ChangeEmailTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(url.getValue("environment") + "\\Signup.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			try {
				dashboard.deleteAllMail();
			} catch (Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}

	@BeforeMethod
	public void initialization() throws Exception {

		try {
			driver = TestBase.init();
			try {
				driver.get(url.livesignUp());
			}catch(Exception e) {
				driver.get(url.livesignUp());
			}
			registrationPage = PageFactory.initElements(driver, SignupPage.class);
			signupTest = PageFactory.initElements(driver, SignupTest.class);
			dashboard = PageFactory.initElements(driver, SignupPage.class);
			loginpage = PageFactory.initElements(driver, LoginPage.class);
			webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);

		} catch (Exception e) {
			Common.Screenshot(driver, " Web SignupTest - Fail ", "BeforeMethod - initialization");
			System.out.println("----Need to check issue - SignupTest - BeforeMethod - initialization----");
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		 driver.quit();
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_validation_for_changeEmail_functinality() throws Exception {

		System.out.println("----Welcome to signup");

		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		assertEquals(registrationPage.getTextMainTitle(), "Thank you for Signing up.");

		registrationPage.ValidateVerifyButtonOnGmail();
		dashboard.verifyWelcomeToCallHippoPopup();

		// empty and invalid email validation in profile section
		registrationPage.clickOnUserProfile();
		Common.pause(3);
		assertEquals(registrationPage.validateUserProfile(), true);

		registrationPage.clearEmailProfileSection();
		assertEquals(registrationPage.validationMessageInvalidInput(), "Email is required.");
		Common.pause(3);
		String invalidemailForProfile = gmailUser + "+" + date + "callhippo.com";
		registrationPage.enterEmailInProfileSection(invalidemailForProfile);

		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);

		registrationPage.enterEmailInProfileSection("test");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInProfileSection("test@");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInProfileSection("test@gmail");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInProfileSection("test@gmail.");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);    	
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);

		Common.pause(3);
		String emailForProfile1 = gmailUser + "+profile1+" + date + "@callhippo.com";
		registrationPage.enterEmailInProfileSection(emailForProfile1);

		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");

		Common.pause(40);
		String emailForProfile2 = gmailUser + "+profile2+" + date + "@callhippo.com";
		registrationPage.enterEmailInProfileSection(emailForProfile2);

		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email"); // 2 tab
		Common.pause(20);

		registrationPage.ggetTitleFoEmail();
		registrationPage.gClickOnRecentMagicLoginlink();// new tab dasboard
		webAppMainUserLogin.switchToWindowBasedOnTitle("Dashboard | Callhippo.com");
		// registrationPage.ggetTitle();
		System.out.println(registrationPage.validationErrorMessage());
		assertEquals(registrationPage.validationErrorMessage(), "User is already verified, Please login."); // sign up user
		Common.pause(3);
		dashboard.userLogout();
		Common.pause(10);
		registrationPage.getGmailURL();
		registrationPage.openMail();
	    registrationPage.gClickOnExpiryMagicLoginlink();
	
		webAppMainUserLogin.switchToWindowBasedOnTitle("Login | Callhippo.com");

		System.out.println(registrationPage.validationErrorMessage());

		assertEquals(registrationPage.validationErrorMessage(),
				"Your email has been changed. Please login with new email."); // profile1
		Common.pause(5);
		loginpage.enterEmail(emailForProfile1);
		loginpage.enterPassword(excel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");
		Common.pause(7);
		loginpage.enterEmail(emailForProfile2);
		loginpage.enterPassword(excel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");

		Common.pause(10);

		registrationPage.getGmailURL();
		registrationPage.openMail1();
	    registrationPage.gClickOnRecentMagicLoginlink();
	
		webAppMainUserLogin.switchToWindowBasedOnTitle("Login | Callhippo.com");
		
		System.out.println(registrationPage.validationSuccessMessage());
		assertEquals(registrationPage.validationSuccessMessage(), "Your email address has been successfully changed"); // profile2
		Common.pause(7);
		loginpage.enterEmail(emailForProfile2);
		loginpage.enterPassword(excel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		registrationPage.clickOnUserProfile();
		Common.pause(3);
		assertEquals(registrationPage.validateUserProfile(), true);
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), emailForProfile2);

	}

	@Test(priority = 2, retryAnalyzer = Retry.class, enabled = false)
	public void verify_validation_for_changeEmail_functinality_before_verification() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		assertEquals(registrationPage.validateActivationMessage(), true);
		registrationPage.clickOnChangeYourEmailButton();
		Common.pause(3);
		assertEquals(registrationPage.validateCancelButtonInPopup(), true);
		assertEquals(registrationPage.validatSendActivationButtonInPopup(), true);

		registrationPage.clearEmailInPopup();

		assertEquals(registrationPage.validationMessageInvalidInput(), "Please input your E-mail!");
		Common.pause(3);
		String invalidemail = gmailUser + "+" + date + "callhippo.com";
		registrationPage.enterEmailInPopup(invalidemail);

		assertEquals(registrationPage.validationMessageInvalidInput(), "The input is not valid E-mail!");

		Common.pause(3);

		registrationPage.enterEmailInPopup("test");
		assertEquals(registrationPage.validationMessageInvalidInput(), "The input is not valid E-mail!");
		Common.pause(3);
		registrationPage.enterEmailInPopup("test@");
		assertEquals(registrationPage.validationMessageInvalidInput(), "The input is not valid E-mail!");
		Common.pause(3);
		registrationPage.enterEmailInPopup("test@gmail");
		assertEquals(registrationPage.validationMessageInvalidInput(), "The input is not valid E-mail!");
		Common.pause(3);
		registrationPage.enterEmailInPopup("test@gmail.");
		assertEquals(registrationPage.validationMessageInvalidInput(), "The input is not valid E-mail!");
		Common.pause(3);

		registrationPage.enterEmailInPopup(email);
		registrationPage.clickOnSendActivationButtonInPopup();
		assertEquals(registrationPage.validationErrorMessage(),
				"New email can not be the same as previous email . Please enter different email address.");

		registrationPage.ValidateVerifyButtonOnGmail();
		dashboard.verifyWelcomeToCallHippoPopup();
		// empty and invalid email validation in profile section
		registrationPage.clickOnUserProfile();
		Common.pause(3);
		assertEquals(registrationPage.validateUserProfile(), true);
		registrationPage.clearEmailProfileSection();

		assertEquals(registrationPage.validationMessageInvalidInput(), "Email is required.");
		Common.pause(3);
		String invalidemailForProfile = gmailUser + "+" + date + "callhippo.com";
		registrationPage.enterEmailInProfileSection(invalidemailForProfile);

		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);

		registrationPage.enterEmailInProfileSection("test");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInProfileSection("test@");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInProfileSection("test@gmail");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInProfileSection("test@gmail.");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);

		// resend activation
		registrationPage.clickOnResendActivationButton();
		assertEquals(registrationPage.validationSuccessMessage(),
				"We have sent you verification mail Please verify it to process further");

		registrationPage.ValidateVerificationMailOnGmail(); // just email check but do not click on verify button

		assertEquals(registrationPage.validateActivationMessage(), true);

		assertEquals(registrationPage.validateUserProfile(), true);
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), email);

		registrationPage.clickOnChangeYourEmailButton();

		Common.pause(3);
		String emailForheader = gmailUser + "+header+" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(emailForheader);
		registrationPage.clickOnSendActivationButtonInPopup();

		assertEquals(registrationPage.validationSuccessMessage(),
				"We have sent you verification mail Please verify it to process further");

		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);

		Common.pause(3);
		String emailForProfile1 = gmailUser + "+profile1+" + date + "@callhippo.com";
		registrationPage.enterEmailInProfileSection(emailForProfile1);

		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");

		Common.pause(3);
		String emailForProfile2 = gmailUser + "+profile2+" + date + "@callhippo.com";
		registrationPage.enterEmailInProfileSection(emailForProfile2);

		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");
		Common.pause(10);
		registrationPage.ValidateVerifyButtonOnGmailForheader();

		registrationPage.ggetTitleFoEmail();
		registrationPage.gClickOnRecentMagicLoginlink();
		registrationPage.ggetTitle();

		System.out.println(registrationPage.validationErrorMessage());
		assertEquals(registrationPage.validationErrorMessage(), "User is already verified, Please login.");
		Common.pause(3);

		System.out.println(registrationPage.validationErrorMessage());
		assertEquals(registrationPage.validationErrorMessage(),
				"Sorry this confirmation request has expired. We have send you a new confirmation link.");
		Common.pause(3);
		dashboard.userLogout();

		loginpage.enterEmail(emailForheader);
		loginpage.enterPassword(excel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals(registrationPage.validationErrorMessage(), "Please activate your account");

		Common.pause(7);
		loginpage.enterEmail(emailForProfile2);
		loginpage.enterPassword(excel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(5);
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");

		Common.pause(10);
		registrationPage.ValidateVerifyButtonOnGmailForUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(), "Your email address has been successfully changed");
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), emailForProfile2);

	}

	@Test(priority = 3, retryAnalyzer = Retry.class, enabled = false)
	public void verify_validation_for_changeEmail_functinality_After_verification() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		assertEquals(registrationPage.validateActivationMessage(), true);
		registrationPage.clickOnChangeYourEmailButton();
		Common.pause(3);
		assertEquals(registrationPage.validateCancelButtonInPopup(), true);
		assertEquals(registrationPage.validatSendActivationButtonInPopup(), true);

		Common.pause(3);
		String emailForheader = gmailUser + "+header+" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(emailForheader);
		registrationPage.clickOnSendActivationButtonInPopup();

		assertEquals(registrationPage.validationSuccessMessage(),
				"We have sent you verification mail Please verify it to process further");

		registrationPage.ValidateVerifyButtonOnGmail();

		dashboard.verifyWelcomeToCallHippoPopup();
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), email);

		Common.pause(3);
		dashboard.userLogout();

		Common.pause(3);
		loginpage.enterEmail(email);
		// loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.enterPassword(excel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		Common.pause(3);
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), email);

		registrationPage.ggetTitleFoEmail();
		registrationPage.gClickOnRecentMagicLoginlink();
		registrationPage.ggetTitle();

		System.out.println(registrationPage.validationErrorMessage());
		assertEquals(registrationPage.validationErrorMessage(), "User is already verified, Please login.");
		Common.pause(3);

		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);
		Common.pause(3);
		String emailForProfile = gmailUser + "+profile+" + date + "@callhippo.com";
		registrationPage.enterEmailInProfileSection(emailForProfile);

		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");
		Common.pause(10);
		registrationPage.ValidateVerifyButtonOnGmailForUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(), "Your email address has been successfully changed");
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), emailForProfile);
	}

	@Test(priority = 4, retryAnalyzer = Retry.class, enabled = false)
	public void verify_validation_with_old_email() throws Exception {

		String date = excel.date();
		String email = gmailUser + "+" + date + "@callValidateVerifyButtonOnGmailForheaderippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		registrationPage.clickOnChangeYourEmailButton();
		registrationPage.validateCancelButtonInPopup();
		registrationPage.validatSendActivationButtonInPopup();
		Common.pause(10);
		String newemail = gmailUser + "+1" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(newemail);
		registrationPage.clickOnSendActivationButtonInPopup();

		assertEquals(registrationPage.validationSuccessMessage(),
				"We have sent you verification mail Please verify it to process further");

		registrationPage.ValidateVerifyButtonOnGmail();

		dashboard.closeAddnumberPopup();
		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");

	}

	@Test(priority = 5, retryAnalyzer = Retry.class, enabled = false)
	public void verify_with_new_signup_change_email_from_profile_section_and_login_without_verification()
			throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		registrationPage.clickOnChangeYourEmailButton();
		registrationPage.validateCancelButtonInPopup();
		registrationPage.validatSendActivationButtonInPopup();
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);

		String newemail = gmailUser + "+1" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(newemail);
		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");

		dashboard.userLogout();
		Common.pause(3);
		// do login without verification
		loginpage.enterEmail(newemail);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");

	}

	@Test(priority = 6, retryAnalyzer = Retry.class, enabled = false)
	public void verify_with_new_signup_and_change_email_from_profile_section_and_login_with_verification()
			throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		registrationPage.clickOnChangeYourEmailButton();
		registrationPage.validateCancelButtonInPopup();
		registrationPage.validatSendActivationButtonInPopup();
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);

		String newemail = gmailUser + "+1" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(newemail);
		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");

		registrationPage.ValidateVerifyButtonOnGmailForUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(), "Your email address has been successfully changed");
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), newemail);

		dashboard.userLogout();
		Common.pause(3);
		// do login after verification
		loginpage.enterEmail(newemail);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

	}

	@Test(priority = 7, retryAnalyzer = Retry.class, enabled = false)
	public void verify_with_new_signup_and_without_change_email_and_resend_activation() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		registrationPage.clickOnChangeYourEmailButton();
		registrationPage.validateCancelButtonInPopup();
		registrationPage.validatSendActivationButtonInPopup();
		registrationPage.clickOnSendActivationButtonInPopup();
		registrationPage.clickOnResendActivationButton();
		assertEquals(registrationPage.validationSuccessMessage(),
				"We have sent you verification mail Please verify it to process further");

		registrationPage.ValidateVerifyButtonOnGmail();

		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), email);
	}

	@Test(priority = 8, retryAnalyzer = Retry.class, enabled = false)
	public void verify_validation_for_new_signup_account_activiation_link_with_email_after_change_email_in_header_verification_message()
			throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();

		registrationPage.clickOnChangeYourEmailButton();
		registrationPage.validateCancelButtonInPopup();
		registrationPage.validatSendActivationButtonInPopup();
		Common.pause(10);
		String newemail = gmailUser + "+1" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(newemail);
		registrationPage.clickOnSendActivationButtonInPopup();

		assertEquals(registrationPage.validationSuccessMessage(),
				"We have sent you verification mail Please verify it to process further");

		registrationPage.ValidateVerifyButtonOnGmail2();

		System.out.println(registrationPage.validationErrorMessage());
		assertEquals(registrationPage.validationErrorMessage(),
				"Sorry this confirmation request has expired. We have send you a new confirmation link.");

	}

	@Test(priority = 9, retryAnalyzer = Retry.class, enabled = false)
	public void verify_validation_for_empty_email_and_invalid_email_from_user_profile_section() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);

		registrationPage.clearEmailInPopup();

		assertEquals(registrationPage.validationMessageInvalidInput(), "Email is required.");
		Common.pause(3);
		String invalidemail = gmailUser + "+" + date + "callhippo.com";
		registrationPage.enterEmailInPopup(invalidemail);

		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);

		registrationPage.enterEmailInPopup("test");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInPopup("test@");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInPopup("test@gmail");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);
		registrationPage.enterEmailInPopup("test@gmail.");
		assertEquals(registrationPage.validationMessageInvalidInput(), "Enter a valid email.");
		Common.pause(3);

		registrationPage.enterEmailInPopup(email);
		registrationPage.clickOnSaveButtonInUserProfile();
		assertEquals(registrationPage.validationSuccessMessage(), "User updated successfully");

		registrationPage.ValidateVerifyButtonOnGmail();
		Common.pause(3);
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);
		System.out.println(registrationPage.getEmailUserProfile());
		assertEquals(registrationPage.getEmailUserProfile(), email);
	}

	@Test(priority = 10, retryAnalyzer = Retry.class, enabled = false)
	public void verify_validation_for_Expire_link_by_sending_activation_link_from_user_profile_section()
			throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);

		dashboard.verifyWelcomeToCallHippoPopup();
		registrationPage.clickOnUserProfile();
		assertEquals(registrationPage.validateUserProfile(), true);

		String newemail = gmailUser + "+1" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(newemail);
		registrationPage.clickOnSaveButtonInUserProfile();

		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");
		Common.pause(3);
		String newemail1 = gmailUser + "+2" + date + "@callhippo.com";
		registrationPage.enterEmailInPopup(newemail1);
		registrationPage.clickOnSaveButtonInUserProfile();
		assertEquals(registrationPage.validationSuccessMessage(),
				"User updated successfully. A Verification has been sent to your new email");
		Common.pause(10);
		registrationPage.ValidateVerifyButtonOnGmailForUserProfile1();
		System.out.print(registrationPage.validationErrorMessage());
		assertEquals(registrationPage.validationErrorMessage(),
				"Your email has been changed. Please login with new email.");

		dashboard.userLogout();
		Common.pause(3);
		// do login without verification
		loginpage.enterEmail(newemail1);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");

		Common.pause(5);
		// do login with Expire link Email
		loginpage.enterEmail(newemail);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals(registrationPage.validationErrorMessage(), "This email doesn't exist");

	}
}
