package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.*;
import com.CallHippo.Init.TestBase;

public class validateexcelcodeTest {
	SignupPage registrationPage;
	SignupPage dashboard;
	LoginPage loginpage;
	WebDriver driver;
	WebDriver driverMail;
	static Common excel;
	PropertiesFile url;
	String gmailUser;

	public validateexcelcodeTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common("ScreenShots\\TestCaseResults.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

	@BeforeTest
	public void deleteAllMails() {
		System.out.println("before test");
	}

	@BeforeMethod
	public void initialization() throws Exception {
		System.out.println("before method");
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception, Throwable {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
//			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
//			System.out.println(testname + " - " + result.getMethod().getMethodName());

//			Throwable e = result.getThrowable();
//			
//			StringWriter sw = new StringWriter();
//            // create a PrintWriter
//            PrintWriter pw = new PrintWriter(sw);
//            e.printStackTrace(pw);
//            String error = sw.toString();
//            System.out.println("Error:\n" + error);
			
			
			
			
			
//			String methodName = result.getMethod().getMethodName();

			
//			excel.updateRusultWithFailerLog(0, methodName, result.getThrowable().toString());
			
			
			
			
			
			excel.updateRusult(0, result.getMethod().getMethodName(), "Fail");
			excel.updateRusultWithFailerLog(0, result.getMethod().getMethodName(), result.getThrowable());
			

		} else {
			String testname = "Pass";
//			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
//			System.out.println(testname + " - " + result.getMethod().getMethodName());

//			String methodName = result.getMethod().getMethodName();

			excel.updateRusult(0, result.getMethod().getMethodName(), "Pass");
		}
//		driver.quit();
	}

	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v1() {
		System.out.println("verifyTitle");
	}

	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v2() {
		System.out.println("verifyTitle");
	}

	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v3() {
		System.out.println("verifyTitle");
//		assertEquals(true, false);
	}

	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v4() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v5() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v6() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v7() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v8() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v9() {
		System.out.println("verifyTitle");
		assertEquals(true, false, "method 9 is failing ");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v10() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v11() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v12() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v13() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v14() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v15() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v16() {
		System.out.println("verifyTitle");
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void v17() {
		System.out.println("verifyTitle");
	}
	
	

}
