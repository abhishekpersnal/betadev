package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;

public class ChangePasswordTest {
	WebDriver driverWebApp1;
	WebDriver driverMail;
	ChangePasswordPage changePasswordPage;
	WebToggleConfiguration webAppMainUserLogin;
	SignupPage registrationPage;
	LoginPage loginpage;
	ForgotPasswordPage forgotPasswordPage;

	static Common signUpExcel;
	static Common excel;
	PropertiesFile url;
	String gmailUser;
	String email;

	public ChangePasswordTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		signUpExcel = new Common(url.getValue("environment")+"\\Signup.xlsx");
		excel = new Common(url.getValue("environment")+"\\Forgot Password.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

//	@BeforeTest
//	public void deleteAllMails() {
//		try {
//			driverMail = TestBase.init3();
//			registrationPage = PageFactory.initElements(driverMail, SignupPage.class);
//			// driver.get(url);
//			try {
//				registrationPage.deleteAllMail();
//			} catch (Exception e) {
//
//				registrationPage.deleteAllMail();
//			}
//		} catch (Exception e) {
//			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
//			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
//		}
//		driverMail.quit();
//	}

	@BeforeTest
	public void beforeTestInitialization() throws Exception {
		 deleteAllMails();

		try {
			driverWebApp1 = TestBase.init();
			changePasswordPage = PageFactory.initElements(driverWebApp1, ChangePasswordPage.class);
			webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
			registrationPage = PageFactory.initElements(driverWebApp1, SignupPage.class);
			loginpage = PageFactory.initElements(driverWebApp1, LoginPage.class);
			forgotPasswordPage = PageFactory.initElements(driverWebApp1, ForgotPasswordPage.class);
			
		} catch (Exception e) {
			Common.Screenshot(driverWebApp1, " Web ChangePasswordTest - Fail ", "BeforeMethod - initialization");
			System.out.println("----Need to check issue - ChangePasswordTest - BeforeMethod - initialization----");
		}

		signUpUser();
		LoginGmailAndConfirmYourMail();
	}

	@AfterMethod
	public void afterMethodWork(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
//		driverWebApp1.quit();
	}
	
	@AfterTest (alwaysRun = true) 
	public void tearDown(){
		driverWebApp1.quit();
	}

	private void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			registrationPage = PageFactory.initElements(driverMail, SignupPage.class);
			try {
				registrationPage.deleteAllMail();
			} catch (Exception e) {

				registrationPage.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}

	private String signUpUser() throws Exception {
		driverWebApp1.get(url.livesignUp());
		Common.pause(1);
		String date = signUpExcel.date();
		email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---email1---" + email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signUpExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signUpExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signUpExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signUpExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signUpExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");

		return email;
	}

	private void LoginGmailAndConfirmYourMail() throws Exception {

		registrationPage.ValidateVerifyButtonOnGmail();

		String actualResult = loginpage.loginSuccessfully();
		String expectedResult = "Dashboard | Callhippo.com";
		assertEquals(actualResult, expectedResult);

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();
	}

	public void changePassword(WebToggleConfiguration web) {
		web.clickOnChangePassword();
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_ChangePassword_Page_Title_Description() {
		changePassword(webAppMainUserLogin);

		String actualResult = changePasswordPage.validatePageTitle();
		String expectedResult = "Change Password";
		assertEquals(actualResult, expectedResult);
		String actualResult1 = changePasswordPage.validatePageTitleDescription();
		String expectedResult1 = "Update your account password. Always set a strong password, which helps to prevent unauthorized access to your account.";
		assertEquals(actualResult1, expectedResult1);
	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_ChangePassword_Page_Fields() {
		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);
		assertEquals(changePasswordPage.validateCurrentPasswordTextBoxIsDisplayed(), true);
		assertEquals(changePasswordPage.validateNewPasswordTextBoxIsDisplayed(), true);
		//assertEquals(changePasswordPage.validateConfirmTextBoxIsDisplayed(), true);
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void check_require_field_validations() {
		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);
		changePasswordPage.clearCurrentPassword();
		String actualResult = changePasswordPage.validateCurrentPasswordRequiredField();
		String expectedResult = "Please input your Current Password!";
		assertEquals(actualResult, expectedResult);

		changePasswordPage.clearNewPassword();
		assertEquals(forgotPasswordPage.validateMinimun8CharacterPassword(), true);
		assertEquals(forgotPasswordPage.validateMinimunOnenumberPassword(), true);
		assertEquals(forgotPasswordPage.validateUpperCaseValidationInPassword(), true);
		assertEquals(forgotPasswordPage.validateLowerCaseValidationInPassword(), true);
		assertEquals(forgotPasswordPage.validateSpecialCharacterValidationInPassword(), true);
		
//		String actualResult1 = changePasswordPage.validateNewPasswordRequiredField();
		
//		String expectedResult1 = "Please input your password!";
//		assertEquals(actualResult1, expectedResult1);

//		changePasswordPage.clearConfirmPassword();
//		String actualResult2 = changePasswordPage.validateConfirmPasswordRequiredField();
//		String expectedResult2 = "Please confirm your password!";
//		assertEquals(actualResult2, expectedResult2);
	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_Blank_Current_Password() {
     	changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);
		
		changePasswordPage.clearCurrentPassword();
		changePasswordPage.enterNewPassword("Test@1234");
		//changePasswordPage.enterConfirmPassword("test@1234");
		String actualResult = changePasswordPage.validateCurrentPasswordRequiredField();
		String expectedResult = "Please input your Current Password!";
		assertEquals(actualResult, expectedResult);
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(enabled=false, priority = 5, retryAnalyzer = Retry.class)
	public void verify_Wrong_Current_Password_And_Missmatch_New_Password_And_Confirm_Password() {
//		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);

		changePasswordPage.enterCurrentPassword("test@100");
		changePasswordPage.enterNewPassword("test@1234");
		changePasswordPage.enterConfirmPassword("test@12345");
		String actualResult = changePasswordPage.validateMissMatchNewAndConfirmPassword();
		String expectedResult = "New Password and confirm password does not matched!";
		assertEquals(actualResult, expectedResult);
		assertEquals(actualResult, expectedResult);
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(enabled=false,priority = 6, retryAnalyzer = Retry.class)
	public void verify_Correct_Current_Password_And_Missmatch_New_Password_And_Confirm_Password() {
//		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);

		changePasswordPage.enterCurrentPassword("test@123");
		changePasswordPage.enterNewPassword("test@1234");
		changePasswordPage.enterConfirmPassword("test@12345");
		String actualResult = changePasswordPage.validateMissMatchNewAndConfirmPassword();
		String expectedResult = "New Password and confirm password does not matched!";
		assertEquals(actualResult, expectedResult);
		assertEquals(actualResult, expectedResult);
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_Proper_Current_Password_New_Password_with_different_validation() {
		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);

		changePasswordPage.enterCurrentPassword(signUpExcel.getdata(0, 27, 2));
//		changePasswordPage.enterNewPassword("test");
//		//changePasswordPage.enterConfirmPassword("test");
//		String actualResult = changePasswordPage.newPasswordLessThan8CharactersValidation();
//		String expectedResult = "New Password is too short.";
//		assertEquals(actualResult, expectedResult);
//		assertEquals(actualResult, expectedResult);
		
		changePasswordPage.enterNewPassword(excel.getdata(0, 20, 2));
		//forgotPasswordPage.enterConfirmPassword(excel.getdata(0, 11, col));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateMinimun8CharacterPassword(), true);
		
		changePasswordPage.clearNewPassword();
		changePasswordPage.enterNewPassword(excel.getdata(0, 20, 3));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateMinimunOnenumberPassword(), true);
		
		changePasswordPage.clearNewPassword();
		changePasswordPage.enterNewPassword(excel.getdata(0, 20, 4));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateUpperCaseValidationInPassword(), true);
		
		changePasswordPage.clearNewPassword();
		changePasswordPage.enterNewPassword(excel.getdata(0, 20, 5));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateLowerCaseValidationInPassword(), true);
		
		changePasswordPage.clearNewPassword();
		changePasswordPage.enterNewPassword(excel.getdata(0, 20, 6));
		Common.pause(1);
		assertEquals(forgotPasswordPage.validateSpecialCharacterValidationInPassword(), true);
		
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_Proper_Current_Password_Blank_New_Password() {
		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);
		
		changePasswordPage.enterCurrentPassword(signUpExcel.getdata(0, 27, 2));
		changePasswordPage.clearNewPassword();
		//changePasswordPage.clearConfirmPassword();
//		String actualResult = changePasswordPage.validateNewPasswordRequiredField();
//		String expectedResult = "Please input your password!";
//		assertEquals(actualResult, expectedResult);
//		String actualResult1 = changePasswordPage.validateConfirmPasswordRequiredField();
//		String expectedResult1 = "Please confirm your password!";
//		assertEquals(actualResult1, expectedResult1);
		assertEquals(forgotPasswordPage.validateMinimun8CharacterPassword(), true);
		assertEquals(forgotPasswordPage.validateMinimunOnenumberPassword(), true);
		assertEquals(forgotPasswordPage.validateUpperCaseValidationInPassword(), true);
		assertEquals(forgotPasswordPage.validateLowerCaseValidationInPassword(), true);
		assertEquals(forgotPasswordPage.validateSpecialCharacterValidationInPassword(), true);
		boolean btn = changePasswordPage.saveButtonIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_Successfully_Change_Password_Validate_Old_Password_Login_With_New_Password() {
//		changePassword(webAppMainUserLogin);
		driverWebApp1.navigate().refresh();
		Common.pause(3);
		
		changePasswordPage.enterCurrentPassword(signUpExcel.getdata(0, 27, 2));
		changePasswordPage.enterNewPassword("Test@12345");
	//	changePasswordPage.enterConfirmPassword("test@12345");
		Common.pause(1);
		changePasswordPage.clickOnSaveButton();
//		String actualResult = changePasswordPage.changePasswordReloginValidation();
//		String expectedResult = "You password has been updated, please relogin";
//		assertEquals(actualResult, expectedResult);
		String actualResult1 = loginpage.getTitle();
		String expectedResult1 = "Login | Callhippo.com";
		assertEquals(actualResult1, expectedResult1);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signUpExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		String actualResult2 = loginpage.validationErrorMessage();
		String expectedResult2 = "Incorrect email or password";
		assertEquals(actualResult2, expectedResult2);
		Common.pause(1);
		loginpage.enterEmail(email);
		loginpage.enterPassword("Test@12345");
		loginpage.clickOnLogin();
		String actualResult3 = loginpage.loginSuccessfully();
		String expectedResult3 = "Dashboard | Callhippo.com";
		assertEquals(actualResult3, expectedResult3);
	}
}