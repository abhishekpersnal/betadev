package com.CallHippo.web.authentication;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class ChangePasswordPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	WebDriver driver1;
	PropertiesFile credential;
	
	@FindBy(xpath = "//h1[@class='pageTitle']")
	WebElement pageTitle;
	@FindBy(xpath = "//p[contains(.,'Update your account password. Always set a strong password, which helps to prevent unauthorized access to your account.')]")
	WebElement pageTitleDescription;	
	@FindBy(xpath = "//input[@placeholder='Enter your current password']")
	WebElement currentPassword;
	@FindBy(xpath = "//input[@placeholder='Enter your new password']")
	WebElement newPassword;
	@FindBy(xpath = "//input[@placeholder='Confirm password']")
	WebElement confirmPassword;
	@FindBy(xpath="//button[contains(.,'Save')]")
	WebElement saveButton;
	//@FindBy(xpath = "//input[@placeholder='Enter your current password']//following-sibling::div[contains(.,'Please input your Current Password!')]")
	@FindBy(xpath = "//div[@class='ant-form-explain'][contains(.,'Please input your Current Password!')]")
	WebElement requireCurrentPasswordValidation;
	//@FindBy(xpath = "//input[@placeholder='Enter your new password']//following-sibling::div[contains(.,'Please input your password!')]")
	@FindBy(xpath = "//div[@class='ant-form-explain'][contains(.,'Please input your password!')]")
	WebElement requireNewPasswordValidation;
	@FindBy(xpath = "//div[@class='ant-form-explain'][contains(.,'Please confirm your password!')]")
	//@FindBy(xpath = "//input[@placeholder='Confirm password']//following-sibling::div[contains(.,'Please input your Confirm Password!')]")
	WebElement requireConfirmPasswordValidation;
	@FindBy(xpath = "//div[@class='ant-form-explain'][contains(.,'New Password and confirm password does not matched!')]")
	WebElement missMatchNewAndConfirmPasswordValidation;
	@FindBy(xpath = "//div[@class='ant-form-explain'][contains(.,'New Password is too short.')]")
	WebElement newPasswordLessThan8CharactersValidation;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement validationReloginMsg;


	public ChangePasswordPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		action = new Actions(this.driver);
		
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}	
	public String validatePageTitle() {
		wait.until(ExpectedConditions.visibilityOf(pageTitle));
		return pageTitle.getText();
	}
	
	public String validatePageTitleDescription() {
		wait.until(ExpectedConditions.visibilityOf(pageTitleDescription));
		return pageTitleDescription.getText();
	}
	
	public boolean validateCurrentPasswordTextBoxIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(currentPassword));
		return currentPassword.isDisplayed();
	}
	
	public boolean validateNewPasswordTextBoxIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(newPassword));
		return newPassword.isDisplayed();
	}
	
	public boolean validateConfirmTextBoxIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(confirmPassword));
		return confirmPassword.isDisplayed();
	}
	public boolean saveButtonIsNotClickable() {

		try {
			Thread.sleep(1000);

			//WebElement w = isclickablesignup;
			driver.findElement(By.xpath("//button[contains(.,'Save')][contains(@disabled.,'')]"));

			return true;
		} catch (Exception e) {

			return false;
		}
	}

	public void clearCurrentPassword() {
		wait.until(ExpectedConditions.visibilityOf(currentPassword));
		currentPassword.sendKeys("a");
		currentPassword.sendKeys(Keys.BACK_SPACE);
		currentPassword.clear();
	}

	public void clearNewPassword() {
		wait.until(ExpectedConditions.visibilityOf(newPassword));
		newPassword.sendKeys("b");
		newPassword.sendKeys(Keys.BACK_SPACE);
		newPassword.clear();
	}
	public void clearConfirmPassword() {
		wait.until(ExpectedConditions.visibilityOf(confirmPassword));
		confirmPassword.sendKeys("c");
		confirmPassword.sendKeys(Keys.BACK_SPACE);
		confirmPassword.clear();	
	}
	
	public String validateCurrentPasswordRequiredField() {
		wait.until(ExpectedConditions.visibilityOf(requireCurrentPasswordValidation));
		return requireCurrentPasswordValidation.getText();
	}
	public String validateNewPasswordRequiredField() {
		wait.until(ExpectedConditions.visibilityOf(requireNewPasswordValidation));
		return requireNewPasswordValidation.getText();
	}
	public String validateConfirmPasswordRequiredField() {
		wait.until(ExpectedConditions.visibilityOf(requireConfirmPasswordValidation));
		return requireConfirmPasswordValidation.getText();
	}
	
	public String validateMissMatchNewAndConfirmPassword() {
		wait.until(ExpectedConditions.visibilityOf(missMatchNewAndConfirmPasswordValidation));
		return missMatchNewAndConfirmPasswordValidation.getText();
	}
	public String newPasswordLessThan8CharactersValidation() {
		wait.until(ExpectedConditions.visibilityOf(newPasswordLessThan8CharactersValidation));
		return newPasswordLessThan8CharactersValidation.getText();
	}	
	public void enterCurrentPassword(String currentPassword) {
		wait.until(ExpectedConditions.visibilityOf(this.currentPassword));
		this.currentPassword.sendKeys(currentPassword);
	}
	public void enterNewPassword(String newPassword) {
		wait.until(ExpectedConditions.visibilityOf(this.newPassword));
		this.newPassword.sendKeys(newPassword);
	}
	
	public void enterConfirmPassword(String confirmPassword) {
		wait.until(ExpectedConditions.visibilityOf(this.confirmPassword));
		this.confirmPassword.sendKeys(confirmPassword);
	}
	
//	public String validateChangePasswordSuccessfully() {
//		wait.until(ExpectedConditions.visibilityOf(changePasswordSuccessfully));
//		return changePasswordSuccessfully.getText();
//	}
	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveButton));
		saveButton.click();
	}
	
	public String changePasswordReloginValidation() {
		wait.until(ExpectedConditions.visibilityOf(validationReloginMsg));
		return driver
				.findElement(By.xpath(
						"//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'You password has been updated, please relogin')]"))
				.getText();

	}

}
	
