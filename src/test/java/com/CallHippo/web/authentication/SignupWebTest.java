package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.*;
import com.CallHippo.Init.TestBase;

public class SignupWebTest {
	SignupPage registrationPage;
	SignupPage dashboard;
	LoginPage loginpage;
	WebDriver driver;
	WebDriver driverMail;
	static Common excel;
	PropertiesFile url;
	String gmailUser;
	WebToggleConfiguration webAppMainUserLogin;

	public SignupWebTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(url.getValue("environment")+"\\Signup.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			try {
				dashboard.deleteAllMail();
			}catch(Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail," Gmail issue ","BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}
	
	@BeforeMethod
	public void initialization() throws Exception {
		try {
			driver = TestBase.init();
			try {
				driver.get(url.livesignUp());
			}catch(Exception e) {
				driver.get(url.livesignUp());
			}
			registrationPage = PageFactory.initElements(driver, SignupPage.class);
			dashboard = PageFactory.initElements(driver, SignupPage.class);
			loginpage = PageFactory.initElements(driver, LoginPage.class);
			webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
			
		} catch (Exception e) {
			Common.Screenshot(driver," Web SignupTest - Fail ","BeforeMethod - initialization");
			System.out.println("----Need to check issue - SignupTest - BeforeMethod - initialization----");
		} 
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driver.quit();
	}
	
	@DataProvider(name = "FirstNameValidation")
	public static Object[][] invalidFirstName() {
				return new Object[][] { { excel.getdata(0, 7, 2) }, { excel.getdata(0, 7, 4) },
					{ excel.getdata(0, 7, 5) } };
	}
	@Test(priority = 1, dataProvider = "FirstNameValidation", retryAnalyzer = Retry.class)
		public void verify_firstName_validation(String firstname) throws Exception {
		
		System.out.println("----Welcome to signup");

		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(firstname);
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		boolean actualResult = registrationPage.firstNameValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);

	}
	
	@DataProvider(name = "LastNameValidation")
	public static Object[][] invalidLastName() {


				return new Object[][] { { excel.getdata(0, 7, 2) }, { excel.getdata(0, 7, 4) },
					{ excel.getdata(0, 7, 5) } };
	}
	@Test(priority = 2, dataProvider = "LastNameValidation", retryAnalyzer = Retry.class)
		public void verify_lastName_validation(String lastname) throws Exception {
		
		System.out.println("----Welcome to signup");

		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(lastname);
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		boolean actualResult = registrationPage.lastNameValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_email_validation_with_Invalid_emails() {
		System.out.println("----Welcome to signup");

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(excel.getdata(0, 9, 5));
		Common.pause(2);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		boolean actualResult = registrationPage.emailsValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void Verify_with_already_registered_email() {
		
		System.out.println("----Welcome to signup");

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(excel.getdata(0, 12, 2));
		Common.pause(2);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		String actualResult = registrationPage.alreadyregisteredEmailValidation();
		String expectedResult = "You are already registered with us, please login to continue.";
		assertEquals(actualResult, expectedResult);
		
	}

	@DataProvider(name = "NumbersValidation")
	public static Object[][] invalidMobileNumbers() {

		return new Object[][] { { excel.getdata(0, 8, 2) }, { excel.getdata(0, 8, 3) }, { excel.getdata(0, 8, 4) } };
	}

	@Test(priority = 5, dataProvider = "NumbersValidation", retryAnalyzer = Retry.class)
	public void verify_number_validation_with_Invalid_numbers(String number) throws Exception {
		
		System.out.println("----Welcome to signup");

		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(number);
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		boolean actualResult = registrationPage.phoneNumberValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
			
	}

	@DataProvider(name = "PasswordsValidation")
	public static Object[][] invalidPasswords() {

		return new Object[][] { { excel.getdata(0, 10, 2) }, { excel.getdata(0, 10, 3) }, { excel.getdata(0, 10, 4) },
				{ excel.getdata(0, 10, 5) } };
	}

	@Test(priority = 6, dataProvider = "PasswordsValidation", retryAnalyzer = Retry.class)
	public void verify_password_validation_with_Invalid_passwords(String pass) throws Exception {
		
		System.out.println("----Welcome to signup");

		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(pass);
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		boolean actualResult = registrationPage.passwordsValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_Sign_Up_popup () {
		
		System.out.println("----Welcome to signup");

		registrationPage.validate_website_signUp_popup_by_signUp_btn();
		registrationPage.validate_website_signUp_popup_by_trynow_btn();
		
	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_SignUp_wit_valid_credential() throws Exception {
		System.out.println("----Welcome to signup");
		
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
		
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	