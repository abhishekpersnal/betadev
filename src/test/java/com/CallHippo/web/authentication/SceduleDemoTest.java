package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import com.CallHippo.Dialer.WebToggleConfiguration;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.TestBase;

public class SceduleDemoTest {
	WebDriver driverWebApp;
	WebToggleConfiguration webApp2;
	PropertiesFile url= new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data= new PropertiesFile("Data\\url Configuration.properties");
    Common excelTestResult;

	public SceduleDemoTest() throws Exception {
		
		
	}
	
	String account2EmailMainUser = data.getValue("account2MainEmail");
	String account2PasswordMainUser = data.getValue("masterPassword");
	
	@BeforeTest
	public void initialization() throws Exception {

		driverWebApp = TestBase.init();
		System.out.println("Opened Webapp session");
		webApp2 = PageFactory.initElements(driverWebApp, WebToggleConfiguration.class);
		
		try {

			try {
				driverWebApp.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			} catch (Exception e) {
				driverWebApp.get(url.signIn());
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}
			
		} catch (Exception e1) {
			String testname = "GlobalConnect Before Test ";
			Common.Screenshot(driverWebApp, testname, "PhoneAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}
	@BeforeMethod
	public void login() throws IOException, InterruptedException {
	try {
		  try {
			Common.pause(2);
			driverWebApp.get(url.signIn());			
			Common.pause(3);
			
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}
		
		} catch (Exception e) {

			Common.pause(2);
			driverWebApp.get(url.signIn());			
			Common.pause(3);
			
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2EmailMainUser, account2PasswordMainUser);
				Common.pause(3);
			}
			
		} 
	}catch (Exception e2) {
			String testname = "GlobalConnect Before Method ";
			Common.Screenshot(driverWebApp, testname, "PhoneAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp, testname, "PhoneApp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
			
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp, testname, "PhoneApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			
			
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driverWebApp.quit();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_ScheduleDemo_button() throws Exception {
		webApp2.clickOnScheduleDemo();
		Common.pause(5);
		
		webApp2.switchToWindowBasedOnTitle("Schedule A Demo- CallHippo");
		System.out.println(webApp2.validateScheduleDemoPageTitle());
		assertEquals(webApp2.validateScheduleDemoPageTitle(),"Schedule A Demo- CallHippo");
		Common.pause(5);
		//webApp2.validateLandingScheduleDemoPage();
		
	}
}
