package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.awt.image.CropImageFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class SignupPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	PropertiesFile credential;
	SoftAssert softAssert;

	@FindBy(xpath = "//input[contains(@placeholder,'Full Name')]")
	private WebElement fullname;
	@FindBy(xpath = "//input[contains(@placeholder,'Company Name')]")
	private WebElement companyName;
	@FindBy(xpath = "//input[contains(@placeholder,'Mobile Number')]")
	private WebElement mobileNumber;
	@FindBy(xpath = "//input[contains(@placeholder,'Email')]")
	private WebElement email;
	@FindBy(xpath = "//input[contains(@placeholder,'Password')]")
	private WebElement password;
	@FindBy(xpath = "//div[@class[contains(.,'chSignInWrapper')]]/button[contains(.,'SignUp')]")
	private WebElement signup;
	@FindBy(xpath = "//button[contains(.,'SignUp')][contains(@disabled,'')]")
	private WebElement isclickablesignup;

	// validation xpaths

	@FindBy(xpath = "//span[@class='help-block'][contains(.,'Enter a valid Mobile Number')]")
	private WebElement numberValidation;
	@FindBy(xpath = "//div[@class='selected-flag']")
	private WebElement flag;

	// Gamil xpaths
	@FindBy(xpath = "//input[@id='identifierId']")
	private WebElement gemail;
	@FindBy(xpath = "//*[@id=\"identifierNext\"]/div/button")
	private WebElement gemailNext;
	@FindBy(xpath = "//input[@name='password']")
	private WebElement gpassword;
	@FindBy(xpath = "//*[@id=\"passwordNext\"]/div/button")
	private WebElement gpasswordNext;
	@FindBy(xpath = "//span[contains(text(),'CallHippo | Confirm Your Email')]")
	private WebElement mailtitle;
	@FindBy(xpath = "//a[contains(text(),'Verify')]")
	private WebElement verifybtn;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Your account has been verified. Login to access your account.')]")
	private WebElement accountVerified;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Sorry this confirmation request has expired. Please request for a new confirmation link.')]")
	private WebElement linkExpired;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement validationMsg;

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span")
	private WebElement messageInfo;

	@FindBy(xpath = "//button[@class='ngdialog-close']")
	private WebElement closeDialog;
	@FindBy(xpath = "//li[@class='isoUser']")
	private WebElement userDropdown;
	@FindBy(xpath = "//span[@class='isoDropdownLink'][contains(.,'Logout')]")
	WebElement logout;

	@FindBy(xpath = "//button[@class[contains(.,'addnumber')]][contains(.,'Add Number')]") // button[contains(.,'End
																							// Tour')]
	WebElement addNumber_popup_btn;

	@FindBy(xpath = "//h3[contains(.,'Welcome to CallHippo!!')]")
	WebElement welcome_popup_msg;

	@FindBy(xpath = "//button[@class='endtourbtn'][contains(.,'End Tour')]")
	WebElement endTour_btn;

	@FindBy(xpath = "//img[@src[contains(.,'gsuite')]]")
	WebElement gsuiteImg;

	// SignUp xpath from website

	@FindBy(xpath = "//span[contains(.,'Sign Up Now')]")
	List<WebElement> website_signUp_Btn;
	
	@FindBy(xpath = "//button[contains(.,'Try Now')] | //span[contains(.,'Try Now')]")
	List<WebElement> website_try_Now_Btn;
	
	@FindBy(xpath = "//div[@class='modal-title h4']")
	WebElement popup_title;
	
	@FindBy(xpath = "//button[@class='close']")
	WebElement popup_close_btn;
	
	@FindBy(xpath = "//div[contains(@class,'chmainnav')]//button[contains(.,'Sign Up')]")
	WebElement website_header_signUp_Btn;

	@FindBy(xpath = "//button[contains(.,'Sign up now!')]/ancestor::div[contains(@class,'popup')]//div[contains(text(),'Get Started Now!')]")
	WebElement website_signUp_popUp_header;

	@FindBy(xpath = "//label[contains(.,'First Name')]//following-sibling::input[@name='firstName']")
	WebElement website_signUp_popUp_firstName;

	@FindBy(xpath = "//label[contains(.,'Last Name')]//following-sibling::input[@name='lastName']")
	WebElement website_signUp_popUp_lastName;

	@FindBy(xpath = "//label[contains(.,'Company Name')]//following-sibling::input[@name='companyName']")
	WebElement website_signUp_popUp_companyName;

	@FindBy(xpath = "//label[contains(.,'Work Email')]//following-sibling::input[@name='email']")
	WebElement website_signUp_popUp_workEmail;

	@FindBy(xpath = "//label[contains(.,'Phone Number')]/following-sibling::div//div[@class='selected-flag']")
	WebElement website_signUp_popUp_phone_countryflag;

	@FindBy(xpath = "//label[contains(.,'Phone Number')]//following-sibling::div//input[@name='mobileNo']")
	WebElement website_signUp_popUp_phoneNumberTxt;

	@FindBy(xpath = "//label[contains(.,'Password')]//following-sibling::input[@name='password']")
	WebElement website_signUp_popUp_password;
	
	@FindBy(xpath = "//button[@type='submit'][contains(.,'Sign up now!')]")
	WebElement website_signUp_popUp_signUpNow_Btn;

	// changeEmail
	
	@FindBy(xpath = "//h1[contains(@class,'mainTitle')]")
	WebElement mainTitle;

	@FindBy(xpath = "//a[@class='activationemailchange']/span[@class='changeemailpopover']")
	WebElement btnChangeYourEmail;

	@FindBy(xpath = "//a[@class='activationemailchange']/span[text()='Resend Activation']")
	WebElement btnResendActivation;

	@FindBy(xpath = "//button[@type='button'][contains(.,'Cancel')]")
	WebElement btnCancel;

	@FindBy(xpath = "//button[@type='submit'][contains(.,'Send Activation')]")
	WebElement btnSendActivation;

	@FindBy(xpath = "//label[@title='E-mail']/parent::div/following-sibling::div//input[@id='email']")
	WebElement txtEmailForheader;
	@FindBy(xpath = "//input[contains(@placeholder,'Email')]")
	WebElement txtEmailForProfile;
	@FindBy(xpath = "//div[contains(@class,'ant-form-explain')]")
	WebElement validationInvalidInput;

	@FindBy(xpath = "//div[@class=\"ant-message-custom-content ant-message-error\"]//span")
	WebElement validationErrorMessage;

	@FindBy(xpath = "//div[@class=\"ant-message-custom-content ant-message-success\"]//span")
	WebElement validationSucessMessage;
	@FindBy(xpath = "//span[@class='usrname_topbar']")
	WebElement userProfile;
	@FindBy(xpath = "//a[@href='/userProfile']")
	WebElement userProfilelink;
	@FindBy(xpath = "//button[@data-rtl='ltr'][contains(.,'Save')]")
	WebElement btnSAVE;

	public SignupPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		action = new Actions(this.driver);
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
		softAssert = new SoftAssert();
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}

	public String getTitle() {
		String signupTitle = driver.getTitle();
		return signupTitle;
	}

	public void validate_website_signUp_popup_by_signUp_btn() {
		System.out.println("website_signUp_Btn size:" + website_signUp_Btn.size());
		if (website_signUp_Btn.size() > 0) {
			for (int i = 0; i < website_signUp_Btn.size(); i++) {
				website_signUp_Btn.get(i).click();
				Common.pause(1);
				assertEquals(popup_title.getText(), "Get Started Now!");
				assertEquals(website_signUp_popUp_signUpNow_Btn.isDisplayed(), true);
				popup_close_btn.click();
			}
		}

	}

	public void validate_website_signUp_popup_by_trynow_btn() {
		Common.pause(2);
		List<WebElement> website_signUp_popUp_signUpNow_Btn1 = driver
				.findElements(By.xpath("//button[@type='submit'][contains(.,'Sign up now!')]"));
		if (website_try_Now_Btn.size() > 0) {
			for (int i = 1; i <= website_try_Now_Btn.size(); i++) {
				WebElement trynow = driver.findElement(
						By.xpath("(//button[contains(.,'Try Now')] | //span[contains(.,'Try Now')])[" + i + "]"));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", trynow);
				Common.pause(1);
				softAssert.assertEquals(popup_title.getText(), "Get Started Now!", "Text is mismatch at index:" + i);
				softAssert.assertEquals(website_signUp_popUp_signUpNow_Btn1.size() > 0, true,"signUpNow_Btn is not found at index:" + i);
				popup_close_btn.click();
				softAssert.assertAll();
			}
		}

	}

	public void clickOn_website_header_signUp_Btn() {
		website_header_signUp_Btn.click();
	}

	public void enter_website_signUp_popUp_firstName(String firstName) {
		website_signUp_popUp_firstName.sendKeys(firstName);
	}

	public void enter_website_signUp_popUp_lastName(String lastName) {
		website_signUp_popUp_lastName.sendKeys(lastName);
	}

	public void enter_website_signUp_popUp_companyName(String companyName) {
		website_signUp_popUp_companyName.sendKeys(companyName);
	}

	public String getTextMainTitle() {
		return mainTitle.getText();
	}

	public void enter_website_signUp_popUp_workEmail(String workEmail) {
		website_signUp_popUp_workEmail.sendKeys(workEmail);
	}

	public void enter_website_signUp_popUp_phoneNumber(String phoneNumber) {

		Actions action = new Actions(driver);

		action.moveToElement(website_signUp_popUp_phone_countryflag).click().perform();

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.xpath("//label[contains(.,'Phone Number')]/following-sibling::div//div[@class='selected-flag']"))))
				.click();
		Common.pause(2);
		List<WebElement> number= driver.findElements(By.xpath(
				"(//label[contains(.,'Phone Number')]/following-sibling::div//div[@class='selected-flag']/following-sibling::ul/li[@data-dial-code=1]/span[contains(.,'United States')])[1]"));
		if(number.size()>0) {
			//number.get(0).click();
			website_signUp_popUp_phoneNumberTxt.click();
			Common.pause(3);
			website_signUp_popUp_phoneNumberTxt.clear();
			website_signUp_popUp_phoneNumberTxt.sendKeys(phoneNumber);
			Common.pause(1);
		}
		

	}

	public void enter_website_signUp_popUp_password(String password) {
		website_signUp_popUp_password.sendKeys(password);
	}

	public void clickOn_website_signUp_popUp_signUpButton() {
		website_signUp_popUp_signUpNow_Btn.click();
	}

	public void enterFullname(String fullName) {
		fullname.sendKeys(fullName);

	}

	public void enterCompanyName(String companyName) {
		this.companyName.sendKeys(companyName);
	}

	public void enterMobile(String mobileNumber) {
		this.mobileNumber.sendKeys(mobileNumber);
	}

	public void enterEmail(String email) {
		this.email.sendKeys(email);
	}

	public void enterPassword(String password) {
		this.password.sendKeys(password);
	}

	public boolean signupIsClickable() {

		try {
			Thread.sleep(1000);

			WebElement w = isclickablesignup;
			driver.findElement(By.xpath("//button[contains(.,'SignUp')][contains(@disabled,'')]"));

			return true;
		} catch (Exception e) {

			return false;
		}

	}

	public boolean numberValidation() {

		try {
			driver.findElement(By.xpath(
					"//input[contains(@placeholder,'Mobile Number')]//following-sibling::div[contains(.,'Enter a valid Mobile Number')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void selectFlag() {
		flag.click();
		action.sendKeys(Keys.ARROW_DOWN).build().perform();
		action.sendKeys(Keys.ENTER).build().perform();

	}

	public String getFlagTitle() {

		return flag.getAttribute("title");

	}

	public boolean emailValidation() {

		try {
			driver.findElement(By.xpath(
					"//input[contains(@placeholder,'Email')]//following-sibling::div[contains(.,'Enter valid email.')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean passwordValidation() {

		try {
			driver.findElement(By.xpath(
					"//input[contains(@placeholder,'Password')]//following-sibling::div[contains(text(),'Password is too short.')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void clickOnSignupButton() {

		signup.click();
		// wait_angular5andjs();
	}

	public boolean fullNameValidation() {

		try {
			driver.findElement(By.xpath(
					"//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'User name cannot be less than 5 characters.')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean firstNameValidation() {

		try {
			driver.findElement(By.xpath("//span[@class='errorSpan'][contains(.,'Enter valid First Name')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean lastNameValidation() {

		try {
			driver.findElement(By.xpath("//span[@class='errorSpan'][contains(.,'Enter valid Last Name')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean phoneNumberValidation() {

		try {
			driver.findElement(By.xpath("//span[@class='errorSpan'][contains(.,'Enter valid Phone Number')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean passwordsValidation() {

		try {
			driver.findElement(
					By.xpath("//span[@class='errorSpan'][contains(.,'Password is too short (Minimum 8 characters)')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean emailsValidation() {

		try {
			driver.findElement(By.xpath("//span[@class='errorSpan'][contains(.,'Enter valid Work Email')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public String alreadyregisteredEmailValidation() {
		return driver.findElement(By.xpath(
				"//span[@class='responseMsg'][contains(.,'You are already registered with us, please login to continue.')]"))
				.getText();

	}

	public String blockedEmailValidation() {
		wait.until(ExpectedConditions.visibilityOf(validationMsg));
		return validationMsg.getText();

	}

	public String registeredEmailValidation() {
		wait.until(ExpectedConditions.visibilityOf(validationMsg));
		return driver.findElement(By.xpath(
				"//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'You are already registered with us, please login to continue.')]"))
				.getText();

	}

	public boolean nonRegisteredEmailValidation() {

		try {
			wait.until(ExpectedConditions.visibilityOf(messageInfo));

			driver.findElement(By.xpath(
					"//div[@class[contains(.,'ant-message-info')]]/span[contains(.,'We have sent you an email with instructions to activate your account.')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void gmailLogin() {
		driver.navigate().to("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gemail.sendKeys(credential.gmailId());
		} catch (IOException e1) {

		}
		gemailNext.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		try {
			gpassword.sendKeys(credential.gmailPassword());
		} catch (IOException e1) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
		wait.until(ExpectedConditions.stalenessOf(driver
				.findElements(
						By.xpath("//span[contains(@class,'bqe')][contains(text(),'Please verify your email address')]"))
				.get(1)));
		driver.findElements(
				By.xpath("//span[contains(@class,'bqe')][contains(text(),'Please verify your email address')]")).get(1)
				.click();
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
		verifybtn.click();

		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver.close();
			}
		}

	}

	// ---------------------------Start Gmail Login functions ---------------------

	public void validateMagicLickExpired() {
		gloginGmail();
		openMail();
		gClickOnMagicLoginlink();
		// ggetTitle();

	}

	public void validateClickOnExpireLinkFromGmail() {
		gloginGmail();
		openMail();
		gClickOnRecentMagicLoginlink();
		ggetTitle3();
	}

	public void ValidateVerifyButtonOnGmail() {
		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail();
		gClickOnRecentMagicLoginlink();
		ggetTitle();
	}
	
	public void validateVerifyButtonOnGmail() {
		Common.pause(5);
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        List<WebElement> gsuiteImg1 =driver.findElements(By.xpath("//img[@src[contains(.,'gsuite')]]"));
        System.out.println("gsuiteImg1 size----"+gsuiteImg1.size());
		Common.pause(10);
			if (gsuiteImg1.size()>0) { 
				Common.pause(3);
				openMail();
				gClickOnRecentMagicLoginlink();
				ggetTitle();
				
			}else {
				Common.pause(3);
				gloginGmail();
				openMail();
				gClickOnRecentMagicLoginlink();
				ggetTitle();
			}
		
	}

	public void closeTab() {
		driver.close();

	}

	public void ValidateVerificationMailOnGmail() throws InterruptedException {
		Thread.sleep(5000);
		System.out.print("go for gmail login ");
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail();
		Thread.sleep(5000);
		logoutGmail();

		ggetTitleForUserProfile();

	}

	public void ValidateVerifyButtonOnGmail2() {
		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail3();
		gClickOnRecentMagicLoginlink();
		ggetTitle();
	}

	public void ValidateVerifyButtonOnGmailForUserProfile() {
		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail();
		gClickOnRecentMagicLoginlink();
		ggetTitleForUserProfile();
	}

	public void ValidateVerifyButtonOnGmailForheader() {
		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail();
		gClickOnExpiryMagicLoginlink();
		ggetTitle();
	}

	public void ValidateVerifyButtonOnGmailForUserProfile1() {
		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail();
		gClickOnExpiryMagicLoginlink();
		ggetTitleForUserProfile();
	}

	public void ValidateVerifyButtonOnGmail1() {
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		try {
			if (gsuiteImg.isDisplayed()) {
				DonotLoginGmailAndConfirmMail();
			} else {
				gloginGmail();
				openMail();
				gClickOnRecentMagicLoginlink();
				ggetTitle();
			}
		} catch (Exception e) {
			gloginGmail();

			openMail();
			gClickOnRecentMagicLoginlink();
			ggetTitle();
		}
	}

	public void DonotLoginGmailAndConfirmMail() {
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		Common.pause(3);
		openMail();
		gClickOnRecentMagicLoginlink();
		switchToWindowBasedOnTitle("Dashboard | Callhippo.com");
	}

	public void getGmailURL() {
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		driver.navigate().refresh();
	}

	public void gloginGmail() {
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gemail.sendKeys(credential.gmailId());
		} catch (IOException e1) {
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		gemailNext.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gpassword.sendKeys(credential.gmailPassword());
		} catch (IOException e1) {
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void gClickOnRecentMagicLoginlink() {

		int attempt = 0;
		int MAX_ATTEMPTS = 5;

		while (attempt < MAX_ATTEMPTS) {
			try {
				wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath("//a[contains(text(),'Verify')]"))));
				int size = driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).size();
				System.out.println(size);

				if (size < 2) {
					driver.findElement(By.xpath("//a[contains(text(),'Verify')]")).click();
				} else {
					Common.pause(1);
					driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).get(size - 1).click();

				}
				break;
			} catch (StaleElementReferenceException e) {
				System.out.println(attempt);
				// if ( error.message.includes( "StaleElementReference" ) )
				attempt++;
			}
		}
	}

	public void gClickOnExpiryMagicLoginlink() {

		int attempt = 0;
		int MAX_ATTEMPTS = 5;

		while (attempt < MAX_ATTEMPTS) {
			try {
				wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath("//a[contains(text(),'Verify')]"))));
				int size = driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).size();
				System.out.println(size);

				if (size >= 2) {
					Common.pause(1);
					driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).get(size - 2).click();
				}
				// else {
//					Common.pause(1);
//					driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).get(size).click();
//					
//				}
				break;
			} catch (StaleElementReferenceException e) {
				System.out.println(attempt);
				// if ( error.message.includes( "StaleElementReference" ) )
				attempt++;
			}
		}
	}

	public void gClickOnMagicLoginlink() {
		int attempt = 0;
		int MAX_ATTEMPTS = 5;

		while (attempt < MAX_ATTEMPTS) {
			try {
				wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath("//a[contains(text(),'Verify')]"))));
				int size = driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).size();
				System.out.println(size);

				if (size > 0) {
					driver.findElement(By.xpath("//a[contains(text(),'Verify')]")).click();
				} else {
					Common.pause(1);
					driver.findElements(By.xpath("//a[contains(text(),'Verify')]")).get(size - 1).click();

				}
				break;
			} catch (StaleElementReferenceException e) {
				System.out.println(attempt);
				// if ( error.message.includes( "StaleElementReference" ) )
				attempt++;
			}
		}
	}

	public void ggetTitle() {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				wait.until(ExpectedConditions.titleIs("Dashboard | Callhippo.com"));
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}

	public void ggetTitlebasedOnTitle1() {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window("Users | Callhippo.com");
				// wait.until(ExpectedConditions.titleIs("Dashboard | Callhippo.com"));
				// System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}

	public void ggetTitleFoEmail() {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				// wait.until(ExpectedConditions.titleIs("Please verify your email address"));
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}

	public void ggetTitleForUserProfile() {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				wait.until(ExpectedConditions.titleIs("Users | Callhippo.com"));
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}

	public void ggetTitle2() {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				wait.until(ExpectedConditions.titleIs("Login | Callhippo.com"));
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}

	public void ggetTitle3() {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		int i = 0;
		while (itr.hasNext()) {
			String childWindow = itr.next();
			// System.out.println(childWindow);
			driver.switchTo().window(childWindow);
			if (driver.getTitle().contentEquals("Login | Callhippo.com")) {
				break;
			}

			Common.pause(1);
			i = i + 1;

			if (i == 20) {
				break;
			}
		}
	}

	public void switchToWindowBasedOnTitle(String title) {
		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		int i = 0;
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			driver.switchTo().window(childWindow);
			if (driver.getTitle().contains(title)) {
				break;
			}

			Common.pause(1);
			i = i + 1;

			if (i == 20) {
				break;
			}
		}
	}

	public void openMail() {
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By.xpath(
							"//span[contains(@class,'bqe')][contains(text(),'Please verify your email address')]"))
							.get(1).click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(driver.findElements(By.xpath(
							"//span[contains(@class,'bqe')][contains(text(),'Please verify your email address')]"))
							.get(1)));
					driver.findElements(By.xpath(
							"//span[contains(@class,'bqe')][contains(text(),'Please verify your email address')]"))
							.get(1).click();
				}

				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	// ---------------------------Start Gmail Login functions ---------------------

	public boolean accountVerifiedMsg() {

		try {
			driver.findElement(By.xpath(
					"//span[@ng-bind-html='message.content'][contains(.,'Your account has been verified. Login to access your account.')]"));
			return true;

		} catch (Exception e) {
			// TODO Auto-generated catch block
		}

		return false;
	}

	public String accountLinkExpiredMsg() {
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@class[contains(.,'ant-message-error')]]/span")));
		wait.until(ExpectedConditions.visibilityOf(validationMsg));
		return validationMsg.getText();
	}

	public void closeAddnumberPopup() {

		// wait_angular5andjs();
		Common.pause(1);
		action.sendKeys(Keys.ESCAPE).build().perform();
		Common.pause(1);
		// closeDialog.click();
	}

	public void userLogout() {
		wait.until(ExpectedConditions.visibilityOf(userDropdown));
		userDropdown.click();
		wait.until(ExpectedConditions.visibilityOf(logout));
		logout.click();
	}

	public boolean verifyWelcomeToCallHippoPopup() {
		boolean welcome = false;
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(endTour_btn));

		String actual_addNumber_msg_str = welcome_popup_msg.getText();
		String expected_addNumber_msg_str = "Welcome to CallHippo!!";// "Hey Unblocked User Welcome Aboard!";

		if (actual_addNumber_msg_str.contains(expected_addNumber_msg_str)) {
			Common.pause(2);
			endTour_btn.click();
			welcome = true;
		} else {
			welcome = false;
		}
		return welcome;
	}

	public boolean verifyWelcomeToCallHippoPopup1() {
		boolean welcome = false;
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(endTour_btn));

		String actual_addNumber_msg_str = welcome_popup_msg.getText();
		String expected_addNumber_msg_str = "Welcome to CallHippo!!";// "Hey Unblocked User Welcome Aboard!";

		if (actual_addNumber_msg_str.contains(expected_addNumber_msg_str)) {
			Common.pause(2);
			welcome = true;
		} else {
			welcome = false;
		}
		return welcome;
	}


	public void validateSecondLastInvitationButton() {
		// gloginGmail();
		openInvitationMail();
		gClickOnSecondLastRecentAcceptInvitationlink();
		ggetTitle3();
	}

	public void validateSecondLastInvitationButton1() {
		// gloginGmail();
		openInvitationMail();
		gClickOnRecentAcceptInvitationlink();
		ggetTitle3();
	}

	public void OpenInvitationMailForSecondUser() {
		// gloginGmail();
		openInvitationMailForSecondUser();
		gClickOnRecentAcceptInvitationlink();
		ggetTitle3();
	}

	public void validateRecentInvitationmail() {
		// gloginGmail();
		// openInvitationMail();
		gClickOnRecentAcceptInvitationlink();
		ggetTitle2();
	}

	public void openInvitationMail() {

		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		// driver.get("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By
							.xpath("//span[contains(@class,'bqe')][contains(text(),'invited you to join CallHippo')]"))
							.get(1).click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(driver
							.findElements(By.xpath(
									"//span[contains(@class,'bqe')][contains(text(),'invited you to join CallHippo')]"))
							.get(1)));
					driver.findElements(By
							.xpath("//span[contains(@class,'bqe')][contains(text(),'invited you to join CallHippo')]"))
							.get(1).click();
				}

				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	public void openInvitationMailForSecondUser() {

		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		// driver.get("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(text(),'invited you to join CallHippo')]")).get(1)
							.click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(
							driver.findElements(By.xpath("//span[contains(text(),'invited you to join CallHippo')]"))
									.get(1)));
					driver.findElements(By.xpath("//span[contains(text(),'invited you to join CallHippo')]")).get(1)
							.click();
				}

				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	public void gClickOnSecondLastRecentAcceptInvitationlink() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]"))));
		int size = driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).size();
		System.out.println(size);

		if (size < 2) {
			driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]")).click();
		} else {
			Common.pause(1);
			driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).get(size - 2).click();

		}
	}

	public void gClickOnRecentAcceptInvitationlink() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]"))));
		int size = driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).size();
		System.out.println(size);

		if (size < 2) {
			driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]")).click();
		} else {
			Common.pause(1);
			driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).get(size - 1).click();

		}
	}

	public void deleteAllMail() {
		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		Common.pause(5);

		deleteMail("Please verify your email address");
		deleteMail("CallHippo | Reset your password");
		deleteMail("CallHippo: Magic Link");
		deleteMail("Test Automation invited you to join CallHippo");
		deleteMail("undefined invited you to join CallHippo");
//		deleteMail("CallHippo | Missed call");
//		deleteMail("CallHippo: Received SMS");
//		deleteMail("CallHippo | Voicemail");
		deleteMail("Activity Feed Reports From CallHippo");
		Common.pause(3);
		logoutGmail();
	}
	public void deleteAllMailForSMS() {
		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		Common.pause(5);

		deleteMail("Please verify your email address");
		
		deleteMail("Test Automation invited you to join CallHippo");
		Common.pause(3);
		logoutGmail();
	}

	private boolean isGmailIDdisplayed() {

		try {
			return gemail.isDisplayed();
		} catch (Exception e) {
			return false;
		}

	}

	public void deleteMail(String title) {

		if (isGmailIDdisplayed() == true) {
			gloginGmail();
		}
		int attempts = 0;
		int size = 0;
		while (attempts < 2) {
			try {
				try {
					size = driver.findElements(By.xpath("//span[contains(.,'" + title
							+ "')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
							.size();
				} catch (Exception e) {
					try {
						// wait.until(ExpectedConditions.stalenessOf(
						// driver.findElements(By.xpath("//span[contains(.,'"+title+"')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']")).get(1)));
						driver.findElements(By.xpath("//span[contains(.,'" + title
								+ "')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))
								.size();
					} catch (Exception e1) {
						size = 0;
					}
				}

				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}

		if (size > 0) {
			for (int i = 0; i < size; i++) {
				driver.findElements(By.xpath("//span[contains(.,'" + title
						+ "')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']")).get(i)
						.click();
			}
			action.contextClick(driver.findElement(By.xpath("//span[contains(.,'" + title
					+ "')]/parent::div/parent::div/parent::div/parent::td/parent::tr//*[@role='checkbox']"))).build()
					.perform();
			action.contextClick().build().perform();
			for (int j = 1; j <= 6; j++) {
				action.sendKeys(Keys.ARROW_DOWN).build().perform();
			}

			action.sendKeys(Keys.ENTER).build().perform();

			Common.pause(1);

		}
		// wait.until(ExpectedConditions.jsReturnsValue("return document.readyState ==
		// 'complete'"));
	}

	public void logoutGmail() {
		driver.findElement(By.xpath("//img[@src[contains(.,'gsuite')]]")).click();

		Common.pause(1);

		driver.findElement(By.xpath("//a[contains(.,'Sign out')]")).click();
		Common.pause(2);
	}

	public void validateClickOnExpireLinkFromGmail1() {
		// gloginGmail();
		openMail1();
		gClickOnRecentMagicLoginlink();
		ggetTitle3();
	}

	public void openMail1() {
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(text(),'Please verify your email address')]")).get(1)
							.click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(
							driver.findElements(By.xpath("//span[contains(text(),'Please verify your email address')]"))
									.get(1)));
					driver.findElements(By.xpath("//span[contains(text(),'Please verify your email address')]")).get(1)
							.click();
				}
				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void openMail3() {
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElement(By.xpath("(//span[contains(text(),'welcome')])[2]")).click();
				} catch (Exception e) {
					wait.until(ExpectedConditions
							.stalenessOf(driver.findElement(By.xpath("(//span[contains(text(),'welcome')])[2]"))));

					driver.findElement(By.xpath("(//span[contains(text(),'welcome')])[2]")).click();
				}
				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void openMail2() {
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(text(),'Activity Feed Reports From CallHippo')]"))
							.get(1).click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(driver
							.findElements(By.xpath("//span[contains(text(),'Activity Feed Reports From CallHippo')]"))
							.get(1)));
					driver.findElements(By.xpath("//span[contains(text(),'Activity Feed Reports From CallHippo')]"))
							.get(1).click();
				}
				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void gClickOnActivityFeedReportslink() {

		int attempt = 0;
		int MAX_ATTEMPTS = 5;

		while (attempt < MAX_ATTEMPTS) {
			try {
				wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath("	//a[(text()='Activity Feed Reports')]"))));
				int size = driver.findElements(By.xpath("//a[(text()='Activity Feed Reports')]")).size();
				System.out.println(size);

				if (size > 0) {
					driver.findElement(By.xpath("//a[(text()='Activity Feed Reports')]")).click();
				} else {
					Common.pause(1);
					driver.findElements(By.xpath("//a[(text()='Activity Feed Reports')]")).get(size - 1).click();

				}
				break;
			} catch (StaleElementReferenceException e) {
				System.out.println(attempt);
				// if ( error.message.includes( "StaleElementReference" ) )
				attempt++;
			}
		}

	}

	public void ggetTitle4() {

		String mainWindow = driver.getWindowHandle();
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				wait.until(ExpectedConditions.titleIs("Activity Feed | Callhippo.com"));
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}

	}

	public void ValidateActivityFeedReportOnGmail() throws IOException, InterruptedException {
		Thread.sleep(5000);
		System.out.print("go for gmail login ");
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		try {
			if (!gsuiteImg.isDisplayed()) {
				gloginGmail();
			}
		} catch (Exception e) {
			gloginGmail();
		}
		openMail2();
		gClickOnActivityFeedReportslink();
//		driver.get(credential.signIn());
		Common.pause(4);
		driver.switchTo().window(tabs.get(0));
		System.out.println("Switch main  tab");
		// ggetTitle4();
	}

	public void clickOnChangeYourEmailButton() {
		wait.until(ExpectedConditions.visibilityOf((btnChangeYourEmail))).click();
	}

	public void clickOnResendActivationButton() {
		wait.until(ExpectedConditions.visibilityOf((btnResendActivation))).click();
	}

	public boolean validateActivationMessage() {
		return btnResendActivation.isDisplayed();
	}

	public boolean validateCancelButtonInPopup() {
		return btnCancel.isDisplayed();

	}

	public boolean validatSendActivationButtonInPopup() {
		return btnSendActivation.isDisplayed();

	}

	public void clickOnSendActivationButtonInPopup() {
		wait.until(ExpectedConditions.visibilityOf((btnSendActivation))).click();
	}

	public void clearEmailInPopup() {
		wait.until(ExpectedConditions.visibilityOf((txtEmailForheader)));
		txtEmailForheader.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));

	}

	public void enterEmailInPopup(String email) {
		wait.until(ExpectedConditions.visibilityOf((txtEmailForheader)));
		txtEmailForheader.clear();
		txtEmailForheader.sendKeys(email);

	}

	public void clearEmailProfileSection() {
		wait.until(ExpectedConditions.visibilityOf((txtEmailForProfile)));
		txtEmailForProfile.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));

	}

	public void enterEmailInProfileSection(String email) {
		wait.until(ExpectedConditions.visibilityOf((txtEmailForProfile)));
		txtEmailForProfile.clear();
		txtEmailForProfile.sendKeys(email);

	}

	public String validationMessageInvalidInput() {
		return wait.until(ExpectedConditions.visibilityOf(validationInvalidInput)).getText();

	}

	public String validationErrorMessage() {
		return validationErrorMessage.getText();
		// return
		// wait.until(ExpectedConditions.visibilityOf(validationErrorMessage)).getText();

	}

	public void waitForInvisibleErrorMessage() {
		wait.until(ExpectedConditions.invisibilityOf(validationErrorMessage));
	}

	public String validationSuccessMessage() {
		return wait.until(ExpectedConditions.visibilityOf(validationSucessMessage)).getText();

	}

	public void clickOnUserProfile() {
		wait.until(ExpectedConditions.visibilityOf((userProfile))).click();
		wait.until(ExpectedConditions.visibilityOf((userProfilelink))).click();
	}

	public void clickOnSaveButtonInUserProfile() {
		wait.until(ExpectedConditions.visibilityOf((btnSAVE))).click();

	}

	public boolean validateUserProfile() {
		driver.navigate().refresh();
		System.out.println(driver.getCurrentUrl());
		return driver.getCurrentUrl().contains("userProfile");
	}

	public String getEmailUserProfile() {
		return wait.until(ExpectedConditions.visibilityOf((txtEmailForProfile))).getAttribute("value");
	}
}
