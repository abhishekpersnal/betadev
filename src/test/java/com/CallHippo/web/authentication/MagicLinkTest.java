package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;




public class MagicLinkTest {

	MagicLinkPage magicLink;
	WebDriver driver;
	static Common excel;
	PropertiesFile url;
	WebDriver driverMail;
	SignupPage dashboard;

	
	public MagicLinkTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(url.getValue("environment")+"\\MagicLink.xlsx");
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			//driver.get(url);
			try {
				dashboard.deleteAllMail();
			}catch(Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail," Gmail issue ","BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}
	
	@BeforeMethod
	public void initialization() throws Exception {
		try {
			url = new PropertiesFile("Data\\url Configuration.properties");
			driver = TestBase.init();
			try {
				driver.get(url.signIn());
			}catch(Exception e) {
				driver.get(url.signIn());
			}
			magicLink = PageFactory.initElements(driver, MagicLinkPage.class);
		} catch (Exception e) {
			Common.Screenshot(driver," Web MagicLinkTest - Fail ","BeforeMethod - initialization");
			System.out.println("----Need to check issue - MagicLinkTest - BeforeMethod - initialization----");
		} 
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driver.quit();
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void VerifyTitle() throws IOException {
		driver.get(url.forgotPassword());
		String actualResult = magicLink.getTitle();
		String expectedResult = "Login | Callhippo.com";
		assertEquals(actualResult, expectedResult);

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Verify_magicLink_button_enabled_by_clicking_on_checkbox() throws IOException {
		//driver.get(url.forgotPassword());
		magicLink.clickOMagicLinkCheckbox();
		String actualMagiclinkText = magicLink.IsMagicLinkButtonEnabled();
		String expectedMagiclinkText = "Send Magic Link";
		assertEquals(actualMagiclinkText, expectedMagiclinkText);
	}

	@Test(enabled = false, priority = 3, retryAnalyzer = Retry.class)
	public void Verify_Email_is_required_field() throws IOException {
		//driver.get(url.forgotPassword());
		magicLink.clickOMagicLinkCheckbox();
		magicLink.clickOnMagicLink();
		String actualResult = magicLink.verifyValidEmailValidationMsg();
		String expectedResult = "Email is required";
		assertEquals(actualResult, expectedResult);

	}

	@DataProvider(name = "EmailValidation")
	public static Object[][] invalidEmails() {

		return new Object[][] { { excel.getdata(0, 1, 2) }, { excel.getdata(0, 1, 3) }, { excel.getdata(0, 1, 4) },
				{ excel.getdata(0, 1, 5) }, { excel.getdata(0, 1, 6) } };

	}

	@Test(enabled = false, priority = 4, dataProvider = "EmailValidation", retryAnalyzer = Retry.class)
	public void verify_email_validation_with_Invalid_email(String email) throws IOException {
		//driver.get(url.forgotPassword());
		magicLink.enterEmail(email);
		magicLink.clickOMagicLinkCheckbox();
		magicLink.clickOnMagicLink();
		String actualResult = magicLink.verifyValidEmailValidationMsg();
		String expectedResult = "Enter a valid email.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_email_validation_with_nonRegistered_email() throws IOException {
		//driver.get(url.forgotPassword());
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 2, 2));
		magicLink.clickOnMagicLink();
		String actualResult = magicLink.errorMessage();
		String expectedResult = "Email does not exist";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_email_validation_with_Registered_email() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 3, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 3, 2));
		magicLink.clickOnMagicLink();
		String actualResult = magicLink.infoMessage();
		String expectedResult = "Magic link is sent to your email";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 7, dependsOnMethods = "verify_email_validation_with_Registered_email", retryAnalyzer = Retry.class)
	public void verify_Expired_MagicLink_on_Gmail_inbox() throws IOException {
		verify_email_validation_with_Registered_email();
		magicLink.validateMagicLickExpired();
		String actualTitle = magicLink.validateErrormessage();
		String expectedTitle = "Your link is expired";
		assertEquals(actualTitle, expectedTitle);
	}
	
	@Test(priority = 8, dependsOnMethods = "verify_email_validation_with_Registered_email", retryAnalyzer = Retry.class)
	public void verify_MagicLink_on_Gmail_inbox() throws IOException {
		verify_email_validation_with_Registered_email();
		magicLink.ValidateMagicLinOnGmail();
		Common.pause(5);
		String actualTitle = magicLink.ggetTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_blocked_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 6, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 6, 2));
		magicLink.clickOnMagicLink();
		magicLink.ValidateMagicLinOnGmail();
		String actualResult = magicLink.validateErrormessageOfBlockedUser();
		//String expectedResult = "Your account has been blocked due to blocked for automation testing";
		String expectedResult = "Your account has been blocked as you have blocked for automation testing, Please Contact support@callhippo.com.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Unblocked_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 7, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 7, 2));
		magicLink.clickOnMagicLink();
		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.ggetTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Login_attempt_fail_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 8, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 8, 2));
		magicLink.clickOnMagicLink();
		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.validateErrormessage();
		String expectedTitle = "Account blocked due to multiple attempts, kindly check your email to unblock.";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Cancel_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 9, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 9, 2));
		magicLink.clickOnMagicLink();
		Common.pause(2);
		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.ggetTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(enabled = false, priority = 13, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Reactive_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 10, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 10, 2));
		magicLink.clickOnMagicLink();
		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.ggetTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Deleted_Main_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 11, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 11, 2));
		magicLink.clickOnMagicLink();
//		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.errorMessage();
		String expectedTitle = "Your account has been deactivated. Please contact us at support@callhippo.com.";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Deleted_sub_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 12, 3));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 12, 2));
		magicLink.clickOnMagicLink();
		Common.pause(2);
//		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.errorMessage();
		String expectedTitle = "Your account has been deactivated. Please contact us at support@callhippo.com.";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Hard_Deleted_main_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 13, 2));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 13, 2));
		magicLink.clickOnMagicLink();
//		magicLink.ValidateMagicLinOnGmail();
		String actualTitle = magicLink.errorMessage();
		String expectedTitle = "Your account has been deactivated. Please contact us at support@callhippo.com.";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void verify_MagicLink_with_Hard_Deleted_sub_user() throws IOException {
		//driver.get(url.forgotPassword());
		//magicLink.enterEmail(excel.getdata(0, 14, 3));
		magicLink.clickOMagicLinkCheckbox();
		magicLink.enterEmail(excel.getdata(0, 14, 2));
		magicLink.clickOnMagicLink();
		String actualResult = magicLink.errorMessage();
		String expectedResult = "Email does not exist";
		assertEquals(actualResult, expectedResult); 
	}
	
}
