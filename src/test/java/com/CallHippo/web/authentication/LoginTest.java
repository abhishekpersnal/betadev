package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.numberAndUserSubscription.AddNumberPage;
import com.CallHippo.web.numberAndUserSubscription.InviteUserPage;
import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;

public class LoginTest {

	SignupPage registrationPage;
	LoginPage loginpage;
	WebDriver driver;
	static Common signUpexcel;
	static Common magiclinkexcel;
	PropertiesFile url;
	WebDriver driverMail;
	SignupPage dashboard;
	DialerIndex phoneApp2;
	WebToggleConfiguration webApp2;
	String gmailUser;
	AddNumberPage addNumberPage;
	InviteUserPage inviteUserPageObj;
	
	public LoginTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		signUpexcel = new Common(url.getValue("environment") + "\\Signup.xlsx");
		magiclinkexcel = new Common(url.getValue("environment") + "\\MagicLink.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

	@BeforeTest
	public void deleteAllMails() {
		
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			//driver.get(url);
			try {
				dashboard.deleteAllMail();
			}catch(Exception e) {
				dashboard.deleteAllMail();
			} 
		} catch (Exception e) {
			Common.Screenshot(driverMail," Gmail issue ","BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
	driverMail.quit();
	}

	@BeforeMethod
	public void initialization() throws Exception {
		try {	
			driver = TestBase.init();
			try {
				driver.get(url.signIn());
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.signIn());
			}
			loginpage = PageFactory.initElements(driver, LoginPage.class);
			phoneApp2 = PageFactory.initElements(driver, DialerIndex.class);
			webApp2 = PageFactory.initElements(driver, WebToggleConfiguration.class);
			registrationPage = PageFactory.initElements(driver, SignupPage.class);
			addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
			inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);
			
		} catch (Exception e) {
			Common.Screenshot(driver, " Web LoginTest - Fail ", "BeforeMethod - initialization");
			System.out.println("----Need to check issue - LoginTest - BeforeMethod - initialization----");
		}
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driver.quit();
	}
	

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_title() {
		String actualResult = loginpage.getTitle();
		String expectedResult = "Login | Callhippo.com";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 2, retryAnalyzer = Retry.class) 
	public void check_require_field_validation() {
		assertEquals(loginpage.verifyFieldsDisplayOnWebLoginPage(), true);
		loginpage.clearEmailPassword();
		assertEquals(loginpage.verifyEmailAndPasswordFieldRequireValidation(), true);
	}
	@DataProvider(name = "VerifySignInBtnIsNotClickable")
	public static Object[][] invalidCredentials() {

		return new Object[][] { { signUpexcel.getdata(0, 9, 8), " " },
				{ signUpexcel.getdata(0, 9, 2), signUpexcel.getdata(0, 10, 2) },
				{ signUpexcel.getdata(0, 9, 2), signUpexcel.getdata(0, 5, 2) },
				{ signUpexcel.getdata(0, 9, 5), signUpexcel.getdata(0, 10, 2) } };
	}

	@Test(priority = 3, dataProvider = "VerifySignInBtnIsNotClickable", retryAnalyzer = Retry.class)
	public void verify_signInButton_IsDisable(String emailId, String pass) {
		loginpage.enterEmail(emailId);
		loginpage.enterPassword(pass);

		boolean btn = loginpage.signupIsNotClickable();
		assertEquals(btn, true);
	}

	@DataProvider(name = "InvalidEmailValidation")
	public static Object[][] invalidEmails() {
		return new Object[][] { { signUpexcel.getdata(0, 9, 2) }, { signUpexcel.getdata(0, 9, 3) },
				{ signUpexcel.getdata(0, 9, 4) }, { signUpexcel.getdata(0, 9, 5) }, { signUpexcel.getdata(0, 9, 6) } };
	}

	@Test(priority = 4, dataProvider = "InvalidEmailValidation", retryAnalyzer = Retry.class)
	public void verify_Invalid_Email_Validation(String email) {
		loginpage.enterEmail(email);
		String actualResult = loginpage.invalidEmailValidationTxt();
		String expectedResult = "Enter a valid email.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_validation_for_InvalidEmail_InvalidPass() {
		loginpage.enterEmail(signUpexcel.getdata(0, 9, 5));
		loginpage.enterPassword(signUpexcel.getdata(0, 10, 2));
		String actualResult = loginpage.invalidEmailValidationTxt();
		String expectedResult = "Enter a valid email.";
		assertEquals(actualResult, expectedResult);
		boolean btn = loginpage.signupIsNotClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_validation_for_validEmail_InvalidPass() {
		loginpage.enterEmail(signUpexcel.getdata(0, 4, 2));
		loginpage.enterPassword(signUpexcel.getdata(0, 10, 2));
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "Incorrect email or password";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void validation_for_NotRegisteredUser() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 2, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();

		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "This email doesn't exist";
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void validation_for_BlockedUser() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 6, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		loginpage.verifyLoginTitle();
		String actualResult = loginpage.validationWarningMessage();
		String expectedResult = "Your account has been blocked as you have blocked for automation testing, Please Contact support@callhippo.com.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void vefify_UnBlockedUser() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 7, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		loginpage.loginSuccessfully();
	
		Common.pause(2);
		// assertEquals(loginpage.verifyWelcomePopup(), true);
	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void vefify_BlockedUser_BymultipleAttempt() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 8, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "Account blocked due to multiple attempts, kindly check your email to unblock.";
		assertEquals(actualResult, expectedResult);
	}
	
	@Test(priority = 11, retryAnalyzer = Retry.class) //new
	public void vefify_BlockedsubUser_BymultipleAttempt() {
	loginpage.enterEmail(magiclinkexcel.getdata(0, 8, 3));
	loginpage.enterPassword("Test@123");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "Account blocked due to multiple attempts, kindly check your email to unblock.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void vefify_CancelUser_login() throws IOException {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 9, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		assertEquals(loginpage.verifyWelcomePopup(), false);
		// loginpage.clickOnEndTourButton();
		// assertEquals(loginpage.verifyAddNumberPopup(), true);
		// loginpage.closeAddNumberPopup();
		driver.get(url.numbersPage());
		// loginpage.clickDummynumberPage();
		assertEquals(loginpage.verifyDummynumberPage(), true);
		assertEquals(loginpage.verifyUserPageForCancelUser(), true);
	}
	
	@Test(priority = 13, retryAnalyzer = Retry.class) //new
	public void vefify_CancelSubUser_login() throws IOException {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 9, 3));
		loginpage.enterPassword("Test@123");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "This email doesn't exist";
		assertEquals(actualResult, expectedResult);
		
	}
	
	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void vefify_ReactiveUser_login() {
		try {
			loginpage.enterEmail(magiclinkexcel.getdata(0, 10, 2));
			loginpage.enterPassword(url.getValue("masterPassword"));
			loginpage.clickOnLogin();
			assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
			// assertEquals(loginpage.verifyWelcomePopup(), true);
			// loginpage.clickOnEndTourButton();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test(priority = 15, retryAnalyzer = Retry.class) //new
	public void vefify_ReactiveSubUser_login() {
			loginpage.enterEmail(magiclinkexcel.getdata(0, 10, 3));
			loginpage.enterPassword("Test@123");
			loginpage.clickOnLogin();
			assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
			// assertEquals(loginpage.verifyWelcomePopup(), true);
			// loginpage.clickOnEndTourButton();
		
	}

	@Test(priority = 16, retryAnalyzer = Retry.class) //new
	public void vefify_InactiveUser_login() {
			loginpage.enterEmail(magiclinkexcel.getdata(0, 15, 2));
			loginpage.enterPassword("Test@123");
			loginpage.clickOnLogin();
			String actualResult = loginpage.validationErrorMessage();
			String expectedResult = "Please activate your account";
			assertEquals(actualResult, expectedResult);
		
	}
	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void vefify_Deleted_Main_User_login() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 11, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "Your account has been deactivated. Please contact us at support@callhippo.com.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void vefify_Deleted_Sub_User_login() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 12, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		//String expectedResult = "Your account has been deactivated. Please contact us at support@callhippo.com.";
		String expectedResult = "This email doesn't exist";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void vefify_Hard_Deleted_Main_User_login() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 13, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "Your account has been permanently deleted upon request. Please contact us at support@callhippo.com.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void vefify_Hard_Deleted_Sub_User_login() {
		loginpage.enterEmail(magiclinkexcel.getdata(0, 14, 2));
		loginpage.enterPassword("12345678");
		loginpage.clickOnLogin();
		String actualResult = loginpage.validationErrorMessage();
		String expectedResult = "This email doesn't exist";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void verify_login_with_validEmail_validPass() {
		loginpage.enterEmail(signUpexcel.getdata(0, 4, 2));
		loginpage.enterPassword(signUpexcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		String actualResult = loginpage.loginSuccessfully();
		String expectedResult = "Dashboard | Callhippo.com";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_open_dialer_Web() throws Exception {
		verify_login_with_validEmail_validPass();
		loginpage.clickOnopendialerbutton();
		Common.pause(5);

		webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
		phoneApp2.waitForDialerPage();
		assertEquals(true, phoneApp2.validateDialerScreenDisplayed());
		assertEquals(true, phoneApp2.validateDialButtonIsDisplayed());
	}
	
	
	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_ScheduleDemo_Btn_For_Old_Account() throws Exception {
		verify_login_with_validEmail_validPass();

		Common.pause(5);
		assertEquals(webApp2.validateScheduleDemoBtnIsDisplayed(), false);

	}

}
