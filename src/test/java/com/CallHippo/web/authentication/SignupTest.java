package com.CallHippo.web.authentication;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.*;
import com.CallHippo.Init.TestBase;

public class SignupTest {
	SignupPage registrationPage;
	SignupPage dashboard;
	LoginPage loginpage;
	WebDriver driver;
	WebDriver driverMail;
	static Common excel;
	PropertiesFile url;
	String gmailUser;
	WebToggleConfiguration webAppMainUserLogin;

	public SignupTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(url.getValue("environment")+"\\Signup.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			try {
				dashboard.deleteAllMail();
			}catch(Exception e) {
				dashboard.deleteAllMail();
			}
		} catch (Exception e) {
			Common.Screenshot(driverMail," Gmail issue ","BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}
	
	@BeforeMethod
	public void initialization() throws Exception {
		try {
			driver = TestBase.init();
			try {
				driver.get(url.signUp());
			}catch(Exception e) {
				driver.get(url.signUp());
			}
			registrationPage = PageFactory.initElements(driver, SignupPage.class);
			dashboard = PageFactory.initElements(driver, SignupPage.class);
			loginpage = PageFactory.initElements(driver, LoginPage.class);
			webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
			
		} catch (Exception e) {
			Common.Screenshot(driver," Web SignupTest - Fail ","BeforeMethod - initialization");
			System.out.println("----Need to check issue - SignupTest - BeforeMethod - initialization----");
		} 
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driver.quit();
	}
	
	@Test(priority = 1, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void VerifyTitle() {
		String actualResult = registrationPage.getTitle();
		String expectedResult = "Signup | Callhippo.com";
		
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 2, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void verifyFunllNameFieldIsRequired() {
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(excel.getdata(0, 4, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		boolean btn = registrationPage.signupIsClickable();
		assertEquals(btn, true);
	}
	
	@Test(priority = 3, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void verify_Number_IsRequired() {
		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterEmail(excel.getdata(0, 4, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));

		boolean btn = registrationPage.signupIsClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 4, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void verify_Email_IsRequired() {
		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));

		boolean btn = registrationPage.signupIsClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 5, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void verify_password_IsRequired() {
		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(excel.getdata(0, 4, 2));

		boolean btn = registrationPage.signupIsClickable();
		assertEquals(btn, true);
	}

	@Test(priority = 6, groups = "Signup-Required fields validation", retryAnalyzer = Retry.class)
	public void verify_with_filled_all_Filled_IsEnabled() {
		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(excel.getdata(0, 4, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		boolean btn = registrationPage.signupIsClickable();
		assertEquals(btn, true);
	}

	@DataProvider(name = "FullNameValidation")
	public static Object[][] lessThan5CharectersOfFullName() {

		return new Object[][] { { excel.getdata(0, 7, 2) }, { excel.getdata(0, 7, 3) }, { excel.getdata(0, 7, 4) },
				{ excel.getdata(0, 7, 5) } };
				
	}
	@Test(priority = 7, dataProvider = "FullNameValidation", retryAnalyzer = Retry.class)
	public void Verify_fullName_validation(String fullname) throws Exception {
		registrationPage.enterFullname(fullname);
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(excel.getdata(0, 4, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.fullNameValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);

	}

	@DataProvider(name = "NumberValidation")
	public static Object[][] invalidMobleNumbers() {

		return new Object[][] { { excel.getdata(0, 8, 2) }, { excel.getdata(0, 8, 3) }, { excel.getdata(0, 8, 4) } };
	}

	@Test(enabled = false,priority = 8, dataProvider = "NumberValidation", retryAnalyzer = Retry.class)
	public void verify_number_validation_with_Invalid_number(String number) {
		registrationPage.enterMobile(number);
		boolean actualResult = registrationPage.numberValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_number_validation_with_Valid_number() {
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		boolean actualResult = registrationPage.numberValidation();
		boolean expectedResult = false;
		assertEquals(actualResult, expectedResult);
	}

	@DataProvider(name = "EmailValidation")
	public static Object[][] invalidEmails() {

		return new Object[][] { { excel.getdata(0, 9, 2) }, { excel.getdata(0, 9, 3) }, { excel.getdata(0, 9, 4) },
				{ excel.getdata(0, 9, 5) }, { excel.getdata(0, 9, 6) } };

	}

	@Test(priority = 10, dataProvider = "EmailValidation", retryAnalyzer = Retry.class)
	public void verify_email_validation_with_Invalid_email(String email) {
		registrationPage.enterEmail(email);
		boolean actualResult = registrationPage.emailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void verify_email_validation_with_valid_email() {
		registrationPage.enterEmail(excel.getdata(0, 4, 2));
		boolean actualResult = registrationPage.emailValidation();
		boolean expectedResult = false;
		assertEquals(actualResult, expectedResult);
	}

	@DataProvider(name = "PasswordValidation")
	public static Object[][] invalidpasswords() {

		return new Object[][] { { excel.getdata(0, 10, 2) }, { excel.getdata(0, 10, 3) }, { excel.getdata(0, 10, 4) },
				{ excel.getdata(0, 10, 5) } };
	}

	@Test(priority = 12, dataProvider = "PasswordValidation", retryAnalyzer = Retry.class)
	public void verify_password_validation_with_Invalid_password(String pass) {
		registrationPage.enterPassword(pass);
		boolean actualResult = registrationPage.passwordValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void verify_password_validation_with_Valid_password() {
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		boolean actualResult = registrationPage.passwordValidation();
		boolean expectedResult = false;
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Verify_with_blocked_domain() {
		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(excel.getdata(0, 11, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		Common.pause(1);
		String actualResult = registrationPage.blockedEmailValidation();
		String expectedResult = "Please sign up with your company email address. To prevent voice call spamming, we don't allow sign up from generic domains like gmail.com, yahoo.com, etc. In case of any query, please contact us at support@callhippo.com.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Verify_with_registered_email() {
		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(excel.getdata(0, 12, 2));
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		Common.pause(1);
		String actualResult = registrationPage.registeredEmailValidation();
		String expectedResult = "You are already registered with us, please login to continue.";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Verify_createAccount() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		registrationPage.ValidateVerifyButtonOnGmail();
	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void verifyExpiredLink() throws Exception {
		Verify_loginWithNewRegisterEmail();
		Thread.sleep(2000);
		//dashboard.verifyWelcomeToCallHippoPopup();
		dashboard.closeAddnumberPopup();
		dashboard.userLogout();
		registrationPage.validateClickOnExpireLinkFromGmail1();
		String actualVerifyMsg = registrationPage.accountLinkExpiredMsg();
		String expectedVerifyMsg = "User is already verified, Please login.";
		assertEquals(actualVerifyMsg, expectedVerifyMsg);
	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void Verify_loginWithNewRegisterEmail() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		Common.pause(2);
		registrationPage.ValidateVerifyButtonOnGmail();
		
		dashboard.verifyWelcomeToCallHippoPopup();
		dashboard.closeAddnumberPopup();
		dashboard.userLogout();
		
		loginpage.enterEmail(email);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
		
		assertEquals(webAppMainUserLogin.validateScheduleDemoBtnIsDisplayed(),true);
		webAppMainUserLogin.clickOnScheduleDemo();
		Common.pause(5);
		
		webAppMainUserLogin.switchToWindowBasedOnTitle("Schedule A Demo- CallHippo");
		System.out.println(webAppMainUserLogin.validateScheduleDemoPageTitle());
		assertEquals(webAppMainUserLogin.validateScheduleDemoPageTitle(),"Schedule A Demo- CallHippo");
	
		Common.pause(5);
		webAppMainUserLogin.switchToWindowBasedOnTitle("Dashboard | Callhippo.com");
	}
	
	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void Verify_loginWithInactiveUser() throws Exception {
		String date = excel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.enterFullname(excel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(excel.getdata(0, 2, 2));
		registrationPage.enterMobile(excel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword(excel.getdata(0, 5, 2));
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		//registrationPage.ValidateVerifyButtonOnGmail();
		Common.pause(2);
		dashboard.verifyWelcomeToCallHippoPopup();
		dashboard.userLogout();
		assertEquals("Login | Callhippo.com", loginpage.getTitle());
		
		loginpage.enterEmail(email);
		loginpage.enterPassword(excel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		
		assertEquals("Please activate your account", loginpage.validationErrorMessage());
		
	}

	@Test(enabled = false)
	public void Verify_createAccount1() {
		for (int i = 3; i <= 58; i++) {
			driver = TestBase.init();
			registrationPage = PageFactory.initElements(driver, SignupPage.class);
			// driver.get("https://staging-app.callhippo.com/#!/signup");
			// driver.get("https://dev-app.callhippo.com/#!/signup");
			registrationPage.enterFullname(excel.getdata(0, 1, i));
			registrationPage.enterCompanyName(excel.getdata(0, 2, i));
			registrationPage.enterMobile(excel.getdata(0, 3, i));
			registrationPage.enterEmail(excel.getdata(0, 4, i));
			registrationPage.enterPassword(excel.getdata(0, 5, i));
			registrationPage.clickOnSignupButton();
			boolean actualResult = registrationPage.nonRegisteredEmailValidation();
			boolean expectedResult = true;
			assertEquals(actualResult, expectedResult);
			System.out.println("loop " + i);
			driver.quit();
		}
	}
	
	

}
