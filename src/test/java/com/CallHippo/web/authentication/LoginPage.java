package com.CallHippo.web.authentication;

import static org.testng.Assert.expectThrows;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class LoginPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		action = new Actions(this.driver);
	}
	
	@FindBy(xpath = "//span[contains(.,'Sign in to CallHippo')]")
	WebElement signIn_CallHippo_txt;
	
	@FindBy(xpath = "//input[@name='email']")
	WebElement email;
	
	@FindBy(xpath = "//input[@name='password']")
	WebElement password;
	
	@FindBy(xpath = "(//iframe[@src[contains(.,'recaptcha')]])[1]")
	WebElement googleCaptcha;
	
	@FindBy(xpath = "//button[contains(.,'Sign in')]")
	WebElement login;
	
	@FindBy(xpath = "//button[@class='chLoginWithGoogle']/span[contains(.,'Sign In')]")
	WebElement google_SignIn;
	
	@FindBy(xpath = "//div[@class='chMagicLinkWrapper']/label/span/input[@type='checkbox']")
	WebElement magic_link;
	
	@FindBy(xpath = "//a/span[contains(.,'Forgot password')]")
	WebElement forgot_password_link;
	
	@FindBy(xpath = "//a[contains(.,'Create Account')]")
	WebElement create_account_btn;
	
	@FindBy(xpath = "//img[@src[contains(.,'googleplay')]][contains(@alt,'googleplay')]")
	WebElement android_google_play_img;
	
	@FindBy(xpath = "//img[@src[contains(.,'iosappstore')]][contains(@alt,'iosappstore')]")
	WebElement ios_app_store_img;
	
	@FindBy(xpath = "//input[@name='email']//following-sibling::div[contains(.,'Email is required.')]")
	WebElement requiredEmail;
	
	@FindBy(xpath = "//input[@name='password']//following-sibling::div[contains(.,'Password is required.')]")
	WebElement requiredPassword;
	
	@FindBy(xpath = "//input[@name='email']//following-sibling::div[contains(.,'Enter a valid email.')]")
	WebElement emailValidation;
	
	@FindBy(xpath = "//span[contains(@ng-bind-html,'message.content')]")
	WebElement credentialValidation;
	
	@FindBy(xpath = "//a[@href='#!/forgotpassword']")
	WebElement forgotPassword;
	
	@FindBy(xpath="//input[@test-automation='welcome_message_toggle']")
	WebElement welcomeme;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement validationErrorMessage;
	
	@FindBy(xpath = "//div[@class='ant-message-custom-content ant-message-error']") 
	WebElement validationWarningMessage;
	
	@FindBy(xpath="(//div[contains(.,'Welcome Aboard')])[7]")
	WebElement welcome_msg;
	
	@FindBy(xpath="(//button[@ng-click='tour.end()'])[5]")
	WebElement endTour_button;
	
	@FindBy(xpath="//b[@class='ng-binding'][contains(.,'Welcome Aboard!')]")
	WebElement addNumber_popup_msg;
	
	@FindBy(xpath = "//button[@class[contains(.,'addnumber')]][contains(.,'Add Number')]")
	WebElement addNumber_popup_btn;
	
	@FindBy(xpath = "//button[contains(@aria-label,'Dismiss')]")
	WebElement addNumber_popup_close_btn;
	
	@FindBy(xpath = "//a[@href='#!/dummynumber']")
	WebElement dummyNumber_page;
	
	@FindBy(xpath = "//a[contains(.,'Dummy')]")
	WebElement dummyNumber_txt;
	
	@FindBy(xpath = "//a[@href='/numbers/dummy'][contains(.,'+1223XXX5566')]")
	WebElement dummyNumber;
	
	@FindBy(xpath = "//a[@href='/users']")
	WebElement supervisor_user;
	
	@FindBy(xpath = "//button[@class='circlest btn btn-sm btn-info']")
	List<WebElement> user_settings_button;
	@FindBy(xpath = "//a[contains(@class,'opendialerbutton')]")
	WebElement opendialerbutton;
	
	
	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}

	public static boolean isClickable(WebElement element)      
	{
	    try
	    {
	        WebDriverWait wait = new WebDriverWait(driver, 5);
	        wait.until(ExpectedConditions.elementToBeClickable(element));
	        return true;
	    }
	    catch (Exception e)
	    {
	        return false;
	    }
	}
	
	public String getTitle() {
		wait.until(ExpectedConditions.titleContains("Login | Callhippo.com"));
		String signupTitle = driver.getTitle();
		return signupTitle;
	}
	
	public boolean verifyFieldsDisplayOnWebLoginPage() {
		boolean welcome = false;
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(signIn_CallHippo_txt));
		if(
				signIn_CallHippo_txt.isDisplayed() && email.isDisplayed()
				&& password.isDisplayed() 
				//&& googleCaptcha.isDisplayed()
				&& login.isDisplayed() 
				&& google_SignIn.isDisplayed()
				&& magic_link.isSelected()  == false 
				&& forgot_password_link.isDisplayed()
				&& create_account_btn.isDisplayed() 
				&& android_google_play_img.isDisplayed()
				&& ios_app_store_img.isDisplayed()
				)
		{
			welcome = true;
		}else{
			welcome = false;
		}
		return welcome;
	}
	
	public void clearEmailPassword() {
		wait.until(ExpectedConditions.visibilityOf(email));
		email.sendKeys("a");
		email.sendKeys(Keys.BACK_SPACE);
		email.clear();
		
		password.sendKeys("b");
		password.sendKeys(Keys.BACK_SPACE);
		password.clear();
	}
	
	public boolean verifyEmailAndPasswordFieldRequireValidation() {
		boolean welcome = false;
		
		if(requiredEmail.isDisplayed()
				&& requiredPassword.isDisplayed()) {
			welcome = true;
		}else{
			welcome = false;
		}
		return welcome;
	}
	
	public void closePopup() {

		wait_angular5andjs();
		action.sendKeys(Keys.ESCAPE).build().perform();
		wait_angular5andjs();
		//closeDialog.click();
	}

	public void clickOnForgotPasswordLink() {
		wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
		forgotPassword.click();
	}

	public String validateforgotPasswordPageTitle() {
		wait.until(ExpectedConditions.titleContains("Forgot Password | Callhippo.com"));
		return driver.getTitle();
	}

	public void enterEmail(String email) {
		wait.until(ExpectedConditions.visibilityOf(this.email));
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void enterPassword(String password) {
		wait.until(ExpectedConditions.visibilityOf(this.password));
		this.password.clear();
		this.password.sendKeys(password);
	}

	public void clickOnLogin() {
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(googleCaptcha));
		//driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.elementToBeClickable(login));
		login.click();
	}
	
	public void clearEmail() {
		email.clear();
	}

	public void clearPassword() {
		password.clear();
	}

	public String requiredEmailMessage() {
		return requiredEmail.getText();
	}

	public String requiredPasswordMessage() {
		return requiredPassword.getText();
	}

	public boolean IsdisabledLoginButton() {

		try {
			driver.findElement(By.xpath("//button[@id='web_login'][@disabled='disabled']"));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String invalidEmailValidationTxt() {
		return emailValidation.getText();
	}

	public String loginValidation() {
		return credentialValidation.getText();
	}

	public String loginSuccessfully() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.titleContains("Dashboard | Callhippo.com"));
		return driver.getTitle();
	}
	
	public String verifySubscribeTitle() {
		/*
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		*/
		wait.until(ExpectedConditions.titleContains("Subscribe | Callhippo.com"));
		return driver.getTitle();
	}
	public String verifyLoginTitle() {
		/*
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
		*/
		wait.until(ExpectedConditions.titleContains("Login | Callhippo.com"));
		return driver.getTitle();
	} 
	public void welcomeMessageStatus() {
		
		
		System.out.println("welcome message status : "+ welcomeme.isSelected());
		

		JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("$('#welcome_message_toggle').click();");
		if(welcomeme.isSelected()) {
			
		}else {
//			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("$(\"[test-automation='welcome_message_toggle']\").click();");	
		}
		
	}
	
	public boolean signupIsNotClickable() {

		try {
			Thread.sleep(1000);

			//WebElement w = isclickablesignup;
			driver.findElement(By.xpath("//button[contains(.,'Sign in')][contains(@disabled.,'')]"));

			return true;
		} catch (Exception e) {

			return false;
		}

	}
	
	public String validationErrorMessage() {
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(validationErrorMessage));
		return validationErrorMessage.getText();
	}
	
	public String validationWarningMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationWarningMessage));
		try {
			return validationWarningMessage.getText();
		}catch(Exception e) {
			return validationWarningMessage.getText();
		}
		
	}
	
	public boolean verifyWelcomePopup() {
		boolean welcome = false;
		wait.until(ExpectedConditions.visibilityOf(welcome_msg));
		String actual_welcome_msg_str = welcome_msg.getText();
		if(actual_welcome_msg_str.contains("Welcome Aboard")) {
			welcome = true;
		}else{
			welcome = false;
		}
		action.sendKeys(Keys.ESCAPE).build().perform();
		return welcome;
	}
	
	public void clickOnEndTourButton() {
		wait.until(ExpectedConditions.elementToBeClickable(endTour_button));
		endTour_button.click();
	}
	
	public boolean verifyAddNumberPopup() {
		boolean welcome = false;
		wait.until(ExpectedConditions.visibilityOf(addNumber_popup_btn));
		
		String actual_addNumber_msg_str = addNumber_popup_msg.getText();
		String expected_addNumber_msg_str = "Welcome Aboard!";//"Hey Unblocked User Welcome Aboard!";
		
		if(actual_addNumber_msg_str.contains(expected_addNumber_msg_str) && 
				addNumber_popup_btn.isDisplayed()) {
			welcome = true;
		}else{
			welcome = false;
		}
		return welcome;
	}

	public void closeAddNumberPopup() {
		wait.until(ExpectedConditions.elementToBeClickable(addNumber_popup_close_btn));
		addNumber_popup_close_btn.click();
	}
	
	public boolean verifyNumberPagForCancelUser() {
		boolean welcome = false;
		wait.until(ExpectedConditions.visibilityOf(addNumber_popup_btn));
		
		String actual_addNumber_msg_str = addNumber_popup_msg.getText();
		String expected_addNumber_msg_str = "Welcome Aboard!";//"Hey Unblocked User Welcome Aboard!";
		
		if(actual_addNumber_msg_str.contains(expected_addNumber_msg_str) && 
				addNumber_popup_btn.isDisplayed()) {
			welcome = true;
		}else{
			welcome = false;
		}
		action.sendKeys(Keys.ESCAPE).build().perform();
		return welcome;
	}
	
	public void clickDummynumberPage() {
		wait.until(ExpectedConditions.elementToBeClickable(dummyNumber_page));
		dummyNumber_page.click();
	}
	
	public boolean verifyDummynumberPage() {
		boolean welcome = false;
		wait.until(ExpectedConditions.visibilityOf(dummyNumber_txt));
		
		if(dummyNumber_txt.isDisplayed() && dummyNumber.isDisplayed()) {
			welcome = true;
		}else{
			welcome = false;
		}
		return welcome;
		
	}
	
	public boolean verifyUserPageForCancelUser() {
		
		wait.until(ExpectedConditions.elementToBeClickable(supervisor_user));
		supervisor_user.click();
		
		boolean cancelUser = false;
		
		if(driver.findElements(By.xpath("//a[@class='settingcircle userSettingIcon'][contains(.,'settings')]")).size()<=1) {
			cancelUser = true;
		}else{
			cancelUser = false;
		}
		return cancelUser;
	}
	public void clickOnopendialerbutton() {
		wait.until(ExpectedConditions.elementToBeClickable(opendialerbutton));
		opendialerbutton.click();
	}
}
