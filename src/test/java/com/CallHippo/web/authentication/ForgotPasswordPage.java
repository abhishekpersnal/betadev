package com.CallHippo.web.authentication;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class ForgotPasswordPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	WebDriver driver1;
	PropertiesFile credential;
	
	@FindBy(xpath = "//div[@class='chForgotPassWrapper']/a")
	WebElement forgot_password_link;

	@FindBy(xpath = "//img[@alt='CallHippo Logo']")
	private WebElement callhippoLogo;
	@FindBy(name = "email")
	private WebElement email;
	@FindBy(xpath = "//button[contains(.,'Reset Password')]")
	private WebElement btnResetPassword;
	@FindBy(xpath = "//input[contains(@placeholder,'Email')]//following-sibling::div[contains(.,'Email is required')]")
	private WebElement emailRequired;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'Email does not exist')]")
	private WebElement emailDoesnotExist;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span[contains(.,'An email has been sent to reset your password')]")
	private WebElement registeredemail;
	@FindBy(xpath = "//span[contains(.,'Back to sign in')]")
	private WebElement backToSignin;
	@FindBy(xpath = "//span[contains(.,'Use Magic Link')]")
	private WebElement magiCLink;

	@FindBy(xpath = "(//iframe[@src[contains(.,'recaptcha')]])[1]")
	private WebElement googleCaptcha;

	// Reset password page
	@FindBy(xpath = "//input[@name='fullName']")
	private WebElement fullName;
	@FindBy(xpath = "//input[@name='password']")
	private WebElement resetpassword;
	@FindBy(xpath = "//input[@name='confirmPassword']")
	private WebElement confirmPassword;
	@FindBy(xpath = "//button[contains(.,'Reset Password')]")
	private WebElement resetPasswordBtn;
	@FindBy(xpath = "//input[contains(@placeholder,'Confirm Password')]//following-sibling::div[contains(.,'Password and confirm password does not matched.')]")
	private WebElement mismatchPasswordValidation;
	@FindBy(xpath = "//input[@name='password']//following-sibling::div[contains(.,'Password is too short.')]")
	private WebElement shortpassword;
	@FindBy(xpath = "//input[contains(@placeholder,'Confirm Password')]//following-sibling::div[contains(.,'Confirm password is too short.')]")
	private WebElement shortConfirmPassword;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'Sorry this password reset request has expired. We have send you a new link to reset your password.')]")
	private WebElement ExpiredResetPasswordLink;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span[contains(.,'Your password has been reset successfully')]")
	private WebElement resetPasswordSuccessfully;
	
	@FindBy(xpath = "//span[@class='eightcharacter cancelicon']")
	private WebElement minimun8CharacterPassword;
	@FindBy(xpath = "//span[@class='onenumber cancelicon']")
	private WebElement minimunOnenumberPassword;
	@FindBy(xpath = "//span[@class='upparcase cancelicon']")
	private WebElement minimunOneupparcasePassword;
	@FindBy(xpath = "//span[@class='lowercase cancelicon']")
	private WebElement minimunOnelowercasePassword;
	@FindBy(xpath = "//span[@class='specialcharacter cancelicon']")
	private WebElement minimunOnespecialcharacterPassword;

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'You used this password recently. Please choose a different one.')]")
	private WebElement resetAlreadySetPasswordSuccessfully;

	// gmail xpath
	@FindBy(xpath = "//input[@id='identifierId']")
	private WebElement gemail;
	@FindBy(xpath = "//*[@id=\"identifierNext\"]/div/button")
	private WebElement gemailNext;
	@FindBy(xpath = "//input[@name='password']")
	private WebElement gpassword;
	@FindBy(xpath = "//*[@id=\"passwordNext\"]/div/button")
	private WebElement gpasswordNext;
	@FindBy(xpath = "//span[contains(text(),'CallHippo | Confirm Your Email')]")
	private WebElement mailtitle;
	@FindBy(xpath = "//a[contains(.,'Reset Password')]")
	private WebElement resetPassword;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'Please Accept Invitation')]")
	private WebElement acceptInvitation;
	
	@FindBy(xpath = "//span[contains(.,'Sign in CallHippo')]")
	private WebElement signInTxt;

	public ForgotPasswordPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		action = new Actions(this.driver);
		
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}

	public String getTitle() {
		return driver.getTitle();
	}

	
	
	public void enterEmail(String email) {
		wait.until(ExpectedConditions.visibilityOf(this.email));
		this.email.sendKeys(email);
	}
	
	public void clickForgotPasswordLink() {
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(googleCaptcha));
		//driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.elementToBeClickable(forgot_password_link));
		new Actions(driver).moveToElement(forgot_password_link).click().perform();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", forgot_password_link);
	}
	public void clickForgotPasswordLink1() {
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(googleCaptcha));
		//driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.elementToBeClickable(forgot_password_link));
		//new Actions(driver).moveToElement(forgot_password_link).click().build().perform();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", forgot_password_link);
		
	}
	public void clearEmail() {
		Common.pause(1);
		email.sendKeys(Keys.BACK_SPACE);
	}
	
	public void clickOnSignInTxt() {
		Common.pause(1);
		signInTxt.click();
	}

	public boolean emailValidation() {

		try {
			driver.findElement(By.xpath("//input[contains(@placeholder,'Email')]//following-sibling::div[contains(.,'Enter a valid email.')]"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void clickOnResetPassword() {
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(googleCaptcha));
		//driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.elementToBeClickable(btnResetPassword));
		btnResetPassword.click();
	}

	public String ValidateEmailRequiredField() {
		wait.until(ExpectedConditions.visibilityOf(emailRequired));
		return emailRequired.getText();
	}

	public String ValidateEmailDoesnotExist() {
		wait.until(ExpectedConditions.visibilityOf(emailDoesnotExist));
		return emailDoesnotExist.getText();
	}

	public String validateWithRegisteredEmail() {
		wait.until(ExpectedConditions.visibilityOf(registeredemail));
		return registeredemail.getText();
	}
	
	public String validateAcceptInvitationMsg() {
		wait.until(ExpectedConditions.visibilityOf(acceptInvitation));
		return acceptInvitation.getText();
	}

	// ---------------------------Start Gmail Login functions ---------------------

	public void validateMagicLickExpired() {
		gloginGmail();
		openMail();
		gClickOnResetPasswordlink();
		ggetTitle();

	}

	public void ValidateResetPasswordButtonOnGmail() {
		gloginGmail();
		openMail();
		gClickOnRecentResetPasswordlink();
		ggetTitle();
	}
	public void ValidateResetPasswordButtonOnGmail1() {
		gloginGmail();
		openMail1();
		gClickOnRecentResetPasswordlink();
		ggetTitle();
	}
	
	public void ValidateResetPasswordButtonTwiceOnGmail() {
		gloginGmail();
		openMail();
		gClickOnRecentResetPasswordlink();
		gstwitchToWindow();
	}
	
	

	public void gloginGmail() {
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		// driver.get("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gemail.sendKeys(credential.gmailId());
		} catch (IOException e1) {
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		gemailNext.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		try {
			gpassword.sendKeys(credential.gmailPassword());
		} catch (IOException e1) {
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void gClickOnRecentResetPasswordlink() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Reset Password')]"))));
		int size = driver.findElements(By.xpath("//a[contains(.,'Reset Password')]")).size();
		System.out.println(size);

		if (size < 2) {
			driver.findElement(By.xpath("//a[contains(.,'Reset Password')]")).click();
		} else {
			Common.pause(1);
			driver.findElements(By.xpath("//a[contains(.,'Reset Password')]")).get(size - 1).click();

		}
	}

	public void gClickOnResetPasswordlink() {
		int attempt=0;
		int MAX_ATTEMPTS = 5;
		
		while( attempt < MAX_ATTEMPTS ) {
		    try {
		    	Common.pause(1);
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Reset Password')]"))));
				int size = driver.findElements(By.xpath("//a[contains(.,'Reset Password')]")).size();
				System.out.println(size);
		
				if (size > 0) {
					driver.findElement(By.xpath("//a[contains(.,'Reset Password')]")).click();
				} else {
					Common.pause(1);
					driver.findElements(By.xpath("//a[contains(.,'Reset Password')]")).get(size - 1).click();
		
				}
				break;
		    }catch ( StaleElementReferenceException e )
		    {
		    	System.out.println(attempt);
		      //if ( error.message.includes( "StaleElementReference" ) )
		    	attempt++;
		    }
		}
	}

	public void ggetTitle() {
		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				//wait.until(ExpectedConditions.titleContains("Reset Password | Callhippo.com"));
				//System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}
	
	public void gstwitchToWindow() {
		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				System.out.println(driver.switchTo().window(childWindow).getTitle());
				// driver1 =driver.switchTo().window(childWindow);
				// return driver.switchTo().window(childWindow).getTitle();
				// driver.close();
			}
		}
		// return mainWindow;
	}

	public void openMail() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
		}
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(text(),'CallHippo | Reset your password')]")).get(1)
							.click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(
							driver.findElements(By.xpath("//span[contains(text(),'CallHippo | Reset your password')]"))
									.get(1)));
					driver.findElements(By.xpath("//span[contains(text(),'CallHippo | Reset your password')]")).get(1)
							.click();
				}

				break;
			} catch (StaleElementReferenceException e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}
	public void openMail1() {
		int attempts = 0;
		while (attempts < 5) {
			try {
				try {
					driver.findElements(By
							.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo | Reset your password')]"))
							.get(1).click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(driver.findElements(By
							.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo | Reset your password')]"))
							.get(1)));
					driver.findElements(By
							.xpath("//span[contains(@class,'bqe')][contains(text(),'CallHippo | Reset your password')]"))
							.get(1).click();
				}

				break;
			} catch (Exception e) {
				driver.get(
						"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
				Common.pause(5);
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	} 

	// ---------------------------Start Gmail Login functions ---------------------
	public void ValidateGmailResetPasswordLink() {
		driver.navigate().to("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		try {
			gemail.sendKeys(credential.gmailId());
		} catch (IOException e1) {
		}
		gemailNext.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		try {
			gpassword.sendKeys(credential.gmailPassword());
		} catch (IOException e1) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

		int attempts = 0;
		while (attempts < 2) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(text(),'CallHippo | Reset your password')]")).get(1)
							.click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(
							driver.findElements(By.xpath("//span[contains(text(),'CallHippo | Reset your password')]"))
									.get(1)));
					driver.findElements(By.xpath("//span[contains(text(),'CallHippo | Reset your password')]")).get(1)
							.click();
				}

				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}


		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

		resetPassword.click();

		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
			if (!mainWindow.equals(childWindow)) {
				driver.switchTo().window(childWindow);
				wait.until(ExpectedConditions.titleIs("Reset Password | Callhippo.com"));
				//System.out.println(driver.switchTo().window(childWindow).getTitle());
			}
		}

	}

	public String validateSigninLink() {
		backToSignin.click();
		wait.until(ExpectedConditions.titleIs("Login | Callhippo.com"));
		return driver.getTitle();
	}

	public boolean validateCallHippoLogoIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(callhippoLogo));
		return callhippoLogo.isDisplayed();
	}

	public boolean validateEmailTextBoxIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(email));
		return email.isDisplayed();
	}

	public boolean validateResetPasswordButtonIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(btnResetPassword));
		return btnResetPassword.isDisplayed();
	}

	public boolean validateMagicLinkIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(magiCLink));
		return magiCLink.isDisplayed();
	}

	public boolean resetPasswordButtonIsClickable() {

		try {
			Thread.sleep(1000);
			if(driver.findElement(
					By.xpath("//button[contains(@disabled.,'')][contains(.,'Reset Password')]")).isEnabled()) {
				return false;
			}else {
				return true;
			}
			
		} catch (Exception e) {

			return false;
		}

	}

	public void enterFullName(String fullNmae) {
		// driver = driver1;
		wait.until(ExpectedConditions.visibilityOf(this.fullName));
		this.fullName.clear();
		this.fullName.sendKeys(fullNmae);
		// driver1.findElement(By.xpath("//input[@id='password']")).sendKeys(password);
		// driver = driver1;

	}

	public void enterPassword(String password) {
		
		try {
			wait.until(ExpectedConditions.visibilityOf(resetpassword));
			resetpassword.clear();
			Common.pause(1);
			resetpassword.sendKeys(password);
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(resetpassword));
			resetpassword.clear();
			Common.pause(1);
			resetpassword.sendKeys(password);
		}

	}

	public void enterConfirmPassword(String confirmPasswordstr) {
		try {
			wait.until(ExpectedConditions.visibilityOf(confirmPassword));
			confirmPassword.clear();
			Common.pause(1);
			confirmPassword.sendKeys(confirmPasswordstr);
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(confirmPassword));
			confirmPassword.clear();
			Common.pause(1);
			confirmPassword.sendKeys(confirmPasswordstr);
		}
	}

	public void clickOnResetPasswordButton() {
		wait.until(ExpectedConditions.elementToBeClickable(resetPasswordBtn));
		resetPasswordBtn.click();
	}

	public String mismatchResetPasswordValidation() {
		wait.until(ExpectedConditions.visibilityOf(mismatchPasswordValidation));
		return mismatchPasswordValidation.getText();
	}

	public String validateShortPassword() {
		wait.until(ExpectedConditions.visibilityOf(shortpassword));
		return shortpassword.getText();
	}

	public String validateShortConfirmPassword() {
		wait.until(ExpectedConditions.visibilityOf(shortConfirmPassword));
		return shortConfirmPassword.getText();
	}
	public boolean validateMinimun8CharacterPassword() {
		return wait.until(ExpectedConditions.visibilityOf(minimun8CharacterPassword)).isDisplayed();
		
	}
	public boolean validateMinimunOnenumberPassword() {
		return wait.until(ExpectedConditions.visibilityOf(minimunOnenumberPassword)).isDisplayed();
		
	}
	public boolean validateUpperCaseValidationInPassword() {
		return wait.until(ExpectedConditions.visibilityOf(minimunOneupparcasePassword)).isDisplayed();
		
	}
	public boolean validateLowerCaseValidationInPassword() {
		return wait.until(ExpectedConditions.visibilityOf(minimunOnelowercasePassword)).isDisplayed();
		
	}
	public boolean validateSpecialCharacterValidationInPassword() {
		return wait.until(ExpectedConditions.visibilityOf(minimunOnespecialcharacterPassword)).isDisplayed();
		
	}

	public String validateexpiredResetPasswordLinkErrorMessage() {
		wait.until(ExpectedConditions.visibilityOf(ExpiredResetPasswordLink));
		return ExpiredResetPasswordLink.getText();

	}

	public String ValidateResetPasswordSuccessfully() {
		wait.until(ExpectedConditions.visibilityOf(resetPasswordSuccessfully));
		return resetPasswordSuccessfully.getText();
	}
	public String ValidateAlreadysetPasswordValidation() {
		wait.until(ExpectedConditions.visibilityOf(resetAlreadySetPasswordSuccessfully));
		return resetAlreadySetPasswordSuccessfully.getText();
	}

}
