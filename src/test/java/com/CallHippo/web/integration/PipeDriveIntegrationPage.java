package com.CallHippo.web.integration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.PropertiesFile;

public class PipeDriveIntegrationPage {
	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait2;
	Actions action;

	PropertiesFile url;
	JavascriptExecutor js;
	PropertiesFile credential;

	public PipeDriveIntegrationPage(WebDriver driver) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		url = new PropertiesFile("Data\\url Configuration.properties");
		js = (JavascriptExecutor) driver;

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	@FindBy(xpath = "//input[@id='login']") WebElement pipeDriveEmail;
	@FindBy(xpath = "//input[@id='password']") WebElement pipeDrivePassword;
	@FindBy(xpath = "//button[contains(.,'Log in')]") WebElement pipeDriveLoginButton;
	
	@FindBy(xpath = "//a[@href='/contacts']") WebElement contactIconPipedrive;
	@FindBy(xpath = "(//div[@data-test='name-label'])[1]") WebElement contactname;
	
	
	public void enterEmailInPipeDrive(String email) {
		wait.until(ExpectedConditions.visibilityOf(pipeDriveEmail)).sendKeys(email);
	}
	
	public void enterPasswordInPipeDrive(String password) {
		wait.until(ExpectedConditions.visibilityOf(pipeDrivePassword)).sendKeys(password);
	}
	
	public void clickOnLoginButtonInPipeDrive() {
		wait.until(ExpectedConditions.visibilityOf(pipeDriveLoginButton)).click();
	}
	
	
	public void clickOnPersionIcon() {
		wait.until(ExpectedConditions.visibilityOf(contactIconPipedrive)).click();
	}
	
	public String getFirstValueFromContactlist() {
		return wait.until(ExpectedConditions.visibilityOf(contactname)).getText();
	}
	
	
}
