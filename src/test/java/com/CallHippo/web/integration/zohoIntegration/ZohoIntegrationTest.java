package com.CallHippo.web.integration.zohoIntegration;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.integration.PipeDriveAPIs;
import com.CallHippo.web.integration.PipeDriveIntegrationPage;

public class ZohoIntegrationTest {
	
	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	WebDriver driver3;

	WebToggleConfiguration webApp2;
	WebToggleConfiguration webApp1;
	WebDriver driver4;

	WebDriver driver5;
	PipeDriveIntegrationPage pipeDrivePage;

	ZohoAPIs api;
	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	
	String webRestAPI_Token="5fe06e274b7aed5f828dfef8";
	String accessToken="1000.bd884b5e2d677471806a6975e1cb1f08.c7cf25f1ba4e7d2bbc91d8c3230fd84d";
	String contactId="171341000000227171";
	String updatedContactNumber="+919408133900";
	String accountId = "171341000000233041";
	String dealId="171341000000233052";

	public ZohoIntegrationTest() throws Exception {

		excel = new Common("Data\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new ZohoAPIs();
	}

	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);

		driver1 = TestBase.init();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}

		} catch (Exception e1) {
			String testname = "ZohoIntegration Before Method ";
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driver4.get(url.signIn());
			driver1.navigate().to(url.dialerSignIn());
		} catch (Exception e) {

			try {
				Common.pause(2);
				driver4.get(url.signIn());
				driver1.navigate().to(url.dialerSignIn());
			} catch (Exception e1) {
				String testname = "Normal calling Before Method ";
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

//		driver.quit();
//		driver1.quit();
//		driver2.quit();
//		driver4.quit();
//		driver3.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Contact_Added_Successfully() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		String webRestAPI_Token = webApp2.getRestAPIFromSettingPage();
		System.out.println("----webRestAPI_Token--"+webRestAPI_Token);//5fe06e274b7aed5f828dfef8
		
		Common.pause(2);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		String Url = driver4.getCurrentUrl();
		String userID = webApp2.getUserId(Url);
		System.out.println("----userID--"+userID);//5f3cf4b1d73e3e771629b5e2
		
		webApp2.clickOnIntegrationLinkFromSideMenu();
		webApp2.clickOnIntegrationButton("Zoho");
		webApp2.selectZohoDomainRadioBtn();
		webApp2.clickOnConnectNowButton();
		webApp2.enterEmailInZoho("automation@callhippo.com");
		webApp2.clickOnZohoEmailNextBtn();
		webApp2.enterPasswordInZoho("Automation@123");
		webApp2.clickOnZohoSignInBtn();
		webApp2.clickOnZohoCRMAcceptBtn();
		webApp2.clickOnSkipButton();
		assertEquals(webApp2.validateCheckIconOnIntegration("Zoho"), true, "validate check/delete icon border.");

		Common.pause(10);
		
		String accessToken = api.getAccessTokenFromZoho(userID);
		System.out.println("-------------accessToken : "+accessToken);

		String contactFirstName="Fname1";
		String contactLastName="Lname1";
		
		String contactName = contactFirstName+" "+contactLastName;
		String contactID = api.addContactInZoho(accessToken, contactFirstName, contactLastName, "+919876543214");
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(contactName, webRestAPI_Token, userID), true);
		
		contactFirstName = contactFirstName+"updated";
		String updatedContactName = contactFirstName+" "+contactLastName;
		api.updateContactInZoho(accessToken, contactID, contactFirstName, contactLastName, "+91987654323");
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(updatedContactName, webRestAPI_Token, userID), true);
		
		api.deleteContactInZoho(accessToken, contactID);
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(updatedContactName, webRestAPI_Token, userID), false);
	}
	
	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Contact_Added_Updated_Deleted_Successfully() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		String webRestAPI_Token = webApp2.getRestAPIFromSettingPage();
		System.out.println("----webRestAPI_Token--"+webRestAPI_Token);//5fe06e274b7aed5f828dfef8
		
		Common.pause(2);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		String Url = driver4.getCurrentUrl();
		String userID = webApp2.getUserId(Url);
		System.out.println("----userID--"+userID);//5f3cf4b1d73e3e771629b5e2
		
		webApp2.clickOnIntegrationLinkFromSideMenu();
		webApp2.clickOnIntegrationButton("Zoho");
		webApp2.selectZohoDomainRadioBtn();
		webApp2.clickOnConnectNowButton();
		webApp2.enterEmailInZoho("automation@callhippo.com");
		webApp2.clickOnZohoEmailNextBtn();
		webApp2.enterPasswordInZoho("Automation@123");
		webApp2.clickOnZohoSignInBtn();
		webApp2.clickOnZohoCRMAcceptBtn();
		webApp2.clickOnSkipButton();
		assertEquals(webApp2.validateCheckIconOnIntegration("Zoho"), true, "validate check/delete icon border.");

		Common.pause(10);
		
		String accessToken = api.getAccessTokenFromZoho(userID);
		System.out.println("-------------accessToken : "+accessToken);

		String contactFirstName="Fname1";
		String contactLastName="Lname1";
		String contactNumber = "+919876543212";
		updatedContactNumber = "+919408133900";
		
		
		String contactName = contactFirstName+" "+contactLastName;
		String contactID = api.addContactInZoho(accessToken, contactFirstName, contactLastName, contactNumber);
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(contactName, webRestAPI_Token, userID), true);
		
		contactFirstName = contactFirstName+"updated";
		String updatedContactName = contactFirstName+" "+contactLastName;
		api.updateContactInZoho(accessToken, contactID, contactFirstName, contactLastName, updatedContactNumber);
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(updatedContactName, webRestAPI_Token, userID), true);
		
		api.deleteContactInZoho(accessToken, contactID);
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(updatedContactName, webRestAPI_Token, userID), false);
	}
	
	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Deal_Created_Successfully() throws Exception {
		
		webApp2.clickLeftMenuSetting();
		webRestAPI_Token = webApp2.getRestAPIFromSettingPage();
		System.out.println("----webRestAPI_Token--"+webRestAPI_Token);//5fe06e274b7aed5f828dfef8
		
		Common.pause(2);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		String Url = driver4.getCurrentUrl();
		String userID = webApp2.getUserId(Url);
		System.out.println("----userID--"+userID);//5f3cf4b1d73e3e771629b5e2
		
		webApp2.clickOnIntegrationLinkFromSideMenu();
		webApp2.clickOnIntegrationButton("Zoho");
		webApp2.selectZohoDomainRadioBtn();
		webApp2.clickOnConnectNowButton();
		webApp2.enterEmailInZoho("automation@callhippo.com");
		webApp2.clickOnZohoEmailNextBtn();
		webApp2.enterPasswordInZoho("Automation@123");
		webApp2.clickOnZohoSignInBtn();
		webApp2.clickOnZohoCRMAcceptBtn();
		webApp2.clickOnSkipButton();
		assertEquals(webApp2.validateCheckIconOnIntegration("Zoho"), true, "validate check/delete icon border.");

		Common.pause(10);
		
		accessToken = api.getAccessTokenFromZoho(userID);
		System.out.println("-------------accessToken : "+accessToken);

		String contactFirstName="Fname1";
		String contactLastName="Lname1";
		String contactNumber = "+919408133900";//"+919876543212";
		updatedContactNumber = "+919408133900";
		String accountName = "AutoAccountTest2";
		String dealName = "Deal3";
		
		String contactName = contactFirstName+" "+contactLastName;
		contactId = api.addContactInZoho(accessToken, contactFirstName, contactLastName, contactNumber);
		Common.pause(10);
		assertEquals(api.validateContactNameInCallHippo(contactName, webRestAPI_Token, userID), true);
		
		dealId = api.createDealInZoho(accessToken, dealName, accountName, contactId);
		
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void outgoing_Cancelled_Call_Activity() throws Exception {
//		
//		validate_Deal_Created_Successfully();
//		
//		phoneApp2.enterNumberinDialer(updatedContactNumber);
//		phoneApp2.clickOnDialButton();
//		Common.pause(15);
//		phoneApp2.clickOnOutgoingHangupButton();
//		
//		Common.pause(180);
//		
		assertEquals(api.getContactRecentCallActivityFromZoho(accessToken,contactId), api.cancelledCallActivity(webRestAPI_Token));
		
		api.deleteContactInZoho(accessToken, contactId);
		
		api.deleteAccountInZoho(accessToken, accountId);
		
		api.deleteDealInZoho(accessToken, dealId);
		
	}

}
