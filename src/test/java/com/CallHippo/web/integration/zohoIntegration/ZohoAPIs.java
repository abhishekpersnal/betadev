package com.CallHippo.web.integration.zohoIntegration;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.fasterxml.jackson.core.util.RequestPayload;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class ZohoAPIs {

	@Test
	public String getRefreshTockenforZoho(String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.basePath("script/"+webUserID+"/zoho");
		Response response = given()
				.spec(requestSpec)
				.when()
				.get("https://jenkins-app.callhippo.com/script/"+webUserID+"/zoho");

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String refreshToken = jsonPathEvaluator.getString("data.integrations[0].refreshToken");
		System.out.println("---refreshToken---"+refreshToken);
		return refreshToken;
	}
	
	@Test
	public String getAccessTokenFromZoho(String webUserID) {

		String refreshToken = getRefreshTockenforZoho(webUserID);
		String clientID = "1000.9SVOUQLQTHJA64W11LD70JQD3ZGW6D";
		String clientSecret = "66dd8133e0372a92b38f62c43638723586b407731a";

		Response response = given()
				.formParam("refresh_token", refreshToken)
				.formParam("client_id", clientID)
				.formParam("client_secret", clientSecret)
				.formParam("grant_type", "refresh_token")
				.request().post("https://accounts.zoho.in/oauth/v2/token");

		JsonPath jsonPathEvaluator = response.jsonPath();
		String accessToken = jsonPathEvaluator.getString("access_token");
		System.out.println("----accessToken---"+accessToken);
		return accessToken;
	}
	
	@Test
	public String addContactInZoho(String accessToken, String firstName, String lastName, String phone) {
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Contacts");
		requestSpec.header("Authorization",authToken);
		
		JSONObject requestParams = new JSONObject();
		
		Map<String, Object> map = new LinkedHashMap<>();
//		map.put("CustomerID", "539177");
//		map.put("ReminderTitle", "Demo Reminder Tds");
//		map.put("ReminderDescription", "xyz Reminder");
//		map.put("ReminderLocation", "New Delhi");
//		map.put("ReminderDate", "2020-03-27");
//		map.put("ReminderTime", "15:33");
		map.put("data", Arrays.asList(new LinkedHashMap<String, Object>() {
		    {
		    	put("First_Name", firstName);
		        put("Last_Name", lastName);
		        put("Phone", phone);
		    }
		}));
		
		requestSpec.body(map);
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("Created contact body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id[0]");
		System.out.println("zoho created contact id=="+id);
		return id;
	}
	
	@Test
	public boolean validateContactNameInCallHippo(String addedContacteName, String webAPIToken, String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.headers("apitoken", webAPIToken);
		requestSpec.basePath("contact/" + webUserID);
		Response response = given().spec(requestSpec).when()
				.get("https://jenkins-app.callhippo.com/contact/"+webUserID);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);
		
		System.out.println("callhippo contact body :    "+response.body().asString());
		
		System.out.println(name1.contains(addedContacteName));

		return name1.contains(addedContacteName);
	}
	
	@Test
	public void updateContactInZoho(String accessToken, String contactId, String firstName, String lastName, String phone) {
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Contacts/"+contactId);
		requestSpec.header("Authorization",authToken);
		
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("data", Arrays.asList(new LinkedHashMap<String, Object>() {
		    {
		    	put("First_Name", firstName);
		        put("Last_Name", lastName);
		        put("Phone", phone);
		    }
		}));
		
		requestSpec.body(map);
		Response response = requestSpec.put("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("updated contact body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id");
		System.out.println("zoho updated contact id=="+id);
	}
	
	@Test
	public void deleteContactInZoho(String accessToken, String contactId) {
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Contacts/"+contactId);
		requestSpec.header("Authorization",authToken);
		
		Response response = requestSpec.delete("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("deleted contact body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id");
		System.out.println("zoho deleted contact id=="+id);
	}
	
	@Test
	public String createAccountInZoho(String accessToken, String accountName) {
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Accounts");
		requestSpec.header("Authorization",authToken);
		
		JSONObject requestParams = new JSONObject();
		
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("data", Arrays.asList(new LinkedHashMap<String, Object>() {
		    {
		    	put("Account_Name", accountName);
		    }
		}));
		
		requestSpec.body(map);
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("created Account In Zoho body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id");
		System.out.println("zoho created account id=="+id);
		return id;
	}
	
	@Test
	public void deleteAccountInZoho(String accessToken, String accountId) {
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/accounts/"+accountId);
		requestSpec.header("Authorization",authToken);
		
		Response response = requestSpec.delete("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("deleted contact body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id");
		System.out.println("zoho deleted Account id=="+id);
	}
	
	@Test
	public String createDealInZoho(String accessToken, String dealName, String accountName, String contactId) {
//		String accessToken, String dealName, String accountName, String contactId
//		String accessToken; String dealName; String accountName; String contactId;
//		accessToken = "1000.521212b4e5e2a258756e23247c6a667d.8806f5f1add904e20e842d47b93de8d7";
//		dealName = "Deal2";
//		accountName = "AutoAccountTest2";
//		contactId = "171341000000222073";
		System.out.println("--- accessToken --"+accessToken);
		System.out.println("--- dealName --"+dealName);
		System.out.println("--- accountName --"+accountName);
		System.out.println("--- contactId --"+contactId);
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Deals");
		requestSpec.header("Authorization",authToken);
		
		JSONObject requestParams = new JSONObject();
		
		JSONObject contactObj = new JSONObject();
		contactObj.put("id", contactId);
		
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("data", Arrays.asList(new LinkedHashMap<String, Object>() {
		    {
		    	put("Deal_Name", dealName);
		    	put("Account_Name", accountName);
		    	put("Contact_Name", contactObj);
		    }
		}));
		
		requestSpec.body(map);
		
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("zoho deal Created body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id[0]");
		System.out.println("zoho deal id=="+id);
		return id;
	}
	
	@Test
	public void deleteDealInZoho(String accessToken, String dealId) {
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/deals/"+dealId);
		requestSpec.header("Authorization",authToken);
		
		Response response = requestSpec.delete("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("deleted contact body: "+response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.details.id");
		System.out.println("zoho deleted Deal id=="+id);
	}
	
	@Test
	public String getContactRecentCallActivityFromZoho(String accessToken, String contactId) throws Exception {
		
//		String accessToken; //String dealName; String accountName; String contactId;
//		accessToken = "1000.5dbe66272fc4f4281e0ad84d5f629d86.216c7410579a6bae875f437be0edab70";
		System.out.println("---accessToken--to get activity-"+accessToken+"----contactId---"+contactId);
		String authToken = "Zoho-oauthtoken "+accessToken;
		
//		String contactId = "171341000000208817";
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Activities/search");
		requestSpec.header("Authorization",authToken);
		requestSpec.queryParam("criteria", "Who_Id.id:equals:"+contactId);
		
		
		Response response = given().spec(requestSpec).when()
				.get("https://www.zohoapis.in/crm/v2/Activities/search");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println("===Response all activity body=="+bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String activityCreatedTime = jsonPathEvaluator.getString("data.Created_Time");
		System.out.println("=== All activity created time=="+activityCreatedTime);
		
		
		List<Integer> jsonResponse = response.jsonPath().getList("data.Created_Time");
		List<Integer>sortedList =jsonResponse.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()); 
		System.out.println("-----------> Sorted date list size :"+sortedList.size());
		System.out.println("-----------> Sorted date list :"+sortedList);
		System.out.println("-----------> 1st value from sorted list :"+sortedList.get(0));

		List<String> createdTimeList = response.path("data.Created_Time");
		System.out.println("-----------> createdTimeList :"+createdTimeList);
	    // check that they are all 'open'
		
		String subject=null;
		
	    for (int i=0; i<sortedList.size();i++)
	    {
	        if(jsonPathEvaluator.getString("data.Created_Time["+i+"]").equals(sortedList.get(0))) {
	        	System.out.println("-----------> Created_Time from list :"+jsonPathEvaluator.getString("data.Created_Time["+i+"]"));
	        	System.out.println("-----------> Activity id :"+jsonPathEvaluator.getString("data.id["+i+"]"));
	        	System.out.println("-----------> Activity Subject:"+jsonPathEvaluator.getString("data.Subject["+i+"]"));
	        	subject = jsonPathEvaluator.getString("data.Subject["+i+"]");
	        }else {
	        	System.out.println("data not matched");
	        }
	    }
		
	    return subject;
	}
	
	@Test
	public String getCallActivityForContactFromZoho(String accessToken, String contactId) throws Exception {
		
//		String accessToken; //String dealName; String accountName; String contactId;
//		accessToken = "1000.ecd91bd0eeeb73f50b456575fbdcf0c2.38bc9186c31deffc4847957b680b215a";
		
		String authToken = "Zoho-oauthtoken "+accessToken;
		
//		String contactId = "171341000000208129";
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://www.zohoapis.in");
		requestSpec.basePath("crm/v2/Activities/search");
		requestSpec.header("Authorization",authToken);
		requestSpec.queryParam("criteria", "Who_Id.id:equals:"+contactId);
		
		
		Response response = given().spec(requestSpec).when()
				.get("https://www.zohoapis.in/crm/v2/Activities/search");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.id[0]");
		System.out.println("---Zoho ActivityFeed ID---"+id);
		
		System.out.println("String value: "+id);
		
		
		RequestSpecification requestSpec1 = RestAssured.given();
		requestSpec1.baseUri("https://www.zohoapis.in");
		requestSpec1.basePath("crm/v2/Activities");
		requestSpec1.header("Authorization",authToken);
		Response response1 = given().spec(requestSpec1).when()
				.get("https://www.zohoapis.in/crm/v2/Activities/"+id);
		ResponseBody body1 = response1.getBody();

		String bodyAsString1 = body1.asString();

		System.out.println("---Zoho ActivityFeed body for perticular ID---"+bodyAsString1);
		
		JsonPath jsonPathEvaluator1 = response1.jsonPath();
		String subject = jsonPathEvaluator1.getString("data.Subject[0]");
		System.out.println("---Zoho Activity Subject"+subject);
		
		String actualPersonID = jsonPathEvaluator1.getString("data.Who_Id.id[0]");
		
		assertEquals(actualPersonID, contactId,"validate person id in pipedrive activity");
		
		return subject;
//		return name;
		
	}
	
	@Test
	public String cancelledCallActivity(String webAPIToken) {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", "2020/12/28");
		request.put("endDate", "2020/12/28");
		request.put("apiToken", webAPIToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://staging-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String callDuration = jsonPathEvaluator.getString("data.callLogs.callDuration[0]");
		System.out.println(callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus);
//		System.out.println(callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus +" ("+callDuration+")");
		
		String activity = callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus;
		
		return activity;
		
	}
	
}
