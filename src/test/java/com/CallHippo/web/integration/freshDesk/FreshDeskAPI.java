package com.CallHippo.web.integration.freshDesk;

import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class FreshDeskAPI {

	String restAPIToken = "S3RBVRAIbesuOclUXtqq";
	String domainName = "newaccount1609412699856";
	String authentication = "Basic amF5YWRpcEBjYWxsaGlwcG8uY29tOnRlc3RAMTIz";
	
	
	
	@Test
	public String addContactInFreshDesk(String name, String phone ) {
		
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/contacts");
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("name", name);
//		requestParams.put("email", "automation2@callhippo.com");
		requestParams.put("phone", phone);
		
		requestSpec.body(requestParams.toString());
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("id");
		System.out.println(id);
		return id;
	}
	
	
	@Test
	public String crateTicketInFreshDesk(String ContactID,String subject) {
		
		String stringContatId = ContactID;
		long contactID=Long.parseLong(stringContatId); 
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/tickets");
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("subject", subject);
		requestParams.put("description", "test description");
		requestParams.put("priority", 1);
		requestParams.put("status", 2);
		requestParams.put("requester_id", contactID);
		
		
		requestSpec.body(requestParams.toString());
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("id");
		System.out.println(id);
		return id;
	}
	
	@Test
	public String getRecentTicketFromFreshDesk() {
		
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/tickets");
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		
		Response response = requestSpec.get("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("id[0]");
		System.out.println(id);
		return id;
	}
	
	@Test
	public String getSubjectTicketInFreshDesk() {
		
		String ticketID = getRecentTicketFromFreshDesk();
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/tickets/"+ticketID);
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		
		Response response = requestSpec.get("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String subject = jsonPathEvaluator.getString("subject");
		System.out.println(subject);
		return subject;
	}
	
	
	@Test
	public String getDescriptionTicketInFreshDesk() {
		
		String ticketID = getRecentTicketFromFreshDesk();
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/tickets/"+ticketID);
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		
		Response response = requestSpec.get("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String descriptiontext = jsonPathEvaluator.getString("description_text");
		System.out.println(descriptiontext);
		return descriptiontext;
	}
	
	@Test
	public void deleteTicketInFreshDesk(String ticketID) {
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/tickets/"+ticketID);
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		
		Response response = requestSpec.delete("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
	}
	
	@Test
	public void deleteContactInFreshDesk(String contactID) {
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+domainName+".freshdesk.com");
		requestSpec.basePath("api/v2/contacts/"+contactID);
		requestSpec.header("APIkey",restAPIToken);
		requestSpec.header("Authorization", authentication);
		requestSpec.header("Content-Type","application/json");
		
		
		Response response = requestSpec.delete("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
	}
	
	
	//---------------------------- callHIppo APIs ---------------------------------
	
	
	
	@Test
	public boolean validateContactNumberInCallHippo(String pipeDriveNumber, String webAPIToken, String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.headers("apitoken", webAPIToken);
		requestSpec.basePath("contact/" + webUserID);
		Response response = given().spec(requestSpec).when()
				.get("https://jenkins-app.callhippo.com/contact/"+webUserID);

		
		JsonPath jsonPathEvaluator1 = response.jsonPath();
		String number1 = jsonPathEvaluator1.getString("data.number");
		System.out.println(number1);
		
		
//		assertEquals(number1.contains(pipeDriveNumber), true, "pipeDrive number not matched with callHippo contact.");
		
		System.out.println(number1.contains(pipeDriveNumber));

		System.out.println("callhippo contact detail: "+ response.getBody().asString());
		return number1.contains(pipeDriveNumber);
	}
}
