package com.CallHippo.web.integration.freshDesk;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.integration.PipeDriveAPIs;
import com.CallHippo.web.integration.PipeDriveIntegrationPage;

public class FreshDeskIntegrationTest {
	


	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp3;
	WebDriver driver2;
	WebDriver driver3;

	WebToggleConfiguration webApp2;
	WebToggleConfiguration webApp1;
	WebDriver driver4;

	WebDriver driver5;
	PipeDriveIntegrationPage pipeDrivePage;

	PipeDriveAPIs api;
	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	
	
	String updatedContactNumber;
	String accessToken;
	String contactID;
	String restAPI;

	public FreshDeskIntegrationTest() throws Exception {

		excel = new Common("Data\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new PipeDriveAPIs();
	}

	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);

//		driver5 = TestBase.init();
//		System.out.println("Opened webaap session");
//		pipeDrivePage = PageFactory.initElements(driver5, PipeDriveIntegrationPage.class);
//		
//		driver3 = TestBase.init();
//		System.out.println("Opened webaap session");
//		webApp1 = PageFactory.initElements(driver3, WebToggleConfiguration.class);
//
		driver = TestBase.init();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);
//
//		driver2 = TestBase.init();
//		System.out.println("Opened phoneAap3 session");
//		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

//				try {
//					driver5.get("https://callhippo-sandbox2.pipedrive.com/");
//					pipeDrivePage.enterEmailInPipeDrive("bhavinradadiya+1@callhippo.com");
//					pipeDrivePage.enterPasswordInPipeDrive("Pass@123");
//					pipeDrivePage.clickOnLoginButtonInPipeDrive();
//					
//				} catch (Exception e) {
//					driver5.get("https://callhippo-sandbox2.pipedrive.com/");
//					pipeDrivePage.enterEmailInPipeDrive("bhavinradadiya+1@callhippo.com");
//					pipeDrivePage.enterPasswordInPipeDrive("Pass@123");
//					pipeDrivePage.clickOnLoginButtonInPipeDrive();
//					
//				}

//			try {
//				driver3.get(url.signIn());
//				Common.pause(4);
//				System.out.println("Opened webaap signin Page");
//				loginWeb(webApp1, account1Email, account1Password);
//			} catch (Exception e) {
//				driver3.get(url.signIn());
//				Common.pause(4);
//				System.out.println("Opened webaap signin Page");
//				loginWeb(webApp1, account1Email, account1Password);
//			}
//
			try {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}
//
			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}
//			try {
//				driver2.get(url.dialerSignIn());
//				Common.pause(4);
//				System.out.println("Opened phoneAap3 signin page");
//				loginDialer(phoneApp3, account3Email, account3Password);
//				System.out.println("loggedin phoneAap3");
//			} catch (Exception e) {
//				driver2.get(url.dialerSignIn());
//				Common.pause(4);
//				System.out.println("Opened phoneAap3 signin page");
//				loginDialer(phoneApp3, account3Email, account3Password);
//				System.out.println("loggedin phoneAap3");
//			}

//			try {
//				System.out.println("loggedin webApp");
//				Thread.sleep(9000);
//				webApp2.numberspage();
//
//				webApp2.navigateToNumberSettingpage(number);
//
//				Thread.sleep(9000);
//				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//				webApp2.userTeamAllocation("users");
//				Thread.sleep(2000);
//				webApp2.userspage();
//				webApp2.navigateToUserSettingPage(account2Email);
//				Thread.sleep(9000);
//
//				String Url = driver4.getCurrentUrl();
//				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
//
//				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
//				webApp2.userspage();
//				webApp2.navigateToUserSettingPage(account2EmailSubUser);
//				Thread.sleep(9000);
//				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
//
//				driver1.get(url.dialerSignIn());
//				phoneApp2.deleteAllContactsFromDialer();
//				driver1.get(url.dialerSignIn());
//
//				phoneApp1.afterCallWorkToggle("off");
//				driver.get(url.dialerSignIn());
//				phoneApp2.afterCallWorkToggle("off");
//				driver1.get(url.dialerSignIn());
//				
//				webApp2.clickLeftMenuDashboard();
//				Common.pause(2);
//				webApp2.clickLeftMenuSetting();
//				Common.pause(2);
//				webApp2.scrollToCallBlockingTxt();
//				Common.pause(1);
//				webApp2.deleteAllNumbersFromBlackList();
//
//			} catch (Exception e) {
//				System.out.println("loggedin webApp");
//				Thread.sleep(9000);
//				webApp2.numberspage();
//
//				webApp2.navigateToNumberSettingpage(number);
//
//				Thread.sleep(9000);
//				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
//				webApp2.userTeamAllocation("users");
//				Thread.sleep(2000);
//				webApp2.userspage();
//				webApp2.navigateToUserSettingPage(account2Email);
//				Thread.sleep(9000);
//
//				String Url = driver4.getCurrentUrl();
//				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
//
//				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
//				webApp2.userspage();
//				webApp2.navigateToUserSettingPage(account2EmailSubUser);
//				Thread.sleep(9000);
//				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
//
//				driver1.get(url.dialerSignIn());
//				phoneApp2.deleteAllContactsFromDialer();
//				driver1.get(url.dialerSignIn());
//
//				phoneApp1.afterCallWorkToggle("off");
//				driver.get(url.dialerSignIn());
//				phoneApp2.afterCallWorkToggle("off");
//				driver1.get(url.dialerSignIn());
//				
//				webApp2.clickLeftMenuDashboard();
//				Common.pause(2);
//				webApp2.clickLeftMenuSetting();
//				Common.pause(2);
//				webApp2.scrollToCallBlockingTxt();
//				Common.pause(1);
//				webApp2.deleteAllNumbersFromBlackList();
//
//			}

		} catch (Exception e1) {
			String testname = "Normal calling Before Method ";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
//			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
//			Common.Screenshot(driver5, testname, "PipeDrive Fail login");
//			Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
//			driver3.get(url.signIn());
			driver4.get(url.signIn());
//			driver5.get("https://callhippo-sandbox2.pipedrive.com/");
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
//			driver2.navigate().to(url.dialerSignIn());
//			Common.pause(3);
//			if (webApp2.validateWebAppLoggedinPage() == true) {
//				loginWeb(webApp2, account2Email, account2Password);
//				Common.pause(3);
//			}
//			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
//				loginDialer(phoneApp1, account1Email, account1Password);
//				Common.pause(3);
//			}
//			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
//				loginDialer(phoneApp2, account2Email, account2Password);
//				Common.pause(3);
//			}
//			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
//				loginDialer(phoneApp3, account3Email, account3Password);
//				Common.pause(3);
//			}
		} catch (Exception e) {

			try {
				Common.pause(2);
//				driver3.get(url.signIn());
				driver4.get(url.signIn());
//				driver5.get("https://callhippo-sandbox2.pipedrive.com/");
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
//				driver2.navigate().to(url.dialerSignIn());
//				Common.pause(3);
//				if (webApp2.validateWebAppLoggedinPage() == true) {
//					loginWeb(webApp2, account2Email, account2Password);
//					Common.pause(3);
//				}
//				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
//					loginDialer(phoneApp1, account1Email, account1Password);
//					Common.pause(3);
//				}
//				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
//					loginDialer(phoneApp2, account2Email, account2Password);
//					Common.pause(3);
//				}
//				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
//					loginDialer(phoneApp3, account3Email, account3Password);
//					Common.pause(3);
//				}
			} catch (Exception e1) {
				String testname = "Normal calling Before Method ";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
//				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
//				Common.Screenshot(driver5, testname, "PipeDrive Fail login");
//				Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
//			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
//			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
//			Common.Screenshot(driver5, testname, "PipeDrive Fail " + result.getMethod().getMethodName());
//			Common.Screenshot(driver3, testname, "WebAppp1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
//			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
//			Common.Screenshot(driver5, testname, "PipeDrive Pass " + result.getMethod().getMethodName());
//			Common.Screenshot(driver3, testname, "WebAppp1 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {

//		driver.quit();
//		driver1.quit();
//		driver2.quit();
//		driver4.quit();
//		driver3.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}
	
	
	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Integration_successfully_and_sync_Contact_And_Created_Deal() throws Exception {
		
		String restAPIToken = "S3RBVRAIbesuOclUXtqq";
		String domainName = "newaccount1609412699856";
		
		webApp2.clickLeftMenuSetting();
		restAPI = webApp2.getRestAPIFromSettingPage();

		Common.pause(2);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		String Url = driver4.getCurrentUrl();
		String userID = webApp2.getUserId(Url);

		webApp2.clickOnIntegrationLinkFromSideMenu();
		webApp2.clickOnFreshDeskIntegrationButton();
		Common.pause(3);
		webApp2.enterFreshDeskAPIKey(restAPIToken);
		webApp2.enterFreshDeskDomainName(domainName);
		webApp2.clickOnFreshDeskSaveButton();
		Common.pause(3);
		assertEquals(webApp2.validateFreshDeskCheckIcon(), true, "validate check/delete icon border.");
		
//		
//		String contactName="p23";
//		String updatedName = "jayadip pstn4";
//		String contactNumber = "+919876543217";
//		updatedContactNumber = number3;
//		String dealName = "Deal d11";
//		
//		//add person in Pipe Drive
//		contactID = api.addPersonInPipeDrive(accessToken, contactName, contactNumber);
//		Common.pause(5);
//		
//		// validate recently added contact in pipeDrive is sync to CallHippo
//		assertEquals(api.validateContactNameInCallHippo(contactName, restAPI, userID), true,"validate recently added contact in pipeDrive is sync to CallHippo.");
//		assertEquals(api.validateContactNumberInCallHippo(contactNumber, restAPI, userID), true,"validate recently added contact in pipeDrive is sync to CallHippo.");
//		
//		// update person name and number in PipeDrive
//		api.UpdatePersonInPipeDrive(accessToken, contactID, updatedName, updatedContactNumber);
//		Common.pause(5);
//		
//		// validate recently updated contact in pipeDrive is sync to CallHippo
//		assertEquals(api.validateContactNameInCallHippo(updatedName, restAPI, userID), true, "validate recently updated contact in pipeDrive is sync to CallHippo.");
//		assertEquals(api.validateContactNumberInCallHippo(updatedContactNumber, restAPI, userID), true, "validate recently updated contact in pipeDrive is sync to CallHippo.");
//		
//		//add Deal in pipeDrive with recently added contact
//		api.addDealInPipeDrive(accessToken, contactID, dealName);
		
	}
	
	
	
	
	
	
	
	

}
