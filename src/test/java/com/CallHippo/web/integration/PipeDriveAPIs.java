package com.CallHippo.web.integration;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.google.common.collect.Ordering;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class PipeDriveAPIs {
	PropertiesFile credential;
	private String userID;
	private static DecimalFormat df2 = new DecimalFormat("#.###");

	public PipeDriveAPIs() {

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
			userID = credential.userId();
		} catch (Exception e) {
		}

	}
	
	
	@Test
	public String getAccessTokenFromPipeDrive(String webUserID) {

		String refreshToken = getRefreshTockenforPipeDrive(webUserID);
		String clientID = "ba3192864e02a648";
		String clientSecret = "89d3232d9b403aff394e193216317b90b29724b6";

		given().header("Content-Type", "application/x-www-form-urlencoded").auth().preemptive()
				.basic(clientID, clientSecret)
				.formParam("grant_type", "refresh_token")
				.formParam("refresh_token", refreshToken).request()
				.post("https://oauth.pipedrive.com/oauth/token").getBody().asString();

//		System.out.println(given().header("Content-Type", "application/x-www-form-urlencoded").auth().preemptive()
//				.basic(clientID, clientSecret).formParam("grant_type", "refresh_token")
//				.formParam("refresh_token", refreshToken).request().post("https://oauth.pipedrive.com/oauth/token")
//				.getBody().asString());
		Response response = given().header("Content-Type", "application/x-www-form-urlencoded").auth().preemptive()
				.basic(clientID, clientSecret).formParam("grant_type", "refresh_token")
				.formParam("refresh_token", refreshToken).request().post("https://oauth.pipedrive.com/oauth/token");
		JsonPath jsonPathEvaluator = response.jsonPath();
		String accessToken = jsonPathEvaluator.getString("access_token");
//		System.out.println(accessToken);
		return accessToken;

	}

	@Test
	public String getRefreshTockenforPipeDrive(String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.basePath(webUserID+"/pipedrive");
		Response response = given().spec(requestSpec).when()
				.get("https://jenkins-app.callhippo.com/script/"+webUserID+"/pipedrive");

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		// System.out.println(bodyAsString.contains("name"));

//		System.out.println(bodyAsString);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String refreshToken = jsonPathEvaluator.getString("data.integrations[0].refreshToken");
		System.out.println("---------------RefreshToken : "+refreshToken);
		return refreshToken;
//		 
	}
	
	
	
	//-------------------------------------------------------
	
	@Test
	public int getPersonFromPipeDrive(String accessToken1) {
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/persons");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.name");
		System.out.println(name);

		String apistrint = response.asString();
		List<Map<String, String>> booksOfUser = JsonPath.from(apistrint).get("data.name");
//		Assert.assertEquals(2, booksOfUser.size());
		System.out.println("size of users of PipeDrive: "+booksOfUser.size());
		return booksOfUser.size();
	}
	
	@Test
	public int getContactSizeFromCallHippo(String webAPIToken, String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.headers("apitoken", webAPIToken);
		requestSpec.basePath("contact/" + webUserID);
		Response response = given().spec(requestSpec).when()
				.get("https://jenkins-app.callhippo.com/contact/"+webUserID);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);

		String apistrint = response.asString();
		List<Map<String, String>> contactName = JsonPath.from(apistrint).get("data.name");
		//Assert.assertEquals(2, booksOfUser.size());
		int contactCount = contactName.size();

		String name = jsonPathEvaluator.get("name");
		System.out.println(name);

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString.contains("name"));

		System.out.println(bodyAsString);
		
		System.out.println("size of users of CallHippo: "+contactCount);
		return contactCount;
	}
	
	
	
	

	@Test
	public boolean validateContactNameInCallHippo(String pipeDriveName, String webAPIToken, String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.headers("apitoken", webAPIToken);
		requestSpec.basePath("contact/" + webUserID);
		Response response = given().spec(requestSpec).when()
				.get("https://jenkins-app.callhippo.com/contact/"+webUserID);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);
		
		
		
		System.out.println(name1.contains(pipeDriveName));

		System.out.println("callhippo contact detail: "+ response.getBody().asString());
		return name1.contains(pipeDriveName);
	}
	
	@Test
	public boolean validateContactNumberInCallHippo(String pipeDriveNumber, String webAPIToken, String webUserID) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://jenkins-app.callhippo.com");
		requestSpec.headers("apitoken", webAPIToken);
		requestSpec.basePath("contact/" + webUserID);
		Response response = given().spec(requestSpec).when()
				.get("https://jenkins-app.callhippo.com/contact/"+webUserID);

		
		JsonPath jsonPathEvaluator1 = response.jsonPath();
		String number1 = jsonPathEvaluator1.getString("data.number");
		System.out.println(number1);
		
		
//		assertEquals(number1.contains(pipeDriveNumber), true, "pipeDrive number not matched with callHippo contact.");
		
		System.out.println(number1.contains(pipeDriveNumber));

		System.out.println("callhippo contact detail: "+ response.getBody().asString());
		return number1.contains(pipeDriveNumber);
	}
	
	@Test
	public String getPersonNameFromPipeDrive(String accessToken1,int arraySize) {
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/persons");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.name["+arraySize+"]");
		System.out.println(name);
		return name;
		
	}

	@Test
	public String addPersonInPipeDrive(String accessToken1, String name, String phone) {
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("name", name);
		requestParams.put("phone", phone);
		
		requestSpec.body(requestParams.toString());
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.getString("data.id");
		System.out.println(id);
		return id;
	}
	
	
	@Test
	public void UpdatePersonInPipeDrive(String accessToken1, String id,String name, String phone) {
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons/"+id);
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("name", name);
		requestParams.put("phone", phone);
		
		requestSpec.body(requestParams.toString());
		Response response = requestSpec.put("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("updated body: "+response.getBody().asString());
		
	}
	
	@Test
	public void deletePersonInPipeDrive(String accessToken1, String id) {
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons/"+id);
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		
		Response response = requestSpec.delete("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("delted body: "+response.getBody().asString());
		
	}
	
	
	@Test
	public void addDealInPipeDrive(String accessToken1, String personID, String dealName) {
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("deals");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("Accept","application/json");
		requestSpec.header("Content-Type","application/x-www-form-urlencoded");
		
		requestSpec.formParam("title", dealName);
		requestSpec.formParam("value", "100");
		requestSpec.formParam("person_id", personID);
		
		Response response = given().spec(requestSpec).when()
				.post("https://api-proxy.pipedrive.com/deals");
		
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println("add deal :  "+response.getBody().asString());
		
		
		
	}
	
	
	@Test
	public void getPersonNameFromPipeDrive() {
		
		String accesToken = "Bearer "+"7484568:11424248:90bb8764e6077af6870d50ed3c76030f9bdc5f4b";
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/persons");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.name");
		System.out.println(name);
		
		
//		return name;
		
	}
	
	@Test
	public void test() throws Exception {
		System.out.println(Common.currentDate1());
	}
	@Test
	public String getCallActivityFromPipeDrive(String accessToken1, String personID) throws Exception {
		
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
//		requestSpec.basePath("activities?start_date=2020-12-24");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/activities?start_date="+Common.currentDate());
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.id");
		System.out.println(name);
		
		List<Integer> jsonResponse = response.jsonPath().getList("data.id");
		List<Integer>sortedList =jsonResponse.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()); 
		sortedList.forEach(System.out::println); 
		int i =sortedList.get(0);
		System.out.println("-----------> Sorted value :"+sortedList.get(0));
		
		String id =String.valueOf(i);
		System.out.println("String value: "+id);
		
		
		Response response1 = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/activities/"+id);
		ResponseBody body1 = response1.getBody();

		String bodyAsString1 = body1.asString();


		System.out.println(bodyAsString1);
		
		JsonPath jsonPathEvaluator1 = response1.jsonPath();
		String subject = jsonPathEvaluator1.getString("data.subject");
		System.out.println(subject);
		
		String actualPersonID = jsonPathEvaluator1.getString("data.participants.person_id[0]");
		
		assertEquals(actualPersonID, personID,"validate person id in pipedrive activity");
		
		return subject;
		
		
		
		
//		return name;
		
	}
	
	@Test
	public String cancelledCallActivity(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		System.out.println("call hippo activity ::  "+callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus);
		
		String activity = callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus;
		
		return activity;
		
		
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
		
		
		
	}
	
	
	@Test
	public String cancelledCallActivityNote(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String to = jsonPathEvaluator.getString("data.callLogs.to[0]");
//		System.out.println(callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus);
		
		String note = "<p>Called by : "+caller+"</p><p>Contact Person : "+toName+"</p><p>From : "+from+"</p><p>To : "+to+"</p>";
		
		System.out.println(note);
		
		return note;
//		<p>Called by : local acc1</p><p>Contact Person : jayadip pstn2</p><p>From : +13513000476</p><p>To : +918511695975</p>
		
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
		
		
		
	}
	
	
	
	@Test
	public String getCancelledCallActivityNoteFromPipeDrive(String accessToken1, String personID) throws Exception {
		
		
		String accesToken = "Bearer "+accessToken1;
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
//		requestSpec.basePath("activities?start_date=2020-12-24");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/activities?start_date="+Common.currentDate());
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.id");
		System.out.println(name);
		
		List<Integer> jsonResponse = response.jsonPath().getList("data.id");
		List<Integer>sortedList =jsonResponse.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()); 
		sortedList.forEach(System.out::println); 
		int i =sortedList.get(0);
		System.out.println("-----------> Sorted value :"+sortedList.get(0));
		
		String id =String.valueOf(i);
		System.out.println("String value: "+id);
		
		
		Response response1 = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/activities/"+id);
		ResponseBody body1 = response1.getBody();

		String bodyAsString1 = body1.asString();


		System.out.println(bodyAsString1);
		
		JsonPath jsonPathEvaluator1 = response1.jsonPath();
		String note = jsonPathEvaluator1.getString("data.note");
		System.out.println(note);
		
		String actualPersonID = jsonPathEvaluator1.getString("data.participants.person_id[0]");
		
		assertEquals(actualPersonID, personID,"validate person id in pipedrive activity");
		
		return note;
		
		
		
		
//		return name;
		
	}
	
	
	@Test
	public String missedCallActivity(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String to = jsonPathEvaluator.getString("data.callLogs.to[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String fromName = jsonPathEvaluator.getString("data.callLogs.fromName[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		System.out.println("call hippo activity ::  "+callType+" Call From "+fromName+" To "+to+" - "+callStatus);
		
		String activity = callType+" Call From "+fromName+" To "+to+" - "+callStatus;
		
		return activity;
		
		//Incoming Call From jayadip pstn4 To +13513000476 - Missed
		
		
		
	}
	
	@Test
	public String missedCallActivityNote(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String fromName = jsonPathEvaluator.getString("data.callLogs.fromName[0]");
		
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String to = jsonPathEvaluator.getString("data.callLogs.to[0]");
//		System.out.println(callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus);
		
		String note = "<p>Contact Person : "+fromName+"</p><p>From : "+from+"</p><p>To : "+to+"</p>";
		
		System.out.println(note);
		
		return note;
		
//		<p>Contact Person : jayadip pstn4</p><p>From : +18475128682</p><p>To : +13513000476</p>
//		<p>Called by : local acc1</p><p>Contact Person : jayadip pstn2</p><p>From : +13513000476</p><p>To : +918511695975</p>
		
	}
	
	@Test
	public String IncomingCompletedCallActivity(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String to = jsonPathEvaluator.getString("data.callLogs.to[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String fromName = jsonPathEvaluator.getString("data.callLogs.fromName[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String callDuration = jsonPathEvaluator.getString("data.callLogs.callDuration[0]");
		System.out.println("call hippo activity ::  "+callType+" Call From "+fromName+" To "+to+"("+toName+")"+" - "+callStatus+" ("+callDuration+")");
		
		String activity = callType+" Call From "+fromName+" To "+to+"("+caller+")"+" - "+callStatus+" ("+callDuration+")";
		
		return activity;
		
		//Incoming Call From jayadip pstn4 To +13513000476(local acc1) - Completed (00:06)
		//Incoming Call From jayadip pstn4 To +13513000476 - Missed
		
		
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
		
		
		
	}
	
	@Test
	public String completedCallActivityNote(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String fromName = jsonPathEvaluator.getString("data.callLogs.fromName[0]");
		
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String to = jsonPathEvaluator.getString("data.callLogs.to[0]");
		
		String recordingUrl = jsonPathEvaluator.getString("data.callLogs.recordingUrl[0]");
//		System.out.println(callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus);
		
		String note = "Received By : "+caller+" <p>Contact Person : "+fromName+"</p><p>From : "+from+"</p><p>To : "+to+"</p><p>Recording : <a href=\\\""+recordingUrl+"\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\">CallHippo Recording</a></p>";
		
		System.out.println(note);
		
		return note;
//		"Received By : local acc1 <p>Contact Person : jayadip pstn4</p><p>From : +18475128682</p><p>To : +13513000476</p><p>Recording : <a href=\""+recordingUrl+"\" rel=\"noopener noreferrer\" target=\"_blank\">CallHippo Recording</a></p>"
//		Received By : local acc1 <p>Contact Person : jayadip pstn4</p><p>From : +18475128682</p><p>To : +13513000476</p><p>Recording : <a href=\"https://callhippo-staging-copy.s3.amazonaws.com/https://s3.amazonaws.com/callhippo-staging-copy/callrecordings/CAa32ee807a2a7382626390d42866d3e37.mp3\" rel=\"noopener noreferrer\" target=\"_blank\">CallHippo Recording</a></p>
		
//		<p>Contact Person : jayadip pstn4</p><p>From : +18475128682</p><p>To : +13513000476</p>
//		<p>Called by : local acc1</p><p>Contact Person : jayadip pstn2</p><p>From : +13513000476</p><p>To : +918511695975</p>
		
	}
	
	@Test
	public String outgoingCompletedCallActivity(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String callDuration = jsonPathEvaluator.getString("data.callLogs.callDuration[0]");
		System.out.println("call hippo activity ::  "+callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus);
		
		String activity = callType+" Call To "+toName+" From "+from+"("+caller+") - "+callStatus+" ("+callDuration+")";
		
		return activity;
		
		// Outgoing Call To jayadip pstn4 From +13513000476(local acc1) - Completed (00:11)
		
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
		
		
		
	}
	
	@Test
	public String IncomingVoicemailByRejectCallActivity(String apiToken) throws Exception {
		JSONObject request = new JSONObject();

		request.put("skip", "0");
		request.put("limit", "1");
		request.put("startDate", Common.currentDate1());
		request.put("endDate", Common.currentDate1());
		request.put("apiToken", apiToken);

		Response response =given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://jenkins-app.callhippo.com/activityfeed");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString);
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String callType = jsonPathEvaluator.getString("data.callLogs.callType[0]");
		String toName = jsonPathEvaluator.getString("data.callLogs.toName[0]");
		String to = jsonPathEvaluator.getString("data.callLogs.to[0]");
		String from = jsonPathEvaluator.getString("data.callLogs.from[0]");
		String fromName = jsonPathEvaluator.getString("data.callLogs.fromName[0]");
		String caller = jsonPathEvaluator.getString("data.callLogs.caller[0]");
		String callStatus = jsonPathEvaluator.getString("data.callLogs.callStatus[0]");
		String callDuration = jsonPathEvaluator.getString("data.callLogs.callDuration[0]");
		System.out.println("call hippo activity ::  "+callType+" Call From "+fromName+" To "+to+"("+toName+")"+" - "+callStatus+" ("+callDuration+")");
		
		String activity = callType+" Call From "+fromName+" To "+to+" - "+callStatus+" ("+callDuration+")";
		
		return activity;
		
		//Incoming Call From jayadip pstn4 To +13513000476(local acc1) - Completed (00:06)
		//Incoming Call From jayadip pstn4 To +13513000476 - Missed
		
		
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
//		Outgoing Call To jayadip pstn2 From +13513000476(local acc1) - Cancelled
		
		
		
	}
	
}
