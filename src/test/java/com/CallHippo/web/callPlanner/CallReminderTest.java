package com.CallHippo.web.callPlanner;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class CallReminderTest {

	DialerIndex phoneApp1;
	WebDriver driver;
	DialerIndex phoneApp2;
	WebDriver driver1;
	DialerIndex phoneApp2Subuser;
	WebDriver driver6;
	DialerIndex phoneApp3;
	WebDriver driver2;
	WebDriver driver3;

	WebToggleConfiguration webApp2;
	HolidayPage webApp2HolidayPage;
	WebToggleConfiguration webApp2Subuser;
	WebToggleConfiguration webApp1;
	DialerIndex webApp2sDialer;
	DialerIndex webApp2sSubuserDialer;
	WebDriver driver4;
	WebDriver driver5;

	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String acc2Number2 = data.getValue("account2Number2");
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");
	String number3 = data.getValue("account3Number1"); // account 3's number
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	
	Common excelTestResult;

	public CallReminderTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");

	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {

		driver4 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2sDialer = PageFactory.initElements(driver4, DialerIndex.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);


		driver5 = TestBase.init();
		System.out.println("Opened webaap session");
		webApp2Subuser = PageFactory.initElements(driver5, WebToggleConfiguration.class);
		webApp2sSubuserDialer = PageFactory.initElements(driver5, DialerIndex.class);
//
//			driver3 = TestBase.init();
//			System.out.println("Opened webaap session");
//			webApp1 = PageFactory.initElements(driver3, WebToggleConfiguration.class);
//
		driver = TestBase.init_dialer();
		System.out.println("Opened phoneAap1 session");
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2 session");
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver6 = TestBase.init_dialer();
		System.out.println("Opened phoneAap2Subuser session");
		phoneApp2Subuser = PageFactory.initElements(driver6, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		System.out.println("Opened phoneAap3 session");
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		try {
			try {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			} catch (Exception e) {
				driver4.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap signin Page");
				loginWeb(webApp2, account2Email, account2Password);
			}

			try {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap2SubUser signin Page");
				loginWeb(webApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			} catch (Exception e) {
				driver5.get(url.signIn());
				Common.pause(4);
				System.out.println("Opened webaap2Subuser signin Page");
				loginWeb(webApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			}

//
//				try {
//					driver3.get(url.signIn());
//					Common.pause(4);
//					System.out.println("Opened webaap signin Page");
//					loginWeb(webApp1, account1Email, account1Password);
//				} catch (Exception e) {
//					driver3.get(url.signIn());
//					Common.pause(4);
//					System.out.println("Opened webaap signin Page");
//					loginWeb(webApp1, account1Email, account1Password);
//				}
//
			try {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap1 signin page");
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2 signin page");
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}

			try {
				driver6.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2Subuser signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				System.out.println("loggedin phoneAap2Subuser");
			} catch (Exception e) {
				driver6.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap2Subuser signin page");
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				System.out.println("loggedin phoneAap2Subuser");
			}

			try {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				Common.pause(4);
				System.out.println("Opened phoneAap3 signin page");
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2Subuser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Subuser.getCallerName();
				driver6.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);

				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			} catch (Exception e) {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2Subuser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2Subuser.getCallerName();
				driver6.get(url.dialerSignIn());
				
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();

				webApp2.navigateToNumberSettingpage(number);

				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driver1.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

			}

		} catch (Exception e) {
			String testname = "Normal calling Before Method ";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Fail login");
			Common.Screenshot(driver6, testname, "PhoneApp2Subuser Fail login");
//				Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
//				driver3.get(url.signIn());
			driver4.get(url.signIn());
			driver.navigate().to(url.dialerSignIn());
			driver1.navigate().to(url.dialerSignIn());
			driver2.navigate().to(url.dialerSignIn());
			driver5.navigate().to(url.signIn());
			driver6.navigate().to(url.dialerSignIn());

			Common.pause(3);
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (webApp2Subuser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}

			phoneApp2.clickOnSideMenu();
			phoneApp2.clickOnCallPlannerLeftMenu();
			phoneApp2.waitforFirstEntryOfCallReminder();
			Common.pause(10);
			phoneApp2.deleteAllRemindersFromCallPlanner();

			phoneApp2Subuser.clickOnSideMenu();
			phoneApp2Subuser.clickOnCallPlannerLeftMenu();
			phoneApp2Subuser.waitforFirstEntryOfCallReminder();
			Common.pause(10);
			phoneApp2Subuser.deleteAllRemindersFromCallPlanner();

		} catch (Exception e) {

			try {
				Common.pause(2);
//					driver3.get(url.signIn());
				driver4.get(url.signIn());
				driver.navigate().to(url.dialerSignIn());
				driver1.navigate().to(url.dialerSignIn());
				driver2.navigate().to(url.dialerSignIn());
				driver5.navigate().to(url.signIn());
				driver6.navigate().to(url.dialerSignIn());

				Common.pause(3);
				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (webApp2Subuser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}

				phoneApp2.clickOnSideMenu();
				phoneApp2.clickOnCallPlannerLeftMenu();
				phoneApp2.waitforFirstEntryOfCallReminder();
				Common.pause(10);
				phoneApp2.deleteAllRemindersFromCallPlanner();

				phoneApp2Subuser.clickOnSideMenu();
				phoneApp2Subuser.clickOnCallPlannerLeftMenu();
				phoneApp2Subuser.waitforFirstEntryOfCallReminder();
				Common.pause(10);
				phoneApp2Subuser.deleteAllRemindersFromCallPlanner();

			} catch (Exception e1) {
				String testname = "Normal calling Before Method ";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driver5, testname, "WebAppp2Subuser Fail login");
				Common.Screenshot(driver6, testname, "PhoneApp2Subuser Fail login");
//					Common.Screenshot(driver3, testname, "WebAppp1 Fail login");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}

	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}
		
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneApp2Subuser Fail " + result.getMethod().getMethodName());
//				Common.Screenshot(driver3, testname, "WebAppp1 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver5, testname, "WebAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneApp2Subuser Pass " + result.getMethod().getMethodName());
//				Common.Screenshot(driver3, testname, "WebAppp1 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() throws IOException {

		try {

			driver1.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
		} catch (Exception e) {

			driver1.get(url.dialerSignIn());
			phoneApp2.deleteAllContactsFromDialer();
		} finally {
			driver.quit();
			driver1.quit();
			driver2.quit();
			driver4.quit();
			driver5.quit();
			driver6.quit();
//			driver3.quit();
		}
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}


	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_incoming_Call() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_Toggle_is_Off_in_incoming_Call() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("off");
		Common.pause(3);
		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();
		assertEquals(phoneApp2.validateCallReminderPopupIsDisplayed(), false);

	}

	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_Make_Call_from_Call_Planner() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		phoneApp2.clickOnCallBtnOnCallReminderPage();

		assertEquals(number2, phoneApp2.validateCallingScreenOutgoingNumber());
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2.validateDurationInCallingScreen(1);
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		driver1.get(url.dialerSignIn());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2.ValidateNoCallsInCallReminder(), true);
		phoneApp2.clickOnCompltedOnCallPlanner();

		assertEquals(number2, phoneApp2.validateFirstEntryOfCompletedOnOfCallPlanner());

	}

	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Verify_the_ordering_of_the_call_Reminders() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number);
		phoneApp2.clickOnDialButton();
		Common.pause(4);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("30 Min");

		Common.pause(5);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(4);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");

		Common.pause(5);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		Common.pause(4);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("1 Hour");

		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);
		assertEquals(phoneApp2.validateSecondEntryOfCallReminder(), number);
		assertEquals(phoneApp2.validateThirdEntryOfCallReminder(), number3);

	}

	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_incoming_Call_Missed() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		Common.pause(10);
		phoneApp2.waitForDialerPage();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("1 Hour");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_incoming_Call_Completed() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("1 Week");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_Completed() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("1 Week");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_NoAnswer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp1.waitForDialerPage();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("1 Hour");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_Rejected() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp1.clickOnIncomingRejectButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

	}

	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_Make_Call_from_Call_Planner_From_Web() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver4.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.signIn());

		driver1.get(url.dialerSignIn());
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);
	    webApp2.clickOnOpenDialerButton();
	  
	    webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
	    webApp2sDialer.waitForDialerPage();
	    driver4.close();
	    webApp2.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");
		webApp2.clickOnMakeTheCallFromCallPlanner();

		webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
		assertEquals(webApp2sDialer.validateCallingScreenOutgoingNumber(), number2);
		phoneApp1.clickOnIncomimgAcceptCallButton();

		webApp2sDialer.validateDurationInCallingScreen(1);
		Common.pause(10);
		webApp2sDialer.clickOnOutgoingHangupButton();

		driver4.get(url.dialerSignIn());

		webApp2sDialer.clickOnSideMenu();
		webApp2sDialer.clickOnCallPlannerLeftMenu();
		assertEquals(webApp2sDialer.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
		webApp2sDialer.clickOnCompltedOnCallPlanner();

		assertEquals(number2, webApp2sDialer.validateFirstEntryOfCompletedOnOfCallPlanner());
        driver4.close();
		webApp2.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		assertEquals(webApp2.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

		webApp2.clickOnCompletedOnFromCallPlanner();

		assertEquals(webApp2.getNameFromCallPlannerCompletedOn(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlannerCompletedOn(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlannerCompletedOn(), number2);

	}

	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_Void_The_Call_from_Call_Planner_From_Web() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver4.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.signIn());

		driver1.get(url.dialerSignIn());
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		webApp2.voidTheCallofAllCallPlannerEntries();
//			webApp2.clickOnYesOfCallPlanner();
		assertEquals(webApp2.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

	}

	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Incoming_Call_Void_The_Call_from_Call_Planner_From_Web() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver4.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.signIn());

		driver1.get(url.dialerSignIn());
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(10);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		webApp2.voidTheCallofAllCallPlannerEntries();
//			webApp2.clickOnYesOfCallPlanner();
		assertEquals(webApp2.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_incoming_Call_Make_Call_from_Call_Planner_From_Web() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver4.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.signIn());

		driver1.get(url.dialerSignIn());
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		webApp2.clickOnOpenDialerButton();
		  try {
	        webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
		    webApp2sDialer.waitForDialerPage();
		  }catch (Exception e) {
			  driver4.get(url.dialerSignIn());
			  System.out.println("Dialer not opened in single click");
			  webApp2sDialer.waitForDialerPage();
		}
		driver4.close();
		webApp2.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");
		webApp2.clickOnMakeTheCallFromCallPlanner();

		webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
		assertEquals(webApp2sDialer.validateCallingScreenOutgoingNumber(), number2);
		phoneApp1.clickOnIncomimgAcceptCallButton();

		webApp2sDialer.validateDurationInCallingScreen(1);
		Common.pause(10);
		webApp2sDialer.clickOnOutgoingHangupButton();

		driver4.get(url.dialerSignIn());

		webApp2sDialer.clickOnSideMenu();
		webApp2sDialer.clickOnCallPlannerLeftMenu();
		assertEquals(webApp2sDialer.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
		webApp2sDialer.clickOnCompltedOnCallPlanner();

		assertEquals(number2, webApp2sDialer.validateFirstEntryOfCompletedOnOfCallPlanner());
		driver4.close();
		webApp2.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		assertEquals(webApp2.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

		webApp2.clickOnCompletedOnFromCallPlanner();

		assertEquals(webApp2.getNameFromCallPlannerCompletedOn(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlannerCompletedOn(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlannerCompletedOn(), number2);

	}

	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_incoming_Call_Make_Call_from_Call_Planner_From_Web_Subuser() throws Exception {

		driver6.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver5.get(url.dialerSignIn());
		Common.pause(5);
		driver5.get(url.signIn());

		driver6.get(url.dialerSignIn());
		if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);
 
		webApp2Subuser.clickOnOpenDialerButton();
		  
		webApp2Subuser.switchToWindowBasedOnTitle("CallHippo Dialer");
		webApp2sSubuserDialer.waitForDialerPage();
		driver5.close();
		webApp2Subuser.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");
		webApp2Subuser.clickOnMakeTheCallFromCallPlanner();

		webApp2Subuser.switchToWindowBasedOnTitle("CallHippo Dialer");
		assertEquals(webApp2sSubuserDialer.validateCallingScreenOutgoingNumber(), number2);
		phoneApp1.clickOnIncomimgAcceptCallButton();

		webApp2sSubuserDialer.validateDurationInCallingScreen(1);
		Common.pause(10);
		webApp2sSubuserDialer.clickOnOutgoingHangupButton();

		driver5.get(url.dialerSignIn());

		webApp2sSubuserDialer.clickOnSideMenu();
		webApp2sSubuserDialer.clickOnCallPlannerLeftMenu();
		assertEquals(webApp2sSubuserDialer.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
		webApp2sSubuserDialer.clickOnCompltedOnCallPlanner();

		assertEquals(number2, webApp2sSubuserDialer.validateFirstEntryOfCompletedOnOfCallPlanner());
		driver5.close();
		webApp2Subuser.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		assertEquals(webApp2Subuser.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

		webApp2Subuser.clickOnCompletedOnFromCallPlanner();

		assertEquals(webApp2Subuser.getNameFromCallPlannerCompletedOn(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlannerCompletedOn(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlannerCompletedOn(), number2);

	}

	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_outgoing_Call_Make_Call_from_Call_Planner_From_Web_Subuser() throws Exception {

		driver6.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver5.get(url.dialerSignIn());
		Common.pause(5);
		driver5.get(url.signIn());

		driver6.get(url.dialerSignIn());
		if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}

		phoneApp2Subuser.enterNumberinDialer(number2);
		phoneApp2Subuser.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp2Subuser.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);
		

		webApp2Subuser.clickOnOpenDialerButton();
		 try {
			 webApp2Subuser.switchToWindowBasedOnTitle("CallHippo Dialer");
			 webApp2sSubuserDialer.waitForDialerPage();
			  }catch (Exception e) {
			   driver5.get(url.dialerSignIn());
			   System.out.println("Dialer not opened in single click");
			   webApp2sSubuserDialer.waitForDialerPage();
			}
		driver5.close();
		webApp2Subuser.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");
		webApp2Subuser.clickOnMakeTheCallFromCallPlanner();

		webApp2Subuser.switchToWindowBasedOnTitle("CallHippo Dialer");
		assertEquals(webApp2sSubuserDialer.validateCallingScreenOutgoingNumber(), number2);
		phoneApp1.clickOnIncomimgAcceptCallButton();

		webApp2sSubuserDialer.validateDurationInCallingScreen(1);
		Common.pause(10);
		webApp2sSubuserDialer.clickOnOutgoingHangupButton();

		driver5.get(url.dialerSignIn());

		webApp2sSubuserDialer.clickOnSideMenu();
		webApp2sSubuserDialer.clickOnCallPlannerLeftMenu();
		assertEquals(webApp2sSubuserDialer.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
		webApp2sSubuserDialer.clickOnCompltedOnCallPlanner();

		assertEquals(number2, webApp2sSubuserDialer.validateFirstEntryOfCompletedOnOfCallPlanner());
		driver5.close();
		webApp2Subuser.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		assertEquals(webApp2Subuser.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

		webApp2Subuser.clickOnCompletedOnFromCallPlanner();

		assertEquals(webApp2Subuser.getNameFromCallPlannerCompletedOn(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlannerCompletedOn(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlannerCompletedOn(), number2);

	}

	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Outgoing_Call_Void_The_Call_from_Call_Planner_From_Web_Subuser()
			throws Exception {

		driver6.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver5.get(url.dialerSignIn());
		Common.pause(5);
		driver5.get(url.signIn());

		driver6.get(url.dialerSignIn());
		if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}

		phoneApp2Subuser.enterNumberinDialer(number2);
		phoneApp2Subuser.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp2Subuser.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("Tomorrow");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);

		webApp2Subuser.voidTheCallofAllCallPlannerEntries();
//			webApp2.clickOnYesOfCallPlanner();
		assertEquals(webApp2Subuser.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

	}

	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Incoming_Call_Void_The_Call_from_Call_Planner_From_Web_Subuser()
			throws Exception {

		driver6.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver5.get(url.dialerSignIn());
		Common.pause(5);
		driver5.get(url.signIn());

		driver6.get(url.dialerSignIn());
		if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("Tomorrow");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);

		webApp2Subuser.voidTheCallofAllCallPlannerEntries();
//			assertEquals(webApp2Subuser.validationMessage(), "Reminder deleted successfully.");
//			webApp2.clickOnYesOfCallPlanner();
		assertEquals(webApp2Subuser.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

	}
	

	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Rescedule_call_Reminder_of_Unsaved_Number_in_Mainuser_Dialer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);
		
		
		phoneApp2.clickOnRemainderBtnOnReminderPage("15 Min");
		phoneApp2.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer = phoneApp2.GetDateAndTimeOfRemainder();
		String RescheduleTime = Common.ResceduleTimeofRemainderDialer(15, 0, 0);
		 String RescheduleTimeofWeb = Common.ResceduleTimeofRemainderWeb(15, 0, 0);
		System.out.println(timeofResceduleDialer);
		System.out.println(RescheduleTime);
		assertEquals(timeofResceduleDialer, RescheduleTime);
		
		driver4.get(url.signIn());
		webApp2.clickOnCallPlannerSideMenu();
		Common.pause(5);
		
	    String Timeofremainderinweb = webApp2.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb, RescheduleTimeofWeb);
	    
	    
	    phoneApp2.clickOnRemainderBtnOnReminderPage("30 Min");
		phoneApp2.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer1 = phoneApp2.GetDateAndTimeOfRemainder();
		String RescheduleTime1 = Common.ResceduleTimeofRemainderDialer(30, 0, 0);
		 String RescheduleTimeofWeb1 = Common.ResceduleTimeofRemainderWeb(30, 0, 0);
		System.out.println(timeofResceduleDialer1);
		System.out.println(RescheduleTime1);
		assertEquals(timeofResceduleDialer1, RescheduleTime1);
		
		driver4.get(url.signIn());
		webApp2.clickOnCallPlannerSideMenu();
		Common.pause(5);
		
	    String Timeofremainderinweb1 = webApp2.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb1, RescheduleTimeofWeb1);
		

	}
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Rescedule_call_Reminder_of_Unsaved_Number_in_SubUser_Dialer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver6.get(url.dialerSignIn());
		phoneApp2Subuser.waitForDialerPage();
		phoneApp2Subuser.enterNumberinDialer(number2);
		phoneApp2Subuser.clickOnDialButton();
		Common.pause(10);
		phoneApp2Subuser.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerNam2sub = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerNam2sub);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "Unknown Caller");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);
		
		phoneApp2Subuser.clickOnRemainderBtnOnReminderPage("1 Hour");
		phoneApp2Subuser.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer = phoneApp2Subuser.GetDateAndTimeOfRemainder();
		String RescheduleTime = Common.ResceduleTimeofRemainderDialer(0, 1, 0);
		 String RescheduleTimeofWeb = Common.ResceduleTimeofRemainderWeb(0, 1, 0);
		System.out.println(timeofResceduleDialer);
		System.out.println(RescheduleTime);
		assertEquals(timeofResceduleDialer, RescheduleTime);
		
		driver5.get(url.signIn());
		webApp2Subuser.clickOnCallPlannerSideMenu();
		Common.pause(5);
		
	    String Timeofremainderinweb = webApp2Subuser.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb, RescheduleTimeofWeb);
	    
	    phoneApp2Subuser.clickOnRemainderBtnOnReminderPage("2 Hour");
	    phoneApp2Subuser.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer1 = phoneApp2Subuser.GetDateAndTimeOfRemainder();
		String RescheduleTime1 = Common.ResceduleTimeofRemainderDialer(0, 2, 0);
		String RescheduleTimeofWeb1 = Common.ResceduleTimeofRemainderWeb(0, 2, 0);
		System.out.println(timeofResceduleDialer1);
		System.out.println(RescheduleTime1);
		assertEquals(timeofResceduleDialer1, RescheduleTime1);
		
		driver5.get(url.signIn());
		webApp2Subuser.clickOnCallPlannerSideMenu();
		Common.pause(5);
		
	    String Timeofremainderinweb1 = webApp2Subuser.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb1, RescheduleTimeofWeb1);
	}

	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.AddContact("AutoContact1");
		driver1.get(url.dialerSignIn());
		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		driver1.get(url.dialerSignIn());
		Common.pause(1);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		phoneApp2.clickOnCallBtnOnCallReminderPage();

		assertEquals("AutoContact1", phoneApp2.validateExtCallOutgoingNameCallingScreen());
		assertEquals(number2, phoneApp2.validateSubUserOutgoingNumberCallingScreen());

		phoneApp1.waitForIncomingCallingScreen();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		driver1.get(url.dialerSignIn());
		Common.pause(1);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);
	}

	@Test(priority = 22, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_No_Answer_And_Void_From_Dailer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		driver1.get(url.dialerSignIn());
		Common.pause(1);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		phoneApp2.deleteAllRemindersFromCallPlanner();
		assertEquals(phoneApp2Subuser.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
	}

	@Test(priority = 23, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_Of_Saved_Contact_Incoming_Call_Rejected_And_Make_Outgoing_Call() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		phoneApp2.clickOnCallBtnOnCallReminderPage();

		assertEquals("AutoContact1", phoneApp2.validateExtCallOutgoingNameCallingScreen());
		assertEquals(number2, phoneApp2.validateSubUserOutgoingNumberCallingScreen());
		phoneApp2.clickOnOutgoingHangupButton();
	}

	@Test(priority = 24, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_Of_Saved_Contact_Incoming_Completed_Call_And_Make_Outgoing_Call()
			throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(3);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		phoneApp2.clickOnCallBtnOnCallReminderPage();

		assertEquals("AutoContact1", phoneApp2.validateExtCallOutgoingNameCallingScreen());
		assertEquals(number2, phoneApp2.validateSubUserOutgoingNumberCallingScreen());
		phoneApp2.clickOnOutgoingHangupButton();
	}

	@Test(priority = 25, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_Of_Saved_Contact_Incoming_Call_And_Void_From_Subuser_Dailer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		Common.pause(10);
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("15 Min");
		Common.pause(5);
		
		driver6.get(url.dialerSignIn());
		
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2Subuser.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);

		phoneApp2Subuser.deleteAllRemindersFromCallPlanner();
		assertEquals(phoneApp2Subuser.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
	}

	@Test(priority = 26, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Saved_Contact_Outgoing_Call_Make_Call_from_Call_Planner_From_Web()
			throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver4.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.signIn());

		driver1.get(url.dialerSignIn());
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		webApp2.clickOnOpenDialerButton();
		  
		webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
		webApp2sDialer.waitForDialerPage();
			driver4.close();
			webApp2.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");
		webApp2.clickOnMakeTheCallFromCallPlanner();

		webApp2.switchToWindowBasedOnTitle("CallHippo Dialer");
		assertEquals("AutoContact1", webApp2sDialer.validateExtCallOutgoingNameCallingScreen());
		assertEquals(webApp2sDialer.validateSubUserOutgoingNumberCallingScreen(), number2);
		phoneApp1.clickOnIncomimgAcceptCallButton();

		webApp2sDialer.validateDurationInCallingScreen(1);
		Common.pause(10);
		webApp2sDialer.clickOnOutgoingHangupButton();

		driver4.get(url.dialerSignIn());

		webApp2sDialer.clickOnSideMenu();
		webApp2sDialer.clickOnCallPlannerLeftMenu();
		try {
			assertEquals(webApp2sDialer.ValidateNoCallsInCallReminder(), true,
					" no call logs in call planner page in dialer");
			}catch (Exception e) {	
				webApp2sDialer.hardRefreshByKeyboard();
			assertEquals(webApp2sDialer.ValidateNoCallsInCallReminder(), true,
						" no call logs in call planner page in dialer after hard relod");	
		    }
		webApp2sDialer.clickOnCompltedOnCallPlanner();

		assertEquals(number2, webApp2sDialer.validateFirstEntryOfCompletedOnOfCallPlanner());
		driver4.close();
		webApp2.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		assertEquals(webApp2.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

		webApp2.clickOnCompletedOnFromCallPlanner();

		assertEquals(webApp2.getNameFromCallPlannerCompletedOn(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlannerCompletedOn(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlannerCompletedOn(), number2);

	}

	@Test(priority = 27, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Saved_Contact_incoming_Call_Make_Call_from_Call_Planner_From_Web_Subuser()
			throws Exception {

		driver6.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver5.get(url.dialerSignIn());
		Common.pause(5);
		driver5.get(url.signIn());

		driver6.get(url.dialerSignIn());
		if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2Subuser.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2Subuser.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);
		webApp2Subuser.clickOnOpenDialerButton();
		  
		webApp2Subuser.switchToWindowBasedOnTitle("CallHippo Dialer");
		webApp2sSubuserDialer.waitForDialerPage();
		driver5.close();
			webApp2Subuser.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		webApp2Subuser.clickOnMakeTheCallFromCallPlanner();

		webApp2Subuser.switchToWindowBasedOnTitle("CallHippo Dialer");
		assertEquals(webApp2sSubuserDialer.validateSubUserOutgoingNumberCallingScreen(), number2);
		phoneApp1.clickOnIncomimgAcceptCallButton();

		webApp2sSubuserDialer.validateDurationInCallingScreen(1);
		Common.pause(10);
		webApp2sSubuserDialer.clickOnOutgoingHangupButton();

		driver5.get(url.dialerSignIn());

		webApp2sSubuserDialer.clickOnSideMenu();
		webApp2sSubuserDialer.clickOnCallPlannerLeftMenu();
		try {
		assertEquals(webApp2sSubuserDialer.ValidateNoCallsInCallReminder(), true,
				" no call logs in call planner page in dialer");
		}catch (Exception e) {
			
			webApp2sSubuserDialer.hardRefreshByKeyboard();
			assertEquals(webApp2sSubuserDialer.ValidateNoCallsInCallReminder(), true,
					" no call logs in call planner page in dialer");
		
		}
		webApp2sSubuserDialer.clickOnCompltedOnCallPlanner();

		assertEquals(number2, webApp2sSubuserDialer.validateFirstEntryOfCompletedOnOfCallPlanner());
		driver5.close();
		webApp2Subuser.switchToWindowBasedOnTitle("Call Planner | Callhippo.com");

		assertEquals(webApp2Subuser.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

		webApp2Subuser.clickOnCompletedOnFromCallPlanner();

		assertEquals(webApp2Subuser.getNameFromCallPlannerCompletedOn(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlannerCompletedOn(), "AutoContact1");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlannerCompletedOn(), number2);

	}

	@Test(priority = 28, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Saved_Contact_Outgoing_Call_Void_The_Call_from_Call_Planner_From_Web()
			throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver4.get(url.dialerSignIn());
		Common.pause(5);
		driver4.get(url.signIn());

		driver1.get(url.dialerSignIn());
		if (phoneApp2.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2, account2Email, account2Password);
			Common.pause(3);
		}

		phoneApp2.enterNumberinDialer(number2);
		phoneApp2.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp2.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);

		webApp2.voidTheCallofAllCallPlannerEntries();
		assertEquals(webApp2.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");
	}

	@Test(priority = 29, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_call_Reminder_in_Saved_Contact_Outgoing_Call_Void_The_Call_from_Call_Planner_From_Web_Subuser()
			throws Exception {

		driver6.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver5.get(url.dialerSignIn());
		Common.pause(5);
		driver5.get(url.signIn());

		driver6.get(url.dialerSignIn());
		if (phoneApp2Subuser.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp2Subuser, account2EmailSubUser, account2PasswordSubUser);
			Common.pause(3);
		}

		phoneApp2Subuser.enterNumberinDialer(number2);
		phoneApp2Subuser.clickOnDialButton();
		phoneApp1.clickOnIncomimgAcceptCallButton();
		phoneApp2Subuser.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp2Subuser.clickOnOutgoingHangupButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("Tomorrow");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();
		assertEquals(phoneApp2Subuser.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);

		webApp2Subuser.voidTheCallofAllCallPlannerEntries();
		assertEquals(webApp2Subuser.validateNoDataOnCallPlanner(), true, " no data in call planner page in webpage");

	}
	
	
	
	@Test(priority = 30, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Rescedule_call_Reminder_of_saved_Number_in_MAinUser_Dialer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		Common.pause(10);
		phoneApp2.clickOnIncomingRejectButton();

		phoneApp2.clickOnCallReminderDropdown();
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2.clickOnSideMenu();
		String callerName1 = phoneApp2.getCallerName();
		phoneApp2.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2.validateFirstEntryOfCallReminder(), number2);

		webApp2.clickOnCallPlannerSideMenu();

		assertEquals(webApp2.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2.getContactNumberFromCallPlanner(), number2);
		
		phoneApp2.clickOnRemainderBtnOnReminderPage("Tomorrow");
		phoneApp2.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer = phoneApp2.GetDateAndTimeOfRemainder();
		String RescheduleTime = Common.ResceduleTimeofRemainderDialer(0, 0, 1);
		 String RescheduleTimeofWeb = Common.ResceduleTimeofRemainderWeb(0, 0, 1);
		System.out.println(timeofResceduleDialer);
		System.out.println(RescheduleTime);
		assertEquals(timeofResceduleDialer, RescheduleTime);
		
		driver4.get(url.signIn());
		webApp2.clickOnCallPlannerSideMenu();
		Common.pause(5);
		
	    String Timeofremainderinweb = webApp2.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb, RescheduleTimeofWeb);
	    
	    
	    phoneApp2.clickOnRemainderBtnOnReminderPage("1 Week");
		phoneApp2.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer1 = phoneApp2.GetDateAndTimeOfRemainder();
		String RescheduleTime1 = Common.ResceduleTimeofRemainderDialer(0, 0, 7);
		 String RescheduleTimeofWeb1 = Common.ResceduleTimeofRemainderWeb(0, 0, 7);
		System.out.println(timeofResceduleDialer1);
		System.out.println(RescheduleTime1);
		assertEquals(timeofResceduleDialer1, RescheduleTime1);
		
		driver4.get(url.signIn());
		webApp2.clickOnCallPlannerSideMenu();
		Common.pause(5);
		
	    String Timeofremainderinweb1 = webApp2.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb1, RescheduleTimeofWeb1);

}
	
	@Test(priority = 31, dependsOnMethods = "verify_call_Reminder_Of_Saved_Contact_Outgoing_Call_And_Make_Outgoing_Call", retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void verify_Rescedule_call_Reminder_of_saved_Number_in_SubUser_Dialer() throws Exception {

		driver1.get(url.dialerSignIn());
		Common.pause(3);
		phoneApp2Subuser.callReminderToggle("on");
		Common.pause(3);

		driver1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		Common.pause(10);
		phoneApp2Subuser.clickOnIncomingRejectButton();

		phoneApp2Subuser.clickOnCallReminderDropdown();
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("15 Min"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("30 Min"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("1 Hour"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("2 Hour"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("Tomorrow"), true);
		assertEquals(phoneApp2Subuser.validateCallReminderDropdownOptions("1 Week"), true);
		Common.pause(3);
		phoneApp2Subuser.setCallReminder("15 Min");
		Common.pause(5);
		phoneApp2Subuser.clickOnSideMenu();
		String callerName1 = phoneApp2Subuser.getCallerName();
		phoneApp2Subuser.clickOnCallPlannerLeftMenu();

		assertEquals(phoneApp2Subuser.validateSavedNameFirstEntryOfCallReminder(), "AutoContact1");
		assertEquals(phoneApp2Subuser.validateFirstEntryOfCallReminder(), number2);

		webApp2Subuser.clickOnCallPlannerSideMenu();

		assertEquals(webApp2Subuser.getNameFromCallPlanner(), callerName1);
		assertEquals(webApp2Subuser.getContactNameFromCallPlanner(), "AutoContact1");
		assertEquals(webApp2Subuser.getContactNumberFromCallPlanner(), number2);
		
		phoneApp2Subuser.clickOnRemainderBtnOnReminderPage("30 Min");
		phoneApp2Subuser.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer = phoneApp2Subuser.GetDateAndTimeOfRemainder();
		String RescheduleTime = Common.ResceduleTimeofRemainderDialer(30, 0, 0);
		 String RescheduleTimeofWeb = Common.ResceduleTimeofRemainderWeb(30, 0, 0);
		System.out.println(timeofResceduleDialer);
		System.out.println(RescheduleTime);
		assertEquals(timeofResceduleDialer, RescheduleTime);

		driver5.get(url.signIn());
		webApp2Subuser.clickOnCallPlannerSideMenu();
		Common.pause(5);
	    String Timeofremainderinweb = webApp2Subuser.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb, RescheduleTimeofWeb);
	    
	    phoneApp2Subuser.clickOnRemainderBtnOnReminderPage("2 Hour");
	    phoneApp2Subuser.GetsuccessMsgforResceduleremainder();
		String timeofResceduleDialer1 = phoneApp2Subuser.GetDateAndTimeOfRemainder();
		String RescheduleTime1 = Common.ResceduleTimeofRemainderDialer(0, 2, 0);
		String RescheduleTimeofWeb1 = Common.ResceduleTimeofRemainderWeb(0, 2, 0);
		System.out.println(timeofResceduleDialer1);
		System.out.println(RescheduleTime1);
		assertEquals(timeofResceduleDialer1, RescheduleTime1);
		
		driver5.get(url.signIn());
		webApp2Subuser.clickOnCallPlannerSideMenu();
		Common.pause(5);
	    String Timeofremainderinweb1 = webApp2Subuser.getTimeofRemainderFromCallPlanner();
	    assertEquals(Timeofremainderinweb1, RescheduleTimeofWeb1);

}
	

}
