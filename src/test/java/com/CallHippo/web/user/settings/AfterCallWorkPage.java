package com.CallHippo.web.user.settings;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;

public class AfterCallWorkPage {
	
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;

	public AfterCallWorkPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		js = (JavascriptExecutor) driver;
	}
	
	
	@FindBy(xpath = "//h3[contains(.,'After Call Work')]")
	WebElement acw_Txt;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_title')]]/span[contains(.,'After Call Work')]")
	WebElement acw_toggle_Txt;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]")
	WebElement acw_desc_Txt;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//p[contains(.,'Duration')]")
	WebElement acw_duration_Txt;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//p[contains(.,'Duration')]/i")
	WebElement acw_duration_info_icon;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//p[2]")
	WebElement acw_duration_num_Txt;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//i[@Class='material-icons'][contains(.,'edit')]")
	WebElement acw_duration_edit_icon;
	
	@FindBy(xpath = "//button[@test-automation='user_after_call_work_switch']")
	WebElement userAfterCallwork;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//input[@title[contains(.,'After Call Work')]]")
	WebElement acw_duration_textBox;
	
	@FindBy(xpath = "//section[@id='afterCallWork']//div[@class[contains(.,'deparmentname_description')]]//following-sibling::div//i[@Class='material-icons'][contains(.,'save')]")
	WebElement acw_duration_save_icon;
	
	@FindBy(xpath = "//div[@class='brdrcountdown']//div[@class='afterclduration']")
	WebElement dialer_acw_duration;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement errorMessage;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	WebElement successMessage;
	
	
	
	public void scrollToACWTxt() {
		wait.until(ExpectedConditions.visibilityOf(acw_Txt));
		js.executeScript("arguments[0].scrollIntoView();", acw_Txt);
	}
	
	public boolean velidateACWFields() {
		boolean allFieldsAvailable;
		if (acw_toggle_Txt.isDisplayed()
				&& acw_desc_Txt.getText().equalsIgnoreCase("Allow some time after every call for your agents to process after call work.")
				&& acw_duration_Txt.isDisplayed()
				&& acw_duration_info_icon.isDisplayed()
				&& acw_duration_num_Txt.getText().equalsIgnoreCase("5")
				&& acw_duration_edit_icon.isDisplayed()
			) {
			allFieldsAvailable = true;
		} else {
			allFieldsAvailable = false;
		}
		return allFieldsAvailable;
	}

	public void setAfterCallWorkDuration(String duration) {

		if (userAfterCallwork.getAttribute("aria-checked").equals("true")) {
			acw_duration_edit_icon.click();
			Common.pause(1);
			int durationText = acw_duration_textBox.getAttribute("value").length();
			for(int i = 0; i < durationText; i++){
				acw_duration_textBox.sendKeys(Keys.BACK_SPACE);
			}
			Common.pause(1);
			acw_duration_textBox.sendKeys(duration);
			acw_duration_save_icon.click();
		}
	}
	
	public String successMessage() {
		wait.until(ExpectedConditions.visibilityOf(successMessage));
		Common.pause(1);
		return successMessage.getText();
	}
	
	public String errorMessage() {
		wait.until(ExpectedConditions.visibilityOf(errorMessage));
		Common.pause(1);
		return errorMessage.getText();
	}
	
	
	
}
