package com.CallHippo.web.user.settings;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.settings.CallBlockingPage;
import com.CallHippo.web.settings.holiday.HolidayPage;

public class AfterCallWorkTest {

	WebDriver driverWebApp1;
	WebDriver driverWebApp2;
	WebDriver driverPhoneApp1;
	WebDriver driverPhoneApp2;
	WebDriver driverPhoneApp3;
	WebDriver driverPhoneApp4;

	WebToggleConfiguration webApp1;
	WebToggleConfiguration webApp2;

	DialerIndex phoneApp1;
	DialerIndex phoneApp2;
	DialerIndex phoneApp3;
	DialerIndex phoneApp4;

	AfterCallWorkPage acwPageApp1;
	AfterCallWorkPage acwPageApp2;
	
	HolidayPage webApp2HolidayPage;
	CallingCallCostTest callingCallCostobj;

	RestAPI creditAPI;
	static Common excel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");

	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2SubEmail = data.getValue("account2SubEmail");
	String account2SubPassword = data.getValue("masterPassword");

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");

	String account1Number1 = data.getValue("account1Number1"); // account 1's number1
	String account2Number1 = data.getValue("account2Number1"); // account 2's number1
	String account3Number1 = data.getValue("account3Number1"); // account 3's number1
	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	
	Common excelTestResult;

     String outgoingCallPriceAcc2Num1;
	public AfterCallWorkTest() throws Exception {

		excel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		
		excelTestResult = new Common("ScreenShots\\TestCaseResults.xlsx");
		callingCallCostobj=new CallingCallCostTest();
	}

	// before test is opening all needed sessions
	@BeforeTest
	public void initialization() throws Exception {
		
		driverWebApp1 = TestBase.init();
		driverWebApp1.get(url.signIn());
		webApp1 = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
		acwPageApp1 = PageFactory.initElements(driverWebApp1, AfterCallWorkPage.class);

		driverWebApp2 = TestBase.init();
		driverWebApp2.get(url.signIn());
		webApp2 = PageFactory.initElements(driverWebApp2, WebToggleConfiguration.class);
		acwPageApp2 = PageFactory.initElements(driverWebApp2, AfterCallWorkPage.class);
		webApp2HolidayPage = PageFactory.initElements(driverWebApp2, HolidayPage.class);


		driverPhoneApp1 = TestBase.init_dialer();
		driverPhoneApp1.get(url.dialerSignIn());
		phoneApp1 = PageFactory.initElements(driverPhoneApp1, DialerIndex.class);

		driverPhoneApp2 = TestBase.init_dialer();
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp2 = PageFactory.initElements(driverPhoneApp2, DialerIndex.class);

		driverPhoneApp3 = TestBase.init_dialer();
		driverPhoneApp3.get(url.dialerSignIn());
		phoneApp3 = PageFactory.initElements(driverPhoneApp3, DialerIndex.class);

		driverPhoneApp4 = TestBase.init_dialer();
		driverPhoneApp4.get(url.dialerSignIn());
		phoneApp4 = PageFactory.initElements(driverPhoneApp4, DialerIndex.class);

		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		outgoingCallPriceAcc2Num1=callingCallCostobj.callCost(account2Number1,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPriceAcc2Num1);
		Common.pause(4);

		try {
			try {
				loginWeb(webApp1, account1Email, account1Password);
				System.out.println("loggedin webAap1");
			} catch (Exception e) {
				loginWeb(webApp1, account1Email, account1Password);
				System.out.println("loggedin webAap1");
			}

			try {
				loginWeb(webApp2, account2Email, account1Password);
				System.out.println("loggedin webAap2");
			} catch (Exception e) {
				loginWeb(webApp2, account2Email, account1Password);
				System.out.println("loggedin webAap2");
			}

			try {
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			} catch (Exception e) {
				loginDialer(phoneApp1, account1Email, account1Password);
				System.out.println("loggedin phoneAap1");
			}

			try {
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			} catch (Exception e) {
				loginDialer(phoneApp2, account2Email, account2Password);
				System.out.println("loggedin phoneAap2");
			}

			try {
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			} catch (Exception e) {
				loginDialer(phoneApp3, account3Email, account3Password);
				System.out.println("loggedin phoneAap3");
			}


			try {
				System.out.println("--- WebApp1 Settings--");
				Thread.sleep(9000);
				webApp1.numberspage();
				webApp1.navigateToNumberSettingpage(account1Number1);
				Thread.sleep(9000);
				webApp1.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp1.userTeamAllocation("users");
				Thread.sleep(2000);

			} catch (Exception e) {
				System.out.println("--- WebApp1 Settings--");
				Thread.sleep(9000);
				webApp1.numberspage();
				webApp1.navigateToNumberSettingpage(account1Number1);
				Thread.sleep(9000);
				webApp1.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp1.userTeamAllocation("users");
				Thread.sleep(2000);
			}

			try {
				
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverPhoneApp2.get(url.dialerSignIn());

				
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(account2Number1);
				Thread.sleep(9000);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(account2Number1);

				String Url = driverWebApp2.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2SubEmail);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(account2Number1);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverPhoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverPhoneApp2.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driverPhoneApp1.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverPhoneApp2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				

			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driverPhoneApp2.get(url.dialerSignIn());

				
				System.out.println("loggedin webApp");
				Thread.sleep(9000);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(account2Number1);
				Thread.sleep(9000);

				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				
				webApp2.setCallRecordingToggle("on");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Thread.sleep(9000);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(account2Number1);

				String Url = driverWebApp2.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));

				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2SubEmail);
				Thread.sleep(9000);
				webApp2.makeDefaultNumber(account2Number1);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

				driverPhoneApp2.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driverPhoneApp2.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driverPhoneApp1.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				driverPhoneApp2.get(url.dialerSignIn());
				
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();

				
			}

		} catch (Exception e) {
			String testname = "AfterCallWork - BeforeTest - ";
			Common.Screenshot(driverWebApp1, testname, "WebAppp1 Fail");
			Common.Screenshot(driverWebApp2, testname, "WebAppp2 Fail");
			Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Fail");
			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Fail");
			Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Fail");
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail");

			System.out.println("\n" + testname + " has been fail... \n");
		}
	}

	@BeforeMethod
	public void login() throws IOException, InterruptedException {
		try {
			Common.pause(2);
			driverWebApp1.get(url.signIn());
			driverWebApp2.get(url.signIn());
			driverPhoneApp1.navigate().to(url.dialerSignIn());
			driverPhoneApp2.navigate().to(url.dialerSignIn());
			driverPhoneApp3.navigate().to(url.dialerSignIn());
			driverPhoneApp4.navigate().to(url.dialerSignIn());

			if (webApp1.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp1, account1Email, account2Password);
			}
			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
			}
			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
			}
			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
			}
			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
			}
			

			Common.pause(3);

			webApp1.userspage();
			webApp1.navigateToUserSettingPage(account1Email);
			Thread.sleep(9000);
			acwPageApp1.scrollToACWTxt();
			acwPageApp1.setAfterCallWorkDuration("5");
			Common.pause(2);
			webApp1.setUserSttingToggles("off", "off", "off", "open", "off");

			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2Email);
			Thread.sleep(9000);
			acwPageApp1.scrollToACWTxt();
			acwPageApp2.setAfterCallWorkDuration("5");
			Common.pause(2);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		} catch (Exception e) {

			try {
				driverWebApp1.get(url.signIn());
				driverWebApp2.get(url.signIn());
				driverPhoneApp1.navigate().to(url.dialerSignIn());
				driverPhoneApp2.navigate().to(url.dialerSignIn());
				driverPhoneApp3.navigate().to(url.dialerSignIn());
				driverPhoneApp4.navigate().to(url.dialerSignIn());

				if (webApp1.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp1, account1Email, account2Password);
				}
				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
				}
				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
				}
				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
				}
				Common.pause(3);
			} catch (Exception e1) {
				String testname = "AfterCallWork - BeforeMethod - ";
				Common.Screenshot(driverWebApp1, testname, "WebAppp1 Fail");
				Common.Screenshot(driverWebApp2, testname, "WebAppp2 Fail");
				Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Fail");
				Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Fail");
				Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Fail");
				Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail");
				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, "WebAppp1 Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "WebAppp2 Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Fail - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail - " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, "WebAppp1 Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2, testname, "WebAppp2 Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp1, testname, "PhoneAppp1 Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp2, testname, "PhoneAppp2 Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp3, testname, "PhoneAppp3 Pass - " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Pass - " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driverWebApp1.quit();
		driverWebApp2.quit();
		driverPhoneApp1.quit();
		driverPhoneApp2.quit();
		driverPhoneApp3.quit();
		driverPhoneApp4.quit();
	}

	public void loginDialer(DialerIndex dialer, String email, String password) {
		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	@Test(priority = 1, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Web_ACW_fields() throws Exception {
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.velidateACWFields();

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.velidateACWFields();
	}

	@Test(priority = 2, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_ACW_toggles_web_dialer() throws Exception {
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.velidateACWFields();
		driverPhoneApp1.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnSetting();
		Thread.sleep(5000);
		assertEquals(true, phoneApp1.validateAfterCallWorkToggleIsOn());

		webApp1.userAfterCallWork("off");
		Common.pause(2);
		driverPhoneApp1.navigate().refresh();
		Common.pause(5);
		assertEquals(false, phoneApp1.validateAfterCallWorkToggleIsOn());
	}

	@Test(priority = 3, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_ACW_toggles_from_dialer_web() throws Exception {
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnSetting();
		Thread.sleep(5000);
		phoneApp1.clickAfterCallWorkToggle("on");
		driverWebApp1.navigate().refresh();
		Common.pause(5);
		assertEquals(true, webApp1.validateWebAfterCallWork());
		
		Common.pause(5);
		phoneApp1.clickAfterCallWorkToggle("off");
		driverWebApp1.navigate().refresh();
		Common.pause(5);
		assertEquals(false, webApp1.validateWebAfterCallWork());
	}
	
	@Test(priority = 4, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_Message_On_Web_ACW() throws Exception {

		acwPageApp1.scrollToACWTxt();
		webApp1.userAfterCallWork("on");
		Common.pause(2);

		acwPageApp1.setAfterCallWorkDuration("");
		assertEquals("Please enter valid value", acwPageApp1.errorMessage());
		Common.pause(2);

		driverWebApp1.navigate().refresh();
		Common.pause(2);
		acwPageApp1.scrollToACWTxt();
		acwPageApp1.setAfterCallWorkDuration("0");
		assertEquals("Please Enter Value greater than 0", acwPageApp1.errorMessage());
		Common.pause(2);

		driverWebApp1.navigate().refresh();
		Common.pause(2);
		acwPageApp1.scrollToACWTxt();
		acwPageApp1.setAfterCallWorkDuration("61");
		assertEquals("Please Enter Value less than 60", acwPageApp1.errorMessage());
		Common.pause(2);

		driverWebApp1.navigate().refresh();
		Common.pause(2);
		acwPageApp1.scrollToACWTxt();
		acwPageApp1.setAfterCallWorkDuration("-1");
		assertEquals("Please Enter positive value", acwPageApp1.errorMessage());
		Common.pause(2);

		driverWebApp1.navigate().refresh();
		Common.pause(2);
		acwPageApp1.scrollToACWTxt();
		acwPageApp1.setAfterCallWorkDuration("1");
		assertEquals("User updated successfully", acwPageApp1.successMessage());
		Common.pause(2);

		acwPageApp1.setAfterCallWorkDuration("60");
		assertEquals("User updated successfully", acwPageApp1.successMessage());
		Common.pause(2);
	}
	
	@Test(priority = 5, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void validate_fileds_on_dialer_EndACW() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateFieldsOnEndACWPage(account2Number1));
		Thread.sleep(1000);
		phoneApp1.clickEndACWButton();
		phoneApp1.waitForDialerPage();

		assertEquals(true, phoneApp2.validateFieldsOnEndACWPage(account1Number1));
		Thread.sleep(1000);
		phoneApp2.clickEndACWButton();
		phoneApp2.waitForDialerPage();
	}
	
	@Test(priority = 6, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_NO_Anawer_Incoming_Missed() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		// validate outgoing Number on outgoing calling screen
		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		// Validate IncomingVia on Incoming calling screen
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		// validate Incoming Number on Incoming calling screen
		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		try {
		assertEquals("No Answer", phoneApp1.validateCallStatusInCallDetail());
		} catch (AssertionError e) {
		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());
		}
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		driverPhoneApp2.get(url.dialerSignIn());
		Common.pause(3);

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail(), "validate  number2 in call log details");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());

		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		try {
		assertEquals("No Answer", webApp1.validateCallStatus());
     	} catch (AssertionError e) {
     	assertEquals("Rejected", webApp1.validateCallStatus());
    	}
		System.out.println("WebApp1 Call Status" + webApp1.validateCallStatus()); // ------check 1
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp1.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Missed", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		System.out.println("All Assert Pass for WEB2");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		System.out.println("DONE----priority = 1-----");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());

		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
	}
	
	@Test(priority = 7, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Cancelled_Incoming_Missed() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++
		phoneApp1.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp1.get(url.dialerSignIn());
		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Cancelled", phoneApp1.validateCallStatusInCallDetail());
		String status = phoneApp1.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail(), "validate  number2 in call log details");

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Cancelled", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp1.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		phoneApp1.clickOnDialButton();
		phoneApp1.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
		assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
		phoneApp1.clickOnOutgoingHangupButton();
	}
	
	@Test(priority = 8, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_Rejected() throws Exception {

		
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++
		phoneApp2.lastCallDetailToggle("on");
		driverPhoneApp2.get(url.dialerSignIn());
		Thread.sleep(2000);

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail(), "validate  number in call log details");

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp1.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());

	}
	
	@Test(priority = 9, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_Number_Closed() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "closed", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Number is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Number is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		assertEquals(webApp1.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide, "0");
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide, "0");
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		phoneApp2.clickOnOutgoingHangupButton();

	}
	
	@Test(priority = 10, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_ACW_Screen_is_Running() throws Exception {
		
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account3Number1);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(2000);
		phoneApp2.clickOnOutgoingHangupButton();

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.waitForDialerPage();

		Thread.sleep(1000);

		
		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote(
				"ACW_Note_Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_ACW_Screen_is_Running");

		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		Common.pause(2);
		webApp2.clickOnCloseiButton();
		Common.pause(2);
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(3);
		assertEquals("ACW_Note_Outgoing_Rejected_Incoming_CallNotSetup_By_user_Busy_with_ACW_Screen_is_Running",
				webApp2.validateItagNotes(2));

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Rejected", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp1.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp1.validateItagNotesIsEmpty(1), "There are no notes for this call.");
	}
		
	@Test(priority = 11, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();
		Thread.sleep(500);
		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("Outgoing_Completed");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote("Incoming_Completed");

		phoneApp2.clickEndACWButton();

		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Incoming_Completed", webApp2.validateItagNotes(1));

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		Common.pause(1);
		assertEquals("Outgoing_Completed", webApp1.validateItagNotes(1));

		// ------------ Credit Validation ------------------
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				callCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				callCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
	}
	
	@Test(priority = 12, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_WelcomeMessage() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(2000);

		phoneApp2.waitForDialerPage();

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("Outgoing_Completed_Incoming_WelcomeMessage");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Welcome message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Welcome message", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals("Outgoing_Completed_Incoming_WelcomeMessage", webApp1.validateItagNotes(1));

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				callCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				callCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		phoneApp2.clickOnOutgoingHangupButton();

	}
	
	@Test(priority = 13, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Missed_after_WelcomeMessage() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		Thread.sleep(15000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(2000);

		phoneApp2.waitForDialerPage();

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("ACW_Note_Outgoing_Completed_Incoming_Missed_after_WelcomeMessage");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals("ACW_Note_Outgoing_Completed_Incoming_Missed_after_WelcomeMessage", webApp1.validateItagNotes(1));
	}

	@Test(priority = 14, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed_after_WelcomeMessage() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		phoneApp1.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp1.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Thread.sleep(5000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(3);
		String durationOnOutgoingCallScreen1 = phoneApp1.validateDurationInCallingScreen(2);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(2000);


		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("ACW_Note_Outgoing_Completed_Incoming_Completed_after_WelcomeMessage");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();
		
		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		//phoneApp2.insertACWNote("ACW_Note_Outgoing_Completed_Incoming_Completed_after_WelcomeMessage");

		phoneApp2.clickEndACWButton();
		
		phoneApp2.waitForDialerPage();
		
		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		String status = phoneApp1.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals("ACW_Note_Outgoing_Completed_Incoming_Completed_after_WelcomeMessage",
				webApp1.validateItagNotes(1));

		driverPhoneApp1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		phoneApp1.clickOnDialButton();
		phoneApp1.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp1.validateLastCallDetailStatus());
		assertEquals(true, phoneApp1.validateLastCallDetailOutgoingIcon());
		phoneApp1.clickOnOutgoingHangupButton();
	}

	@Test(priority = 15, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Rejected_after_WelcomeMessage() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());


		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		Thread.sleep(10000);
		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);
		phoneApp2.clickOnIncomingRejectButton();

		Thread.sleep(3000);

		phoneApp2.waitForDialerPage();

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("ACW_Note_Outgoing_Completed_Incoming_Rejected_after_WelcomeMessage");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals("ACW_Note_Outgoing_Completed_Incoming_Rejected_after_WelcomeMessage",
				webApp1.validateItagNotes(1));
	}
	
	@Test(priority = 16, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVRMessage() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);
		Thread.sleep(2000);
		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(3000);

		phoneApp2.waitForDialerPage();

		Thread.sleep(1000);
		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("ACW_Note_Outgoing_Completed_Incoming_IVRMessage");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("IVR message", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("IVR message", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl(),
				"-----verify call recording- recording not availble--------");
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals("ACW_Note_Outgoing_Completed_Incoming_IVRMessage", webApp1.validateItagNotes(1));

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				callCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				callCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		phoneApp2.clickOnOutgoingHangupButton();
	}
	
	@Test(priority = 17, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_IVR_User_Unavailable() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp1.validateDurationInCallingScreen(1);

		Thread.sleep(2000);

		phoneApp1.clickOnDialPad();
		Thread.sleep(2000);
		phoneApp1.pressIVRKey1();

		Thread.sleep(10000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("Outgoing_Completed_Incoming_IVR_User_Unavailable");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		loginDialer(phoneApp2, account2Email, account2Password);

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals(phoneApp2.validateCallStatusInCallDetail(), "Unavailable");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName());
		assertEquals("Completed", webApp1.validateCallStatus());
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp1.validateItagNotes(1), "Outgoing_Completed_Incoming_IVR_User_Unavailable");
		System.out.println("All Assert Pass for WEB1");

		Thread.sleep(4000);

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName());
		assertEquals("Unavailable", webApp2.validateCallStatus());
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		System.out.println("All Assert Pass for WEB2");

		System.out.println("DONE----priority = 30-----");
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				callCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				callCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp2.enterNumberinDialer(account1Number1);
		phoneApp2.clickOnDialButton();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		assertEquals(" " + status, phoneApp2.validateLastCallDetailStatus());
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		phoneApp2.clickOnOutgoingHangupButton();

	}
	
	@Test(priority = 18, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_Reject() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		// ++++++++++++++++++++++++++++++++++++++
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp1.clickLeftMenuPlanAndBilling();
		webApp1.clickLeftMenuDashboard();
		String leftPanelcreditValBeforeOutgoingSide = webApp1.getCreditValueFromLeftPanel();

		webApp1.clickLeftMenuPlanAndBilling();

		String settingcreditValueBeforeOutgoingSide = webApp1.availableCreditValue();
		assertEquals(leftPanelcreditValBeforeOutgoingSide, settingcreditValueBeforeOutgoingSide);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "open", "off");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia());

		assertEquals(account1Number1, phoneApp2.validateCallingScreenIncomingNumber());

		phoneApp2.clickOnIncomingRejectButton();

		Thread.sleep(10000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(3000);

		phoneApp2.waitForDialerPage();

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());

		phoneApp1.insertACWNote("ACW_Note_Outgoing_Completed_Incoming_Voicemail_by_Reject");

		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(2);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note_Outgoing_Completed_Incoming_Voicemail_by_Reject");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driverWebApp2.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

		driverWebApp1.get(url.signIn());
		String settingcreditValueAfter1 = webApp1.validateCreditDeduction(settingcreditValueBeforeOutgoingSide,
				callCost1);
		String leftPanelcreditValueAfter1 = webApp1.validateCreditDeduction(leftPanelcreditValBeforeOutgoingSide,
				callCost1);
		Common.pause(2);
		webApp1.clickLeftMenuDashboard();
		Thread.sleep(10000);
		Common.pause(2);
		Thread.sleep(10000);
		webApp1.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated1 = webApp1.availableCreditValue();
		String leftPanelcreditValueUpdated1 = webApp1.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated1, leftPanelcreditValueAfter1);
		assertEquals(settingcreditValueUpdated1, settingcreditValueAfter1);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		driverPhoneApp1.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForLastCallDetailPopuppDisplayed();
		System.out.println("----status---" + status.replace(" ", "") + "----" + "---phoneApp2---"
				+ phoneApp2.validateLastCallDetailStatus().replace(" ", ""));
		assertTrue(status.replace(" ", "").equalsIgnoreCase(phoneApp2.validateLastCallDetailStatus().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals(true, phoneApp2.validateLastCallDetailIncomingIcon());
		phoneApp1.clickOnOutgoingHangupButton();

	}
	
	@Test(priority = 19, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Voicemail_by_user_busy_with_ACW_Screen_is_Running() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		driverWebApp2.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Thread.sleep(2000);
		driverWebApp2.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "on", "off", "open", "off");

		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp2.enterNumberinDialer(account3Number1);
		phoneApp2.clickOnDialButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();
		Thread.sleep(9000);
		phoneApp2.clickOnOutgoingHangupButton();

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());
		phoneApp2.insertACWNote("WebApp2_ACW_Note_PhoneApp2_Outgoing_Completed_PhoneApp3_Incoming_Completed");

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp1.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		Thread.sleep(30000);

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(4);

		phoneApp2.clickOnEndACWButton();
		phoneApp2.waitForDialerPage();

		phoneApp1.clickOnOutgoingHangupButton();
		Thread.sleep(1000);
		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("WebApp1_ACW_Note_PhoneApp1_Outgoing_Completed_PhoneApp2_VoiceMail");
		phoneApp1.clickOnEndACWButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		assertEquals("Via " + departmentName, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		Common.pause(2);
		webApp2.clickOnCloseiButton();
		Common.pause(2);
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(3);
		assertEquals(webApp2.validateItagNotes(2),
				"WebApp2_ACW_Note_PhoneApp2_Outgoing_Completed_PhoneApp3_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc2Num1, webApp1.validateCallCost(), "------verify call cost in webapp1-----");
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals(webApp1.validateItagNotes(1), "WebApp1_ACW_Note_PhoneApp1_Outgoing_Completed_PhoneApp2_VoiceMail");
	}
	
	@Test(priority = 20, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed_Auto_Close_ACW_Screen() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(3);
		acwPageApp1.setAfterCallWorkDuration("1");
		assertEquals(webApp1.validationMessage(), "User updated successfully");
		Common.pause(7);
		
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("1");
		assertEquals(webApp2.validationMessage(), "User updated successfully");
		Common.pause(5);
		
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnLogout();
		
		loginDialer(phoneApp2, account2Email, account2Password);
		phoneApp2.waitForDialerPage();
	
		loginDialer(phoneApp1, account1Email, account1Password);
		phoneApp1.waitForDialerPage();
		
		Common.pause(2);
		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(7000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("ACW_Note1_PhoneApp1_Outgoing_Completed");

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote("ACW_Note2_PhoneApp2_Incoming_Completed");
		Thread.sleep(10000);
		phoneApp2.waitForDialerPage();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);
		
		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(webApp2.validateItagNotes(1), "ACW_Note2_PhoneApp2_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note1_PhoneApp1_Outgoing_Completed");
	}
	
	@Test(priority = 21, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed_ACW_Screen_Incoming_Queue_call_Completed() throws Exception {

		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("ACW_Note_PhoneApp1_Outgoing_Completed");

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_Incoming_Completed");

		phoneApp3.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(account2Number1, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp3.validateDurationInCallingScreen(3);

		phoneApp2.clickEndACWButton();

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Common.pause(10);

		phoneApp3.clickOnOutgoingHangupButton();
		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_Queued_Call_serve");
		Common.pause(2);
		phoneApp2.clickEndACWButton();
		phoneApp2.waitForDialerPage();

		phoneApp1.clickEndACWButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account3Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account3Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());

		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(webApp2.validateItagNotes(1), "ACW_Note_PhoneApp2_Queued_Call_serve");
		Common.pause(2);
		webApp2.clickOnCloseiButton();
		Common.pause(2);
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(3);
		assertEquals(webApp2.validateItagNotes(2), "ACW_Note_PhoneApp2_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note_PhoneApp1_Outgoing_Completed");
	}
	
	@Test(priority = 22, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void Outgoing_Completed_Incoming_Completed_ACW_Auto_Screen_Close_Incoming_Queue_call_Completed() throws Exception {
		
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");
		assertEquals(webApp1.validationMessage(), "User updated successfully");
		Common.pause(7);
		
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("1");
		assertEquals(webApp2.validationMessage(), "User updated successfully");
		Common.pause(5);
	
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnLogout();
		
		loginDialer(phoneApp2, account2Email, account2Password);
		phoneApp2.waitForDialerPage();
		
		loginDialer(phoneApp1, account1Email, account1Password);
		phoneApp1.waitForDialerPage();
		
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("300");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());

		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(5000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("ACW_Note_PhoneApp1_Outgoing_Completed");

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());
		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_Incoming_Completed");

		Common.Pause(20);
		phoneApp3.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		assertEquals(account2Number1, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp3.validateDurationInCallingScreen(3);
		Common.Pause(6);

		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		Common.pause(6);

		phoneApp3.clickOnOutgoingHangupButton();
		Thread.sleep(1000);

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_Queued_Call_serve");
		Common.pause(60);

		phoneApp2.waitForDialerPage();
		
		phoneApp1.clickEndACWButton();

		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());

		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());

		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();

		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());

		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		assertEquals(account3Number1, phoneApp2.validateNumberInAllcalls());

		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals(account3Number1, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(webApp2.validateItagNotes(1), "ACW_Note_PhoneApp2_Queued_Call_serve");
		Common.pause(2);
		webApp2.clickOnCloseiButton();
		Common.pause(2);
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(3);
		assertEquals(webApp2.validateItagNotes(2), "ACW_Note_PhoneApp2_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note_PhoneApp1_Outgoing_Completed");
	}

	@Test(priority = 23, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void First_Outgoing_Completed_Incoming_Completed_ACW_Screen_Manual_Close_2nd_Incoming_Queue_call_CallNotSetup() throws Exception {
		
		
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("ACW_Note_PhoneApp1_Outgoing_Completed");

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());

		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());

		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_1st_Incoming_Completed");

		phoneApp3.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		// Validate outgoingVia on outgoing calling screen
		assertEquals("Outgoing Via " + departmentName, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		// validate outgoing Number on outgoing calling screen
		assertEquals(account2Number1, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		Common.pause(10);
		phoneApp2.clickEndACWButton();
		Thread.sleep(1000);
		phoneApp2.waitForDialerPage();

		phoneApp1.clickEndACWButton();
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account3Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(account3Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp3.clickOnSideMenu();
		String callerName3 = phoneApp3.getCallerName();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp3.validateNumberInAllcalls());
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp3.validatecallType());

		phoneApp3.clickOnRightArrow();
		assertEquals(account2Number1, phoneApp3.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp3.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		Common.pause(2);
		webApp2.clickOnCloseiButton();
		Common.pause(2);
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");
		Common.pause(3);
		assertEquals(webApp2.validateItagNotes(2), "ACW_Note_PhoneApp2_1st_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note_PhoneApp1_Outgoing_Completed");
	}
	
	@Test(priority = 24, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void First_Outgoing_Completed_Incoming_Completed_ACW_Screen_Auto_Close_2nd_Incoming_Queue_call_CallNotSetup() throws Exception {
		
		
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("1");
		assertEquals(webApp1.validationMessage(), "User updated successfully");
		Common.pause(5);
		
		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("1");
		assertEquals(webApp2.validationMessage(), "User updated successfully");
		Common.pause(5);
	
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnLogout();
		phoneApp1.clickOnSideMenu();
		phoneApp1.clickOnLogout();
		
		loginDialer(phoneApp2, account2Email, account2Password);
		phoneApp2.waitForDialerPage();
		
		loginDialer(phoneApp1, account1Email, account1Password);
		phoneApp1.waitForDialerPage();
		
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp3.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(1000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("ACW_Note_PhoneApp1_Outgoing_Completed");

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());
		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_1st_Incoming_Completed");

		Common.Pause(20);
		phoneApp3.enterNumberinDialer(account2Number1);
		String departmentName = phoneApp3.validateDepartmentNameinDialpade();
		phoneApp3.clickOnDialButton();
		assertEquals("Outgoing Via " + departmentName, phoneApp3.validateCallingScreenOutgoingVia(),
				"--Department name of dialer1--");

		assertEquals(account2Number1, phoneApp3.validateCallingScreenOutgoingNumber(),
				"--Number on calling screen of dialer1--");

		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage(); 
		phoneApp1.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account3Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(account3Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp3.clickOnSideMenu();
		String callerName3 = phoneApp3.getCallerName();
		phoneApp3.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp3.validateNumberInAllcalls());
		assertEquals(departmentName3, phoneApp3.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp3.validatecallType());

		phoneApp3.clickOnRightArrow();
		assertEquals(account2Number1, phoneApp3.validateNumberInCallDetail());
		assertEquals("Rejected", phoneApp3.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName3, phoneApp3.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		Common.pause(1);
		assertEquals(webApp2.validateItagNotesIsEmpty(1), "There are no notes for this call.");
		Common.pause(2);
		webApp2.clickOnCloseiButton();
		Common.pause(2);
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry(), "--Recording URL in web2 Activityfeed page--");
		Common.pause(3);
		assertEquals(webApp2.validateItagNotes(2), "ACW_Note_PhoneApp2_1st_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp1.validateRecordingUrl(), "-----verify call recording- recording  availble--------");
		Common.pause(1);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note_PhoneApp1_Outgoing_Completed");
	}
	
	@Test(priority = 25, retryAnalyzer = com.CallHippo.Init.Retry.class)
	public void One_Number_Allocated_to_2_Users_1stUser_ACW_ON_2nduser_ACW_OFF() throws Exception {

		if (phoneApp4.validateDialerAppLoggedinPage() == true) {
			loginDialer(phoneApp4, account2SubEmail, account2SubPassword);
		}
		
		webApp1.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp1.setAfterCallWorkDuration("5");

		webApp2.userAfterCallWork("on");
		Common.pause(2);
		acwPageApp2.setAfterCallWorkDuration("5");
		Common.pause(3);
		driverPhoneApp1.get(url.dialerSignIn());
		driverPhoneApp2.get(url.dialerSignIn());
		phoneApp1.waitForDialerPage();
		phoneApp2.waitForDialerPage();
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(account2Number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");

		phoneApp2.lastCallDetailToggle("on");
		Thread.sleep(2000);
		driverPhoneApp2.get(url.dialerSignIn());

		phoneApp1.enterNumberinDialer(account2Number1);
		String departmentName1 = phoneApp1.validateDepartmentNameinDialpade();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String departmentName3 = phoneApp4.validateDepartmentNameinDialpade();

		phoneApp1.clickOnDialButton();

		assertEquals("Outgoing Via " + departmentName1, phoneApp1.validateCallingScreenOutgoingVia());
		assertEquals(account2Number1, phoneApp1.validateCallingScreenOutgoingNumber());

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp4.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp4.waitForDialerPage();

		String durationOnOutgoingCallScreen = phoneApp1.validateDurationInCallingScreen(2);

		Thread.sleep(9000);

		phoneApp1.clickOnOutgoingHangupButton();

		Thread.sleep(2000);

		assertEquals(true, phoneApp1.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp1.validateAfterCallWorkTitle());
		phoneApp1.insertACWNote("ACW_Note_PhoneApp1_Outgoing_Completed");
		phoneApp1.clickEndACWButton();
		phoneApp1.waitForDialerPage();

		assertEquals(true, phoneApp2.validateDialerACWDurationVisible());
		assertEquals(true, phoneApp2.validateAfterCallWorkTitle());
		phoneApp2.insertACWNote("ACW_Note_PhoneApp2_Incoming_Completed");
		phoneApp2.clickEndACWButton();
		phoneApp2.waitForDialerPage();

		Thread.sleep(10000);

		phoneApp1.clickOnSideMenu();
		String callerName1 = phoneApp1.getCallerName();
		phoneApp1.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account2Number1, phoneApp1.validateNumberInAllcalls());
		assertEquals(departmentName1, phoneApp1.validateViaNumberInAllCalls());
		assertEquals("Outgoing", phoneApp1.validatecallType());

		phoneApp1.clickOnRightArrow();
		assertEquals(account2Number1, phoneApp1.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp1.validateCallStatusInCallDetail());
		String durationOnCallDetail = phoneApp1.validateDurationInCallDetail();
		System.out.println("calling screen" + durationOnOutgoingCallScreen + " Mins");
		System.out.println("call details" + durationOnCallDetail);
		assertEquals("Via " + departmentName1, phoneApp1.validateDepartmentNameinCallDetail());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp2.validateNumberInAllcalls());
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		assertEquals(account1Number1, phoneApp2.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();
		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		phoneApp4.clickOnSideMenu();
		String callerName3 = phoneApp4.getCallerName();
		phoneApp4.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals(account1Number1, phoneApp4.validateNumberInAllcalls());
		assertEquals(departmentName3, phoneApp4.validateViaNumberInAllCalls());
		assertEquals("Incoming", phoneApp4.validatecallType());

		phoneApp4.clickOnRightArrow();
		assertEquals(account1Number1, phoneApp4.validateNumberInCallDetail());
		assertEquals("Completed", phoneApp4.validateCallStatusInCallDetail());
		assertEquals("Via " + departmentName3, phoneApp4.validateDepartmentNameinCallDetail());

		driverWebApp2.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(webApp2.validateItagNotes(1), "ACW_Note_PhoneApp2_Incoming_Completed");

		driverWebApp1.get(url.signIn());
		webApp1.clickOnActivitFeed();
		Common.pause(5);
		assertEquals(departmentName1, webApp1.validateDepartmentName(), "----verify departmrnt name in webapp1");
		assertEquals(callerName1, webApp1.validateCallerName(), "-----verify caller name in webapp1");
		assertEquals("Completed", webApp1.validateCallStatus(), "----verify call statuts in webapp1---");
		assertEquals(true, webApp1.validateOutgoingCallTypeIsDisplayed());
		webApp1.clickOniButton();
		Common.pause(5);
		assertEquals(webApp1.validateItagNotes(1), "ACW_Note_PhoneApp1_Outgoing_Completed");

	}

}
