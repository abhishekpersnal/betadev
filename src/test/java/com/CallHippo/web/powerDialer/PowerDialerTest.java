package com.CallHippo.web.powerDialer;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.web.numberAndUserSubscription.AddNumberPage;
import com.CallHippo.web.settings.CallBlockingPage;
import com.CallHippo.web.settings.holiday.HolidayPage;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.credit.CreditWebSettingPage;
import com.CallHippo.Init.CallingCallCostTest;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;

public class PowerDialerTest {

	DialerIndex phoneApp1;
	WebDriver driver;

	DialerIndex phoneApp2;
	WebDriver driver1;

	DialerIndex phoneApp3;
	WebDriver driver2;

	DialerIndex phoneApp4;
	WebDriver driverPhoneApp4;

	DialerIndex phoneApp2SubUser;
	WebDriver driver6;

	WebDriver driverphoneApp5;
	DialerIndex phoneApp5;

	WebToggleConfiguration webApp2;
	PowerDialerPage webApp2PowerDialerPage;
	CallBlockingPage webApp2blocking;
	CreditWebSettingPage webbApp2page;
	WebDriver driver4;
	AddNumberPage WebAppAdd1;

	WebToggleConfiguration webApp2Subuser;
	PowerDialerPage webApp2SubuserPowerDialerPage;
	WebDriver driverWebApp2Subuser;

	WebToggleConfiguration webApp3;
	PowerDialerPage webApp3PowerDialerPage;
	WebDriver driverWebApp3;

	DialerLoginPage phoneApp5New;
	
	HolidayPage webApp2HolidayPage;
	CallingCallCostTest callingCallCostobj;

	RestAPI creditAPI;
	RestAPI api;
	static Common excel;
	static Common powerDialer1NUmberExcel;
	static Common powerDialer2NUmbersExcel;
	static Common powerDialer4NUmbersExcel;
	static Common powerDialerCustomNumber;
	static Common powerDialer15NUmbersExcel;
	PropertiesFile url = new PropertiesFile("Data\\url Configuration.properties");
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	SoftAssert softAssert;

	String account1Email = data.getValue("account1MainEmail");
	String account1Password = data.getValue("masterPassword");
	String account2Email = data.getValue("account2MainEmail");
	String account2Password = data.getValue("masterPassword");
	String account2EmailSubUser = data.getValue("account2SubEmail");
	String account2PasswordSubUser = data.getValue("masterPassword");
	String number = data.getValue("account2Number1"); // account 2's number
	String number1 = data.getValue("account2Number2"); // account 2's number 2
	String number2 = data.getValue("account1Number1"); // account 1's number

	String account3Email = data.getValue("account3MainEmail");
	String account3Password = data.getValue("masterPassword");

	String account4Email = data.getValue("account4MainEmail");
	String account5Email = data.getValue("account5MainEmail");
	String account4Password = data.getValue("masterPassword");

	String number3 = data.getValue("account3Number1"); // account 3's number
	String number4 = data.getValue("account4Number1"); // account 4's number
	String number5 = data.getValue("account5Number1"); // account 4's number

	String callCost1 = data.getValue("oneMinuteCallCost");
	String callCost2 = data.getValue("twoMinuteCallCost");
	String userID;
	String numberID;
	String subuserID;

	String userIDWebApp2;
	int sheetNumber = 0;
	int secondRow = 1;
	int thirdRow = 2;
	int forthRow = 3;
	int fifthRow = 4;
	int secondColumn = 1;

	String outgoingCallPriceAcc1Num1,outgoingCallPriceAcc3Num1,outgoingCallPriceAcc4Num1;
	String outgoingCallPriceAcc3Num1_2Min,outgoingCallPriceAcc1Num1_2Min,outgoingCallPriceAcc4Num1_2Min;
	public PowerDialerTest() throws Exception {
		powerDialer1NUmberExcel = new Common("powerDialer\\powerdialer1.xlsx");
		powerDialer2NUmbersExcel = new Common("powerDialer\\powerdialer2Numbers.xlsx");
		powerDialer4NUmbersExcel = new Common("powerDialer\\powerdialer4Numbers.xlsx");
		powerDialerCustomNumber = new Common("powerDialer\\powerdialerCustomNumber.xlsx");
		powerDialer15NUmbersExcel = new Common("powerDialer\\powerdialer15Number.xlsx");
		softAssert = new SoftAssert();
		creditAPI = new RestAPI();
		api = new RestAPI();
		callingCallCostobj=new CallingCallCostTest();
		excel = new Common();
	}

	@BeforeTest
	public void initialization() throws Exception {

		powerDialer1NUmberExcel.write(sheetNumber, secondRow, secondColumn, number3);
		powerDialer2NUmbersExcel.write(sheetNumber, secondRow, secondColumn, number3);
		powerDialer2NUmbersExcel.write(sheetNumber, thirdRow, secondColumn, number4);
		powerDialer4NUmbersExcel.write(sheetNumber, secondRow, secondColumn, number3);
		powerDialer4NUmbersExcel.write(sheetNumber, thirdRow, secondColumn, number3);
		powerDialer4NUmbersExcel.write(sheetNumber, forthRow, secondColumn, number3);
		powerDialer4NUmbersExcel.write(sheetNumber, fifthRow, secondColumn, number3);

		driver4 = TestBase.init();
		webApp2 = PageFactory.initElements(driver4, WebToggleConfiguration.class);
		webApp2PowerDialerPage = PageFactory.initElements(driver4, PowerDialerPage.class);
		webApp2blocking = PageFactory.initElements(driver4, CallBlockingPage.class);
		webbApp2page = PageFactory.initElements(driver4, CreditWebSettingPage.class);
		WebAppAdd1 = PageFactory.initElements(driver4, AddNumberPage.class);
		webApp2HolidayPage = PageFactory.initElements(driver4, HolidayPage.class);

		
		driverWebApp2Subuser = TestBase.init();
		webApp2Subuser = PageFactory.initElements(driverWebApp2Subuser, WebToggleConfiguration.class);
		webApp2SubuserPowerDialerPage = PageFactory.initElements(driverWebApp2Subuser, PowerDialerPage.class);

		driverWebApp3 = TestBase.init();
		webApp3 = PageFactory.initElements(driverWebApp3, WebToggleConfiguration.class);
		webApp3PowerDialerPage = PageFactory.initElements(driverWebApp3, PowerDialerPage.class);

		driver = TestBase.init_dialer();
		phoneApp1 = PageFactory.initElements(driver, DialerIndex.class);

		driver1 = TestBase.init_dialer();
		phoneApp2 = PageFactory.initElements(driver1, DialerIndex.class);

		driver2 = TestBase.init_dialer();
		phoneApp3 = PageFactory.initElements(driver2, DialerIndex.class);

		driverPhoneApp4 = TestBase.init_dialer();
		phoneApp4 = PageFactory.initElements(driverPhoneApp4, DialerIndex.class);

		driver6 = TestBase.init_dialer();
		phoneApp2SubUser = PageFactory.initElements(driver6, DialerIndex.class);

		XLSReader filePath= new XLSReader(excel.getLastUpdateFile());
		
		outgoingCallPriceAcc3Num1=callingCallCostobj.callCost(number3,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPriceAcc3Num1);
		
		outgoingCallPriceAcc4Num1=callingCallCostobj.callCost(number4,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPriceAcc4Num1);
		
		outgoingCallPriceAcc1Num1=callingCallCostobj.callCost(number2,filePath);
		System.out.println("outgoingCallPrice:"+outgoingCallPriceAcc4Num1);
		
		outgoingCallPriceAcc3Num1_2Min=Double.toString(Double.parseDouble(outgoingCallPriceAcc3Num1)*2);
		System.out.println("outgoingCallPrice 2 Minute:"+outgoingCallPriceAcc3Num1_2Min);
		
		outgoingCallPriceAcc1Num1_2Min=Double.toString(Double.parseDouble(outgoingCallPriceAcc1Num1)*2);
		System.out.println("outgoingCallPrice 2 Minute:"+outgoingCallPriceAcc1Num1_2Min);
		
		outgoingCallPriceAcc4Num1_2Min=Double.toString(Double.parseDouble(outgoingCallPriceAcc4Num1)*2);
		System.out.println("outgoingCallPrice 2 Minute:"+outgoingCallPriceAcc4Num1_2Min);
		
		
	 try {
		try {
			driver4.get(url.signIn());
			loginWeb(webApp2, account2Email, account2Password);
			Common.pause(3);
		} catch (Exception e) {
			driver4.get(url.signIn());
			loginWeb(webApp2, account2Email, account2Password);
			Common.pause(3);
		}

			try {
				driverWebApp2Subuser.get(url.signIn());
				loginWeb(webApp2Subuser, account2EmailSubUser, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driverWebApp2Subuser.get(url.signIn());
				loginWeb(webApp2Subuser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}

			try {
				driverWebApp3.get(url.signIn());
				loginWeb(webApp3, account3Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driverWebApp3.get(url.signIn());
				loginWeb(webApp3, account3Email, account2Password);
				Common.pause(3);
			}

			try {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			} catch (Exception e) {
				driver.get(url.dialerSignIn());
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			try {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			} catch (Exception e) {
				driver1.get(url.dialerSignIn());
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}
			try {
				driver6.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			} catch (Exception e) {
				driver6.get(url.dialerSignIn());
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			try {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			} catch (Exception e) {
				driver2.get(url.dialerSignIn());
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			try {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			} catch (Exception e) {
				driverPhoneApp4.get(url.dialerSignIn());
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}

			try {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());

				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= " + numberID);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);			
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(5);
				webApp2.makeDefaultNumber(number);
				
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				Common.pause(3);
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();


			} catch (Exception e) {
				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());

				Common.pause(3);
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(9);
				webApp2.setCallRecordingToggle("on");
				String Url1 = driver4.getCurrentUrl();
				numberID = webApp2.getNumberId(Url1);
				System.out.println("Number id= " + numberID);
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Thread.sleep(2000);
				webApp2.SetNumberSettingToggles("on", "on", "on", "off", "custom", "off", "on", "on");
				Thread.sleep(2000);
				webApp2.selectMessageType("Welcome message","Text");
				webApp2.selectMessageType("Voicemail","Text");
				webApp2.clickonAWHMVoicemail();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.clickonAWHMGreeting();
				webApp2.selectMessageType("After Work Hours Message","Text");
				webApp2.selectMessageType("Wait Music","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
				webApp2.selectMessageType("IVR","Text");
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");				
				Thread.sleep(3000);				
				
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				webApp2.Stickyagent("off","Strictly");
				Common.pause(3);
				webApp2.customRingingTime("off"); 
				System.out.println("customRingingTime OFF");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);
				webApp2.userTeamAllocation("Teams");
				Common.pause(3);
				webApp2.selectSpecificUserOrTeam("Team", "Yes");
				Common.pause(2);
				webApp2.userTeamAllocation("users");
				Common.pause(1);
				
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2Email);
				Common.pause(9);
				
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "on", "open", "off");
				webApp2.selectMessageType("Voicemail","Text");
				Common.pause(2);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(2);
				
				webApp2.makeDefaultNumber(number);

				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				creditAPI.UpdateCreditCountyWisezero(webApp2.getUserId(Url));
				userID = webApp2.getUserId(Url);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				
				Common.pause(3);
				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(5);
				webApp2.makeDefaultNumber(number);
				
				String subuserUrl = driver4.getCurrentUrl();
				subuserID = webApp2.getUserId(subuserUrl);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);

				driver1.get(url.dialerSignIn());
				phoneApp2.deleteAllContactsFromDialer();
				driver1.get(url.dialerSignIn());

				phoneApp1.afterCallWorkToggle("off");
				driver.get(url.dialerSignIn());
				phoneApp2.afterCallWorkToggle("off");
				Common.pause(3);
				driver1.get(url.dialerSignIn());

				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();
				Common.pause(2);
				webApp2HolidayPage.clickOnHolidaySubMenu();
				webApp2HolidayPage.deleteAllHolidayEntries();
				Common.pause(2);
				webApp2.scrollToPriceLimitTxt();
				webApp2.removePriceLimit();


			}
		} catch (Exception e) {
			String testname = "Power Dialer Before Test";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
			Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
			Common.Screenshot(driverWebApp2Subuser, testname, "WebAppp2Subuser Fail login");
			Common.Screenshot(driverWebApp3, testname, "WebAppp3 Fail login");
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
			Common.Screenshot(driver6, testname, "PhoneAppp6 Fail login");
			System.out.println("\n" + testname + " has been fail... \n");
		}

	}

	@BeforeMethod
	public void login() throws Exception {
		try {
			Common.pause(3);
			driver4.get(url.signIn());
			driver.get(url.dialerSignIn());
			driver1.get(url.dialerSignIn());
			driver2.get(url.dialerSignIn());
			driverPhoneApp4.get(url.dialerSignIn());
			driver6.get(url.dialerSignIn());
			Common.pause(3);

			if (webApp2.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2, account2Email, account2Password);
				Common.pause(3);
			}

			if (webApp2Subuser.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp2Subuser, account2EmailSubUser, account2Password);
				Common.pause(3);
			}

			if (webApp3.validateWebAppLoggedinPage() == true) {
				loginWeb(webApp3, account3Email, account2Password);
				Common.pause(3);
			}

			if (phoneApp1.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp1, account1Email, account1Password);
				Common.pause(3);
			}

			if (phoneApp2.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2, account2Email, account2Password);
				Common.pause(3);
			}

			if (phoneApp3.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp3, account3Email, account3Password);
				Common.pause(3);
			}
			if (phoneApp4.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp4, account4Email, account4Password);
				Common.pause(3);
			}
			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
			webApp2PowerDialerPage.deleteAllPowerDialerEntries();

			phoneApp2.clickOnSideMenu();
			String callerName2 = phoneApp2.getCallerName();
			driver1.get(url.dialerSignIn());

			phoneApp2SubUser.clickOnSideMenu();
			String callerName2Subuser = phoneApp2SubUser.getCallerName();
			driver6.get(url.dialerSignIn());

			phoneApp2.afterCallWorkToggle("off");
			driver1.get(url.dialerSignIn());

			driver4.get(url.usersPage());
			webApp2.navigateToUserSettingPage(account2Email);
			String Url = driver4.getCurrentUrl();
			creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
			driver4.get(url.planPage());
			driver4.get(url.signIn());
			driver4.get(url.planPage());
			Common.pause(9);
			driver4.get(url.usersPage());
			webApp2.navigateToUserSettingPage(account2Email);
			Common.pause(9);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(5);
			webApp2.makeDefaultNumber(number);
			Common.pause(3);
			webApp2.clickLeftMenuSetting();
			driver4.get(url.signIn());
			webApp2.clickLeftMenuSetting();
			webApp2.userspage();
			webApp2.navigateToUserSettingPage(account2EmailSubUser);
			Common.pause(9);
			webApp2.makeDefaultNumber(number);
			webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
			Common.pause(3);

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(3);
			webApp2.setCallRecordingToggle("on");
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			webApp2.userTeamAllocation("users");
			Common.pause(3);
			webApp2.selectSpecificUserOrTeam(callerName2, "Yes");
			webApp2.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
			Common.pause(3);

			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number1);
			Common.pause(9);
			webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Common.pause(4);	
            webApp2.userTeamAllocation("users");
			Common.pause(3);
			webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
			webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
			driverWebApp3.get(url.numbersPage());
			webApp3.navigateToNumberSettingpage(number3);
			Common.pause(9);
			webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Common.pause(3);
			webApp2.clickLeftMenuDashboard();
			Common.pause(2);
			webApp2.clickLeftMenuSetting();
			Common.pause(2);
			webApp2.scrollToCallBlockingTxt();
			Common.pause(1);
			webApp2.deleteAllNumbersFromBlackList();
	
		} catch (Exception e) {
			try {
				Common.pause(3);
				driver4.get(url.signIn());
				driver.get(url.dialerSignIn());
				driver1.get(url.dialerSignIn());
				driver2.get(url.dialerSignIn());
				driverPhoneApp4.get(url.dialerSignIn());
				driver6.get(url.dialerSignIn());
				Common.pause(3);

				if (webApp2.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2, account2Email, account2Password);
					Common.pause(3);
				}
				if (webApp2Subuser.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp2Subuser, account2EmailSubUser, account2Password);
					Common.pause(3);
				}

				if (webApp3.validateWebAppLoggedinPage() == true) {
					loginWeb(webApp3, account3Email, account2Password);
					Common.pause(3);
				}
				if (phoneApp1.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp1, account1Email, account1Password);
					Common.pause(3);
				}

				if (phoneApp2.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2, account2Email, account2Password);
					Common.pause(3);
				}

				if (phoneApp3.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp3, account3Email, account3Password);
					Common.pause(3);
				}
				if (phoneApp4.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp4, account4Email, account4Password);
					Common.pause(3);
				}
				if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
					loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
					Common.pause(3);
				}

				webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
				webApp2PowerDialerPage.deleteAllPowerDialerEntries();

				phoneApp2.clickOnSideMenu();
				String callerName2 = phoneApp2.getCallerName();
				driver1.get(url.dialerSignIn());

				phoneApp2SubUser.clickOnSideMenu();
				String callerName2Subuser = phoneApp2SubUser.getCallerName();
				driver6.get(url.dialerSignIn());

				driver4.get(url.usersPage());
				webApp2.navigateToUserSettingPage(account2Email);
				String Url = driver4.getCurrentUrl();
				creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
				Common.pause(5);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);
				driver4.get(url.planPage());
				driver4.get(url.signIn());
				driver4.get(url.planPage());

				webApp2.userspage();
				webApp2.navigateToUserSettingPage(account2EmailSubUser);
				Common.pause(9);
				webApp2.setUserSttingToggles("off", "off", "off", "open", "off");
				Common.pause(3);

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number);
				Common.pause(3);
				webApp2.setCallRecordingToggle("on");
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				webApp2.userTeamAllocation("users");
				Common.pause(3);
				webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
				webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				Common.pause(3);

				webApp2.numberspage();
				webApp2.navigateToNumberSettingpage(number1);
				Common.pause(9);
				webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			    Common.pause(3);
			    webApp2.userTeamAllocation("users");
			    Common.pause(3);
			    webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		    	webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
				driverWebApp3.get(url.numbersPage());
				webApp3.navigateToNumberSettingpage(number3);
				Common.pause(9);
				webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
				Common.pause(3);
				webApp2.clickLeftMenuDashboard();
				Common.pause(2);
				webApp2.clickLeftMenuSetting();
				Common.pause(2);
				webApp2.scrollToCallBlockingTxt();
				Common.pause(1);
				webApp2.deleteAllNumbersFromBlackList();

			} catch (Exception e1) {
				String testname = "Power Dialer Before Method";
				Common.Screenshot(driver, testname, "PhoneAppp1 Fail login");
				Common.Screenshot(driver1, testname, "PhoneAppp2 Fail login");
				Common.Screenshot(driver2, testname, "PhoneAppp3 Fail login");
				Common.Screenshot(driver4, testname, "WebAppp2 Fail login");
				Common.Screenshot(driverWebApp2Subuser, testname, "WebAppp2Subuser Fail login");
				Common.Screenshot(driverWebApp3, testname, "WebAppp3 Fail login");
				Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail login");
				Common.Screenshot(driver6, testname, "PhoneAppp6 Fail login");

				System.out.println("\n" + testname + " has been fail... \n");
			}
		}
	}

	@AfterMethod
	public void endTestWork(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, "PhoneAppp1 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebAppp2 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2Subuser, testname,
					"WebAppp2Subuser Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp3, testname, "WebAppp3 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Fail " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp6 Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, "PhoneAppp1 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver1, testname, "PhoneAppp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver2, testname, "PhoneAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver4, testname, "WebApp2 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp2Subuser, testname,
					"WebAppp2Subuser Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverWebApp3, testname, "WebAppp3 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driverPhoneApp4, testname, "PhoneAppp4 Pass " + result.getMethod().getMethodName());
			Common.Screenshot(driver6, testname, "PhoneAppp6 Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	}

	@AfterTest(alwaysRun = true)
	public void tearDown() throws IOException {
		
		try {
			driverWebApp3.get(url.numbersPage());
			webApp3.navigateToNumberSettingpage(number3);
			Common.pause(9);
			webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Common.pause(3);
		}catch(Exception e) {
			driverWebApp3.get(url.numbersPage());
			webApp3.navigateToNumberSettingpage(number3);
			Common.pause(9);
			webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
			Common.pause(3);
		}
		
		
		driver.quit();
		driver1.quit();
		driver2.quit();
		driver4.quit();
		driverPhoneApp4.quit();
		driver6.quit();
		driverWebApp2Subuser.quit();
		driverWebApp3.quit();

	}

	public void loginDialer(DialerIndex dialer, String email, String password) {

		dialer.enterEmail(email);
		dialer.enterPassword(password);
		dialer.clickOnLogin();

	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {

		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();

	}

	@Test(priority = 72, retryAnalyzer = Retry.class)
	public void add_Campaign_with_User_Assignment() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.validatehowtousepowerdialer();
		webApp2.switchToWindowBasedOnTitle("How to use Power Dialer? : CallHippo");
		assertEquals(webApp2PowerDialerPage.powerDialerusepage(), "How to use Power Dialer?");
		driver4.close();
		webApp2.switchToWindowBasedOnTitle("PowerDialer | CallHippo.com");
		assertEquals(webApp2PowerDialerPage.validatepowerDilertitle(), "Power Dialer");
		webApp2PowerDialerPage.validateserchfieldOnPowerDialerpage();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.AttemptperContactToggle("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		// webApp2PowerDialerPage.clickOndownloadcapaign();
		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
//		String date1 = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
	}

	@Test(priority = 71, retryAnalyzer = Retry.class)
	public void add_Campaign_with_Team_Assignment() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(5);
		webApp2PowerDialerPage.selectAssignee1("Team");
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.ValidateAssignee("Team"), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		// webApp2PowerDialerPage.clickOndownloadcapaign();
		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee("Team"), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");

	}

	@Test(priority = 70, retryAnalyzer = Retry.class)
	public void add_Campaign_with_Single_User_Assignment_Then_Edit_with_Multiple_User() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		String date = Common.powerDialerDate();
		System.out.println(date);
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		Common.pause(5);
		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");

		Common.pause(2);
		// webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2Subuser), true);
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
//		String date1 = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
	}

	@Test(priority = 69, retryAnalyzer = Retry.class)
	public void add_Campaign_with_Single_User_Assignment_Then_Edit_with_Team() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		

		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(5);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee1("Team");
		Common.pause(5);
		webApp2PowerDialerPage.selectFromNumber(number);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee("Team"), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
//		String date1 = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
	}

	@Test(priority = 68, retryAnalyzer = Retry.class)
	public void add_Campaign_with_User_Assignment_Edit_Campiagn_with_Second_Number() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		// assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		webApp2PowerDialerPage.selectFromNumber(number1);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
	}

	@Test(priority = 67, retryAnalyzer = Retry.class)
	public void add_Campaign_with_Multiple_User_Assignment_Then_Edit_With_Team() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();


		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2Subuser), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);

		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(5);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee1("Team");
		Common.pause(5);
		webApp2PowerDialerPage.selectFromNumber(number);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee("Team"), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
//		String date1 = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
	}

	@Test(priority = 66, retryAnalyzer = Retry.class)
	public void active_Campaign_With_Two_Users_Subuser_not_logged_into_dialer_Validation_For_Minimum_Users_DIsplayed()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");

		Common.pause(5);
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("2");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		assertEquals(webApp2PowerDialerPage.validateMinimum2UserLoggedInMessage(), true,
				"minimum campaign loggedin message");

	}

	@Test(priority = 65, retryAnalyzer = Retry.class)
	public void active_Campaign_minimum_user_not_logged_into_dialer() throws Exception {

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(5);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee1("Team");
//		webApp2PowerDialerPage.selectAssignee("localacc2");
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("3");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		assertEquals(webApp2PowerDialerPage.validateMinimumUserLoggedInMessage(), true,
				"minimum campaign loggedin message");

	}

	@Test(priority = 64, retryAnalyzer = Retry.class)
	public void completed_start_Campaign_with_2_Users_Assigned_subuser_loggedout() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
        phoneApp2.validateNameandNumberDataOnLeftSide("name", "");
        phoneApp2.validateNameandNumberDataOnLeftSide("Number", number3);
      //  phoneApp2.validateNameandNumberDataOnLeftSide("City", " ");
      // phoneApp2.validateNameandNumberDataOnLeftSide("Pincode", " ");
		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 63, retryAnalyzer = Retry.class)
	public void Assignee_user_has_started_session_when_always_close() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		webApp2.setUserSttingToggles("off", "off", "off", "closed", "off");

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
//
//		phoneApp2SubUser.clickOnSideMenu();
//		String callerName2Subuser = phoneApp2SubUser.getCallerName();
////		phoneApp2SubUser.clickOnLogout();
//		driver6.get(url.dialerSignIn());
//
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(webApp2PowerDialerPage.campaignActivationStatus(),true);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
//		phoneApp3.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		assertEquals(webApp2PowerDialerPage.campaignActivationStatus(),false);
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 62, retryAnalyzer = Retry.class)
	public void Assignee_user_has_started_session_when_Custom_close() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);
		api.addCustomTimeSlot("user", userID, false);
		webApp2.setUserSttingToggles("off", "off", "off", "custom", "off");

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
//		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(webApp2PowerDialerPage.campaignActivationStatus(),true);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
//		phoneApp3.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		assertEquals(webApp2PowerDialerPage.campaignActivationStatus(),false);
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 61, retryAnalyzer = Retry.class)
	public void completed_start_Campaign_with_1_Team_Assigned_subuser_loggedout() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee1("Team");
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 60, retryAnalyzer = Retry.class)
	public void No_Answer_start_Campaign_with_2_Users_Assigned_subuser_loggedout() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
//		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
//		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 59, retryAnalyzer = Retry.class)
	public void Power_dialer_call_answered_by_client_missed_by_user_from_dialer() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
//		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
//		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 58, retryAnalyzer = Retry.class)
	public void Power_dialer_call_answered_by_client_rejected_by_user_from_dialer() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
//		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
//		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 57, retryAnalyzer = Retry.class)
	public void rejected_start_Campaign_with_2_Users_Assigned_subuser_loggedout() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Rejected", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
//		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
//		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 56, retryAnalyzer = Retry.class)
	public void completed_start_Campaign_with_1_User_Assigned_both_users_loggedin() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
//		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 55, retryAnalyzer = Retry.class)
	public void call_queue_Getting_another_incoming_call_on_assignees_number_when_campaign_call_is_running_and_miss_the_queued_call_from_queue()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.enterCallQueueDuration("10");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		phoneApp1.waitForDialerPage();

		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(10);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(10);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost2);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost2);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 54, retryAnalyzer = Retry.class)
	public void add_Campaign_with_User_Assignment_By_Subuser() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		webApp2SubuserPowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2SubuserPowerDialerPage.clickOnAddCampaignButton();
		webApp2SubuserPowerDialerPage.enterCampaignName("Campaign 2");
		webApp2SubuserPowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2SubuserPowerDialerPage.selectAssignee(callerName2);
		webApp2SubuserPowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2SubuserPowerDialerPage.selectFromNumber(number);
		webApp2SubuserPowerDialerPage.autoVoicemailDrop("off");
		webApp2SubuserPowerDialerPage.importCSVFile("powerdialer1.xlsx");
		webApp2SubuserPowerDialerPage.validateinfoicononaddcamoaignpage();
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2SubuserPowerDialerPage.campaignDescription("Test automation");
		webApp2SubuserPowerDialerPage.clickOnSaveButton();
		// webApp2PowerDialerPage.clickOndownloadcapaign();

		assertEquals(webApp2SubuserPowerDialerPage.validateCampaignsName(), "Campaign 2");
		webApp2SubuserPowerDialerPage.clickOnEditButton();
		webApp2SubuserPowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		webApp2SubuserPowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2SubuserPowerDialerPage.validateCampaignsName(), "Campaign updated");
	}

	@Test(priority = 53, retryAnalyzer = Retry.class)
	public void call_queue_Getting_another_incoming_call_on_assignees_number_when_campaign_call_is_running_and_miss_the_queued_call_from_dialer()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Common.pause(4);
		webApp2.enterCallQueueDuration("10");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		Common.pause(5);
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Missed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Missed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost2);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost2);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 52, retryAnalyzer = Retry.class)
	public void call_queue_Voicemail_Getting_another_incoming_call_on_assignees_number_when_campaign_call_is_running_and_Reject_the_queued_call_from_dialer()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "on", "off");
		Common.pause(3);
		webApp2.enterCallQueueDuration("10");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(5);

		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomingRejectButton();
		Common.pause(11);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(15);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost2);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost2);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 51, retryAnalyzer = Retry.class)
	public void Call_answer_from_client_and_hangup_by_itself_before_answered_from_dialer() throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
//		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();

		Common.pause(7);
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp3.waitForDialerPage();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("No Answer", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 50, retryAnalyzer = Retry.class)
	public void Two_user_in_campiagn_and_number_is_assigned_to_only_main_user__Queue_call_should_serve_only_to_user_who_is_assigned_number()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "No");
		Common.pause(3);
		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(2);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.completedcalls(2);
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(5);

		phoneApp2SubUser.clickOnIncomingHangupButton();
		Common.pause(3);

		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(11);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		// assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(),
		// "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc1Num1));
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc1Num1));
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 49, retryAnalyzer = Retry.class)
	public void Team_with_Two_user_in_campiagn_and_number_is_assigned_to_only_main_user__Queue_call_should_serve_only_to_user_who_is_assigned_number()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Common.pause(3);
		webApp2.userTeamAllocation("Team");
		Common.pause(4);
		webApp2PowerDialerPage.selectSpecificUserOrTeam("Team", "No");
		webApp2.userTeamAllocation("Team");
		webApp2PowerDialerPage.selectSpecificUserOrTeam("main user team", "Yes");
		Common.pause(3);
		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee1("Team");
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		Common.pause(3);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(5);

		phoneApp2SubUser.clickOnIncomingHangupButton();
		Common.pause(3);

		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();

		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(11);
		phoneApp1.clickOnOutgoingHangupButton();

		phoneApp2.waitForDialerPage();
		phoneApp1.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(),"-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerNameFor3rdEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed3rdentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor3rdEntry(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed3rd());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCostFor3rdEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton3rdEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor3rdEntry());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc1Num1));
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc1Num1));
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 48, retryAnalyzer = Retry.class)
	public void active_Campaign_Team_with_Two_user_in_campiagn() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam("Team", "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam("main user team", "No");
		
//		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee1("Team");
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		Common.pause(3);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Thread.sleep(4000);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2SubUser.clickOnIncomingHangupButton();

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
//		assertEquals(callerName2Subuser, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "validate recording");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 47, retryAnalyzer = Retry.class)
	public void active_Campaign_user_ACW_is_ON() throws Exception {

		phoneApp2.afterCallWorkToggle("on");
		driver1.get(url.dialerSignIn());

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "No");
//		webApp2.enterCallQueueDuration("10");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(10);
			assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
			assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(1, 2), "1 out of 2");
			phoneApp2.clickEndACWButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(2, 2), "2 out of 2");
			phoneApp2.clickOnIncomingHangupButton();

			Common.pause(3);
			phoneApp2.clickEndACWButton();

		} else {
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(10);
			assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false);
			assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(1, 2), "1 out of 2");
			phoneApp2.clickEndACWButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(2, 2), "2 out of 2");
			phoneApp2.clickOnIncomingHangupButton();

			Common.pause(3);
			phoneApp2.clickEndACWButton();
		}

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		// assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(),
		// "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true,
				"-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
//		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 46, retryAnalyzer = Retry.class)
	public void active_Campaign_and_logged_in_Sub_User() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
//		webApp2.enterCallQueueDuration("10");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();

			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.completedcalls(2);
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();

			if (phoneApp2SubUser.validateDialerAppLoggedinPage() == true) {
				loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
				Common.pause(3);
			}

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.completedcalls(2);
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

			phoneApp2.clickOnIncomingHangupButton();

		}

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		// assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(),
		// "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true,
				"-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
//		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 45, retryAnalyzer = Retry.class)
	public void delete_campaign_from_Web_when_by_stopping_campaign_in_middle() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
//		webApp2.enterCallQueueDuration("10");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();

			webApp2PowerDialerPage.deleteAllPowerDialerEntries();

			phoneApp2.clickOnIncomingHangupButton();

			Common.pause(5);

			assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false,
					"not getting power dialer next call");

		} else {
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();

			webApp2PowerDialerPage.deleteAllPowerDialerEntries();

			phoneApp2.clickOnIncomingHangupButton();

			Common.pause(5);

			assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false,
					"not getting power dialer next call");
		}

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "validate recording");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 44, retryAnalyzer = Retry.class)
	public void completed_start_Campaign_with_internal_call_is_going_2_Users_Assigned() throws Exception {

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			assertEquals(number, phoneApp3.validateCallingScreenIncomingNumber());
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.enterNumberinDialer(account2MainUserExtension);

			Common.pause(7);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnDialButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp4.validateCallingScreenIncomingNumber());
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.enterNumberinDialer(account2MainUserExtension);

			Common.pause(7);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnDialButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		}

		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 43, retryAnalyzer = Retry.class)
	public void Logout_from_Dialer_after_start_Campaign_() throws Exception {

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp3.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnSideMenu();
			phoneApp2.clickOnLogout();
			phoneApp3.waitForDialerPage();
			assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false,
					"not getting power dialer next call");
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp4.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnSideMenu();
			phoneApp2.clickOnLogout();
			phoneApp4.waitForDialerPage();
			assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false,
					"not getting power dialer next call");
		}

		loginDialer(phoneApp2, account2Email, account1Password);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(status, webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 42, retryAnalyzer = Retry.class)
	public void Unassign_nunber_from_User_when_Campaign_is_already_running_Campaign_should_Run_as_it_Is()
			throws Exception {

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp3.validateCallingScreenIncomingNumber());
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.userTeamAllocation("users");
			Common.pause(3);
			webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "No");
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(2);
			phoneApp4.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp4.validateCallingScreenIncomingNumber());
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp4.validateCallingScreenIncomingNumber());
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			webApp2.numberspage();
			webApp2.navigateToNumberSettingpage(number);
			Common.pause(9);
			webApp2.userTeamAllocation("users");
			Common.pause(3);
			webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "No");
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(2);
			phoneApp3.waitForIncomingCallingScreen();
			assertEquals(number, phoneApp3.validateCallingScreenIncomingNumber());
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();

		}
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(status, webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(status, webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);
		webApp2.numberspage();
		webApp2.navigateToNumberSettingpage(number);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "No");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");

	}

	@Test(priority = 41, retryAnalyzer = Retry.class)
	public void View_dialer_after_power_dialer_toggle_disabled_fromweb_app() throws Exception {

		

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp2.validateSessionButtononDialer(), true);
		webApp2PowerDialerPage.campaignActivation("off");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp2.validateSessionButtononDialer(), false);

	}

	@Test(priority = 40, retryAnalyzer = Retry.class)
	public void Add_Campaign_Two_user_in_campiagn_login_Via_new_User_After_Campaign_start() throws Exception {

		phoneApp2.clickOnSideMenu();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam("Team", "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam("main user team", "No");
//		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(2);
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(webApp2PowerDialerPage.validateEditButton(), false);

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			driver6.get(url.dialerSignIn());
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			phoneApp2SubUser.waitForDialerPage();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(2);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();

			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			driver6.get(url.dialerSignIn());
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			phoneApp2SubUser.waitForDialerPage();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();

		}

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2Subuser, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "validate recording");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	@Test(priority = 39, retryAnalyzer = Retry.class)
	public void active_Campaign_Team_with_Two_user_in_campiagn_login_Via_new_User() throws Exception {

		phoneApp2.clickOnSideMenu();
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("Team");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam("Team", "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam("main user team", "No");
//		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee1("Team");
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		Common.pause(3);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(2);
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(webApp2PowerDialerPage.validateEditButton(), false);

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp3.waitForIncomingCallingScreen();

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			driver6.get(url.dialerSignIn());
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			phoneApp2SubUser.waitForDialerPage();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(2);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();

			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			driver6.get(url.dialerSignIn());
			loginDialer(phoneApp2SubUser, account2EmailSubUser, account2PasswordSubUser);
			phoneApp2SubUser.waitForDialerPage();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();

		}

		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();
		phoneApp3.waitForDialerPage();
		phoneApp4.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2Subuser, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "--Voicemail call status in web2 Activityfeed page--");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl(), "validate recording");

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 38, retryAnalyzer = Retry.class)
	public void Edit_User_in_power_dialer_campaign_and_verify_in_Web() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver2.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.clickOnEditButton();

		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();
		webApp2.validateuserinpowerDiler("User", callerName2Subuser);

	}

	@Test(priority = 37, retryAnalyzer = Retry.class)
	public void Edit_User_in_Running_power_dialer_campaign_and_verify_in_Web_Dialer() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2);
			webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
			Common.pause(2);
			webApp2PowerDialerPage.selectFromNumber(number);
			Common.pause(5);
			webApp2PowerDialerPage.clickOnSaveButton();
			webApp2.validateuserinpowerDiler("User", callerName2Subuser);
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();

			webApp2PowerDialerPage.clickOnEditButton();

			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2);
			webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
			Common.pause(2);
			webApp2PowerDialerPage.selectFromNumber(number);
			Common.pause(5);
			webApp2PowerDialerPage.clickOnSaveButton();
			webApp2.validateuserinpowerDiler("User", callerName2Subuser);
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 36, retryAnalyzer = Retry.class)
	public void Edit_and_update_Campaign_number_with_new_number_when_campaign_is_running_with_main_User()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.selectDepartmentNumber(number1);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			Common.pause(2);
			webApp2PowerDialerPage.selectFromNumber(number1);
			Common.pause(5);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
//			assertEquals("Incoming Via " + departmentName, phoneApp2.validateCallingScreenIncomingVia());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();

			webApp2PowerDialerPage.clickOnEditButton();

			Common.pause(2);
			webApp2PowerDialerPage.selectFromNumber(number1);
			Common.pause(5);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
//			assertEquals("Incoming Via " + departmentName, phoneApp2.validateCallingScreenIncomingVia());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 35, retryAnalyzer = Retry.class)
	public void Edit_and_update_assign_Team_Campaign_with_new_team_when_campaign_is_running_with_main_User()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Teams");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssigneeTeam("main user team");
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			webApp2PowerDialerPage.selectAssigneeTeam("subuser team");
			Common.pause(3);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(4);
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();

			webApp2PowerDialerPage.clickOnEditButton();
			webApp2PowerDialerPage.selectAssigneeTeam("subuser team");
			Common.pause(3);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(4);
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 34, retryAnalyzer = Retry.class)
	public void Edit_and_update_Campaign_User_Single_to_multiple_User_make_powerdialer_call() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
			Common.pause(2);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(5);
			webApp2PowerDialerPage.campaignActivation("on");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			Common.pause(2);
			assertEquals(phoneApp2.validateSessionButtononDialer(), true);
			assertEquals(phoneApp2SubUser.validateSessionButtononDialer(), true);
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
			Common.pause(5);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(5);
			webApp2PowerDialerPage.campaignActivation("on");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			Common.pause(2);
			assertEquals(phoneApp2.validateSessionButtononDialer(), true);
			assertEquals(phoneApp2SubUser.validateSessionButtononDialer(), true);
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 33, retryAnalyzer = Retry.class)
	public void Edit_and_update_Campaign_User_multiple_to_Single_to_User_make_powerdialer_call() throws Exception {
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
        Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2);
			Common.pause(2);
			webApp2PowerDialerPage.clickOnSaveButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(5);
			driver1.get(url.dialerSignIn());
			driver6.get(url.dialerSignIn());
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			assertEquals(phoneApp2.validateSessionButtononDialer(), false);
			assertEquals(phoneApp2SubUser.validateSessionButtononDialer(), true);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2SubUser.completedcalls(1);

			phoneApp2.waitForIncomingCallingScreen();
			// phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2);
			Common.pause(2);
			webApp2PowerDialerPage.clickOnSaveButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(5);
			driver1.get(url.dialerSignIn());
			driver6.get(url.dialerSignIn());
			webApp2PowerDialerPage.campaignActivation("on");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			assertEquals(phoneApp2.validateSessionButtononDialer(), false);
			assertEquals(phoneApp2SubUser.validateSessionButtononDialer(), true);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 32, retryAnalyzer = Retry.class)
	public void Edit_and_update_assign_User_Campaign_with_new_team_when_campaign_is_running_with_main_User()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			webApp2PowerDialerPage.selectAllocation("Team");
			webApp2PowerDialerPage.selectAssigneeTeam("subuser team");
			Common.pause(3);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(4);
			webApp2PowerDialerPage.campaignActivation("on");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();

			webApp2PowerDialerPage.clickOnEditButton();
			webApp2PowerDialerPage.selectAllocation("Team");
			webApp2PowerDialerPage.selectAssigneeTeam("subuser team");
			Common.pause(3);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(4);
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 31, retryAnalyzer = Retry.class)
	public void Edit_and_update_assign_Team_Campaign_User_when_campaign_is_running_with_Team() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssigneeTeam("main user team");
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			webApp2PowerDialerPage.selectAllocation("Users");
			webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
			Common.pause(3);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(4);
			phoneApp2.clickOnIncomingHangupButton();
			webApp2PowerDialerPage.campaignActivation("on");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			webApp2PowerDialerPage.campaignActivation("off");
			Common.pause(2);
			webApp2PowerDialerPage.clickOnYesButton();
			webApp2PowerDialerPage.clickOnEditButton();
			webApp2PowerDialerPage.selectAllocation("Users");
			webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
			Common.pause(3);
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(4);
			phoneApp2.clickOnIncomingHangupButton();
			webApp2PowerDialerPage.campaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
		}
	}

	@Test(priority = 30, retryAnalyzer = Retry.class)
	public void Verify_dialer_when_campaign_not_started_Normal_incoming_outgoing_Call_should_work_proper()
			throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		assertEquals(phoneApp2.validateSessionButtononDialer(), false);
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp3.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnOutgoingHangupButton();
		phoneApp3.waitForDialerPage();

	}

	@Test(priority = 29, retryAnalyzer = Retry.class)
	public void Verify_validation_of_add_campaign_without_Campaign_name_() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.errormessageforcampaignname(), "Campaign name is required.");
		webApp2PowerDialerPage.enterCampaignName("1");
		assertEquals(webApp2PowerDialerPage.errormessageforcampaignname(),
				"Campaign Name cannot be less than 5 chars.");
		webApp2PowerDialerPage.enterCampaignName("12");
		assertEquals(webApp2PowerDialerPage.errormessageforcampaignname(),
				"Campaign Name cannot be less than 5 chars.");
		webApp2PowerDialerPage.enterCampaignName("123");
		assertEquals(webApp2PowerDialerPage.errormessageforcampaignname(),
				"Campaign Name cannot be less than 5 chars.");
		webApp2PowerDialerPage.enterCampaignName("1234");
		assertEquals(webApp2PowerDialerPage.errormessageforcampaignname(),
				"Campaign Name cannot be less than 5 chars.");
		webApp2PowerDialerPage.enterCampaignName("12345");
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		// webApp2PowerDialerPage.Downloadtempcampaign();
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign created successfully.");

	}

	@Test(priority = 28, retryAnalyzer = Retry.class)
	public void Start_Campaign_With_Team_Fixed_Order_make_powerdialer_call_Call_Should_recive_in_simultaneous_Order()
			throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnFixedOrder();
		webApp2.clickOnUpdateButton();
		driver4.get(url.signIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		Common.pause(3);
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee1("Team");
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);
			Common.pause(5);

			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(2);
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2SubUser.completedcalls(1);

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(2);
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();

		}
	}

	@Test(priority = 27, retryAnalyzer = Retry.class)
	public void Start_Campaign_With_Team_Round_Order_make_powerdialer_call_Call_Should_recive_in_simultaneous_Order()
			throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.teamPage());
		webApp2.clickOnTeam("Team");
		webApp2.clickOnRoundRobin();
		webApp2.clickOnUpdateButton();
		driver4.get(url.signIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();

		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee1("Team");
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);

			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(5);
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2SubUser.completedcalls(1);

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2);
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(5);
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2SubUser.waitForIncomingCallingScreen();
		}
	}

	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void Start_Campaign_when_no_number_is_allocated_powerdialer_user_call_Call_Should_recieve_to_user_as_normal()
			throws Exception {

		phoneApp2.selectDepartmentNumber(number);
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.selectDepartmentNumber(number);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "No");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "No");
		driver4.get(url.signIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();

		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
//			assertEquals("Incoming Via " + departmentName, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.completedcalls(1);
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(1);
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
//			assertEquals("Incoming Via " + departmentName, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.completedcalls(2);
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(1);

		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
//			assertEquals("Incoming Via " + departmentName, phoneApp2SubUser.validateCallingScreenIncomingVia());

			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.completedcalls(1);
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(1);
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
//			assertEquals("Incoming Via " + departmentName, phoneApp2SubUser.validateCallingScreenIncomingVia());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.completedcalls(2);
			Common.pause(10);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			Common.pause(1);
		}

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number1);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
	}

	@Test(priority = 25, retryAnalyzer = Retry.class)
	public void Inactive_Running_Campaign_Campaign_Should_Stop() throws Exception {

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		// +++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("off");
		Common.pause(2);
		webApp2PowerDialerPage.clickOnYesButton();
		Common.pause(2);
		phoneApp2.clickOnIncomingHangupButton();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false, "not getting power dialer next call");
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());
		Common.pause(10);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(status, webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc4Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc4Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(enabled=false)//(priority = 24, retryAnalyzer = Retry.class)
	public void Logout_by_asign_User_when_campaign_is_Running_Validation_For_Minimum_Avl_Users_Limit_Goes_Down_DIsplayed()
			throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		Common.pause(2);

		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");

		Common.pause(5);
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("2");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();
			//assertEquals("account Three", phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			// phoneApp2.completedcalls(1);
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnSideMenu();
			phoneApp2SubUser.clickOnLogout();
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(10);
			assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
					"not getting power dialer next call");

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			//assertEquals("account Three", phoneApp2SubUser.validateCallingScreenIncomingNumber());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			// phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnSideMenu();
			phoneApp2SubUser.clickOnLogout();
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(10);

			assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(), false,
					"not getting power dialer next call");

		}
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();
		try {
		assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
		 } catch (AssertionError e) {
			 assertEquals("No Answer", phoneApp2.validateCallStatusInCallDetail());
		 }
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
//		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
//		assertEquals(webApp2.validateClientNumber(), number3);
		assertEquals(status, webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
//		assertEquals(webApp2.validateClientNumberFor2ndentry(), number3);
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, "0.040");
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, "0.040");
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void Logout_by_asign_User_when_campaign_is_Running_Validation_For_Minimum_Avl_Users_Limit_Not_Goes_Down_Campaign_Remains_Running()
			throws Exception {

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");

		Common.pause(5);
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);
			if (i == 60) {
				break;
			}
		}
		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {
			phoneApp2.waitForIncomingCallingScreen();
			// assertEquals(number3, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2SubUser.waitForIncomingCallingScreen();
			// phoneApp2.completedcalls(1);
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnSideMenu();
			phoneApp2SubUser.clickOnLogout();
			assertEquals(webApp2PowerDialerPage.validateMinimum2UserLoggedInMessage(), false,
					"minimum campaign loggedin message");
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(5);
			phoneApp2.waitForIncomingCallingScreen();

			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp2.clickOnIncomingRejectButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			// assertEquals(number3,
			// phoneApp2SubUser.validateCallingScreenIncomingNumber());
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
		    phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();
			phoneApp2SubUser.clickOnSideMenu();
			phoneApp2SubUser.clickOnLogout();
			assertEquals(webApp2PowerDialerPage.validateMinimum2UserLoggedInMessage(), false,
					"minimum campaign loggedin message");
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();
			Common.pause(5);
			phoneApp2.waitForIncomingCallingScreen();
			webApp2PowerDialerPage.campaignActivation("off");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp2.clickOnIncomingRejectButton();

		}
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals("Rejected", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName, phoneApp2.validateDepartmentNameinCallDetail());
		Common.pause(10);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		// assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller
		// name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
//		assertEquals(webApp2.validateClientNumber(), number3);
		assertEquals(status, webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		// assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(),
		// "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true,
				"-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
//		assertEquals(webApp2.validateClientNumberFor2ndentry(), number3);
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore,WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc4Num1));
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc4Num1));
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void User_Is_Busy_on_Normal_outgoing_Call_Campaign_Start_No_Client_should_get_call() throws Exception {
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "off", "off");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		phoneApp2.enterNumberinDialer(number3);
		phoneApp2.clickOnDialButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp3.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void User_Is_Busy_on_Normal_incoming_Call_Campaign_Start_No_Client_should_get_call() throws Exception {
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		webApp3.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp2.waitForDialerPage();
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void User_Is_Busy_on_Internal_Call_Campaign_Start_No_Client_should_get_call() throws Exception {

		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String account2MainUserExtension = "+" + webApp2.getUserExtensionNumber();
		driver4.get(url.usersPage());
		webApp2.navigateToUserSettingPage(account2EmailSubUser);
		Common.pause(9);
		String account2SubUserExtension = "+" + webApp2.getUserExtensionNumber();
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		phoneApp2.enterNumberinDialer(account2SubUserExtension);
		phoneApp2.clickOnDialButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		Common.pause(10);
		phoneApp2SubUser.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
		Common.pause(10);
		phoneApp2.waitForDialerPage();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc1Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc1Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void User_Is_Busy_on_Queue_Call_Campaign_Start_No_Client_should_get_call() throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Common.pause(9);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Common.pause(2);
		webApp2.enterCallQueueDuration("300");
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		phoneApp3.enterNumberinDialer(number);
		phoneApp3.clickOnDialButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.clickOnOutgoingHangupButton();
		Common.pause(2);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(2);
		assertEquals(phoneApp3.validateIncomingCallingScreenIsdisplayed(), false);
		assertEquals(phoneApp4.validateIncomingCallingScreenIsdisplayed(), false);
		phoneApp2.clickOnOutgoingHangupButton();
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
	}
	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void power_Dialer_call_remains_continue_after_queue_Call_Answer_and_hangup_between_campaign_is_running()
			throws Exception {

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		Common.pause(3);
		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();
			phoneApp1.validateDurationInCallingScreen(1);
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp1.enterNumberinDialer(number);
			phoneApp1.clickOnDialButton();
			phoneApp1.validateDurationInCallingScreen(1);
			Common.pause(10);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp2.waitForIncomingCallingScreen();
			assertEquals(number2, phoneApp2.validateCallingScreenIncomingNumber());
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(5);
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc1Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc1Num1));
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc1Num1));
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void active_Campaign_when_Credit_is_Less_Than_$1_Validation_For_minimum_credit_display() throws Exception {
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		String Url = driver4.getCurrentUrl();
		creditAPI.UpdateCredit("0.09", "0", "0", webApp2.getUserId(Url));
		driver4.get(url.signIn());
		webApp2.clickLeftMenuPlanAndBilling();
		driver4.get(url.signIn());
		webApp2.clickLeftMenuPlanAndBilling();
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		Common.pause(2);
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");

		Common.pause(5);
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2.closeLowcreditNotification();
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		driver4.get(url.signIn());
		Common.pause(5);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.campaignActivation("on");
		assertEquals("Your Balance is low, Please add credits before start campaign",
				webApp2PowerDialerPage.validatedateNoticemessage());

	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Campaign_have_some_blocked_number_Campaign_Start_Client_should_get_call() throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		String Url = driver4.getCurrentUrl();
		creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
		driver4.get(url.planPage());
		driver4.get(url.signIn());
		webApp2.clickLeftMenuPlanAndBilling();
		driver4.get(url.signIn());
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuSetting();
		webApp2.scrollToCallBlockingTxt();
		webApp2blocking.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2blocking.verifyEnterNumberTextBoxDisplay());
		webApp2blocking.enterNumberIntoTextBox(number3);
		webApp2blocking.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2blocking.verifySuccessMessage());
		webApp2blocking.clickOnPlustBtnToAddBlackListNumber();
		assertTrue(webApp2blocking.verifyEnterNumberTextBoxDisplay());
		webApp2blocking.enterNumberIntoTextBox(number4);
		webApp2blocking.clickOnSaveButton();
		assertEquals("Number added To Blacklist successfully", webApp2blocking.verifySuccessMessage());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
		webApp2.clickLeftMenuSetting();
		Common.pause(2);
		webApp2.scrollToCallBlockingTxt();
		Common.pause(1);
		webApp2.deleteAllNumbersFromBlackList();

	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Assign_2nd_Campaign_to_same_user_and_start_cpmgn_Client_should_get_call_from_first_started_campaign_then_from_2nd_campaign()
			throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		String Url = driver4.getCurrentUrl();
		creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
		driver4.get(url.planPage());
		driver4.get(url.signIn());
		driver4.get(url.planPage());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			webApp2PowerDialerPage.clickOnAddCampaignButton();
			webApp2PowerDialerPage.enterCampaignName("Campaign 3");
			webApp2PowerDialerPage.selectAllocation("Users");
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2);
			Common.pause(2);
			webApp2PowerDialerPage.selectFromNumber(number);
			webApp2PowerDialerPage.autoVoicemailDrop("off");
			webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
			Common.pause(5);
			// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
			webApp2PowerDialerPage.campaignDescription("Test automation1");
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(5);
			webApp2PowerDialerPage.secondcampaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();

			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			webApp2PowerDialerPage.clickOnAddCampaignButton();
			webApp2PowerDialerPage.enterCampaignName("Campaign 3");
			webApp2PowerDialerPage.selectAllocation("Users");
			Common.pause(2);
			webApp2PowerDialerPage.selectAssignee(callerName2);
			Common.pause(2);
			webApp2PowerDialerPage.selectFromNumber(number);
			webApp2PowerDialerPage.autoVoicemailDrop("off");
			webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
			Common.pause(5);
			// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
			webApp2PowerDialerPage.campaignDescription("Test automation1");
			webApp2PowerDialerPage.clickOnSaveButton();
			Common.pause(5);
			webApp2PowerDialerPage.secondcampaignActivation("on");
			webApp2PowerDialerPage.clickOnYesButton();
			phoneApp2.clickOnIncomingHangupButton();

			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Assign_two_Campaign_to_same_user_Start_Both_campaign_Client_should_get_call_from_first_started_campaign_then_from_2nd_campaign()
			throws Exception {

		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		String Url = driver4.getCurrentUrl();
		creditAPI.UpdateCredit("500", "0", "0", webApp2.getUserId(Url));
		driver4.get(url.planPage());
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 3");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		Common.pause(2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation1");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(5);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		Common.pause(1);
		webApp2PowerDialerPage.secondcampaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp3.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp4.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp3.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();
		} else {
			phoneApp4.waitForIncomingCallingScreen();
			phoneApp4.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(1);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			phoneApp2.clickOnIncomingHangupButton();
			phoneApp3.waitForIncomingCallingScreen();
			phoneApp3.clickOnIncomimgAcceptCallButton();
			phoneApp2.waitForIncomingCallingScreen();
			phoneApp2.completedcalls(2);
			phoneApp2.clickOnIncomimgAcceptCallButton();
			Common.pause(6);
			phoneApp2.clickOnIncomingHangupButton();

		}
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void Client_Welcome_Message_Enabled_Call_Missed_From_CLient() throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		// webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2.completedcalls(1);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.waitForDialerPage();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void Client_VoiceMail_Enabled_Call_Missed_From_CLient() throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		// webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		Common.pause(19);
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(20);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		webApp2PowerDialerPage.campaignActivation("off");
		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Client_VoiceMail_Enabled_Call_Rejected_From_CLient() throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		// webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(20);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void Client_Welcome_Message_Enabled_Call_Rejected_From_CLient() throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		// webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		// webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
//		phoneApp3.waitForIncomingCallingScreen();
//		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		// phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2.completedcalls(1);
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomingRejectButton();
		Common.pause(7);
		// phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Client_Welcome_Message_Enabled_Call_Hangup_From_Agent_Itself() throws Exception {

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("on", "off", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		// webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		Common.pause(2);
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		// webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(2);
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(7);
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc3Num1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc3Num1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void call_queue_off_Getting_another_incoming_call_on_assignees_number_when_campaign_call_is_Running()
			throws Exception {
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		Common.pause(3);
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		// phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(2);
		phoneApp1.waitForDialerPage();

		Common.pause(4);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("User is not available", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("User is not available", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals("-", webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(false, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost1);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost1);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void call_queue_Getting_another_incoming_call_on_assignees_number_when_campaign_call_is_Running_Call_Goes_To_Voicemail()
			throws Exception {
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(30);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp1.waitForDialerPage();

		Common.pause(4);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Incoming", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(10);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals("-", webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Voicemail", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayed());
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, callCost2);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, callCost2);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Credit_of_account_going_low_when_campaign_is_running_AutoRecharger_ON() throws Exception {

		System.out.println("welcome");
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url1 = driver4.getCurrentUrl();
		userIDWebApp2 = webApp2.getUserId(Url1);
		System.out.println("userIDWebApp1:" + userIDWebApp2);
		creditAPI.deleteRechargeActivity(userIDWebApp2);
		
		webApp2.clickLeftMenuPlanAndBilling();
		webbApp2page.selectAddCreditDropdownValue("30");
		webbApp2page.clickAddCreditButton();
		webbApp2page.clickOnYesButtonValidation();
		webbApp2page.autoRechargeToggle("on");
		webbApp2page.selectBalanceFallsBelow("5");
		webbApp2page.selectRechargeCredit("30");
		webbApp2page.clickSaveRechargeButton();
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driver4.getCurrentUrl();
		creditAPI.UpdateCredit("5.001", "0", "0", webApp2.getUserId(Url));
		creditAPI.deleteRechargeActivity(userIDWebApp2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webbApp2page.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webbApp2page.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);
		Common.pause(1);
		webbApp2page.scrollToBillingAndCardTxt();
		webbApp2page.updateCardDetailWithSuccessCard();
		webbApp2page.clickLeftMenuDashboard();
		Common.pause(2);

        phoneApp2.clickOnSideMenu();
        String callerName2 = phoneApp2.getCallerName();
        driver1.get(url.dialerSignIn());
        phoneApp2.selectDepartmentNumber(number);
        String departmentName = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		driver4.get(url.signIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();

		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2.closeLowcreditNotification();
		webApp2PowerDialerPage.campaignDescription("Test automation");

		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
//		assertEquals("Incoming Via " + departmentName, phoneApp2.validateCallingScreenIncomingVia());

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2.completedcalls(1);
		Common.pause(10);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(1);
	    driver.get(url.planPage());
	    Common.pause(5);
	    driver.get(url.planPage()); 
		webbApp2page.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webbApp2page.availableCreditValue();
		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);
		System.out.println("-----settingcreditValueUpdated----" + settingcreditValueUpdated);
		assertEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Credit_of_account_going_low_when_campaign_is_running_AutoRecharger_OFF() throws Exception {

		System.out.println("welcome");
		
		Common.pause(3);
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Thread.sleep(9000);

		String Url1 = driver4.getCurrentUrl();
		userIDWebApp2 = webApp2.getUserId(Url1);
		System.out.println("userIDWebApp1:" + userIDWebApp2);
		creditAPI.deleteRechargeActivity(userIDWebApp2);
		
		webApp2.clickLeftMenuPlanAndBilling();
		webbApp2page.autoRechargeToggle("on");
		webbApp2page.selectBalanceFallsBelow("5");
		webbApp2page.selectRechargeCredit("30");
		webbApp2page.clickSaveRechargeButton();
		webbApp2page.autoRechargeToggle("off");
		webApp2.userspage();
		webApp2.navigateToUserSettingPage(account2Email);
		Common.pause(9);
		String Url = driver4.getCurrentUrl();
		creditAPI.UpdateCredit("5.001", "0", "0", webApp2.getUserId(Url));
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueBefore = webbApp2page.availableCreditValue();
		double charge = 0.020;
		float newCharge = new Float(charge);
		String ExpectedsettingcreditValue = webbApp2page.validateCreditAddedSuccessfullySMS(settingcreditValueBefore,
				"30", newCharge);

		System.out.println("-----ExpectedsettingcreditValue----" + ExpectedsettingcreditValue);
		Common.pause(1);
		webbApp2page.scrollToBillingAndCardTxt();
		webbApp2page.updateCardDetailWithSuccessCard();
		webbApp2page.clickLeftMenuDashboard();
		Common.pause(2);

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.selectDepartmentNumber(number);
		String departmentName = phoneApp2.validateDepartmentNameinDialpade();

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		driver4.get(url.signIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();

		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2.closeLowcreditNotification();
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		Common.pause(3);
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
//		assertEquals("Incoming Via " + departmentName, phoneApp2.validateCallingScreenIncomingVia());

		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2.completedcalls(1);
		Common.pause(10);
		phoneApp2.clickOnIncomingHangupButton();
		Common.pause(1);

		webbApp2page.clickLeftMenuDashboard();
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webbApp2page.availableCreditValue();
		assertNotEquals(settingcreditValueUpdated, ExpectedsettingcreditValue);
	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void two_campaign_is_added_with_different_assignee() throws Exception {
		powerDialerCustomNumber.write(sheetNumber, secondRow, secondColumn, number4);

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "on", "off", "off", "open", "off", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
//		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(3);
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 3");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
//		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialerCustomNumber.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		webApp2PowerDialerPage.secondcampaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
//		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2SubUser.waitForIncomingCallingScreen();
//		phoneApp2SubUser.completedcalls(1);
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Common.pause(4);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2SubUser.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();
		phoneApp2SubUser.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
//		assertTrue("Voicemail".equalsIgnoreCase(phoneApp2.validateCallStatusInCallDetail().replace(" ", "")),
//				"Voicemail status not matched");
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2Subuser, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, outgoingCallPriceAcc4Num1_2Min);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, outgoingCallPriceAcc4Num1_2Min);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void two_campaign_is_added_with_different_assignee_2nd_campaign_assigned_user_is_logged_out() throws Exception {
		//powerDialerCustomNumber.write(sheetNumber, secondRow, secondColumn, number4);

		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		Common.pause(2);
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(3);
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 3");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer4Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		webApp2PowerDialerPage.secondcampaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.completedcalls(1);
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();

		Common.pause(4);
		phoneApp2SubUser.clickOnIncomingHangupButton();
		phoneApp2SubUser.clickOnSideMenu();
		phoneApp2SubUser.clickOnLogout();
		Common.pause(20);
		loginDialer(phoneApp2SubUser, account2EmailSubUser, account1Password);
		driver4.get(url.signIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.secondcampaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp2SubUser.validateSessionButtononDialer(), true);
		phoneApp2SubUser.waitForIncomingCallingScreen();
		phoneApp2SubUser.completedcalls(2);
		phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
	
		webApp2PowerDialerPage.secondcampaignActivation("off");
		webApp2PowerDialerPage.clickOnYesButton();
		Common.pause(2);
		webApp2PowerDialerPage.campaignActivation("off");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2SubUser.clickOnIncomingHangupButton();
		Common.pause(15);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2Subuser, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(status, webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(callerName2Subuser, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(status, webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore,  WebAppAdd1.multiplicationOfTwoStringNumber1("4", outgoingCallPriceAcc4Num1));
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, WebAppAdd1.multiplicationOfTwoStringNumber1("4", outgoingCallPriceAcc4Num1));
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Add_Campaign_with_different_assignee_Number_should_display_as_per_allocation_While_add_campaign() throws Exception {

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++
        phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "No");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		assertEquals(true, webApp2PowerDialerPage.verifyNumerInFromNumber(number1));
		assertEquals(false, webApp2PowerDialerPage.verifyNumerInFromNumber(number));
		
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(3);
		webApp2SubuserPowerDialerPage.clickOnPowerDialerSideMenu();
		Common.pause(3);
		webApp2SubuserPowerDialerPage.clickOnAddCampaignButton();
		webApp2SubuserPowerDialerPage.enterCampaignName("Campaign 3");
		webApp2SubuserPowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
//		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2SubuserPowerDialerPage.selectAssignee(callerName2Subuser);
		assertEquals(true, webApp2SubuserPowerDialerPage.verifyNumerInFromNumber(number));
		assertEquals(true, webApp2SubuserPowerDialerPage.verifyNumerInFromNumber(number1));
		webApp2SubuserPowerDialerPage.autoVoicemailDrop("off");
		webApp2SubuserPowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2SubuserPowerDialerPage.campaignDescription("Test automation");
		webApp2SubuserPowerDialerPage.clickOnSaveButton();
		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");

	}
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void start_Campaign_For_Assigned_user_All_calls_should_serve_to_user() throws Exception {
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		int Row;
		for(Row=1; Row <= 15; Row++ ){
			powerDialer15NUmbersExcel.write(sheetNumber, Row, secondColumn, number3);
		}
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerDialer15Number.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		 for (int i = 2; i <= 15; i++) {
			 phoneApp2.waitForIncomingCallingScreen();
			 webApp2PowerDialerPage.validateCompletedCallsinWeb(i, 15);
			 phoneApp2.completedcalls(i);
			 phoneApp2.clickOnIncomimgAcceptCallButton();
			 Common.pause(7);
			 phoneApp2.clickOnIncomingHangupButton();
		 }
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc4Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String CallCost = WebAppAdd1.multiplicationOfTwoStringNumber1("15", outgoingCallPriceAcc4Num1);
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, CallCost);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, CallCost);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	@Test(priority = 73, retryAnalyzer = Retry.class)
	public void Start_Campaign_with_User_Assignment_Edit_Campiagn_with_Second_Number() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());
		phoneApp2.selectDepartmentNumber(number1);
		String departmentName2 = phoneApp2.validateDepartmentNameinDialpade();
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);

		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		webApp2PowerDialerPage.selectFromNumber(number1);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		
		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		assertEquals("Incoming Via " + departmentName2, phoneApp2.validateCallingScreenIncomingVia(),
				"--Department name of dialer2--");
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

//		 validate via number in All Calls page
		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

	}
	
	
	@Test(priority = 74, retryAnalyzer = Retry.class)
	public void Start_Campaign_Assignment_After_Edit_from_User_to_Team() throws Exception {
		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.deleteAllPowerDialerEntries();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();


		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2Subuser), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1),"0 out of 1");
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);

		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		webApp2PowerDialerPage.selectAllocation("Team");
		Common.pause(5);
//		webApp2PowerDialerPage.clickOnAssigneeDropdown();
		webApp2PowerDialerPage.selectAssignee1("Team");
		Common.pause(5);
		webApp2PowerDialerPage.selectFromNumber(number);
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee("Team"), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
//		String date1 = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Common.pause(9);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		int i = 1;
		while (phoneApp2.validateIncomingCallingScreenIsdisplayed() == false
				&& phoneApp2SubUser.validateIncomingCallingScreenIsdisplayed() == false) {
			i = i + 1;
			Thread.sleep(1000);

			if (i == 60) {
				break;
			}
		}

		if (phoneApp2.validateIncomingCallingScreenIsdisplayed() == true) {

			phoneApp2.clickOnIncomimgAcceptCallButton();
			// phoneApp2.completedcalls(2)
			Common.pause(5);

			phoneApp2.clickOnIncomingHangupButton();

		} else {
			phoneApp2SubUser.waitForIncomingCallingScreen();
			phoneApp2SubUser.clickOnIncomimgAcceptCallButton();
			// phoneApp2SubUser.completedcalls(1);
			Common.pause(5);
			phoneApp2SubUser.clickOnIncomingHangupButton();

		}
	}
	
	@Test(priority = 75, retryAnalyzer = Retry.class)
	public void add_Campaign_with_User_Assignment_Without_Discription() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.AttemptperContactToggle("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		assertEquals(webApp2PowerDialerPage.campaigndiscription(),false);
		// webApp2PowerDialerPage.clickOndownloadcapaign();
		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		
//		String date1 = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
	}
	
	@Test(priority = 76, retryAnalyzer = Retry.class)
	public void add_Campaign_with_User_Assignment_Edit_And_remove_description() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());
		
		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectAssignee(callerName2Subuser);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.AttemptperContactToggle("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		assertEquals(webApp2PowerDialerPage.campaigndiscription(),true);
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
		webApp2PowerDialerPage.clickOnEditButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign updated");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("");
		
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2.ValidateAddcampSuccessmessage(), "Campaign is updated.");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign updated");
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		assertEquals(webApp2PowerDialerPage.campaigndiscription(),false);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
	}

	
	@Test(enabled = false)//(priority = 77, retryAnalyzer = Retry.class)
	public void Start_campiagn_with_main_user_Queue_call_should_serve_after_power_Dilaer_and_PD_campaign_Running()
			throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		phoneApp2SubUser.clickOnLogout();

		driver4.get(url.numbersPage());
		webApp2.navigateToNumberSettingpage(number);
		Thread.sleep(9000);
		webApp2.SetNumberSettingToggles("off", "off", "off", "off", "open", "off", "on", "off");
		webApp2.userTeamAllocation("users");
		Common.pause(3);
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2, "Yes");
		webApp2PowerDialerPage.selectSpecificUserOrTeam(callerName2Subuser, "Yes");
		Common.pause(3);
		webApp2.enterCallQueueDuration("300");

		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

		// +++++++++++++++++++++++++++++++++++++++++

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialer2Numbers.xlsx");
		Common.pause(5);
		// webApp2PowerDialerPage.enterMinumAvailableUserToRunCampaign("1");
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();

		phoneApp3.waitForIncomingCallingScreen();
		phoneApp3.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();

		phoneApp1.enterNumberinDialer(number);
		phoneApp1.clickOnDialButton();
		phoneApp1.validateDurationInCallingScreen(1);
		Common.pause(5);

		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
        phoneApp2.validateNumberInCallingScreen(number2);

		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(11);
		phoneApp1.clickOnOutgoingHangupButton();
		phoneApp4.waitForIncomingCallingScreen();
		phoneApp4.clickOnIncomimgAcceptCallButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(2);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		phoneApp2.clickOnIncomingHangupButton();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		assertEquals(callerName2, webApp2.validateCallerName(),"-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(callCost1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed2ndentry(), false, "-----verify powerDilaercall in webapp2--");
		assertEquals(callerName2, webApp2.validateCallerNameFor2ndEntry(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor2ndEntry(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor2ndEntry());
		assertEquals(callCost1, webApp2.validateCallCostFor2ndEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButtonFor2ndEntry();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor2ndEntry());
		
		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed3rdentry(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals(callerName2, webApp2.validateCallerNameFor3rdEntry(), "-----verify caller name in webapp2--");
		assertEquals("Completed", webApp2.validateCallstatusFor3rdEntry(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateIncomingCallTypeIsDisplayedFor3rdEntry());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCostFor3rdEntry(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validaterecordingUrlFor3rdEntry());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc3Num1));
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore,WebAppAdd1.multiplicationOfTwoStringNumber("3", outgoingCallPriceAcc3Num1));
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	@Test(enabled = false)//(priority = 78, retryAnalyzer = Retry.class)
	public void start_Campaign_of_Number_without_Country_code() throws Exception {
		
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		int Row;
		for(Row=1; Row <= 4; Row++ ){
			powerDialerCustomNumber.write(sheetNumber, Row, secondColumn, number4.substring(2));
		}
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialerCustomNumber.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 4), "0 out of 4");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		assertEquals(phoneApp2.validateIncomingCallingScreenIsdisplayed(),false);
		Common.pause(100);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(4, 4), "4 out of 4");
		
	}
	
	@Test(enabled = false)//(priority = 79, retryAnalyzer = Retry.class)
	public void start_Campaign_woth_Number_With_And_without_Country_code() throws Exception {
		
		powerDialerCustomNumber.write(sheetNumber, secondRow, secondColumn, number3.substring(2));
		powerDialerCustomNumber.write(sheetNumber, thirdRow, secondColumn, number3);
		powerDialerCustomNumber.write(sheetNumber, forthRow, secondColumn, number3.substring(2));
		powerDialerCustomNumber.write(sheetNumber, fifthRow, secondColumn, number3);
		
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialerCustomNumber.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 4), "0 out of 4");
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(2);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		
		phoneApp2.waitForIncomingCallingScreen();
	    webApp2PowerDialerPage.validateCompletedCallsinWeb(4, 4);
		phoneApp2.completedcalls(4);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);

		// validate Number in All Calls page
//		assertEquals(number2, phoneApp2.validateNumberInAllcalls());

		// validate via number in All Calls page
//		assertEquals(departmentName2, phoneApp2.validateViaNumberInAllCalls());

		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String CallCost = WebAppAdd1.multiplicationOfTwoStringNumber1("2", outgoingCallPriceAcc3Num1);
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, CallCost);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, CallCost);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	@Test(priority = 80, retryAnalyzer = Retry.class)
	public void start_Campaign_of_Number_with_space_row() throws Exception {
		
		powerDialerCustomNumber.write(sheetNumber, secondRow, secondColumn, number3);
		powerDialerCustomNumber.write(sheetNumber, thirdRow, secondColumn, "");
		powerDialerCustomNumber.write(sheetNumber, forthRow, secondColumn, "");
		powerDialerCustomNumber.write(sheetNumber, fifthRow, secondColumn, number3);
		
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialerCustomNumber.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 2), "0 out of 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		webApp2PowerDialerPage.validateCompletedCallsinWeb(2, 2);
		phoneApp2.completedcalls(2);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);


		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String CallCost = WebAppAdd1.multiplicationOfTwoStringNumber1("2", outgoingCallPriceAcc3Num1);
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, CallCost);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, CallCost);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	@Test(priority = 82, retryAnalyzer = Retry.class)
	public void start_Campaign_of_Number_with_special_charecter() throws Exception {
		
		powerDialerCustomNumber.write(sheetNumber, secondRow, secondColumn, number3);
		powerDialerCustomNumber.write(sheetNumber, thirdRow, secondColumn, "!@#$%^&*()");
		powerDialerCustomNumber.write(sheetNumber, forthRow, secondColumn, "-------){]");
		powerDialerCustomNumber.write(sheetNumber, fifthRow, secondColumn, number3);
		
		driverWebApp3.get(url.numbersPage());
		webApp3.navigateToNumberSettingpage(number3);
		Thread.sleep(9000);
		webApp3.SetNumberSettingToggles("off", "off", "off", "off", "open", "on", "off", "off");
		
		webApp2.clickLeftMenuPlanAndBilling();
		webApp2.clickLeftMenuDashboard();

		String leftPanelcreditValBefore = webApp2.getCreditValueFromLeftPanel();
		webApp2.clickLeftMenuPlanAndBilling();

		String settingcreditValueBefore = webApp2.availableCreditValue();
		assertEquals(leftPanelcreditValBefore, settingcreditValueBefore);

//+++++++++++++++++++++++++++++++++++++++++

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importCSVFile("powerdialerCustomNumber.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 4), "0 out of 4");

		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");

		Common.pause(5);

		webApp2PowerDialerPage.campaignActivation("on");
		webApp2PowerDialerPage.clickOnYesButton();
		phoneApp2.waitForIncomingCallingScreen();
		phoneApp2.completedcalls(1);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(5);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForIncomingCallingScreen();
		webApp2PowerDialerPage.validateCompletedCallsinWeb(4, 4);
		phoneApp2.completedcalls(4);
		phoneApp2.clickOnIncomimgAcceptCallButton();
		Common.pause(7);
		phoneApp2.clickOnIncomingHangupButton();
		phoneApp2.waitForDialerPage();

		Common.pause(10);
		phoneApp2.clickOnSideMenu();
		phoneApp2.clickOnAllCalls();
		Thread.sleep(4000);


		// validate call type in all calls page
		assertEquals("Outgoing", phoneApp2.validatecallType());

		phoneApp2.clickOnRightArrow();

		// validate Number on call details page
//		assertEquals(number2, phoneApp2.validateNumberInCallDetail());

		assertEquals("Completed", phoneApp2.validateCallStatusInCallDetail());
		String status = phoneApp2.validateCallStatusInCallDetail();

//		assertEquals("Via " + departmentName2, phoneApp2.validateDepartmentNameinCallDetail());

		driver4.get(url.signIn());
		webApp2.clickOnActivitFeed();
		Common.pause(5);
//		assertEquals(departmentName2, webApp2.validateDepartmentName(), "----verify departmrnt name in webapp2");

		assertEquals(callerName2, webApp2.validateCallerName(), "-----verify caller name in webapp2--");
		assertEquals(webApp2.validatepowerDialericoninactivityfeed(), true, "-----verify powerDilaercall in webapp2--");
		assertEquals("Completed", webApp2.validateCallStatus(), "----verify call statuts in webapp2---");
		assertEquals(true, webApp2.validateOutgoingCallTypeIsDisplayed());
		assertEquals(outgoingCallPriceAcc3Num1, webApp2.validateCallCost(), "------verify call cost in webapp2-----");
		webApp2.clickOniButton();
		Common.pause(5);
		assertEquals(true, webApp2.validateRecordingUrl());

		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		driver4.get(url.signIn());
		String CallCost = WebAppAdd1.multiplicationOfTwoStringNumber1("2", outgoingCallPriceAcc3Num1);
		String settingcreditValueAfter = webApp2.validateCreditDeduction(settingcreditValueBefore, CallCost);
		String leftPanelcreditValueAfter = webApp2.validateCreditDeduction(leftPanelcreditValBefore, CallCost);
		Common.pause(2);
		Thread.sleep(20000);
		webApp2.clickLeftMenuDashboard();
		Common.pause(2);
		Thread.sleep(10000);
		webApp2.clickLeftMenuPlanAndBilling();
		String settingcreditValueUpdated = webApp2.availableCreditValue();
		String leftPanelcreditValueUpdated = webApp2.getCreditValueFromLeftPanel();
		assertEquals(leftPanelcreditValueUpdated, leftPanelcreditValueAfter);
		assertEquals(settingcreditValueUpdated, settingcreditValueAfter);

	}
	
	
	@Test(priority = 81, retryAnalyzer = Retry.class)
	public void Add_Campaign_with_Invalid_file_formate() throws Exception {

		phoneApp2.clickOnSideMenu();
		String callerName2 = phoneApp2.getCallerName();
		driver1.get(url.dialerSignIn());

		phoneApp2SubUser.clickOnSideMenu();
		String callerName2Subuser = phoneApp2SubUser.getCallerName();
		driver6.get(url.dialerSignIn());

		webApp2PowerDialerPage.clickOnPowerDialerSideMenu();
		webApp2PowerDialerPage.clickOnAddCampaignButton();
		webApp2PowerDialerPage.enterCampaignName("Campaign 2");
		webApp2PowerDialerPage.selectAllocation("Users");
		Common.pause(2);
		webApp2PowerDialerPage.selectAssignee(callerName2);
		webApp2PowerDialerPage.selectFromNumber(number);
		webApp2PowerDialerPage.autoVoicemailDrop("off");
		webApp2PowerDialerPage.importInvalidFile("url Configuration.properties");
		assertEquals(webApp2PowerDialerPage.validatepowerDUploadCampError(), "You can only upload csv/xlsx file!");
		webApp2PowerDialerPage.importInvalidFile("Forgot Password.xlsx");
		Common.pause(5);
		webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.validatepowerDUploadCampValidationMsg(), "Please upload valid campaign sheet");
		webApp2PowerDialerPage.importCSVFile("powerdialer1.xlsx");
		
		Common.pause(5);
		String campaignDiscription = webApp2PowerDialerPage.campaignDescription("Test automation");
		webApp2PowerDialerPage.clickOnSaveButton();
		assertEquals(webApp2PowerDialerPage.ValidateAssignee(callerName2), true);
		assertEquals(webApp2PowerDialerPage.validateCampaignsName(), "Campaign 2");
		assertEquals(webApp2PowerDialerPage.validateCampaignsAssignerName(), callerName2);
		String date = Common.powerDialerDate();
		assertEquals(webApp2PowerDialerPage.validatedateofcreation(date),true);
		assertEquals(webApp2PowerDialerPage.validateCompletedCallsinWeb(0, 1), "0 out of 1");
		webApp2PowerDialerPage.validatecampaigndiscription(campaignDiscription);
	
	}
}
