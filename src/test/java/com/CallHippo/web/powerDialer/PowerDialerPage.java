package com.CallHippo.web.powerDialer;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.google.inject.spi.Element;

public class PowerDialerPage {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait2;
	Actions action;

	PropertiesFile url;
	JavascriptExecutor js;
	PropertiesFile credential;

	public PowerDialerPage(WebDriver driver) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 100);
		wait2 = new WebDriverWait(this.driver, 10);
		action = new Actions(this.driver);
		url = new PropertiesFile("Data\\url Configuration.properties");
		js = (JavascriptExecutor) driver;

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	@FindBy(xpath = "//a[@href='/powerDialer'][contains(.,'Power Dialer')]")
	private WebElement powerDilerSideMenuLink;
	@FindBy(xpath = "(//button[@type='button'][contains(.,'Add Campaigns')])[1]")
	private WebElement addCampaignBtn;
	@FindBy(xpath = "//div[@class='ant-table-placeholder'][contains(.,'No campaign has been assigned')]")
	private WebElement noCampaign;
	@FindBy(xpath = "//input[@id='basic_campaignName']")
	private WebElement campaignName;
	@FindBy(xpath = "//label[contains(.,'Allocation')]/parent::div/following-sibling::div//div[@class='ant-select-selection__rendered']")
	private WebElement allocationDropdown;
	@FindBy(xpath = "//label[contains(.,'Assignee')]/parent::div/following-sibling::div//div[@class='ant-select-selection__rendered']")
	private WebElement AssigneeDropdown;
	@FindBy(xpath = "//label[contains(.,'From Number')]/parent::div/following-sibling::div//div[@class='ant-select-selection__rendered']")
	private WebElement selectFromNumberDropdown;
	@FindBy(xpath = "//label[contains(.,'Auto Voicemail Drop')]/parent::div/following-sibling::div//button[@class='ant-switch']")
	private WebElement autoVoicemailDropSwitch;
	@FindBy(xpath = "//label[contains(.,'Attempt per Contact')]/parent::div/following-sibling::div//button[@class='ant-switch']")
	private WebElement AttemptperContact;

	@FindBy(xpath = "//label[contains(.,'Minimum available user to run campaign')]/parent::div/following-sibling::div//input[@type='text']")
	private WebElement mininumAvailableUserToRunCampaign;
	@FindBy(xpath = "//button[@type='button'][contains(.,'cloudUpload')]")
	private WebElement importCampaignCSVBtn;
	@FindBy(xpath = "//textarea[@id='basic_campaignDescription']")
	private WebElement campaignDiscriptionTextarea;
	@FindBy(xpath = "//button[@type='submit'][contains(.,'Save')]")
	private WebElement saveButton;
	@FindBy(xpath = "//input[@type='file']")
	WebElement fileUpload;
	@FindBy(xpath = "(//i[contains(.,'delete_forever')])[1]")
	private WebElement deleteCampaign;
	@FindBy(xpath = "//p[contains(.,'Are you sure you want to delete this campaign ?')]")
	private WebElement deleteCampaignMsg;
	@FindBy(xpath = "(//div[contains(@class,'ant-form-explain')])[1]")
	private WebElement errormessage;
	
	@FindBy(xpath = "//button[contains(.,'Yes')]")
	private WebElement yesButon;
	@FindBy(xpath = "(//i[contains(.,'file_download')])[1]")
	private WebElement downloadcapaign;
	@FindBy(xpath = "//a[contains(.,'Download Template')]")
	private WebElement downloadtempcampaign;
	
	@FindBy(xpath = "//a[contains(.,'How to use the Power Dialer?')]")
	private WebElement howtouserpDialer;
	@FindBy(xpath = "//h2[contains(@class,'single-article-title')]")
	private WebElement powerDilerpage;
	@FindBy(xpath = "//button[@type='button'][contains(.,'No')]")
	private WebElement noButton;
	@FindBy(xpath = "//table//tbody/tr[1]/td[1]/div")
	private WebElement campaignsTitle;
	@FindBy(xpath = "(//table//tbody/tr[1]/td[2]/div[contains(@class,'teamMemberSpan pwtmspan')])|(//table//tbody/tr[1]/td[2]/div/span[contains(@class,'teamMemberSpan pwtmspan')])")
	private WebElement campaignsAssignee;
	@FindBy(xpath = "//table//tbody/tr[1]/td[3]/div")
	private WebElement campaignsAssignor;
	@FindBy(xpath = "(//i[contains(.,'edit')])[1]")
	private WebElement editButton;
	@FindBy(xpath = "//button[contains(@role,'switch')]")
	private WebElement campaignActivationToggle;
	@FindBy(xpath = "(//button[contains(@role,'switch')])[2]")
	private WebElement secondcampaignActivationToggle;
	@FindBy(xpath = "//span[@class='ant-popover-open']")
	private WebElement dates;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	WebElement validationErrorMessage;
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-warning')]]/span")
	WebElement validationNoticeMessage;
	@FindBy(xpath = "//h2[contains(.,'Power Dialer')]")
	WebElement powerDialertiltle;
	@FindBy(xpath = "//span[@class='powerCsvUploadFileError']")
	WebElement powerDialerError;
	@FindBy(xpath = "//div[@class='ant-message-custom-content ant-message-error']/span")
	WebElement powerDialerValidation;
	@FindBy(xpath = "//input[@placeholder=\"Search\"] ")
	WebElement powerDialersearch;
	@FindBy(xpath = "//table//tbody/tr[1]/td[4]/div")
	private WebElement CallCompletedinWEB;
	@FindBy(xpath = "//a[contains(.,'Power Dialer contact information')]")
	private WebElement powerDilerinfo;
	@FindBy(xpath = "//div[contains(@class,'ant-collapse')]/child::div/div[contains(.,'name')]/child::span")
	private WebElement clickonplusiconofnameornumber;
	@FindBy(xpath = "//div[contains(@class,'ant-collapse')]/child::div/div[contains(.,'name')]/following-sibling::div/div[contains(.,'Dnaniel')]")
	private WebElement clickonplusiconofnameornumber1;
	
	
	
	//a[contains(.,'Power Dialer contact information')]
	
	
	public void clickOnPowerDialerSideMenu() {
		wait.until(ExpectedConditions.visibilityOf(powerDilerSideMenuLink)).click();

	}

	public boolean validateTableHeader(String title) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver
					.findElement(By.xpath("//span[@class='ant-table-column-title'][contains(.,'" + title + "')]"))));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean validateNoCampaign() {

		try {
			wait.until(ExpectedConditions.visibilityOf(noCampaign));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void clickOnAddCampaignButton() {
		wait.until(ExpectedConditions.visibilityOf(addCampaignBtn)).click();
	}

	public void enterCampaignName(String campaignName) {
		wait.until(ExpectedConditions.visibilityOf(this.campaignName));
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(this.campaignName)).clear();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(this.campaignName)).sendKeys(campaignName);
	}

	public void selectAllocation(String option) {
		wait.until(ExpectedConditions.visibilityOf(allocationDropdown)).click();
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//*[@role=\"option\"][contains(text(),'" + option + "')]"))))
				.click();
	}

	public void clickOnAssigneeDropdown() {
		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
	}

	public void selectAssignee(String userOrTeamName) {
		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//*[@role=\"option\"][contains(text(),'" + userOrTeamName + "')]"))))
				.click();
		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
	}

	public void selectAssignee1(String userOrTeamName) {
		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("(//*[@role=\"option\"][contains(text(),'" + userOrTeamName + "')])[2]"))))
				.click();
//		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
	}
	public void selectAssigneeTeam(String userOrTeamName) {
		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//*[@role=\"option\"][contains(text(),'" + userOrTeamName + "')]"))))
				.click();
//		wait.until(ExpectedConditions.visibilityOf(AssigneeDropdown)).click();
	}

	public void selectFromNumber(String fromNumber) {
		wait.until(ExpectedConditions.visibilityOf(selectFromNumberDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//*[@role=\"option\"][contains(.,'" + fromNumber + "')]")))).click();
	}
	
	public boolean verifyNumerInFromNumber(String fromNumber) {
		try {
		wait.until(ExpectedConditions.visibilityOf(selectFromNumberDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//*[@role=\"option\"][contains(.,'" + fromNumber + "')]")))).click();
		return true;
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(selectFromNumberDropdown)).click();
			return false;
			
		}	
	}

	public void autoVoicemailDrop(String value) {

		wait.until(ExpectedConditions.visibilityOf(autoVoicemailDropSwitch));
		if (value.contentEquals("on")) {

			if (autoVoicemailDropSwitch.getAttribute("aria-checked").equals("true")) {

			} else {
				autoVoicemailDropSwitch.click();
			}
		} else {
			if (autoVoicemailDropSwitch.getAttribute("aria-checked").equals("true")) {
				autoVoicemailDropSwitch.click();
			} else {

			}
		}

	}
	public void AttemptperContactToggle(String value) {

		wait.until(ExpectedConditions.visibilityOf(AttemptperContact));
		if (value.contentEquals("on")) {

			if (AttemptperContact.getAttribute("aria-checked").equals("true")) {

			} else {
				AttemptperContact.click();
			}
		} else {
			if (AttemptperContact.getAttribute("aria-checked").equals("true")) {
				AttemptperContact.click();
			} else {

			}
		}

	}

//	public void importCSVFile(String path) {
//		importCampaignCSVBtn.click();
//		wait.until(ExpectedConditions.visibilityOf(importCampaignCSVBtn)).sendKeys(path);
////		importCampaignCSVBtn.click();
//	}

	public void importCSVFile(String fileName) throws InterruptedException {
		String workingDir = System.getProperty("user.dir");
		String filepath = workingDir + "\\powerDialer\\" + fileName;
		System.out.println("filepath to be uploaded :: " + filepath);
//			importCampaignCSVBtn.click();
		fileUpload.sendKeys(filepath);
		Common.pause(10);
	}
	
	public void importInvalidFile(String fileName) throws InterruptedException {
		String workingDir = System.getProperty("user.dir");
		String filepath = workingDir + "\\Data\\" + fileName;
		System.out.println("filepath to be uploaded :: " + filepath);
//			importCampaignCSVBtn.click();
		fileUpload.sendKeys(filepath);
		Common.pause(10);
	}

	public void enterMinumAvailableUserToRunCampaign(String userCount) {
		wait.until(ExpectedConditions.visibilityOf(mininumAvailableUserToRunCampaign)).clear();
		wait.until(ExpectedConditions.visibilityOf(mininumAvailableUserToRunCampaign)).sendKeys(userCount);
	}

	public String campaignDescription(String campaignDescription) {
		wait.until(ExpectedConditions.visibilityOf(this.campaignDiscriptionTextarea)).click();
		wait.until(ExpectedConditions.visibilityOf(this.campaignDiscriptionTextarea)).sendKeys(Keys.CONTROL,"a",Keys.BACK_SPACE);
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(this.campaignDiscriptionTextarea)).sendKeys(campaignDescription);
		return campaignDescription;
	}

	public boolean validateMinimumUserLoggedInMessage() {

		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
					"(//span[contains(.,'At least 3 users need to be logged into dialer to run this campaign.')])[2]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton)).click();
	}

	public String validatepowerDilertitle() {
		return wait.until(ExpectedConditions.visibilityOf(powerDialertiltle)).getText();
	}
	
	public String validatepowerDUploadCampError() {
		return wait.until(ExpectedConditions.visibilityOf(powerDialerError)).getText();
	}
	public String validatepowerDUploadCampValidationMsg() {
		return wait.until(ExpectedConditions.visibilityOf(powerDialerValidation)).getText();
	}
	
		public void validateserchfieldOnPowerDialerpage() {
		 wait.until(ExpectedConditions.visibilityOf(powerDialersearch));
		
	}
	public String getDeleteCampaignComfirmationMessage() {
		return wait.until(ExpectedConditions.visibilityOf(deleteCampaignMsg)).getText();
	}
	public void validatedateofcreation1(String date) {
		driver.findElement(By.xpath(""));
			
	}
	
	public String validateCompletedCallsinWeb(int i,int t) {
		
		String totalcallsinsheet = wait2.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//table//tbody/tr[1]/td[4]/div//span[contains(.,'"+i+" out of "+t+"')]")))).getText();
		System.out.println(totalcallsinsheet);
		return totalcallsinsheet;
	 }
	
	public boolean validatedateofcreation(String date) {
		
		 String creationdate = driver.findElement(By.xpath("//td[@class='crdatenm']")).getText();
		
		 if(creationdate.contains(date)) {
			 return true;
		 }else {
			 return false;
		 }
	 }
	public String validatedateNoticemessage() {
		
		return wait.until(ExpectedConditions.visibilityOf(validationNoticeMessage)).getText();
	 }
	
	public String errormessageforcampaignname() {
		return wait.until(ExpectedConditions.visibilityOf(errormessage)).getText();
	}
	

	public void clickOnYesButton() {
		wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
		Common.pause(2);
	}

	public void clickOnNoButton() {
		wait.until(ExpectedConditions.visibilityOf(noButton)).click();
	}

	public void deleteAllPowerDialerEntries() {
		boolean isElementDisplayed = true;
		try {
			wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("(//i[contains(.,'delete_forever')])[1]"))));
			isElementDisplayed = true;
		} catch (Exception e) {
			isElementDisplayed = false;
		}

		if (isElementDisplayed == true) {
			List<WebElement> elementList = driver.findElements(By.xpath("//i[contains(.,'delete_forever')]"));
			for (int i = 0; i <= elementList.size(); i++) {

				List<WebElement> newElementList = driver.findElements(By.xpath("//i[contains(.,'delete_forever')]"));
				if (newElementList.size() > 0) {
//					newElementList.get(0).click();
					Common.pause(1);
					deleteCampaign.click();
					assertEquals(getDeleteCampaignComfirmationMessage(),
							"Are you sure you want to delete this campaign ?");
					clickOnYesButton();
//					assertEquals(validationMessage(), "Contact has been deleted Successfully");
					Common.pause(3);
				}
			}
			Common.pause(1);
		}
	}
	
	//
	
	public void clickOndownloadcapaign() {
		wait.until(ExpectedConditions.visibilityOf(downloadcapaign)).click();
		Common.pause(3);
		clickOnYesButton();
	}
	public void validatehowtousepowerdialer() {
		wait.until(ExpectedConditions.visibilityOf(howtouserpDialer)).click();	
	}
	public void Downloadtempcampaign() {
		wait.until(ExpectedConditions.visibilityOf(downloadtempcampaign)).click();	
	}
	
	public String powerDialerusepage() {
		return wait.until(ExpectedConditions.visibilityOf(powerDilerpage)).getText();
	}
	
	public boolean ValidateAssignee(String UserOrTeam ) {
		Common.pause(3);
		 wait.until(ExpectedConditions.elementToBeClickable(campaignsAssignee)).click();
		 Actions a = new Actions(driver);
		 a.moveToElement(campaignsAssignee).build().perform();
		 String Assignee0 = wait.until(ExpectedConditions
					.visibilityOf(driver.findElement(By.xpath("//ul[@class='teamMemberPopover']")))).getText();
		 System.out.println(Assignee0);
		 if(Assignee0.contains(UserOrTeam)) {
			 return true;
		 }else {
			 return false;
		 }
	}
	

	public String validateCampaignsName() {
		return wait.until(ExpectedConditions.visibilityOf(campaignsTitle)).getText();
	}
	public String validateCampaignsAssignerName() {
		return wait.until(ExpectedConditions.visibilityOf(campaignsAssignor)).getText();
	}
	

	public void clickOnEditButton() {
		wait.until(ExpectedConditions.visibilityOf(editButton)).click();
	}
	
	
	
	public boolean validateEditButton() {
		Common.pause(3);
		 int size = driver.findElements(By.xpath("(//i[contains(.,'edit')])[1]")).size();
		
		if(size>0) {
			return true;
		}else {
			return false;
		}
	}

	public void campaignActivation(String value) {

		wait.until(ExpectedConditions.visibilityOf(campaignActivationToggle));
		if (value.contentEquals("on")) {

			if (campaignActivationToggle.getAttribute("aria-checked").equals("true")) {

			} else {
				campaignActivationToggle.click();
			}
		} else {
			if (campaignActivationToggle.getAttribute("aria-checked").equals("true")) {
				campaignActivationToggle.click();
			} else {

			}
		}

	}
	
	public boolean campaignActivationStatus() {

		wait.until(ExpectedConditions.visibilityOf(campaignActivationToggle));

			if (campaignActivationToggle.getAttribute("aria-checked").equals("true")) {
				return true;
			}else {
				return false;
			}
	}
	public void secondcampaignActivation(String value) {

		wait.until(ExpectedConditions.visibilityOf(secondcampaignActivationToggle));
		if (value.contentEquals("on")) {

			if (secondcampaignActivationToggle.getAttribute("aria-checked").equals("true")) {

			} else {
				secondcampaignActivationToggle.click();
			}
		} else {
			if (secondcampaignActivationToggle.getAttribute("aria-checked").equals("true")) {
				secondcampaignActivationToggle.click();
			} else {

			}
		}

	}

	public void selectSpecificUserOrTeam(String userOrTeamName, String YesOrNo) {

		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'chflex flexcolomch')]"))));

		String attributeName = driver.findElement(By.xpath("//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'"
				+ userOrTeamName + "')]/parent::div/parent::div/parent::div")).getAttribute("class");

		if (YesOrNo.contains("Yes")) {
			if (attributeName.contains("allocateduserboxSelected")) {

			} else {
				driver.findElement(By.xpath(
						"//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'" + userOrTeamName + "')]/parent::div"))
						.click();
				wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
				Common.pause(2);
			}
		} else {
			if (attributeName.contains("allocateduserboxSelected")) {
				driver.findElement(By.xpath(
						"//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'" + userOrTeamName + "')]/parent::div"))
						.click();
				wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
				Common.pause(2);
			} else {

			}
		}

	}
	
	public void makeDefaultNumber(String number) {

		
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'"+number+"')]"))));
		
		
		List<WebElement> ele = driver.findElements(By.xpath("//span[contains(.,'"+number+"')]/parent::div/parent::div/parent::div/parent::div//span[contains(@class,'makeDefaultUser')]"));
		
		if(ele.size()>0) {
			driver.findElement(By.xpath("//span[contains(.,'"+number+"')]/parent::div/parent::div/parent::div/parent::div//span[contains(@class,'makeDefaultUser')]")).click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[@type='button'][contains(.,'Yes')]")))).click();
			Common.pause(3);
		}
			
	}
	
	

	public void selectAllUsersAndTeamAllocation(String YesOrNo) {

		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("(//div[@class='chflex flexcolomch fwidth mgleft15'])"))));

		int size = driver.findElements(By.xpath("(//div[@class='chflex flexcolomch fwidth mgleft15'])")).size();

		if (YesOrNo.contains("Yes")) {
			for (int i = 1; i <= size; i++) {

				String attributeName = driver.findElement(By.xpath(
						"(//div[@class='chflex flexcolomch fwidth mgleft15']/parent::div/parent::div)[" + i + "]"))
						.getAttribute("class");

				if (attributeName.contains("allocateduserboxSelected")) {

				} else {
					driver.findElement(By.xpath("(//div[@class='chflex flexcolomch fwidth mgleft15'])[" + i + "]"))
							.click();
					wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
					Common.pause(2);
				}

			}
		} else {
			for (int i = 1; i <= size; i++) {

				String attributeName = driver.findElement(By.xpath(
						"(//div[@class='chflex flexcolomch fwidth mgleft15']/parent::div/parent::div)[" + i + "]"))
						.getAttribute("class");

				if (attributeName.contains("allocateduserboxSelected")) {
					driver.findElement(By.xpath("(//div[@class='chflex flexcolomch fwidth mgleft15'])[" + i + "]"))
							.click();
					wait.until(ExpectedConditions.visibilityOf(yesButon)).click();
					Common.pause(2);
				} else {

				}

			}
		}

	}
	
	public  String validateinfoicononaddcamoaignpage() {
		Actions a = new Actions(driver);
	    a.moveToElement(driver.findElement(By.xpath("//span[text()='info']"))).build().perform();
	    
	    return driver.findElement(By.xpath("//div[@role='tooltip']")).getText();
		 
		}
	
	public  void validatecampaigndiscription(String description) {
		Actions a = new Actions(driver);
	    a.moveToElement(driver.findElement(By.xpath("//table//tbody/tr[1]/td[6]/span/i"))).build().perform();
	    
	   driver.findElement(By.xpath("//p[contains(.,'"+description+"')]")); 
		}
	
	public  boolean campaigndiscription() {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//table//tbody/tr[1]/td[6]/span/i"))));
	      return true; 
		} catch (Exception e) {
		 return false;
		}
		}
	
	public String validationErrorMessage() {
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(validationErrorMessage));
		return validationErrorMessage.getText();
	}
	
	
	public boolean validateMinimum2UserLoggedInMessage() {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
					"(//span[contains(.,'At least 2 users need to be logged into dialer to run this campaign.')])[2]"))));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
