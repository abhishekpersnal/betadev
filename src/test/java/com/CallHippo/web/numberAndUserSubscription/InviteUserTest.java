package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.credit.CreditWebSettingPage;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.authentication.SignupTest;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;

public class InviteUserTest {

	WebDriver driverWebApp1;
	Common signUpExcel;
	PropertiesFile url;
	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;
	SignupPage registrationPage;
	LoginPage loginpage;
	AddNumberPage addNumberPage;
	XLSReader planPrice;
	WebDriver driverMail;
	SignupPage dashboard;
	LiveCallPage Web2liveCallPage;
	RestAPI planPriceAPI;
	RestAPI numberPriceAPI;

	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	String gmailUser = data.getValue("gmailUser");
	String planPriceBronzeMonthly,planPriceSilverMonthly,planPricePlatinumMonthly;
	String planPriceBronzeAnnually,planPriceSilverAnnually,planPricePlatinumAnnually;
	private String email;

	public InviteUserTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		signUpExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		planPriceAPI= new RestAPI();
		numberPriceAPI= new RestAPI();
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			// driver.get(url);S
			
	            planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
				
				planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
			
				planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
				
				planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
				
				planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
			
				planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
				
				//--------------------Number Price ---------------------------------------
				
				driverMail = TestBase.init3();
				dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			
				try {
					dashboard.deleteAllMail();
				} catch (Exception e) {
					dashboard.deleteAllMail();
				}
			
		} catch (Exception e) {
			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		
		driverMail.quit();
		
	}

	@BeforeMethod
	public void initialization() throws IOException {
		driverWebApp1 = TestBase.init2();
		webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
		registrationPage = PageFactory.initElements(driverWebApp1, SignupPage.class);
		loginpage = PageFactory.initElements(driverWebApp1, LoginPage.class);
		inviteUserPageObj = PageFactory.initElements(driverWebApp1, InviteUserPage.class);
		addNumberPage = PageFactory.initElements(driverWebApp1, AddNumberPage.class);
		Web2liveCallPage = PageFactory.initElements(driverWebApp1, LiveCallPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		 driverWebApp1.quit();
	}

	private String signUpUser() throws Exception {
		driverWebApp1.get(url.livesignUp());
		Common.pause(1);
		
		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println(email);

		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signUpExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signUpExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signUpExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signUpExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signUpExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");

		return email;
	}

	private void LoginGmailAndConfirmYourMail() throws Exception {

		registrationPage.ValidateVerifyButtonOnGmail();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();
	}

	private void DonotLoginGmailAndConfirmYourMail() throws Exception {

		registrationPage.DonotLoginGmailAndConfirmMail();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();
	}
	
	private String createNewUserWithoutNumberPurchase() throws Exception {
		Common.pause(1);

		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();
		
		registrationPage.closeAddnumberPopup();

		return signUpUserEmailId;
	}
	
	private void add_plan_details(String emailId) throws Exception {
		Common.pause(1);
		
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

	}

	private String createNewUserWithOneNumber() throws Exception {
		Common.pause(1);

		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();

		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, "1"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
//		addNumberPage.clickOnSaveNumberButton();
		driverWebApp1.navigate().to(url.numbersPage());
		Common.pause(2);
		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		registrationPage.closeAddnumberPopup();

		return signUpUserEmailId;
	}

	private String createNewUserWithOneNumberPlatinumPlan() throws Exception {
		Common.pause(1);

		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();

		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		System.out.println("numberBeforePurchased" + numberBeforePurchased);
		addNumberPage.selectPlatinumPlan();
		addNumberPage.clickOnBuyNowButton();

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		driverWebApp1.navigate().to(url.numbersPage());
		Common.pause(2);

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		registrationPage.closeAddnumberPopup();

		return signUpUserEmailId;
	}

	private String createNewUserWith2Numbers() throws Exception {
		Common.pause(1);

		String signUpUserEmailId = signUpUser();
		System.out.println("---email2----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();

		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeMonthly,  data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginWeb(webAppMainUserLogin, signUpUserEmailId, data.getValue("masterPassword"));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnAddNumberInNumbersPopup();

		registrationPage.userLogout();

		loginWeb(webAppMainUserLogin, signUpUserEmailId, data.getValue("masterPassword"));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		return signUpUserEmailId;
	}

	private String createNewUserWithOneNumberDonotLoginGmail() throws Exception {
		Common.pause(1);

		String email2 = signUpUser();
		System.out.println("---email2----" + email2);
		DonotLoginGmailAndConfirmYourMail();

		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, "1"));

		// assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		//addNumberPage.clickOnSaveNumberButton();
		driverWebApp1.navigate().to(url.numbersPage());
		Common.pause(2);

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginWeb(webAppMainUserLogin, email2, signUpExcel.getdata(0, 27, 2));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		registrationPage.closeAddnumberPopup();

		return email2;
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		Common.pause(2);
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void check_Validation_Message_For_InvalidEmail_with_number_purhcase() throws Exception {
		createNewUserWithOneNumber();
		/*
		 * driverWebApp1.navigate().to(url.signIn()); Common.pause(2);
		 * loginWeb(webAppMainUserLogin,
		 * "jigneshshah+09_10_2020_14_15_08@callhippo.com",
		 * data.getValue("masterPassword")); Common.pause(2);
		 * registrationPage.closeAddnumberPopup();
		 */
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.inviteUserButtonIsClickable(), true);
		inviteUserPageObj.clickOnInviteUserButton();
		assertEquals(inviteUserPageObj.verifyFieldsOnInviteUserPage(), true);
		Common.pause(2);
		inviteUserPageObj.enterInviteUserEmail(signUpExcel.getdata(0, 9, 5));
		inviteUserPageObj.clickSaveButton();
		String actualResult = inviteUserPageObj.validateErrorMessage();
		String expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);
		assertEquals(inviteUserPageObj.verifyInviteUserButtonIsDisable(), true);
		Common.pause(2);
		logout(webAppMainUserLogin);
	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void validate_inviteUser_Fields_without_number_purchase() throws Exception {

		String actualResult;
		String expectedResult;
	
		signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(2);
		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		assertEquals(addNumberPage.validatePlanPopupIsDisplay(),true);
		Common.pause(2);

		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, "1");
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeMonthly,  data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		Common.pause(5);
		
		inviteUserPageObj.enterInviteUserEmail("");
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(2);
		inviteUserPageObj.enterInviteUserEmail("");
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		Common.pause(1);
		inviteUserPageObj.clickOnYesBtnForInviteUser();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter at least one invite email";
		assertEquals(actualResult, expectedResult);

		Common.pause(2);
		inviteUserPageObj.enterInviteUserEmail(signUpExcel.getdata(0, 9, 5));
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(2);
		inviteUserPageObj.enterInviteUserEmail(signUpExcel.getdata(0, 9, 5));
		// inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		assertEquals(inviteUserPageObj.validateInviteUserMessageForInvalidEmail(signUpExcel.getdata(0, 9, 5)), true);
		
		
		// ----- verify_cancel_button_functionality() -------
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		
		inviteUserPageObj.enterInviteUserEmail(signUpExcel.getdata(0, 9, 5));
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickCancelButton();
		Common.pause(2);
		inviteUserPageObj.clickOnInviteUserButton();
		assertEquals(inviteUserPageObj.verifyFieldsOnInviteUserPage(), true);
		
		//------ verify_emailAddressValidation() ------
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();		
		
		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		inviteUserPageObj.enterInviteUserEmail("");
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(3);
		inviteUserPageObj.enterInviteUserEmail("test");
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(3);
		inviteUserPageObj.enterInviteUserEmail("test@");
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(3);
		inviteUserPageObj.enterInviteUserEmail("test@gmail");
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(3);
		inviteUserPageObj.enterInviteUserEmail("test@gmail.");
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Please enter the valid email id";
		assertEquals(actualResult, expectedResult);

		Common.pause(3);
		inviteUserPageObj.enterInviteUserEmail("test@gmail.com");
		inviteUserPageObj.clickSaveButton();

		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail2("test@gmail.com");
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		actualResult = inviteUserPageObj.validateErrorMessage();
		expectedResult = "Email already exist.";
		assertEquals(actualResult, expectedResult);
		logout(webAppMainUserLogin);
		
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_Expired_invite_user_Link() throws Exception {

		createNewUserWithOneNumber();
//		driverWebApp1.navigate().to(url.signIn());
//		loginWeb(webAppMainUserLogin, "automation+29_07_2021_05_57_26@callhippo.com", "Test@123");
		Common.pause(2);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";

		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		assertEquals(inviteUserPageObj.addUser_popup_while_only_number_checkbox_selected(), true);
		inviteUserPageObj.clickYesBtnToConfirmInviteUser();
		
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
//		registrationPage.gloginGmail();
		Common.pause(2);
		registrationPage.openInvitationMail();
		registrationPage.gClickOnRecentAcceptInvitationlink();
		registrationPage.switchToWindowBasedOnTitle("Login | Callhippo.com");
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();

		logout(webAppMainUserLogin);

		Common.pause(2);
		// registrationPage.gloginGmail();
		registrationPage.openInvitationMailForSecondUser();
		registrationPage.gClickOnRecentAcceptInvitationlink();
		registrationPage.switchToWindowBasedOnTitle("Login | Callhippo.com");

		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		// registrationPage.verifyWelcomeToCallHippoPopup();

		String actualExpiredValidationMsg = inviteUserPageObj.validateErrorMessage();
		String expectedExpiredValidationMsg = "User is already verified, Please login.";
		assertEquals(actualExpiredValidationMsg, expectedExpiredValidationMsg);

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_invite_two_Users_and_accept_ivitation() throws Exception {

//		webAppMainUserLogin.deleteInvitedUserMail();

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumber();

		System.out.println("---seat count verify before invite user");
		driverWebApp1.get(url.planPage());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		assertEquals(inviteUserPageObj.verifySeatCount(), data.getValue("seatcount"));

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String date = signUpExcel.date();
		Common.pause(1);
		String date1 = signUpExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		String email2 = gmailUser + "+" + date1 + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();

		inviteUserPageObj.clickOnYes();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		Common.pause(30);
		System.out.println("---seat count verify After invite user");
		driverWebApp1.navigate().refresh();
		driverWebApp1.get(url.planPage());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//		assertEquals(inviteUserPageObj.verifySeatCount(), "3");

		logout(webAppMainUserLogin);
		Common.pause(120);

		String invitedUserName1 = "Invited User1";
		String invitedUserName2 = "Invited User2";

		registrationPage.validateSecondLastInvitationButton();
		inviteUserPageObj.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(30);

		registrationPage.OpenInvitationMailForSecondUser();
		inviteUserPageObj.enterSubUserFullName(invitedUserName2);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);

		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		Common.pause(2);

		// verify both invited user are active
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email1), true);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email2), true);

		inviteUserPageObj.clickOnSettingIconOf(email1);
		Common.pause(5);

		// validate invited user in settings page
		// assertEquals(inviteUserPageObj.validateInvitedUserInSettingsPage(invitedUserName1),
		// true);
		assertEquals(inviteUserPageObj.validateInvitedUserInSettingsPage(email1), true);

		driverWebApp1.navigate().back();
		Common.pause(2);
		inviteUserPageObj.clickOnSettingIconOf(email2);
		// assertEquals(inviteUserPageObj.validateInvitedUserInSettingsPage(invitedUserName2),
		// true);
		assertEquals(inviteUserPageObj.validateInvitedUserInSettingsPage(email2), true);
	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void check_validation_when_invite_morethan_10_users() throws Exception {

		createNewUserWithOneNumber();

		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.clickOnInviteUserButton();

		inviteUserPageObj.enterInviteUserEmail("test+101@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail("test+102@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail("test+103@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail("test+104@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail("test+105@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail("test+106@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.scrollToCancelButtonVisible();
		inviteUserPageObj.enterInviteUserEmail("test+107@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.scrollToCancelButtonVisible();
		inviteUserPageObj.enterInviteUserEmail("test+108@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.scrollToCancelButtonVisible();
		inviteUserPageObj.enterInviteUserEmail("test+109@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.scrollToCancelButtonVisible();
		inviteUserPageObj.enterInviteUserEmail("test+110@gmail.com");
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		String actualValidationMsg = inviteUserPageObj.validateWarningMessage();
		String expectedValidationMsg = "You can add only 10 users at once.";
		assertEquals(actualValidationMsg, expectedValidationMsg);
	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void verify_subUser_invited_successfully() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumber();
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void delete_invited_subUser() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumber();

		webAppMainUserLogin.userspage();

		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);

		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void reInvite_deleted_subUser() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithoutNumberPurchase();
		Common.pause(2);
		webAppMainUserLogin.userspage();

		inviteUserPageObj.clickOnInviteUserButton();

		add_plan_details(inviteMainUserEmailWithOneNumber);
		
		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
//		webAppMainUserLogin.deleteInvitedUserMail();
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		logout(webAppMainUserLogin);
		Common.pause(2);

		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();

		acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(10);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();
	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void invite_deleted_subUser_with_different_admin_account() throws Exception {

		String invite1stMainUserEmailWithOneNumber = createNewUserWithOneNumber();
		System.out.println("---invite1stMainUserEmailWithOneNumber---" + invite1stMainUserEmailWithOneNumber);
		Common.pause(2);

		webAppMainUserLogin.userspage();

		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, invite1stMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();

		// *******inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);

		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		logout(webAppMainUserLogin);

		String invite2ndMainUserEmailWithOneNumber = createNewUserWithOneNumberDonotLoginGmail();
		System.out.println("---invite2ndMainUserEmailWithOneNumber---" + invite2ndMainUserEmailWithOneNumber);

		Common.pause(2);
		
		driverWebApp1.navigate().to(url.signIn());
		loginWeb(webAppMainUserLogin, email, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		webAppMainUserLogin.userspage();

		inviteUserPageObj.clickOnInviteUserButton();

		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
//		inviteUserPageObj.clickOnYes();

		acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, "ayadip+03_08_2021_07_11_00@callhippo.com", signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void create_admin_account_using_deleted_subUser() throws Exception {

		String invite1stMainUserEmailWithOneNumber = createNewUserWithOneNumber();
		System.out.println("---invite1stMainUserEmailWithOneNumber---" + invite1stMainUserEmailWithOneNumber);
		Common.pause(2);

		webAppMainUserLogin.userspage();

		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, invite1stMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);

		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		logout(webAppMainUserLogin);
		Common.pause(2);
		// signup with deleted user
		driverWebApp1.get(url.signUp());
		Common.pause(1);

		registrationPage.enterFullname(signUpExcel.getdata(0, 1, 2));
		registrationPage.enterCompanyName(signUpExcel.getdata(0, 2, 2));
		registrationPage.enterMobile(signUpExcel.getdata(0, 3, 2));
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("test@123");
		registrationPage.clickOnSignupButton();
		boolean actualResult = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);

		DonotLoginGmailAndConfirmYourMail();
	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void verify_validation_message_for_reInvite_invited_user() throws Exception {

		String invite1stMainUserEmailWithOneNumber = createNewUserWithOneNumber();
		System.out.println("---invite1stMainUserEmailWithOneNumber---" + invite1stMainUserEmailWithOneNumber);
		Common.pause(2);

		System.out.println("---seat count verify before invite user");
		driverWebApp1.get(url.planPage());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		assertEquals(inviteUserPageObj.verifySeatCount(), data.getValue("seatcount"));

		webAppMainUserLogin.userspage();

		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		Common.pause(2);
		inviteUserPageObj.clickOnInviteUserButton();
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();

		inviteUserPageObj.clickOnYes();

		assertEquals(inviteUserPageObj.valditaionMessageforReinvitedUser(email), true);

//		Common.pause(30);
//		System.out.println("---seat count verify After invite user");
//		driverWebApp1.navigate().refresh();
//		driverWebApp1.get(url.planPage());
//		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//		assertEquals(inviteUserPageObj.verifySeatCount(), "2");
	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void verify_message_for_resent_invitation() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumber();
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		Common.pause(2);
		inviteUserPageObj.clickEmailIcon(email);
		assertEquals(inviteUserPageObj.valditaionMessageToReSentInvitationMail(email), true);
	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void verify_userCount_after_invite_two_Users() throws Exception {

		webAppMainUserLogin.deleteInvitedUserMail();

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumber();

		System.out.println("---seat count verify before invite user");
		driverWebApp1.get(url.planPage());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		assertEquals(inviteUserPageObj.verifySeatCount(), data.getValue("seatcount"));

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String date = signUpExcel.date();
		Common.pause(1);
		String date1 = signUpExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		String email2 = gmailUser + "+" + date1 + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();

		inviteUserPageObj.clickOnYes();

		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		System.out.println("---seat count verify after invite user");
		Common.pause(30);
		driverWebApp1.navigate().refresh();
		driverWebApp1.get(url.planPage());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//		assertEquals(inviteUserPageObj.verifySeatCount(), "3");
		assertTrue(inviteUserPageObj.validateUserCount("3"));

//		Common.pause(20);
//		addNumberPage.openAccountDetailsPopup();
//		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
//		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 3"));

	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void Add_delete_user_verify_in_live_call_filter() throws Exception {

		webAppMainUserLogin.deleteInvitedUserMail();

		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();

		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12"));

		addNumberPage.selectPlatinumPlan();
		addNumberPage.clickOnCheckoutButton();

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		//addNumberPage.clickOnSaveNumberButton();
		Common.pause(3);
		driverWebApp1.navigate().to(url.numbersPage());
		Common.pause(2);

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();
		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		registrationPage.closeAddnumberPopup();

		webAppMainUserLogin.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		String mainuser = "Automation Test";
		assertEquals(webAppMainUserLogin.ValidateFilterIconinWeblivecalls(), true);
		String userOrteam = webAppMainUserLogin.enterUserOrTeaminWebFiltersearch(mainuser);
		webAppMainUserLogin.selectUserOrTeamInWeblivecallFilter(userOrteam);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String date = signUpExcel.date();
		Common.pause(1);
		String date1 = signUpExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		String email2 = gmailUser + "+" + date1 + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		webAppMainUserLogin.clickonyesbutton();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();

		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(120);

		String invitedUserName1 = "Invited User1";
		String invitedUserName2 = "Invited User2";

		registrationPage.validateSecondLastInvitationButton();
		inviteUserPageObj.enterSubUserFullName(invitedUserName2);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(30);

		registrationPage.OpenInvitationMailForSecondUser();
		inviteUserPageObj.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);

		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(2);

		webAppMainUserLogin.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webAppMainUserLogin.ValidateFilterIconinWeblivecalls(), true);
		webAppMainUserLogin.clickOnLogOut();
		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
		webAppMainUserLogin.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webAppMainUserLogin.ValidateFilterIconinWeblivecalls(), true);
		webAppMainUserLogin.selectUserOrTeamInWeblivecallFilter(invitedUserName1);
		webAppMainUserLogin.selectUserOrTeamInWeblivecallFilter(invitedUserName2);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);

		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email2);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		webAppMainUserLogin.clickOnLogOut();
		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
		webAppMainUserLogin.clickLeftMenuDashboard();
		Common.pause(2);
		Web2liveCallPage.clickOnLiveCallTab();
		Common.pause(2);
		assertEquals(webAppMainUserLogin.ValidateFilterIconinWeblivecalls(), true);
		assertEquals(Web2liveCallPage.validateThereIsNoLiveCallsinweb(), true);
		assertEquals(webAppMainUserLogin.ValidateUserOrTeamInWeblivecallFilter(invitedUserName1), true);
		assertEquals(webAppMainUserLogin.ValidateUserOrTeamInWeblivecallFilter(invitedUserName2), false);
	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void Delete_subUser_Added_In_Team() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumberPlatinumPlan();

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYesBtnForInviteUser();

		logout(webAppMainUserLogin);
		Common.pause(120);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.teamspage();
		inviteUserPageObj.clickOnCreateButton();
		inviteUserPageObj.enterTeamName("testteam");
		inviteUserPageObj.clickOnSimultaneously();
		inviteUserPageObj.clickOnSecondUserTeam();
		inviteUserPageObj.clickOnSubmitButton();
		Common.pause(4);
		webAppMainUserLogin.teamspage();
		assertEquals(inviteUserPageObj.verifyMembersCountInTeam("testteam"), "1");
		Common.pause(4);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserYesButton();
		Common.pause(4);
		webAppMainUserLogin.teamspage();
		assertEquals(inviteUserPageObj.verifyMembersCountInTeam("testteam"), "0");
		Common.pause(4);
		inviteUserPageObj.DeleteSpecificTeam("testteam");

//		webAppMainUserLogin.numberspage();
//		inviteUserPageObj.clickOnNUmber();
//		Thread.sleep(9000);
//		inviteUserPageObj.numberIVR("on");
	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void Delete_user_verify_popup() throws Exception {

		webAppMainUserLogin.deleteInvitedUserMail();

		String signUpUserEmailId = signUpUser();
		System.out.println("---signUpUserEmailId----" + signUpUserEmailId);
		LoginGmailAndConfirmYourMail();

		// ------- Add 1st Number -------//
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,planPricePlatinumAnnually);

		addNumberPage.selectPlatinumPlan();
		addNumberPage.clickOnBuyNowButton();

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		// assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("annually"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		driverWebApp1.navigate().to(url.numbersPage());
		Common.pause(3);

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		registrationPage.closeAddnumberPopup();
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String date = signUpExcel.date();
		Common.pause(1);
		String date1 = signUpExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";

		String email2 = gmailUser + "+" + date1 + "@callhippo.com";

		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		webAppMainUserLogin.clickonyesbutton();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();

		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		logout(webAppMainUserLogin);
		Common.pause(120);

		String invitedUserName1 = "Invited User1";
		String invitedUserName2 = "Invited User2";

		registrationPage.validateSecondLastInvitationButton();
		inviteUserPageObj.enterSubUserFullName(invitedUserName2);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(30);

		registrationPage.OpenInvitationMailForSecondUser();
		inviteUserPageObj.enterSubUserFullName(invitedUserName1);
		inviteUserPageObj.enterSubUserNewPassword("Ttest@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);

		loginWeb(webAppMainUserLogin, signUpUserEmailId, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(2);

		webAppMainUserLogin.userspage();

		// inviteUserPageObj.scrollUptoNextPageButtonVisible();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email2);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);

		String actualResult1 = inviteUserPageObj.validateDeletUserPopupDescription();
		String expectedResult1 = "Any numbers assigned to this user will be de-allocated, and this process is irreversible.";
		assertEquals(actualResult1, expectedResult1);
		inviteUserPageObj.clickOnDeleteUserYesButton();
	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void Verify_Deleted_User_In_NumberSetting() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumberPlatinumPlan();

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYesBtnForInviteUser();

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		String SubUserName = "Invited User1";
		inviteUserPageObj.enterSubUserFullName(SubUserName);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserYesButton();
		Common.pause(4);
		webAppMainUserLogin.numberspage();

		inviteUserPageObj.clickOnNUmber();
		assertEquals(inviteUserPageObj.validateDeletedUser(SubUserName), true);
	}

	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void Verify_Deleted_User_In_Ivr() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumberPlatinumPlan();

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYesBtnForInviteUser();

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		String SubUserName = "Invited User1";
		inviteUserPageObj.enterSubUserFullName(SubUserName);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		
		driverWebApp1.navigate().to(url.signIn());
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);

		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Thread.sleep(9000);
		inviteUserPageObj.closeLowcreditNotification();
		Common.pause(1);
		inviteUserPageObj.numberIVR("on");
		// inviteUserPageObj.clickOnAddButtonIVR();
		inviteUserPageObj.enterValueInIvr("1");
		inviteUserPageObj.clickOnDropdownInIVR("User");
		Common.pause(5);
		inviteUserPageObj.clickOnDropdownNumUserTeam("Invited User1");
		inviteUserPageObj.SaveIvrOption();
		
		Common.pause(5);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserYesButton();
		Common.pause(4);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Common.pause(4);
		inviteUserPageObj.clickOnTabTeam();
		assertEquals(inviteUserPageObj.verifyTeamInvitedInIvrNumberIsDisplayed("testteam"), false);
	}

	@Test(priority = 24, retryAnalyzer = Retry.class)
	public void verify_Delete_User_which_is_Allocated_In_Ivr_number() throws Exception {

		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumberPlatinumPlan();

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYesBtnForInviteUser();

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		String SubUserName = "Invited User1";
		inviteUserPageObj.enterSubUserFullName(SubUserName);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.userspage();
		Common.pause(1);

		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Thread.sleep(9000);
		inviteUserPageObj.closeLowcreditNotification();
		Common.pause(1);
		inviteUserPageObj.numberIVR("on");
		// inviteUserPageObj.clickOnAddButtonIVR();
		inviteUserPageObj.enterValueInIvr("1");
		inviteUserPageObj.clickOnDropdownInIVR("User");
		Common.pause(5);
		inviteUserPageObj.clickOnDropdownNumUserTeam("Invited User1");
		inviteUserPageObj.SaveIvrOption();
		// start here
		Common.pause(1);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserNoButton();
		Common.pause(4);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Thread.sleep(9000);
		assertEquals(inviteUserPageObj.verifyUserInvitedInIvrNumberIsDisplayed(SubUserName), true);

		Common.pause(1);

		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
		Thread.sleep(9000);

		// verify allocated user
		Common.pause(4);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		assertEquals(inviteUserPageObj.verifyUserInvitedInIvrNumberIsDisplayed(SubUserName), false);

		assertEquals(inviteUserPageObj.validateDeletedUser(SubUserName), true);

	}

	@Test(priority = 25, retryAnalyzer = Retry.class)
	public void verify_Delete_User_which_is_Allocated_In_Ivr_team() throws Exception {
		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumberPlatinumPlan();

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYesBtnForInviteUser();

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		inviteUserPageObj.enterSubUserFullName("Invited User1");
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();
		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.teamspage();
		inviteUserPageObj.clickOnCreateButton();
		inviteUserPageObj.enterTeamName("testteam");
		inviteUserPageObj.clickOnSimultaneously();
		inviteUserPageObj.clickOnSecondUserTeam();
		inviteUserPageObj.clickOnSubmitButton();
		Common.pause(4);
		webAppMainUserLogin.teamspage();
		assertEquals(inviteUserPageObj.verifyMembersCountInTeam("testteam"), "1");
		Common.pause(4);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
		Common.pause(4);
		webAppMainUserLogin.teamspage();
		assertEquals(inviteUserPageObj.verifyMembersCountInTeam("testteam"), "0");
		Common.pause(4);
		inviteUserPageObj.DeleteSpecificTeam("testteam");

		String teamDeletedActualMsg = inviteUserPageObj.validationMessage();
		String teamDeletedExpectedMsg = "Team deleted successfully";
		assertEquals(teamDeletedActualMsg, teamDeletedExpectedMsg);
		Thread.sleep(9000);

		// verify allocated team
		Common.pause(4);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Common.pause(4);
		inviteUserPageObj.clickOnTabTeam();
		assertEquals(inviteUserPageObj.verifyTeamInvitedInIvrNumberIsDisplayed("testteam"), false);

	}

	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void verify_Delete_User_which_is_Allocated_In_Ivr_team_and_number() throws Exception {
		String inviteMainUserEmailWithOneNumber = createNewUserWithOneNumberPlatinumPlan();

		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signUpExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email);
		inviteUserPageObj.enterInviteUserEmail(email);
		inviteUserPageObj.selectNumberCheckbox();
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYesBtnForInviteUser();

		logout(webAppMainUserLogin);
		Common.pause(2);
		registrationPage.validateSecondLastInvitationButton1();
		String SubUserName = "Invited User1";
		inviteUserPageObj.enterSubUserFullName(SubUserName);
		inviteUserPageObj.enterSubUserNewPassword("Test@123");
		inviteUserPageObj.clickOnSubUserSubmitButton();
		registrationPage.verifyWelcomeToCallHippoPopup();
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginWeb(webAppMainUserLogin, inviteMainUserEmailWithOneNumber, signUpExcel.getdata(0, 27, 2));
		Common.pause(2);
		registrationPage.closeAddnumberPopup();

		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.userspage();
		Common.pause(1);

		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Thread.sleep(9000);
		inviteUserPageObj.closeLowcreditNotification();
		Common.pause(1);
		inviteUserPageObj.numberIVR("on");
		// inviteUserPageObj.clickOnAddButtonIVR();
		inviteUserPageObj.enterValueInIvr("1");
		inviteUserPageObj.clickOnDropdownInIVR("User");
		Common.pause(5);
		inviteUserPageObj.clickOnDropdownNumUserTeam("Invited User1");
		inviteUserPageObj.SaveIvrOption();

		Common.pause(1);
		webAppMainUserLogin.userspage();
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.teamspage();
		inviteUserPageObj.clickOnCreateButton();
		inviteUserPageObj.enterTeamName("testteam");
		inviteUserPageObj.clickOnSimultaneously();
		inviteUserPageObj.clickOnSecondUserTeam();
		inviteUserPageObj.clickOnSubmitButton();
		Common.pause(4);
		webAppMainUserLogin.teamspage();
		assertEquals(inviteUserPageObj.verifyMembersCountInTeam("testteam"), "1");

		// delete user
		// start here
		Common.pause(1);
		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserNoButton();
		Common.pause(4);
		assertEquals(inviteUserPageObj.verifyUserInvitedSuccessfully(email), true);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Thread.sleep(9000);
		assertEquals(inviteUserPageObj.verifyUserInvitedInIvrNumberIsDisplayed(SubUserName), true);

		Common.pause(1);

		webAppMainUserLogin.userspage();
		Common.pause(1);
		inviteUserPageObj.deleteInvitedSubUser(email);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		Common.pause(5);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
		Thread.sleep(9000);

		// verify allocated user
		Common.pause(4);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		assertEquals(inviteUserPageObj.verifyUserInvitedInIvrNumberIsDisplayed(SubUserName), false);

		assertEquals(inviteUserPageObj.validateDeletedUser(SubUserName), true);

//		// delete team
//		Common.pause(4);
//		webAppMainUserLogin.userspage();
//		Common.pause(1);
//		inviteUserPageObj.deleteInvitedSubUser(email);
//		Common.pause(1);
//		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
//		Common.pause(5);
//		inviteUserPageObj.clickOnDeleteUserYesButton();
//
//		String userDeletedActualMsg1 = inviteUserPageObj.validateInfoMessage();
//		String userDeletedExpectedMsg1 = "User deleted successfully";
//		assertEquals(userDeletedActualMsg1, userDeletedExpectedMsg1);
		Common.pause(4);
		webAppMainUserLogin.teamspage();
		assertEquals(inviteUserPageObj.verifyMembersCountInTeam("testteam"), "0");
		Common.pause(4);
		inviteUserPageObj.DeleteSpecificTeam("testteam");

		String teamDeletedActualMsg = inviteUserPageObj.validationMessage();
		String teamDeletedExpectedMsg = "Team deleted successfully";
		assertEquals(teamDeletedActualMsg, teamDeletedExpectedMsg);
		Thread.sleep(9000);

		// verify allocated team
		Common.pause(4);
		webAppMainUserLogin.numberspage();
		inviteUserPageObj.clickOnNUmber();
		Common.pause(4);
		inviteUserPageObj.clickOnTabTeam();
		assertEquals(inviteUserPageObj.verifyTeamInvitedInIvrNumberIsDisplayed("testteam"), false);

	}
}
