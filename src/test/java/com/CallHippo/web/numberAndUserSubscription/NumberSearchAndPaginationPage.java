package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class NumberSearchAndPaginationPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	WebDriver driver1;
	PropertiesFile credential;

	@FindBy(xpath = "//li[@title='Next Page'][@aria-disabled='true']")
	WebElement nextBtnPaginationDisable;

	@FindBy(xpath = "//input[@placeholder='Search by Name or Number.']")
	WebElement searchInput;

	@FindBy(xpath = "//p[contains(.,'No Data')]")
	WebElement noDataTxt;

	public NumberSearchAndPaginationPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		action = new Actions(this.driver);

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public boolean verifyNoDataTxtPresent() {
		Common.pause(1);
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//p[contains(.,'No Data')]"));
		if (ele.size() != 0) {
			elementIsAvailable = true;
		} else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}

	public boolean verifyNextBtnPaginationEnable() {
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Next Page'][@aria-disabled='false']"));
		if (ele.size() != 0) {
			elementIsAvailable = true;
		} else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}
	
	public boolean verifyNextBtnPaginationDisable() {
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Next Page'][@aria-disabled='true']"));
		if (ele.size() != 0) {
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}
	
	public boolean verifyPreviousBtnPaginationEnable() {
		boolean elementIsEnable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Previous Page'][@aria-disabled='false']"));
		if (ele.size() != 0) {
			elementIsEnable = true;
		} else {
			elementIsEnable = false;
		}
		return elementIsEnable;
	}
	
	public boolean verifyPreviousBtnPaginationDisable() {
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Previous Page'][@aria-disabled='true']"));
		if (ele.size() != 0) {
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}

	public Integer getTotalRows() {
		boolean elementIsAvailable;
		List<WebElement> numRows = driver.findElements(By
				.xpath("//div[@class[contains(.,'numbertable')]]//table//tr//td[2]//a[@href[contains(.,'numbers')]]"));
		int numRowsInt = numRows.size();
		System.out.println("--TotalRows:"+numRowsInt);
		return numRowsInt;
	}

	public String randomlyGetUserNumber(Integer randomdigit) {
		if (randomdigit == 0) {
			randomdigit = 1;
		}
		String getNumber = driver.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr["
				+ randomdigit + "]//td[2]//a[@href[contains(.,'numbers')]]")).getText();

		System.out.println("---getNumber--" + getNumber);
		return getNumber;
	}

	public String getStringFromNumber(String number, Integer startIndex, Integer endIndex) {

		System.out.println(number.substring(startIndex, endIndex));

		return number.substring(startIndex, endIndex);
	}

	public void verifyNumberStringInSearchResult(String numberSearchString) {
		boolean recordMatch;
		searchInput.clear();
		searchInput.sendKeys(numberSearchString);
//		for (int j = 0; j < numberSearchString.length(); j++) {
//			searchInput.sendKeys(Character.toString(numberSearchString.charAt(j)));
//			Common.pause(1);
//		}

		Common.pause(2);
		
		if(numberSearchString.matches("[0-9]+")) {
			wait.until(ExpectedConditions.visibilityOf(driver
					.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr[1]//td[2]//a[contains(text(),'" + numberSearchString + "')]"))));
		}
		
		Integer rowsAfterSearch = getTotalRows();
		int ext = 0;
		if (rowsAfterSearch != 0) {
			do {

				ext = ext + 1;

				if (ext == 1) {
					for (int i = 1; i <= rowsAfterSearch; i++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr[" + i
										+ "]//td[2]//a[@href[contains(.,'numbers')]]"))
								.getText();
						recordMatch = number.contains(numberSearchString);

						assertEquals(true, recordMatch, "Search string " + numberSearchString
								+ " is not matched with number-- " + number);

					}

				} else {

					driver.findElement(By.xpath("//li[@title='Next Page'][@aria-disabled='false']/a")).click();

					Common.pause(5);
					Integer rowsAfterSearchnew = getTotalRows();

					for (int j = 1; j <= rowsAfterSearchnew; j++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr[" + j
										+ "]//td[2]//a[@href[contains(.,'numbers')]]"))
								.getText();
						recordMatch = number.contains(numberSearchString);

						assertEquals(true, recordMatch, "Search string " + numberSearchString
								+ " is not matched with number-- " + number);

					}
				}
				if (ext == 10) {
					break;
				}

			} while (verifyNextBtnPaginationEnable() == true);

		}
	}

	public void clickOnDepartmentName() {

		WebElement numberSetting = driver
				.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr[1]//td[1]/a"));
		wait.until(ExpectedConditions.visibilityOf(numberSetting));
		numberSetting.click();
	}

	public void clickDepartmentNameEditIcon() {

		wait.until(ExpectedConditions.visibilityOf(driver
				.findElement(By.xpath("//div[@class='deparmentname_div2']//button[@class[contains(.,'editbtn')]]"))))
				.click();
	}

	public void insert_departmentName(String value) {
		Common.pause(1);
		WebElement departmentNameInputStr = driver.findElement(
				By.xpath("//div[@class[contains(.,'editdepartment')]]//input[@id[contains(.,'contactName')]]"));
		wait.until(ExpectedConditions.visibilityOf(departmentNameInputStr)).click();
		departmentNameInputStr.clear();
		departmentNameInputStr.sendKeys(value);
	}

	public void save_departmentName() {
		Common.pause(1);
		WebElement saveDepartmentName = driver
				.findElement(By.xpath("//div[@class[contains(.,'deparmentname')]]/div//i[contains(.,'save')]"));
		wait.until(ExpectedConditions.visibilityOf(saveDepartmentName)).click();
	}

//	public void validate_departmentName() {
//		Common.pause(2);
//		WebElement specialCharText = driver.findElement(By.xpath(
//				"//div[@class[contains(.,'editdepartment')]]/div/div[contains(.,'Special characters are not allowed')]"));
//		assertTrue(specialCharText.isDisplayed());
//	}

	public void validate_departmentName() {
		boolean elementIsAvailable;
		List<WebElement> specialCharText = driver.findElements(By.xpath(
				"//div[@class[contains(.,'editdepartment')]]/div/div[contains(.,'Special characters are not allowed')]"));
		if (specialCharText.size() != 0) {
			elementIsAvailable = true;
		} else {
			elementIsAvailable = false;
		}
		assertTrue(elementIsAvailable);
	}
	
	public void verifyDepartmentNameInSearchResult(String numberSearchString) {
		boolean recordMatch;
		searchInput.clear();
		searchInput.sendKeys(numberSearchString);
//		for (int j = 0; j < numberSearchString.length(); j++) {
//			searchInput.sendKeys(Character.toString(numberSearchString.charAt(j)));
//			Common.pause(1);
//		}

		Common.pause(2);

		Integer rowsAfterSearch = getTotalRows();

		int ext = 0;
		if (rowsAfterSearch != 0) {
			do {

				ext = ext + 1;

				if (ext == 1) {
					for (int i = 1; i <= rowsAfterSearch; i++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr[" + i
										+ "]//td[1]//a[@href[contains(.,'numbers')]]"))
								.getText();
						recordMatch = number.contains(numberSearchString);

						assertEquals(true, recordMatch, "Search string " + numberSearchString
								+ " is not matched with number-- " + number);

					}

				} else {

					driver.findElement(By.xpath("//li[@title='Next Page'][@aria-disabled='false']/a")).click();

					Common.pause(5);
					Integer rowsAfterSearchnew = getTotalRows();

					for (int j = 1; j <= rowsAfterSearchnew; j++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//table//tr[" + j
										+ "]//td[1]//a[@href[contains(.,'numbers')]]"))
								.getText();
						recordMatch = number.contains(numberSearchString);

						assertEquals(true, recordMatch, "Search string " + numberSearchString
								+ " is not matched with number-- " + number);

					}
				}
				if (ext == 10) {
					break;
				}

			} while (verifyNextBtnPaginationEnable() == true);

		}else {
			assertTrue(false);
		}
	}

	public Integer getPaginationSize() {
		List<WebElement> pagination = driver.findElements(By
				.xpath("//ul[@class[contains(.,'pagination')]]/li"));
		WebElement lastpagination = driver.findElement(By
				.xpath("//li[@title='Next Page'][@aria-disabled='false']/preceding-sibling::li[1]"));
		Integer paginationsize = Integer.parseInt(lastpagination.getAttribute("title"));
		//int paginationSize = pagination.size();
		
		return paginationsize;
	}
	
	public void verifyNextPageEnable() {
		
		Integer totalpageSize = getPaginationSize();
//		System.out.println("---paginationSize.size()--" + paginationSize);
//		Integer totalpageSize = paginationSize-2;
		System.out.println("---totalpageSize--" + totalpageSize);
		
		if(totalpageSize==1) {
			assertTrue(verifyNextBtnPaginationDisable());
		}
		if(totalpageSize>1) {
			assertTrue(verifyNextBtnPaginationEnable());
		}
	}
	
	public void verifyNextPageDisableAtlastPageOfPagination() {
		
		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);
		
		if(totalpageSize>1) {
			driver.findElement(By
					.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='"+totalpageSize+"']")).click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
		}
	}
	
	public void clickOnlastPageOfPagination() {
		
		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);
		
		if(totalpageSize>1) {
			driver.findElement(By
					.xpath("//li[@title='Next Page'][@aria-disabled='false']/preceding-sibling::li[1]")).click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
		}
	}
	
	public void verifyActivePageInPagination() {

		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);

		if (totalpageSize > 1) {
			driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + totalpageSize + "']"))
					.click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
			
			driver.findElement(
					By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + (totalpageSize - 1) + "']"))
					.click();
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationEnable());
		}
	}
	
	public void verifyPaginationAfterNumberDeleteFromLastPage() {

		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);

		if (totalpageSize > 1) {
			driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + totalpageSize + "']"))
					.click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
			
			driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
			Common.pause(2);
			driver.findElement(
					By.xpath("//div[@id[contains(.,Dialog)]][contains(.,'Deleting a Number')]/..//following-sibling::div//button[contains(.,'Yes')]"))
					.click();
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationDisable());
		}
	}
	
	public void deleteNumbers() {

		WebElement lastpagination = driver.findElement(By
				.xpath("//li[@title='Next Page'][@aria-disabled='true']/preceding-sibling::li[1]"));
		Integer totalpageSize = Integer.parseInt(lastpagination.getAttribute("title"));

		driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
		Common.pause(2);
		driver.findElement(By.xpath(
				"//div[@id[contains(.,Dialog)]][contains(.,'Deleting a Number')]/..//following-sibling::div//button[contains(.,'Yes')]"))
				.click();
		Common.pause(2);
	}
	
	public void verifyPaginationAfterNumberDeleteFrom2ndLastPage() {

		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);

		if (totalpageSize > 1) {
			driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + totalpageSize + "']"))
					.click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
			
			driver.findElement(
					By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + (totalpageSize - 1) + "']"))
					.click();
			
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationEnable());
			
			driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
			Common.pause(2);
			driver.findElement(
					By.xpath("//div[@id[contains(.,Dialog)]][contains(.,'Deleting a Number')]/..//following-sibling::div//button[contains(.,'Yes')]"))
					.click();
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationDisable());
		}
	}
	
}
