package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.expectThrows;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.support.ui.Select;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.XLSReader;
import com.codoid.products.exception.FilloException;

import groovyjarjarantlr4.v4.runtime.tree.xpath.XPath;
//import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class AddNumberPage {
	XLSReader numberPrice;
	static WebDriver driver; 
	WebDriverWait wait;
	Actions action;
	PropertiesFile credential;
	JavascriptExecutor js = (JavascriptExecutor) driver;
	
	SoftAssert softAssert = new SoftAssert();

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement validationMsg;

	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span")
	private WebElement messageInfo;

	@FindBy(xpath = "//button[@class='ngdialog-close']")
	private WebElement closeDialog;
	@FindBy(xpath = "//i[contains(.,'arrow_drop_down')]")
	private WebElement userDropdown;
	@FindBy(xpath = "//span[@class='isoDropdownLink'][contains(.,'Logout')]")
	private WebElement logout;

	@FindBy(xpath = "//button[@class='etaddnumber'][contains(.,'Add Number')]")
	private WebElement addNumber_popup_btn;
	@FindBy(xpath = "//h3[contains(.,'Welcome to CallHippo!!')]")
	private WebElement welcome_popup_msg;
	@FindBy(xpath = "//button[@class='endtourbtn'][contains(.,'End Tour')]")
	private WebElement endTour_btn;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Add Number')]")
	private WebElement addNumber;
	@FindBy(xpath = "//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'United States')]")
	private WebElement unitedStates;
	@FindBy(xpath = "(//div[contains(@class,'numbertable')]//a)[2]")
	private WebElement firstNumberOnNumberPage;
	@FindBy(xpath = "(//span[contains(@class,'numberlist_country srchspannumty')])[1]")
	private WebElement firstNumber;
	@FindBy(xpath = "//input[@value='monthly']/parent::span/parent::label")
	private WebElement monthlyPlan;
	@FindBy(xpath = "//input[@value='annually']/parent::span/parent::label")
	private WebElement annuallyPlan;
	@FindBy(xpath = "//div[@class='plandigit num-annu num_background'][contains(.,'bronze')]/following-sibling::div/button[@type='button'][contains(.,'Select Plan')]")
	private WebElement bronzePlan;
	@FindBy(xpath = "//div[contains(@class,'plan_title_silver')]/following-sibling::div/button[@type='button'][contains(.,'Select Plan')]")
	private WebElement silverPlan;
	@FindBy(xpath = "//div[@class='plandigit num-annu num_background'][contains(.,'platinum')]/following-sibling::div/button[@type='button'][contains(.,'Select Plan')]")
	private WebElement platinumPlan;

	@FindBy(xpath = "//div[@class='plandigit num-annu num_background'][contains(.,'bronze')]/following-sibling::div//span[@class='plan_price_position']")
	private WebElement priceBronzePlan;
	@FindBy(xpath = "//div[contains(@class,'plan_title_silver')]/following-sibling::div//span[@class='plan_price_position']")
	private WebElement priceSilverPlan;
	@FindBy(xpath = "//div[@class='plandigit num-annu num_background'][contains(.,'platinum')]/following-sibling::div//span[@class='plan_price_position']")
	private WebElement pricePlatinumPlan;

	@FindBy(xpath = "//button[@type='button'][contains(.,'Buy Now')]")
	private WebElement checkoutButton;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Buy Now')]")
	private WebElement BuyNowButton;
	@FindBy(xpath = "//span[contains(@class,'cb-item__middle')]")
	private WebElement ypPrice;
	@FindBy(xpath = "//h2[contains(@class,'sub-group--left')]")
	private WebElement ypPlanDetails;
	@FindBy(xpath = "//div[@class='cb-discount__action'][contains(.,'Apply coupon')]")
	private WebElement ypCouponcodeLink;
	@FindBy(xpath = "//button[contains(.,'Proceed To Checkout')]")
	private WebElement ypProceedToCheckout;

	@FindBy(xpath = "//button[contains(.,'subscribe')]")
	private WebElement coSubscribeButton;
	@FindBy(xpath = "//button[contains(.,'Not now, I am done')]")
	private WebElement notNowImDone;
	@FindBy(xpath = "//button[@class='bntsaveassignnum'][contains(.,'Add')]")
	private WebElement saveNumberButton;

	@FindBy(xpath = "//span[@class='usrname_topbar']")
	private WebElement profileDropdown;

	@FindBy(xpath = "//button[@type='button'][contains(.,'Account Details')]")
	private WebElement accountDetails;
	@FindBy(xpath = "//div[@class='cb-bar__title']")
	private WebElement msPlanDetails;
	@FindBy(xpath = "//span[@class='cb-bar__price']")
	private WebElement msPrice;
	
	@FindBy(xpath = "//div[@data-cb-id='subscription_details']")
	private WebElement cb_SubDetails;
	

	@FindBy(xpath = "//input[@id='first_name']")
	private WebElement cardFirstName;
	@FindBy(xpath = "//input[@id='last_name']")
	private WebElement cardLastName;
	@FindBy(xpath = "//input[@id='line1']")
	private WebElement cardAddressLine1;
	@FindBy(xpath = "//input[@id='city']")
	private WebElement cardCity;
	@FindBy(xpath = "//select[@id='country']")
	private WebElement cardCountry;
	@FindBy(xpath = "//span[@class='cb-button__text'][contains(.,'Next')]")
	private WebElement cardNextButton;
	@FindBy(xpath = "//input[@id='number']")
	private WebElement cardNumber;
	@FindBy(xpath = "//input[@id='exp_month']")
	private WebElement cardExpiryMonth;
	@FindBy(xpath = "//input[@id='exp_year']")
	private WebElement cardExpiryYear;
	@FindBy(xpath = "//input[@id='cvv']")
	private WebElement cardCVV;

	@FindBy(xpath = "//span[@class='sidebarCreditSpan']")
	private WebElement creditOnSideMenu;
	@FindBy(xpath = "//h5[contains(text(),'Free Incoming Call :')]//span")
	private WebElement freeInMinutes;
	@FindBy(xpath = "//p[@class='formpsize numtpwd'][contains(.,'Number Type')]/parent::div//div[@class='ant-select-selection-selected-value']")
	private WebElement numberTypeDropdown;
	//@FindBy(xpath = "//p[@class='formpsize numtpwd srchbypad'][contains(.,'Search By')]/parent::div//div[@class='ant-select-selection-selected-value']")
	@FindBy(xpath = "//p[@class='formpsize numtpwd searchwidth srchbypad'][contains(.,'Search By')]/parent::div//div[@class='ant-select-selection-selected-value']")
	private WebElement searchByDropdown;
	@FindBy(xpath = "//input[@placeholder='Enter a Number.']")
	private WebElement prefixOrNumberSearchbox;
	@FindBy(xpath = "//div[@class='ant-modal-content']//button[@type='button'][contains(.,'Next')]")
	private WebElement nextButtonOfDocumentRequired;

	@FindBy(xpath = "(//h2[contains(@class,'cb-item__name  cb-item__sub-group--left')])[1]")
	private WebElement pYourOrderPlanDetails;
	@FindBy(xpath = "(//h2[contains(@class,'cb-item__name  cb-item__sub-group--left')])[2]")
	private WebElement pYourOrderNumberDetails;
	@FindBy(xpath = "(//span[@class='cb-item__middle'])[1]")
	private WebElement pYourOrderPlanPrice;
	@FindBy(xpath = "(//span[@class='cb-item__middle'])[2]")
	private WebElement pYourOrderNumberPrice;
	@FindBy(xpath = "//div[@class='cb-item__total']")
	private WebElement pYourOrderTotal;

	@FindBy(xpath = "//span[@class='cb-bar__middle']")
	private WebElement msPlanDetailsOfPremiumNumber;
	@FindBy(xpath = "(//div[@class='cb-bar__title'])[2]")
	private WebElement msNumberDetailsOfPremiumNumber;
	@FindBy(xpath = "(//div[@class='cb-bar__title'])[3]")
	private WebElement msNumberDetailsOfSecondNumber;
	@FindBy(xpath = "//span[@class='cb-bar__price']")
	private WebElement msTotalOfPremiumNumber;
	
	@FindBy(xpath = "//div[@class='cb-modal__text']")
	private WebElement cardErrorMessage;

	@FindBy(xpath = "//button[@type='button'][contains(.,'Next')]")
	private WebElement nextButtonInNumberPage;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Previous')]")
	private WebElement previousButtonInNumberPage;
	@FindBy(xpath = "//span[@class='countrylist_numberselect callEnabled'][1]")
	private WebElement voiceEnabled;
	@FindBy(xpath = "//span[@class='countrylist_numberselect smsEnabled'][1]")
	private WebElement smsEnabled;

	@FindBy(xpath = "//button[@type='button'][contains(.,'Add')]")
	private WebElement popup_addNumberBtn;

	@FindBy(xpath = "(//span[contains(@class,'cb-bar__middle')])[2]")
	private WebElement planBucketCount;
	@FindBy(xpath = "//p[@class='free_num_note_p'][contains(.,'You won’t be charged as 1 number/user is free in bronze plan.')]")
	private WebElement addFreeNumberPopupMessageForBronzeMonthly;
	@FindBy(xpath = "//p[@class='free_num_note_p'][contains(.,'You won’t be charged as 1 number/user is free in silver plan.')]")
	private WebElement addFreeNumberPopupMessageForSilverMonthly;
	@FindBy(xpath = "//p[@class='free_num_note_p'][contains(.,'You won’t be charged as 1 number/user is free in platinum plan.')]")
	private WebElement addFreeNumberPopupMessageForPlatinumMonthly;

	@FindBy(xpath = "//div[@id[contains(.,'Dialog')]][contains(.,'Purchasing a Number')]/..//following-sibling::div//button[@type='button'][contains(.,'Yes')]")
	private WebElement addButtonOfFreeNumberPopupMessage;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Cancel')]")
	private WebElement cancelButtonOfFreeNumberPopupMessage;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'No')]")
	private WebElement nOButtonOfFreeNumberPopupMessage;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	private WebElement yesButtonOfFreeNumberPopupMessage;

	@FindBy(xpath = "//div[contains(@class,' css-1hwfws3')]")
	private WebElement locationDropdown;

	@FindBy(xpath = "//p[@class='free_num_note_p'][contains(.,'NOTE: This number will be added as per your plan and period.')]")
	private WebElement addPaidNumberPopupMessage;

	@FindBy(xpath = "//input[contains(@placeholder,'Enter location')]")
	private WebElement locationDropdownText;

	@FindBy(xpath = "//p[contains(.,'By deleting this number you give us the rights to this number.')]")
	//@FindBy(xpath = "//p[contains(.,'Are you sure you want to delete this number ?')]")
	private WebElement areYouSureDeleteThisNumber;
	@FindBy(xpath = "//button[@type='button'][contains(.,'No')]") private WebElement noDeleteNumberPopup;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]") private WebElement yesDeleteNumberPopup;
	@FindBy(xpath = "//div[@class='ant-message-custom-content ant-message-success'][contains(.,'Number deleted successfully')]") private WebElement deleteNumberValidationMessage;
	
	@FindBy(xpath = "//h5[@class='ellipsis countrytitle tcenterlnfix']") private List<WebElement> topCountryList;
	
	@FindBy(xpath = "//h5[@class='ellipsis countrytitle']") private List<WebElement> allCountryList;
	
	@FindBy(xpath = "(//a[@class='teamMemberNameSpan'])[1]") private WebElement userSettingButton;
	
	@FindBy(xpath = "//h3[contains(.,'Allocate Numbersinfo')]") private WebElement usersAllocateNumberText;
	
	@FindBy(xpath = "//span[@class='ant-alert-message'][contains(.,'Number not available.')]")
	private WebElement numberNotAvailableTxt;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Enter a Number.')]") private WebElement enterNumber;
	@FindBy(xpath = "//input[contains(@placeholder,'Enter a Prefix.')]") private WebElement enterPrefix;
	@FindBy(xpath = "//input[contains(@placeholder,'Enter location')]") private WebElement enterLocation;
	
	@FindBy(xpath = "//i[@class='material-icons seatpurchasedtooltip']") 
	private WebElement seatiTag;
	@FindBy(xpath = "//div[@class='ant-tooltip-inner']") 
	private WebElement seatiTagValue;
	@FindBy(xpath = "//input[contains(@class,'totalseatinput')]") 
	private WebElement seatValue;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]") private WebElement yesFreeNumberPopup;
	
	
	
	
	public AddNumberPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 45);
		action = new Actions(this.driver);
		numberPrice = new XLSReader("Data\\numberprices.xlsx");
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public boolean validateNextButton() {
		int i = driver.findElements(By.xpath("//span[@class='numberlist_country srchspannumty']")).size();
		if (i == 20) {
			try {
				return nextButtonInNumberPage.isDisplayed();
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		}

	}

	public void clickOnNextButtonInNumberList() {
		wait.until(ExpectedConditions.visibilityOf(nextButtonInNumberPage)).click();
	}

	public void clickOnPreviousButtonInNumberList() {
		wait.until(ExpectedConditions.visibilityOf(previousButtonInNumberPage)).click();
	}

	public boolean validatePreviousButton() {

		try {
			return previousButtonInNumberPage.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateFirstNumberVoiceEnabled() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(voiceEnabled)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateFirstNumberSMSEnabled() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(smsEnabled)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}

	public void clickOnAddNumberInPopup() {
		wait.until(ExpectedConditions.visibilityOf(addNumber_popup_btn)).click();
	}

	public boolean validateTourPopupIsDisplaying() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(welcome_popup_msg)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void endTourButtonInpopup() {
		wait.until(ExpectedConditions.visibilityOf(addNumber_popup_btn)).click();
	}

	public void clickOnAddNumberInNumbersPage() {
		wait.until(ExpectedConditions.visibilityOf(addNumber)).click();
	}
	public void waitForAddNumberInNumbersPage() {
		wait.until(ExpectedConditions.visibilityOf(addNumber));
	}

	public void clickOnCountry(String countryName) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.xpath("//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'" + countryName + "')]"))))
				.click();
	}
	
	public void clickOnCountryFromAll(String countryName) {
		
		WebElement Country = wait.until(ExpectedConditions.visibilityOf( driver.findElement(
				By.xpath("//h5[@class='ellipsis countrytitle'][contains(.,'"+countryName+"')]"))));
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("arguments[0].scrollIntoView();", Country);
		js1.executeScript("arguments[0].click();", Country);
	}

public void clickOnCountryOfAllCountryList(String countryName) {
	List<WebElement> closeAddcredit = driver.findElements(By.xpath("//i[@id='g_reminder_alert_close']"));
	
	if(closeAddcredit.size()>0) {
		driver.findElement(By.xpath("//i[@id='g_reminder_alert_close']")).click();
		System.out.println("closeAddcredit");
		Common.pause(2);
	}
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(
				By.xpath("//h5[@class='ellipsis countrytitle'][contains(.,'"+countryName+"')]"))))
				.click();
	}
	
	public void clickOnCountryUnitedStates() {
		wait.until(ExpectedConditions.visibilityOf(unitedStates)).click();
	}
	
	public String getFirstNumberOfNumberPage() {
		return wait.until(ExpectedConditions.visibilityOf(firstNumberOnNumberPage)).getText();
	}

	public String getFirstNumberOfNumberListPage() {
		return wait.until(ExpectedConditions.visibilityOf(firstNumber)).getText();
	}
	
	public String getCardErrorValidationMessage() {
		
		try{
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("cb-frame"));
		cardErrorMessage.click();
		return wait.until(ExpectedConditions.visibilityOf(cardErrorMessage)).getText();
		}catch (Exception e) {
		wait.until(ExpectedConditions.visibilityOf(coSubscribeButton)).click();
		return wait.until(ExpectedConditions.visibilityOf(cardErrorMessage)).getText();
		}
	}

	public void clickOnFirstNumberOfNumberListPage() {
		wait.until(ExpectedConditions.visibilityOf(firstNumber)).click();
	}

	public void clickOnMonthlyPlan() {
		wait.until(ExpectedConditions.visibilityOf(monthlyPlan)).click();
	}

	public void clickOnAnnuallyPlan() {
		wait.until(ExpectedConditions.visibilityOf(annuallyPlan)).click();
	}

	public void selectBronzePlan() {
		wait.until(ExpectedConditions.visibilityOf(bronzePlan)).click();
	}

	public void selectSilverPlan() {
		wait.until(ExpectedConditions.visibilityOf(silverPlan)).click();
	}

	public void selectPlatinumPlan() {
		wait.until(ExpectedConditions.visibilityOf(platinumPlan)).click();
	}

	public String getBronzePlanPrice() {

		String price = wait.until(ExpectedConditions.visibilityOf(priceBronzePlan)).getText();
		String price1 = price.replace("$", "");
		return price1.trim();
	}

	public String getSilverPlanPrice() {
		String price = wait.until(ExpectedConditions.visibilityOf(priceSilverPlan)).getText();
		String price1 = price.replace("$", "");
		return price1.trim();
	}

	public String getPlatinumPlanPrice() {
		String price = wait.until(ExpectedConditions.visibilityOf(pricePlatinumPlan)).getText();
		String price1 = price.replace("$", "");
		return price1.trim();
	}
	
	public String getseatiTagValue() {
		wait.until(ExpectedConditions.visibilityOf(seatiTag));
		action.moveToElement(seatiTag).build().perform();
		return wait.until(ExpectedConditions.visibilityOf(seatiTagValue)).getText();
	}
	
	public String getSeatCount() {
		return wait.until(ExpectedConditions.visibilityOf(seatValue)).getAttribute("value");
	}
	
	public void enterSeatCount(String value) {
		wait.until(ExpectedConditions.visibilityOf(seatValue)).clear();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(seatValue)).sendKeys(value);
	}

	
	
	public void clickOnCheckoutButton() {
		try {
		wait.until(ExpectedConditions.elementToBeClickable(checkoutButton)).click();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("cb-frame"));
		}catch (Exception e) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", checkoutButton);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("cb-frame"));
		}
	}
	public void clickOnBuyNowButton() {
		wait.until(ExpectedConditions.elementToBeClickable(BuyNowButton)).click();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("cb-frame"));
	}
	public String getPlanPriceFromYourOrderPopup() {

		return wait.until(ExpectedConditions.visibilityOf(ypPrice)).getText().replace("$", "").replace(".00", "");
	}

	public String getPlanDetailsFromYourOrderPopup() {
		System.out.println("---------"+ypPlanDetails.getText()+"------");
		return wait.until(ExpectedConditions.visibilityOf(ypPlanDetails)).getText();
	}

	public void clickOnCouponCodeLinkFromYourOrderPopup() {
		wait.until(ExpectedConditions.visibilityOf(ypCouponcodeLink)).click();
	}

	public void clickOnypProceedToCheckoutFromYourOrderPopup() {
		wait.until(ExpectedConditions.visibilityOf(ypProceedToCheckout)).click();
	}

	public void clickOnSubscribeButtonFromCompleteOrderPopup() {
		wait.until(ExpectedConditions.visibilityOf(coSubscribeButton)).click();
	}

	public void clickOnNotNowImDonePopup() {
		wait.until(ExpectedConditions.visibilityOf(notNowImDone)).click();
	}
	public void closeAddProofPopup() {

		Common.pause(1);
		action.sendKeys(Keys.ESCAPE).build().perform();
		Common.pause(1);
	}

	public void clickOnProfileDropDown() {
		wait.until(ExpectedConditions.visibilityOf(profileDropdown)).click();
	}

	public void clickOnAccountDetail() {
		wait.until(ExpectedConditions.visibilityOf(accountDetails)).click();
	}

	public void addDetailsInCard() {
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(cardFirstName)).sendKeys("Test");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardLastName)).sendKeys("Automation");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardAddressLine1)).sendKeys("signature building 3rd Floor");
		wait.until(ExpectedConditions.visibilityOf(cardCity)).sendKeys("Ahmedabad");
		wait.until(ExpectedConditions.visibilityOf(cardCountry));
		Common.pause(2);
		Select drpCountry = new Select(cardCountry);
		drpCountry.selectByValue("IN");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardNextButton)).click();
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(cardNumber));
		wait.until(ExpectedConditions.elementToBeClickable(cardNumber));//.sendKeys("4242424242424242");
		cardNumber.click();
		cardNumber.sendKeys("4242424242424242");
		Common.pause(2);
		action.sendKeys(Keys.TAB).build().perform();
		action.sendKeys(Keys.ENTER).build().perform();
		// action.sendKeys(Keys.CONTROL).sendKeys(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();
		wait.until(ExpectedConditions.visibilityOf(cardExpiryMonth));//.sendKeys("10");
		cardExpiryMonth.click();
		cardExpiryMonth.sendKeys("10");
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(cardExpiryYear)).sendKeys("2024");
		wait.until(ExpectedConditions.visibilityOf(cardCVV)).sendKeys("100");
		Common.pause(1);
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardNextButton)).click();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(coSubscribeButton)).click();
		driver.switchTo().defaultContent();

	}

	public void addDetailsInCardOfTransationError() {
		wait.until(ExpectedConditions.visibilityOf(cardFirstName)).sendKeys("Test");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardLastName)).sendKeys("Automation");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardAddressLine1)).sendKeys("signature building 3rd Floor");
		wait.until(ExpectedConditions.visibilityOf(cardCity)).sendKeys("Ahmedabad");
		wait.until(ExpectedConditions.visibilityOf(cardCountry));
		Common.pause(1);
		Select drpCountry = new Select(cardCountry);
		drpCountry.selectByValue("IN");
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardNextButton)).click();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardNumber)).sendKeys("4000000000000341");
		Common.pause(1);
		action.sendKeys(Keys.TAB).build().perform();
		action.sendKeys(Keys.ENTER).build().perform();
		// action.sendKeys(Keys.CONTROL).sendKeys(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();
		wait.until(ExpectedConditions.visibilityOf(cardExpiryMonth)).sendKeys("10");
		wait.until(ExpectedConditions.visibilityOf(cardExpiryYear)).sendKeys("2024");
		wait.until(ExpectedConditions.visibilityOf(cardCVV)).sendKeys("100");
		Common.pause(1);
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(cardNextButton)).click();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(coSubscribeButton)).click();
		driver.switchTo().defaultContent();

	}

	public void clickOnSaveNumberButton() {
		Common.pause(7);
		action.sendKeys(Keys.ESCAPE).build().perform();
		Common.pause(3);
		wait.until(ExpectedConditions.visibilityOf(saveNumberButton)).click();
	}
	
	
	public boolean validateNumberAddedSuccessfully(String number) {
		try {
			return wait
					.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'numbertable')]//a[contains(.,'" + number + "')]"))))
					.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
		public boolean validateNumberAddedSuccessfullyOnNumberOnboardingPage(String number) {
		try {
			return wait
					.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath("//span[contains(.,'" + number + "')]"))))
					.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void openAccountDetailsPopup() {
		wait.until(ExpectedConditions.visibilityOf(profileDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(accountDetails)).click();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("cb-frame"));
	}

	public String getPlanDetailsFromManageSubscription() {
		return wait.until(ExpectedConditions.visibilityOf(msPlanDetails)).getText();
	}
	
	public String getPlanDetailsFromAccountdetails() {
		return wait.until(ExpectedConditions.visibilityOf(cb_SubDetails)).getText();
	}

	public String getPlanPriceFromManageSubscription() {
		String price = wait.until(ExpectedConditions.visibilityOf(msPrice)).getText().replace("$", "").replace(".00",
				"");
		action.sendKeys(Keys.ESCAPE).build().perform();
		if(price.contains(",")) {
			System.out.println(price.replace(",", ""));
			return price.replace(",", "");
		}else {
			return price;
		}
		
	}
	public void hardRefreshByKeyboard() {
		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL)
		.keyDown(Keys.SHIFT)
		.sendKeys("r")
		.keyUp(Keys.SHIFT)
		.keyUp(Keys.CONTROL).build().perform();
		
	}

	public String getCreditFromSideMenu() {
		Common.pause(2);
		return wait.until(ExpectedConditions.visibilityOf(creditOnSideMenu)).getText().replace("Credit: $", "")
				.replace(".00", "");
	}

	public String getFreeIncomingMinutesFromDashboard() {
		//return wait.until(ExpectedConditions.visibilityOf(freeInMinutes)).getText().replace(" minutes", "");
		return "0";
	}

	public void selectNumberTypeByText(String text) {
		wait.until(ExpectedConditions.visibilityOf(numberTypeDropdown)).click();
		Common.pause(3);
		
		if(driver.findElements(By.xpath("//li[@role='option'][contains(.,'" + text + "')]")).size()>0) {
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//li[@role='option'][contains(.,'" + text + "')]"))))
				.click();
		}else {
			wait.until(ExpectedConditions.visibilityOf(numberTypeDropdown)).click();

		}
			
		Common.pause(5);
	}
	
	public String getSelectedNumberType() {
		return wait.until(ExpectedConditions.visibilityOf(numberTypeDropdown)).getText();
	}

	public void selectSearchByText(String text) {
		wait.until(ExpectedConditions.visibilityOf(searchByDropdown)).click();
		Common.pause(3);
		
		if(driver.findElements(By.xpath("//li[@role='option'][contains(.,'" + text + "')]")).size()>0) {
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//li[@role='option'][contains(.,'" + text + "')]"))))
				.click();
		}
		Common.pause(5);
		
	}

	public void enterPrefixOrNumber(String prefixOrNumber) {
		wait.until(ExpectedConditions.visibilityOf(prefixOrNumberSearchbox)).sendKeys(prefixOrNumber);
		action.sendKeys(Keys.TAB).build().perform();
		Common.pause(5);
	}

	public void clickOnNextButtonOfDocumentRequired() {
		wait.until(ExpectedConditions.visibilityOf(nextButtonOfDocumentRequired)).click();
	}
	
	public void clickOnCloseButtonOfchat() {
		
	try {
	WebElement closepopup=wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='cc-g0ak cc-hy0f']"))));
	closepopup.click();
	System.out.print("Close pop up.");
	}catch (Exception e) {
	}
	}

	public String getPlanDetailsFromYourOrderOfPremiumNumber() {
		return wait.until(ExpectedConditions.visibilityOf(pYourOrderPlanDetails)).getText();
	}

	public String getNumberDetailFromYourOrderOfPremiumNumber() {
		return wait.until(ExpectedConditions.visibilityOf(pYourOrderNumberDetails)).getText();
	}

	public String getPlanPriceFromYourOrderOfPremiumNumber() {

		return wait.until(ExpectedConditions.visibilityOf(pYourOrderPlanPrice)).getText().replace("$", "")
				.replace(".00", "");
	}

	public String getNumberPriceFromYourOrderOfPremiumNumber() {

		return wait.until(ExpectedConditions.visibilityOf(pYourOrderNumberPrice)).getText().replace("$", "")
				.replace(".00", "");
	}

	public String getTotalPriceFromYourOrderOfPremiumNumber() {

		return wait.until(ExpectedConditions.visibilityOf(pYourOrderTotal)).getText().replace("$", "").replace(".00",
				"");
	}

	public String additionOfTwoStringNumber(String value1, String value2) {

		int i1 = Integer.parseInt(value1);
		int i2 = Integer.parseInt(value2);
		int intTotal = i1 + i2;
		return String.valueOf(intTotal);

	}
	
	public String subtractionOfTwoStringNumber(String value1, String value2) {

		int i1 = Integer.parseInt(value1);
		int i2 = Integer.parseInt(value2);
		int intTotal = i1 - i2;
		return String.valueOf(intTotal);

	}

	public String multiplicationOfTwoStringNumber(String value1, String value2) {

		int i1 = Integer.parseInt(value1);
		int i2 = Integer.parseInt(value2);
		int intTotal = i1 * i2;
		return String.valueOf(intTotal);
	}
	
	public String multiplicationOfTwoStringNumber1(String value1, String value2) {

		int i1 = Integer.parseInt(value1);
		float f1 = Float.parseFloat(value2);
		float intTotal = i1 * f1;
		return String.valueOf(intTotal);

	}
	public String divisonOfTwoStringNumber(String value1, String value2) {

		int i1 = Integer.parseInt(value1);
		int i2 = Integer.parseInt(value2);
		int intTotal = i1 / i2;
		return String.valueOf(intTotal);

	}

	public String roundof(String doublevalue) {
		double str = Double.parseDouble(doublevalue);
		double str1 = Math.round(str);
		int value = (int)str1;
		String s=String.valueOf(value);
		return s;
	}
	
	public String getTotalPriceFromManageSubscriptionOfPremiumNumber() {

		String price= wait.until(ExpectedConditions.visibilityOf(msTotalOfPremiumNumber)).getText().replace("$", "")
				.replace(".00", "");
		if(price.contains(",")) {
			System.out.println(price.replace(",", ""));
			return price.replace(",", "");
		}else {
			return price;
		}
	}

	public String getPlanDetailsFromManageSubscriptionOfPremiumNumber() {
		return wait.until(ExpectedConditions.visibilityOf(msPlanDetailsOfPremiumNumber)).getText();
	}

	public String getNumberDetailFromManageSubscriptionOfPremiumNumber() {
		Common.pause(2);
		List<WebElement> msNumberDetailsOfPremiumNumber1= driver.findElements(By.xpath("(//div[@class='cb-bar__title'])[2]"));
		if(msNumberDetailsOfPremiumNumber1.size()>0) {
			return wait.until(ExpectedConditions.visibilityOf(msNumberDetailsOfPremiumNumber)).getText();
		}else {
			return "false";
		}
		
	}
	
	public String getNumberDetailFromManageSubscriptionOfSecondNumber() {
		return wait.until(ExpectedConditions.visibilityOf(msNumberDetailsOfSecondNumber)).getText();
	}

	public void clickOnAddNumberInNumbersPopup() {
		wait.until(ExpectedConditions.visibilityOf(popup_addNumberBtn)).click();
	}

	public void clickOnAddButtonOfFreeNumberPopup() {
		wait.until(ExpectedConditions.visibilityOf(addButtonOfFreeNumberPopupMessage)).click();
	}
	
	public void clickOnYesdButtonOfFreeNumberPopup() {
		wait.until(ExpectedConditions.visibilityOf(yesButtonOfFreeNumberPopupMessage)).click();
	}

	public void clickOnCancelButtonOfFreeNumberPopup() {
		wait.until(ExpectedConditions.visibilityOf(cancelButtonOfFreeNumberPopupMessage)).click();
	}
	
	public void clickOnNoButtonOfFreeNumberPopup() {
		wait.until(ExpectedConditions.visibilityOf(nOButtonOfFreeNumberPopupMessage)).click();
	}

	public boolean getMessageOfFreeNumberPopupForBronzeMonthly() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(addFreeNumberPopupMessageForBronzeMonthly)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean getMessageOfPaidNumberPopup() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(addPaidNumberPopupMessage)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean getMessageOfFreeNumberPopupForSilverMonthly() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(addFreeNumberPopupMessageForSilverMonthly)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean getMessageOfFreeNumberPopupForPlatinumMonthly() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(addFreeNumberPopupMessageForPlatinumMonthly))
					.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void selectLocation1(String countryCode) {
		wait.until(ExpectedConditions.visibilityOf(locationDropdown)).click();
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElement(By.xpath("//*[@class='css-138jdgo-option'][contains(.,'" + countryCode + "')]"))))
				.click();
		Common.pause(5);
	}
	
	public String returnLocationName() {
		wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).click();
		Common.pause(1);
		action.sendKeys(Keys.ENTER).build().perform();
		Common.pause(1);
		String s1 = wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]"))))
				.getText();
		Common.pause(5);
		locationDropdownText.click();
		System.out.println("---returnLocationName---" + s1.substring(0, s1.lastIndexOf(" ")));
		return s1.substring(0, s1.lastIndexOf(" "));
	}
	
	public String returnLocationCode() {
		wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).click();
		Common.pause(1);
		action.sendKeys(Keys.ENTER).build().perform();
		Common.pause(1);
		String s1 = wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]"))))
				.getText();
		driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]")).click();
		Common.pause(5);
		System.out.println("---returnLocationCode---" + s1.substring(s1.indexOf("(") + 1, s1.lastIndexOf(")")));
		return s1.substring(s1.indexOf("(") + 1, s1.lastIndexOf(")"));
	}

	public String selectLocation() {
		wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).click();
		Common.pause(1);
		action.sendKeys(Keys.ENTER).build().perform();
		Common.pause(1);
		String s1 = wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]"))))
				.getText();
		driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]")).click();
		Common.pause(5);
		return s1.substring(s1.indexOf("(") + 1, s1.lastIndexOf(")"));
	}

	public String enterLocationPrefix(String locationPrefix) {
		wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).sendKeys(locationPrefix);
		Common.pause(3);
		action.sendKeys(Keys.TAB).build().perform();
		Common.pause(5);
		String s1 = wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]"))))
				.getText();
		driver.findElement(By.xpath("//div[contains(@class,'infinite-scroll-component ')]/div[1]")).click();
		Common.pause(5);
		System.out.println("---substring---" + s1.substring(s1.indexOf("(") + 1, s1.lastIndexOf(")")));
		return s1.substring(s1.indexOf("(") + 1, s1.lastIndexOf(")"));
	}

	public void deleteNumber(String number) {
		try{wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'" + number
				+ "')]/parent::td/following-sibling::td//i[@class='material-icons'][contains(.,'delete_forever')]"))))
				.click();}catch (Exception e) {
					Common.pause(3);
					wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'" + number
							+ "')]/parent::td/following-sibling::td//i[@class='material-icons'][contains(.,'delete_forever')]"))));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//a[contains(.,'" + number
							+ "')]/parent::td/following-sibling::td//i[@class='material-icons'][contains(.,'delete_forever')]")));
					System.out.println( " deleteNumber by javascript..");
				}

	}

	public boolean validateAreYouSureDeleteThisNumber() {

		try {
			return wait.until(ExpectedConditions.visibilityOf(areYouSureDeleteThisNumber)).isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public void clickOnNoNumberDeletePopup() {
		wait.until(ExpectedConditions.visibilityOf(noDeleteNumberPopup)).click();
	}
	
	public void clickOnYesNumberDeletePopup() {
		wait.until(ExpectedConditions.visibilityOf(yesDeleteNumberPopup)).click();
	}
	public void clickOnYesFreeNumberPopup() {
		wait.until(ExpectedConditions.visibilityOf(yesFreeNumberPopup)).click();
	}
	
	public boolean validateDeleteNumberSuccessfullMessageIsDisplayed() {

		try {
			wait.until(ExpectedConditions.visibilityOf(deleteNumberValidationMessage));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void userspage() throws IOException {
		driver.get(credential.usersPage());
	}
	
	public void navigatetoUserSettingPage() throws Exception {
		// driver.get(url.usersPage());
		wait.until(ExpectedConditions.visibilityOf(userSettingButton));
		userSettingButton.click();
		// js.executeScript("$(\"[class='circlest btn btn-sm btn-info']\").click();");
	}
	
	public boolean veryAllocatedNumberVisibleInUsers(String country, String number) {
		boolean numberVisible = false;
		try {
			Common.pause(1);
			wait.until(ExpectedConditions.visibilityOf(usersAllocateNumberText));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", usersAllocateNumberText);
			Common.pause(1);
			if(driver.findElement(By.xpath("//span[contains(.,'"+country+"')]")).isDisplayed() &&
					driver.findElement(By.xpath("//span[contains(.,'"+country+"')]//following-sibling::span")).getText().equalsIgnoreCase(number))
			{
				numberVisible = true;
			}
		} catch (Exception e) {
			numberVisible = false;
		}
		return numberVisible;
	}
	
	public boolean verifyFieldsOnNumbersPage(String country, String number) {
		boolean numberDetails = false;
		try {
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(@href,'numbers')][contains(.,'"+country+"')]"))));
			if(driver.findElement(By.xpath("//a[contains(@href,'numbers')][contains(.,'"+country+"')]")).isDisplayed() &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[2]/a")).getText().equalsIgnoreCase(number) &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[3]/a")).getText().equalsIgnoreCase("Enabled") &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[4]/a")).getText().equalsIgnoreCase("Enabled") &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[5]/a")).getText().equalsIgnoreCase("Available") &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[6]//a/b")).getText().equalsIgnoreCase("1 Users") &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[7]/div/span[contains(@class,'settingcircle')]")).isDisplayed() &&
					driver.findElement(By.xpath("//tbody[@class='ant-table-tbody'][contains(.,'"+country+"')]/tr/td[7]/div/span[contains(@class,'deletecircle')]")).isDisplayed() 
					)
			{
				numberDetails = true;
			}
		} catch (Exception e) {
			numberDetails = false;
		}
		return numberDetails;
	}
	
	public boolean validateNumberNotAvailable() {

		try {
			return numberNotAvailableTxt.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public void verifyTopCountryDetails()
	{
//		try
//		{
			for(int i=0; i<topCountryList.size(); i++)
			{
				System.out.println("----"+topCountryList.get(i).getText());
				if (topCountryList.get(i).getText().equalsIgnoreCase("United States")) {
					System.out.println("got the united states country ");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'United States')]/../span/span[@class[contains(.,'flag-icon-us')]]"))
							.isDisplayed(), "US flag is not matched.");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'United States')]//following-sibling::div[contains(.,'+1')]"))
							.isDisplayed(), "US country code is not matched.");
				}
				if(topCountryList.get(i).getText().equalsIgnoreCase("United Kingdom"))
				{
					System.out.println("got the united Kingdom country ");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'United Kingdom')]/../span/span[@class[contains(.,'flag-icon-gb')]]"))
							.isDisplayed(), "United Kingdom flag is not matched.");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'United Kingdom')]//following-sibling::div[contains(.,'+44')]"))
							.isDisplayed(), "United Kingdom country code is not matched.");
				}
				if(topCountryList.get(i).getText().equalsIgnoreCase("Canada"))
				{
					System.out.println("got the united Kingdom country ");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'Canada')]/../span/span[@class[contains(.,'flag-icon-ca')]]"))
							.isDisplayed(), "Canada flag is not matched.");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'Canada')]//following-sibling::div[contains(.,'+1')]"))
							.isDisplayed(), "Canada country code is not matched.");
				}
				if(topCountryList.get(i).getText().equalsIgnoreCase("Australia"))
				{
					System.out.println("got the Australia country ");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'Australia')]/../span/span[@class[contains(.,'flag-icon-au')]]"))
							.isDisplayed(), "Australia flag is not matched.");
					softAssert.assertTrue(driver.findElement(By.xpath(
							"//h5[@class='ellipsis countrytitle tcenterlnfix'][contains(.,'Australia')]//following-sibling::div[contains(.,'+61')]"))
							.isDisplayed(), "Australia country code is not matched.");
				}
				
			}
			softAssert.assertAll();
	}
	
	public void verifyAllCountryDetails() throws FilloException
	{
		for(int i=0; i<allCountryList.size(); i++)
		{
			String countryName = allCountryList.get(i).getText();
			String countryCode = numberPrice.getField("countryCode","select * from numberprices where country='"+countryName+"'");
			softAssert.assertTrue(driver.findElement(By.xpath(
						"//h5[@class='ellipsis countrytitle'][contains(.,'"+countryName+"')]//following-sibling::div[contains(.,'+"+countryCode+"')]"))
						.isDisplayed(), "US country code is not matched.");
		}
	}
	
	// ------------------ search country wise number test logic ----------------------------------
	
	public int getCountryCountOnNumberListPage() {
		
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h5[@class='ellipsis countrytitle'] | //h5[@class='ellipsis countrytitle tcenterlnfix']"))));
		
		return driver.findElements(By.xpath("//h5[@class='ellipsis countrytitle'] | //h5[@class='ellipsis countrytitle tcenterlnfix']")).size();
		
	}
	
	public void clickOnCountryByIndex(int index) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//h5[@class='ellipsis countrytitle'] | //h5[@class='ellipsis countrytitle tcenterlnfix'])["+index+"]")))).click();
		System.out.println("click on country");
	}
	
	public String getCountryNameByIndex(int index) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//h5[@class='ellipsis countrytitle'] | //h5[@class='ellipsis countrytitle tcenterlnfix'])["+index+"]")))).getText();
		}
	
	public boolean validateNumberIsDisplay() {
		try {
			return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='numberlist_country srchspannumty']")))).isDisplayed();
//			return true;
		}catch(Exception e) {
			return false;
		}
		
	}
	
	public void clickOnLocationDropdownAndCrollTillEnd() {
		wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).click();
		Common.pause(1);
		
		if(driver.findElements(By.xpath("(//div/span[contains(.,'(')])")).size()>0) {
			int size3,size1=0,size2=1;
			while(size1!=size2) {
			
			 size1 = driver.findElements(By.xpath("(//div/span[contains(.,'(')])")).size();
			
			action.moveToElement(driver.findElement(By.xpath("(//div/span[contains(.,'(')])["+size1+"]"))).build().perform();
			Common.pause(5);
			
			 size2 = driver.findElements(By.xpath("(//div/span[contains(.,'(')])")).size();
			
			}
		}
	}
	
	public void clickOnLocationDropdown() {
		wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).click();
		Common.pause(3);
	}
	
	public void clickOnLocation(int index) {
		action.moveToElement(driver.findElement(By.xpath("(//div/span[contains(.,'(')])["+index+"]"))).build().perform();
		Common.pause(1);
		driver.findElement(By.xpath("(//div/span[contains(.,'(')])["+index+"]")).click();
		Common.pause(3);
	}
	
	public String getLocationCode() {
		String s1 =wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).getText();
	return s1.substring(s1.indexOf("(") + 1, s1.lastIndexOf(")"));
	}
	
	public int getSizeOfLocations() {
		return driver.findElements(By.xpath("(//div/span[contains(.,'(')])")).size();
	}
	
	public String getLocationName() {
		return wait.until(ExpectedConditions.visibilityOf(locationDropdownText)).getAttribute("value");
	
	}
	
	
	public void validateSearchedLocationDisplayInList(int index) {
		action.moveToElement(driver.findElement(By.xpath("(//div/span[contains(.,'(')])["+index+"]"))).build().perform();
		Common.pause(1);
		driver.findElement(By.xpath("(//div/span[contains(.,'(')])["+index+"]")).click();
		Common.pause(3);
	}
	
	public String getNumberBetween(int from, int to) {
			return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='numberlist_country srchspannumty']")))).getText().substring(from, to);
	}
	
	public void enterNumberInNumberSearchBox(String numberText) {
		wait.until(ExpectedConditions.visibilityOf(enterNumber)).clear();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(enterNumber)).sendKeys(numberText);
		Common.pause(10);
	}
	
	public void enterPrefixInNumberSearchBox(String numberText) {
		wait.until(ExpectedConditions.visibilityOf(enterPrefix)).clear();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(enterPrefix)).sendKeys(numberText);
		Common.pause(10);
	}
	
	public void assertNumberInAllNumbersOfList(String numberText){
		
		int size = driver.findElements(By.xpath("//span[@class='numberlist_country srchspannumty']")).size();
		SoftAssert as = new SoftAssert();
		for(int i=0; i<size;i++) {
			
			as.assertTrue(driver.findElements(By.xpath("//span[@class='numberlist_country srchspannumty']")).get(i).getText().contains(numberText));
		}
		
		as.assertAll();
		
	}
	
	public void assertLocationInAllLocationsInDropdown(String location) {
		Common.pause(3);
		int size1 = driver.findElements(By.xpath("(//div/span[contains(.,'(')])")).size();
		
		SoftAssert as = new SoftAssert();
		for(int i=0;i<size1;i++) {
			as.assertTrue(driver.findElements(By.xpath("(//div/span[contains(.,'(')])")).get(i).getText().contains(location));
		}
		
		as.assertAll();
		
	}
	
	
	public void enterLocationInNumberSearchBox(String location) {
		wait.until(ExpectedConditions.visibilityOf(enterLocation)).clear();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(enterLocation)).sendKeys(location);
		Common.pause(10);
	}
	
	public boolean validateNumberNotAvailableMessageIsDisplay() {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='ant-alert-message'][contains(.,'Number not available.')]"))));
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public boolean validatePlanPopupIsDisplay() {
		Boolean elementavailable = false;
		int eleSize = driver.findElements(By.xpath("//input[@value='monthly']/parent::span/parent::label")).size();
		if(eleSize>0) {
			return elementavailable=true;
		}else {
			return elementavailable=false;
		}
	}
	
}
