package com.CallHippo.web.numberAndUserSubscription;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class PlanUpgradePage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	PropertiesFile credential;

	@FindBy(xpath = "//span[contains(.,'Bronze')]/parent::label/child::span[1]/input") private WebElement bronze;
	@FindBy(xpath = "//span[contains(.,'Silver')]/parent::label/child::span[1]/input") private WebElement silver;
	@FindBy(xpath = "//span[contains(.,'Platinum')]/parent::label/child::span[1]/input") private WebElement platinum;
	
	@FindBy(xpath = "//span[contains(.,'Monthly')]/parent::label/child::span[1]/input") private WebElement monthly;
	@FindBy(xpath = "//span[contains(.,'Annually')]/parent::label/child::span[1]/input") private WebElement annually;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Upgrade Plan')]") private WebElement planUpgrade;
	
	@FindBy(xpath = "//div[contains(@class,'ant-message-custom-content ant-message-success')]") private WebElement suceesfulMessageElement;
	
	public PlanUpgradePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 45);
		action = new Actions(this.driver);
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}
	
	
	public void clickOnBronzeRadioButton() {
		Common.pause(5);
		bronze.click();
//		wait.until(ExpectedConditions.visibilityOf(bronze)).click();
	}
	
	public void clickOnSilverRadioButton() {
		Common.pause(5);
		silver.click();
//		wait.until(ExpectedConditions.visibilityOf(silver)).click();
	}
	
	public void clickOnPlatinumRadioButton() {
		Common.pause(5);
		platinum.click();
//		wait.until(ExpectedConditions.visibilityOf(platinum)).click();
	}
	
	public void clickOnMonthlyRadioButton() {
		Common.pause(5);
		monthly.click();
//		wait.until(ExpectedConditions.visibilityOf(monthly)).click();
	}
	
	public void clickOnAnnuallyRadioButton() {
		Common.pause(5);
		annually.click();
//		wait.until(ExpectedConditions.visibilityOf(annually)).click();
	}
	
	public void clickOnPlanUpgradeButton() {
		Common.pause(5);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", planUpgrade);
		//planUpgrade.click();
//wait.until(ExpectedConditions.elementToBeClickable(suceesfulMessageElement));
	}
	
	
}
