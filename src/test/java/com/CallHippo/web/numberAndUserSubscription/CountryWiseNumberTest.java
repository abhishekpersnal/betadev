package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class CountryWiseNumberTest {

	WebDriver driver;

	AddNumberPage addNumberPage;
	SignupPage registrationPage;
	LoginPage loginpage;

	Common signupExcel;
	PropertiesFile url;
	PropertiesFile data;

	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;

	String newEmail = null;
	String gmailUser;
	String getNumber;
	String locationName;
	String locationCode;

	public CountryWiseNumberTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signupExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");

		gmailUser = data.getValue("gmailUser");
	}

	@BeforeTest
	public void initialization() throws Exception {
		driver = TestBase.init();
		addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
		registrationPage = PageFactory.initElements(driver, SignupPage.class);
		loginpage = PageFactory.initElements(driver, LoginPage.class);

		webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
		inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);

		driver.get(url.signIn());
	}

	@BeforeMethod
	public void beforeMethod() throws IOException {
		driver.navigate().to(url.numbersPage());
	}

	@AfterMethod
	public void afterMethodProcess(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}

	}

	@AfterTest(alwaysRun = true)
	public void tearDown() {
		driver.quit();
	}

	private String signup() throws Exception {
		try {
			driver.get(url.livesignUp());
		} catch (IOException e) {
		}
		String date = signupExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		newEmail = email;
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signupExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signupExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signupExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signupExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signupExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
	    
		registrationPage.ValidateVerifyButtonOnGmail();

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		System.out.println(email);
		return email;
	}

	@Test
	public void checkNumberListIsAvailableInAllCountry() throws Exception {
//		String email = signup();

		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

//		driver.get(url.numbersPage());
//		addNumberPage.clickOnAddNumberInNumbersPage();
//		addNumberPage.clickOnCountry("United States");
//
//		addNumberPage.selectNumberTypeByText("local");
//		addNumberPage.selectSearchByText("Location");
//		addNumberPage.clickOnLocationDropdown();
//		

		SoftAssert as = new SoftAssert();

		String countryListPage = "https://jenkins-app.callhippo.com/addnumber";
		driver.get(countryListPage);
		Common.pause(4);
		int countryCount = addNumberPage.getCountryCountOnNumberListPage();
		if (countryCount > 0) {

//			for (int i = 24; i <= 24; i++) {
			for (int i = 1; i <= countryCount; i++) {

				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				Common.pause(2);

				String countryName = addNumberPage.getCountryNameByIndex(i);

				addNumberPage.clickOnCountryByIndex(i);
				Common.pause(3);

				System.out.println("check country number : " + i);

				addNumberPage.selectNumberTypeByText("local");
				Common.pause(3);

				String NumberType = addNumberPage.getSelectedNumberType();

				if (NumberType.contains("Local")) {
					as.assertEquals(addNumberPage.validateNumberIsDisplay(), true, "Not getting Number in country : "
							+ countryName + " and Number Type is " + NumberType + ".");

					if (addNumberPage.validateNumberIsDisplay() == true) {
						addNumberPage.selectSearchByText("Location");
						Common.pause(3);

						as.assertEquals(addNumberPage.validateNumberIsDisplay(), true,
								"Not getting Number in country : " + countryName + " and Number Type is " + NumberType
										+ ". and Search by Location. ");

						if (addNumberPage.validateNumberIsDisplay() == true) {
							addNumberPage.clickOnLocationDropdownAndCrollTillEnd();

							int locationsSize = addNumberPage.getSizeOfLocations();

							addNumberPage.clickOnLocationDropdown();

							for (int j = 1; j <= locationsSize; j++) {
								addNumberPage.clickOnLocationDropdown();

								addNumberPage.clickOnLocation(j);
								String locationName = addNumberPage.getLocationName();
								as.assertEquals(addNumberPage.validateNumberIsDisplay(), true,
										"Not getting Number in country : " + countryName + " and Number Type is "
												+ NumberType + ". and Search by Location is " + locationName + ".");

							}
						}
					}
				}

				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				Common.pause(2);

				String countryName1 = addNumberPage.getCountryNameByIndex(i);

				addNumberPage.clickOnCountryByIndex(i);
				Common.pause(3);

				addNumberPage.selectNumberTypeByText("tollfree");
				Common.pause(3);

				String NumberType3 = addNumberPage.getSelectedNumberType();

				if (NumberType.contains("Tollfree")) {
					as.assertEquals(addNumberPage.validateNumberIsDisplay(), true, "Not getting Number in country : "
							+ countryName1 + " and Number Type is " + NumberType3 + ".");
				}

				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				Common.pause(2);

				String countryName2 = addNumberPage.getCountryNameByIndex(i);

				addNumberPage.clickOnCountryByIndex(i);
				Common.pause(3);

				addNumberPage.selectNumberTypeByText("mobile");
				Common.pause(3);

				String NumberType1 = addNumberPage.getSelectedNumberType();
				if (NumberType1.contains("Mobile")) {
					as.assertEquals(addNumberPage.validateNumberIsDisplay(), true, "Not getting Number in country : "
							+ countryName2 + " and Number Type is " + NumberType1 + ".");
				}
			}
		}

		as.assertAll();
	}

}
