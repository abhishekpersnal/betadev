package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class PlanUpgradeTest {

	PlanUpgradePage settingPage;
	AddNumberPage addNumberPage;
	SignupPage registrationPage;
	SignupPage dashboard;
	LoginPage loginpage;
	WebDriver driver;
	Common signupExcel;
	PropertiesFile url;
	XLSReader planPrice;
	XLSReader numberPrice;
	PropertiesFile data;

	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;

	String newEmail = null;
	String gmailUser;

	public PlanUpgradeTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signupExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		numberPrice = new XLSReader("Data\\numberprices.xlsx");
		gmailUser = url.getValue("gmailUser");
	}

	@BeforeMethod
	public void initialization() throws Exception {
		driver = TestBase.init();
		settingPage = PageFactory.initElements(driver, PlanUpgradePage.class);
		addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
		registrationPage = PageFactory.initElements(driver, SignupPage.class);
		dashboard = PageFactory.initElements(driver, SignupPage.class);
		loginpage = PageFactory.initElements(driver, LoginPage.class);

		webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
		inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);

		driver.get(url.signIn());

	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driver.quit();
	}

	@Test(retryAnalyzer = Retry.class)
	private String signup() throws Exception {
		try {
			driver.get(url.livesignUp());
		} catch (IOException e) {
		}
		String date = signupExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---email--"+email);
		newEmail = email;
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signupExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signupExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signupExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signupExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signupExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
		registrationPage.ValidateVerifyButtonOnGmail();

		dashboard.verifyWelcomeToCallHippoPopup();
		dashboard.closeAddnumberPopup();
//		dashboard.userLogout();
//		
//		loginpage.enterEmail(email);
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		return email;
	}

	@Test(retryAnalyzer = Retry.class)
	public void signup1() throws Exception {
		signup();
	} 

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Bronze_Monthly_To_Bronze_Annuaaly() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		driver.get(url.planPage());

		settingPage.clickOnBronzeRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Bronze'"));

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Bronze_Monthly_To_Silver_Monthly() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		driver.get(url.planPage());

		settingPage.clickOnSilverRadioButton();
		settingPage.clickOnMonthlyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Bronze_Monthly_To_Silver_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		driver.get(url.planPage());

		settingPage.clickOnSilverRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Silver'"));

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Bronze_Monthly_To_Platinum_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		driver.get(url.planPage());

		settingPage.clickOnPlatinumRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Platinum"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Platinum'"));

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Bronze_Annually_To_Silver_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				planPrice.getField("annuallyPerMonth", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("annually", "select * from Sheet1 where plan='Bronze'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("annually", "select * from Sheet1 where plan='Bronze'"));

		driver.get(url.planPage());

		settingPage.clickOnSilverRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Silver'"));
		
		
		
	}
	
	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Bronze_Annually_To_Platinum_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				planPrice.getField("annuallyPerMonth", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("annually", "select * from Sheet1 where plan='Bronze'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("annually", "select * from Sheet1 where plan='Bronze'"));

		driver.get(url.planPage());

		settingPage.clickOnPlatinumRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Platinum"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Platinum'"));
		
	}
	
	
	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Silver_Monthly_To_Silver_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.selectSilverPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));
		
		
		driver.get(url.planPage());

		settingPage.clickOnSilverRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Silver'"));

	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Silver_Monthly_To_Platinum_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.selectSilverPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("monthly", "select * from Sheet1 where plan='Silver'"));
		
		
		driver.get(url.planPage());

		settingPage.clickOnPlatinumRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Platinum"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Platinum'"));

	}
	
	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Verify_Plan_Upgrade_From_Silver_Annually_To_Platinum_Annually() throws Exception {
		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				planPrice.getField("annuallyPerMonth", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.selectSilverPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPrice.getField("annually", "select * from Sheet1 where plan='Silver'"));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				planPrice.getField("annually", "select * from Sheet1 where plan='Silver'"));
		
		driver.get(url.planPage());

		settingPage.clickOnPlatinumRadioButton();
		settingPage.clickOnAnnuallyRadioButton();
		settingPage.clickOnPlanUpgradeButton();

		driver.get(url.signIn());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Platinum"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				planPrice.getField("annually", "select * from Sheet1 where plan='Platinum'"));


	}
	
}
