package com.CallHippo.web.numberAndUserSubscription;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.internal.WebElementToJsonConverter;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.StaleElementReferenceException;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

import java.util.Set;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class InviteUserPage {

	WebDriver driver;
	WebDriverWait wait;
	WebDriverWait wait2;
	Actions action;
	PropertiesFile credential;
	
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	public InviteUserPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 30);
		wait2 = new WebDriverWait(this.driver, 2);
	}
	
	@FindBy(xpath = "//div[@class='totalseattext'][contains(.,'TOTAL SEATS')]")
	WebElement totalSeatCount;
	@FindBy(xpath = "//button[@class[contains(.,'ant-btn ant-btn-primary')]]/span[text()='Yes']")
	WebElement btn_yes;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Invite Users')]")
	WebElement inviteUserBtn;
	
	@FindBy(xpath = "//h2[contains(.,'Invite User')]")
	WebElement inviteUserText;
	
	@FindBy(xpath = "//label[contains(.,'Email Address')]")
	WebElement email_label;
	
	@FindBy(xpath = "//div[@style[contains(.,'display: flex')]]/i[@class='material-icons'][contains(.,'save')]/../../..//input[@placeholder='Enter email address']")
	WebElement email_Textbox;
	
	@FindBy(xpath = "(//input[@placeholder='Enter email address'])[2]")
	WebElement email_Textbox2;
	
	@FindBy(xpath = "//div[@style[contains(.,'display: flex')]]/i[@class='material-icons'][contains(.,'save')]")
	WebElement inviteUser_save_button;
	
	@FindBy(xpath = "//label[contains(.,'Allocate Numbers')]")
	WebElement allocateNum_label;
	
	@FindBy(xpath = "//button[contains(.,'Invite  1 Users')][contains(disabled,'')]")
	WebElement invite1User_disable_button;
	
	@FindBy(xpath = "//button[contains(@class,'inviteUserBtn')]")
	WebElement inviteNUser_button;
	
	@FindBy(xpath = "//div[@id[contains(.,'DialogTitle')]]/..//following-sibling::div//button[contains(.,'Yes')]")
	WebElement inviteUser_confirm_yes_button;
	
	@FindBy(xpath = "//button/span[contains(.,'Cancel')]")
	WebElement inviteUser_cancel_btn;
	
	@FindBy(xpath = "//span[@class='ant-checkbox']")
	WebElement inviteUser_num_checkbox;
	
	@FindBy(xpath = "(//label[contains(.,'Allocate Numbers')]/../following-sibling::div/div//input[@type='checkbox'])[1]")
	WebElement inviteUser_number_checkbox;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-error')]]/span")
	private WebElement validateErrorMsg;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-warning')]]/span")
	private WebElement validateWarningMsg;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-success')]]/span")
	private WebElement validateSuccessMsg;
	
	@FindBy(xpath = "//div[@class[contains(.,'ant-message-info')]]/span")
	private WebElement validateInfoMsg;
	@FindBy(xpath = "//div[contains(@class,'ant-message-success')]//span")
	WebElement validationMessage;
	@FindBy(xpath = "//div[@class[contains(.,'chflex')]]/button[@class[contains(.,'insuercircleadd')]][@style[contains(.,'display: flex')]]")
	WebElement addButton;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Enter Full Name')]")
	WebElement fullName_textbox;
	
	@FindBy(xpath = "//input[contains(@placeholder,'New Password')]")
	WebElement newPassword_textbox;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Confirm Password')]")
	WebElement confirmPassword_textbox;
	
	@FindBy(xpath = "//button[contains(.,'Submit')]")
	WebElement subUser_submitButton;
	
	@FindBy(xpath = "//b[contains(.,'Your account has been activated.')]")
	WebElement activatedMessage;
	
	@FindBy(xpath = "//div[@class='ant-modal-title'][contains(.,'Forget user')]")
	WebElement deleteUser_popupHeader;
	
	@FindBy(xpath = "//p[contains(.,'Still want to delete the user?')]")
	WebElement deleteUser_confirmMessage;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement deleteUser_popup_yesButton;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
	WebElement updateSeat_popup_yesButton;
	@FindBy(xpath = "//button[@type='button'][contains(.,'Cancel')]/following-sibling::button[@type='button'][contains(.,'Upgrade')]")
	WebElement upgradeButton;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'No')]")
	WebElement deleteUser_popup_noButton;
	
	@FindBy(xpath = "//li[@title='Next Page']/a")
	WebElement nextPage_button;
	
	@FindBy(xpath = "//button[contains(.,'Yes')]")
	WebElement YesBtnForInviteUser;
	
	@FindBy(xpath = "//h3[contains(.,'Department Details')]")
	WebElement deptNameAndNumberTxt;
	
	@FindBy(xpath = "//a[contains(.,'United States')]")
	WebElement numberName;
	@FindBy(xpath = "//button[@test-automation='num_ivr_switch']")
	WebElement numberIVR;
	@FindBy(xpath = "//p[contains(.,'Any numbers assigned to this user will be de-allocated, and this process is irreversible.')]")
	WebElement deleteUserPopupMessage;
	
		@FindBy(xpath = "//button[@type='button'][contains(.,'Create Team')]")
		WebElement createTeam;
		
		@FindBy(xpath = "//input[@name='teamName']")
		WebElement teamName;
		
		@FindBy(xpath = "//span[contains(.,'delete_forever')]")
		WebElement deleteTeam;
		
		@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
		WebElement yesButton;
		
		@FindBy(xpath = "//p[contains(.,'Are you sure you want to delete this record?')]")
		WebElement deleteTeamPopupMessage;
		@FindBy(xpath = "(//span[contains(.,'Please delete the IVR(s) associated with this team before deleting this team.')])[2]")
		WebElement validationMessageWhenTeamIsAssignedInIVR;
		@FindBy(xpath = "(//div[contains(@class,'listSortEmail')])[1]")
		WebElement firstUserTeam;
		@FindBy(xpath = "(//div[contains(@class,'listSortEmail')])[2]")
		WebElement secondUserTeam;
		@FindBy(xpath = "(//div[contains(@class,'listSortEmail')])[3]")
		WebElement thirdUserTeam;
		@FindBy(xpath = "//input[contains(@value,'simultaneously')]")
		WebElement simultaneousButton;
		@FindBy(xpath = "//input[@value='fixedorder']")
		WebElement fixedOrder;
		@FindBy(xpath = "//input[@value='roundrobin']")
		WebElement roundRobin;
		@FindBy(xpath = "//button[@type='submit']")
		WebElement submitButton;
		@FindBy(xpath = "//input[contains(@class,'ant-input searchinttext')]")
		WebElement searchOptionInTeamPage;
		WebElement addButtoninIVR;
		@FindBy(xpath = "//input[@id='pressInputValueAdd']")
		WebElement enterValueInIvrField;
		@FindBy(xpath = "//div[@class='ivrsection']//p[contains(.,'Action:')]/following-sibling::div[1]")
	    WebElement openDropdownForIVR;
		@FindBy(xpath = "//div[@class='ivrsection']//p[contains(.,'Action:')]/following-sibling::div[2]")
	    WebElement openDropdownForNumUserTeam;
		@FindBy(xpath = "//label[contains(@class,'adminusercheckbox')]/span[contains(.,'Make it as an admin user')]")
	    WebElement adminUserCheckboc;
		
		@FindBy(xpath = "(//div[contains(.,'Select team')])[36]")
		WebElement teamListInIvr;
		@FindBy(xpath = "//li[@role='option'][contains(.,'testteam')]")
		WebElement selectTeamInIVR;
		@FindBy(xpath = "(//span[contains(.,'Please delete the IVR(s) associated with this team before deleting this team.')])[2]")
		WebElement validationMessageTeamInIVR;
		@FindBy(xpath = "//i[@class='material-icons g_reminder_remove_icon_style'][contains(@id,'close')][contains(.,'clear')]")
		WebElement closelowbalancepopup;
		
		@FindBy(xpath = "(//p[@class='mgright5'][contains(.,'Press')]/following-sibling::p[@class='ivrListDigit mgright5'][contains(.,'1')]/parent::div/parent::li/parent::ul/parent::div/following-sibling::div/div[@class='circlestmdl ivr_msg_savefn ivrdltbtn fleft'][contains(.,'delete_forever')]")
		WebElement deleteAssignedIVR;
		@FindBy(xpath = "//button[@type='button'][contains(.,'Yes')]")
		WebElement deleteUsers_popup_yesButton;
		@FindBy(xpath = "//div[@class='ivrsection']//p[contains(.,'Action:')]/../../following-sibling::div/div[contains(.,'save')]")
		WebElement clickOnIvrSaveBtn;
		@FindBy(xpath = "//div[contains(@test-automation,'num_team_allocation_tab')]")
		WebElement teamTab;
		
		@FindBy(xpath = "//div[@class='totalseattext']//i[normalize-space()='info']")
		WebElement seatiTag;
		
		@FindBy(xpath = "//button[@class='ant-btn circlest editbtn_chcirle totalseateditbtn']")
		WebElement seatPencilButton;
		@FindBy(xpath = "//label[@class='totalseatlabel']/following-sibling::input[@type='number']")
		WebElement seatCountTextBox;
		@FindBy(xpath = "//label[@class='totalseatlabel']/following-sibling::button[@class='ant-btn circlest crchsave totalseateditbtn']")
		WebElement seatSaveButton;
		@FindBy(xpath = "//div[@class='ant-message-custom-content ant-message-error']")
		WebElement errorMessage;
		
		@FindBy(xpath = "//tr[contains(@class,'ant-table-row ant-table-row-level')]")
		List<WebElement> number_on_numberpage;
		
		
	public String validateTitle() {
		Common.pause(2);
		wait.until(ExpectedConditions.titleContains("Callhippo.com"));
		return driver.getTitle();

	}
	
	public String verifySeatCount() throws IOException {
		wait.until(ExpectedConditions.visibilityOf(totalSeatCount));
		System.out.println("---totalSeatCount---"+totalSeatCount.getText());
		System.out.println("---totalSeatCount---"+totalSeatCount.getText().substring(totalSeatCount.getText().indexOf(":") + 2));
		return totalSeatCount.getText().substring(totalSeatCount.getText().indexOf(":") + 2);
		
	}
		public boolean validateUserCount(String userCount) {
		
		String value = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(@class,'totalseatrelative')]")))).getText();
		
		if(value.contains(userCount)) {
			return true;
		}else {
			return false;
		}
		
	}
	

	
	public int totalNumOnNumberPage() throws IOException {
		List<WebElement> number_on_numberpageElements = number_on_numberpage;
		return number_on_numberpageElements.size();	
	}
	
//	public static void main(String[] args) {
//	      int seat = 5;
//	      int i = 1;
//       
//           for (int i2 = i; i2 <= seat; i2++) {
//			
//			 if(seat>i) {
//				 System.out.println("free number added");
//				 System.out.println(i2<seat);
//	      }else {
//			 System.out.println("then paid");
//		  
//	}
//}
//
//}
	
	public void enterSeatCount(String seatCount) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", seatPencilButton);
		wait.until(ExpectedConditions.visibilityOf(seatPencilButton)).click();
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(seatCountTextBox));
		seatCountTextBox.clear();
		seatCountTextBox.sendKeys(seatCount);
		Common.pause(1);
		wait.until(ExpectedConditions.visibilityOf(seatSaveButton)).click();
	}
	
	public String validateErrormessage() {
		return wait.until(ExpectedConditions.visibilityOf(errorMessage)).getText();
	}
	
	public String getTotalSeatiTagValue() {
		return wait.until(ExpectedConditions.visibilityOf(seatiTag)).getAttribute("title");
	}
	public void clickOnYes() {
		try {
			wait.until(ExpectedConditions.visibilityOf(btn_yes));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click()", btn_yes);
			System.out.println("click by js");
		}catch (Exception e) {
			
		}
	}
		
		public void clickOnYesbutton() {
			try {
				wait2.until(ExpectedConditions.visibilityOf(btn_yes));
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click()", btn_yes);
				System.out.println("click by js");
			}catch (Exception e) {
				
			}
		
		
	}
	public boolean inviteUserButtonIsClickable() {
		try {
			Common.pause(3);
			wait.until(ExpectedConditions.visibilityOf(inviteUserBtn));
			wait.until(ExpectedConditions.elementToBeClickable(inviteUserBtn));
			if (inviteUserBtn.getCssValue("background-color").equalsIgnoreCase("rgba(24, 144, 255, 1)"))
			{
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public void clickOnInviteUserButton() {
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(inviteUserBtn));
			wait.until(ExpectedConditions.elementToBeClickable(inviteUserBtn));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", inviteUserBtn);
			js.executeScript("arguments[0].click();", inviteUserBtn);
	}
	
	public boolean verifyFieldsOnInviteUserPage() {
		boolean fieldsAreAvailable = false;
		Common.pause(2);
			
		if (inviteUserText.isDisplayed() && 
				email_label.isDisplayed() &&
				email_Textbox.isDisplayed() &&
				inviteUser_save_button.isDisplayed() &&
//				allocateNum_label.isDisplayed() &&
				inviteUser_num_checkbox.isDisplayed() &&
				invite1User_disable_button.isDisplayed() &&
				inviteUser_cancel_btn.isDisplayed()	)
		{
			fieldsAreAvailable = true;
		}else {
			fieldsAreAvailable = false;
		}
		return fieldsAreAvailable;
	}
	
	public void enterInviteUserEmail(String email) {
		email_Textbox.clear();
		email_Textbox.sendKeys(email);
	}
	
	public void enterInviteUserEmail2(String email) {
		email_Textbox2.sendKeys(email);
	}
	
	public void clickSaveButton() {
		inviteUser_save_button.click();
	}

	public void clickOnNUmber() {
		numberName.click();
	}

	public void clickOnTabTeam() {
		wait.until(ExpectedConditions.visibilityOf(teamTab));
		wait.until(ExpectedConditions.elementToBeClickable(teamTab));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", teamTab);
		
	}
	
	public void clickOnYesButtonOfUpdateSeatPopup() {
		wait.until(ExpectedConditions.visibilityOf(updateSeat_popup_yesButton)).click();
	}
	
	public void clickOnUpgradeButton() {
		Common.pause(2);
		wait.until(ExpectedConditions.visibilityOf(upgradeButton)).click();
	}
	
	public String validateErrorMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateErrorMsg));
		return validateErrorMsg.getText();
	}
	
	public String validateWarningMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateWarningMsg));
		return validateWarningMsg.getText();
	}
	
	public String validateSuccessMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateSuccessMsg));
		return validateSuccessMsg.getText();
	}
	
	public String validateInfoMessage() {
		wait.until(ExpectedConditions.visibilityOf(validateInfoMsg));
		return validateInfoMsg.getText();
	}
	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		System.out.println(validationMessage.getText());
		return validationMessage.getText();
	}
	public boolean verifyInviteUserButtonIsDisable() {
		boolean buttonIsDisable = false;
		Common.pause(2);
			
		if (invite1User_disable_button.isDisplayed())
		{
			buttonIsDisable = true;
		}else {
			buttonIsDisable = false;
		}
		return buttonIsDisable;
	}
	
	public void selectNumberCheckbox() {
		try {
			wait.until(ExpectedConditions.visibilityOf(inviteUser_num_checkbox));
			inviteUser_number_checkbox.click();
		}catch(Exception e){
			System.out.println("Number checkbox already selected.");
		}
	}
	
	public void selectAdminuserCheckbox() {
			wait.until(ExpectedConditions.elementToBeClickable(adminUserCheckboc));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click()", adminUserCheckboc);
   }
	
	
	
	public void selectNumberCheckbox(String number) {
//		try {
//			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@value='"+number+"']"))));
		driver.findElement(By.xpath("//input[@value='"+number+"']")).click();
//		}catch(Exception e){
//			System.out.println("Number checkbox already selected.");
//		}
		Common.pause(2);
	}
	
	public void clickInviteNUserButton() {
		wait.until(ExpectedConditions.elementToBeClickable(inviteNUser_button));
		inviteNUser_button.click();
		Common.pause(2);
	}
	
	public String getChargesOfPopupMessage() {
		String message = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Price : You will be charged $')]")))).getText();
		
		String price = message.substring(message.indexOf("$")+1,message.indexOf("as") - 1);
		
		return price;
	}
	
	public String getChargesOfPopupMessageInBillingPage() {
		String message = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'You will be charged $')]")))).getText();
		
		String price = message.substring(message.indexOf("$")+1,message.indexOf("as") - 1);
		
		return price;
	}
	
	public boolean validatePopupMessageWithCharge(String price) {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'Are you sure you want to add user?')]"))));
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public boolean validatePopupMessageWithChargeInBillingPage(String price) {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'You will be charged $"+price+" as per prorated charges. Please confirm to process the payment')]"))));
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public boolean validatePopupMessageOfUpdateSeat() {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[normalize-space()='Are you sure you want to update seat?']"))));
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(String price) {
		try {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[contains(.,'You will be charged $"+price+" as per prorated charges. Please confirm to process the payment.')]"))));
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public void clickYesBtnToConfirmInviteUser() {
		wait.until(ExpectedConditions.elementToBeClickable(inviteUser_confirm_yes_button));
		inviteUser_confirm_yes_button.click();
	}
	
	public boolean validateInviteUserMessageForInvalidEmail(String value) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'User " + value + " invalid , please enter  valid Email id')]"))))
				.isDisplayed();
	}
	
	public void clickCancelButton() {
		wait.until(ExpectedConditions.elementToBeClickable(inviteUser_cancel_btn));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", inviteUser_cancel_btn);
//		inviteUser_cancel_btn.click();
	}
	
	public void clickOnAddButton() {
		wait.until(ExpectedConditions.visibilityOf(addButton));
		wait.until(ExpectedConditions.elementToBeClickable(addButton)).click();
//		addButton.click();
	}

	public void enterSubUserFullName(String fullname) {
		fullName_textbox.sendKeys(fullname);
	}
	
	public void enterSubUserNewPassword(String newPassword) {
		newPassword_textbox.sendKeys(newPassword);
	}
	
	public void enterSubUserConfirmPassword(String confirmPassword) {
		confirmPassword_textbox.sendKeys(confirmPassword);
	}
	
	public void clickOnSubUserSubmitButton() {
		wait.until(ExpectedConditions.elementToBeClickable(subUser_submitButton)).click();
	}
	
	public String validateAccountActivatedMessage() {
		wait.until(ExpectedConditions.visibilityOf(activatedMessage));
		return activatedMessage.getText();
	}
	
	public void scrollToCancelButtonVisible() {
		wait.until(ExpectedConditions.visibilityOf(inviteUser_cancel_btn));
//		js.executeScript("arguments[0].scrollIntoView();", inviteUser_cancel_btn);
		Actions actions = new Actions(driver);
		actions.moveToElement(inviteUser_cancel_btn);
		actions.perform();
	}
	
	public boolean verifyUserInvitedSuccessfully(String value) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//td[contains(.,'"+value+"')]//following-sibling::td/a[contains(.,'Active')]"))))
				.isDisplayed();
	}
	public boolean verifyUserInvitedInIvrNumberIsDisplayed(String value) {
		
		List<WebElement> inviteuser= driver.findElements(By.xpath(
				"//div[contains(@class,'chaligncenter')]//div[text()='"+value+"']"));
		if (inviteuser.size()>0) {
			return true;
		} else {
			return false;
		}
		
		
	}
	public boolean verifyTeamInvitedInIvrNumberIsDisplayed(String value) {
		
		List<WebElement> inviteTeam= driver.findElements(By.xpath(
				"//div[contains(@class,'allocateduserbox')]//span[text()='"+value+"']"));
		if (inviteTeam.size()>0) {
			return true;
		} else {
			return false;
		}
		
		
	}
	
	public void deleteInvitedSubUser(String value) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//td[contains(.,'"+value+"')]//following-sibling::td[contains(.,'Active')]//following-sibling::td//span/i[contains(.,'delete_forever')]"))));
		WebElement deleteUser = driver.findElement(By.xpath(
				"//td[contains(.,'"+value+"')]//following-sibling::td[contains(.,'Active')]//following-sibling::td//span/i[contains(.,'delete_forever')]"));
		deleteUser.click();
//		jse.executeScript("arguments[0].click()", deleteUser);
	}
	
	public void deleteInvitedPendingSubUser(String value) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//td[contains(.,'"+value+"')]//following-sibling::td[contains(.,'Pending')]//following-sibling::td//span/i[contains(.,'delete_forever')]"))));
		WebElement deleteUser = driver.findElement(By.xpath(
				"//td[contains(.,'"+value+"')]//following-sibling::td[contains(.,'Pending')]//following-sibling::td//span/i[contains(.,'delete_forever')]"));
		deleteUser.click();
//		jse.executeScript("arguments[0].click()", deleteUser);
	}
	
	public boolean verifyDeleteUserPopup() {
		boolean popupdisplay;
		wait.until(ExpectedConditions.visibilityOf(deleteUser_popupHeader));
		if(deleteUser_popupHeader.isDisplayed() 
				&& deleteUser_confirmMessage.isDisplayed()) {
			popupdisplay = true;
		}else {
			popupdisplay = false;
		}
		return popupdisplay;
	}
	
	public void clickOnDeleteUserYesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(deleteUser_popup_yesButton)).click();
	}
	public void clickOnDeleteUserNoButton() {
		wait.until(ExpectedConditions.elementToBeClickable(deleteUser_popup_noButton)).click();
	}
	
	public void scrollUptoNextPageButtonVisible() {
		wait.until(ExpectedConditions.visibilityOf(nextPage_button));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", nextPage_button);
	}
	
	public void clickOnSettingIconOf(String invitedUserEmail) {
		String path = "//a[contains(text(),'"+invitedUserEmail+"')]/..//following-sibling::td//a[contains(@class,'settingcircle')]";
		driver.findElement(By.xpath(path));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		driver.findElement(By.xpath(path)).click();
	}
	
	public boolean validateInvitedUserInSettingsPage(String value) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//div[@class='deparmentname_title']/span[contains(.,'"+value+"')]"))))
				.isDisplayed();
	}
	
	public boolean valditaionMessageforReinvitedUser(String value) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//div[@class[contains(.,'ant-message-error')]]/span[contains(.,'User "+value+" Already Exists')]"))))
				.isDisplayed();
	}
	
	public boolean valditaionMessageToReSentInvitationMail(String value) {
		return wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//div[@class[contains(.,'ant-message-success')]]/span[contains(.,'An invitation email has been sent to "+value+"')]"))))
				.isDisplayed();
	}
	
	public void clickEmailIcon(String value) {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//td[contains(.,'"+value+"')]//following-sibling::td[contains(.,'Pending')]//following-sibling::td//span/i[contains(.,'email')]"))))
				.click();
	}
	public void clickOnYesBtnForInviteUser() {
		wait.until(ExpectedConditions.visibilityOf(YesBtnForInviteUser)).click();
	}	
	public void clickOnCreateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createTeam));
		createTeam.click();
		
	}	
	public void clickOnSubmitButton() {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.click();
		
	}	
		
public String enterTeamName(String teamName) {
			wait.until(ExpectedConditions.visibilityOf(this.teamName));
			Common.pause(3);
			wait.until(ExpectedConditions.visibilityOf(this.teamName)).clear();
			Common.pause(2);
			wait.until(ExpectedConditions.visibilityOf(this.teamName)).sendKeys(teamName);
			return teamName;
		}
public void clickOnDeleteTeamButton() {
	wait.until(ExpectedConditions.elementToBeClickable(deleteTeam));
	deleteTeam.click();
	Common.pause(2);
}
public void clickOnDeleteTeamYesButton() {
			wait.until(ExpectedConditions.elementToBeClickable(yesButton));
			yesButton.click();
			Common.pause(2);
		}

public void clickOnFirstUserTeam() {
	wait.until(ExpectedConditions.elementToBeClickable(firstUserTeam));
	firstUserTeam.click();
	Common.pause(2);
}
public void clickOnSecondUserTeam() {
	wait.until(ExpectedConditions.elementToBeClickable(secondUserTeam));
	secondUserTeam.click();
	Common.pause(2);
}
public void clickOnThirdUserTeam() {
	wait.until(ExpectedConditions.elementToBeClickable(thirdUserTeam));
	thirdUserTeam.click();
	Common.pause(2);		
}	
public void clickOnSimultaneously() {
	Common.pause(3);
	simultaneousButton.click();
}
public void clickOnFixedOrder() {
	Common.pause(3);
	fixedOrder.click();
}
public void clickOnRoundRobin() {
	Common.pause(3);
	roundRobin.click();
}


public String validateWhenDeleteTeamWhichIsAssignedInIVRTeamNameV() {
	return wait.until(ExpectedConditions.visibilityOf(validationMessageTeamInIVR)).getText();
}

public void enterTeamNameInSearchBox(String searchOptionInTeamPage) {
	wait.until(ExpectedConditions.visibilityOf(this.searchOptionInTeamPage));
	Common.pause(3);
	wait.until(ExpectedConditions.visibilityOf(this.searchOptionInTeamPage)).clear();
	Common.pause(2);
	wait.until(ExpectedConditions.visibilityOf(this.searchOptionInTeamPage)).sendKeys(searchOptionInTeamPage);
}

public void clickOnAddButtonIVR() {
	wait.until(ExpectedConditions.elementToBeClickable(addButtoninIVR));
	addButtoninIVR.click();
	Common.pause(2);
}
public void enterValueInIvr(String enterValueInIvrField) {
	wait.until(ExpectedConditions.visibilityOf(this.enterValueInIvrField));
	Common.pause(3);
	wait.until(ExpectedConditions.visibilityOf(this.enterValueInIvrField)).clear();
	Common.pause(2);
	wait.until(ExpectedConditions.visibilityOf(this.enterValueInIvrField)).sendKeys(enterValueInIvrField);
//
}		
public void clickOnDropdownInIVR(String numUserTeam) {
	wait.until(ExpectedConditions.visibilityOf(openDropdownForIVR));
	openDropdownForIVR.click();
	wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+numUserTeam+"')]"))))
			.click();
	Common.pause(2);	
}		
public void clickOnDeleteBtnOfnumUserTeam(String numUserTeam) {
	wait.until(ExpectedConditions.visibilityOf(openDropdownForIVR));
	openDropdownForIVR.click();
	wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath(""
					+ "(//p//div[contains(.,'Team')])/../../../../../following-sibling::div//i[contains(.,'delete_forever')]")))).click();
		
	Common.pause(2);	
}		
public void clickOnDropdownNumUserTeam(String numUserTeam) {
	wait.until(ExpectedConditions.visibilityOf(openDropdownForNumUserTeam));
	openDropdownForNumUserTeam.click();
	wait.until(ExpectedConditions.visibilityOf(
			driver.findElement(By.xpath("//li[@role='option'][contains(.,'"+numUserTeam+"')]"))))
			.click();
	Common.pause(2);	
}		

public void viewTeamListInIVR() {
	wait.until(ExpectedConditions.elementToBeClickable(teamListInIvr));
	teamListInIvr.click();
	Common.pause(2);
	//selectTeamInIVR
}
public void addTeamInIVRDropdown() {
	wait.until(ExpectedConditions.elementToBeClickable(selectTeamInIVR));
	selectTeamInIVR.click();
	Common.pause(2);
	//saveButtonInIvr
}

public void ClickonDeltebuttonofNumUserTeam(String numTeamUser) {
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
			+ "(//p//div[contains(.,'"+numTeamUser+"')])/../../../../../following-sibling::div//i[contains(.,'delete_forever')]")))).click();
	Common.pause(2);
	clickOnYesButton();
	}
public boolean validateDeletedUser(String userName) {
	try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
			+ "//span[contains(@class,'alctboxfnsize ng-binding')][contains(.,'" + userName + "')]/parent::div"))));
	return false;
	}
	catch (Exception e) {
		return true;
	}
	}

public void clickOnYesButton() {
	wait.until(ExpectedConditions.elementToBeClickable(deleteUser_popup_yesButton)).click();
}
public void SaveIvrOption() {
	wait.until(ExpectedConditions.elementToBeClickable(clickOnIvrSaveBtn)).click();
}
public String validateDeletUserPopupDescription() {
	wait.until(ExpectedConditions.visibilityOf(deleteUserPopupMessage));
	return deleteUserPopupMessage.getText();
}	

public String verifyMembersCountInTeam(String TeamName) {
	String count = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(""
			+ "//tr[contains(@class,'ant-table-row')]/child::td//a[contains(.,'"+TeamName+"')]/parent::td/following-sibling::td[1]/child::a")))).getText();
	
	Common.pause(2);
	return count;
}
public void DeleteSpecificTeam(String Teamname) {
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
			"//tr[contains(@class,'ant-table-row')]/child::td//a[contains(.,'"+Teamname+"')]/parent::td/following-sibling::td/following-sibling::td/child::div/child::a//following-sibling::span")))).click();
	Common.pause(2);
	clickOnYesButton();
}
public void closeLowcreditNotification() {
	try {
    wait.until(ExpectedConditions.visibilityOf(closelowbalancepopup));
	closelowbalancepopup.click();
	}
	catch (Exception e) {
	}
}
public void numberIVR(String value) {

	wait.until(ExpectedConditions.visibilityOf(numberIVR));

	if (value.contentEquals("on")) {

		if (numberIVR.getAttribute("aria-checked").equals("true")) {

		} else {
			numberIVR.click();
		}
	} else {
		if (numberIVR.getAttribute("aria-checked").equals("true")) {
			numberIVR.click();
		} else {

		}
	}

}
	public void navigateToNumberSettingpage(String number) {

		WebElement numberSetting = driver.findElement(By.xpath("//a[contains(text(),'" + number + "')]"));
		wait.until(ExpectedConditions.visibilityOf(numberSetting));
		numberSetting.click();
		int i = 5000;
		while (deptNameAndNumberTxt.isDisplayed() == false) {
			int j = i + 5000;
			try {
				Thread.sleep(j);
				numberSetting.click();
			} catch (InterruptedException e) {
			}

			if (j == 60000) {
				break;
			}
		}
	}
	
	public boolean addUser_popup_while_only_number_checkbox_selected() {
		boolean element_available=false;
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(.,'Adding a user')]/../following-sibling::div/p[1]"))));
		String actual_msg1 = driver.findElement(By.xpath("//div[contains(.,'Adding a user')]/../following-sibling::div/p[1]")).getText();
		//String actual_msg2 = driver.findElement(By.xpath("//div[contains(.,'Adding a user')]/../following-sibling::div/p[2]")).getText();
		System.out.println("---actual_msg1--"+actual_msg1);
		//System.out.println("---actual_msg2--"+actual_msg2);
		String expected_msg1 = "Are you sure you want to add user?";
		//String expected_msg2 = "Price : You will be charged $19.97 as per prorated charges. Please confirm to process the payment";
		
		if(expected_msg1.equalsIgnoreCase(expected_msg1)) {
			return element_available=true;
		}else {
			return element_available=false;
		}
	}
	
}
