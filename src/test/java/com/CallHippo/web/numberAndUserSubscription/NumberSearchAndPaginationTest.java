package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class NumberSearchAndPaginationTest {

	WebDriver driver;
	
	AddNumberPage addNumberPage;
	SignupPage registrationPage;
	LoginPage loginpage;
	NumberSearchAndPaginationPage numberSearchAndPaginationPage;
	
	Common signupExcel;
	PropertiesFile url;
	XLSReader planPrice;
	XLSReader numberPrice;
	PropertiesFile data;

	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;

	String newEmail = null;
	String gmailUser;
	String getNumber;
	String locationName;
	String locationCode;

	public NumberSearchAndPaginationTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signupExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		numberPrice = new XLSReader("Data\\numberprices.xlsx");

		gmailUser = data.getValue("gmailUser");
	}

	@BeforeTest
	public void initialization() throws Exception {
		driver = TestBase.init();
		addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
		registrationPage = PageFactory.initElements(driver, SignupPage.class);
		loginpage = PageFactory.initElements(driver, LoginPage.class);
		numberSearchAndPaginationPage = PageFactory.initElements(driver, NumberSearchAndPaginationPage.class);

		webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
		inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);

		driver.get(url.signIn());
	}
	
	@BeforeMethod
	public void beforeMethod() throws IOException {
		driver.navigate().to(url.numbersPage());
	}

	@AfterMethod
	public void afterMethodProcess(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		
	}
	
	@AfterTest (alwaysRun = true) 
	public void tearDown() {
	driver.quit();
	}

	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}
	
	private String signup() throws Exception {
		try {
			driver.get(url.livesignUp());
		} catch (IOException e) {
		}
		String date = signupExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		newEmail = email;
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signupExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signupExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signupExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signupExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signupExcel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
		registrationPage.ValidateVerifyButtonOnGmail();

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		System.out.println(email);
		return email;
	}

	
	public void addFirstNumberForUser() throws Exception {
		String email = signup();
		
//		loginpage.enterEmail("jayadip+17_02_2021_15_15_48@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Number");
		addNumberPage.enterPrefixOrNumber("14");

		assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains("14"));
		
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"),"2"));

		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
	}
	
	public void addMultipleNumbersForUser(Integer counter, String prefixNum) throws Exception {
		Common.pause(3);
		driver.get(url.numbersPage());
		for (int i=1 ; i<=counter ; i++) {
			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			addNumberPage.selectNumberTypeByText("local");
			addNumberPage.selectSearchByText("Number");
			addNumberPage.enterPrefixOrNumber(prefixNum);

			assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains(prefixNum));
			
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesFreeNumberPopup();
			//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
			Common.pause(3);
		}
		
		Common.pause(3);
		driver.get(url.numbersPage());
	}
	
	
	
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void addNumberForSearchResult() throws Exception {
		loginpage.enterEmail("automation+09_03_2021_19_08_08@callhippo.com");
//		loginpage.enterEmail("jayadip+25_02_2021_17_01_57@callhippo.com");
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
//		addFirstNumberForUser();
//		addMultipleNumbersForUser(25,"124");
//		addMultipleNumbersForUser(25,"14");
//		addMultipleNumbersForUser(25,"15");
//		addMultipleNumbersForUser(25,"17");
//		
//		addNumberPage.clickOnAddNumberInNumbersPage();
//		addNumberPage.clickOnCountry("United Kingdom");
//
//		addNumberPage.selectNumberTypeByText("tollfree");
//		addNumberPage.selectSearchByText("Number");
//		addNumberPage.enterPrefixOrNumber("123");
//
//		assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains("123"));
//		
//		addNumberPage.clickOnFirstNumberOfNumberListPage();
//		addNumberPage.clickOnAddButtonOfFreeNumberPopup();
//		Common.pause(3);
	}
	

	@Test(priority = 2, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_special_character_notAllowed_in_search() throws Exception {
//		loginpage.enterEmail("automation+09_03_2021_19_08_08@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
//		Common.pause(3);
//		driver.get(url.numbersPage());
		
		Common.pause(3);
		driver.navigate().to(url.numbersPage());
		Common.pause(3);
		addNumberPage.waitForAddNumberInNumbersPage();
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("~");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("`");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("!");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("@");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("#");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("$");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("%");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("^");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("&");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("*");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("(");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(")");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("{");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("}");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("[");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("]");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("|");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("\\");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(":");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(";");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("'");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("<");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(",");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(">");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("?");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult("/");
		assertTrue(numberSearchAndPaginationPage.verifyNoDataTxtPresent());
	}
	
	@Test(priority = 3, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_number_search_with_specific_number_string() throws Exception {
		
		Common.pause(3);
		driver.navigate().to(url.numbersPage());
		Common.pause(5);
		addNumberPage.waitForAddNumberInNumbersPage();
		String randomNumberFromRecords = numberSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(numberSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = numberSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,3,6);
		System.out.println("subStringFromNumber"+subStringFromNumber);
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 4, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_number_search_with_plus() throws Exception {
		addNumberPage.waitForAddNumberInNumbersPage();
		String randomNumberFromRecords = numberSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(numberSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = numberSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,3,6)+"+";
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 5, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_number_search_with_minus() throws Exception {
		addNumberPage.waitForAddNumberInNumbersPage();
		String randomNumberFromRecords = numberSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(numberSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = numberSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,3,6)+"-";
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 6, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_number_search_with_multiply() throws Exception {
		addNumberPage.waitForAddNumberInNumbersPage();
		String randomNumberFromRecords = numberSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(numberSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = numberSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,3,6)+"*";
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 7, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_number_search_with_divide() throws Exception {
		addNumberPage.waitForAddNumberInNumbersPage();
		String randomNumberFromRecords = numberSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(numberSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = numberSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,3,6)+"/";
		
		numberSearchAndPaginationPage.verifyNumberStringInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 8, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_special_character_notAllowed_in_departmentName() throws Exception {
		
		Common.pause(3);
		driver.get(url.numbersPage());
		addNumberPage.waitForAddNumberInNumbersPage();
		numberSearchAndPaginationPage.clickOnDepartmentName();
		numberSearchAndPaginationPage.clickDepartmentNameEditIcon();
		numberSearchAndPaginationPage.insert_departmentName("~");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("`");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("!");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("@");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("#");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("$");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("%");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("^");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("&");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("*");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("(");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName(")");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("+");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("=");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("{");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("}");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("[");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("]");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("|");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("\\");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("'");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName(";");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName(":");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("/");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("?");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName(".");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName(">");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName(",");
		numberSearchAndPaginationPage.validate_departmentName();
		
		numberSearchAndPaginationPage.insert_departmentName("<");
		numberSearchAndPaginationPage.validate_departmentName();
	}
	
	@Test(priority = 9, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void validate_departmentName_on_numberSearch() throws Exception {
//		loginpage.enterEmail("jayadip+24_02_2021_17_58_29@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
		Common.pause(3);
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.clickOnDepartmentName();
		numberSearchAndPaginationPage.clickDepartmentNameEditIcon();
		numberSearchAndPaginationPage.insert_departmentName("United_States");
		numberSearchAndPaginationPage.save_departmentName();
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(2);
		numberSearchAndPaginationPage.verifyDepartmentNameInSearchResult("United_States");
		
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.clickOnDepartmentName();
		numberSearchAndPaginationPage.clickDepartmentNameEditIcon();
		numberSearchAndPaginationPage.insert_departmentName("United States_");
		numberSearchAndPaginationPage.save_departmentName();
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(2);
		numberSearchAndPaginationPage.verifyDepartmentNameInSearchResult("United States_");
		
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.clickOnDepartmentName();
		numberSearchAndPaginationPage.clickDepartmentNameEditIcon();
		numberSearchAndPaginationPage.insert_departmentName("United_States_");
		numberSearchAndPaginationPage.save_departmentName();
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(2);
		numberSearchAndPaginationPage.verifyDepartmentNameInSearchResult("United_States_");
		
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.clickOnDepartmentName();
		numberSearchAndPaginationPage.clickDepartmentNameEditIcon();
		numberSearchAndPaginationPage.insert_departmentName(" _United States");
		numberSearchAndPaginationPage.save_departmentName();
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(2);
		numberSearchAndPaginationPage.verifyDepartmentNameInSearchResult("_United States");
		
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.clickOnDepartmentName();
		numberSearchAndPaginationPage.clickDepartmentNameEditIcon();
		numberSearchAndPaginationPage.insert_departmentName("United-States");
		numberSearchAndPaginationPage.save_departmentName();
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(2);
		numberSearchAndPaginationPage.verifyDepartmentNameInSearchResult("United-States");
	}
	
	@Test(priority = 10, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void verifyPreviousPageIsDisble_1st_time_On_NumbersPage() throws Exception {
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(3);
		assertTrue(numberSearchAndPaginationPage.verifyPreviousBtnPaginationDisable());
	}
	
	@Test(priority = 11, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void verifyNextPageBtn_enable_for_more_records() throws Exception {
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.verifyNextPageEnable();
	}
	
	@Test(priority = 12, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void verifyNextPageBtn_disable_lastPageOfPagination() throws Exception {
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.verifyNextPageDisableAtlastPageOfPagination();
	}
	
	@Test(priority = 13, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void verify_activePageInPagination() throws Exception {
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(3);
		numberSearchAndPaginationPage.verifyActivePageInPagination();
	}
	
	@Test(priority = 14, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void delete_number_from_lastPage_check_pagenation() throws Exception {
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(3);
		Integer lastpage = numberSearchAndPaginationPage.getPaginationSize();
		Common.pause(2);
		numberSearchAndPaginationPage.clickOnlastPageOfPagination();
		
		if (lastpage == 11)
		{
			Integer totalrow = numberSearchAndPaginationPage.getTotalRows();
			for(int i=0;i<totalrow;i++) {
				numberSearchAndPaginationPage.deleteNumbers();
			}
		}
		if (lastpage == 10)
		{
			Integer totalrow1 = numberSearchAndPaginationPage.getTotalRows();
			if (totalrow1<10) {
				for(int i=0;i<(10-totalrow1);i++) {
					addMultipleNumbersForUser(1,"14");
				}
			}
		}
		
		addMultipleNumbersForUser(1,"14");
		numberSearchAndPaginationPage.verifyPaginationAfterNumberDeleteFromLastPage();
	}
	
	@Test(priority = 15, dependsOnMethods = "addNumberForSearchResult", retryAnalyzer = Retry.class)
	public void delete_number_from_2ndLastPage_check_pagenation() throws Exception {
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(3);
		addMultipleNumbersForUser(1,"14");
		numberSearchAndPaginationPage.verifyPaginationAfterNumberDeleteFromLastPage();
	}
	
}
