package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class SearchNumberWhilePurchaseTest {

	WebDriver driver;

	AddNumberPage addNumberPage;
	SignupPage registrationPage;
	LoginPage loginpage;

	Common signupExcel;
	PropertiesFile url;
	PropertiesFile data;

	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;

	String newEmail = null;
	String gmailUser;
	String getNumber;

	public SearchNumberWhilePurchaseTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signupExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");

		gmailUser = data.getValue("gmailUser");
	}

//	@BeforeTest
//	public void initialization() throws Exception {
//		
//	}

	@BeforeMethod
	public void beforeMethod() throws IOException {
		driver = TestBase.init();
		addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
		registrationPage = PageFactory.initElements(driver, SignupPage.class);
		loginpage = PageFactory.initElements(driver, LoginPage.class);

		webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
		inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);

		driver.get(url.signIn());
		driver.navigate().to(url.numbersPage());
	}

	@AfterMethod
	public void afterMethodProcess(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driver.quit();

	}

//	@AfterTest(alwaysRun = true)
//	public void tearDown() {
//		
//	}

	private String signup() throws Exception {
		try {
			driver.get(url.livesignUp());
		} catch (IOException e) {
		}
		String date = signupExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		newEmail = email;
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signupExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signupExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signupExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signupExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signupExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
		registrationPage.ValidateVerifyButtonOnGmail();

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		System.out.println(email);
		return email;
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_search_By_number_search() throws Exception {
		String email = signup();

//		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Number");

		String numberText = addNumberPage.getNumberBetween(2, 10);

		addNumberPage.enterNumberInNumberSearchBox(numberText);

		addNumberPage.assertNumberInAllNumbersOfList(numberText);

		String numberText1 = addNumberPage.getNumberBetween(2, 5);

		addNumberPage.enterNumberInNumberSearchBox(numberText1);

		addNumberPage.assertNumberInAllNumbersOfList(numberText1);

		String numberText2 = addNumberPage.getNumberBetween(5, 9);

		addNumberPage.enterNumberInNumberSearchBox(numberText2);

		addNumberPage.assertNumberInAllNumbersOfList(numberText2);

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_search_By_number_prefix() throws Exception {
		String email = signup();

//		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Prefix");

		String numberText = addNumberPage.getNumberBetween(2, 5);

		addNumberPage.enterPrefixInNumberSearchBox(numberText);

		addNumberPage.assertNumberInAllNumbersOfList(numberText);

		String numberText1 = addNumberPage.getNumberBetween(2, 4);

		addNumberPage.enterPrefixInNumberSearchBox(numberText1);

		addNumberPage.assertNumberInAllNumbersOfList(numberText1);

		String numberText2 = addNumberPage.getNumberBetween(2, 8);

		addNumberPage.enterPrefixInNumberSearchBox(numberText2);

		addNumberPage.assertNumberInAllNumbersOfList(numberText2);

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_search_By_Location_code() throws Exception {
		String email = signup();

//		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Location");

		String locationName = addNumberPage.returnLocationName();
		System.out.println(locationName);
		String locationCode = addNumberPage.returnLocationCode();
		System.out.println(locationCode);

//		addNumberPage.clickOnLocationDropdown();
		addNumberPage.enterLocationInNumberSearchBox(locationCode);
		addNumberPage.assertLocationInAllLocationsInDropdown(locationCode);

//		addNumberPage.enterPrefixInNumberSearchBox(locationCode);

		addNumberPage.assertNumberInAllNumbersOfList(locationCode);

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_search_By_Location_City_Name() throws Exception {
		String email = signup();

//		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Location");

		String locationName = addNumberPage.returnLocationName();
		System.out.println(locationName);
		String locationCode = addNumberPage.returnLocationCode();
		System.out.println(locationCode);

//		addNumberPage.clickOnLocationDropdown();
		addNumberPage.enterLocationInNumberSearchBox(locationName);
		addNumberPage.assertLocationInAllLocationsInDropdown(locationName);

//		addNumberPage.enterPrefixInNumberSearchBox(locationCode);

		addNumberPage.assertNumberInAllNumbersOfList(locationCode);

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_search_By_number_search_number_Not_available() throws Exception {
		String email = signup();

//		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Number");

		addNumberPage.enterNumberInNumberSearchBox("0000000000");

		assertTrue(addNumberPage.validateNumberNotAvailableMessageIsDisplay());

		assertFalse(addNumberPage.validateNextButton());

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_search_By_number_prefix_Number_Not_available() throws Exception {
		String email = signup();

//		loginpage.enterEmail("automation+13_05_2021_13_47_44@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Prefix");

		addNumberPage.enterPrefixInNumberSearchBox("00000");

		assertTrue(addNumberPage.validateNumberNotAvailableMessageIsDisplay());

		assertFalse(addNumberPage.validateNextButton());

	}

}
