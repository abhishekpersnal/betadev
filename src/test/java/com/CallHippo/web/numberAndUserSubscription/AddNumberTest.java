package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class AddNumberTest {

	AddNumberPage addNumberPage;
	SignupPage registrationPage;
	SignupPage dashboard;
	LoginPage loginpage;
	WebDriver driver;
	Common signupExcel;
	PropertiesFile url;
	XLSReader planPrice;
    XLSReader numberPrice;
	PropertiesFile data;
	WebDriver driverMail;
	RestAPI planPriceAPI;
	RestAPI numberPriceAPI;

	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;

	String newEmail = null;
	String gmailUser;
	String getNumber;
	String locationName;
	String locationCode;
	String email2 = null;
	String email3 = null;
	String planPriceBronzeMonthly,planPriceSilverMonthly,planPricePlatinumMonthly;
	String planPriceBronzeAnnually,planPriceSilverAnnually,planPricePlatinumAnnually;
	
	String numberPriceUSMonthly,numberPriceUSAnnually;
	String numberPriceAustraliaMonthly,numberPriceAustraliaAnnually;
	String numberPriceCanadaMonthly,numberPriceCanadaAnnually;
	String numberPriceIsraelMonthly,numberPriceIsraelAnnually;

	public AddNumberTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signupExcel = new Common(data.getValue("environment") + "\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
	    numberPrice = new XLSReader("Data\\numberprices.xlsx");
		gmailUser = data.getValue("gmailUser");
		planPriceAPI= new RestAPI();
		numberPriceAPI= new RestAPI();
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			
            planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
			
			planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
		
			planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
			
			planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
			
			planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
		
			planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
			
			//--------------------Number Price ---------------------------------------
			numberPriceUSMonthly=numberPriceAPI.getNumberPrice("United States", "fixed", "monthly");
			
			//numberPriceUSAnnually=numberPriceAPI.getNumberPrice("United States", "fixed", "annually");
			
			numberPriceCanadaMonthly=numberPriceAPI.getNumberPrice("Canada", "fixed", "monthly");
			
			//numberPriceCanadaAnnually=numberPriceAPI.getNumberPrice("Canada", "fixed", "annually");
			
            numberPriceAustraliaMonthly=numberPriceAPI.getNumberPrice("Australia", "tollfree", "monthly");
			
			//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Australia", "tollfree", "annually");
			
			numberPriceIsraelMonthly=numberPriceAPI.getNumberPrice("Israel", "fixed", "monthly");
			
			//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Israel", "fixed", "annually");
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
		
			try {
				dashboard.deleteAllMail();
			} catch (Exception e) {
				dashboard.deleteAllMail();
			}
			
		
		} catch (Exception e) {
			 planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
				
				planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
			
				planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
				
				planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
				
				planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
			
				planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
				
				//--------------------Number Price ---------------------------------------
				numberPriceUSMonthly=numberPriceAPI.getNumberPrice("United States", "fixed", "monthly");
				
				//numberPriceUSAnnually=numberPriceAPI.getNumberPrice("United States", "fixed", "annually");
				
				numberPriceCanadaMonthly=numberPriceAPI.getNumberPrice("Canada", "fixed", "monthly");
				
				//numberPriceCanadaAnnually=numberPriceAPI.getNumberPrice("Canada", "fixed", "annually");
				
	            numberPriceAustraliaMonthly=numberPriceAPI.getNumberPrice("Australia", "tollfree", "monthly");
				
				//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Australia", "tollfree", "annually");
				
				numberPriceIsraelMonthly=numberPriceAPI.getNumberPrice("Israel", "fixed", "monthly");
				
				//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Israel", "fixed", "annually");
			e.printStackTrace();
			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}

	@BeforeMethod
	public void initialization() throws Exception {
		driver = TestBase.init();
		addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
		registrationPage = PageFactory.initElements(driver, SignupPage.class);
		dashboard = PageFactory.initElements(driver, SignupPage.class);
		loginpage = PageFactory.initElements(driver, LoginPage.class);

		webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
		inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);

		driver.get(url.signIn());
	}
	

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
			Common.errorLogs(result.getThrowable());
		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
	  driver.quit();

	}

	@Test(retryAnalyzer = Retry.class)
	public String signup() throws Exception {
		try {
			driver.get(url.livesignUp());
		} catch (Exception e) {
			driver.get(url.livesignUp());
		}
		System.out.println("----Welcome to signup");

		String date = signupExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		newEmail=email;
		System.out.println(email);
        Common.pause(2);
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signupExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signupExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signupExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signupExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signupExcel.getdata(0, 27, 2));
		registrationPage.clickOn_website_signUp_popUp_signUpButton();
		Common.pause(2);
		assertEquals(registrationPage.getTextMainTitle(), "Thank you for Signing up.");

		registrationPage.ValidateVerifyButtonOnGmail();
		dashboard.verifyWelcomeToCallHippoPopup();
		dashboard.closeAddnumberPopup();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		System.out.println(email);
		return email;
	}

	@Test(retryAnalyzer = Retry.class)
	public void signup1() throws Exception {
		signup();
	}

	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void Verify_LoginSuccessfully() {
		loginpage.enterEmail(signupExcel.getdata(0, 4, 2));
		System.out.println(signupExcel.getdata(0, 4, 2));
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		String actualResult = loginpage.loginSuccessfully();
		String expectedResult = "Dashboard | Callhippo.com";
		assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 2, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void validate_Number_List() throws Exception {
		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 3, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void verify_search_By_number_prefix() throws Exception {
		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Number");
		addNumberPage.enterPrefixOrNumber("12");

		assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains("12"));

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 4, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void verify_search_By_location() throws Exception {
		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Location");
		locationName = addNumberPage.returnLocationName();
		System.out.println(locationName);
		locationCode = addNumberPage.returnLocationCode();
		System.out.println(locationCode);
		// addNumberPage.enterPrefixOrNumber("12");

		assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains(locationCode));

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_bronze_Monthly() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number
		Common.pause(1);
		driver.get(url.numbersPage());
		Common.pause(7);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		//assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		Common.pause(2);
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		// assertEquals(priceAfterCheckout,
		// addNumberPage.multiplicationOfTwoStringNumber(planPrice.getField("monthly",
		// "select * from Sheet1 where plan='Bronze'"),"2")); //storeseatcount
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount"))); 

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_bronze_Annually() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Silver_Monthly() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);

		// addNumberPage.selectSilverPlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPriceSilverMonthly);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Silver_Annually() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));

		// addNumberPage.selectSilverPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
        Common.pause(2);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();   
		Common.pause(2);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Platinum_Monthly() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Platinum_Annually() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));

		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Premium_Number_with_bronze_Monthly() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().contains("Bronze"));
		// -----------------------
		String numberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		assertEquals(numberPriceAfterCheckout, numberPriceAustraliaMonthly);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")),
				numberPriceAustraliaMonthly);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.closeAddProofPopup();
		Common.pause(5);
		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly,
						data.getValue("seatcount")),
				numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);
		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Premium_Number_with_Silver_Monthly() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout, planPriceSilverMonthly);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().toLowerCase().contains("silver"));
		// -----------------------
		String numberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		assertEquals(numberPriceAfterCheckout,numberPriceAustraliaMonthly);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverMonthly, numberPriceAustraliaMonthly);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.closeAddProofPopup();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverMonthly, numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Premium_Number_with_Platinum_Monthly() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);

		addNumberPage.selectPlatinumPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout, planPricePlatinumMonthly);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().toLowerCase().contains("platinum"));
		// -----------------------
		String numberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		assertEquals(numberPriceAfterCheckout, numberPriceAustraliaMonthly);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumMonthly,numberPriceAustraliaMonthly);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.closeAddProofPopup();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumMonthly, numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Premium_Number_with_bronze_Annually() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().contains("Bronze"));
		// -----------------------
		String actualNumberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		String expectedNumberPriceAfterCheckout = addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12");
		assertEquals(actualNumberPriceAfterCheckout, expectedNumberPriceAfterCheckout);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")),
				expectedNumberPriceAfterCheckout);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.closeAddProofPopup();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage
				.additionOfTwoStringNumber(addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")), expectedNumberPriceAfterCheckout);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);
		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Premium_Number_with_Silver_Annually() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");
		addNumberPage.clickOnCloseButtonOfchat();
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout, planPriceSilverAnnually);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().toLowerCase().contains("silver"));
		// -----------------------
		String actualNumberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		String expectedNumberPriceAfterCheckout = addNumberPage.multiplicationOfTwoStringNumber(
				 numberPriceAustraliaMonthly, "12");
		assertEquals(actualNumberPriceAfterCheckout, expectedNumberPriceAfterCheckout);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverAnnually,
				expectedNumberPriceAfterCheckout);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.closeAddProofPopup();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(
				addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverAnnually,
				expectedNumberPriceAfterCheckout);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);
		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Premium_Number_with_Platinum_Annually() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));

		addNumberPage.selectPlatinumPlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout, planPricePlatinumAnnually);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().toLowerCase().contains("platinum"));
		// -----------------------
		String actualNumberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		String expectedNumberPriceAfterCheckout = addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12");
		assertEquals(actualNumberPriceAfterCheckout, expectedNumberPriceAfterCheckout);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumAnnually,
				expectedNumberPriceAfterCheckout);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.closeAddProofPopup();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumAnnually,
				expectedNumberPriceAfterCheckout);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);

	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void verify_Free_number_popup_in_Bronze_monthly_plan() throws Exception {
		// Verify_Add_First_Number_with_bronze_Monthly();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number
		Common.pause(1);
		driver.get(url.numbersPage());
		Common.pause(7);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		Common.pause(2);
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		// assertEquals(priceAfterCheckout,
		// addNumberPage.multiplicationOfTwoStringNumber(planPrice.getField("monthly",
		// "select * from Sheet1 where plan='Bronze'"),"2")); //storeseatcount
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount"))); 

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		System.out.println("Start test...");

		driver.get(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCount = inviteUserPageObj.totalNumOnNumberPage();
		int seatcount1 = Integer.parseInt(seatCountAfterUpdateandInviteUser);
		if (numCount < seatcount1) {
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
//		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription()
					.contains("× " + seatCountAfterUpdateandInviteUser));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPriceBronzeMonthly,
							data.getValue("seatcount")));

		} else {

			for (seatcount1 = 1; seatcount1 <= numCount; seatcount1++) {

				if (numCount == seatcount1) {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date2 = signupExcel.date();
					String email2 = gmailUser + "+" + date2 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email2);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					try {
						assertTrue(
								inviteUserPageObj.validatePopupMessageWithCharge(
										planPriceBronzeMonthly),
								"validate charge while invite user. (popup message)");
					} catch (AssertionError e) {
						assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
								planPriceBronzeMonthly);
					}
					inviteUserPageObj.clickOnYes();
					String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterAdd4thUser, String.valueOf(seatcount1 + 1),
							"validate seat count after invite 2nd sub user");
					String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
					assertEquals(seatiTagValue2,
							"The charges will be calculated based on the total seats.");
				} else {
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date3 = signupExcel.date();
					String email3 = gmailUser + "+" + date3 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email3);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
					addNumberPage.openAccountDetailsPopup();
					assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
					String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
					assertEquals(actualPriceFromManageSubscription3,
							addNumberPage.multiplicationOfTwoStringNumber(
									planPriceBronzeMonthly,
									String.valueOf(seatcount1 + 1)));
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterUpdateandInvite2ndUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterUpdateandInvite2ndUser, String.valueOf(seatcount1 + 1),
							"validate seat count after update seat count and invite user");
				}

			}
			driver.get(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterInviteUser = inviteUserPageObj.verifySeatCount();
			System.out.println(" seat count after invite user:" + seatCountAfterInviteUser);
			System.out.println(" seat count :" + seatcount1);

			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

			addNumberPage.clickOnFirstNumberOfNumberListPage();

			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),
					"----validate free number pop up for bronze monthly.");

			// addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
			// addNumberPage.clickOnSaveNumberButton();

			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
			// assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("�
			// 2"));

			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPriceBronzeMonthly,
							String.valueOf(seatcount1)));

		}

		// -----------------validate number count After added one free number ---------
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCountafteradded = inviteUserPageObj.totalNumOnNumberPage();
		System.out.println("------numberCountafteradded::" + numCountafteradded);
		assertEquals(numCountafteradded, numCount + 1, "----verify number count.");
	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void verify_Free_number_popup_in_Bronze_annually_plan() throws Exception {
		// Verify_Add_First_Number_with_bronze_Annually();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		System.out.println("Start test...");

		driver.get(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCount = inviteUserPageObj.totalNumOnNumberPage();
		int seatcount1 = Integer.parseInt(seatCountAfterUpdateandInviteUser);

		if (numCount < seatcount1) {
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
//			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription()
					.contains("× " + seatCountAfterUpdateandInviteUser));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPriceBronzeAnnually,
							data.getValue("seatcount")));

		} else {

			for (seatcount1 = 1; seatcount1 <= numCount; seatcount1++) {

				if (numCount == seatcount1) {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date3 = signupExcel.date();
					String email2 = gmailUser + "+" + date3 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email2);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					try {
						assertTrue(
								inviteUserPageObj.validatePopupMessageWithCharge(
										planPriceBronzeAnnually),
								"validate charge while invite user. (popup message)");
					} catch (AssertionError e) {

						assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
								planPriceBronzeAnnually);
					}

					inviteUserPageObj.clickOnYes();
					String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterAdd4thUser, String.valueOf(seatcount1 + 1),
							"validate seat count after invite 3rd sub user");
					String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
					assertEquals(seatiTagValue2,
							"The charges will be calculated based on the total seats.");

				} else {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date4 = signupExcel.date();
					String email4 = gmailUser + "+" + date4 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email4);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
					addNumberPage.openAccountDetailsPopup();
					assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
					String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
					assertEquals(actualPriceFromManageSubscription3,
							addNumberPage.multiplicationOfTwoStringNumber(
									planPriceBronzeAnnually,
									String.valueOf(seatcount1 + 1)));
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(seatcount1 + 1),
							"validate seat count after update seat count and invite user");

				}

			}

			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

			addNumberPage.clickOnFirstNumberOfNumberListPage();

			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),
					"----validate free number pop up for bronze annually.");

			// addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
			// addNumberPage.clickOnSaveNumberButton();

			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
			// assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("×
			// 2"));

			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPriceBronzeAnnually,
							String.valueOf(seatcount1)));

		}

		// -----------------validate number count After added one free number ---------
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCountafteradded = inviteUserPageObj.totalNumOnNumberPage();
		System.out.println("------numberCountafteradded::" + numCountafteradded);
		assertEquals(numCountafteradded, numCount + 1, "----verify number count.");

	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void verify_Free_number_popup_in_Silver_monthly_plan() throws Exception {
		// Verify_Add_First_Number_with_Silver_Monthly();
		// driver.get(url.signIn());

		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);

		// addNumberPage.selectSilverPlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPriceSilverMonthly);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");


		System.out.println("Start test...");

		driver.get(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCount = inviteUserPageObj.totalNumOnNumberPage();
		int seatcount1 = Integer.parseInt(seatCountAfterUpdateandInviteUser);

		if (numCount < seatcount1) {
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
           //assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription()
					.contains("× " + seatCountAfterUpdateandInviteUser));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPriceSilverAnnually,
							data.getValue("seatcount")));

		} else {

			for (seatcount1 = 1; seatcount1 <= numCount; seatcount1++) {

				if (numCount == seatcount1) {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date3 = signupExcel.date();
					String email3 = gmailUser + "+" + date3 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email3);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					try {
						assertTrue(
								inviteUserPageObj.validatePopupMessageWithCharge(
										planPriceSilverMonthly),
								"validate charge while invite user. (popup message)");
					} catch (AssertionError e) {

						assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
								planPriceSilverMonthly);
					}
					inviteUserPageObj.clickOnYes();
					String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterAdd4thUser, String.valueOf(seatcount1 + 1),
							"validate seat count after invite 3rd sub user");
					String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
					assertEquals(seatiTagValue2,
							"The charges will be calculated based on the total seats.");
				} else {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date4 = signupExcel.date();
					String email4 = gmailUser + "+" + date4 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email4);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
					addNumberPage.openAccountDetailsPopup();
					assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
					String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
					assertEquals(actualPriceFromManageSubscription3,
							addNumberPage.multiplicationOfTwoStringNumber(
									planPriceSilverMonthly,
									String.valueOf(seatcount1 + 1)));
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(seatcount1 + 1),
							"validate seat count after update seat count and invite user");

				}


			}

			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

			addNumberPage.clickOnFirstNumberOfNumberListPage();

			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly(),
					"----validate free number pop up for Silver monthly.");

			// addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
			// addNumberPage.clickOnSaveNumberButton();

			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			// assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("×
			// 2"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPriceSilverMonthly,
							String.valueOf(seatcount1)));	

		}

//-----------------validate number count After added one free number ---------
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCountafteradded = inviteUserPageObj.totalNumOnNumberPage();
		System.out.println("------numberCountafteradded::" + numCountafteradded);
		assertEquals(numCountafteradded, numCount + 1, "----verify number count.");


	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void verify_Free_number_popup_in_Silver_annually_plan() throws Exception {
		//Verify_Add_First_Number_with_Silver_Annually();
		
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));

		// addNumberPage.selectSilverPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
        Common.pause(3);
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		


		System.out.println("Start test...");

		driver.get(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCount = inviteUserPageObj.totalNumOnNumberPage();
		int seatcount1 = Integer.parseInt(seatCountAfterUpdateandInviteUser);

		if (numCount < seatcount1) {
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly(),"----validate free number pop up for silver annually.");
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
//			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription()
					.contains("× " + seatCountAfterUpdateandInviteUser));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverAnnually, String.valueOf(seatcount1 + 1)));

		} else {

			for (seatcount1 = 1; seatcount1 <= numCount; seatcount1++) {

				if (numCount == seatcount1) {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date3 = signupExcel.date();
					String email1 = gmailUser + "+" + date3 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email1);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					try {
						assertTrue(
								inviteUserPageObj.validatePopupMessageWithCharge(
										planPriceSilverAnnually),
								"validate charge while invite user. (popup message)");
					} catch (AssertionError e) {

						assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
								planPriceSilverAnnually);
					}

					inviteUserPageObj.clickOnYes();
					String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterAdd4thUser, String.valueOf(seatcount1 + 1), "validate seat count after invite 3rd sub user");
					String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
					assertEquals(seatiTagValue2,
							"The charges will be calculated based on the total seats.");
					
				}else {
					
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date4 = signupExcel.date();
					String email4 = gmailUser + "+" + date4 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email4);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
					addNumberPage.openAccountDetailsPopup();
					assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
					String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
					assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
							planPriceSilverAnnually, String.valueOf(seatcount1 + 1)));
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(seatcount1 + 1),
							"validate seat count after update seat count and invite user");
					
				}
			}

		
			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

			addNumberPage.clickOnFirstNumberOfNumberListPage();

			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly(),
					"----validate free number pop up for silver annually.");

			// addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
			// addNumberPage.clickOnSaveNumberButton();

			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			// assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("×
			// 2"));

			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverAnnually, String.valueOf(seatcount1)));
		

		}

		// -----------------validate number count After added one free number ---------
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCountafteradded = inviteUserPageObj.totalNumOnNumberPage();
		System.out.println("------numberCountafteradded::" + numCountafteradded);
		assertEquals(numCountafteradded, numCount + 1, "----verify number count.");
		

	}

	@Test(priority = 21, retryAnalyzer = Retry.class) //new added
	public void verify_Free_number_popup_in_Platinum_monthly_plan() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		
		System.out.println("Start test...");

		driver.get(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCount = inviteUserPageObj.totalNumOnNumberPage();
		int seatcount1 = Integer.parseInt(seatCountAfterUpdateandInviteUser);

		if (numCount < seatcount1) {
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly(),"----validate free number pop up for platinum Monthly.");
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
//			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));
			addNumberPage.openAccountDetailsPopup();
			
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumMonthly, String.valueOf(seatcount1 + 1)));
			
			
		} else {

			for (seatcount1 = 1; seatcount1 <= numCount; seatcount1++) {

				if (numCount == seatcount1) {
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date3 = signupExcel.date();
					String email1 = gmailUser + "+" + date3 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email1);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					try {
						assertTrue(
								inviteUserPageObj.validatePopupMessageWithCharge(
										planPricePlatinumMonthly),
								"validate charge while invite user. (popup message)");
					} catch (AssertionError e) {

						assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
								planPricePlatinumMonthly);
					}

					inviteUserPageObj.clickOnYes();
					String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterAdd4thUser, String.valueOf(seatcount1 + 1), "validate seat count after invite 2nd sub user");
					String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
					assertEquals(seatiTagValue2,
							"The charges will be calculated based on the total seats.");
					
				}else {
					
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date4 = signupExcel.date();
					String email4 = gmailUser + "+" + date4 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email4);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
					addNumberPage.openAccountDetailsPopup();
					assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
					String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
					assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
							planPricePlatinumMonthly, String.valueOf(seatcount1 + 1)));
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(seatcount1 + 1),
							"validate seat count after update seat count and invite user");
					
				}
			}

		
			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

			addNumberPage.clickOnFirstNumberOfNumberListPage();

			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly(),"----validate free number pop up for platinum monthly.");
			

			// addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
			// addNumberPage.clickOnSaveNumberButton();

			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

			addNumberPage.openAccountDetailsPopup();
			
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumMonthly, String.valueOf(seatcount1)));
			

		}

		// -----------------validate number count After added one free number ---------
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCountafteradded = inviteUserPageObj.totalNumOnNumberPage();
		System.out.println("------numberCountafteradded::" + numCountafteradded);
		assertEquals(numCountafteradded, numCount + 1, "----verify number count.");
		

	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void verify_Free_number_popup_in_Platinum_annually_plan() throws Exception {
	//	Verify_Add_First_Number_with_Platinum_Annually();
	
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));

		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

	
		System.out.println("Start test...");

		driver.get(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCount = inviteUserPageObj.totalNumOnNumberPage();
		int seatcount1 = Integer.parseInt(seatCountAfterUpdateandInviteUser);

		if (numCount < seatcount1) {
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly(),"----validate free number pop up for platinum Annually.");
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
//			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));
			addNumberPage.openAccountDetailsPopup();
			
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumAnnually,
							String.valueOf(seatcount1 + 1)));
			
		} else {

			for (seatcount1 = 1; seatcount1 <= numCount; seatcount1++) {

				if (numCount == seatcount1) {
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date3 = signupExcel.date();
					String email1 = gmailUser + "+" + date3 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email1);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					try {
						assertTrue(
								inviteUserPageObj.validatePopupMessageWithCharge(
										planPricePlatinumAnnually),
								"validate charge while invite user. (popup message)");
					} catch (AssertionError e) {

						assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
								planPricePlatinumAnnually);
					}
					inviteUserPageObj.clickOnYes();
					String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterAdd4thUser, String.valueOf(seatcount1 + 1),
							"validate seat count after invite 3rd sub user");
					String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
					assertEquals(seatiTagValue2,
							"The charges will be calculated based on the total seats.");
				} else {

					driver.navigate().to(url.signIn());
					webAppMainUserLogin.userspage();
					inviteUserPageObj.clickOnInviteUserButton();
					String date4 = signupExcel.date();
					String email4 = gmailUser + "+" + date4 + "@callhippo.com";
					inviteUserPageObj.enterInviteUserEmail(email4);
					inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
					inviteUserPageObj.clickInviteNUserButton();
					String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
					String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
					assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
					addNumberPage.openAccountDetailsPopup();
					assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
					String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
					assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
							planPricePlatinumAnnually,
									String.valueOf(seatcount1 + 1)));
					driver.navigate().to(url.signIn());
					webAppMainUserLogin.clickLeftMenuPlanAndBilling();
					String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
					assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(seatcount1 + 1),
							"validate seat count after update seat count and invite user");

				}
			}

		
			driver.get(url.numbersPage());
			addNumberPage.clickOnAddNumberInNumbersPage();
			addNumberPage.clickOnCountry("United States");

			String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

			addNumberPage.clickOnFirstNumberOfNumberListPage();

			assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly(),"----validate free number pop up for platinum Annually.");
			

			// addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
			addNumberPage.clickOnNoButtonOfFreeNumberPopup();
			addNumberPage.clickOnFirstNumberOfNumberListPage();
			Common.pause(2);
			addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
			// addNumberPage.clickOnSaveNumberButton();

			assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

			addNumberPage.openAccountDetailsPopup();
			
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));

			String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription1,
					addNumberPage.multiplicationOfTwoStringNumber(
							planPricePlatinumAnnually,
							String.valueOf(seatcount1)));

		}

		// -----------------validate number count After added one free number ---------
		driver.get(url.numbersPage());
		Common.pause(5);
		int numCountafteradded = inviteUserPageObj.totalNumOnNumberPage();
		System.out.println("------numberCountafteradded::" + numCountafteradded);
		assertEquals(numCountafteradded, numCount + 1, "----verify number count.");


	}

	@Test(priority = 23, retryAnalyzer = Retry.class)
	public void Bronze_monthly_plan_2_users_2_standard_numbers_Delete_1_user() throws Exception {
	
		
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		//1 invite user
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signupExcel.date();
				email2 = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		
		//Add free number
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPriceBronzeMonthly,
				planPriceBronzeMonthly);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);

		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email2);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
        //as sheet count remains two so number charge will remains it is. Number subscription will not display
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
				planPriceBronzeMonthly,
				planPriceBronzeMonthly);
		assertEquals(actualPriceFromManageSubscription2, expectedPriceFromManageSubscription1);

	}

	@Test(priority = 24, retryAnalyzer = Retry.class)
	public void Silver_monthly_plan_2_users_2_standard_numbers_Delete_1_user() throws Exception {

		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		//1 invite user
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signupExcel.date();
				email2 = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		
		//Add free number
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverMonthly,
				planPriceSilverMonthly);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);

		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email2);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), "2"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
        //as sheet count remains two so number charge will remains it is. Number subscription will not display
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverMonthly,
				planPriceSilverMonthly);
		assertEquals(actualPriceFromManageSubscription2, expectedPriceFromManageSubscription1);


	}
	
	@Test(priority = 25, retryAnalyzer = Retry.class)
	public void Platinum_monthly_plan_2_users_2_standard_numbers_Delete_1_user() throws Exception {

		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		//1 invite user
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signupExcel.date();
				email2 = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		
		//Add free number
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumMonthly,
				planPricePlatinumMonthly);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);

		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email2);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), "2"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
        //as sheet count remains two so number charge will remains it is. Number subscription will not display
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumMonthly,
				planPricePlatinumMonthly);
		assertEquals(actualPriceFromManageSubscription2, expectedPriceFromManageSubscription1);


	}

	@Test(priority = 26, retryAnalyzer = Retry.class)
	public void Silver_Annually_plan_2_users_2_standard_numbers_Delete_1_user() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		//1 invite user
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverAnnually);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signupExcel.date();
				email2 = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		
		//Add free number
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverAnnually,
				planPriceSilverAnnually);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);

		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email2);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), seatCountAfterUpdateandInviteUser));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
        //as sheet count remains two so number charge will remains it is. Number subscription will not display
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
				planPriceSilverAnnually,
				planPriceSilverAnnually);
		assertEquals(actualPriceFromManageSubscription2, expectedPriceFromManageSubscription1);

	}

	@Test(priority = 27, retryAnalyzer = Retry.class)
     public void Platinum_Annually_plan_2_users_2_standard_numbers_Delete_1_user() throws Exception {
		
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		//1 invite user
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumAnnually);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signupExcel.date();
				email2 = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		
		//Add free number
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumAnnually,
				planPricePlatinumAnnually);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);

		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email2);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), seatCountAfterUpdateandInviteUser));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
        //as sheet count remains two so number charge will remains it is. Number subscription will not display
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
				planPricePlatinumAnnually,
				planPricePlatinumAnnually);
		assertEquals(actualPriceFromManageSubscription2, expectedPriceFromManageSubscription1);
		


	}

	@Test(priority = 28, retryAnalyzer = Retry.class)
	public void Bronze_Annually_plan_2_users_2_standard_numbers_Delete_1_user() throws Exception {
		
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		
		//1 invite user
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeAnnually);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signupExcel.date();
				email2 = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		
		//Add free number
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				planPriceBronzeAnnually,
				planPriceBronzeAnnually);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);

		
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email2);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();

		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"), seatCountAfterUpdateandInviteUser));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
        //as sheet count remains two so number charge will remains it is. Number subscription will not display
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
				planPriceBronzeAnnually,
				planPriceBronzeAnnually);
		assertEquals(actualPriceFromManageSubscription2, expectedPriceFromManageSubscription1);
		

	}

	@Test(priority = 29, retryAnalyzer = Retry.class)
	public void bronze_monthly_plan_1_users_2_standard_numbers_Add_1_user() throws Exception {
		//Verify_Add_First_Number_with_bronze_Monthly();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number
		Common.pause(1);
		driver.get(url.numbersPage());
		Common.pause(7);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		Common.pause(3);
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		Common.pause(2);
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount"))); 
																											

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		System.out.println("Seat based start...");
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();

				// put validation of chargebee subscription
				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscription1);
				String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeMonthly, numberPriceUSMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription1);  // 1 user + 1 num

			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signupExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
			}

		}
		i=i-1; 
		System.out.println("Invite user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
		assertFalse(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"),"-----Validate virtual number");
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		
		String expectedPriceFromManageSubscription2 =addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 1));
		assertEquals(actualPriceFromManageSubscription2,expectedPriceFromManageSubscription2,"--validate user charge."); // 2user +0 num
		
		System.out.println("Number1 adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		
//		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
//				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String expectedPriceFromManageSubscriptionNumber1=addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeMonthly, String.valueOf(i + 1)), 
						 numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscriptionNumber1, expectedPriceFromManageSubscriptionNumber1,"--validate user charge.");
		
		System.out.println("Number2 adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Canada");

		String numberBeforePurchasedNumber3 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2);
		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscriptionNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionNumber1,numberPriceCanadaMonthly);
		assertEquals(actualPriceFromManageSubscriptionNumber2, expectedPriceFromManageSubscriptionNumber2,"--validate user charge.");
		
		driver.get(url.signIn());
		Common.pause(3);
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		System.out.println("Adding user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
        Common.pause(2);
		assertEquals(inviteUserPageObj.validateTitle(), "Users | Callhippo.com");

		String date1 = signupExcel.date();
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email2);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchasedNumber3);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");

		Common.pause(2);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
	
		String actualPriceFromManageSubscriptionuser2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
       System.out.println("Last price:"+actualPriceFromManageSubscriptionuser2);
        Common.pause(2);
		assertEquals(addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber(), addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly, String.valueOf(i + 2)),  numberPriceCanadaMonthly),"--validate user charge.");

	}

	@Test(priority = 30, retryAnalyzer = Retry.class)
	public void silver_monthly_plan_1_users_2_standard_numbers_Add_1_user() throws Exception {
		//Verify_Add_First_Number_with_Silver_Monthly();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);

		// addNumberPage.selectSilverPlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPriceSilverMonthly);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
	
		System.out.println("Seat based start...");
		
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();

				// put validation of chargebee subscription
				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscription1);
				String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverMonthly, numberPriceUSMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription1);  // 1 user + 1 num

			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signupExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
			}

		}
		i=i-1; 
		System.out.println("Invite user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
		assertFalse(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"),"-----Validate virtual number");
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		
		String expectedPriceFromManageSubscription2 =addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 1));
		assertEquals(actualPriceFromManageSubscription2,expectedPriceFromManageSubscription2,"--validate user charge."); // 2user +0 num
		
		System.out.println("Number1 adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2);
		// put validation of chargebee subscription
		driver.get(url.signIn());
		driver.navigate().refresh();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
	
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String expectedPriceFromManageSubscriptionNumber1=addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(planPriceSilverMonthly, String.valueOf(i + 1)), numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscriptionNumber1, expectedPriceFromManageSubscriptionNumber1,"--validate user charge.");
		
		System.out.println("Number2 adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Canada");

		String numberBeforePurchasedNumber3 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		
		//assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscriptionNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionNumber1, 
						 numberPriceCanadaMonthly);
		assertEquals(actualPriceFromManageSubscriptionNumber2, expectedPriceFromManageSubscriptionNumber2,"--validate user charge.");
		
		driver.get(url.signIn());
		Common.pause(3);
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		System.out.println("Adding user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
        Common.pause(2);
		assertEquals(inviteUserPageObj.validateTitle(), "Users | Callhippo.com");

		String date1 = signupExcel.date();
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email2);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchasedNumber2);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");

		Common.pause(2);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
	
		String actualPriceFromManageSubscriptionuser2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
       System.out.println("Last price:"+actualPriceFromManageSubscriptionuser2);
        Common.pause(2);
		assertEquals(addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber(), addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverMonthly, String.valueOf(i + 2)), numberPriceCanadaMonthly),"--validate user charge.");

	}

	
	@Test(priority = 31, retryAnalyzer = Retry.class)
	public void platinum_monthly_plan_1_users_2_standard_numbers_Add_1_user() throws Exception {
		//Verify_Add_First_Number_with_Silver_Monthly();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		System.out.println("Seat based start...");
		
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();

				// put validation of chargebee subscription
				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("platinum"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscription1);
				String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
						planPricePlatinumMonthly, numberPriceUSMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription1);  // 1 user + 1 num

			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signupExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
			}

		}
		i=i-1; 
		System.out.println("Invite user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("platinum"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
		assertFalse(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"),"-----Validate virtual number");
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		
		String expectedPriceFromManageSubscription2 =addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 1));
		assertEquals(actualPriceFromManageSubscription2,expectedPriceFromManageSubscription2,"--validate user charge."); // 2user +0 num
		
		System.out.println("Number1 adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2);
		// put validation of chargebee subscription
		driver.get(url.signIn());
		driver.navigate().refresh();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String expectedPriceFromManageSubscriptionNumber1=addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(planPricePlatinumMonthly, String.valueOf(i + 1)),  numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscriptionNumber1, expectedPriceFromManageSubscriptionNumber1,"--validate user charge.");
		
		System.out.println("Number2 adding...");
		driver.get(url.numbersPage());
		Common.pause(3);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Canada");

		String numberBeforePurchasedNumber3 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscriptionNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionNumber1,numberPriceCanadaMonthly);
		assertEquals(actualPriceFromManageSubscriptionNumber2, expectedPriceFromManageSubscriptionNumber2,"--validate user charge.");
		
		driver.get(url.signIn());
		Common.pause(3);
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		System.out.println("Adding user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
        Common.pause(2);
		assertEquals(inviteUserPageObj.validateTitle(), "Users | Callhippo.com");

		String date1 = signupExcel.date();
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email2);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchasedNumber2);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");

		Common.pause(2);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("platinum"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
	
		String actualPriceFromManageSubscriptionuser2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
       System.out.println("Last price:"+actualPriceFromManageSubscriptionuser2);
        Common.pause(2);
		assertEquals(addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber(), addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly, String.valueOf(i + 2)), 
					numberPriceCanadaMonthly),"--validate user charge.");
	
	}
		
		
	@Test(priority = 32, retryAnalyzer = Retry.class)
	public void bronze_Annually_plan_1_users_2_standard_numbers_Add_1_user() throws Exception {
		//Verify_Add_First_Number_with_bronze_Annually();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
	
		
		System.out.println("Seat based start...");
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze Annually.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1);
				// put validation of chargebee subscription
				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscription1);
				
				String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(numberPriceUSMonthly, "12");
				String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeAnnually, annuallyPrice);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription1);  // 1 user + 1 num

			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signupExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
			}

		}
		i=i-1; 
		System.out.println("Invite user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
		assertFalse(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"),"-----Validate virtual number");
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		
		String expectedPriceFromManageSubscription2 =addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, String.valueOf(i + 1));
		assertEquals(actualPriceFromManageSubscription2,expectedPriceFromManageSubscription2,"--validate user charge."); // 2user +0 num
		
		System.out.println("Number1 adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String annuallyPrice1 = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		String expectedPriceFromManageSubscriptionNumber1=addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeAnnually, String.valueOf(i + 1)), annuallyPrice1);
		assertEquals(actualPriceFromManageSubscriptionNumber1, expectedPriceFromManageSubscriptionNumber1,"--validate user charge.");
		
		System.out.println("Number2 adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Canada");

		String numberBeforePurchasedNumber3 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze Annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String annuallyPrice2 = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceCanadaMonthly, "12");
		String expectedPriceFromManageSubscriptionNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionNumber1, annuallyPrice2);
		assertEquals(actualPriceFromManageSubscriptionNumber2, expectedPriceFromManageSubscriptionNumber2,"--validate user charge.");
		
		driver.get(url.signIn());
		Common.pause(3);
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		System.out.println("Adding user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
        Common.pause(2);
		assertEquals(inviteUserPageObj.validateTitle(), "Users | Callhippo.com");

		String date1 = signupExcel.date();
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email2);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchasedNumber3);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");

		Common.pause(2);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
	
		String actualPriceFromManageSubscriptionuser2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
       System.out.println("Last price:"+actualPriceFromManageSubscriptionuser2);
        Common.pause(2);
		assertEquals(addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber(), addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually, String.valueOf(i + 2)), annuallyPrice2),"--validate user charge.");

	}

	@Test(priority = 33, retryAnalyzer = Retry.class)
	public void silver_Annually_plan_1_users_2_standard_numbers_Add_1_user() throws Exception {
		//Verify_Add_First_Number_with_Silver_Annually();
		
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));

		// addNumberPage.selectSilverPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		System.out.println("Seat based start...");
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver Annually.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1);
				// put validation of chargebee subscription
				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscription1);
				
				String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
						numberPriceUSMonthly, "12");
				String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverAnnually, annuallyPrice);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription1);  // 1 user + 1 num

			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signupExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().toLowerCase().contains("Silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
			}

		}
		i=i-1; 
		System.out.println("Invite user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
		assertFalse(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"),"-----Validate virtual number");
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		
		String expectedPriceFromManageSubscription2 =addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, String.valueOf(i + 1));
		assertEquals(actualPriceFromManageSubscription2,expectedPriceFromManageSubscription2,"--validate user charge."); // 2user +0 num
		
		System.out.println("Number1 adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		driver.navigate().refresh();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"),String.valueOf(i + 1)));


		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String annuallyPrice1 = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		String expectedPriceFromManageSubscriptionNumber1=addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(planPriceSilverAnnually, String.valueOf(i + 1)), annuallyPrice1);
		assertEquals(actualPriceFromManageSubscriptionNumber1, expectedPriceFromManageSubscriptionNumber1,"--validate user charge.");
		
		System.out.println("Number2 adding...");
		driver.get(url.numbersPage());
		Common.pause(3);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Canada");

		String numberBeforePurchasedNumber3 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver Annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		driver.navigate().refresh();
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
//		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
//				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
		
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String annuallyPrice2 = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceCanadaMonthly, "12");
		String expectedPriceFromManageSubscriptionNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionNumber1, annuallyPrice2);
		assertEquals(actualPriceFromManageSubscriptionNumber2, expectedPriceFromManageSubscriptionNumber2,"--validate user charge.");
		
		driver.get(url.signIn());
		Common.pause(3);
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		System.out.println("Adding user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
        Common.pause(2);
		assertEquals(inviteUserPageObj.validateTitle(), "Users | Callhippo.com");

		String date1 = signupExcel.date();
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email2);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchasedNumber3);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");

		Common.pause(2);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
	
		String actualPriceFromManageSubscriptionuser2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
       System.out.println("Last price:"+actualPriceFromManageSubscriptionuser2);
        Common.pause(2);
		assertEquals(addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber(), addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually, String.valueOf(i + 2)), annuallyPrice2),"--validate user charge.");

	}

	@Test(priority = 34, retryAnalyzer = Retry.class)
	public void platinum_Annually_plan_1_users_2_standard_numbers_Add_1_user() throws Exception {
		//Verify_Add_First_Number_with_Platinum_Annually();
		
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));

		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		
		System.out.println("Seat based start...");
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum Annually.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1);
				// put validation of chargebee subscription
				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscription1);
				
				String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
						numberPriceUSMonthly, "12");
				String expectedPriceFromManageSubscription1 = addNumberPage.additionOfTwoStringNumber(
						planPricePlatinumAnnually, annuallyPrice);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription1);  // 1 user + 1 num

			} else {

				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signupExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually,
								String.valueOf(i + 1)));
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
			}

		}
		i=i-1; 
		System.out.println("Invite user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---subUserMail---" + email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
		assertFalse(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"),"-----Validate virtual number");
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		
		String expectedPriceFromManageSubscription2 =addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, String.valueOf(i + 1));
		assertEquals(actualPriceFromManageSubscription2,expectedPriceFromManageSubscription2,"--validate user charge."); // 2user +0 num
		
		System.out.println("Number1 adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		driver.navigate().refresh();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"),String.valueOf(i + 1)));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String annuallyPrice1 = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		String expectedPriceFromManageSubscriptionNumber1=addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(planPricePlatinumAnnually, String.valueOf(i + 1)), annuallyPrice1);
		assertEquals(actualPriceFromManageSubscriptionNumber1, expectedPriceFromManageSubscriptionNumber1,"--validate user charge.");
		
		System.out.println("Number2 adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Canada");

		String numberBeforePurchasedNumber3 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum Annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();

		// put validation of chargebee subscription
		driver.get(url.signIn());
		driver.navigate().refresh();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		
		
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"),String.valueOf(i + 1)));

//		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
//				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String annuallyPrice2 = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceCanadaMonthly, "12");
		String expectedPriceFromManageSubscriptionNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionNumber1, annuallyPrice2);
		assertEquals(actualPriceFromManageSubscriptionNumber2, expectedPriceFromManageSubscriptionNumber2,"--validate user charge.");
		
		driver.get(url.signIn());
		Common.pause(3);
		logout(webAppMainUserLogin);
		Common.pause(2);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		Common.pause(2);
		System.out.println("Adding user...");
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
        Common.pause(2);
		assertEquals(inviteUserPageObj.validateTitle(), "Users | Callhippo.com");

		String date1 = signupExcel.date();
		String email2 = gmailUser + "+" + date1 + "@callhippo.com";
		System.out.println("---subUserMail---" + email2);
		inviteUserPageObj.enterInviteUserEmail(email2);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchasedNumber3);
		inviteUserPageObj.clickInviteNUserButton();
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");

		Common.pause(2);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		//assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× 2"));
	
		String actualPriceFromManageSubscriptionuser2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
       System.out.println("Last price:"+actualPriceFromManageSubscriptionuser2);
        Common.pause(2);
		assertEquals(addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber(), addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually, String.valueOf(i + 2)), annuallyPrice2),"--validate user charge.");



	}

	@Test(priority = 35, retryAnalyzer = Retry.class)
	public void add_premium_number_while_Bronze_monthly_plan_is_running_2_users_2_standard_numbers() throws Exception {
		
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceBronzeMonthly),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceBronzeMonthly);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceBronzeMonthly,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeMonthly,
						planPriceBronzeMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

               //Add premium number
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Australia");

				addNumberPage.selectNumberTypeByText("tollfree");

				String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
				getNumber = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				webAppMainUserLogin.closeLowcreditNotification();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				Common.pause(10);
				addNumberPage.closeAddProofPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
						actualPriceFromManageSubscription1),numberPriceAustraliaMonthly);
				assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);
				//Invite 1User
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(PremiumrBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd3rdUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd3rdUser, String.valueOf(seatAfterUpdateandInviteUser + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription4 = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceBronzeMonthly);		
				assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscription4);
				
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String freeNumPurchased = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(freeNumPurchased);
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceBronzeMonthly);		
				assertEquals(actualPriceFromManageSubscriptionAfterAddfreeNumber, expectedPriceFromManageSubscriptionAfterAddfreeNumber);
							
	}

	@Test(priority = 36, retryAnalyzer = Retry.class)
	public void add_premium_number_while_Silver_monthly_plan_is_running_2_users_2_standard_numbers() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased);
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceSilverMonthly),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceSilverMonthly);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceSilverMonthly,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverMonthly,
						planPriceSilverMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), seatCountAfterUpdateandInviteUser));

               //Add premium number
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Australia");

				addNumberPage.selectNumberTypeByText("tollfree");

				String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
				getNumber = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				webAppMainUserLogin.closeLowcreditNotification();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				Common.pause(10);
				addNumberPage.closeAddProofPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
						actualPriceFromManageSubscription1),numberPriceAustraliaMonthly);
				assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);
				//Invite 1User
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(PremiumrBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd3rdUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd3rdUser, String.valueOf(seatAfterUpdateandInviteUser + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription4 = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceSilverMonthly);		
				assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscription4);
				
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String freeSTDNumPurchased = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(freeSTDNumPurchased);
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceSilverMonthly);		
				assertEquals(actualPriceFromManageSubscriptionAfterAddfreeNumber, expectedPriceFromManageSubscriptionAfterAddfreeNumber);

	}
	
	@Test(priority = 37, retryAnalyzer = Retry.class)
	public void add_premium_number_while_Platinum_monthly_plan_is_running_2_users_2_standard_numbers() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased);
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPricePlatinumMonthly),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPricePlatinumMonthly);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPricePlatinumMonthly,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				
				System.out.println("Outside executed");
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,seatCountAfterUpdateandInviteUser);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), seatCountAfterUpdateandInviteUser));

               //Add premium number
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Australia");

				addNumberPage.selectNumberTypeByText("tollfree");

				String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
				getNumber = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				webAppMainUserLogin.closeLowcreditNotification();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				Common.pause(10);
				addNumberPage.closeAddProofPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
						actualPriceFromManageSubscription1),numberPriceAustraliaMonthly);
				assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);
				//Invite 1User
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(PremiumrBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd3rdUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd3rdUser, String.valueOf(seatAfterUpdateandInviteUser + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription4 = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPricePlatinumMonthly);		
				assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscription4);
				
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String freeSTDNumPurchased = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(freeSTDNumPurchased);
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPricePlatinumMonthly);		
				assertEquals(actualPriceFromManageSubscriptionAfterAddfreeNumber, expectedPriceFromManageSubscriptionAfterAddfreeNumber);
	}

	@Test(priority = 38, retryAnalyzer = Retry.class)
	public void add_premium_number_while_Bronze_annually_plan_is_running_2_users_2_standard_numbers() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		//String expectedPlanPrice = addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeAnnually,"12");
		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ) );
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceBronzeAnnually),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceBronzeAnnually);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceBronzeAnnually,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeAnnually,
						planPriceBronzeAnnually);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);
				System.out.println(actualPriceFromManageSubscription1+"1");



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

               //Add premium number
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Australia");

				addNumberPage.selectNumberTypeByText("tollfree");

				String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
				getNumber = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				webAppMainUserLogin.closeLowcreditNotification();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				Common.pause(10);
				addNumberPage.closeAddProofPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				
				
				String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber(
						actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber( numberPriceAustraliaMonthly, "12"));
				System.out.println(expectedPriceFromManageSubscription3);
				System.out.println(actualPriceFromManageSubscription3);
				assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);
				//Invite 1User
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(PremiumrBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeAnnually);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd3rdUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd3rdUser, String.valueOf(seatAfterUpdateandInviteUser + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription4 = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceBronzeAnnually);		
				assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscription4);
				
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String freeNumPurchased = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(freeNumPurchased);
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceBronzeAnnually);		
				assertEquals(actualPriceFromManageSubscriptionAfterAddfreeNumber, expectedPriceFromManageSubscriptionAfterAddfreeNumber);

	}

	@Test(priority = 39, retryAnalyzer = Retry.class)
	public void add_premium_number_while_Silver_annually_plan_is_running_2_users_2_standard_numbers() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();

		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ) );
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceSilverAnnually),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceSilverAnnually);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceSilverAnnually,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverAnnually,
						planPriceSilverAnnually);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);
				System.out.println(actualPriceFromManageSubscription1+"1");



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
				
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), 
						seatCountAfterUpdateandInviteUser));

               //Add premium number
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Australia");

				addNumberPage.selectNumberTypeByText("tollfree");

				String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
				getNumber = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				webAppMainUserLogin.closeLowcreditNotification();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				Common.pause(10);
				addNumberPage.closeAddProofPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				
				
				String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber(
						actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12"));
				System.out.println(expectedPriceFromManageSubscription3);
				System.out.println(actualPriceFromManageSubscription3);
				assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);
				//Invite 1User
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(PremiumrBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverAnnually);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd3rdUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd3rdUser, String.valueOf(seatAfterUpdateandInviteUser + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription4 = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceSilverAnnually);		
				assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscription4);
				
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String freeNumPurchased = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(freeNumPurchased);
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPriceSilverAnnually);		
				assertEquals(actualPriceFromManageSubscriptionAfterAddfreeNumber, expectedPriceFromManageSubscriptionAfterAddfreeNumber);

	}

	@Test(priority = 40, retryAnalyzer = Retry.class)
	public void add_premium_number_while_Platinum_annually_plan_is_running_2_users_2_standard_numbers()
			throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();

		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ) );
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPricePlatinumAnnually),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPricePlatinumAnnually);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPricePlatinumAnnually,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPricePlatinumAnnually,
						planPricePlatinumAnnually);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);
				System.out.println(actualPriceFromManageSubscription1+"1");



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
				
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), 
						seatCountAfterUpdateandInviteUser));

               //Add premium number
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Australia");

				addNumberPage.selectNumberTypeByText("tollfree");

				String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
				getNumber = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				webAppMainUserLogin.closeLowcreditNotification();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.getMessageOfPaidNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				Common.pause(10);
				addNumberPage.closeAddProofPopup();
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				
				
				String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber(
						actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber(
								 numberPriceAustraliaMonthly, "12"));
				System.out.println(expectedPriceFromManageSubscription3);
				System.out.println(actualPriceFromManageSubscription3);
				assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);
				//Invite 1User
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date2 = signupExcel.date();
				email2 = gmailUser + "+" + date2 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email2);
				inviteUserPageObj.selectNumberCheckbox(PremiumrBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumAnnually);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driver.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd3rdUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd3rdUser, String.valueOf(seatAfterUpdateandInviteUser + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription4 = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPricePlatinumAnnually);		
				assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscription4);
				
				driver.get(url.numbersPage());
				Common.pause(5);

				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String freeNumPurchased = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				
				addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(freeNumPurchased);
				
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
                assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterAdd3rdUser));
				
				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscriptionAfterAddfreeNumber = addNumberPage.additionOfTwoStringNumber((
						expectedPriceFromManageSubscription3),
						planPricePlatinumAnnually);		
				assertEquals(actualPriceFromManageSubscriptionAfterAddfreeNumber, expectedPriceFromManageSubscriptionAfterAddfreeNumber);

	}

	@Test(priority = 41, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Silver_Monthly_Anually_with_Transation_Error_Card() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();

		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ) );
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCardOfTransationError();
		String CardErrorValidationMessage = addNumberPage.getCardErrorValidationMessage();
		assertEquals(CardErrorValidationMessage, 
				"Your card issuer bank has declined this payment.Please contact your bank for support.");
		Common.pause(5);
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberPurchaseAftercardValidation = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberPurchaseAftercardValidation);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPriceAftercardValidation = addNumberPage.getSilverPlanPrice();

		assertEquals(actualPlanPriceAftercardValidation, planPriceSilverMonthly );
		String seatiTagValueInNumberPopupAftercardValidation = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopupAftercardValidation,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopupAftercardValidation = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopupAftercardValidation, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckoutAftercardValidation = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckoutAftercardValidation, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCardOfTransationError();
		String CardErrorValidationMessage2ndTime = addNumberPage.getCardErrorValidationMessage();
		assertEquals(CardErrorValidationMessage2ndTime, "Your card issuer bank has declined this payment.Please contact your bank for support.");

	}
	
	@Test(priority = 42, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Bronze_Monthly_Anually_with_Transation_Error_Card() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();

		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ) );
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCardOfTransationError();
		String CardErrorValidationMessage = addNumberPage.getCardErrorValidationMessage();
		assertEquals(CardErrorValidationMessage, 
				"Your card issuer bank has declined this payment.Please contact your bank for support.");
		Common.pause(5);
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberPurchaseAftercardValidation = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberPurchaseAftercardValidation);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPriceAftercardValidation = addNumberPage.getBronzePlanPrice();

		assertEquals(actualPlanPriceAftercardValidation, planPriceBronzeMonthly );
		String seatiTagValueInNumberPopupAftercardValidation = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopupAftercardValidation,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopupAftercardValidation = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopupAftercardValidation, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckoutAftercardValidation = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckoutAftercardValidation, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCardOfTransationError();
		String CardErrorValidationMessage2ndTime = addNumberPage.getCardErrorValidationMessage();
		assertEquals(CardErrorValidationMessage2ndTime, "Your card issuer bank has declined this payment.Please contact your bank for support.");

	}
	
	
	@Test(priority = 43, retryAnalyzer = Retry.class)
	public void Verify_Add_First_Number_with_Platinum_Monthly_Anually_with_Transation_Error_Card() throws Exception {
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();

		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ) );
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCardOfTransationError();
		String CardErrorValidationMessage = addNumberPage.getCardErrorValidationMessage();
		assertEquals(CardErrorValidationMessage, 
				"Your card issuer bank has declined this payment.Please contact your bank for support.");
		Common.pause(5);
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberPurchaseAftercardValidation = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberPurchaseAftercardValidation);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPriceAftercardValidation = addNumberPage.getPlatinumPlanPrice();

		assertEquals(actualPlanPriceAftercardValidation, planPricePlatinumMonthly );
		String seatiTagValueInNumberPopupAftercardValidation = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopupAftercardValidation,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopupAftercardValidation = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopupAftercardValidation, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckoutAftercardValidation = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckoutAftercardValidation, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCardOfTransationError();
		String CardErrorValidationMessage2ndTime = addNumberPage.getCardErrorValidationMessage();
		assertEquals(CardErrorValidationMessage2ndTime, "Your card issuer bank has declined this payment.Please contact your bank for support.");

	}


	@Test(priority = 44, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void verify_location_search_by_cityName_prefix() throws Exception {
		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Location");
		//locationCode = addNumberPage.returnLocationCode();
		locationName = addNumberPage.returnLocationName();
		System.out.println(locationName);
		locationCode = addNumberPage.enterLocationPrefix(locationName);
		Common.pause(1);
		// String location = addNumberPage.selectLocation();
		// addNumberPage.enterPrefixOrNumber("12");
		System.out.println(locationCode);

		assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains(locationCode));

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 45, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void verify_location_search_by_areaCode_prefix() throws Exception {
		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Location");
		locationCode = addNumberPage.returnLocationCode();
		locationCode = addNumberPage.enterLocationPrefix(locationCode);
		Common.pause(1);
		// String location = addNumberPage.selectLocation();
		// addNumberPage.enterPrefixOrNumber("12");
		System.out.println(locationCode);

		assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains(locationCode));

		assertTrue(addNumberPage.validateFirstNumberVoiceEnabled());
		assertTrue(addNumberPage.validateFirstNumberSMSEnabled());

		if (addNumberPage.validateNextButton()) {
			assertTrue(addNumberPage.validateNextButton());

			addNumberPage.clickOnNextButtonInNumberList();
			assertTrue(addNumberPage.validatePreviousButton());

		}

	}

	@Test(priority = 46, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void select_number_only_voice_service_with_bronze_Monthly() throws Exception {
		loginpage.enterEmail(newEmail);
		System.out.println("---email---" + newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		addNumberPage.clickOnNextButtonOfDocumentRequired();

		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, 	addNumberPage.multiplicationOfTwoStringNumber(planPriceBronzeMonthly,data.getValue("seatcount")));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderOfPremiumNumber();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderOfPremiumNumber().contains("Bronze"));
		// -----------------------
		String numberPriceAfterCheckout = addNumberPage.getNumberPriceFromYourOrderOfPremiumNumber();
		assertEquals(numberPriceAfterCheckout,numberPriceAustraliaMonthly);

		assertTrue(addNumberPage.getNumberDetailFromYourOrderOfPremiumNumber().contains("Virtual Number"));

		String actualTotal = addNumberPage.getTotalPriceFromYourOrderOfPremiumNumber();
		String expectedTotal = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly, data.getValue("seatcount")),
						numberPriceAustraliaMonthly);
		assertEquals(actualTotal, expectedTotal);

		// ----------------------
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		//addNumberPage.clickOnSaveNumberButton();
	     addNumberPage.closeAddProofPopup();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly, data.getValue("seatcount")),
				
						numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription, expectedPriceFromManageSubscription);

	}
	@Test(priority = 47, retryAnalyzer = Retry.class)
	public void Increase_decrease_sheet_Count_Add_number_User_add_in_Bronze_Plan() throws Exception {
	
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceBronzeMonthly),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceBronzeMonthly);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceBronzeMonthly,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeMonthly,
						planPriceBronzeMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
			// 6. update seat count less than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i-1));
			assertEquals(inviteUserPageObj.validateErrormessage(),
					"Numbers of seats cannot be less than the numbers of users created");
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i), "validate seat count");

			// 7. update seat count grater than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
//			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
//			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
//			driver.navigate().to(url.signIn());
//			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//			String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
//			assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
//					"validate seat count after invite sub user");
			try {
				assertTrue(
						inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
								planPriceBronzeMonthly),
						"validate charge while update seat. (popup message)");
			} catch (AssertionError e) {

				assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
						planPriceBronzeMonthly);
			}
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceBronzeMonthly, String.valueOf(i + 1)));
			Common.pause(25);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountUpdated = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountUpdated, String.valueOf(i + 1), "validate seat count after updated seat count");
			String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
			assertEquals(seatiTagValue4,
					"The charges will be calculated based on the total seats.");

			// 9. invite user after updated seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date4 = signupExcel.date();
			String email4 = gmailUser + "+" + date4 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email4);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.clickInviteNUserButton();
			String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceBronzeMonthly, String.valueOf(i + 1)));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date5 = signupExcel.date();
			String email5 = gmailUser + "+" + date5 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email5);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.selectAdminuserCheckbox();
			inviteUserPageObj.clickInviteNUserButton();
			inviteUserPageObj.clickOnYesBtnForInviteUser();
			String acutualSuccessfulValidationMsg5 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg5 = "Your invitation sent successfully";
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteAdminUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteAdminUser, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");

			// 10. delete user and validate seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.deleteInvitedPendingSubUser(email4);
			Common.pause(1);
			assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
			inviteUserPageObj.clickOnDeleteUserYesButton();
			String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
			String userDeletedExpectedMsg = "User deleted successfully";
			assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
			assertTrue(addNumberPage.getPlanDetailsFromAccountdetails().toLowerCase().contains("adminuser"));
			
			
			String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
			String ExpectedplanPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
					planPriceBronzeMonthly, String.valueOf(i + 1));
			assertEquals(actualPriceFromManageSubscription5, addNumberPage.additionOfTwoStringNumber(
					ExpectedplanPriceFromManageSubscription, "10"));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 1), "validate seat count after delete user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i));
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			Common.pause(5);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValueafterdeleteuser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValueafterdeleteuser, String.valueOf(i+1), "validate seat count");

	}
	
	@Test(priority = 48, retryAnalyzer = Retry.class)
	public void Increase_decrease_sheet_Count_Add_number_User_add_in_Bronze_Annually_Plan() throws Exception {
	
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceBronzeMonthly),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceBronzeMonthly);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceBronzeAnnually,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeAnnually,
						planPriceBronzeAnnually);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
			// 6. update seat count less than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i-1));
			assertEquals(inviteUserPageObj.validateErrormessage(),
					"Numbers of seats cannot be less than the numbers of users created");
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i), "validate seat count");

			// 7. update seat count grater than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
//			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
//			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
//			driver.navigate().to(url.signIn());
//			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//			String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
//			assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
//					"validate seat count after invite sub user");
			try {
				assertTrue(
						inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
								planPriceBronzeAnnually),
						"validate charge while update seat. (popup message)");
			} catch (AssertionError e) {

				assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
						planPriceBronzeAnnually);
			}
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceBronzeAnnually, String.valueOf(i + 1)));
			Common.pause(25);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountUpdated = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountUpdated, String.valueOf(i + 1), "validate seat count after updated seat count");
			String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
			assertEquals(seatiTagValue4,
					"The charges will be calculated based on the total seats.");

			// 9. invite user after updated seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date4 = signupExcel.date();
			String email4 = gmailUser + "+" + date4 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email4);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.clickInviteNUserButton();
			String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceBronzeAnnually, String.valueOf(i + 1)));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date5 = signupExcel.date();
			String email5 = gmailUser + "+" + date5 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email5);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.selectAdminuserCheckbox();
			inviteUserPageObj.clickInviteNUserButton();
			inviteUserPageObj.clickOnYesBtnForInviteUser();
			String acutualSuccessfulValidationMsg5 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg5 = "Your invitation sent successfully";
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteAdminUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteAdminUser, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");

			// 10. delete user and validate seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.deleteInvitedPendingSubUser(email4);
			Common.pause(1);
			assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
			inviteUserPageObj.clickOnDeleteUserYesButton();
			String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
			String userDeletedExpectedMsg = "User deleted successfully";
			assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
			String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
			String ExpectedplanPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
					planPriceBronzeAnnually, String.valueOf(i + 1));
			assertEquals(actualPriceFromManageSubscription5, addNumberPage.additionOfTwoStringNumber(
					ExpectedplanPriceFromManageSubscription, "120"));
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 1), "validate seat count after delete user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i));
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			Common.pause(5);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValueafterdeleteuser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValueafterdeleteuser, String.valueOf(i+1), "validate seat count");

	}
	
	
	@Test(priority = 49, retryAnalyzer = Retry.class)
	public void Increase_decrease_sheet_Count_Add_number_User_add_in_Silver_Monthly_Plan() throws Exception {
	
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceSilverMonthly),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceSilverMonthly);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceBronzeMonthly,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverMonthly,
						planPriceSilverMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), 
						seatCountAfterUpdateandInviteUser));
			// 6. update seat count less than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i-1));
			assertEquals(inviteUserPageObj.validateErrormessage(),
					"Numbers of seats cannot be less than the numbers of users created");
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i), "validate seat count");

			// 7. update seat count grater than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
//			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
//			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
//			driver.navigate().to(url.signIn());
//			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//			String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
//			assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
//					"validate seat count after invite sub user");
			try {
				assertTrue(
						inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
								planPriceSilverMonthly),
						"validate charge while update seat. (popup message)");
			} catch (AssertionError e) {

				assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
						planPriceSilverMonthly);
			}
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverMonthly, String.valueOf(i + 1)));
			Common.pause(25);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountUpdated = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountUpdated, String.valueOf(i + 1), "validate seat count after updated seat count");
			String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
			assertEquals(seatiTagValue4,
					"The charges will be calculated based on the total seats.");

			// 9. invite user after updated seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date4 = signupExcel.date();
			String email4 = gmailUser + "+" + date4 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email4);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.clickInviteNUserButton();
			String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverMonthly, String.valueOf(i + 1)));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date5 = signupExcel.date();
			String email5 = gmailUser + "+" + date5 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email5);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.selectAdminuserCheckbox();
			inviteUserPageObj.clickInviteNUserButton();
			inviteUserPageObj.clickOnYesBtnForInviteUser();
			String acutualSuccessfulValidationMsg5 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg5 = "Your invitation sent successfully";
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteAdminUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteAdminUser, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");

			// 10. delete user and validate seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.deleteInvitedPendingSubUser(email4);
			Common.pause(1);
			assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
			inviteUserPageObj.clickOnDeleteUserYesButton();
			String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
			String userDeletedExpectedMsg = "User deleted successfully";
			assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
			String ExpectedplanPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverMonthly, String.valueOf(i + 1));
			assertEquals(actualPriceFromManageSubscription5, addNumberPage.additionOfTwoStringNumber(
					ExpectedplanPriceFromManageSubscription, "10"));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 1), "validate seat count after delete user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i));
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(),
					"Seats has been updated and it will be effecting from the next billing date.");
			Common.pause(5);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValueafterdeleteuser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValueafterdeleteuser, String.valueOf(i+1), "validate seat count");
	}
	
	@Test(priority = 50, retryAnalyzer = Retry.class)
	public void Increase_decrease_sheet_Count_Add_number_User_add_in_Silver_Anually_Plan() throws Exception {
	
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectBronzePlan();
		addNumberPage.selectSilverPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPriceSilverAnnually),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPriceSilverAnnually);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Silver"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPriceSilverAnnually,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverAnnually,
						planPriceSilverAnnually);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),addNumberPage.multiplicationOfTwoStringNumber(
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), 
						seatCountAfterUpdateandInviteUser));
			// 6. update seat count less than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i-1));
			assertEquals(inviteUserPageObj.validateErrormessage(),
					"Numbers of seats cannot be less than the numbers of users created");
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i), "validate seat count");

			// 7. update seat count grater than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
//			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
//			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
//			driver.navigate().to(url.signIn());
//			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//			String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
//			assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
//					"validate seat count after invite sub user");
			try {
				assertTrue(
						inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
								planPriceSilverAnnually),
						"validate charge while update seat. (popup message)");
			} catch (AssertionError e) {

				assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
						planPriceSilverAnnually);
			}
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverAnnually, String.valueOf(i + 1)));
			Common.pause(25);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountUpdated = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountUpdated, String.valueOf(i + 1), "validate seat count after updated seat count");
			String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
			assertEquals(seatiTagValue4,
					"The charges will be calculated based on the total seats.");

			// 9. invite user after updated seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date4 = signupExcel.date();
			String email4 = gmailUser + "+" + date4 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email4);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.clickInviteNUserButton();
			String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverAnnually, String.valueOf(i + 1)));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date5 = signupExcel.date();
			String email5 = gmailUser + "+" + date5 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email5);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.selectAdminuserCheckbox();
			inviteUserPageObj.clickInviteNUserButton();
			inviteUserPageObj.clickOnYesBtnForInviteUser();
			String acutualSuccessfulValidationMsg5 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg5 = "Your invitation sent successfully";
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteAdminUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteAdminUser, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");

			// 10. delete user and validate seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.deleteInvitedPendingSubUser(email4);
			Common.pause(1);
			assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
			inviteUserPageObj.clickOnDeleteUserYesButton();
			String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
			String userDeletedExpectedMsg = "User deleted successfully";
			assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
			String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
			String ExpectedplanPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
					planPriceSilverAnnually, String.valueOf(i + 1));
			assertEquals(actualPriceFromManageSubscription5, addNumberPage.additionOfTwoStringNumber(
					ExpectedplanPriceFromManageSubscription, "120"));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 1), "validate seat count after delete user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i));
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			Common.pause(5);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValueafterdeleteuser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValueafterdeleteuser, String.valueOf(i+1), "validate seat count");

	}
	
	
	@Test(priority = 51, retryAnalyzer = Retry.class)
	public void Increase_decrease_sheet_Count_Add_number_User_add_in_Pltinum_Annually_Plan() throws Exception {
	
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPricePlatinumAnnually),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPricePlatinumAnnually);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPricePlatinumAnnually,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPricePlatinumAnnually,
						planPricePlatinumAnnually);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						addNumberPage.multiplicationOfTwoStringNumber(planPrice.getField(""
								+ "feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), seatCountAfterUpdateandInviteUser));
			// 6. update seat count less than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i-1));
			assertEquals(inviteUserPageObj.validateErrormessage(),
					"Numbers of seats cannot be less than the numbers of users created");
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i), "validate seat count");

			// 7. update seat count grater than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
//			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
//			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
//			driver.navigate().to(url.signIn());
//			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//			String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
//			assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
//					"validate seat count after invite sub user");
			try {
				assertTrue(
						inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
								planPricePlatinumAnnually),
						"validate charge while update seat. (popup message)");
			} catch (AssertionError e) {

				assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
						planPricePlatinumAnnually);
			}
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumAnnually, String.valueOf(i + 1)));
			Common.pause(25);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountUpdated = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountUpdated, String.valueOf(i + 1), "validate seat count after updated seat count");
			String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
			assertEquals(seatiTagValue4,
					"The charges will be calculated based on the total seats.");

			// 9. invite user after updated seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date4 = signupExcel.date();
			String email4 = gmailUser + "+" + date4 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email4);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.clickInviteNUserButton();
			String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumAnnually, String.valueOf(i + 1)));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date5 = signupExcel.date();
			String email5 = gmailUser + "+" + date5 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email5);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.selectAdminuserCheckbox();
			inviteUserPageObj.clickInviteNUserButton();
			inviteUserPageObj.clickOnYesBtnForInviteUser();
			String acutualSuccessfulValidationMsg5 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg5 = "Your invitation sent successfully";
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteAdminUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteAdminUser, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");

			// 10. delete user and validate seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.deleteInvitedPendingSubUser(email4);
			Common.pause(1);
			assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
			inviteUserPageObj.clickOnDeleteUserYesButton();
			String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
			String userDeletedExpectedMsg = "User deleted successfully";
			assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
			String ExpectedplanPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumAnnually, String.valueOf(i + 1));
			assertEquals(actualPriceFromManageSubscription5, addNumberPage.additionOfTwoStringNumber(
					ExpectedplanPriceFromManageSubscription, "120"));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 1), "validate seat count after delete user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i));
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			Common.pause(5);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValueafterdeleteuser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValueafterdeleteuser, String.valueOf(i+1), "validate seat count");
	}
	
	
	@Test(priority = 52, retryAnalyzer = Retry.class)
	public void Increase_decrease_sheet_Count_Add_number_User_add_in_Platinum_Monthly_Plan() throws Exception {
	
		String email = signup();
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.selectPlatinumPlan();
		Common.pause(10);
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		logout(webAppMainUserLogin);
		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));

		 // 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
		 //1 invite user
				int i;
				int seatcount = Integer.parseInt(data.getValue("seatcount"));
				for (i = 1; i <= seatcount; i++) {

					if (i == seatcount) {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date2 = signupExcel.date();
						email2 = gmailUser + "+" + date2 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						try {
							assertTrue(
									inviteUserPageObj.validatePopupMessageWithCharge(
											planPricePlatinumAnnually),
									"validate charge while invite user. (popup message)");
						} catch (AssertionError e) {

							assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
									planPricePlatinumAnnually);
						}
						inviteUserPageObj.clickOnYes();
						String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
								"validate seat count after invite 3rd sub user");
						String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
						assertEquals(seatiTagValue2,
								"The charges will be calculated based on the total seats.");
					} else {

						driver.navigate().to(url.signIn());
						webAppMainUserLogin.userspage();
						inviteUserPageObj.clickOnInviteUserButton();
						String date3 = signupExcel.date();
						email2 = gmailUser + "+" + date3 + "@callhippo.com";
						inviteUserPageObj.enterInviteUserEmail(email2);
						inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
						inviteUserPageObj.clickInviteNUserButton();
						String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
						String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
						assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
						addNumberPage.openAccountDetailsPopup();
						assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
						String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
						assertEquals(actualPriceFromManageSubscription3,
								addNumberPage.multiplicationOfTwoStringNumber(
										planPricePlatinumMonthly,
										String.valueOf(i + 1)));
						driver.navigate().to(url.signIn());
						webAppMainUserLogin.clickLeftMenuPlanAndBilling();
						String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
						assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
								"validate seat count after update seat count and invite user");
					
					}

				}
				//Add free number
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				int seatAfterUpdateandInviteUser = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				driver.get(url.numbersPage());
				Common.pause(5);
				int num = inviteUserPageObj.totalNumOnNumberPage();
				int seat = Integer.parseInt(seatCountAfterUpdateandInviteUser);
				for (num = num+1; num <= seat; num++) {

					if (num == seatcount) {
				System.out.println("If executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num);
					}else {
						System.out.println("else executed");
						addNumberPage.clickOnAddNumberInNumbersPage();
						addNumberPage.clickOnCountry("United States");
						addNumberPage.clickOnFirstNumberOfNumberListPage();

						assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

						addNumberPage.clickOnNoButtonOfFreeNumberPopup();
						addNumberPage.clickOnFirstNumberOfNumberListPage();
						addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

				        Common.pause(10);
				        driver.get(url.numbersPage());
						Common.pause(5);
						int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
				        assertEquals(numcountafternumberAdd , num);
						
					}
				}
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("× "+seatCountAfterUpdateandInviteUser));
				//assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
				String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
				String expectedPriceFromManageSubscription = addNumberPage.additionOfTwoStringNumber(
						planPricePlatinumMonthly,
						planPricePlatinumMonthly);
				assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription);



				driver.get(url.signIn());
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						addNumberPage.multiplicationOfTwoStringNumber(planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"), 
								seatCountAfterUpdateandInviteUser));
			// 6. update seat count less than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i-1));
			assertEquals(inviteUserPageObj.validateErrormessage(),
					"Numbers of seats cannot be less than the numbers of users created");
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i), "validate seat count");

			// 7. update seat count grater than users added in account
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
//			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
//			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
//			driver.navigate().to(url.signIn());
//			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
//			String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
//			assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
//					"validate seat count after invite sub user");
			try {
				assertTrue(
						inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
								planPricePlatinumMonthly),
						"validate charge while update seat. (popup message)");
			} catch (AssertionError e) {

				assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
						planPricePlatinumMonthly);
			}
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 1));
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumMonthly, String.valueOf(i + 1)));
			Common.pause(25);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountUpdated = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountUpdated, String.valueOf(i + 1), "validate seat count after updated seat count");
			String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
			assertEquals(seatiTagValue4,
					"The charges will be calculated based on the total seats.");

			// 9. invite user after updated seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date4 = signupExcel.date();
			String email4 = gmailUser + "+" + date4 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email4);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.clickInviteNUserButton();
			String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
			assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumMonthly, String.valueOf(i + 1)));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteUser1 = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteUser1, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.clickOnInviteUserButton();
			String date5 = signupExcel.date();
			String email5 = gmailUser + "+" + date5 + "@callhippo.com";
			inviteUserPageObj.enterInviteUserEmail(email5);
			inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
			inviteUserPageObj.selectAdminuserCheckbox();
			inviteUserPageObj.clickInviteNUserButton();
			inviteUserPageObj.clickOnYesBtnForInviteUser();
			String acutualSuccessfulValidationMsg5 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg5 = "Your invitation sent successfully";
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateandInviteAdminUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateandInviteAdminUser, String.valueOf(i + 1),
					"validate seat count after update seat count and invite user");

			// 10. delete user and validate seat count
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.userspage();
			inviteUserPageObj.deleteInvitedPendingSubUser(email4);
			Common.pause(1);
			assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
			inviteUserPageObj.clickOnDeleteUserYesButton();
			String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
			String userDeletedExpectedMsg = "User deleted successfully";
			assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
			addNumberPage.openAccountDetailsPopup();
			assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().toLowerCase().contains("platinum"));
			String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
			String ExpectedplanPriceFromManageSubscription = addNumberPage.multiplicationOfTwoStringNumber(
					planPricePlatinumMonthly, String.valueOf(i + 1));
			assertEquals(actualPriceFromManageSubscription5, addNumberPage.additionOfTwoStringNumber(
					ExpectedplanPriceFromManageSubscription, "10"));
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 1), "validate seat count after delete user");
			
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			inviteUserPageObj.enterSeatCount(String.valueOf(i));
			inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
			Common.pause(5);
			driver.navigate().to(url.signIn());
			webAppMainUserLogin.clickLeftMenuPlanAndBilling();
			String seatCountAfterUpdateLessValueafterdeleteuser = inviteUserPageObj.verifySeatCount();
			assertEquals(seatCountAfterUpdateLessValueafterdeleteuser, String.valueOf(i+1), "validate seat count");

	}
	@Test(priority = 53, retryAnalyzer = Retry.class, dependsOnMethods = "signup1")
	public void verify_countryList() throws Exception {
		loginpage.enterEmail(newEmail);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.verifyTopCountryDetails();
		addNumberPage.verifyAllCountryDetails();

	}

	@Test(priority = 54, retryAnalyzer = Retry.class)
	public void allocated_display_In_Users() throws Exception {

		Verify_Add_First_Number_with_bronze_Monthly();

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());

//		assertEquals(addNumberPage.getCreditFromSideMenu(),
//				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
//		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
//				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly,data.getValue("seatcount")));

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"----validate paid number pop up for bronze Monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		// put validation of chargebee subscription
		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly, data.getValue("seatcount")),
						numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription2);

	}

	@Test(priority = 55, retryAnalyzer = Retry.class)
	public void delete_Number_and_Add_again_2_Numbers_in_bronze_monthly() throws Exception {

		//Verify_Add_First_Number_with_bronze_Monthly();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number
		Common.pause(1);
		driver.get(url.numbersPage());
		Common.pause(7);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		Common.pause(2);
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount"))); // store
																															// seat

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		driver.get(url.numbersPage());
		Common.pause(5);	
		System.out.println(" ---- num for delete:" +numberBeforePurchased);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(2);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly,
				data.getValue("seatcount")));
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")),  numberPriceUSMonthly);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Chargeable second Number adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2));
		// put validation of chargebee subscription
		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String expectedPriceFromManageSubscriptionAfterAddedNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,  numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2,"--validate user charge.");

	}

	@Test(priority = 56, retryAnalyzer = Retry.class)
	public void delete_Number_and_Add_again_2_Numbers_in_silver_monthly() throws Exception {

	//	Verify_Add_First_Number_with_Silver_Monthly();
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);

		// addNumberPage.selectSilverPlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, planPriceSilverMonthly);

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		driver.get(url.numbersPage());
		Common.pause(5);	
		System.out.println(" ---- num for delete:" +numberBeforePurchased);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(2);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverMonthly,
						data.getValue("seatcount")));
		
		System.out.println("Seat based start...");
		int i;
		String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for silver monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));

				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
			    addNumberPage.hardRefreshByKeyboard();
				Common.pause(5);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly,
								data.getValue("seatcount")), 
								 numberPriceUSMonthly);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
		
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for silver monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
			    addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly,
								data.getValue("seatcount")));

			}

		}

		System.out.println("Chargeable second Number adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for silver monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2));

		// put validation of chargebee subscription
		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
	    addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String expectedPriceFromManageSubscriptionAfterAddedNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1, numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2,"--validate user charge.");


	}
	

	@Test(priority = 57, retryAnalyzer = Retry.class)
	public void delete_Number_and_Add_again_2_Numbers_in_platinum_monthly() throws Exception {

		//Verify_Add_First_Number_with_Platinum_Monthly();
		
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		driver.get(url.numbersPage());
		Common.pause(5);	
		System.out.println(" ---- num for delete:" +numberBeforePurchased);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(2);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
	    addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("platinum"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly,
						data.getValue("seatcount")));
		
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();

				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
			    addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(2);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("platinum"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly,
								data.getValue("seatcount")),  numberPriceUSMonthly);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for Platinum monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
			    addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("platinum"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly,
								data.getValue("seatcount")));

			}

		}

		System.out.println("Chargeable second Number adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2));
		// put validation of chargebee subscription
		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
	    addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		String expectedPriceFromManageSubscriptionAfterAddedNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1, numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2,"--validate user charge.");

	}
	
	@Test(priority = 58, retryAnalyzer = Retry.class)
	public void delete_Number_and_Add_again_2_Numbers_in_bronze_annually() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		driver.get(url.numbersPage());
		Common.pause(5);	
		System.out.println(" ---- num for delete:" +numberBeforePurchased);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(2);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually,
				data.getValue("seatcount")));
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze annually.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));
				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(numberPriceUSMonthly, "12");
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						planPriceBronzeAnnually, annuallyPrice);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);
				
	
				
			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze annually.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")));
			}

		}
		
		System.out.println("Chargeable second Number adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2));
		// put validation of chargebee subscription
		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		String expectedPriceFromManageSubscriptionAfterAddedNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1, annuallyPrice);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2,"--validate user charge.");
	}

	@Test(priority = 59, retryAnalyzer = Retry.class)
	public void delete_Number_and_Add_again_2_Numbers_in_silver_annually() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));

	//	addNumberPage.selectSilverPlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().toLowerCase().contains("silver"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().toLowerCase().contains("silver"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		driver.get(url.numbersPage());
		Common.pause(5);	
		System.out.println(" ---- num for delete:" +numberBeforePurchased);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(2);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually,
				data.getValue("seatcount")));
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver annually.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
						numberPriceUSMonthly, "12");
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						planPriceSilverAnnually, annuallyPrice);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);
				
	
				
			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly(),"-----validate Free number pop up for Silver annually.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually,
								data.getValue("seatcount")));
			}

		}
		
		System.out.println("Chargeable second Number adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Silver annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2));
		// put validation of chargebee subscription
		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		String expectedPriceFromManageSubscriptionAfterAddedNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1, annuallyPrice);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2,"--validate user charge.");


	}
	
	@Test(priority = 60, retryAnalyzer = Retry.class)
	public void delete_Number_and_Add_again_2_Numbers_in_platinum_annually() throws Exception {
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = numberBeforePurchased;
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));

		addNumberPage.selectPlatinumPlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		driver.get(url.numbersPage());
		Common.pause(5);	
		System.out.println(" ---- num for delete:" +numberBeforePurchased);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(2);
		addNumberPage.deleteNumber(numberBeforePurchased);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		Common.pause(3);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));

		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription1,addNumberPage.multiplicationOfTwoStringNumber(planPricePlatinumAnnually,
				data.getValue("seatcount")));
		
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum annually.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));
				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
						numberPriceUSMonthly, "12");
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						planPricePlatinumAnnually, annuallyPrice);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);
				
	
				
			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly(),"-----validate Free number pop up for Platinum annually.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually,
								data.getValue("seatcount")));
			}

		}
		
		System.out.println("Chargeable second Number adding..."); 
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String numberBeforePurchasedNumber2 = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for Platinum annually.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchasedNumber2));
		// put validation of chargebee subscription
		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String annuallyPrice = addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		String expectedPriceFromManageSubscriptionAfterAddedNumber2=addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1, annuallyPrice);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2,"--validate user charge.");


	}
	@Test(priority = 61, retryAnalyzer = Retry.class)
	public void allocate_number_verify_fields_on_numberPage() throws Exception {

		String email = signup();
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();

		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		Common.pause(3);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		
		assertEquals(actualPlanPrice, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly,
				data.getValue("seatcount")));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly,
				data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		//addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased));

		driver.get(url.numbersPage());
		Common.pause(5);
		System.out.println(numberBeforePurchased);
		assertTrue(addNumberPage.verifyFieldsOnNumbersPage("United States", numberBeforePurchased));
	}

	@Test(priority = 62, retryAnalyzer = Retry.class)
	public void verify_message_if_number_not_available() throws Exception {

		signup();
		Common.pause(2);
		driver.get(url.numbersPage());
		Common.pause(2);
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(3);
		//addNumberPage.clickOnCountryFromAll("Romania");
		addNumberPage.clickOnCountryFromAll("Slovakia");
		Common.pause(2);
		addNumberPage.selectSearchByText("Number");
		assertTrue(addNumberPage.validateNumberNotAvailable());
	}

	@Test(priority = 63, retryAnalyzer = Retry.class)
	public void Delete_higher_amount_Standard_number_when_number_having_different_price_Plan_period_monthly()
			throws Exception {
	
		Verify_Add_First_Number_with_bronze_Monthly();
		
		
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Canada");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")),  numberPriceCanadaMonthly);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Second Chargeable Number adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("Israel");

		String numberIsrael = addNumberPage.getFirstNumberOfNumberListPage(); // 12

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberIsrael));

		// put validation of chargebee subscription

		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
	
		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		System.out.println("---actualPriceFromManageSubscription2:"+actualPriceFromManageSubscriptionAfterAddedNumber2);
		String expectedPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,
				
								numberPriceIsraelMonthly);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2);  // 1 user + 2 num

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(numberIsrael);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(numberIsrael);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");


		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(5);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscriptionAfterAddedNumber1);

	}

	@Test(priority = 64, retryAnalyzer = Retry.class)
	public void Delete_Standard_number_when_number_having_different_price_Plan_period_monthly() throws Exception {

	//	Verify_Add_First_Number_with_bronze_Monthly();
		
		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number
		Common.pause(1);
		driver.get(url.numbersPage());
		Common.pause(7);
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String standardNumber = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		Common.pause(2);
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
	
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(standardNumber));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
																															

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Canada");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")), numberPriceCanadaMonthly);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Second Chargeable Number adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("Israel");

		String numberIsrael = addNumberPage.getFirstNumberOfNumberListPage(); // 12

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberIsrael));

		// put validation of chargebee subscription

		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
	
		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		System.out.println("---actualPriceFromManageSubscription2:"+actualPriceFromManageSubscriptionAfterAddedNumber2);
		String expectedPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,
								numberPriceIsraelMonthly);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2);  // 1 user + 2 num

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(standardNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(standardNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");


		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(5);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4, addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly,
						data.getValue("seatcount")),
								numberPriceIsraelMonthly));
	
	}

	@Test(priority = 65, retryAnalyzer = Retry.class)
	public void Delete_Lower_amount_Standard_number_when_number_having_different_price_Plan_period_monthly()
			throws Exception {

		Verify_Add_First_Number_with_bronze_Monthly();
		
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null, numberCanada =null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountryOfAllCountryList("Canada");
				
				 numberCanada = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberCanada));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")), 
								 numberPriceCanadaMonthly);
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Second Chargeable Number adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("Israel");

		String numberIsrael = addNumberPage.getFirstNumberOfNumberListPage(); 

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberIsrael));

		// put validation of chargebee subscription

		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
	
		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		System.out.println("---actualPriceFromManageSubscription2:"+actualPriceFromManageSubscriptionAfterAddedNumber2);
		String expectedPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,
				
								numberPriceIsraelMonthly);
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2);  // 1 user + 2 num

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(numberCanada);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(numberCanada);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");


		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(5);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4, addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly,
						data.getValue("seatcount")), numberPriceIsraelMonthly));		
	}

	@Test(priority = 66, retryAnalyzer = Retry.class)
	public void Delete_and_add_Standard_number_in_same_month_having_different_price_Plan_period_monthly()
			throws Exception {

		Delete_Standard_number_when_number_having_different_price_Plan_period_monthly();

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("United States");

		String numberUnitedStates = addNumberPage.getFirstNumberOfNumberListPage(); // 8

		addNumberPage.clickOnFirstNumberOfNumberListPage();
		
		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberUnitedStates));

		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly,
						data.getValue("seatcount")),
				addNumberPage.additionOfTwoStringNumber(
								numberPriceUSMonthly,
								numberPriceIsraelMonthly));
		assertEquals(actualPriceFromManageSubscription1, expectedPriceFromManageSubscription2);

	}

	@Test(priority = 67, retryAnalyzer = Retry.class)
	public void Delete_Premium_number_plan_period_Monthly() throws Exception {
		
		Verify_Add_First_Number_with_bronze_Monthly();

		
		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));
		
		///////////////////////////////////////////////////////////////////////////////////////////
		

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeMonthly, seatcount),numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("bronze"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains(
				numberPriceAustraliaMonthly));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),
						numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		// assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfSecondNumber().contains("Virtual
		// Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription4 = 
				addNumberPage.additionOfTwoStringNumber((addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, seatcount)),numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription4);
	}

	@Test(priority = 50, retryAnalyzer = Retry.class)
	public void Delete_standard_numbers_when_user_has_Premium_and_standard_number_Platinum_plan_period_Annually()
			throws Exception {

		Verify_Add_First_Number_with_Platinum_Annually();

		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));
		
		

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		
		String planprice  = addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, seatcount);
		String numberprice  =addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		System.out.println(planprice);
		System.out.println(numberprice);
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(planprice,numberprice);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
//////
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12"));
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup(); 
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		// assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfSecondNumber().contains("Virtual
		// Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription2);	}

	@Test(priority = 69, retryAnalyzer = Retry.class)
	public void Delete_higher_amount_Standard_number_when_number_having_different_price_Plan_period_Annually()
			throws Exception {

		Verify_Add_First_Number_with_bronze_Annually();

		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Canada");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")), 
						addNumberPage.multiplicationOfTwoStringNumber(numberPriceCanadaMonthly, "12"));
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num		
				
			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Second Chargeable Number adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("Israel");

		String numberIsrael = addNumberPage.getFirstNumberOfNumberListPage(); // 12

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberIsrael));

		// put validation of chargebee subscription

		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
	
		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		System.out.println("---actualPriceFromManageSubscription2:"+actualPriceFromManageSubscriptionAfterAddedNumber2);
		String expectedPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,
				
				addNumberPage.multiplicationOfTwoStringNumber(numberPriceIsraelMonthly, "12"));
				
		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2);  // 1 user + 2 num

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(numberIsrael);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(numberIsrael);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");


		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(5);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4, expectedPriceFromManageSubscriptionAfterAddedNumber1);
		


	}

	@Test(priority = 70, retryAnalyzer = Retry.class)
	public void Delete_Standard_number_when_number_having_different_price_Plan_period_annually() throws Exception {

		//Verify_Add_First_Number_with_bronze_Annually();

		String email = signup();
		System.out.println("signup done...");
		// 1.verify seat count
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		String standardNumber = addNumberPage.getFirstNumberOfNumberListPage();
		
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		// Common.pause(10);
		addNumberPage.clickOnAnnuallyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));

		addNumberPage.selectBronzePlan();
		// verify seat value
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));

		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));

		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		// addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(standardNumber));

		dashboard.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 27, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());

		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));

		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription,
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driver.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");
	
		
		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Canada");

				String numberBeforePurchased1 = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberBeforePurchased1));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")), addNumberPage.multiplicationOfTwoStringNumber(numberPriceCanadaMonthly, "12"));
								
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Second Chargeable Number adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("Israel");

		String numberIsrael = addNumberPage.getFirstNumberOfNumberListPage(); // 12

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberIsrael));

		// put validation of chargebee subscription

		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
	
		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		System.out.println("---actualPriceFromManageSubscription2:"+actualPriceFromManageSubscriptionAfterAddedNumber2);
		String expectedPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,
				
				 addNumberPage.multiplicationOfTwoStringNumber(numberPriceIsraelMonthly, "12"));

		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2);  // 1 user + 2 num

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(standardNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(standardNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");


		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(5);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();

		assertEquals(actualPriceFromManageSubscription4, addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")),
				 addNumberPage.multiplicationOfTwoStringNumber(
						 numberPriceIsraelMonthly, "12")));

	
	}

	@Test(priority = 71, retryAnalyzer = Retry.class)
	public void Delete_Lower_amount_Standard_number_when_number_having_different_price_Plan_period_Annually()
			throws Exception {

		Verify_Add_First_Number_with_bronze_Annually();

		System.out.println("Seat based start...");
	    String expectedPriceFromManageSubscriptionAfterAddedNumber1=null,numberCanada=null;
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {			
				
				System.out.println("Chargeable Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("Canada");

				 numberCanada = addNumberPage.getFirstNumberOfNumberListPage();

				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

				//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				//addNumberPage.clickOnSaveNumberButton();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberCanada));
				// put validation of chargebee subscription
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

				assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

				String actualPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
				System.out.println("---actualPriceFromManageSubscription1:"+actualPriceFromManageSubscriptionAfterAddedNumber1);
				expectedPriceFromManageSubscriptionAfterAddedNumber1 = addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")), addNumberPage.multiplicationOfTwoStringNumber(numberPriceCanadaMonthly, "12"));
								
				assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber1, expectedPriceFromManageSubscriptionAfterAddedNumber1);  // 1 user + 1 num

			} else {
				System.out.println("Free Number adding...");
				driver.get(url.numbersPage());
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				String numberFree = addNumberPage.getFirstNumberOfNumberListPage();
				
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly(),"-----validate Free number pop up for bronze monthly.");
				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
				assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberFree));
				System.out.println("validation of chargebee subscription...");	
				driver.get(url.signIn());
				addNumberPage.hardRefreshByKeyboard();
				Common.pause(3);
				assertEquals(addNumberPage.getCreditFromSideMenu(),
						planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
				assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
						planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

				addNumberPage.openAccountDetailsPopup();
				Common.pause(3);
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually,
								data.getValue("seatcount")));

			}

		}
		
		System.out.println("Second Chargeable Number adding...");
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		Common.pause(2);
		addNumberPage.clickOnCountryOfAllCountryList("Israel");

		String numberIsrael = addNumberPage.getFirstNumberOfNumberListPage(); // 12

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberIsrael));

		// put validation of chargebee subscription

		System.out.println("validation of chargebee subscription...");	
		driver.get(url.signIn());
		addNumberPage.hardRefreshByKeyboard();
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
	
		String actualPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		System.out.println("---actualPriceFromManageSubscription2:"+actualPriceFromManageSubscriptionAfterAddedNumber2);
		String expectedPriceFromManageSubscriptionAfterAddedNumber2 = addNumberPage.additionOfTwoStringNumber(
				expectedPriceFromManageSubscriptionAfterAddedNumber1,
				
				 addNumberPage.multiplicationOfTwoStringNumber(numberPriceIsraelMonthly, "12"));

		assertEquals(actualPriceFromManageSubscriptionAfterAddedNumber2, expectedPriceFromManageSubscriptionAfterAddedNumber2);  // 1 user + 2 num

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(numberCanada);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		Common.pause(3);
		addNumberPage.deleteNumber(numberCanada);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");


		driver.get(url.signIn());
		Common.pause(3);
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		Common.pause(5);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4, addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")),
				 addNumberPage.multiplicationOfTwoStringNumber(
						 numberPriceIsraelMonthly, "12")));

	}

	@Test(priority = 72, retryAnalyzer = Retry.class)
	public void Delete_and_add_Standard_number_in_same_month_having_different_price_Plan_period_annually()
			throws Exception {

		Delete_Standard_number_when_number_having_different_price_Plan_period_annually();

		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountryOfAllCountryList("United States");

		String numberUnitedStates = addNumberPage.getFirstNumberOfNumberListPage(); // 8

		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup(),"-----validate paid number pop up for bronze monthly.");

		//addNumberPage.clickOnCancelButtonOfFreeNumberPopup();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		//addNumberPage.clickOnAddButtonOfFreeNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		//addNumberPage.clickOnSaveNumberButton();
		assertTrue(addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(numberUnitedStates));
		
		// put validation of chargebee subscription
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription1 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually,
						data.getValue("seatcount")),
				addNumberPage.additionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
										numberPriceUSMonthly,
								"12"),
						addNumberPage.multiplicationOfTwoStringNumber(
								numberPriceIsraelMonthly, "12")));
		assertEquals(actualPriceFromManageSubscription1.replace(",", ""), expectedPriceFromManageSubscription2);

	}

	@Test(priority = 73, retryAnalyzer = Retry.class)
	public void Delete_Premium_number_plan_period_Annually() throws Exception {

		Verify_Add_First_Number_with_Silver_Annually();

		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForBronzeMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
		
		///////////////////////////////////////////////////////////////////////////////////////////
		

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().contains("Silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		
		String planprice  = addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, seatcount);
		String numberprice  =addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		System.out.println(planprice);
		System.out.println(numberprice);
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(planprice,numberprice);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
//////
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12"));
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));


		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription2);
	}

	@Test(priority = 74, retryAnalyzer = Retry.class)
	public void Delete_standard_numbers_when_user_has_Premium_and_standard_number_plan_period_Anually()
			throws Exception {

		Verify_Add_First_Number_with_Silver_Annually();

		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
		
		///////////////////////////////////////////////////////////////////////////////////////////
		

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		
		String planprice  = addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, seatcount);
		String numberprice  =addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		System.out.println(planprice);
		System.out.println(numberprice);
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(planprice,numberprice);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
//////
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12"));
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));


		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription2);
	}

	
	@Test(priority = 75, retryAnalyzer = Retry.class)
	public void Delete_Premium_number_Silver_plan_period_Monthly() throws Exception {
		
		Verify_Add_First_Number_with_Silver_Monthly();

		
		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForSilverMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
		
		///////////////////////////////////////////////////////////////////////////////////////////
		

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverMonthly, seatcount),numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
//////
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains(numberPriceAustraliaMonthly));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("silver"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		// assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfSecondNumber().contains("Virtual
		// Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription4 = 
				addNumberPage.additionOfTwoStringNumber((addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, seatcount)),numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription4);
	}
	
	@Test(priority = 76, retryAnalyzer = Retry.class)
	public void Delete_Premium_number_Platinum_plan_period_Monthly() throws Exception {
		
		Verify_Add_First_Number_with_Platinum_Monthly();

		
		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));
		
		///////////////////////////////////////////////////////////////////////////////////////////
		

		addNumberPage.openAccountDetailsPopup();
		Common.pause(3);
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(
				addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumMonthly, seatcount), numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
//////
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains(
				numberPriceAustraliaMonthly));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),
						numberPriceAustraliaMonthly);
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("platinum"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		// assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfSecondNumber().contains("Virtual
		// Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		String expectedPriceFromManageSubscription4 = 
				addNumberPage.additionOfTwoStringNumber((addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, seatcount)),numberPriceUSMonthly);
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription4);
	}
	
	
	@Test(priority = 77, retryAnalyzer = Retry.class)
	public void Delete_standard_numbers_when_user_has_Premium_and_standard_number_Bronze_plan_period_Annually()
			throws Exception {

		Verify_Add_First_Number_with_bronze_Annually();

		String seatcount = inviteUserPageObj.verifySeatCount();
		driver.get(url.numbersPage());
		Common.pause(5);
		int num = inviteUserPageObj.totalNumOnNumberPage();
		int seat = Integer.parseInt(seatcount);
		for (num = num+0; num <= seat; num++) {

		if (num == seat) {
		System.out.println("If executed");
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		addNumberPage.clickOnFirstNumberOfNumberListPage();

		assertTrue(addNumberPage.getMessageOfPaidNumberPopup());

		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

        Common.pause(10);
        driver.get(url.numbersPage());
		Common.pause(5);
		int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
        assertEquals(numcountafternumberAdd , num+1);
			}else {
				System.out.println("else executed");
				addNumberPage.clickOnAddNumberInNumbersPage();
				addNumberPage.clickOnCountry("United States");
				addNumberPage.clickOnFirstNumberOfNumberListPage();

				assertTrue(addNumberPage.getMessageOfFreeNumberPopupForPlatinumMonthly());

				addNumberPage.clickOnNoButtonOfFreeNumberPopup();
				addNumberPage.clickOnFirstNumberOfNumberListPage();
				addNumberPage.clickOnYesdButtonOfFreeNumberPopup();

		        Common.pause(10);
		        driver.get(url.numbersPage());
				Common.pause(5);
				int numcountafternumberAdd = inviteUserPageObj.totalNumOnNumberPage();
		        assertEquals(numcountafternumberAdd , num+1);
				
			}
		}
		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));
		
		

		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription11 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		
		String planprice  = addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, seatcount);
		String numberprice  =addNumberPage.multiplicationOfTwoStringNumber(
				numberPriceUSMonthly, "12");
		System.out.println(planprice);
		System.out.println(numberprice);
		String expectedPriceFromManageSubscription2 = addNumberPage.additionOfTwoStringNumber(planprice,numberprice);
		assertEquals(actualPriceFromManageSubscription11.replace(",", ""), expectedPriceFromManageSubscription2);
//////
		//Add premium number
		driver.get(url.numbersPage());
		Common.pause(5);

		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("Australia");

		addNumberPage.selectNumberTypeByText("tollfree");

		String PremiumrBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		webAppMainUserLogin.closeLowcreditNotification();
		addNumberPage.clickOnNoButtonOfFreeNumberPopup();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.getMessageOfPaidNumberPopup();
		addNumberPage.clickOnYesdButtonOfFreeNumberPopup();
		Common.pause(10);
		addNumberPage.closeAddProofPopup();
		addNumberPage.validateNumberAddedSuccessfullyOnNumberOnboardingPage(PremiumrBeforePurchased);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("bronze"));
		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));

		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		String expectedPriceFromManageSubscription3 = addNumberPage.additionOfTwoStringNumber((
				expectedPriceFromManageSubscription2),addNumberPage.multiplicationOfTwoStringNumber(numberPriceAustraliaMonthly, "12"));
		assertEquals(actualPriceFromManageSubscription3, expectedPriceFromManageSubscription3);

		driver.get(url.numbersPage());

		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnNoNumberDeletePopup();
		addNumberPage.deleteNumber(getNumber);
		assertTrue(addNumberPage.validateAreYouSureDeleteThisNumber(), " validate that number is displaying ");
		addNumberPage.clickOnYesNumberDeletePopup();
		assertTrue(addNumberPage.validateDeleteNumberSuccessfullMessageIsDisplayed(),
				" validate that delete number successfull message is displayed. ");

		driver.get(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.openAccountDetailsPopup(); 
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscriptionOfPremiumNumber().toLowerCase().contains("bronze"));

		assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfPremiumNumber().contains("Virtual Number"));
		// assertTrue(addNumberPage.getNumberDetailFromManageSubscriptionOfSecondNumber().contains("Virtual
		// Number"));

		String actualPriceFromManageSubscription4 = addNumberPage.getTotalPriceFromManageSubscriptionOfPremiumNumber();
		assertEquals(actualPriceFromManageSubscription4.replace(",", ""), expectedPriceFromManageSubscription2);	
	}
}
