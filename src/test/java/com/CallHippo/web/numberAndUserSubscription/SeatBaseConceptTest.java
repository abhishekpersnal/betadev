package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.DialerIndex;
import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Dialer.authentication.DialerLoginPage;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.RestAPI;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.credit.CreditWebSettingPage;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;
import com.CallHippo.web.authentication.SignupTest;
import com.CallHippo.web.dashboard.livecall.LiveCallPage;

public class SeatBaseConceptTest {

	WebDriver driverWebApp1;
	Common signUpExcel;
	PropertiesFile url;
	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;
	SignupPage registrationPage;
	LoginPage loginpage;
	AddNumberPage addNumberPage;
	XLSReader planPrice;
	WebDriver driverMail;
	SignupPage dashboard;
	LiveCallPage Web2liveCallPage;
	PlanUpgradePage planAndBillingPage;
	static Common excel;
	RestAPI planPriceAPI;
	RestAPI numberPriceAPI;

	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	String gmailUser = data.getValue("gmailUser");
	private String email;
	
	String planPriceBronzeMonthly,planPriceSilverMonthly,planPricePlatinumMonthly;
	String planPriceBronzeAnnually,planPriceSilverAnnually,planPricePlatinumAnnually;
	
	String numberPriceUSMonthly,numberPriceUSAnnually;
	String numberPriceAustraliaMonthly,numberPriceAustraliaAnnually;
	String numberPriceCanadaMonthly,numberPriceCanadaAnnually;
	String numberPriceIsraelMonthly,numberPriceIsraelAnnually;

	public SeatBaseConceptTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		excel = new Common(data.getValue("environment") + "\\Signup.xlsx");
		signUpExcel = new Common(data.getValue("environment") + "\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		planPriceAPI= new RestAPI();
		numberPriceAPI= new RestAPI();
	}

	@BeforeTest
	public void deleteAllMails() {
		try {
			
			planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
				
			planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
			
			planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
				
			planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
				
			planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
			
			planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
				
			//--------------------Number Price ---------------------------------------
			numberPriceUSMonthly=numberPriceAPI.getNumberPrice("United States", "fixed", "monthly");
				
			//numberPriceUSAnnually=numberPriceAPI.getNumberPrice("United States", "fixed", "annually");
				
			numberPriceCanadaMonthly=numberPriceAPI.getNumberPrice("Canada", "fixed", "monthly");
				
			//numberPriceCanadaAnnually=numberPriceAPI.getNumberPrice("Canada", "fixed", "annually");
				
	        numberPriceAustraliaMonthly=numberPriceAPI.getNumberPrice("Australia", "tollfree", "monthly");
				
			//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Australia", "tollfree", "annually");
				
			numberPriceIsraelMonthly=numberPriceAPI.getNumberPrice("Israel", "fixed", "monthly");
				
			//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Israel", "fixed", "annually");
			driverMail = TestBase.init3();
			dashboard = PageFactory.initElements(driverMail, SignupPage.class);
			// driver.get(url);S
			try {
				dashboard.deleteAllMail();
			} catch (Exception e) {
				dashboard.deleteAllMail();
			}
           
		
		} catch (Exception e) {
			System.out.println("catch");
			planPriceBronzeMonthly=planPriceAPI.getPlanPrice("bronze", "monthly");
			
			planPriceSilverMonthly=planPriceAPI.getPlanPrice("silver", "monthly");
			
			planPricePlatinumMonthly=planPriceAPI.getPlanPrice("platinum", "monthly");
				
			planPriceBronzeAnnually=planPriceAPI.getPlanPrice("bronze", "annually");
				
			planPriceSilverAnnually=planPriceAPI.getPlanPrice("silver", "annually");
			
			planPricePlatinumAnnually=planPriceAPI.getPlanPrice("platinum", "annually");
				
			//--------------------Number Price ---------------------------------------
			numberPriceUSMonthly=numberPriceAPI.getNumberPrice("United States", "fixed", "monthly");
				
			//numberPriceUSAnnually=numberPriceAPI.getNumberPrice("United States", "fixed", "annually");
				
			numberPriceCanadaMonthly=numberPriceAPI.getNumberPrice("Canada", "fixed", "monthly");
				
			//numberPriceCanadaAnnually=numberPriceAPI.getNumberPrice("Canada", "fixed", "annually");
				
	        numberPriceAustraliaMonthly=numberPriceAPI.getNumberPrice("Australia", "tollfree", "monthly");
				
			//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Australia", "tollfree", "annually");
				
			numberPriceIsraelMonthly=numberPriceAPI.getNumberPrice("Israel", "fixed", "monthly");
				
			//numberPriceAustraliaAnnually=numberPriceAPI.getNumberPrice("Israel", "fixed", "annually");
			Common.Screenshot(driverMail, " Gmail issue ", "BeforeTest - deleteAllMails");
			System.out.println("----Need to check Gmail issue - SignupTest - BeforeTest - deleteAllMails----");
		}
		driverMail.quit();
	}

	@BeforeMethod
	public void initialization() throws IOException {
		driverWebApp1 = TestBase.init2();
		webAppMainUserLogin = PageFactory.initElements(driverWebApp1, WebToggleConfiguration.class);
		registrationPage = PageFactory.initElements(driverWebApp1, SignupPage.class);
		loginpage = PageFactory.initElements(driverWebApp1, LoginPage.class);
		inviteUserPageObj = PageFactory.initElements(driverWebApp1, InviteUserPage.class);
		addNumberPage = PageFactory.initElements(driverWebApp1, AddNumberPage.class);
		Web2liveCallPage = PageFactory.initElements(driverWebApp1, LiveCallPage.class);
		planAndBillingPage = PageFactory.initElements(driverWebApp1, PlanUpgradePage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driverWebApp1, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		} else {
			String testname = "Pass";
			Common.Screenshot(driverWebApp1, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		driverWebApp1.quit();
	}

	private String signUpUser() throws Exception {
		driverWebApp1.get(url.livesignUp());
		Common.pause(1);
		String date = signUpExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		System.out.println("---email1---" + email1);


		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(excel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(excel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(excel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email1);
		registrationPage.enter_website_signUp_popUp_phoneNumber(excel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(excel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");

		return email1;
	}

	private void LoginGmailAndConfirmYourMail() throws Exception {

		registrationPage.ValidateVerifyButtonOnGmail();

		String actualTitle = inviteUserPageObj.validateTitle();
		String expectedTitle = "Dashboard | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();
	}

	public void loginWeb(WebToggleConfiguration web, String email, String password) {
		Common.pause(2);
		web.enterEmail(email);
		web.enterPassword(password);
		web.clickOnLogin();
	}

	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verifySeatConceptInBronzeMonthly() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");
		
		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signUpExcel.date();
				String email = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly,
								String.valueOf(i + 1)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		i=i-1;
		// 6. update seat count less than users added in account
		inviteUserPageObj.enterSeatCount(String.valueOf(i));
		assertEquals(inviteUserPageObj.validateErrormessage(),
				"Numbers of seats cannot be less than the numbers of users created");
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i + 1), "validate seat count");

		// 7. update seat count equals to users added in account
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
				"validate seat count after invite sub user");

		// 8. update seat count grater than users added in account
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 2));
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
							planPriceBronzeMonthly),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
					planPriceBronzeMonthly);
		}
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 2));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 2)));
		Common.pause(25);
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountUpdated = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountUpdated, String.valueOf(i + 2), "validate seat count after updated seat count");
		String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue4,
				"The charges will be calculated based on the total seats.");

		// 9. invite user after updated seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		String date4 = signUpExcel.date();
		String email4 = gmailUser + "+" + date4 + "@callhippo.com";
		inviteUserPageObj.enterInviteUserEmail(email4);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 2),
				"validate seat count after update seat count and invite user");

		// 10. delete user and validate seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email4);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();
		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription5, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 2), "validate seat count after delete user");

		// 11. invite two user (now, users are less then seat count)
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		String date10 = signUpExcel.date();
		Common.pause(1);
		String date11 = signUpExcel.date();
		String email11 = gmailUser + "+" + date10 + "@callhippo.com";
		String email12 = gmailUser + "+" + date11 + "@callhippo.com";
		System.out.println("---subUserMail---" + email11);
		inviteUserPageObj.enterInviteUserEmail(email11);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email12);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithCharge(
							planPriceBronzeMonthly),
					"validate charge while invite user. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
					planPriceBronzeMonthly);
		}
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 3)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterAdded6thuser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterAdded6thuser, String.valueOf(i + 3), "validate seat count after invited 6th user");

		// 12. increase seat count and decrease seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 4));
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
							planPriceBronzeMonthly),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
					planPriceBronzeMonthly);
		}
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 4));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 4)));

		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 3));
		assertTrue(inviteUserPageObj.validatePopupMessageOfUpdateSeat(),
				"validate message while update seat. (popup message)");
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("Bronze"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, String.valueOf(i + 4)));
		
		

	}
	

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verifySeatConceptInBronzeAnnually() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceBronzeAnnually,"12" ));
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signUpExcel.date();
				String email = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeAnnually);
				}

				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
			}else {
				
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually, String.valueOf(i + 1)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
				
			}
		}
		
		i=i-1;
		
		// 6. update seat count less than users added in account
				inviteUserPageObj.enterSeatCount(String.valueOf(i));
				assertEquals(inviteUserPageObj.validateErrormessage(),
						"Numbers of seats cannot be less than the numbers of users created");
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");

				// 7. update seat count equals to users added in account
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");

				// 8. update seat count grater than users added in account
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 2));
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
									planPriceBronzeAnnually),
							"validate charge while update seat. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
							planPriceBronzeAnnually);
				}
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 2));
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually, String.valueOf(i + 2)));
				Common.pause(25);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountUpdated = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountUpdated, String.valueOf(i + 2), "validate seat count after updated seat count");
				String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue4,
						"The charges will be calculated based on the total seats.");

				// 9. invite user after updated seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually, String.valueOf(i + 2)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 2),
						"validate seat count after update seat count and invite user");

				// 10. delete user and validate seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.deleteInvitedPendingSubUser(email4);
				Common.pause(1);
				assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
				inviteUserPageObj.clickOnDeleteUserYesButton();
				String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
				String userDeletedExpectedMsg = "User deleted successfully";
				assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription5, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceBronzeAnnually, String.valueOf(i + 2)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 2), "validate seat count after delete user");

				// 11. invite two user (now, users are 4 and seat count is 5)
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date10 = signUpExcel.date();
				Common.pause(1);
				String date11 = signUpExcel.date();
				String email11 = gmailUser + "+" + date10 + "@callhippo.com";
				String email12 = gmailUser + "+" + date11 + "@callhippo.com";
				System.out.println("---subUserMail---" + email11);
				inviteUserPageObj.enterInviteUserEmail(email11);
				Common.pause(1);
				inviteUserPageObj.clickSaveButton();
				Common.pause(1);
				inviteUserPageObj.clickOnAddButton();
				Common.pause(1);
				inviteUserPageObj.enterInviteUserEmail(email12);
				inviteUserPageObj.clickSaveButton();
				Common.pause(1);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceBronzeAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceBronzeAnnually);
				}
				inviteUserPageObj.clickOnYes();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually, String.valueOf(i + 3)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdded6thuser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdded6thuser, String.valueOf(i + 3), "validate seat count after invited 6th user");

				// 12. increase seat count and decrease seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 4));
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
									planPriceBronzeAnnually),
							"validate charge while update seat. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
							planPriceBronzeAnnually);
				}
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 4));
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually, String.valueOf(i + 4)));

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 3));
				assertTrue(inviteUserPageObj.validatePopupMessageOfUpdateSeat(),
						"validate message while update seat. (popup message)");
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually, String.valueOf(i + 4)));
		
		

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verifySeatBaseConceptInSilverMonthly() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);
//		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signUpExcel.date();
				String email = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().contains("silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly,
								String.valueOf(i + 1)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		i=i-1;
		// 6. update seat count less than users added in account
		inviteUserPageObj.enterSeatCount(String.valueOf(i));
		assertEquals(inviteUserPageObj.validateErrormessage(),
				"Numbers of seats cannot be less than the numbers of users created");
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i + 1), "validate seat count");

		// 7. update seat count equals to users added in account
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
				"validate seat count after invite sub user");

		// 8. update seat count grater than users added in account
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 2));
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
							planPriceSilverMonthly),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
					planPriceSilverMonthly);
		}
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 2));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountUpdated = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountUpdated, String.valueOf(i + 2), "validate seat count after updated seat count");
		String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue4,
				"The charges will be calculated based on the total seats.");

		// 9. invite user after updated seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		String date4 = signUpExcel.date();
		String email4 = gmailUser + "+" + date4 + "@callhippo.com";
		inviteUserPageObj.enterInviteUserEmail(email4);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 2),
				"validate seat count after update seat count and invite user");

		// 10. delete user and validate seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email4);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();
		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription5, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 2), "validate seat count after delete user");

		// 11. invite two user (now, users are less then seat count)
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		String date10 = signUpExcel.date();
		Common.pause(1);
		String date11 = signUpExcel.date();
		String email11 = gmailUser + "+" + date10 + "@callhippo.com";
		String email12 = gmailUser + "+" + date11 + "@callhippo.com";
		System.out.println("---subUserMail---" + email11);
		inviteUserPageObj.enterInviteUserEmail(email11);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email12);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithCharge(
							planPriceSilverMonthly),
					"validate charge while invite user. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
					planPriceSilverMonthly);
		}
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 3)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterAdded6thuser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterAdded6thuser, String.valueOf(i + 3), "validate seat count after invited 6th user");

		// 12. increase seat count and decrease seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 4));
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
							planPriceSilverMonthly),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
					planPriceSilverMonthly);
		}
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 4));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 4)));

		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 3));
		assertTrue(inviteUserPageObj.validatePopupMessageOfUpdateSeat(),
				"validate message while update seat. (popup message)");
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, String.valueOf(i + 4)));
		
		

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verifySeatConceptInSilverAnnually() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with silver plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPriceSilverAnnually,"12" ));
//		addNumberPage.selectSilverPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Silver'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signUpExcel.date();
				String email = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverAnnually);
				}

				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
			}else {
				
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually, String.valueOf(i + 1)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
				
			}
		}
		
		i=i-1;
		
		// 6. update seat count less than users added in account
				inviteUserPageObj.enterSeatCount(String.valueOf(i));
				assertEquals(inviteUserPageObj.validateErrormessage(),
						"Numbers of seats cannot be less than the numbers of users created");
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");

				// 7. update seat count equals to users added in account
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");

				// 8. update seat count grater than users added in account
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 2));
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
									planPriceSilverAnnually),
							"validate charge while update seat. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
							planPriceSilverAnnually);
				}
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 2));
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
				assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually, String.valueOf(i + 2)));
				Common.pause(25);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountUpdated = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountUpdated, String.valueOf(i + 2), "validate seat count after updated seat count");
				String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue4,
						"The charges will be calculated based on the total seats.");

				// 9. invite user after updated seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
				assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually, String.valueOf(i + 2)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 2),
						"validate seat count after update seat count and invite user");

				// 10. delete user and validate seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.deleteInvitedPendingSubUser(email4);
				Common.pause(1);
				assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
				inviteUserPageObj.clickOnDeleteUserYesButton();
				String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
				String userDeletedExpectedMsg = "User deleted successfully";
				assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
				assertEquals(actualPriceFromManageSubscription5, addNumberPage.multiplicationOfTwoStringNumber(
						planPriceSilverAnnually, String.valueOf(i + 2)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 2), "validate seat count after delete user");

				// 11. invite two user (now, users are less than seat count )
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date10 = signUpExcel.date();
				Common.pause(1);
				String date11 = signUpExcel.date();
				String email11 = gmailUser + "+" + date10 + "@callhippo.com";
				String email12 = gmailUser + "+" + date11 + "@callhippo.com";
				System.out.println("---subUserMail---" + email11);
				inviteUserPageObj.enterInviteUserEmail(email11);
				Common.pause(1);
				inviteUserPageObj.clickSaveButton();
				Common.pause(1);
				inviteUserPageObj.clickOnAddButton();
				Common.pause(1);
				inviteUserPageObj.enterInviteUserEmail(email12);
				inviteUserPageObj.clickSaveButton();
				Common.pause(1);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPriceSilverAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPriceSilverAnnually);
				}
				inviteUserPageObj.clickOnYes();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, String.valueOf(i + 3)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdded6thuser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdded6thuser, String.valueOf(i + 3), "validate seat count after invited 6th user");

				// 12. increase seat count and decrease seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 4));
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
									planPriceSilverAnnually),
							"validate charge while update seat. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
							planPriceSilverAnnually);
				}
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 4));
				Common.pause(10);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, String.valueOf(i + 4)));

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 3));
				assertTrue(inviteUserPageObj.validatePopupMessageOfUpdateSeat(),
						"validate message while update seat. (popup message)");
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
				Common.pause(10);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, String.valueOf(i + 4)));
		
		

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verifySeatBaseConceptInPlatinumMonthly() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with Platinum plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice, planPricePlatinumMonthly);
		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"),
				"validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signUpExcel.date();
				String email = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumMonthly),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumMonthly);
				}
				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1),
						"validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
			} else {

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3,
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly,
								String.valueOf(i + 1)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");

			}

		}
		i=i-1;
		// 6. update seat count less than users added in account
		inviteUserPageObj.enterSeatCount(String.valueOf(i));
		assertEquals(inviteUserPageObj.validateErrormessage(),
				"Numbers of seats cannot be less than the numbers of users created");
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i + 1), "validate seat count");

		// 7. update seat count equals to users added in account
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1),
				"validate seat count after invite sub user");

		// 8. update seat count grater than users added in account
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 2));
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
							planPricePlatinumMonthly),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
					planPricePlatinumMonthly);
		}
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to " + String.valueOf(i + 2));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 2)));
        Common.pause(25);
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountUpdated = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountUpdated, String.valueOf(i + 2), "validate seat count after updated seat count");
		String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue4,
				"The charges will be calculated based on the total seats.");

		// 9. invite user after updated seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		String date4 = signUpExcel.date();
		String email4 = gmailUser + "+" + date4 + "@callhippo.com";
		inviteUserPageObj.enterInviteUserEmail(email4);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
		String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 2),
				"validate seat count after update seat count and invite user");

		// 10. delete user and validate seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.userspage();
		inviteUserPageObj.deleteInvitedPendingSubUser(email4);
		Common.pause(1);
		assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
		inviteUserPageObj.clickOnDeleteUserYesButton();
		String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
		String userDeletedExpectedMsg = "User deleted successfully";
		assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription5, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 2)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 2), "validate seat count after delete user");

		// 11. invite two user (now, users are less then seat count)
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();
		String date10 = signUpExcel.date();
		Common.pause(1);
		String date11 = signUpExcel.date();
		String email11 = gmailUser + "+" + date10 + "@callhippo.com";
		String email12 = gmailUser + "+" + date11 + "@callhippo.com";
		System.out.println("---subUserMail---" + email11);
		inviteUserPageObj.enterInviteUserEmail(email11);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.clickOnAddButton();
		Common.pause(1);
		inviteUserPageObj.enterInviteUserEmail(email12);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
		inviteUserPageObj.clickInviteNUserButton();
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithCharge(
							planPricePlatinumMonthly),
					"validate charge while invite user. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
					planPricePlatinumMonthly);
		}
		inviteUserPageObj.clickOnYes();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 3)));
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterAdded6thuser = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterAdded6thuser, String.valueOf(i + 3), "validate seat count after invited 6th user");

		// 12. increase seat count and decrease seat count
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 4));
		try {
			assertTrue(
					inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
							planPricePlatinumMonthly),
					"validate charge while update seat. (popup message)");
		} catch (AssertionError e) {

			assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
					planPricePlatinumMonthly);
		}
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 4));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 4)));

		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount(String.valueOf(i + 3));
		assertTrue(inviteUserPageObj.validatePopupMessageOfUpdateSeat(),
				"validate message while update seat. (popup message)");
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		assertEquals(addNumberPage.getPlanPriceFromManageSubscription(), addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, String.valueOf(i + 4)));
		
		

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verifySeatConceptInPlatinumAnnually() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with platinum plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnAnnuallyPlan();
		String actualPlanPrice = addNumberPage.getPlatinumPlanPrice();
		assertEquals(actualPlanPrice,
				addNumberPage.divisonOfTwoStringNumber(planPricePlatinumAnnually,"12" ));
		addNumberPage.selectPlatinumPlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("platinum"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("annuallyCredit", "select * from Sheet1 where plan='Platinum'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesAnnually", "select * from Sheet1 where plan='Platinum'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		
		int i;
		int seatcount = Integer.parseInt(data.getValue("seatcount"));
		for (i = 1; i <= seatcount; i++) {

			if (i == seatcount) {
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date3 = signUpExcel.date();
				String email = gmailUser + "+" + date3 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumAnnually);
				}

				inviteUserPageObj.clickOnYes();
				String acutualSuccessfulValidationMsg3 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg3 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg3, expectedSuccessfulValidationMsg3);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdd4thUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdd4thUser, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");
				String seatiTagValue2 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue2,
						"The charges will be calculated based on the total seats.");
				
			}else {
				
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription();
				assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually, String.valueOf(i + 1)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 1),
						"validate seat count after update seat count and invite user");
				
			}
		}
		
		i=i-1;
		
		// 6. update seat count less than users added in account
				inviteUserPageObj.enterSeatCount(String.valueOf(i));
				assertEquals(inviteUserPageObj.validateErrormessage(),
						"Numbers of seats cannot be less than the numbers of users created");
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateLessValue = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateLessValue, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");

				// 7. update seat count equals to users added in account
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 1));
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateEqualsValue = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateEqualsValue, String.valueOf(i + 1), "validate seat count after invite 3rd sub user");

				// 8. update seat count grater than users added in account
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 2));
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
									planPricePlatinumAnnually),
							"validate charge while update seat. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
							planPricePlatinumAnnually);
				}
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 2));
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
				assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually, String.valueOf(i + 2)));
				Common.pause(25);
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountUpdated = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountUpdated, String.valueOf(i + 2), "validate seat count after updated seat count");
				String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
				assertEquals(seatiTagValue4,
						"The charges will be calculated based on the total seats.");

				// 9. invite user after updated seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date4 = signUpExcel.date();
				String email4 = gmailUser + "+" + date4 + "@callhippo.com";
				inviteUserPageObj.enterInviteUserEmail(email4);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				String acutualSuccessfulValidationMsg4 = inviteUserPageObj.validateSuccessMessage();
				String expectedSuccessfulValidationMsg4 = "Your invitation sent successfully";
				assertEquals(acutualSuccessfulValidationMsg4, expectedSuccessfulValidationMsg4);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
				assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually, String.valueOf(i + 2)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterUpdateandInviteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterUpdateandInviteUser, String.valueOf(i + 2),
						"validate seat count after update seat count and invite user");

				// 10. delete user and validate seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.userspage();
				inviteUserPageObj.deleteInvitedPendingSubUser(email4);
				Common.pause(1);
				assertEquals(inviteUserPageObj.verifyDeleteUserPopup(), true);
				inviteUserPageObj.clickOnDeleteUserYesButton();
				String userDeletedActualMsg = inviteUserPageObj.validateInfoMessage();
				String userDeletedExpectedMsg = "User deleted successfully";
				assertEquals(userDeletedActualMsg, userDeletedExpectedMsg);
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				String actualPriceFromManageSubscription5 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
				assertEquals(actualPriceFromManageSubscription5, addNumberPage.multiplicationOfTwoStringNumber(
						planPricePlatinumAnnually, String.valueOf(i + 2)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterDeleteUser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterDeleteUser, String.valueOf(i + 2), "validate seat count after delete user");

				// 11. invite two user (now, users are less than seat count )
				webAppMainUserLogin.userspage();
				inviteUserPageObj.clickOnInviteUserButton();
				String date10 = signUpExcel.date();
				Common.pause(1);
				String date11 = signUpExcel.date();
				String email11 = gmailUser + "+" + date10 + "@callhippo.com";
				String email12 = gmailUser + "+" + date11 + "@callhippo.com";
				System.out.println("---subUserMail---" + email11);
				inviteUserPageObj.enterInviteUserEmail(email11);
				Common.pause(1);
				inviteUserPageObj.clickSaveButton();
				Common.pause(1);
				inviteUserPageObj.clickOnAddButton();
				Common.pause(1);
				inviteUserPageObj.enterInviteUserEmail(email12);
				inviteUserPageObj.clickSaveButton();
				Common.pause(1);
				inviteUserPageObj.selectNumberCheckbox(numberBeforePurchased);
				inviteUserPageObj.clickInviteNUserButton();
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithCharge(
									planPricePlatinumAnnually),
							"validate charge while invite user. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessage()),
							planPricePlatinumAnnually);
				}
				inviteUserPageObj.clickOnYes();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Your invitation sent successfully");
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually, String.valueOf(i + 3)));
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				String seatCountAfterAdded6thuser = inviteUserPageObj.verifySeatCount();
				assertEquals(seatCountAfterAdded6thuser, String.valueOf(i + 3), "validate seat count after invited 6th user");

				// 12. increase seat count and decrease seat count
				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 4));
				try {
					assertTrue(
							inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
									planPricePlatinumAnnually),
							"validate charge while update seat. (popup message)");
				} catch (AssertionError e) {

					assertEquals(addNumberPage.roundof(inviteUserPageObj.getChargesOfPopupMessageInBillingPage()),
							planPricePlatinumAnnually);
				}
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to "+String.valueOf(i + 4));
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually, String.valueOf(i + 4)));

				driverWebApp1.navigate().to(url.signIn());
				webAppMainUserLogin.clickLeftMenuPlanAndBilling();
				inviteUserPageObj.enterSeatCount(String.valueOf(i + 3));
				assertTrue(inviteUserPageObj.validatePopupMessageOfUpdateSeat(),
						"validate message while update seat. (popup message)");
				inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
				assertEquals(inviteUserPageObj.validateSuccessMessage(), "Seats has been updated and it will be effecting from the next billing date.");
				addNumberPage.openAccountDetailsPopup();
				assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
				assertEquals(addNumberPage.getPlanPriceFromManageSubscription().replace(",", ""),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually, String.valueOf(i + 4)));
		
		

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void Plan_upgrade_Bronze_Monthly_to_Bronze_Annually() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// 4. upgrade plan from bronze monthly to bronze annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnBronzeRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeAnnually, data.getValue("seatcount")));

		// 5. upgrade plan from Bronze Annually to Silver Annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnSilverRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeAnnually, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription1 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription1, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));

		// 6. upgrade plan from Silver Annually to Platinum Annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnPlatinumRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void Plan_upgrade_Bronze_Monthly_to_Silver_Monthly() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// 4. upgrade plan from bronze monthly to silver monthly
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnSilverRadioButton();
		planAndBillingPage.clickOnMonthlyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 5. upgrade plan from Silver monthly to silver Annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnSilverRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription4 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription4, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		driverWebApp1.navigate().to(url.signIn());

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void Plan_upgrade_Bronze_Monthly_to_Silver_Annually() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// 4. upgrade plan from bronze monthly to Silver annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnSilverRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverAnnually, data.getValue("seatcount")));
		driverWebApp1.navigate().to(url.signIn());

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void Plan_upgrade_Bronze_Monthly_to_Platinum_Monthly() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// 4. upgrade plan from bronze monthly to Platinum monthly
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnPlatinumRadioButton();
		planAndBillingPage.clickOnMonthlyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));

		// 4. upgrade plan from Platinum monthly to Platinum Annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnPlatinumRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription3 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription3, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));

	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Plan_upgrade_Bronze_Monthly_to_Platinum_Annually() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// 4. upgrade plan from bronze monthly to Platinum annually
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnPlatinumRadioButton();
		planAndBillingPage.clickOnAnnuallyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumAnnually, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumAnnually, data.getValue("seatcount")));
		driverWebApp1.navigate().to(url.signIn());

	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void Plan_upgrade_Silver_Monthly_to_Platinum_Monthly() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getSilverPlanPrice();
		assertEquals(actualPlanPrice, planPriceSilverMonthly);
//		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"));
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("silver"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		driverWebApp1.navigate().to(url.signIn());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Silver'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(), addNumberPage.multiplicationOfTwoStringNumber(
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Silver'"), data.getValue("seatcount")));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, data.getValue("seatcount")));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, data.getValue("seatcount"), "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// 13. upgrade plan from Silver monthly to Platinum monthly
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnPlatinumRadioButton();
		planAndBillingPage.clickOnMonthlyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPricePlatinumMonthly, data.getValue("seatcount")),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly, data.getValue("seatcount")))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("platinum"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPricePlatinumMonthly, data.getValue("seatcount")));
		driverWebApp1.navigate().to(url.signIn());

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void updateSeatCountWhileNumberPurchaseInBronzePlan() throws Exception {

		// 1. signup and verify seat count
		String signUpUserEmailId = signUpUser();
		LoginGmailAndConfirmYourMail();
		Common.pause(1);
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfterSignup = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfterSignup, "1", "signup and verify seat count.");
		String seatiTagValue = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue,
				"The charges will be calculated based on the total seats.");

		// 2. add number with bronze plan
		driverWebApp1.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		System.out.println(numberBeforePurchased);
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		addNumberPage.clickOnMonthlyPlan();
		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPriceBronzeMonthly);
		addNumberPage.selectBronzePlan();
		String seatiTagValueInNumberPopup = addNumberPage.getseatiTagValue();
		assertEquals(seatiTagValueInNumberPopup,
				"The MRR will be calculated based on the Total Seats, and not based on the users created");
		String seatValueInNumberPopup = addNumberPage.getSeatCount();
		assertEquals(seatValueInNumberPopup, data.getValue("seatcount"), "validate seat count in number popup");

		// enter seat value less than one
		addNumberPage.enterSeatCount("0");
		assertEquals(inviteUserPageObj.validateErrormessage(), "You have to choose at least one seat.");
		Common.pause(5);
		// enter seat value grater than 10
		addNumberPage.enterSeatCount("11");
		assertEquals(inviteUserPageObj.validateErrormessage(), "You Can not choose more than 10 seat.");

		// update seat count less than 11 and grater than 0
		addNumberPage.enterSeatCount("10");
		assertEquals(addNumberPage.getSeatCount(), "10",
				"validate seat count in number purchase popup after updated seat count");
		addNumberPage.clickOnCheckoutButton();
		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, "10"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();
		addNumberPage.clickOnNotNowImDonePopup();
		Common.pause(5);
		logout(webAppMainUserLogin);
		loginWeb(webAppMainUserLogin, signUpUserEmailId, excel.getdata(0, 27, 2));
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		assertEquals(addNumberPage.getCreditFromSideMenu(),
				planPrice.getField("monthlyCredit", "select * from Sheet1 where plan='Bronze'"));
		assertEquals(addNumberPage.getFreeIncomingMinutesFromDashboard(),
				planPrice.getField("feeInMinutesMonthly", "select * from Sheet1 where plan='Bronze'"));
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("bronze"));
		String actualPriceFromManageSubscription = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceBronzeMonthly, "10"));

		// 3 after added number verify seat count in plan and billing page
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountAfternumberPurchased = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountAfternumberPurchased, "10", "validate seat count after added number");
		String seatiTagValue1 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue1,
				"The charges will be calculated based on the total seats.");

		// upgrade plan from bronze monthly to Silver Monthly
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		planAndBillingPage.clickOnSilverRadioButton();
		planAndBillingPage.clickOnMonthlyRadioButton();
		planAndBillingPage.clickOnPlanUpgradeButton();
		assertTrue(inviteUserPageObj.validatePopupMessageWithChargeInBillingPageWhilePlanUpgrade(addNumberPage
				.subtractionOfTwoStringNumber(
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceSilverMonthly, "10"),
						addNumberPage.multiplicationOfTwoStringNumber(
								planPriceBronzeMonthly, "10"))
				.replace(",", "")), "validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnUpgradeButton();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Plan upgrade successfully");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription6 = addNumberPage.getPlanPriceFromManageSubscription().replace(",", "");
		assertEquals(actualPriceFromManageSubscription6, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, "10"));

		// update seat count and increase seat count is 11
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		inviteUserPageObj.enterSeatCount("11");
		assertTrue(
				inviteUserPageObj.validatePopupMessageWithChargeInBillingPage(
						planPriceSilverMonthly),
				"validate charge while update seat. (popup message)");
		inviteUserPageObj.clickOnYesButtonOfUpdateSeatPopup();
		assertEquals(inviteUserPageObj.validateSuccessMessage(), "Total seat updated to 11");
		addNumberPage.openAccountDetailsPopup();
		assertTrue(addNumberPage.getPlanDetailsFromManageSubscription().toLowerCase().contains("silver"));
		String actualPriceFromManageSubscription2 = addNumberPage.getPlanPriceFromManageSubscription();
		assertEquals(actualPriceFromManageSubscription2, addNumberPage.multiplicationOfTwoStringNumber(
				planPriceSilverMonthly, "11"));
		Common.pause(25);
		driverWebApp1.navigate().to(url.signIn());
		webAppMainUserLogin.clickLeftMenuPlanAndBilling();
		String seatCountUpdated = inviteUserPageObj.verifySeatCount();
		assertEquals(seatCountUpdated, "11", "validate seat count after updated seat count");
		String seatiTagValue4 = inviteUserPageObj.getTotalSeatiTagValue();
		assertEquals(seatiTagValue4,
				"The charges will be calculated based on the total seats.");

	}

}
