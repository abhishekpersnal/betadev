package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.CallHippo.Dialer.WebToggleConfiguration;
import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;
import com.CallHippo.Init.Retry;
import com.CallHippo.Init.TestBase;
import com.CallHippo.Init.XLSReader;
import com.CallHippo.web.authentication.LoginPage;
import com.CallHippo.web.authentication.SignupPage;

public class UserSearchAndPaginationTest {
WebDriver driver;
	
	AddNumberPage addNumberPage;
	SignupPage registrationPage;
	LoginPage loginpage;
	UserSearchAndPaginationPage userSearchAndPaginationPage;
	
	Common signupExcel;
	PropertiesFile url;
	XLSReader planPrice;
	XLSReader numberPrice;
	PropertiesFile data;

	WebToggleConfiguration webAppMainUserLogin;
	InviteUserPage inviteUserPageObj;

	String newEmail = null;
	String gmailUser;
	String getNumber;
	String locationName;
	String locationCode;

	public UserSearchAndPaginationTest() throws Exception {
		url = new PropertiesFile("Data\\url Configuration.properties");
		data = new PropertiesFile("Data\\url Configuration.properties");
		signupExcel = new Common(data.getValue("environment")+"\\Signup.xlsx");
		planPrice = new XLSReader("Data\\PlanPrice.xlsx");
		numberPrice = new XLSReader("Data\\numberprices.xlsx");

		gmailUser = data.getValue("gmailUser");
	}

	@BeforeTest
	public void initialization() throws Exception {
		driver = TestBase.init();
		addNumberPage = PageFactory.initElements(driver, AddNumberPage.class);
		registrationPage = PageFactory.initElements(driver, SignupPage.class);
		loginpage = PageFactory.initElements(driver, LoginPage.class);
		userSearchAndPaginationPage = PageFactory.initElements(driver, UserSearchAndPaginationPage.class);

		webAppMainUserLogin = PageFactory.initElements(driver, WebToggleConfiguration.class);
		inviteUserPageObj = PageFactory.initElements(driver, InviteUserPage.class);

		driver.get(url.signIn());
	}
	
	@BeforeMethod
	public void beforeMethod() throws IOException {
		driver.navigate().to(url.usersPage());
	}

	@AfterMethod
	public void afterMethodProcess(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = "Fail";
			Common.Screenshot(driver, testname, " Fail " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());

		} else {
			String testname = "Pass";
			Common.Screenshot(driver, testname, " Pass " + result.getMethod().getMethodName());
			System.out.println(testname + " - " + result.getMethod().getMethodName());
		}
		
	}
	
	@AfterTest (alwaysRun = true) 
	public void tearDown() {
		driver.quit();
	}

	public void logout(WebToggleConfiguration web) {
		web.clickOnLogOut();
	}
	
	private String signup() throws Exception {
		try {
			driver.get(url.livesignUp());
		} catch (IOException e) {
		}
		String date = signupExcel.date();
		String email = gmailUser + "+" + date + "@callhippo.com";
		newEmail = email;
		registrationPage.clickOn_website_header_signUp_Btn();
		registrationPage.enter_website_signUp_popUp_firstName(signupExcel.getdata(0, 23, 2));
		registrationPage.enter_website_signUp_popUp_lastName(signupExcel.getdata(0, 24, 2));
		registrationPage.enter_website_signUp_popUp_companyName(signupExcel.getdata(0, 25, 2));
		registrationPage.enter_website_signUp_popUp_workEmail(email);
		registrationPage.enter_website_signUp_popUp_phoneNumber(signupExcel.getdata(0, 26, 2));
		registrationPage.enter_website_signUp_popUp_password(signupExcel.getdata(0, 27, 2));
	    registrationPage.clickOn_website_signUp_popUp_signUpButton();
        Common.pause(2);
	    assertEquals(registrationPage.getTextMainTitle(),  "Thank you for Signing up.");
		registrationPage.ValidateVerifyButtonOnGmail();

		registrationPage.verifyWelcomeToCallHippoPopup();
		registrationPage.closeAddnumberPopup();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		System.out.println(email);
		return email;
	}

	
	public String addFirstNumberForUser() throws Exception {
		String email = signup();
		
//		loginpage.enterEmail("jayadip+17_02_2021_15_15_48@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		
		driver.get(url.numbersPage());
		addNumberPage.clickOnAddNumberInNumbersPage();
		addNumberPage.clickOnCountry("United States");

		addNumberPage.selectNumberTypeByText("local");
		addNumberPage.selectSearchByText("Number");
	//	addNumberPage.enterPrefixOrNumber("14");

		//assertTrue(addNumberPage.getFirstNumberOfNumberListPage().contains("14"));
		
		String numberBeforePurchased = addNumberPage.getFirstNumberOfNumberListPage();
		getNumber = addNumberPage.getFirstNumberOfNumberListPage();
		addNumberPage.clickOnFirstNumberOfNumberListPage();
		
		addNumberPage.clickOnMonthlyPlan();

		String actualPlanPrice = addNumberPage.getBronzePlanPrice();
		assertEquals(actualPlanPrice, planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"));

		addNumberPage.selectBronzePlan();
		addNumberPage.clickOnCheckoutButton();

		String priceAfterCheckout = addNumberPage.getPlanPriceFromYourOrderPopup();
		assertEquals(priceAfterCheckout, addNumberPage.multiplicationOfTwoStringNumber(planPrice.getField("monthly", "select * from Sheet1 where plan='Bronze'"),"2"));

		//assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().contains("Bronze"));
		assertTrue(addNumberPage.getPlanDetailsFromYourOrderPopup().toLowerCase().contains("bronze"));
		addNumberPage.clickOnypProceedToCheckoutFromYourOrderPopup();
		addNumberPage.addDetailsInCard();

		addNumberPage.clickOnNotNowImDonePopup();
		addNumberPage.clickOnSaveNumberButton();

		assertTrue(addNumberPage.validateNumberAddedSuccessfully(numberBeforePurchased));

		registrationPage.userLogout();

		loginpage.enterEmail(email);
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();

		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
		return email;
		
}
	
	public void invite_Users() throws Exception {

	//	webAppMainUserLogin.deleteInvitedUserMail();
		
//		String inviteMainUserEmailWithOneNumber = addFirstNumberForUser();
		webAppMainUserLogin.numberspage();
		Common.pause(3);
		String number= addNumberPage.getFirstNumberOfNumberPage();
		webAppMainUserLogin.userspage();
		inviteUserPageObj.clickOnInviteUserButton();

		String date = signupExcel.date();
		Common.pause(1);
		String date1 = signupExcel.date();
		String email1 = gmailUser + "+" + date + "@callhippo.com";
		
//		String email2 = gmailUser + "+" + date1 + "@callhippo.com";

		System.out.println("---subUserMail---"+email1);
		inviteUserPageObj.enterInviteUserEmail(email1);
		Common.pause(1);
		inviteUserPageObj.clickSaveButton();
		Common.pause(1);
//		inviteUserPageObj.clickOnAddButton();
//		Common.pause(1);
//		inviteUserPageObj.enterInviteUserEmail(email2);
//		inviteUserPageObj.clickSaveButton();
//		Common.pause(1);
		inviteUserPageObj.selectNumberCheckbox(number);
		Common.pause(2);
		inviteUserPageObj.clickInviteNUserButton();
		try {
			String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		}catch (Exception e) {
			inviteUserPageObj.clickOnYes();
			String acutualSuccessfulValidationMsg2 = inviteUserPageObj.validateSuccessMessage();
			String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
			assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
		}
		
}
	


	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void login() throws Exception {
		
		loginpage.enterEmail("automation+09_03_2021_19_08_08@callhippo.com");
		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
		loginpage.clickOnLogin();
		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
	}
	
	@Test(priority = 2, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_special_character_notAllowed_in_search() throws Exception {
//		loginpage.enterEmail("automation+09_03_2021_19_08_08@callhippo.com");
//		loginpage.enterPassword(signupExcel.getdata(0, 5, 2));
//		loginpage.clickOnLogin();
//		assertEquals("Dashboard | Callhippo.com", loginpage.loginSuccessfully());
//		Common.pause(3);
//		driver.get(url.numbersPage());
		
		Common.pause(3);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
//		String randomNumberFromRecords = userSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(userSearchAndPaginationPage.getTotalRows()));
		
//		String subStringFromNumber = userSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,3,6);
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("~");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("`");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("!");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("#");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("$");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("%");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("^");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("&");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("*");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("(");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(")");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("{");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("}");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("[");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("]");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("|");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("\\");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(":");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(";");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("'");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("<");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(",");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(">");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("?");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("/");
		assertTrue(userSearchAndPaginationPage.verifyNoDataTxtPresent());
		
	}
	
	@Test(priority = 3, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_user_search_with_specific_number_string() throws Exception {
		
		Common.pause(3);
		driver.navigate().to(url.usersPage());
		Common.pause(10);
		String randomNumberFromRecords = userSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(userSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = userSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,17,30);
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult("2021_19_08_11");
	}
	
	@Test(priority = 4, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_number_search_with_plus() throws Exception {
		
		String randomNumberFromRecords = userSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(userSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = userSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,0,11);
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 5, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_number_search_with_minus() throws Exception {
		
		String randomNumberFromRecords = userSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(userSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = userSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,0,10)+"-";
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 6, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_number_search_with_multiply() throws Exception {
		
		String randomNumberFromRecords = userSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(userSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = userSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,0,10)+"*";
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 7, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_number_search_with_divide() throws Exception {
		
		String randomNumberFromRecords = userSearchAndPaginationPage.randomlyGetUserNumber(Common.randomNumericValueGenerate(userSearchAndPaginationPage.getTotalRows()));
		
		String subStringFromNumber = userSearchAndPaginationPage.getStringFromNumber(randomNumberFromRecords,0,10)+"/";
		
		userSearchAndPaginationPage.verifyUserNameInSearchResult(subStringFromNumber);
	}
	
	@Test(priority = 8, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_special_character_notAllowed_in_UserName() throws Exception {
		Common.pause(3);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.clickOnUserSeetingsIcon();
		userSearchAndPaginationPage.clickUserNameEditIcon();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"~");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"`");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"!");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"@");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"#");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"$");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"%");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"^");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"&");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"*");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"(");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+")");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"+");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"=");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"{");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"}");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"[");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"]");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"|");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"\\");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"'");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+";");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+":");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"/");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"?");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+".");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+">");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+",");
		userSearchAndPaginationPage.validate_UserName();
		
		userSearchAndPaginationPage.insert_UserName("Test-Automation"+"<");
		userSearchAndPaginationPage.validate_UserName();
	}

	@Test(priority = 9, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void validate_userName_on_userSearch() throws Exception {
		Common.pause(3);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.clickOnUserSeetingsIcon();
		userSearchAndPaginationPage.clickUserNameEditIcon();
		userSearchAndPaginationPage.insert_UserName("Test-Automation");
		userSearchAndPaginationPage.save_userName();
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(2);
		userSearchAndPaginationPage.verifyUpdatedUserNameInSearchResult("Test-Automation");
		
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.clickOnUserSeetingsIcon();
		userSearchAndPaginationPage.clickUserNameEditIcon();
		userSearchAndPaginationPage.insert_UserName("Test Automation_");
		userSearchAndPaginationPage.save_userName();
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(2);
		userSearchAndPaginationPage.verifyUpdatedUserNameInSearchResult("Test Automation_");
		
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.clickOnUserSeetingsIcon();
		userSearchAndPaginationPage.clickUserNameEditIcon();
		userSearchAndPaginationPage.insert_UserName("Test_Automation_");
		userSearchAndPaginationPage.save_userName();
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(2);
		userSearchAndPaginationPage.verifyUpdatedUserNameInSearchResult("Test_Automation_");
		
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.clickOnUserSeetingsIcon();
		userSearchAndPaginationPage.clickUserNameEditIcon();
		userSearchAndPaginationPage.insert_UserName(" _Test Automation");
		userSearchAndPaginationPage.save_userName();
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(2);
		userSearchAndPaginationPage.verifyUpdatedUserNameInSearchResult("_Test Automation");
	}
	
	@Test(priority = 10, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void verifyPreviousPageIsDisble_1st_time_On_NumbersPage() throws Exception {
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		assertTrue(userSearchAndPaginationPage.verifyPreviousBtnPaginationDisable());
	}
	
	@Test(priority = 11, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void verifyNextPageBtn_enable_for_more_records() throws Exception {
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.verifyNextPageEnable();
	}
	
	@Test(priority = 12, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void verifyNextPageBtn_disable_lastPageOfPagination() throws Exception {
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.verifyNextPageDisableAtlastPageOfPagination();
	}
	
	@Test(priority = 13, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void verify_activePageInPagination() throws Exception {
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(3);
		userSearchAndPaginationPage.verifyActivePageInPagination();
	}
	
	@Test(priority = 14, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void delete_user_from_lastPage_check_pagenation() throws Exception {
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(10);
		Integer lastpage = userSearchAndPaginationPage.getPaginationSize();
		Common.pause(2);
		userSearchAndPaginationPage.clickOnlastPageOfPagination();
		
		if (lastpage == 4)
		{
			Integer totalrow = userSearchAndPaginationPage.getTotalRows();
			for(int i=0;i<totalrow;i++) {
				userSearchAndPaginationPage.deleteUsers();
			}
		}
		if (lastpage == 3)
		{
			Integer totalrow1 = userSearchAndPaginationPage.getTotalRows();
			if (totalrow1<10) {
				for(int i=0;i<(10-totalrow1);i++) {
					invite_Users();
				}
			}
		}
		Common.pause(3);
		invite_Users();
		userSearchAndPaginationPage.verifyPaginationAfterUserDeleteFromLastPage();
	}
	
	@Test(priority = 15, dependsOnMethods = "login", retryAnalyzer = Retry.class)
	public void delete_number_from_2ndLastPage_check_pagenation() throws Exception {
		Common.pause(2);
		driver.navigate().to(url.usersPage());
		Common.pause(10);
		invite_Users();
		userSearchAndPaginationPage.verifyPaginationAfterUserDeleteFrom2ndLastPage();
	}
	
}
