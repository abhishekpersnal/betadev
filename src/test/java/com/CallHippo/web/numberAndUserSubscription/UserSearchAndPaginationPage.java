package com.CallHippo.web.numberAndUserSubscription;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import com.CallHippo.Init.Common;
import com.CallHippo.Init.PropertiesFile;

public class UserSearchAndPaginationPage {
	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	WebDriver driver1;
	PropertiesFile credential;
	JavascriptExecutor js;

	@FindBy(xpath = "//input[@placeholder='Search']")
	WebElement searchInput;
	
	@FindBy(xpath = "//p[contains(.,'No Data')]")
	WebElement noDataTxt;

	public UserSearchAndPaginationPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 60);
		action = new Actions(this.driver);
		js = (JavascriptExecutor) driver;
		
		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
		} catch (Exception e) {
		}
	}

	public boolean verifyNoDataTxtPresent() {
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//p[contains(.,'No Data')]"));
		if (ele.size() != 0) {
			elementIsAvailable = true;
		} else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}
	
	public void clickOnUserSeetingsIcon() {

		WebElement userSetting = driver
				.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr[1]//td//a[@class='settingcircle userSettingIcon']"));
		wait.until(ExpectedConditions.visibilityOf(userSetting));
		userSetting.click();
	}
	
	public void clickUserNameEditIcon() {

		wait.until(ExpectedConditions.visibilityOf(driver
				.findElement(By.xpath("(//div[@class='deparmentname_div2']//button[@class[contains(.,'editbtn')]])[1]"))))
				.click();
	}
	
	public void insert_UserName(String value) {
		Common.pause(1);
		WebElement departmentNameInputStr = driver.findElement(
				By.xpath("(//div[@class[contains(.,'editdepartment')]]//input[@id[contains(.,'contactName')]])[1]"));
		wait.until(ExpectedConditions.visibilityOf(departmentNameInputStr)).click();
		departmentNameInputStr.clear();
		departmentNameInputStr.sendKeys(value);
	}
	
	public void save_userName() {
		Common.pause(1);
		WebElement saveDepartmentName = driver
				.findElement(By.xpath("(//div[@class[contains(.,'deparmentname')]]//i[contains(.,'save')])[1]"));
		wait.until(ExpectedConditions.visibilityOf(saveDepartmentName)).click();
	}
	
	public void validate_UserName() {
		boolean elementIsAvailable;
		List<WebElement> specialCharText = driver.findElements(By.xpath(
				"//div[@class='deparmentname_title']//following-sibling::div[contains(.,'Special characters are not allowed')]"));
		if (specialCharText.size() != 0) {
			elementIsAvailable = true;
		} else {
			elementIsAvailable = false;
		}
		assertTrue(elementIsAvailable);
	}
	
	public boolean verifyNextBtnPaginationEnable() {
		boolean elementIsAvailable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Next Page'][@aria-disabled='false']"));
		if (ele.size() != 0) {
			elementIsAvailable = true;
		} else {
			elementIsAvailable = false;
		}
		return elementIsAvailable;
	}
	
	public Integer getTotalRows() {
		boolean elementIsAvailable;
		//List<WebElement> numRows = driver.findElements(By.xpath("//div[@class[contains(.,'user')]]//tbody//tr"));
		List<WebElement> numRows = driver.findElements(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr"));
		int numRowsInt = numRows.size();
System.out.println("totalrows::"+numRowsInt);
		return numRowsInt;
	}
	
	public String randomlyGetUserNumber(Integer randomdigit) {
		if (randomdigit == 0) {
			randomdigit = 2;
		}
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr["
				+ randomdigit + "]//td[2]/span"))));
		String getUserId = driver.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr["
				+ randomdigit + "]//td[2]/span")).getText();

		System.out.println("---getUserId--" + getUserId);
		return getUserId;
	}
	
	public String getStringFromNumber(String number, Integer startIndex, Integer endIndex) {

		System.out.println(number.substring(startIndex, endIndex));

		return number.substring(startIndex, endIndex);
	}
	
	public void verifyUserNameInSearchResult(String userSearchString) {
		boolean recordMatch;
		searchInput.clear();
		searchInput.sendKeys(userSearchString);
//		for (int j = 0; j < userSearchString.length(); j++) {
//			searchInput.sendKeys(Character.toString(userSearchString.charAt(j)));
//			Common.pause(1);
//		}

		Common.pause(20);

		Integer rowsAfterSearch = getTotalRows();

		int ext = 0;
		if (rowsAfterSearch != 0) {
			do {

				ext = ext + 1;

				if (ext == 1) {
					for (int i = 1; i <= rowsAfterSearch; i++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr[" + i
										+ "]//td[2]"))
								.getText();
						recordMatch = number.contains(userSearchString);

						assertEquals(true, recordMatch, "Search string " + userSearchString
								+ " is not matched with number-- " + number);

					}

				} else {

					driver.findElement(By.xpath("//li[@title='Next Page'][@aria-disabled='false']/a")).click();

					Common.pause(5);
					Integer rowsAfterSearchnew = getTotalRows();

					for (int j = 1; j <= rowsAfterSearchnew; j++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr[" + j
										+ "]//td[2]"))
								.getText();
						recordMatch = number.contains(userSearchString);

						assertEquals(true, recordMatch, "Search string " + userSearchString
								+ " is not matched with number-- " + number);

					}
				}
				if (ext == 10) {
					break;
				}

			} while (verifyNextBtnPaginationEnable() == true);

		}
	}
	
	public void verifyUpdatedUserNameInSearchResult(String userSearchString) {
		boolean recordMatch;
		searchInput.clear();
		searchInput.sendKeys(userSearchString);
//		for (int j = 0; j < userSearchString.length(); j++) {
//			searchInput.sendKeys(Character.toString(userSearchString.charAt(j)));
//			Common.pause(1);
//		}

		Common.pause(20);

		Integer rowsAfterSearch = getTotalRows();

		int ext = 0;
		if (rowsAfterSearch != 0) {
			do {

				ext = ext + 1;

				if (ext == 1) {
					for (int i = 1; i <= rowsAfterSearch; i++) {
						String username = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr[" + i
										+ "]//td[1]"))
								.getText();
						recordMatch = username.contains(userSearchString);

						assertEquals(true, recordMatch, "Search string " + userSearchString
								+ " is not matched with number-- " + username);

					}

				} else {

					driver.findElement(By.xpath("//li[@title='Next Page'][@aria-disabled='false']/a")).click();

					Common.pause(5);
					Integer rowsAfterSearchnew = getTotalRows();

					for (int j = 1; j <= rowsAfterSearchnew; j++) {
						String number = driver
								.findElement(By.xpath("//div[@class[contains(.,'numbertable')]]//tbody//tr[" + j
										+ "]//td[2]"))
								.getText();
						recordMatch = number.contains(userSearchString);

						assertEquals(true, recordMatch, "Search string " + userSearchString
								+ " is not matched with number-- " + number);

					}
				}
				if (ext == 10) {
					break;
				}

			} while (verifyNextBtnPaginationEnable() == true);

		}else {
			assertTrue(false);
		}
	}
	
	public boolean verifyPreviousBtnPaginationDisable() {
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Previous Page'][@aria-disabled='true']"));
		if (ele.size() != 0) {
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}
	
	public Integer getPaginationSize() {
		List<WebElement> pagination = driver.findElements(By
				.xpath("//ul[@class[contains(.,'pagination')]]/li"));
		WebElement lastpagination = driver.findElement(By
				.xpath("//li[@title='Next Page'][@aria-disabled='false']/preceding-sibling::li[1]"));
		Integer paginationsize = Integer.parseInt(lastpagination.getAttribute("title"));
		//int paginationSize = pagination.size();
		
		return paginationsize;
	}
	
	public void clickOnlastPageOfPagination() {
		
		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);
		
		if(totalpageSize>1) {
			driver.findElement(By
					.xpath("//li[@title='Next Page'][@aria-disabled='false']/preceding-sibling::li[1]")).click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
		}
	}
	
	public void deleteUsers() {

		WebElement lastpagination = driver.findElement(By
				.xpath("//li[@title='Next Page'][@aria-disabled='true']/preceding-sibling::li[1]"));
		Integer totalpageSize = Integer.parseInt(lastpagination.getAttribute("title"));
		
		try {
		driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
		}catch (Exception e) {
		WebElement invitebutton = driver.findElement(By.xpath("//tr[@data-row-key='0']"));
		Actions action = new Actions(driver);
		action.click(invitebutton).build().perform();
		driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
		}
		Common.pause(2);
		driver.findElement(
				By.xpath("//div[@id[contains(.,Dialog)]][contains(.,'Forget user')]/..//following-sibling::div//button[contains(.,'Yes')]"))
				.click();
		Common.pause(2);
	}
	
	public boolean verifyNextBtnPaginationDisable() {
		boolean elementIsDisable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Next Page'][@aria-disabled='true']"));
	
		if (ele.size() != 0) {
		
			elementIsDisable = true;
		} else {
			elementIsDisable = false;
		}
		return elementIsDisable;
	}
	
	public void verifyNextPageEnable() {
		
		Integer totalpageSize = getPaginationSize();
//		System.out.println("---paginationSize.size()--" + paginationSize);
//		Integer totalpageSize = paginationSize-2;
		System.out.println("---totalpageSize--" + totalpageSize);
		
		if(totalpageSize==1) {
			assertTrue(verifyNextBtnPaginationDisable());
		}
		if(totalpageSize>1) {
			assertTrue(verifyNextBtnPaginationEnable());
		}
	}
	
	public boolean verifyPreviousBtnPaginationEnable() {
		boolean elementIsEnable;
		List<WebElement> ele = driver.findElements(By.xpath("//li[@title='Previous Page'][@aria-disabled='false']"));
		if (ele.size() != 0) {
			elementIsEnable = true;
		} else {
			elementIsEnable = false;
		}
		return elementIsEnable;
	}
	
	public void verifyNextPageDisableAtlastPageOfPagination() {
		
		Integer totalpageSize = getPaginationSize();
//		System.out.println("---paginationSize.size()--" + paginationSize);
//		Integer totalpageSize = paginationSize-2;
		System.out.println("---totalpageSize--" + totalpageSize);
		
		if(totalpageSize>1) {
			driver.findElement(By
					.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='"+totalpageSize+"']")).click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
		}
	}
	
	public void verifyActivePageInPagination() {

		Integer totalpageSize = getPaginationSize();
//		System.out.println("---paginationSize.size()--" + paginationSize);
//		Integer totalpageSize = paginationSize - 2;
		System.out.println("---totalpageSize--" + totalpageSize);

		if (totalpageSize > 1) {
			driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + totalpageSize + "']"))
					.click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
			
			driver.findElement(
					By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + (totalpageSize - 1) + "']"))
					.click();
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationEnable());
		}
	}
	
	public void verifyPaginationAfterUserDeleteFromLastPage() {

		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);

		if (totalpageSize > 1) {
			driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + totalpageSize + "']"))
					.click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
			
			driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
			Common.pause(2);
			driver.findElement(
					By.xpath("//div[@id[contains(.,Dialog)]][contains(.,'Forget user')]/..//following-sibling::div//button[contains(.,'Yes')]"))
					.click();
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationDisable());
		}
	}
	
	public void verifyPaginationAfterUserDeleteFrom2ndLastPage() {

		Integer totalpageSize = getPaginationSize();
		System.out.println("---totalpageSize--" + totalpageSize);

		if (totalpageSize > 1) {
			driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + totalpageSize + "']"))
					.click();
			Common.pause(2);
			assertTrue(verifyNextBtnPaginationDisable());
			assertTrue(verifyPreviousBtnPaginationEnable());
			
			driver.findElement(
					By.xpath("//ul[@class[contains(.,'pagination')]]/li/a[text()='" + (totalpageSize - 1) + "']"))
					.click();
			
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationEnable());
			try {
			driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
			}catch (Exception e) {
				WebElement invitebutton = driver.findElement(By.xpath("//tr[@data-row-key='0']"));
				Actions action = new Actions(driver);
				action.click(invitebutton).build().perform();
				driver.findElement(By.xpath("(//span[contains(.,'delete_forever')]/i)[1]")).click();
			}
			Common.pause(2);
			driver.findElement(
					By.xpath("//div[@id[contains(.,Dialog)]][contains(.,'Forget user')]/..//following-sibling::div//button[contains(.,'Yes')]"))
					.click();
			Common.pause(2);
			assertTrue(driver.findElement(By.xpath("//ul[@class[contains(.,'pagination')]]/li[@title='"
					+ (totalpageSize - 1) + "'][@class[contains(.,'active')]]")).isDisplayed());
			
			assertTrue(verifyNextBtnPaginationDisable());
		}
	}

}
