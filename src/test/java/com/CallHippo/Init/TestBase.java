package com.CallHippo.Init;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {

	static WebDriver driver;
	// public static WebDriver driver1 =null;

	public static WebDriver init1() throws MalformedURLException {

		final String USERNAME = "bsuser74416";
		final String AUTOMATE_KEY = "VYzNesrDGwaVQXzh37Cj";
		final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		options.addArguments("--use-fake-ui-for-media-stream");

		DesiredCapabilities caps = DesiredCapabilities.chrome();

		// DesiredCapabilities caps = new DesiredCapabilities();

		caps.setCapability("os", "Windows");
		caps.setCapability("os_version", "10");
		caps.setCapability("browser", "Chrome");
		caps.setCapability("browser_version", "80");
		caps.setCapability("browserstack.idleTimeout", "300");
		caps.setCapability("name", "jaydeepprajapati2's First Test");
		caps.setCapability(ChromeOptions.CAPABILITY, options);

		driver = new RemoteWebDriver(new URL(URL), caps);

		driver.manage().window().maximize();
		// driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		return driver;

	}

	public static WebDriver init() {
		DesiredCapabilities dc = new DesiredCapabilities();
		System.setProperty("webdriver.chrome.driver", "Data\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		options.addArguments("--use-fake-ui-for-media-stream");
		
		HashMap<String, Object> chromePref = new HashMap<>();

		chromePref.put("download.default_directory", System.getProperty("user.dir")+"\\download_files");

		options.setExperimentalOption("prefs", chromePref);
		
		dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

		
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		// driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

		return driver;

	}
	
	public static WebDriver init_dialer() {

		System.setProperty("webdriver.chrome.driver", "Data\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		options.addArguments("--use-fake-ui-for-media-stream");
		options.addArguments("--auto-open-devtools-for-tabs");

		
		
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		// driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

		return driver;
	}
	
	public static WebDriver init2() {
		DesiredCapabilities dc = new DesiredCapabilities();

		System.setProperty("webdriver.chrome.driver", "Data\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		options.addArguments("--use-fake-ui-for-media-stream");
		
		HashMap<String, Object> chromePref = new HashMap<>();

		chromePref.put("download.default_directory", System.getProperty("user.dir")+"\\download_files");

		options.setExperimentalOption("prefs", chromePref);
		dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		// driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

		return driver;

	}
	public static WebDriver init3() {
		DesiredCapabilities dc = new DesiredCapabilities();

		System.setProperty("webdriver.chrome.driver", "Data\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		options.addArguments("--use-fake-ui-for-media-stream");
		HashMap<String, Object> chromePref = new HashMap<>();

		chromePref.put("download.default_directory", System.getProperty("user.dir")+"\\download_files");

		options.setExperimentalOption("prefs", chromePref);
		dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
//		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

		return driver;

	}
	
	public static WebDriver init4() {
		DesiredCapabilities dc = new DesiredCapabilities();
		System.setProperty("webdriver.chrome.driver", "Data\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		options.addArguments("--use-fake-ui-for-media-stream");
		options.addArguments("--auto-open-devtools-for-tabs");
		HashMap<String, Object> chromePref = new HashMap<>();

		chromePref.put("download.default_directory", System.getProperty("user.dir")+"\\download_files");

		options.setExperimentalOption("prefs", chromePref);

		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		// driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

		return driver;

	}

}
