package com.CallHippo.Init;

import static org.testng.Assert.assertEquals;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class RestAPI {

	PropertiesFile credential;
	private String userID;
	private static DecimalFormat df2 = new DecimalFormat("#.###");
	String apiURL;

	public RestAPI() {

		try {
			credential = new PropertiesFile("Data\\url Configuration.properties");
			userID = credential.userId();
			apiURL = credential.apiURL();
		} catch (Exception e) {
		}

	}
	
	public int getAvailableSMSCredit(String userId,String fieldName) {
		// field name should be ::  usedIncomingSms , freeIncomingSms , usedSms , freeSms , dailySmsLimit , todaySmsUsed
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("script/"+userId+"/smsGetUpdate");
		requestSpec.header("content-type","application/json");
		
		JSONObject request = new JSONObject();
		request.put("action", "get");
		request.put("incomingSms", "1");
		request.put("usedIngoingSms", "1");
		request.put("usedOutgoingSms", "1");
		request.put("outgoingSms", "1");
		request.put("smsPerDay", "1");
		
		requestSpec.body(request.toString());
		Response response = requestSpec.post("");
		int a;
		if(fieldName.contentEquals("todaySmsUsed")) {
			try {
			 a = response.path("data."+fieldName+".count");
				System.out.println("todaySmsUsed:"+a);
			}catch (Exception e) {
				a = 0;
				
			}
		}else {
			 a = response.path("data."+fieldName);
				System.out.println(fieldName+":"+a);
		}
		
		int StatusCode = response.getStatusCode();
		return a;
		
	}
	
	public String getPlanPrice(String planName, String planPeriod) {
		// plan name should be:: bronze, silver, platinum
		// plan period should be:: monthly, annually
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("script/systemprice");
		requestSpec.header("content-type","application/json");
		
		JSONObject request = new JSONObject();
		request.put("type", "plan");
		request.put("plan", planName);
		
		requestSpec.body(request.toString());
		Response response = requestSpec.post("");
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		String a = null;
		if(planPeriod.equalsIgnoreCase("monthly")) {
			Common.pause(2);
			 list = response.path("planData.monthly_charge");
			 a=Integer.toString(list.get(0));
			System.out.println(planName+" monthly price:"+a);
			
		}
		if(planPeriod.equalsIgnoreCase("annually")){
			Common.pause(2);
			 list = response.path("planData.annual_charge");
			 a=Integer.toString(list.get(0));
			System.out.println(planName+" annually price:"+a);
		}
		
		int StatusCode = response.getStatusCode();
		return a;
		
	}
	
	public String getNumberPrice(String countryName, String numberType, String NumberPeriod) {
		// period should be monthly or annually
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("script/systemprice");
		requestSpec.header("content-type","application/json");
		
		JSONObject request = new JSONObject();
		request.put("type", "number");
		request.put("country", countryName);
		request.put("numberType", numberType);
		
		requestSpec.body(request.toString());
		Response response = requestSpec.post("");
		ArrayList<Integer> list = new ArrayList<Integer>();
		String a = null;
		
		if(NumberPeriod.equalsIgnoreCase("monthly")) {
			
			 list = response.path("data.monthly");
			 a=Integer.toString(list.get(0));
			 System.out.println(countryName+" "+ numberType+" monthly price:"+a);
			
		}
		if(NumberPeriod.equalsIgnoreCase("annually")){
			 list = response.path("data.annually");
			 a=Integer.toString(list.get(0));
			System.out.println(countryName+" "+ numberType+" annually price:"+a);
		}
		
		int StatusCode = response.getStatusCode();
		return a;
		
	}

	

	public void updateSMSCredit(String userId, String incomingSms, String usedIngoingSms, String usedOutgoingSms,
			String outgoingSms, String smsPerDay) {
		System.out.println("------userID-------"+userId);
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://" + apiURL + "-app.callhippo.com");
		requestSpec.basePath("script/" + userId + "/smsGetUpdate");
		requestSpec.header("content-type", "application/json");

		JSONObject request = new JSONObject();
		request.put("action", "update");
		request.put("incomingSms", incomingSms);
		request.put("usedIngoingSms", usedIngoingSms);
		request.put("usedOutgoingSms", usedOutgoingSms);
		request.put("outgoingSms", outgoingSms);
		request.put("smsPerDay", smsPerDay);

		requestSpec.body(request.toString());
		Response response = requestSpec.post("");
		
		int StatusCode = response.getStatusCode();
		System.out.println( "StatusCode:"+StatusCode);
		assertEquals(StatusCode, 200, "update sms credit");

	}

	public String getAvailableCredit() {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("numberUserBilling/" + userID);
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID);
		try {
			int a = response.path("data.response.availableCredits");

			Integer b = new Integer(a);
			float c = b.floatValue();

			System.out.println(String.format("%.2f", c));
			return String.format("%.2f", c);
		} catch (Exception e) {
			float a = response.path("data.response.availableCredits");

			System.out.println(String.format("%.2f", a));
			return String.format("%.2f", a);
		}
	}

	public void deleteRechargeActivity() {
		given().delete("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID).then().statusCode(200);
	}
	
	public void deleteRechargeActivity(String userId) {
		given().delete("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userId).then().statusCode(200);
	}

	public void UpdateCredit(String availableCredit, String freeInMinutes, String freeOutMinutes) {
		UpdateCreditCountyWisezero(userID);
		JSONObject request = new JSONObject();

		request.put("update", "all");
		request.put("freeOutMinutes", freeOutMinutes);
		request.put("freeInMinutes", freeInMinutes);
		request.put("availableCredits", availableCredit);
		request.put("freeTotalMinutes", "0");
//		System.out.println(request.toJSONString());
		given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID).then().statusCode(200);
	}

	public void UpdateCredit(String availableCredit, String freeInMinutes, String freeOutMinutes, String userID) {
		
		//UpdateCreditCountyWisezero(userID);
		JSONObject request = new JSONObject();

		request.put("update", "all");
		request.put("freeOutMinutes", freeOutMinutes);
		request.put("freeInMinutes", freeInMinutes);
		request.put("freeTotalMinutes", "0");
		request.put("availableCredits", availableCredit);
//		System.out.println(request.toJSONString());
		given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID).then().statusCode(200);
	}
	
public void UpdateCustomCredit(String availableCredit, String freeInMinutes, String freeOutMinutes, String freeTotalMinutes, String userID) {
		
		UpdateCreditCountyWisezero(userID);
		JSONObject request = new JSONObject();

		request.put("update", "all");
		request.put("freeOutMinutes", freeOutMinutes);
		request.put("freeInMinutes", freeInMinutes);
		request.put("freeTotalMinutes", freeTotalMinutes);
		request.put("availableCredits", availableCredit);
//		System.out.println(request.toJSONString());
		given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID).then().statusCode(200);
	}

	@Test
	private void UpdateCredit1() {
		JSONObject request = new JSONObject();

		request.put("update", "all");
		request.put("freeOutMinutes", "0");
		request.put("freeInMinutes", "0");
		request.put("freeTotalMinutes", "0");
		request.put("availableCredits", "500");

		given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + "5fa51c94d96bb41dcced302d").then()
				.statusCode(200);
	}

	@Test
	public void UpdateCreditCountyWise(String freeOutMinutes, String freeInMinutes, String freeTotalMinutes, String shortName, String userID) {
		JSONObject request = new JSONObject();

		request.put("update", "CountryWiseMinutes");
		request.put("freeOutMinutes", freeOutMinutes);
		request.put("freeInMinutes", freeInMinutes);
		request.put("freeTotalMinutes", freeTotalMinutes);
		request.put("shortName", shortName);

		given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID).then().statusCode(200);
		
	}
	
	@Test
	public void UpdateCreditCountyWisezero(String userID) {
		JSONObject request = new JSONObject();

		request.put("update", "CountryWiseMinutes");
		request.put("freeOutMinutes", "0");
		request.put("freeInMinutes", "0");
		request.put("freeTotalMinutes", "0");
		request.put("shortName", "US");

		given().header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON)
				.body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/numberUserBilling/" + userID).then().statusCode(200);
	}

	public void blockCountryAPI(String userId, String countryName, String countryCode) {

		JSONObject request = new JSONObject();
		boolean finalStatus;
		String respondeValueTrue = ": true";
		String respondeValueFalse = ": false";

		request.put("user", userId);
		request.put("name", countryName);
		request.put("code", countryCode);

		Response response = given().header("Content-Type", "application/json").contentType(ContentType.JSON)
				.accept(ContentType.JSON).body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/jenkins/country/block").then().extract().response();
		String CountryBlockResponseBody = response.prettyPrint();

		if (CountryBlockResponseBody.contains(respondeValueTrue)) {
			System.out.println("====Blocked country successfully:: " + countryName);
		} else if (CountryBlockResponseBody.contains(respondeValueFalse)) {
			System.out.println("====This country is already blocked ::" + countryName);
		} else {
			System.out.println("====Not found proper response in API of country block");
		}
	}

	public void unblockCountryAPI(String userId, String countryName, String countryCode) {

		JSONObject request = new JSONObject();
		boolean finalStatus;
		String respondeValueTrue = ": true";

		request.put("user", userId);
		request.put("name", countryName);
		request.put("code", countryCode);

		Response response = given().header("Content-Type", "application/json").contentType(ContentType.JSON)
				.accept(ContentType.JSON).body(request.toJSONString()).when()
				.post("https://"+apiURL+"-app.callhippo.com/jenkins/country/unblock").then().extract().response();
		String CountryUnBlockResponseBody = response.prettyPrint();

		if (CountryUnBlockResponseBody.contains(respondeValueTrue)) {
			System.out.println("====UnBlocked country successfully:: " + countryName);
		} else {
			System.out.println("====Not found proper response in API of country/unblock");
		}
	}
	
	
	
	@Test
	public void addCustomTimeSlot(String type, String userOrNumberID, boolean availability ) {
		// type: it will be 'user' or 'number'
		// id: it will be userID or NumberID
		// userId: it will be main userID
		
		// IsCustomAvailable:  if availability will be true then time slot will be -1 hour to +1 hour based on current time 
		// if availability will be false then time slot will be +2 hours based on current time 
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("script/userNumberAvailability");
		requestSpec.header("content-type","application/json");
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("type", type);
		requestParams.put("id", userOrNumberID);
		requestParams.put("isCustomAvailable", availability);
		requestParams.put("userId", userOrNumberID);
		
		requestSpec.body(requestParams.toString());
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
	}
	
	@Test
	public void addCustomTimeSlot(String type, String userOrNumberID, String userID, boolean availability ) {
		// type: it will be 'user' or 'number'
		// id: it will be userID or NumberID
		// userId: it will be main userID
		
		// IsCustomAvailable:  if availability will be true then time slot will be -1 hour to +1 hour based on current time 
		// if availability will be false then time slot will be +2 hours based on current time 
		
		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("script/userNumberAvailability");
		requestSpec.header("content-type","application/json");
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("type", type);
		requestParams.put("id", userOrNumberID);
		requestParams.put("isCustomAvailable", availability);
		requestParams.put("userId", userID);
		
		requestSpec.body(requestParams.toString());
		Response response = requestSpec.post("");
		int StatusCode = response.getStatusCode(); 
		System.out.println("Status code : " + StatusCode); 
		System.out.println(response.getBody().asString());
		
	}

	@Test
	public String gettotalInboundCallsFromLiveCallPage(String apiToken,String userId) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", apiToken);
		requestSpec.headers("id", userId);
		requestSpec.header("content-type","application/json");
		requestSpec.basePath("liveCallJenkins/" + userId);
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/liveCallJenkins/" + userId);
		
		int a = response.path("data.callLogsStats.totalInboundCalls");
		System.out.println(a);
		String totalInboundCall = Integer.toString(a);
		return totalInboundCall;
	}
	
	public String gettotalOutboundCallsFromLiveCallPage(String apiToken,String userId) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", apiToken);
		requestSpec.headers("id", userId);
		requestSpec.header("content-type","application/json");
		requestSpec.basePath("liveCallJenkins/" + userId);
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/liveCallJenkins/" + userId);
		
		int a = response.path("data.callLogsStats.totalOutboundCalls");
		System.out.println(a);
		String totalInboundCall = Integer.toString(a);
		return totalInboundCall;
	}
	
	public String gettotalMissedCallsFromLiveCallPage(String apiToken,String userId) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", apiToken);
		requestSpec.headers("id", userId);
		requestSpec.header("content-type","application/json");
		requestSpec.basePath("liveCallJenkins/" + userId);
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/liveCallJenkins/" + userId);
		
		int a = response.path("data.callLogsStats.totalMissedCalls");
		System.out.println(a);
		String totalInboundCall = Integer.toString(a);
		return totalInboundCall;
	}
	
	public String gettotalCallDurationFromLiveCallPage(String apiToken,String userId) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", apiToken);
		requestSpec.headers("id", userId);
		requestSpec.header("content-type","application/json");
		requestSpec.basePath("liveCallJenkins/" + userId);
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/liveCallJenkins/" + userId);
		
		int a = response.path("data.callLogsStats.totalCallDuration");
		int totalcall = response.path("data.callLogsStats.totalCalls");
		
		int seconds =a/totalcall;
        int p1 = seconds % 60;
        int p2 = seconds / 60;
        int p3 = p2 % 60;
        p2 = p2 / 60;
        
        int i=p3;
		java.text.DecimalFormat nft = new
		java.text.DecimalFormat("#00.###");
		nft.setDecimalSeparatorAlwaysShown(false);

		int j=p1;
		java.text.DecimalFormat nft1 = new
		java.text.DecimalFormat("#00.###");
		nft.setDecimalSeparatorAlwaysShown(false);
        
        return nft.format(i) + ":" + nft1.format(j);
        
	}
	// --------------------------------------------------------------------------------

	@Test
	public void getContactDetails() {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", "5fe19b444f82b75ccc9f99d3");
		requestSpec.basePath("contact/" + "5f3cf4b1d73e3e771629b5e2");
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/contact/" + "5f3cf4b1d73e3e771629b5e2");

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);

		String name = jsonPathEvaluator.get("name");
		System.out.println(name);

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString.contains("name"));

		System.out.println(bodyAsString);
	}

	@Test
	public void getContactDetailsFromPipeDrive() {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://developers.pipedrive.com");
		requestSpec.headers("api_token", "a70176477897d52ade3d371a84564dbc5bb44e07");
		requestSpec.basePath("users");
		Response response = given().spec(requestSpec).when().get("https://developers.pipedrive.com/users");

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		// System.out.println(bodyAsString.contains("name"));

		System.out.println(bodyAsString);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.id");
		System.out.println(name1);

	}

	@Test
	public void getContactDetails1() {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", "5fe041b94b7aed5f828dfef4");
		requestSpec.basePath("contact/" + "5f3cf4b1d73e3e771629b5e2");
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/contact/" + "5f3cf4b1d73e3e771629b5e2");

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);

		String apistrint = response.asString();
		List<Map<String, String>> booksOfUser = JsonPath.from(apistrint).get("data.name");
		Assert.assertEquals(2, booksOfUser.size());

		String name = jsonPathEvaluator.get("name");
		System.out.println(name);

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString.contains("name"));

		System.out.println(bodyAsString);
	}

	@Test
	public String getAccessTokenFromPipeDrive() {

		String refreshToken = getRefreshTockenforPipeDrive();
		String clientID = "ba3192864e02a648";
		String clientSecret = "89d3232d9b403aff394e193216317b90b29724b6";

		given().header("Content-Type", "application/x-www-form-urlencoded").auth().preemptive()
				.basic("ba3192864e02a648", "89d3232d9b403aff394e193216317b90b29724b6")
				.formParam("grant_type", "refresh_token")
				.formParam("refresh_token", "7484568:11424248:8366831574e8dfee1dac34225773b89c52e044c0").request()
				.post("https://oauth.pipedrive.com/oauth/token").getBody().asString();

//		System.out.println(given().header("Content-Type", "application/x-www-form-urlencoded").auth().preemptive()
//				.basic(clientID, clientSecret).formParam("grant_type", "refresh_token")
//				.formParam("refresh_token", refreshToken).request().post("https://oauth.pipedrive.com/oauth/token")
//				.getBody().asString());
		Response response = given().header("Content-Type", "application/x-www-form-urlencoded").auth().preemptive()
				.basic(clientID, clientSecret).formParam("grant_type", "refresh_token")
				.formParam("refresh_token", refreshToken).request().post("https://oauth.pipedrive.com/oauth/token");
		JsonPath jsonPathEvaluator = response.jsonPath();
		String accessToken = jsonPathEvaluator.getString("access_token");
//		System.out.println(accessToken);
		return accessToken;

	}

	@Test
	public String getRefreshTockenforPipeDrive() {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.basePath("5f3cbb0049f8e46e519ca469/pipedrive");
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/script/5f3cbb0049f8e46e519ca469/pipedrive");

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		// System.out.println(bodyAsString.contains("name"));

//		System.out.println(bodyAsString);

		JsonPath jsonPathEvaluator = response.jsonPath();
		String refreshToken = jsonPathEvaluator.getString("data.integrations[0].refreshToken");
		System.out.println(refreshToken);
		return refreshToken;
//		 
	}
	
	
	
	//-------------------------------------------------------
	
	@Test
	public int getPersonFromPipeDrive() {
		
		String accesToken = "Bearer "+getAccessTokenFromPipeDrive();
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/persons");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.name");
		System.out.println(name);

		String apistrint = response.asString();
		List<Map<String, String>> booksOfUser = JsonPath.from(apistrint).get("data.name");
//		Assert.assertEquals(2, booksOfUser.size());
		System.out.println("size of users of PipeDrive: "+booksOfUser.size());
		return booksOfUser.size();
	}
	
	@Test
	public int getContactFromCallHippo() {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", "5fe19b444f82b75ccc9f99d3");
		requestSpec.basePath("contact/" + "5f3cbb0049f8e46e519ca469");
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/contact/5f3cbb0049f8e46e519ca469");

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);

		String apistrint = response.asString();
		List<Map<String, String>> contactName = JsonPath.from(apistrint).get("data.name");
		//Assert.assertEquals(2, booksOfUser.size());
		int contactCount = contactName.size();

		String name = jsonPathEvaluator.get("name");
		System.out.println(name);

		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();

		System.out.println(bodyAsString.contains("name"));

		System.out.println(bodyAsString);
		
		System.out.println("size of users of CallHippo: "+contactCount);
		return contactCount;
	}
	
	
	
	
	@Test
	public void tset() {
		assertEquals(getPersonFromPipeDrive(), getContactFromCallHippo());
		
		for(int i=0;i<getPersonFromPipeDrive();i++) {
			String contactName = getPersonNameFromPipeDrive(i);
			
			assertEquals(validateContactNameInCallHippo(contactName), true);
			System.out.println("\n for loop :  "+i);
		}
	}
	
	
	@Test
	public boolean validateContactNameInCallHippo(String pipeDriveName) {
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://"+apiURL+"-app.callhippo.com");
		requestSpec.headers("apitoken", "5fe19b444f82b75ccc9f99d3");
		requestSpec.basePath("contact/" + "5f3cbb0049f8e46e519ca469");
		Response response = given().spec(requestSpec).when()
				.get("https://"+apiURL+"-app.callhippo.com/contact/5f3cbb0049f8e46e519ca469");

		JsonPath jsonPathEvaluator = response.jsonPath();
		String name1 = jsonPathEvaluator.getString("data.name");
		System.out.println(name1);
		
		System.out.println(name1.contains(pipeDriveName));

		return name1.contains(pipeDriveName);
	}
	
	@Test
	public String getPersonNameFromPipeDrive(int arraySize) {
		
		String accesToken = "Bearer "+getAccessTokenFromPipeDrive();
		
		RequestSpecification requestSpec = new RequestSpecBuilder().build();
		requestSpec.baseUri("https://api-proxy.pipedrive.com");
		requestSpec.basePath("persons");
		requestSpec.header("user-agent","'callhippo");
		requestSpec.header("authorization", accesToken);
		requestSpec.header("content-type","application/json");
		
		Response response = given().spec(requestSpec).when()
				.get("https://api-proxy.pipedrive.com/persons");
		ResponseBody body = response.getBody();

		String bodyAsString = body.asString();


		System.out.println(bodyAsString);
		
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String name = jsonPathEvaluator.getString("data.name["+arraySize+"]");
		System.out.println(name);
		return name;
		
	}
	
	
	

}
