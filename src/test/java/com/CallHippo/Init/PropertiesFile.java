package com.CallHippo.Init;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;

public class PropertiesFile {

	public static Properties prop;

	public PropertiesFile(String filepath1) throws Exception {

		FileReader reader = new FileReader(filepath1);
		prop = new Properties();
		prop.load(reader);
	}

	public String getValue(String propValue) throws IOException {

		return prop.getProperty(propValue);
	}
	
	public String signUp() throws IOException {

		return prop.getProperty("signup");
	}
	
	public String livesignUp() throws IOException {
		
		return prop.getProperty("livesignup");
	}

	public String signIn() throws IOException {

		return prop.getProperty("signin");
	}

	public String forgotPassword() throws IOException {

		return prop.getProperty("forgotPassword");
	}

	public String numbersPage() throws IOException {

		return prop.getProperty("numbersPage");
	}
	
	public String usersPage() throws IOException {

		return prop.getProperty("usersPage");
	}
	
	public String teamPage() throws IOException {

		return prop.getProperty("teamPage");
	}

	public String planPage() throws IOException {

		return prop.getProperty("plan");
	}
	
	public String dialerSignIn() throws IOException {

		return prop.getProperty("dialerSignin");
	}
	
	public String gmailId() throws IOException {

		return prop.getProperty("gmailId");
	}
	
	public String gmailPassword() throws IOException {

		return prop.getProperty("gmailPassword");
	}

	public String chAdminSignin() throws IOException {

		return prop.getProperty("chadminSignin");
	}
	
	public String userId() throws IOException {

		return prop.getProperty("userId");
	}
	
	public String dialerCreditPage() throws IOException {

		return prop.getProperty("dialerCredit");
	}
	
	public String dialerSMSPage() {

		return prop.getProperty("dialerSMS");
	}
	
	public String apiURL() {

		return prop.getProperty("apiURL");
	}
	
}
