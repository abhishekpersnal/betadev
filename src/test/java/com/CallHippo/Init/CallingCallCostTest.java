package com.CallHippo.Init;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CallingCallCostTest {
	static Common csv;
	WebDriver driver;
	XLSReader callCost;
	String countryCode;
	ArrayList<String> callPrice = new ArrayList<>();
	PropertiesFile data = new PropertiesFile("Data\\url Configuration.properties");

	String account1Number1 = "+12548314222";

	String outCallPriceUrl= "https://callhippo.com/downloads/outcallprices.csv";
	public CallingCallCostTest() throws Exception {
		csv = new Common();
	}

	@BeforeTest
	public void initialization() throws Exception {
		
		try {
			driver = TestBase.init();
			System.out.println("Opened webaap session");
			try {
				driver.get(outCallPriceUrl);
				Common.pause(5);
				System.out.println("Opened webaap signin Page");
				
			} catch (Exception e) {
				driver.get(outCallPriceUrl);
				Common.pause(5);
				System.out.println("Opened webaap signin Page");
			
			}
		     //callCost : file path of the outcallprice excel file
			callCost = new XLSReader(csv.cSvFileDownloadAndConvertIntoxlsx());
				
		} catch (Exception e) {
			String testname = "Add contact Before Method ";
			 Common.Screenshot(driver, testname, "driver Fail login");
			System.out.println("\n" + testname + " has been fail... \n");	
			}
		}
	@AfterTest
	public void tearDown() throws Exception {
	try{
		driver.quit();
	}catch (Exception e) {
		// TODO: handle exception
		driver.quit();
	}
	}
	public String callCost(String number,XLSReader filePath) throws Exception {
	
		callPrice = filePath.getFieldForPrice("prefix", "select * from sheet1 where country='United States'");
		// System.out.println("Before Remove country code:"+callPrice);
		countryCode = filePath.getField("countryCode", "select * from sheet1 where country='United States'");
		// System.out.println(countryCode);
		callPrice.remove(countryCode);

		// System.out.println("After Remove country code:"+callPrice);

		String callCostForNum = null;
		String accNumber = number.replace("+", "");
		System.out.println(accNumber);

		for (int i = 0; i < callPrice.size(); i++) {

			String num = accNumber.substring(0, callPrice.get(i).length());
			// System.out.println(+i+" "+num);
			if (num.equals(callPrice.get(i))) {
				callCostForNum = filePath.getField("price",
						"select * from sheet1 where prefix='" + callPrice.get(i) + "'");
				System.out.println("---------------callCostForNum" + callCostForNum);

			}

		}
		if (callCostForNum == null) {
			callCostForNum = filePath.getField("price", "select * from sheet1 where prefix='" + countryCode + "'");
			System.out.println("---------------NO callCostForNum:" + callCostForNum);
		}
		System.out.println("---------------callCostForNum::" + callCostForNum);
		return callCostForNum;
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void callCostTest() throws Exception {
		XLSReader filePath= new XLSReader(csv.getLastUpdateFile());
		callCost(account1Number1,filePath);

	}
}
